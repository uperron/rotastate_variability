# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.4'
#       jupytext_version: 1.2.1
#   kernelspec:
#     display_name: pychimera
#     language: python
#     name: chimera
# ---

# +
import pandas as pd
import warnings
from itertools import combinations

struct_df = pd.read_csv("tables/ALL_structure_wise_info.tab", sep="\t")

# For each PDB_id compute the RMSD against all other structures using Chimera
# !rm -rf rmsd.tmp
combinations = list(combinations(struct_df['PDB_id'].unique(), 2))
N = len(combinations)
i = .0
pychimera = "/Users/uperron/anaconda3/envs/chimera/bin/pychimera"
for pdb_1, pdb_2 in combinations:
    print "Starting RMSD for " +  pdb_1 + ";" + pdb_2
    !{pychimera} structfit.py pdb_files/{pdb_1}.pdb pdb_files/{pdb_2}.pdb >> rmsd.tmp
    i += 1
    print '%f done' % (100 * i / N)

# +
RMSD_df = pd.read_csv("rmsd.tmp", sep="\t")
RMSD_df.columns = ["pdb_id1", "pdb_id2",
"RMSD", "fullRMSD"]

regexes = ["pdb_files\/", "\.pdb"]
RMSD_df.pdb_id1 = RMSD_df.pdb_id1.replace(to_replace=regexes, value="", regex=True)
RMSD_df.pdb_id2 = RMSD_df.pdb_id2.replace(to_replace=regexes, value="", regex=True)
avg_df = RMSD_df[["pdb_id1", "RMSD"]].groupby("pdb_id1").mean()
avg_df["median_RMSD"] = RMSD_df[["pdb_id1", "RMSD"]].groupby("pdb_id1").median()
avg_df = avg_df.reset_index()
avg_df.columns = ["PDB_id", "mean_RMSD", "median_RMSD"]
avg_df.to_csv("tables/rmsd_avg.txt", sep="\t", index=False)
avg_df.head()
