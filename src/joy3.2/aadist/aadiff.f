      real function AADIFF(AA1,AA2)
C
C computes a physicochemical-based difference of two amino
C acids, inspired by Wolds work, J.Med. Chem 1998
C
      character*1 AA1, AA2
      real Z11, Z12, Z13, Z14, Z15
      real Z21, Z22, Z23, Z24, Z25
      real Z1DIFF, Z2DIFF, Z3DIFF, Z4DIFF, Z5DIFF
      real W1, W2, W3, W4, W5
C
C W is weight of components
C
      W1=1.0
      W2=1.0
      W3=1.0
      W4=1.0
      W5=1.0
C
      call AADIST(AA1,Z11,Z12,Z13,Z14,Z15) 
      call AADIST(AA2,Z21,Z22,Z23,Z24,Z25) 
C
      Z1DIFF=Z11-Z21
      Z2DIFF=Z12-Z22
      Z3DIFF=Z13-Z23
      Z4DIFF=Z14-Z24
      Z5DIFF=Z15-Z25
C
      AADIFF=abs(W1*Z1DIFF) + abs(W2*Z2DIFF) + abs(W3*Z3DIFF) +
     +       abs(W4*Z4DIFF) + abs(W5*Z5DIFF)
C
      return
      end
