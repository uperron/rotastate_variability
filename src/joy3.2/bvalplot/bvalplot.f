      program BVALPLOT
C
      include '../joy/joy.h'
C
      character*(MAXFILELEN) FILE
      real BVAL(MAXRES)
      integer NRES, I, J, IARGC, IIN, IIO
C
      data IIO /STDOUT/			! output to stdout
C
C writes out a grap representation of the Bvalue profile
C n.b. at the moment, the program only plots out the values from
C the CA atoms
C
      if (IARGC().gt.0) then
        IIN=1
        call GETARG(1,FILE)
        open (file=FILE,unit=1,status='old',form='formatted',err=3)
      else
        IIN=5
      end if
      call RDBVAL(IIN,BVAL,NRES)
      close (unit=IIN)
C
      if (NRES .gt. 1) then
        write (IIO,'(A)') FILE
        call GRAPSTART(IIO,NRES)
        do I=1,NRES-1
          write (IIO,'(''line from '',I4,'','',F6.2,'' to '',
     +           I4,'','',F6.2)') I,BVAL(I),I+1,BVAL(I+1)
        end do
        call GRAPEND(IIO)
      end if
C
      call EXIT(0)
C
3     call ERROR('bvalplot: cannot open file',FILE,1)
C
      end
C
C #########################################################################
C
      subroutine GRAPSTART(IIO,NRES)
      integer IIO, NRES
      write (IIO,'(''.G1'')')
      write (IIO,'(''label left "B"'')')
      write (IIO,'(''frame ht 2 wid 6 solid right invis top invis'')')
      write (IIO,'(''coord x 0,'',I4)') NRES+1
      return
      end
C
      subroutine GRAPEND(IIO)
      integer IIO
      write (IIO,'(''.G2'')')
      return
      end
C
C #########################################################################
C
      subroutine RDBVAL(IIN,BVAL,NRES)
C
      include '../joy/joy.h'
C
      character*66  CARD
      integer M, K, IIN, NRES, LENCARD
      real BVAL(MAXRES)
C
      M=0
4     read (IIN,'(I8,A)',end=3,err=902) LENCARD,CARD
        if (CARD(1:6) .eq. 'ATOM  ' .and. CARD(14:15) .eq. 'CA') then
          M=M+1
          if (M .gt. MAXRES) then
            call ERROR('bvalplot: too many residues',' ',1)
          end if
          if (LENCARD .gt. 65) then
            read (CARD,'(60X,F6.2)',err=903) BVAL(M)
          end if
        end if
      goto 4
3     continue
      NRES=M
C
      return
C
902   call ERROR('bvalplot: bad format in file',' ',1)
903   call ERROR('bvalplot: bad read of internal file',' ',1)
C
      end
