      program CAPLOT
C
      include '../joy/joy.h'
C
      character*(MAXFILELEN) FILE
      real CA(3,MAXRES), CUTOFF1, CUTOFF2, CUTOFF3, D1
      integer NRES, I, J, IARGC, IIN, IIO
C
      data IIO /STDOUT/			! output to stdout
      data CUTOFF1 / 64.0/		! 8**2
      data CUTOFF2 /196.0/		! 14**2
      data CUTOFF3 /400.0/		! 20**2
C
C writes out a grap representation of the Ca distance plot of a protein
C
      if (IARGC().gt.0) then
        IIN=1
        call GETARG(1,FILE)
        open (file=FILE,unit=1,status='old',form='formatted',err=3)
      else
        IIN=5
      end if
      call RDCA(IIN,CA,NRES)
      close (unit=IIN)
C
      write (IIO,'(A)') FILE
      call GRAPSTART(IIO,NRES)
C
      do I=1,NRES
        do J=I,NRES
          D1=DIST2(CA(1,I),CA(1,J))
          if (D1 .lt. CUTOFF1) then
            write (IIO,'(''bullet size+2 at '',I4,'','',I4)') I,J
          else if (D1 .lt. CUTOFF2) then
            write (IIO,'(''times at '',I4,'','',I4)') I,J
          else if (D1 .lt. CUTOFF3) then
            write (IIO,'(''dot at '',I4,'','',I4)') I,J
          end if
        end do
      end do
C
      call GRAPEND(IIO)
C
      call EXIT(0)
C
3     call ERROR('caplot: cannot open file',FILE,1)
C
      end
C
C #########################################################################
C
      subroutine GRAPSTART(IIO,NRES)
      integer IIO, NRES
      write (IIO,'(''.G1'')')
      write (IIO,'(''ticks off'')')
      write (IIO,'(''ticks left out'')')
      write (IIO,'(''ticks top out'')')
      write (IIO,'(''frame ht 6 wid 6 solid right invis bot invis'')')
      write (IIO,'(''coord x 0,'',I4,'' y 0,'',I4)') NRES,NRES
      write (IIO,'(''line from 0, 0 to '',I4,'','',I4)') NRES,NRES
      return
      end
C
      subroutine GRAPEND(IIO)
      integer IIO
      write (IIO,'(''.G2'')')
      return
      end
C
C #########################################################################
C
      subroutine RDCA(IIN,CA,NRES)
C
      include '../joy/joy.h'
C
      character*66  CARD
      integer M, K, IIN, NRES
      real CA(3,MAXRES)
C
      M=0
4     read (IIN,'(A)',end=3,err=902) CARD
        if (CARD(1:6) .eq. 'ATOM  ' .and. CARD(14:15) .eq. 'CA') then
          M=M+1
          if (M .gt. MAXRES) then
            call ERROR('rdca: too many residues',' ',1)
          end if
          read (CARD,'(13X,3X,1X,3X,1X,1X,5X,3X,3F8.3)',err=903)
     -          (CA(K,M),K=1,3)
        end if
      goto 4
3     continue
      NRES=M
C
      return
C
902   call ERROR('caplot: bad format in file',' ',1)
903   call ERROR('caplot: bad read of internal file',' ',1)
C
      end
