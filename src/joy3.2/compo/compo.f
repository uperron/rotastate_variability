      program COMPO
C
C calculate the composition profile for a given sequence
C
      implicit NONE
C
C ----------------------------------------------------------------------------
C
      integer MAXSEQ, MAXRES, MAXTITLE, MAXNEWSEQ, MAXOPT, MAXLAB,
     -        MAXTEXT, NUMEXAM, NUMFEAT, MAXFILELEN, MAXSHEET, MAXHELIX
      integer MAXHET, MAXATS
      integer STDOUT, STDIN, STDERR
      real SMALL
      parameter (MAXSEQ=60, MAXRES=10000, MAXTITLE=64, MAXOPT=10,
     -           MAXNEWSEQ=10, MAXLAB=3+MAXSEQ, MAXTEXT=6, NUMFEAT=13,
     -           NUMEXAM=12)
      parameter (STDOUT=6, STDIN=5, STDERR=0)
      parameter (MAXFILELEN=256, SMALL=1.E-15)
      parameter (MAXATS=30, MAXHET=500)
C
C ----------------------------------------------------------------------------
C
C Declarations for date.h
C
      character*40 DATE
      character*4 VERSION
C
C residues
C
      character*1 RESIDUE(20)
      real        NORMAL(20)
C
C declarations for VERBOSE variable, reports more information than usual
C
      logical VERBOSE
      common /VERBOSE/VERBOSE
C
C ----------------------------------------------------------------------------
C
      character*50 LBIN
C
      data RESIDUE /'A','C','D','E','F','G','H','I','K','L',
     +              'M','N','P','Q','R','S','T','V','W','Y'/
C
C NORMAL data is from my thesis (jpo 1990), high correlates with other
C composition work. Should really have sampling errors on these data
C
C n.b. data are % figures in the data statement below
C n.b. C is total for disulphide and thiol form
C
      data NORMAL / 8.4, 2.3, 5.9, 4.5, 3.5, 9.7, 2.1, 5.2, 5.5, 7.3,
     +              1.7, 4.7, 4.3, 3.7, 3.2, 8.4, 6.3, 7.7, 1.6, 3.8 /
C
C ----------------------------------------------------------------------------
C
C definitions for library functions
C
      include '../joylib/joylib.h'
C
      character*(MAXFILELEN) FILE
      integer ISEQ(MAXRES)
      integer NRES, IIN, IIO, IARGC, I, J
C
      integer NTOT(20)
      logical DIFF
C
C DIFF defines whether difference or absolute values are reported
C
      data DIFF /.FALSE./
C
      data IIO /STDOUT/
C
      NRES=0
C
      if (IARGC() .eq. 1) then
        IIN=1
        call GETARG(1,FILE)
        open (file=FILE,unit=IIN,form='formatted',err=3)
      else
        IIN=5
      end if
C
      call RDSEQ2(IIN,ISEQ,NRES)
C
      do I=1,20
        NTOT(I)=0
      end do
      do I=1,20
        do J=1,NRES
          if (ISEQ(J) .eq. I) then
            NTOT(I)=NTOT(I)+1
          end if
        end do
      end do
C
      open (unit=1,file='./grapfile',status='unknown',form='formatted')
      open (unit=2,file='./compo.dat',status='unknown',form='formatted')
C
C
      if (DIFF) then
        call GRAPSTART2()
        do I=1,20
          write (2,'(I4,1X,A,1X,F6.3)') I,RESIDUE(I),
     +                  (real(NTOT(I))/real(NRES)) - (NORMAL(I)/100.0)
        end do
      else
        call GRAPSTART1()
        do I=1,20
          write (2,'(I4,1X,A,1X,F6.3,1X,F6.3)') I,RESIDUE(I),
     +                  real(NTOT(I))/real(NRES),
     +                  NORMAL(I)/100.0
        end do
      end if
C
      call GRAPEND()
      close (1)
C
      call EXIT(0)
C
3     call ERROR('compo: cannot open file',FILE,1)
C
      end
C
C ######################################################
C
      subroutine GRAPSTART1()
      integer IIO
      write (1,'(''.G1'')')
      write (1,'(''label left "composition"'')')
      write (1,'(''label bot "residue"'')')
      write (1,'(''frame solid right invis top invis wid 5.0 ht 3.0'')')
      write (1,'(''coord x 1, 21 y 0, 0.15'')')
      write (1,'(''ticks bot off'')')
      write (1,'(''copy "compo.dat" thru X'')')
      write (1,'(''  line from $1,0 to $1,$3'')')
      write (1,'(''  line from $1,$3 to $1+1,$3'')')
      write (1,'(''  line from $1+1,$3 to $1+1,0'')')
      write (1,'(''  "$2" at $1+0.5,-0.01'')')
      write (1,'(''  line dotted from $1,$4 to $1+1,$4'')')
      return
      end


      subroutine GRAPSTART2()
      integer IIO
      write (1,'(''.G1'')')
      write (1,'(''label left "difference" "composition"'')')
      write (1,'(''label bot "residue"'')')
      write (1,'(''frame solid right invis top invis bot invis wid 5.0 ht 3.0'')')
      write (1,'(''coord x 1, 21 y -0.10, 0.10'')')
      write (1,'(''ticks bot off'')')
      write (1,'(''line from 0,0 to 21,0'')')
      write (1,'(''copy "compo.dat" thru X'')')
      write (1,'(''  line from $1,0 to $1,$3'')')
      write (1,'(''  line from $1,$3 to $1+1,$3'')')
      write (1,'(''  line from $1+1,$3 to $1+1,0'')')
      write (1,'(''  "$2" at $1+0.5,-0.01'')')
      return
      end

C
      subroutine GRAPEND()
      integer IIO
      write (1,'(''X'')')
      write (1,'(''.G2'')')
      return
      end

C
C ######################################################
C
      subroutine RDSEQ(IO,ISEQ,LENSEQ)
C
C reads in one sequence from unit IO
C
      include 'compo.h' 
C
      integer MAXLENLINE
      parameter (MAXLENLINE=80)
C
      character*(MAXLENLINE) LINE
      character*1 SEQ
      integer ISEQ(MAXRES), LENSEQ, IO, LENLINE, N, I
      logical FEND
      FEND=.false.
C
      N=0
1     read (IO,'(A)',end=2,err=9) LINE
      LENLINE=lastchar(LINE)
      if (index(LINE,'*') .ge. 1) then
        LENLINE=LENLINE-1
        FEND=.true.
      end if
      do I=1,LENLINE
        N=N+1
        if (N .gt. MAXRES) then
          call ERROR('too many residues in sequence',' ',1)
        end if
        read (LINE(I:I),'(A)') SEQ
        if (SEQ .eq. '-' .or. SEQ .eq. '/') then
          N=N-1
        else
          ISEQ(N)=SEQ2I(SEQ)
        end if
      end do
      LENSEQ=N
C
      if (FEND) then
        return
      else
        goto 1
      end if
C
      return
C
2     close (UNIT=IO)
      LENSEQ=N
      return
C
9     call ERROR('error reading line:',LINE(1:20),1)
      return
      end
C
C ############################################################################
C
      subroutine RDSEQ2(IO,ISEQ,NRES)
C
      include 'compo.h'
C
      character*14 CARD, TITLE
      integer IO, NRES, ISEQ(MAXRES)
C
2     read (IO,'(A)',end=1,err=901) CARD
      if (CARD(1:4) .eq. '>P1;') then
        read (CARD(5:),'(A)') TITLE
        read (IO,'(A)',end=3,err=901) CARD	! do nothing
        call RDSEQ(IO,ISEQ,NRES)
      end if
      goto 2
1     return
C
3     call ERROR('hydplot: error reading sequence',' ',1)
901   call ERROR('hydplot: error reading ',' ',1)
      end
