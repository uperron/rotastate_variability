      program DOTPLOT
C
C produce a simple dotplot for two sequences
C
C input of two PIR format sequence files
C
      implicit NONE
C
C ----------------------------------------------------------------------------
C
C PARAMETERS
C
C Parameter file for joy. Used as an include file.
C
C MAXSEQ    The maximum number of structures 
C MAXRES    The maximum number of positions in an alignment 
C MAXTITLE  The maximum number of substitution table titles
C
      integer MAXSEQ, MAXRES, MAXFILELEN
      integer STDOUT, STDIN, STDERR
      parameter (MAXSEQ=60, MAXRES=3500)
      parameter (STDOUT=6, STDIN=5, STDERR=0)
      parameter (MAXFILELEN=256)
C
C ----------------------------------------------------------------------------
C
C Declarations for date.h
C
      character*40 DATE
      character*4 VERSION
C
C declarations for VERBOSE variable, reports more information than usual
C
      logical VERBOSE
      common /VERBOSE/VERBOSE
C
C ----------------------------------------------------------------------------
C
      character*50 LBIN
C
C ----------------------------------------------------------------------------
C
C definitions for library functions
C
      include '../joylib/joylib.h'
C
      character*(MAXFILELEN) FILE1, FILE2
      character*14 LABEL1, LABEL2
      character*6 SYMBOL
      real HT, WID
      integer ISEQ1(MAXRES), ISEQ2(MAXRES)
      integer NRES1, NRES2, IIN, IIO, IARGC, I, J
C
      data IIO /STDOUT/
C
      NRES1=0
      NRES2=0
      SYMBOL='bullet'
      LABEL1='sequence 1    '
      LABEL2='sequence 2    '
C
      if (IARGC() .eq. 2) then
        IIN=1
        call GETARG(1,FILE1)
        call GETARG(2,FILE2)
        open (file=FILE1,unit=1,form='formatted',err=3)
        open (file=FILE2,unit=2,form='formatted',err=3)
      else
        write (STDERR,'(''dotplot: usage dotplot file1 file2'')')
        call EXIT(1)
      end if
C
C read in the two sequences
C
      call RDSEQ2(1,ISEQ1,NRES1,LABEL1)
      call RDSEQ2(2,ISEQ2,NRES2,LABEL2)
C
C some stuff to get axes correct, etc.
C
C default width (x) is 5.5i
C ht (y) is then LEN1/LEN2 * default_width
C
      WID=5.5
      HT=(real(NRES2)/real(NRES1)) * WID
      write (IIO,'(''.G1'')')
      write (IIO,'(''label left "'',A,''"'')') LABEL2
      write (IIO,'(''label bot "'',A,''"'')') LABEL1
      write (IIO,'(''frame ht '',F4.2,'' wid '',F4.2,'' solid '')') HT, WID
      write (IIO,'(''coord x 0, '',I4,'' y 0, '',I4)') NRES1, NRES2
C
C at the moment only mark identical residues, but easy to extend
C
      do I=1,NRES1
        do J=1,NRES2
          if (ISEQ1(I) .eq. ISEQ2(J)) then
            write (IIO,'(''  '',A,'' at '',I5,'','',I5)') SYMBOL, I, J
          end if
        end do
      end do
C
C now find glycosylation sites, and mark down axes
C
      do I=1,NRES1-2
        if (ISEQ1(I) .eq. 12) then
          if (ISEQ1(I+2) .eq. 16) then
            write (IIO,'(''  circle at -4, '',I4)') I
          else if (ISEQ1(I+2) .eq. 17) then
            write (IIO,'(''  circle at -4, '',I4)') I
          end if
        end if
      end do
      do I=1,NRES2-2
        if (ISEQ2(I) .eq. 12) then
          if (ISEQ2(I+2) .eq. 16) then
            write (IIO,'(''  circle at '',I4,'', -4'')') I
          else if (ISEQ2(I+2) .eq. 17) then
            write (IIO,'(''  circle at '',I4,'', -4'')') I
          end if
        end if
      end do
C
      write (IIO,'(''.G2'')')
C
      call EXIT(0)
C
3     call ERROR('dotplot: cannot open sequence file',' ',1)
C
      end
C
C ######################################################
C
      subroutine RDSEQ(IO,ISEQ,LENSEQ)
C
C reads in one sequence from unit IO
C
      include '../joy/joy.h' 
C
      integer MAXLENLINE
      parameter (MAXLENLINE=80)
C
      character*(MAXLENLINE) LINE
      character*1 SEQ
      integer ISEQ(MAXRES), LENSEQ, IO, LENLINE, N, I
      logical FEND
      FEND=.false.
C
      N=0
1     read (IO,'(A)',end=2,err=9) LINE
      LENLINE=lastchar(LINE)
      if (index(LINE,'*') .ge. 1) then
        LENLINE=LENLINE-1
        FEND=.true.
      end if
      do I=1,LENLINE
        N=N+1
        if (N .gt. MAXRES) then
          call ERROR('too many residues in sequence',' ',1)
        end if
        read (LINE(I:I),'(A)') SEQ
        if (SEQ .eq. '-' .or. SEQ .eq. '/') then
          N=N-1
        else
          ISEQ(N)=SEQ2I(SEQ)
        end if
      end do
      LENSEQ=N
C
      if (FEND) then
        return
      else
        goto 1
      end if
C
      return
C
2     close (UNIT=IO)
      LENSEQ=N
      return
C
9     call ERROR('error reading line:',LINE(1:20),1)
      return
      end
C
C ############################################################################
C
      subroutine RDSEQ2(IO,ISEQ,NRES,LABEL)
C
      include '../joy/joy.h'
C
      character*14 CARD, LABEL
      integer IO, NRES, ISEQ(MAXRES)
C
2     read (IO,'(A)',end=1,err=901) CARD
      if (CARD(1:4) .eq. '>P1;') then
        read (CARD(5:),'(A)') LABEL
        read (IO,'(A)',end=3,err=901) CARD	! do nothing
        call RDSEQ(IO,ISEQ,NRES)
      end if
      goto 2
1     return
C
3     call ERROR('hydplot: error reading sequence',' ',1)
901   call ERROR('hydplot: error reading ',' ',1)
      end
