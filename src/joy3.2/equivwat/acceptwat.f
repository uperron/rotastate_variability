      program ACCEPTWAT 
C
C read a PDB file, and report waters that are acceptable within a PDB file (more than
C a predetermined distances apart)
C
C ============================================================================
C
      include 'equivwat.h'
C
      integer MAXARGS
      parameter (MAXARGS=256)
      character*(MAXFILELEN) INFILE, OUTFILE, FILES(MAXARGS)
      character*(MAXFILELEN) IINFILE(2)
      character*14 OPTIONS
      character*5 RESNUM(MAXRES), HETNUM1(MAXHET), HETNUM2(MAXHET)
      character*4 VERSION, HETATM1(MAXHET), HETATM2(MAXHET)
      character*3 RESNAM(MAXRES), ATM(MAXATS,MAXRES), HETNAM1(MAXHET), HETNAM2(MAXHET)
      character*1 CHNNAM(MAXRES), SEQ(MAXRES), FLAG, ALT
      integer LENRES(MAXRES), NRES, NATS, NHET1, NHET2, IARGC, IOUT
      integer I, J, K, L, ISPAN, NCHN, IPREV, NCH, NFILES, III, N
      integer NEQUIV
      real COORDS(3,MAXATS,MAXRES), OCCUP(MAXATS,MAXRES)
      real BVAL(MAXATS,MAXRES)
      real HETCOORDS1(3,MAXHET), HETCOORDS2(3,MAXHET), DIST, ANGLCO, ANGLNH, HDIST, ENERGY
      real HATM(3,MAXRES), CATM(3), SHATM(3,MAXSIDE,MAXRES), HBENG
      logical DONOR(MAXATS,MAXRES), ACCEPTOR(MAXATS,MAXRES)
      logical DOHET, DOWAT, VERBOSE, TERMINUS(2,MAXRES)
      logical NEIGHBOUR, YES, DOPDB, COV, MUTUAL, NEIGHBOURM
      logical ALLOWED
C
      character*31 IDENT                         
      data IDENT /'@(#) asdfg.f - joy (c) jpo 1996'/
C
      include 'date.h'
C
      data DOHET    /.true./
      data DOWAT    /.true./
      data DOPDB    /.true./
      data VERBOSE  /.false./
      data MUTUAL   /.false./
C
C jpo 17-5-96: added mutual to define sites a little better
C
C -----------------------------------------------------------------------
C
C Get command line options
C
C Check no of arguments
C
      if (IARGC() .gt. MAXARGS) then
        call ERROR('acceptwat: argument list too long',' ',1)
      end if
C
C get first argument to see if it is an option, if it is then get all
C filenames
C If first argument not an option then get all filenames
C
C default output stream is STDOUT (IOUT)
C
      IOUT=STDOUT
C
C default distance cutoff (0.5 to start with)
C
      DISTVAL=2.0
      DISTVAL2=DISTVAL*DISTVAL
C
      call GETARG(1,OPTIONS)
      if (OPTIONS(1:1) .eq. '-') then
        if (index(OPTIONS,'V') .gt. 0) then
          write (STDERR,'(''acceptwat - version '',A,'' ('',A,
     -           '') - copyright 1996 jpo, compiled - '',A)')
     -           VERSION(),LIBVERSION(),DATE
          if (IARGC() .lt. 2) then
            call EXIT(0)
          end if
        end if
        if (index(OPTIONS(2:),'-') .gt. 0) then
          IOUT=STDOUT
        end if
        if (index(OPTIONS(2:),'d') .gt. 0) then
          read (OPTIONS(3:),'(F5.3)',err=904) DISTVAL
          DISTVAL2=DISTVAL*DISTVAL
        end if
        NFILES=0
        do I=2,IARGC()
          NFILES=NFILES+1
          call GETARG(I,FILES(NFILES))
        end do
        NFILES=IARGC()-1
      else
        do I=1,IARGC()
          call GETARG(I,FILES(I))
        end do
        NFILES=IARGC()
      end if
      if (NFILES .ne. 2) then
        write (STDERR,'(''acceptwat: incorrect number of files'')')
        call EXIT(1)
      end if

c     write (6,*) FILES(1), FILES(2)
C
C -----------------------------------------------------------------------
C
C if file does not have an extension, generate one with .atm extension
C
      do I=1,2
        IINFILE(I)=FILES(I)
        if (index(INFILE,'.') .lt. 1) then
          if (FILEEXIST(INFILE(1:LASTCHAR(INFILE))//'.atm')) then
            IINFILE(I)=INFILE(1:LASTCHAR(INFILE))//'.atm'
          end if
        end if
        if (.not. FILEEXIST(IINFILE(I))) then
          call ERROR('acceptwat: cannot find file',IINFILE(I),0)
        end if
      end do
C
C ------------------------------------------------------------------------
C
C Read in the PDB format data
C
        call RDPDB(IINFILE(2),ATM,RESNUM,RESNAM,CHNNAM,COORDS,OCCUP,BVAL,
     -             LENRES,NATS,NRES,NHET2,HETCOORDS2,
     -             HETNUM2,HETNAM2,HETATM2,VERBOSE)
        call RDPDB(IINFILE(1),ATM,RESNUM,RESNAM,CHNNAM,COORDS,OCCUP,BVAL,
     -             LENRES,NATS,NRES,NHET1,HETCOORDS1,
     -             HETNUM1,HETNAM1,HETATM1,VERBOSE)
C
C ------------------------------------------------------------------------
C
      write (IOUT,'(''REMARK produced by AcceptWat'')')
      write (IOUT,'(''REMARK distance cutoff '',F5.3)') DISTVAL
      NEQUIV=0
      do I=1,NHET2
        if (HETNAM2(I) .eq. 'HOH') then
          ALLOWED=.true.
          do J=1,NRES
            do K=1,LENRES(I)
              if (DIST2(COORDS(1,K,J),HETCOORDS2(1,I)) .lt. DISTVAL2) then
                ALLOWED=.false.
              end if
            end do
          end do
          if (ALLOWED) then
            NEQUIV=NEQUIV+1
            if (DOPDB) then
              write (IOUT,'(''HETATM'',I5,''  O   HOH '',I5,4X,3F8.3)')
     +               NEQUIV,NEQUIV,
     +             HETCOORDS2(1,I),
     +             HETCOORDS2(2,I),
     +             HETCOORDS2(3,I)
            end if
          end if
        end if
      end do
C
C --------------------------------------------------------------------------
C
      call EXIT(0)
C
C ---------------------------------------------------------------------------
C
C Catch bad distance value, etc.
C
5     call ERROR('acceptwat: error opening file',OUTFILE,1)
903   call ERROR('acceptwat: error writing output !',' ',1)
904   call ERROR('acceptwat: error parsing options',OPTIONS,1)
C
      end 
