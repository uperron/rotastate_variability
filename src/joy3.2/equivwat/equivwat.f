      program EQUIVWAT 
C
C read a PDB file, and report waters that are equivalent (less than
C a predetermined distances apart)
C
C ============================================================================
C
      include 'equivwat.h'
C
      integer MAXARGS
      parameter (MAXARGS=256)
      character*(MAXFILELEN) INFILE, OUTFILE, FILES(MAXARGS)
      character*(MAXFILELEN) IINFILE(2)
      character*14 OPTIONS
      character*5 RESNUM(MAXRES), HETNUM1(MAXHET), HETNUM2(MAXHET)
      character*4 VERSION, HETATM1(MAXHET), HETATM2(MAXHET)
      character*3 RESNAM(MAXRES), ATM(MAXATS,MAXRES), HETNAM1(MAXHET), HETNAM2(MAXHET)
      character*2 HBTYPE
      character*1 CHNNAM(MAXRES), SEQ(MAXRES), FLAG, ALT
      integer LENRES(MAXRES), NRES, NATS, NHET1, NHET2, IARGC, IOUT
      integer I, J, K, L, ISPAN, NCHN, IPREV, NCH, NFILES, III, N
      integer NEQUIV
      real COORDS(3,MAXATS,MAXRES), OCCUP(MAXATS,MAXRES)
      real BVAL(MAXATS,MAXRES)
      real HETCOORDS1(3,MAXHET), HETCOORDS2(3,MAXHET), DIST, ANGLCO, ANGLNH, HDIST, ENERGY
      real HATM(3,MAXRES), CATM(3), SHATM(3,MAXSIDE,MAXRES), HBENG
      logical DONOR(MAXATS,MAXRES), ACCEPTOR(MAXATS,MAXRES)
      logical DOHET, DOWAT, VERBOSE, TERMINUS(2,MAXRES)
      logical NEIGHBOUR, YES, DOPDB, COV, NEIGHBOURM
C
      character*31 IDENT                         
      data IDENT /'@(#) equiw.f - joy (c) jpo 1996'/
C
      include 'date.h'
C
      data DOHET    /.true./
      data DOWAT    /.true./
      data DOPDB    /.false./
      data VERBOSE  /.false./
C
C -----------------------------------------------------------------------
C
C Get command line options
C
C Check no of arguments
C
      if (IARGC() .gt. MAXARGS) then
        call ERROR('equivwat: argument list too long',' ',1)
      end if
C
C get first argument to see if it is an option, if it is then get all
C filenames
C If first argument not an option then get all filenames
C
C default output stream is STDOUT (IOUT)
C
      IOUT=STDOUT
C
C default distance cutoff (1.5 to start with)
C
      DISTVAL=1.5
      DISTVAL2=DISTVAL*DISTVAL
C
      call GETARG(1,OPTIONS)
      if (OPTIONS(1:1) .eq. '-') then
        if (index(OPTIONS,'V') .gt. 0) then
          write (STDERR,'(''equivwat - version '',A,'' ('',A,
     -           '') - copyright 1996 jpo, compiled - '',A)')
     -           VERSION(),LIBVERSION(),DATE
          if (IARGC() .lt. 2) then
            call EXIT(0)
          end if
        end if
        if (index(OPTIONS(2:),'-') .gt. 0) then
          IOUT=STDOUT
        end if
        if (index(OPTIONS(2:),'p') .gt. 0) then
          DOPDB=.true.
        end if
        if (index(OPTIONS(2:),'d') .gt. 0) then
          read (OPTIONS(3:),'(F5.3)',err=904) DISTVAL
          DISTVAL2=DISTVAL*DISTVAL
        end if
        NFILES=0
        do I=2,IARGC()
          NFILES=NFILES+1
          call GETARG(I,FILES(NFILES))
        end do
        NFILES=IARGC()-1
      else
        do I=1,IARGC()
          call GETARG(I,FILES(I))
        end do
        NFILES=IARGC()
      end if
      if (NFILES .ne. 2) then
        write (STDERR,'(''equivwat: incorrect number of files'')')
        call EXIT(1)
      end if

c     write (6,*) FILES(1), FILES(2)
C
C -----------------------------------------------------------------------
C
C if file does not have an extension, generate one with .atm extension
C
      do I=1,2
        IINFILE(I)=FILES(I)
        if (index(INFILE,'.') .lt. 1) then
          if (FILEEXIST(INFILE(1:LASTCHAR(INFILE))//'.atm')) then
            IINFILE(I)=INFILE(1:LASTCHAR(INFILE))//'.atm'
          end if
        end if
        if (.not. FILEEXIST(IINFILE(I))) then
          call ERROR('equivwat: cannot find file',IINFILE(I),0)
        end if
      end do
C
C ------------------------------------------------------------------------
C
C Read in the PDB format data
C
        call RDPDB(IINFILE(1),ATM,RESNUM,RESNAM,CHNNAM,COORDS,OCCUP,BVAL,
     -             LENRES,NATS,NRES,NHET1,HETCOORDS1,
     -             HETNUM1,HETNAM1,HETATM1,VERBOSE)
        call RDPDB(IINFILE(2),ATM,RESNUM,RESNAM,CHNNAM,COORDS,OCCUP,BVAL,
     -             LENRES,NATS,NRES,NHET2,HETCOORDS2,
     -             HETNUM2,HETNAM2,HETATM2,VERBOSE)
C
C ------------------------------------------------------------------------
C
c     write (IOUT,'(''REMARK Produced by Equivwat'')')
      write (IOUT,'(''REMARK number of hetatoms '',I5,1X,I5)') NHET1,NHET2
      write (IOUT,'(''REMARK distance cutoff '',F5.3)') DISTVAL
      write (IOUT,'(''REMARK   --residue--    --residue--    dist '',F5.3)')
      NEQUIV=0
      do I=1,NHET1
        do J=1,NHET2
          if (HETNAM1(I) .eq. 'HOH' .and. HETNAM2(J) .eq. 'HOH') then
            if (DIST2(HETCOORDS1(1,I),HETCOORDS2(1,J)) .le. DISTVAL2) then
              write (IOUT,'(''REMARK'',3X,A,1X,A,''  --  '',A,1X,A,4X,F5.3)') HETNAM1(I),HETNUM1(I),
     +               HETNAM2(J),HETNUM2(J),
     +               SQRT(DIST2(HETCOORDS1(1,I),HETCOORDS2(1,J)))
              NEQUIV=NEQUIV+1
              if (DOPDB) then
                write (IOUT,'(''HETATM'',I5,''  O   HOH '',I5,4X,3F8.3)')
     +                 NEQUIV,NEQUIV,
     +               (HETCOORDS1(1,I)+HETCOORDS2(1,J))/2.0,
     +               (HETCOORDS1(2,I)+HETCOORDS2(2,J))/2.0,
     +               (HETCOORDS1(3,I)+HETCOORDS2(3,J))/2.0
              end if
            end if
          end if
        end do
      end do
C
C --------------------------------------------------------------------------
C
      call EXIT(0)
C
C ---------------------------------------------------------------------------
C
C Catch bad distance value, etc.
C
5     call ERROR('equivwat: error opening file',OUTFILE,1)
903   call ERROR('equivwat: error writing output !',' ',1)
904   call ERROR('equivwat: error parsing options',OPTIONS,1)
C
      end 
