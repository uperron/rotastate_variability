C #############################################################################
C
C Parameters for site
C
C MAXFILELEN - maximum length of filename
C
      implicit NONE
      integer MAXFILELEN
      parameter (MAXFILELEN=14)
C
C MAXRES - The maximum number of residues 
C MAXATS - The maximum number of atoms that can occur in a residue (14 for
C          C-terminal tryptophan)
C MAXHET - The maximum number of het-atoms in a file
C MAXSIDE - The maximum number of sidechain polar hydrogens (5 for ARG)
C
      integer MAXRES, MAXATS, MAXHET, MAXSIDE
      parameter (MAXRES=1000, MAXATS=20, MAXHET=1000, MAXSIDE=5)
C
      real DISTVAL, DISTVAL2
C
      character*18 DATE
C
C define STDIN, STDOUT, STDERR
C
      integer STDIN, STDOUT, STDERR
      parameter (STDIN=5, STDOUT=6, STDERR=0)
C
C include joylib definitions
C
      include '../joylib/joylib.h'
C
C #############################################################################
