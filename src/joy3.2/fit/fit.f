      SUBROUTINE RMSFIT(X1,X2,RMS,N,IW,WT)
C
C Subroutine that returns the RMSD between two sets of coordinates
C
      REAL          X1(3,1),X2(3,1),WT(1)
C
      IF (IW.LE.0) THEN
        RMS=0.
        DO I=1,N
          RMS=RMS+(X1(1,I)-X2(1,I))**2+
     -          (X1(2,I)-X2(2,I))**2+
     -          (X1(3,I)-X2(3,I))**2
        end do
        RMS=SQRT(RMS/FLOAT(N))
      ELSE
        WW=0.
        RMS=0.
        DO i=1,N
          WW=WW+WT(I)
          RMS=RMS+WT(I)*((X1(1,I)-X2(1,I))**2+
     -                   (X1(2,I)-X2(2,I))**2+
     -                   (X1(3,I)-X2(3,I))**2)
          end do
        RMS=SQRT(RMS/WW)
      END IF
C
      END
C
C ----------------------------------------------------------------------------
C
      SUBROUTINE ROTFIT(X,R,N)
C
C  Rotates contents of X using rotation matrix R.
C
      REAL          X(3,N),R(3,3)
C
      DO I=1,N
        X1=R(1,1)*X(1,I)+R(1,2)*X(2,I)+R(1,3)*X(3,I)
        X2=R(2,1)*X(1,I)+R(2,2)*X(2,I)+R(2,3)*X(3,I)
        X3=R(3,1)*X(1,I)+R(3,2)*X(2,I)+R(3,3)*X(3,I)
        X(1,I)=X1
        X(2,I)=X2
        X(3,I)=X3
      end do
C
      RETURN
      END
C
C ----------------------------------------------------------------------------
C
      SUBROUTINE ORIGIN(X1,X2,N,NC,C,WT)
C
      REAL          X1(3,N),X2(3,NC),C(3),WT(N)
      SW=0.
      DO I=1,N
        SW=SW+WT(I)
      end do
C
      DO 1 J=1,3
      C(J)=0.
      DO 2 I=1,N
      C(J)=C(J)+X1(J,I)*WT(I)
2     CONTINUE
      C(J)=C(J)/SW
      DO 3 I=1,N
      X1(J,I)=X1(J,I)-C(J)
3     CONTINUE
      DO 4 I=1,NC
      X2(J,I)=X2(J,I)-C(J)
4     CONTINUE
1     CONTINUE
      END
C
C ----------------------------------------------------------------------------
C
      SUBROUTINE MATFIT(X1,X2,RM,N,IW,WT1)
C
      DIMENSION UMAT(3,3),RM(3,3),WT1(500),X1(3,500),X2(3,500)
C
      IF (N.LE.2) THEN
         WRITE(6,'(''rms: too few points fitted'')') 
         call exit(1)
      END IF
C
      IF (IW.LE.0) THEN
	DO I=1,3
	  DO J=1,3
	     UMAT(I,J)=0.0
          end do
	  DO J=1,N
	    DO K=1,3
	      UMAT(I,K)=UMAT(I,K)+X1(I,J)*X2(K,J)
            end do
          end do
        end do
      ELSE
	DO 40,I=1,3
	  DO 30,J=1,3
	    UMAT(I,J)=0.0
30        CONTINUE
	  DO 41,J=1,N
	    DO 42,K=1,3
	      UMAT(I,K)=UMAT(I,K)+WT1(J)*X1(I,J)*X2(K,J)
42          CONTINUE
41        CONTINUE
40      CONTINUE
      END IF
C
C Fit it
C
      CALL QIKFIT(UMAT,RM)
C
      RETURN
      END
C
C ----------------------------------------------------------------------------
C
      SUBROUTINE QIKFIT(UMAT, RM )
C
      REAL VMAT(3,3),RSUM
C
      DIMENSION UMAT(3,3),ROT(3,3),COUP(3),DIR(3),STEP(3)
      DIMENSION TURMAT(3,3),C(3,3),V(3),RM(3,3)
C
      DATA SMALL/1.0E-20/,SMALSN/1.0E-10/
C
      DO 105 L=1,3
        DO 106 M=1,3
          ROT(L,M)=0.0
  106   CONTINUE
        ROT(L,L)=1.0
  105 CONTINUE
C
      JMAX=30
      RTSUM=UMAT(1,1)+UMAT(2,2)+UMAT(3,3)
      DELTA=0.0
      DO 250 NCYC=1,JMAX
C
C--     MODIFIED CONJUGATE GRADIENT
C--     FOR FIRST AND EVERY NSTEEP CYCLES SET PREVIOUS STEP AS
C--     ZERO AND TAKE STEEPEST DESCENT PATH.
C
        NSTEEP=3
        NCC1=NCYC-1
        NREM=NCC1-(NCC1/NSTEEP)*NSTEEP
        IF (NREM.NE.0) GOTO 115
        DO 110 I=1,3
          STEP(I)=0.0
  110   CONTINUE
        CLEP=1.0
  115   CONTINUE
C
C--     COUPLE
C
        COUP(1)=UMAT(2,3)-UMAT(3,2)
        COUP(2)=UMAT(3,1)-UMAT(1,3)
        COUP(3)=UMAT(1,2)-UMAT(2,1)
        CLE=SQRT(COUP(1)**2+COUP(2)**2+COUP(3)**2)
C
C--     GRADIENT VECTOR IS NOW -COUP
C
        GFAC=(CLE/CLEP)**2
C
C--     VALUE OF RTSUM FROM PREVIOUS STEP
C
        RTSUMP=RTSUM
        DELTAP=DELTA
        CLEP=CLE
        IF(CLE.LT.SMALL) GOTO 300
C
C--     STEP VECTOR CONJUGATE TO PREVIOUS
C
        STP=0.0
        DO 120 I=1,3
          STEP(I)=COUP(I)+STEP(I)*GFAC
          STP=STP+STEP(I)**2
  120   CONTINUE
        STP=1.0/SQRT(STP)
C
C--     NORMALISED STEP
C
        DO 130 I=1,3
          DIR(I)=STP*STEP(I)
  130   CONTINUE
C
C--     COUPLE RESOLVED ALONG STEP DIRECTION
C
        STCOUP=COUP(1)*DIR(1)+COUP(2)*DIR(2)+COUP(3)*DIR(3)
C
C--     COMPONENT OF UMAT ALONG DIRECTION
C
        UD=0.0
        DO 140 L=1,3
          DO 140 M=1,3
            UD=UD+UMAT(L,M)*DIR(L)*DIR(M)
  140   CONTINUE
        TR=UMAT(1,1)+UMAT(2,2)+UMAT(3,3)-UD
        TA=SQRT(TR**2+STCOUP**2)
        CS=TR/TA
        SN=STCOUP/TA
C
C--     IF(CS.LT.0) THEN PRESENT POSITION IS UNSTABLE SO DO NOT STOP
C
        IF((CS.GT.0.0).AND.(ABS(SN).LT.SMALSN)) GOTO 300
  150   CONTINUE
C
C--     TURN MATRIX FOR CORRECTING ROTATION
C
C--     SYMMETRIC PART
C
        AC=1.0-CS
        DO 160 L=1,3
          V(L)=AC*DIR(L)
          DO 155 M=1,3
            TURMAT(L,M)=V(L)*DIR(M)
  155     CONTINUE
        TURMAT(L,L)=TURMAT(L,L)+CS
        V(L)=DIR(L)*SN
  160   CONTINUE
C
C--     ASSYMETRIC PART
C
        TURMAT(1,2)=TURMAT(1,2)-V(3)
        TURMAT(2,3)=TURMAT(2,3)-V(1)
        TURMAT(3,1)=TURMAT(3,1)-V(2)
        TURMAT(2,1)=TURMAT(2,1)+V(3)
        TURMAT(3,2)=TURMAT(3,2)+V(1)
        TURMAT(1,3)=TURMAT(1,3)+V(2)
C
C--     UPDATE TOTAL ROTATION MATRIX
C
        DO 170 L=1,3
          DO 170 M=1,3
            C(L,M)=0.0
            DO 170 K=1,3
              C(L,M)=C(L,M)+TURMAT(L,K)*ROT(K,M)
  170   CONTINUE
        DO 180 L=1,3
          DO 180 M=1,3
            ROT(L,M)=C(L,M)
  180   CONTINUE
C
C--     UPDATE UMAT TENSOR
C
        DO 190 L=1,3
          DO 190 M=1,3
            C(L,M)=0.0
              DO 190 K=1,3
                C(L,M)=C(L,M)+TURMAT(L,K)*UMAT(K,M)
  190   CONTINUE
        DO 200 L=1,3
          DO 200 M=1,3
            UMAT(L,M)=C(L,M)
  200   CONTINUE
        RTSUM=UMAT(1,1)+UMAT(2,2)+UMAT(3,3)
        DELTA=RTSUM-RTSUMP
C
C--     IF NO IMPROVEMENT IN THIS CYCLE THEN STOP
C
        IF (ABS(DELTA).LT.SMALL) GOTO 300
C
C--     NEXT CYCLE
C
  250 CONTINUE
  300 CONTINUE
      RSUM=RTSUM
C
C--   COPY ROTATION MATRIX FOR OUTPUT
C
      DO 310 I=1,3
        DO 310 J=1,3
          RM(J,I)=ROT(I,J)
  310 CONTINUE
      RETURN
      END
C
C ----------------------------------------------------------------------------
C
      SUBROUTINE DISTAN(X,Y,D)
C
C-- Calculates the distance between X and Y.
C
      REAL  X(3),Y(3)
C
      D=SQRT((X(1)-Y(1))**2+(X(2)-Y(2))**2+(X(3)-Y(3))**2)
      END
C
C ----------------------------------------------------------------------------
C
      SUBROUTINE LJUST(CARD,LEN,NCHAR)
C
C Modified to allow for zero length strings
C
      CHARACTER    CARD(LEN)
      NPOS=1
C
10    IF (CARD(NPOS).EQ.' ') THEN
        NPOS=NPOS+1
        IF (NPOS.GE.LEN) THEN
          NCHAR=0
          RETURN
        END IF
        GOTO 10
      END IF
C
      DO 20 I=1,LEN-NPOS+1
        CARD(I)=CARD(I+NPOS-1)
20    CONTINUE
      DO 30 I=LEN-NPOS+2,LEN
        CARD(I)=' '
30    CONTINUE
      NCHAR=LEN-NPOS+1
C
      RETURN
      END
