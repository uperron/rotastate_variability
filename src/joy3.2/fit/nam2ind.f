      subroutine FINDSTRING(PROBE,LIST,LENLIST,IINDEX)
C
C Finds an atom name in a list and returns its address
C
      parameter (MAXITEMS=200)
C
      character*(*) PROBE
      character*(*) LIST(MAXITEMS)
      integer IINDEX,LENLIST
C
      do I=1,LENLIST
        if (LIST(I) .eq. PROBE) then
          IINDEX=I
          return
        end if
      end do
C
      return
      end
