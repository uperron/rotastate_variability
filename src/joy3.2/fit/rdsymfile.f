      subroutine RDSYMFILE(SYMFILE,N)
C
C read symmetry file
C
      integer STDERR, MAXBLOCK
      parameter (STDERR=0, MAXPAIR=10, MAXBLOCK=5)
C
      character*(*) SYMFILE 
      character*19 PAIR1(MAXPAIR,MAXBLOCK), PAIR2(MAXPAIR,MAXBLOCK)
      character*40 BUFFER
      integer N, NSIZE(MAXBLOCK)
C
      N=1
      do I=1,MAXBLOCK
        NSIZE(I)=0
      end do
C
      open (unit=1, file=SYMFILE, status='old', form='formatted', err=901)
C
C check file is OK
C
      read (1,'(A)',err=902) BUFFER
      if (BUFFER(1:23) .ne. '# symmetry file for rms') then
        write (STDERR,'(''rms: symmetry file not valid'')')
        call EXIT(1)
      end if 
C
C process contents of file, main read loop
C
2     read (1,'(A)',err=902,end=1) BUFFER
C
      if (BUFFER(1:1) .eq. '#') then
        N=N+1
        NSIZE(N)=0
      else
        NSIZE(N)=NSIZE(N)+1
        PAIR1(NSIZE(N),N)=BUFFER(1:19)
        PAIR2(NSIZE(N),N)=BUFFER(22:40)
      end if
      goto 2
1     continue
C
      close (unit=1)
C
      return
C
901   write (STDERR,'(''rms: error opening symmetry file'')')
      call EXIT(1)
902   write (STDERR,'(''rms: error reading symmetry file'')')
      call EXIT(1)
C
      end
