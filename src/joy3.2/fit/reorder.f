      subroutine REORDER(X,NATS,IORD)
C
C reorders a coordinate array (X of size N) into order defined
C by IORD, X is over-written in this version
C
      parameter (MAXATS=500)
      integer NATS, IORD
      real X(3,MAXATS), XTMP(3,MAXATS)
C
C load in XTMP
C
      do I=1,NATS
        do J=1,3
          XTMP(J,I)=X(J,I)
        end do
      end do
C
      do I=1,NATS
        do J=1,3
          X(J,I)=XTMP(J,IORD)
        end do
      end do
C
      return
