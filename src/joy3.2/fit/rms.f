      program RMS
C
C JPO Nov - 88
C
C Simple RMS fitting program, Fits two datasets to each other, note that
C these must have an equal number of CA records in them, otherwise the
C program will crash.
C
C JPO Jan - 97
C
C modified to be useful in pairwise comparison of MD datasets, basic idea is
C to fit two conformations, and report RMSD. Made more general, and tidied
C up compared to first version, primarily no longer requires CA points !
C Oh, how my style has changed !
C *) Added -A option for CA only fit
C *) added -H option for heavy atoms only
C *) added -S option for symmetry related atoms (not fully tested), this
C             was more challenging to imlement than I initially thought
C             and is not completed (two hours spent codiing so far)
C *) added -D option for drms based metrics
C
C
      integer STDIN, STDOUT, STDERR
      parameter (STDIN=5, STDOUT=6, STDERR=0)
      parameter (MAXATS=500, MAXCONF=24)
C
      character*256 FILE1,FILE2,OPTIONS
      character*256 SYMFILE
      character*80 TEXT
      character*30 TEXT1(MAXATS),TEXT2(MAXATS)
      character*5  RESNM1(MAXATS),RESNM2(MAXATS)
      character*4  VERSION
      character*3  RESTP1(MAXATS),RESTP2(MAXATS)
      real X1(3,MAXATS),X2(3,MAXATS)
      real XALT(3,MAXATS)
      real ROT(3,3),C1(3),C2(3),WT(MAXATS),TRANS(3),DIST(MAXATS)
      integer NATS1,NATS2,NBLOCK
      logical OUTCOR,OUTTRA,OUTDIS,FEXIST,USEDOPT,CAONLY,HEAVYONLY
      logical USESYM,SWAP(MAXATS),USEDRMS
C
C --------------------------------------------------------------------------
C
      data WT        /MAXATS*1.0/
      data CAONLY    /.false./
      data HEAVYONLY /.false./
      data USESYM    /.false./
      data OUTCOR    /.false./
      data OUTTRA    /.false./
      data OUTDIS    /.false./
      data USEDOPT   /.false./
      data USEDRMS   /.false./
      data VERSION   /' 0.4'/
C
C --------------------------------------------------------------------------
C
C Option handling
C
      call getarg(1,OPTIONS)
      if (index(OPTIONS,'.') .ge. 1) then
        FILE1=OPTIONS
        goto 1
      else if (index(OPTIONS,'h').ge.1) then
        write (STDERR,'(''rms - a simple fitting program '')')
        write (STDERR,'(''usage: rms -tf coords.1 coords.2'')')
        write (STDERR,'(''     -h print OPTIONS'')')
        write (STDERR,'(''     -A fit on only CA records'')')
        write (STDERR,'(''     -t output transformation matrix'')')
        write (STDERR,'(''     -f transform fitted set2 to set1 (depends on -H option)'')')
        write (STDERR,'(''     -H ignore all hydrogen atoms'')')
        write (STDERR,'(''     -S use symmetry (from ./sym.dat)'')')
        write (STDERR,'(''     -D use drms metrics (implicitly no superposition)'')')
        call exit(2)
      end if
      if (index(OPTIONS,'v').ge.1) then
        write (STDERR,'(''rms - version '',A)') VERSION
        USEDOPT=.true.
      end if
      if (index(OPTIONS,'t').ge.1) then
        outtra=.true.
        USEDOPT=.true.
      end if
      if (index(OPTIONS,'d').ge.1) then
        outdis=.true.
        USEDOPT=.true.
      end if
      if (index(OPTIONS,'A').ge.1) then
        CAONLY=.true.
        USEDOPT=.true.
      end if
      if (index(OPTIONS,'H').ge.1) then
        HEAVYONLY=.true.
        USEDOPT=.true.
      end if
      if (index(OPTIONS,'D').ge.1) then
        USEDRMS=.true.
        USEDOPT=.true.
      end if
      if (index(OPTIONS,'S').ge.1) then
        write (STDERR,'(''rms: symmetry flag not fully tested, use at own risk !!!'')')
        USESYM=.true.
        SYMFILE='./sym.dat'
        USEDOPT=.true.
        inquire (FILE=SYMFILE, exist=FEXIST)
        if (.not. FEXIST) then
          write (STDERR,'(''rms: symmetry file does not exist '')')
          call exit(3)
        end if
      end if
      if (index(OPTIONS,'f').ge.1) then
        OUTCOR=.true.
        USEDOPT=.true.
      end if
C
C catch incompatible OPTIONS, and reset, drms is override
C
      if (USEDRMS) then
        if (OUTTRA) then
          write (STDERR,'(''rms: translation cannot be output for drms metric'')')
          OUTTRA=.false.
        end if
        if (OUTCOR) then
          write (STDERR,'(''rms: coordinates cannot be output for drms metric'')')
          OUTCOR=.false.
        end if
        if (OUTDIS) then
          write (STDERR,'(''rms: distances cannot be output for drms metric'')')
          OUTDIS=.false.
        end if
        if (USESYM) then
          write (STDERR,'(''rms: symmetry cannot be used for drms metric'')')
          call EXIT(3)
        end if
      end if
C
C this works, but very confusingly !
C
      call getarg(2,FILE1)
1     inquire (FILE=FILE1,EXIST=FEXIST)
      if (.not. FEXIST) then
        write (STDERR,'(''rms: first coordinate file does not exist '')')
        write (STDERR,'(''usage: rms [-dfhtvADHS] <FILE1> <FILE2>'')')
        call exit(1) 
      end if
C
      if (USEDOPT) then
        call getarg(3,FILE2)
      else
        call getarg(2,FILE2)
      end if
      inquire (file=FILE2,EXIST=FEXIST)
      if ( .not. FEXIST) then
        write (STDERR,'(''rms: second coordinate file does not exist '')')
        write (STDERR,'(''usage: rms [-dfhtvAH] <FILE1> <FILE2>'')')
        call exit(1)
      end if
C
C --------------------------------------------------------------------------
C
C     read symmetry data
C
      if (USESYM) then
        call RDSYMFILE(SYMFILE,NBLOCK)
      end if
C
C --------------------------------------------------------------------------
C
C read in first coord file
C
      open (unit=1,FILE=FILE1,STATUS='old',FORM='FORMATTED',ERR=234)
      NATS1=1
10    read (1,'(A)',END=100) TEXT
      if (TEXT(1:6).EQ.'ATOM  ' .or. TEXT(1:6) .eq. 'HETATM') then
      if (CAONLY) then
        if (TEXT(13:16) .ne. ' CA ') then
          goto 10
        end if
      end if
      if (HEAVYONLY) then
        if (TEXT(14:14) .eq. 'H') then
          goto 10
        end if
      end if
      read (TEXT,111) TEXT1(NATS1),(X1(I,NATS1),I=1,3)
      read (TEXT,263) RESTP1(NATS1),RESNM1(NATS1)
263     FORMAT (17X,A3,2X,A5)
111     FORMAT (A30,3F8.3)
        NATS1=NATS1+1
        if (NATS1 .gt. MAXATS) then
          write (STDERR,'(''rms: too many atoms)') 
          call EXIT(2)
        end if
      end if
      GOTO 10
100   NATS1=NATS1-1
      close (unit=1)
      if (NATS1 .lt. 3) then
        write (STDERR,'(''rms: less than three points in molecule 1 -- cannot fit'')')
        call exit(1)
      end if
C
C Data for X2
C
      open (unit=2,FILE=FILE2,STATUS='OLD',FORM='FORMATTED',ERR=234)
      NATS2=1
20    read (2,'(A)',END=200) TEXT
      if (TEXT(1:6) .eq. 'ATOM  ' .or. TEXT(1:6) .eq. 'HETATM') then
      if (CAONLY) then
        if (TEXT(13:16) .ne. ' CA ') then
          goto 20
        end if
      end if
      if (HEAVYONLY) then
        if (TEXT(14:14) .eq. 'H') then
          goto 20
        end if
      end if
      read (TEXT,111) TEXT2(NATS2),(X2(I,NATS2),I=1,3)
      read (TEXT,263) RESTP2(NATS2),RESNM2(NATS2)
        NATS2=NATS2+1
        if (NATS2 .gt. MAXATS) then
          write (STDERR,'(''rms: too many atoms)') 
          call EXIT(2)
        end if
      end if
      goto 20
200   NATS2=NATS2-1
      close (unit=2)
      if (NATS2 .lt. 3) then
        write (STDERR,'(''rms: less than three points in molecule 2 -- cannot fit'')')
        call exit(1)
      end if
C
C --------------------------------------------------------------------------
C
C Check for an unequal number of residues in the two files
C
      if (NATS1 .ne. NATS2) then
        write (STDERR,'(''rms: inconsistent no. of atoms '',I3,2X,I3)') NATS1,NATS2
        call exit(1)
      end if
C
C ----------------------------------------------------------------------------
C
C ****************************** CASE 1, no symmetry
C
      if (.not. USESYM) then
C
C used either rotation based, or distance based metrics
C
        if (USEDRMS) then
C
C only need to output rmsd from this
C
          D=0.0
          do I=1,NATS1
            do J=1,NATS1
              D = D + ((sqrt(DIST2(X1(1,I),X1(1,J))) - sqrt(DIST2(X2(1,I),X2(1,J))) ) **2)
            end do
          end do
C
          rmsd = sqrt(D / (NATS1**2))
C
        else
C
C Calculate COG of first set of coordinates
C Calculate COG of second set of coordinates
C Find the rotation matrix that maps second set of coords onto first
C Apply this transformation to the second set of coordinates
C Find RMS between two coordinate sets
C
          call ORIGIN(X1(1,1),TEMP,NATS2,1,C1,WT)
          call ORIGIN(X2(1,1),TEMP,NATS2,1,C2,WT)
          call MATFIT(X1(1,1),X2(1,1),ROT,NATS2,0,0.)
          call ROTFIT(X2,ROT,NATS2)
          call RMSFIT(X1,X2,RMSD,NATS2,0,WT)
C
          if (OUTCOR) then
            OPEN (unit=2,FILE=FILE2,STATUS='old',FORM='FORMATTED')
            do I=1,NATS2
              write (2,'(A30,3F8.3)') TEXT2(I),(X2(K,I),K=1,3)
            end do
            close (unit=2)
          end if
          if (OUTTRA) then
            do I=1,3
              TRANS(I)=C1(I)-C2(I)
            end do
            write (STDOUT,'(A,'' - '',A,'' translation  : '',3F8.3)')
     +             FILE1(1:lastchar(FILE1)),FILE2(1:lastchar(FILE2)),(TRANS(K),K=1,3)
            write (STDOUT,'(A,'' - '',A,'' rotation     : '',3F8.3)')
     +             FILE1(1:lastchar(FILE1)),FILE2(1:lastchar(FILE2)),(ROT(1,K),K=1,3) 
            do J=2,3
              write (STDOUT,'(47X,3F8.3)') (ROT(J,K),K=1,3)
            end do
          end if
          if (OUTDIS) then
            do I=1,NATS2
              call DISTAN(X1(1,I),X2(1,I),DIST(I))
              write (STDOUT,'(''--'',A,''-- '',F6.3)') TEXT2(I)(13:27), DIST(I)
            end do
          end if
        end if
C
C default report line
C
        write (STDOUT,'(A,'' - '',A,''      rmsd : '',F5.3,'' ('',I4,'')'')')
     +         FILE1(1:lastchar(FILE1)),FILE2(1:lastchar(FILE2)),RMSD,NATS2
C
C ****************************** CASE 2, symmetry
C
      else
C
        write (STDERR,'(''rms: symmetry not fully implemented'')')
C
C key difference is need to generate alternate conformers, stored in XALT()
C
        NCONF=2**NBLOCK
        if (NCONF .gt. MAXCONF) then
          write (STDERR,'(''rms: too many conformers'')')
        end if
        write (STDOUT,'(''  examined '',I4,'' conformers '')') NCONF
C
        RMSLOW=9.999
C
        do I=1,NCONF
C
C this is a real pig to code, will do it later.
C
C ==============================================================================
C
C generate XALT, X2 contains original coordinates at all times
C
          do J=1,NATS2
            do K=1,3
              XALT(K,J)=X2(K,J)
            end do
          end do
C
C repeat fitting for XALT
C
          call ORIGIN(X1(1,1),TEMP,NATS2,1,C1,WT)
          call ORIGIN(XALT(1,1),TEMP,NATS2,1,C2,WT)
          call MATFIT(X1(1,1),XALT(1,1),ROT,NATS2,0,0.)
          call ROTFIT(XALT,ROT,NATS2)
          call RMSFIT(X1,XALT,RMSD,NATS2,0,WT)
          if (RMSD .lt. RMSLOW) then
            RMSLOW=RMSD
          end if
        end do
C
        write (STDOUT,'(A,'' - '',A,''  (s) rmsd : '',F5.3,'' ('',I4,'')'')')
     +         FILE1(1:lastchar(FILE1)),FILE2(1:lastchar(FILE2)),RMSLOW,NATS2
C
C catch uncoded OPTIONS for symmetry case, will need to store index of best conformer
C above.
C
        if (OUTCOR) then
          write (STDERR,'(''rms: coordinate output not coded for symmetry'')')
        end if
        if (OUTTRA) then
          write (STDERR,'(''rms: transformation output not coded for symmetry'')')
        end if
        if (OUTDIS) then
          write (STDERR,'(''rms: atom distance output not coded for symmetry'')')
        end if
C
      end if
C
232   call exit(0)
234   call exit(1)
999   call exit(1)
C
      end
