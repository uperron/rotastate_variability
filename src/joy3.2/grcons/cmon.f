C
C copyright (c) 1998 John Overington, Pfizer Ltd
C part of joy
C
C ---------------------------------------------------------------------------
C
C Routine to read a pairwise alignment
C
C Reads first sequence and treats as template, reads second sequence as model
C and modifies second sequence.
C
C ----------------------------------------------------------------------------
C
       include '../joy/joy.h'
C
C ----------------------------------------------------------------------------
C
      character*500 TEXT
      character*80 CARD, BUFFER
      character*1 SEQ(MAXRES,MAXSEQ)
      integer NSEQ, LEN(MAXSEQ), NCHAR(MAXRES)
      integer SEQID, I, J, K, NENTRY, N, LENGTH
      logical FOUND
C
C ----------------------------------------------------------------------------
C
      data FOUND /.FALSE./
      NSEQ=0
C 
C ---------------------------------------------------------------------------
C
10    read (STDIN,'(A)',end=100,err=901) CARD
        if (CARD(1:5).eq.'     ' .and. FOUND) then
          goto 100
        else if (CARD(1:4) .eq. '>T1;') then
      goto 10
        else if (CARD(1:1) .eq. '#') then
      goto 10
        else if (CARD(1:2) .eq. 'C;') then
      goto 10
        else if (CARD(1:4).eq.'>P1;') then
          read (CARD(5:14),'(A)',err=901) BUFFER
          read (STDIN,'(A)',err=901) CARD
          if (CARD(1:8).eq.'sequence'.or.CARD(1:8).eq.'structur') then
            NSEQ=NSEQ+1
            if (NSEQ .gt. MAXSEQ) then
              call ERROR('cmon: too many sequences',' ',3)
            end if
            FOUND=.true.
            NENTRY=NENTRY+1
C
            N=1
12          read (STDIN,'(A)',err=901) CARD
            if (INDEX(CARD,'*').gt.0) then
              read (CARD(1:75),'(75A)')
     +              (SEQ(I,NSEQ),I=N,N+INDEX(CARD,'*')-2)
          goto 13
              else
                read (CARD(1:75),'(75A)') (SEQ(I,NSEQ),I=N,N+74)
              end if
              N=N+75
            goto 12
13          continue
            LENGTH=I-1
      goto 10
          else
            call ERROR('cmon: corrupted data (sequence) -',
     +                 CARD(1:20),1)
          end if
      goto 10
        else if (CARD(1:4).eq.'>N1;') then
      goto 10
        end if
      goto 10
100   continue
C
C ----------------------------------------------------------------------------
C
C process the first two sequences
C
      do I=1,LENGTH
        if (SEQ(I,1) .eq. SEQ(I,2)) then
          SEQ(I,2)=SEQ(I,2)
        else if (SEQ(I,1) .eq. '-' .or. SEQ(I,1) .eq. '/') then
          if (ISAA(SEQ(I,2))) then
            if (SEQ(I,2) .eq. 'G') then
              SEQ(I,2)='G'
            else
              SEQ(I,2)='A'
            end if
          else
            SEQ(I,2)=SEQ(I,2)
          end if
        else if (SEQ(I,1) .ne. SEQ(I,2)) then
          if (ISAA(SEQ(I,2))) then
            if (SEQ(I,2) .eq. 'G') then
              SEQ(I,2)='G'
            else
              SEQ(I,2)='A'
            end if
          else 
            SEQ(I,2)=SEQ(I,2)
          end if
        end if
      end do
C
      do I=1,LENGTH
        TEXT(I:I)=SEQ(I,2)
      end do
      TEXT(LENGTH+1:LENGTH+1)='*'
      write (STDOUT,'(A)') TEXT(1:LENGTH+1)
C
      call EXIT(0)
C
C ----------------------------------------------------------------------------
C
C Handle errors in opening file
C
999   write (STDERR,'(''cmon: unable to read input '',A)')
      call EXIT(1)
C
901   write (STDERR,'(''cmon: error reading input '',A)') 
      call EXIT(1)
C
      stop
      end
