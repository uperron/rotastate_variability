      program GRCONS
C
C copyright (c) 1988-1991 John Overington
C part of joy
C
C A program to colour a three-dimensional Brookhaven structure according
C to the sequence variation at each position
C
C Normally only the CAs are written out, but can do all atoms by setting -a
C option (all atoms in sidechain obviously have same colour value.
C
      include '../joy/joy.h'
C
      character*256 FILE, CONSFILE, ALFILE
      CHARACTER*5 RESNUM(MAXRES), HETNUM(MAXHET), OPTIONS
      CHARACTER*3 RESNAM(MAXRES), HETNAM(MAXHET), ATM(MAXATS,MAXRES)
      character*3 HETATM(MAXHET)
      CHARACTER*1 CHNNAM(MAXRES)
      INTEGER LENRES(MAXRES), NUMRES1,NUMRES2, NUMCHN, NUMATS, NUMHET
      integer NARGS, MAXREC, NUMREC, I, J, LM, M, K, IARGC
      real COORDS(3,MAXATS,MAXRES), OCCUP(MAXATS,MAXRES)
      real BVAL(MAXATS,MAXRES)
      real HETCOORDS(3,MAXHET), HETBVAL(MAXHET)
      real HETOCCUP(MAXHET), VAR(MAXRES)
      real MAX
      logical CHKATM, DOALLATS, DOGAP
      write (6,*) ' init'
C
      ALFILE='          '
      CHKATM=.false.
      DOALLATS=.false.
      DOGAP=.false.
      MAX=-100.0
C
      NARGS=IARGC()
      write (6,*) nargs,' nargs'
C
      if (NARGS.eq.1) then
        call GETARG(1,FILE)
      else if (NARGS.eq.2) then
        call GETARG(1,OPTIONS)
        if (OPTIONS(1:1).ne.'-') then
          call GETARG(1,FILE)
          call GETARG(2,ALFILE)
          goto 10
c          write (6,'(''grcons: bad option'')')
c          call EXIT(1)
        end if
        if (index(OPTIONS,'a').ge.1) then
          DOALLATS=.true.
        end if
        if (index(OPTIONS,'G').ge.1) then
          DOGAP=.true.
        end if
        call GETARG(2,FILE)
      else if (NARGS.eq.0) then
        write (6,'(''grcons: color a structure by residue variance'')')
        write (6,'('' '')')
        write (6,'(''      usage: grcons [-a] atmfile'')')
        write (6,'(''            -a for all atom output'')')
        write (6,'(''            -G for setting max value for insertions'')')
        write (6,'(''            atmfile has .atm suffix'')')
        write (6,'(''            alifile has .ali suffix'')')
        write (6,'(''            outfile has .con suffix'')')
        write (6,'('' '')')
      else
        write (6,'(''grcons: too many options'')')
      end if
10    continue
C
      if (index(FILE,'.atm').le.1) then
        FILE=FILE(1:LASTCHAR(FILE))//'.atm'
      end if
      if ( .not. FILEEXIST(FILE)) then
        write (6,'(''file not found: '',A)') FILE
        call exit(1)
      end if
      CONSFILE=FILE(1:INDEX(FILE,'.'))//'con'
C
      call RDPDB(FILE,ATM,RESNUM,RESNAM,CHNNAM,COORDS,OCCUP,BVAL,
     -           LENRES,NUMATS,NUMRES1,NUMCHN,NUMHET,HETCOORDS,
     -           HETNUM,HETNAM,HETATM,CHKATM)
C
      ALFILE=FILE(1:index(FILE,'.'))//'ali'
      call RDGRC(ALFILE,NUMRES2,VAR,DOGAP)
C
      if (NUMRES1.ne.NUMRES2) then
        write (6,'(''different number of residues'')') NUMRES1,NUMRES2
        call EXIT(0)
      end if
C
      if (DOALLATS) then
        do I=1,NUMRES1
          do J=1,LENRES(I)
            BVAL(J,I)=VAR(I)
          end do
        end do
        call WRPDB(CONSFILE,ATM,RESNUM,RESNAM,CHNNAM,COORDS,OCCUP,BVAL,
     -             LENRES,NUMATS,NUMRES1,NUMCHN,NUMHET,HETCOORDS,
     -             HETNUM,HETNAM,HETATM)
      else
C
C will screw up chains etc
C
        call WRCA(CONSFILE,NUMRES1,VAR,COORDS,RESNUM,RESNAM)
      end if
C
      call EXIT(0)
      end
