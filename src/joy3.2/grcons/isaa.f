      logical function ISAA(ACHAR)
C
C Returns if a character is the one letter code of an amino acid
C NB Have counted J as an amino acid (explicit free thiol cys)
C
      implicit none
      character*1 ACHAR
      character*21 RES
      integer I
C
      data RES /'GSAVLTDKINPEYQFRCHMWJ'/
C
      do I=1,LEN(RES)
        if (ACHAR.eq.RES(I:I)) then
          ISAA=.true.
          return
        end if
      end do
C
      ISAA=.false.
      return
C
      end
