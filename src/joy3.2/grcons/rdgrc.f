      subroutine RDGRC(FILE,LENGTH,ENT,DOGAP)
C
C copyright (c) 1988-1991 John Overington
C part of joy
C
C ---------------------------------------------------------------------------
C
C Routine to read in a multiple alignment, with optional numbers, labels, etc
C and return length etc of alignment etc.
C Default filename is .PAP, produced by COMPARER
C
C Reads first sequence and then only analyzes variance if not - in that sequence
C
C ----------------------------------------------------------------------------
C
       include '../joy/joy.h'
C
C ----------------------------------------------------------------------------
C
      character*(*) FILE
      character*500 TEXT
      character*80 CARD, BUFFER
      character*10 FILNAM(MAXSEQ)
      character*1 SEQ(MAXRES,MAXSEQ), RES(21)
      real POBS(21,MAXRES)
      real WT(MAXSEQ),RNOBS(22,MAXSEQ),ENT(MAXRES)
      integer NSEQ, LEN(MAXSEQ), LENFIL(MAXSEQ), NCHAR(MAXRES)
      integer NOBS(22,MAXRES)
      integer NITER,ISEQ(MAXRES,MAXSEQ), SEQID, I, J, K
      integer NENTRY, N, LENGTH
      logical FOUND, PROCED, WEIGHT, ISVERBOSE, DOGAP
C
C ----------------------------------------------------------------------------
C
      data RES /'A','C','D','E','F','G','H','I','K','L','M',
     -          'N','P','Q','R','S','T','V','W','Y','J'/
      data FOUND /.FALSE./
      data ISVERBOSE /.false./
      data NITER /2000/
      NSEQ=0
C 
C ----------------------------------------------------------------------------
C
      open (UNIT=1,FILE=FILE,STATUS='OLD',FORM='FORMATTED',ERR=999)
      REWIND (UNIT=1)
C
C ---------------------------------------------------------------------------
C
10    read (1,'(A)',end=100,err=901) CARD
        if (CARD(1:5).eq.'     '.and.FOUND) then
          goto 100
        else if (CARD(1:4).eq.'>T1;') then
      goto 10
        else if (CARD(1:1).eq.'#') then
      goto 10
        else if (CARD(1:4).eq.'>P1;') then
          read (CARD(5:14),'(A)',err=901) BUFFER
          read (1,'(A)',err=901) CARD
          if (CARD(1:8).eq.'sequence'.or.CARD(1:8).eq.'structur') then
            NSEQ=NSEQ+1
            if (NSEQ.gt.MAXSEQ) then
              call ERROR('rdpap: too many sequences',' ',3)
            end if
            FOUND=.true.
            NENTRY=NENTRY+1
C
            N=1
12          read (1,'(A)',err=901) CARD
            if (INDEX(CARD,'*').gt.0) then
              read (CARD(1:75),'(75A)')
     +              (SEQ(I,NSEQ),I=N,N+INDEX(CARD,'*')-2)
          goto 13
              else
                read (CARD(1:75),'(75A)') (SEQ(I,NSEQ),I=N,N+74)
              end if
              N=N+75
            goto 12
13          continue
            LENGTH=I-1
      goto 10
          else
            call ERROR('rdpap: corrupted data (sequence) -',
     +                 CARD(1:20),1)
          end if
      goto 10
        else if (CARD(1:4).eq.'>N1;') then
      goto 10
        end if
      goto 10
C
C ----------------------------------------------------------------------------
C
100   continue
C
      CLOSE (UNIT=1)
C
C NSEQ is the number of sequences read in from the file
C NUMENT is the number of rows that need to be passed to the formatted output
C
      if (ISVERBOSE) then
        write (6,'(''rdgrc: read in '',I4,'' sequences of length '',
     +         I4)')
     -          NSEQ,LENGTH
      end if
C
C process entire alignment, a position at a time
C
      N=0
      do I=1,LENGTH
        if (ISAA(SEQ(I,1))) then
          N=N+1
          ENT(N)=0.000
          do K=1,21
            POBS(K,I)=0.0
            NOBS(K,I)=0
          end do
          do J=1,NSEQ
            do K=1,21
              if (SEQ(I,J) .EQ. RES(K)) then
                NOBS(K,I)=NOBS(K,I)+1
              else
                NOBS(22,I)=NOBS(21,I)+1
              end if
            end do
          end do
          do K=1,21
            POBS(K,I)=real(NOBS(K,I))/real(NSEQ)
          end do
          if (.not. DOGAP) then
            do K=1,21
              if (POBS(K,I).ne.0.00) then
                ENT(N)=ENT(N)+(POBS(K,I)*log(POBS(K,I)))
              end if
            end do
            ENT(N)=-ENT(N)
          else
            write (6,*) ' gap is true ',NOBS(21,I)
            if (NOBS(22,I) .gt. 0) then
C
C value here is for flat distribution, i.e. no information at site
C
              write (6,*) ' found insertion ',N
              ENT(N)=2.9957
            else
              do K=1,21
                if (POBS(K,I).ne.0.00) then
                  ENT(N)=ENT(N)+(POBS(K,I)*log(POBS(K,I)))
                end if
              end do
              ENT(N)=-ENT(N)
            end if
          end if
         write (6,*) ' ent(n), n ',ent(n),n
        end if
      end do
      LENGTH=N
C
      return
C
C ----------------------------------------------------------------------------
C
C Handle errors in opening file
C
999   write (6,'(''rdgrc: unable to read file '',A)') FILE
      call EXIT(1)
C
901   write (6,'(''rdgrc: error reading file '',A)') FILE
      call EXIT(1)
C
      END
