      subroutine WRPDB(FILE,ATM,RESNUM,RESNAM,CHNNAM,COORDS,OCCUP,BVAL,
     +                 LENRES,NUMATS,NUMRES,NUMCHN,NUMHET,HETCOORDS,
     +                 HETNUM,HETNAM,HETATM)
C
C copyright (c) 1988-1991 John Overington
C part of joy
C
      include '../joy/joy.h'
C
      character*(*) FILE
      character*5 RESNUM(MAXRES), HETNUM(MAXHET)
      character*3 RESNAM(MAXRES), HETNAM(MAXHET), ATM(MAXATS,MAXRES)
      character*3 HETATM(MAXHET)
      character*1 CHNNAM(MAXRES)
      integer LENRES(MAXRES), NUMRES, NUMCHN, NUMATS, NUMHET
      integer I, J, K, L, N
      real COORDS(3,MAXATS,MAXRES), OCCUP(MAXATS,MAXRES)
      real HETOCCUP(MAXHET)
      real BVAL(MAXATS,MAXRES), HETCOORDS(3,MAXHET), HETBVAL(MAXHET)
C
      open (unit=1,file=FILE,status='UNKNOWN',form='FORMATTED',err=999)
C
      N=0
      do I=1,NUMRES
        do J=1,LENRES(I)
          N=N+1
          write (1,'(''ATOM  '',I5,2X,A3,1X,A3,1X,A1,A5,3X,3F8.3,2F6.2)',err=903)
     +          N, ATM(J,I),RESNAM(I),CHNNAM(I),RESNUM(I),
     +          (COORDS(K,J,I),K=1,3),OCCUP(J,I),BVAL(J,I)
        end do
      end do
      do L=1,NUMHET
        N=N+1
        write (1,'(''ATOM  '',I5,2X,A3,1X,A3,2X,A5,3X,3F8.3,2F6.2)',err=903)
     +        N, HETATM(L),HETNAM(L),HETNUM(L),(HETCOORDS(K,L),K=1,3),
     +        HETOCCUP(L),HETBVAL(L)
      end do
C
      close (unit=1)
C
      return
C
999   write (6,'(''wrpdb: error opening file '',A)') FILE
      call EXIT(1)
903   write (6,'(''wrpdb: error reading file '',A)') FILE
      call EXIT(1)
C
      end
C
C ==============================================================================
C
      subroutine WRCA(FILE,NRES,VAR,COORDS,RESNUM,RESNAM)
C
C copyright (c) 1988-1991 John Overington
C part of joy
C
C Assume that CA is in pos 2 of COORDS, no atom ID written out at moment
C
      include '../joy/joy.h'
      character*(*) FILE
      character*3 RESNAM(MAXRES)
      character*5 RESNUM(MAXRES)
      integer NRES, I, K
      real VAR(MAXRES)
      real COORDS(3,MAXATS,MAXRES)
      integer LENRES(MAXRES)
C
      open (file=FILE,unit=2,status='UNKNOWN',form='FORMATTED',err=1)
      do I=1,NRES
        write (2,'(''ATOM  '',I5,''  CA  '',A3,2X,A5,3X,3F8.3,
     +             ''  1.00'',F6.2)')
     +  I,RESNAM(I),RESNUM(I),(COORDS(K,2,I),K=1,3),VAR(I)*10.0
      end do
      close (unit=2)
C
      return
C
1     call ERROR('wrca: error opening file',FILE,1)
C
      end
