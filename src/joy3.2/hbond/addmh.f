      subroutine ADDMH(COORDS,RESNUM,RESNAM,ATM,LENRES,NRES,TERMINUS,
     +                 HATM)
C
C subroutine to add the mainchain hydrogens to a residue
C
C fixed problems with internal chain termini
C
C cis-peptides should be correctly treated, assume (CNCA) geom is the
C same
C
      include 'hbond.h'
C
      character*5 RESNUM(MAXRES)
      character*3 RESNAM(MAXRES), ATM(MAXATS,MAXRES)
      integer IERR, NRES, LENRES(MAXRES), I
      real COORDS(3,MAXATS,MAXRES)
      real A(3), B(3), C(3), HATM(3,MAXRES)
      logical TERMINUS(2,MAXRES)
C
      character*35 IDENT
      data IDENT /'@(#) addmh.f - (c) jpo 1990        '/
C
      do I=1,NRES
        if (.not. TERMINUS(1,I)) then
          call GETATM('C  ',ATM(1,I-1),LENRES(I-1),COORDS(1,1,I-1),A,
     +                RESNUM(I),RESNAM(I))
          call GETATM('N  ',ATM(1,I),  LENRES(I),  COORDS(1,1,I),  B,
     +                RESNUM(I),RESNAM(I))
          call GETATM('CA ',ATM(1,I),  LENRES(I),  COORDS(1,1,I),  C,
     +                RESNUM(I),RESNAM(I))
          call ADDNH(A,B,C,HATM(1,I))
        end if
      end do
C
      return
      end
C
C #############################################################################
C
      subroutine ADDSH(COORDS,RESNUM,RESNAM,ATM,LENRES,NRES,HATM)
C
C subroutine to add the mainchain hydrogens to a residue
C
      include     'hbond.h'
C
      character*5 RESNUM(MAXRES)
      character*3 ATM(MAXATS,MAXRES), RESNAM(MAXRES)
      integer I, NRES, LENRES(MAXRES)
      real COORDS(3,MAXATS,MAXRES)
      real A(3), B(3), C(3), HATM(3,MAXSIDE,MAXRES)
C
      character*35 IDENT
      data IDENT /'@(#) addsh.f - (c) jpo 1990        '/
C
      do I=1,NRES
        if (RESNAM(I).eq.'LEU'.or.RESNAM(I).eq.'ILE'.or.
     +      RESNAM(I).eq.'VAL'.or.RESNAM(I).eq.'GLY'.or.
     +      RESNAM(I).eq.'MET'.or.RESNAM(I).eq.'ALA'.or.
     +      RESNAM(I).eq.'PRO'.or.RESNAM(I).eq.'PHE') then
          continue
        else if (RESNAM(I) .eq. 'CYS' .or. RESNAM(I) .eq. 'CYH' .or.
     +           RESNAM(I) .eq. 'LYS' .or. RESNAM(I) .eq. 'SER' .or.
     +           RESNAM(I) .eq. 'THR' .or. RESNAM(I) .eq. 'TYR') then
          continue
        else if (RESNAM(I).eq.'TRP') then
c          write (6,*) '(trp) ', I
          call GETATM('CD1',ATM(1,I),LENRES(I),COORDS(1,1,I),A,
     +                RESNUM(I),RESNAM(I))
          call GETATM('NE1',ATM(1,I),LENRES(I),COORDS(1,1,I),B,
     +                RESNUM(I),RESNAM(I))
          call GETATM('CE2',ATM(1,I),LENRES(I),COORDS(1,1,I),C,
     +                RESNUM(I),RESNAM(I))
          call ADDNH(A,B,C,HATM(1,1,I))
        else if (RESNAM(I).eq.'HIS') then
c          write (6,*) '(his) ', I
          call GETATM('CG ',ATM(1,I),LENRES(I),COORDS(1,1,I),A,
     +                RESNUM(I),RESNAM(I))
          call GETATM('ND1',ATM(1,I),LENRES(I),COORDS(1,1,I),B,
     +                RESNUM(I),RESNAM(I))
          call GETATM('CE1',ATM(1,I),LENRES(I),COORDS(1,1,I),C,
     +                RESNUM(I),RESNAM(I))
          call ADDNH(A,B,C,HATM(1,1,I))
          call GETATM('CD2',ATM(1,I),LENRES(I),COORDS(1,1,I),A,
     +                RESNUM(I),RESNAM(I))
          call GETATM('NE2',ATM(1,I),LENRES(I),COORDS(1,1,I),B,
     +                RESNUM(I),RESNAM(I))
          call GETATM('CE1',ATM(1,I),LENRES(I),COORDS(1,1,I),C,
     +                RESNUM(I),RESNAM(I))
          call ADDNH(A,B,C,HATM(1,2,I))
        else if (RESNAM(I).eq.'ARG') then
c          write (6,*) '(arg) ', I
          call GETATM('CD ',ATM(1,I),LENRES(I),COORDS(1,1,I),A,
     +                RESNUM(I),RESNAM(I))
          call GETATM('NE ',ATM(1,I),LENRES(I),COORDS(1,1,I),B,
     +                RESNUM(I),RESNAM(I))
          call GETATM('CZ ',ATM(1,I),LENRES(I),COORDS(1,1,I),C,
     +                RESNUM(I),RESNAM(I))
          call ADDNH(A,B,C,HATM(1,1,I))
          call GETATM('NH1',ATM(1,I),LENRES(I),COORDS(1,1,I),A,
     +                RESNUM(I),RESNAM(I))
          call GETATM('CZ ',ATM(1,I),LENRES(I),COORDS(1,1,I),B,
     +                RESNUM(I),RESNAM(I))
          call GETATM('NE ',ATM(1,I),LENRES(I),COORDS(1,1,I),C,
     +                RESNUM(I),RESNAM(I))
          call ADDNH2(A,B,C,HATM(1,2,I),HATM(1,3,I))
          call GETATM('NH2',ATM(1,I),LENRES(I),COORDS(1,1,I),A,
     +                RESNUM(I),RESNAM(I))
          call GETATM('CZ ',ATM(1,I),LENRES(I),COORDS(1,1,I),B,
     +                RESNUM(I),RESNAM(I))
          call GETATM('NE ',ATM(1,I),LENRES(I),COORDS(1,1,I),C,
     +                RESNUM(I),RESNAM(I))
          call ADDNH2(A,B,C,HATM(1,4,I),HATM(1,5,I))
        else if (RESNAM(I).eq.'ASN') then
c          write (6,*) '(asn) ', I
          call GETATM('OD1',ATM(1,I),LENRES(I),COORDS(1,1,I),A,
     +                RESNUM(I),RESNAM(I))
          call GETATM('CG ',ATM(1,I),LENRES(I),COORDS(1,1,I),B,
     +                RESNUM(I),RESNAM(I))
          call GETATM('ND2',ATM(1,I),LENRES(I),COORDS(1,1,I),C,
     +                RESNUM(I),RESNAM(I))
          call ADDNH2(A,B,C,HATM(1,1,I),HATM(1,2,I))
          call GETATM('ND2',ATM(1,I),LENRES(I),COORDS(1,1,I),A,
     +                RESNUM(I),RESNAM(I))
          call GETATM('CG ',ATM(1,I),LENRES(I),COORDS(1,1,I),B,
     +                RESNUM(I),RESNAM(I))
          call GETATM('OD1',ATM(1,I),LENRES(I),COORDS(1,1,I),C,
     +                RESNUM(I),RESNAM(I))
          call ADDNH2(A,B,C,HATM(1,3,I),HATM(1,4,I))
        else if (RESNAM(I).eq.'ASP') then
c          write (6,*) '(asp) ', I
          call GETATM('OD1',ATM(1,I),LENRES(I),COORDS(1,1,I),A,
     +                RESNUM(I),RESNAM(I))
          call GETATM('CG ',ATM(1,I),LENRES(I),COORDS(1,1,I),B,
     +                RESNUM(I),RESNAM(I))
          call GETATM('OD2',ATM(1,I),LENRES(I),COORDS(1,1,I),C,
     +                RESNUM(I),RESNAM(I))
          call ADDNH2(A,B,C,HATM(1,1,I),HATM(1,2,I))
          call GETATM('OD2',ATM(1,I),LENRES(I),COORDS(1,1,I),A,
     +                RESNUM(I),RESNAM(I))
          call GETATM('CG ',ATM(1,I),LENRES(I),COORDS(1,1,I),B,
     +                RESNUM(I),RESNAM(I))
          call GETATM('OD1',ATM(1,I),LENRES(I),COORDS(1,1,I),C,
     +                RESNUM(I),RESNAM(I))
          call ADDNH2(A,B,C,HATM(1,3,I),HATM(1,4,I))
        else if (RESNAM(I).eq.'GLN') then
c          write (6,*) '(gln) ', I
          call GETATM('OE1',ATM(1,I),LENRES(I),COORDS(1,1,I),A,
     +                RESNUM(I),RESNAM(I))
          call GETATM('CD ',ATM(1,I),LENRES(I),COORDS(1,1,I),B,
     +                RESNUM(I),RESNAM(I))
          call GETATM('NE2',ATM(1,I),LENRES(I),COORDS(1,1,I),C,
     +                RESNUM(I),RESNAM(I))
          call ADDNH2(A,B,C,HATM(1,1,I),HATM(1,2,I))
          call GETATM('NE2',ATM(1,I),LENRES(I),COORDS(1,1,I),A,
     +                RESNUM(I),RESNAM(I))
          call GETATM('CD ',ATM(1,I),LENRES(I),COORDS(1,1,I),B,
     +                RESNUM(I),RESNAM(I))
          call GETATM('OE1',ATM(1,I),LENRES(I),COORDS(1,1,I),C,
     +                RESNUM(I),RESNAM(I))
          call ADDNH2(A,B,C,HATM(1,3,I),HATM(1,4,I))
        else if (RESNAM(I).eq.'GLU') then
c          write (6,*) '(glu) ', I
          call GETATM('OE1',ATM(1,I),LENRES(I),COORDS(1,1,I),A,
     +                RESNUM(I),RESNAM(I))
          call GETATM('CD ',ATM(1,I),LENRES(I),COORDS(1,1,I),B,
     +                RESNUM(I),RESNAM(I))
          call GETATM('OE2',ATM(1,I),LENRES(I),COORDS(1,1,I),C,
     +                RESNUM(I),RESNAM(I))
          call ADDNH2(A,B,C,HATM(1,1,I),HATM(1,2,I))
          call GETATM('OE2',ATM(1,I),LENRES(I),COORDS(1,1,I),A,
     +                RESNUM(I),RESNAM(I))
          call GETATM('CD ',ATM(1,I),LENRES(I),COORDS(1,1,I),B,
     +                RESNUM(I),RESNAM(I))
          call GETATM('OE1',ATM(1,I),LENRES(I),COORDS(1,1,I),C,
     +                RESNUM(I),RESNAM(I))
          call ADDNH2(A,B,C,HATM(1,3,I),HATM(1,4,I))
        else
          write (6,'(''hbond: bad sidechain (addsh) '',A,I4)') RESNAM(I),I
        end if
      end do
C
      return
      end
C
C ###########################################################################
C
      subroutine GETATM(ATMLBL,ATMNAM,LEN,COORDS,ATM,RESNUM,RESNAM)
C
C Finds an atom in a residue, returns flag for success and coordinates in ATM
C ifunsuccessful ATM is 999.99, 999.99, 999.99
C
      include 'hbond.h'
C
      character*5 RESNUM
      character*3 ATMLBL, ATMNAM(MAXATS), RESNAM
      integer LEN, I, J
      real COORDS(3,MAXATS), ATM(3)
C
      character*35 IDENT
      data IDENT /'@(#) getatm.f - (c) jpo 1990       '/
C
      do I=1,LEN
        if (ATMLBL.eq.ATMNAM(I)) then
          do J=1,3
            ATM(J)=COORDS(J,I)
          end do
          goto 1
        end if
      end do
C
      write (6,'(''hbond: missing atom -'',A3,
     +           ''- cannot add polar hydrogens for '',A3,A5)')
     +           ATMLBL,RESNAM,RESNUM
      ATM(1)=999.99
      ATM(2)=999.99
      ATM(3)=999.99
      return
C
1     continue
C
      return
      end
C
C #############################################################################
C
      subroutine ADDNH(A,B,C,D)
C
C Hydrogen is placed by a real peptide fragment being fitted to the C, N and
C CA atoms, data is taken from Ramachandran and Sasekharan, Adv. Prot. Chem
C The other cases should be treated with so little error that it doesn't
C matter about the precise atom types.
C Uncomment lines starting with c to get fitting statistics.
C
C For mainchain    N: A = C   B = N   C = CA
C For histidine  ND1: A = CG  B = ND1 C = CE1 
C For histidine  NE2: A = CD2 B = NE2 C = CE1 
C For tryptophan NE1: A = CD1 B = NE1 C = CE2
C for arginine    ND: A = CD  B = NE  C = CZ
C
      real A(3), B(3), C(3), D(3), FRG2(3,3), FRG(3,3)
      real ORIGFRG(3), ROTMAT(3,3), REF2(3,4), REF3(3,4)
C
      character*35 IDENT
      data IDENT /'@(#) addnh.f - (c) jpo 1990      '/
C
C Make sure this is centered at the origin (for the first three atoms)
C                  x       y       z
c      data REF / 0.578,  1.417,  0.000,   !   C
c     -          -0.335,  2.370,  0.000,   !   N
c     -           0.000,  3.801,  0.000,   !   CA
c     -          -1.317,  2.181,  0.000/   !   H
C
C Centered data is
C                   x       y       z
c      data REF3 / 0.497, -1.11233,  0.000,
c     -           -0.416, -0.15933,  0.000,
c     -           -0.081,  1.27167,  0.000,
c     -           -1.398, -0.34833,  0.000/
C
C New data taken from 5RSA (wiil replace this when ready)
C                   x       y       z
      data REF3 /-1.048, -0.425, -0.495,
     +            0.032,  0.320, -0.285,
     +            1.017,  0.105,  0.780,
     +            0.139,  1.134, -0.869/
C
      do I=1,3
        FRG(I,1)=A(I)
        FRG(I,2)=B(I)
        FRG(I,3)=C(I)
      end do
      do I=1,4
        do K=1,3
          REF2(K,I)=REF3(K,I)
        end do
      end do
      call ORIGIN(FRG,FRG2,3,3,ORIGFRG)
      call MATFIT(FRG,REF2,ROTMAT,3)
      call ROTATE(REF2,ROTMAT,4)
      do J=1,3
        D(J)=REF2(J,4)+ORIGFRG(J)
      end do
C
C#ifdef DEBUG
Cd      call RMSD(FRG,REF2,RMSVAL,3)
Cd      DIST=DIST2(B,D)
Cd      write (6,'(''HN dist = '',F8.6,'' rmsd = '',F8.6)') SQRT(DIST),RMSVAL
C#endif
C
      return
      end
C
C ##############################################################################
C
      subroutine ADDNH2(A,B,C,D,E)
C
C Addition of two hydrogen atoms to an atom (A)
C Hydrogens are added as in Baker and Hubbard (see also ADDNH)
C be careful how to treat asp, asn, gln, glu
C
C For aspargine  ND2: A = ND2 B = CB C = OD1 (and aspartic acid)
C     glutamine  NE2: A = NE2 B = CD C = OE1 (and glutamic acid)
C     arginine   NH1: A = NH1 B = CZ C = NE
C     arginine   NH2: A = NH2 B = CZ C = NE
C
      real A(3), B(3), C(3), D(3), E(3), FRG2(3,3), REF3(3,5)
      real FRG(3,3), REF(3,5), ORIGFRG(3), ROTMAT(3,3)
      real REF2(3,5), DISTA, DISTB
C
      character*35 IDENT
      data IDENT /'@(#) addnh2.f - (c) jpo 1990     '/
C
C Centered data is (taken from 5RSA, at random, need to take a better example 
C of the geometry)
C                   x       y       z
      data REF3 /-0.384, -0.643333,  0.926666,
     +           -0.218,  0.310667,  0.002666,
     +            0.602,  0.332667, -0.929333,
     +            0.284, -1.471333,  0.815666,
     +           -1.034, -0.642333,  1.674666/
C
      do I=1,3
        FRG(I,1)=A(I)
        FRG(I,2)=B(I)
        FRG(I,3)=C(I)
      end do
C
      do I=1,5
        do K=1,3
          REF2(K,I)=REF3(K,I)
          REF(K,I)=REF3(K,I)
        end do
      end do
      call ORIGIN(FRG,FRG2,3,3,ORIGFRG)
      call MATFIT(FRG,REF2,ROTMAT,3)
      call ROTATE(REF2,ROTMAT,5)
      do J=1,3
        D(J)=REF2(J,4)+ORIGFRG(J)
        E(J)=REF2(J,5)+ORIGFRG(J)
      end do
C
C#ifdef DEBUG
c      call RMSD(FRG,REF2,RMSVAL,3)
c      DISTA=DIST2(A,D)
c      DISTB=DIST2(A,E)
c       write (6,'(''HN dist 1 = '',F8.6,'' HN dist 2 = '',F8.6,
c     +            '' rmsd = '',F8.6)') SQRT(DISTA),SQRT(DISTB),RMSVAL
c      if (DISTA.gt.9.99) then
c        write (6,*) DISTA, DISTB, RMSVAL
c      end if
C#endif
C
      return
      end
