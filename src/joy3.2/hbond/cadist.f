      subroutine CADIST(RESNAM,RESNUM,ATM,LENRES,XYZ,NRES,POSSIBLE)
C 
C subotuine to precalculate a distance matrix for the Ca atoms in a protein
C and assign positions where an interaction is possible
C
      include 'hbond.h'
C
      character*5 RESNUM(MAXRES)
      character*3 ATM(MAXATS,MAXRES), RESNAM(MAXRES)
      real DIST(MAXRES,MAXRES), CA1(3), CA2(3), XYZ(3,MAXATS,MAXRES)
      real CUTOFF
      integer I, J, NRES, NPOSS, NIMPOSS, LENRES(MAXRES)
      logical POSSIBLE(MAXRES,MAXRES)
C
      NPOSS=0
      NIMPOSS=0
      CUTOFF=625.0		! 25 ** 2
C
      do I=1,NRES
        POSSIBLE(I,I)=.false.
        DIST(I,I)=0.0
        call GETATM('CA ',ATM(1,I),LENRES(I),XYZ(1,1,I),CA1,
     +              RESNUM(I),RESNAM(I))
        do J=I+1,NRES
          call GETATM('CA ',ATM(1,J),LENRES(J),XYZ(1,1,J),CA2,
     +              RESNUM(J),RESNAM(J))
          DIST(I,J)=DIST2(CA1,CA2)
C
C not used here but beware !
C
C          DIST(J,I)=DIST(I,J)
          if (DIST(I,J) .le. CUTOFF) then
            NPOSS=NPOSS+1
            POSSIBLE(I,J)=.true.
            POSSIBLE(J,I)=.true.
          else
            NIMPOSS=NIMPOSS+1 
            POSSIBLE(I,J)=.false.
            POSSIBLE(J,I)=.false.
          end if
        end do
      end do
C
C generate statistics on the ratio of interacting to non-interacting pairs
C
C#ifdef DEBUG
c     write (6,'(''saving percentage = '',F6.2,'' for '',I4)')
c    +       (real(NIMPOSS)/real(NIMPOSS+NPOSS))*100.0, NRES
C#endif
C
      return
      end
