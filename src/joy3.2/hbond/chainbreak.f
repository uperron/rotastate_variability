      subroutine CHAINBREAK(ATMNAM,COORDS,LENRES,NRES,ENDCHN,NCHN)
C
C Checks a set of CA coordinates for chain breaks, returns if a residue
C is at a terminus by assigning the logical ENDCHN
C
      include 'hbond.h'
C
      character*3 ATMNAM(MAXATS,MAXRES)
      integer LENRES(MAXRES), NRES, I, J, K, NCHN
      real COORDS(3,MAXATS,MAXRES), CA(3,MAXRES), LIMIT
      logical ENDCHN(2,MAXRES)
C
C TEMINUS contains the chain end information, for convenience a 2-d array,
C index 1 refers to the NH2 terminal end, whilst index 2 refers to a COO2 end
C
C LIMIT is the square of the inter CA distance denoting a chain break,
C currently set at 5 A
C
      LIMIT=25.0
C
      do I=1,NRES
        do J=1,LENRES(I)
          if (ATMNAM(J,I) .eq. 'CA ') then
            do K=1,3
              CA(K,I)=COORDS(K,J,I)
            end do
            goto 1
          end if
        end do
        write (6,'(''hbond: cannot find CA for residue '',I4)') I
1       continue
        ENDCHN(1,I)=.false.
        ENDCHN(2,I)=.false.
      end do
C
C
C Nasty bug here, would assign as true and then on next cycle reassign
C ENDCHN(1,I) as false. fixed by moving init step
C
      NCHN=1
      do I=1,NRES-1
        if (DIST2(CA(1,I),CA(1,I+1)) .gt. LIMIT) then
          NCHN=NCHN+1
          ENDCHN(2,I)=.true.
          ENDCHN(1,I+1)=.true.
        end if
        if (DIST2(CA(1,I),CA(1,I+1)) .lt. 0.1) then
          write (6,'(''hbond: adjacent CAs too close for '',
     -           I4,'' and '',I4)') I,I+1
        end if
      end do
      ENDCHN(1,1)=.true.
      ENDCHN(2,1)=.false.
      ENDCHN(1,NRES)=.false.
      ENDCHN(2,NRES)=.true.
C
      return
      end
