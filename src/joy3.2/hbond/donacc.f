      subroutine DONACC(RES,ATM,LENRES,NRES,ACCEPTOR,DONOR)
C
C Assigns whether an atom is a donor or an acceptor
C
      include 'hbond.h'
C
      character*3 ATM(MAXATS,MAXRES), RES(MAXRES)
      integer NRES, I, J, LENRES(MAXRES)
      logical ACCEPTOR(MAXATS,MAXRES), DONOR(MAXATS,MAXRES)
      logical ISACCEPTOR, ISDONOR
C
      do I=1,NRES
        do J=1,LENRES(I)
          DONOR(J,I)=.false.
          ACCEPTOR(J,I)=.false.
          if (ISDONOR(ATM(J,I),RES(I))) then
            DONOR(J,I)=.true.
          end if
          if (ISACCEPTOR(ATM(J,I),RES(I))) then
            ACCEPTOR(J,I)=.true.
          end if
        end do
      end do
C
      return
      end
C
C ##############################################################################
C
      logical FUNCTION ISACCEPTOR(ATOM,RES)
C
C logical function that returns TRUE if an atom is capable of acting as a
C hydrogen bond acceptor. i.e. has a lone pair 
C
      character*(*) RES
      character*3 ATOM, ACCATM(18)
      integer     NACC,I
C
      data NACC /18/
      data ACCATM /'O  ','OG ','OG1','SG ','ND1','ND2','OD1','OD2',
     -             'OE1','OE2','NE2','OH ','AD1','AD2','AE1','AE2',
     -             'OXT','SD '/
C
      do I=1,NACC
        if (ATOM.EQ.ACCATM(I)) then
          ISACCEPTOR=.true.
          return
        end if
      end do
C
      ISACCEPTOR=.false.
      return
C
      end
C
C #############################################################################
C
      logical FUNCTION ISDONOR(ATOM,RES)
C
C logical function that returns TRUE if an atom is capable of acting as a
C hydrogen bond donor. i.e. has a polar hydrogen.
C
C NOTE ambiguity of length of RES, will fix one day
C
C Proline cannot be a donor
C
      character*(*) RES
      character*3 ATOM, DONATM(21)
      integer NDONOR,I
C
      data NDONOR /21/
      data DONATM /'N  ','OG ','OG1','OH ','NZ ','NE ','NH1','NH2',
     -             'NE1','ND1','NE2','SG ','AD1','AD2','AE1','AE2',
     -             'OD1','OD2','OE1','OE2','ND2'/
C
      if (RES.eq.'PRO') then
        ISDONOR=.false.
        return
      end if
C
      do I=1,NDONOR
        IF (ATOM.EQ.DONATM(I)) THEN
          ISDONOR=.TRUE.
          return
        end if
      end do
C
      ISDONOR=.FALSE.
      return
C
      end
