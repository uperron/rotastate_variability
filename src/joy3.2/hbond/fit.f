      subroutine ROTATE(X,R,N)
C
C  Rotates contents of X using rotation matrix R.
C
      integer I, N
      real X(3,N), R(3,3), X1, X2, X3
C
      do I=1,N
        X1=R(1,1)*X(1,I)+R(1,2)*X(2,I)+R(1,3)*X(3,I)
        X2=R(2,1)*X(1,I)+R(2,2)*X(2,I)+R(2,3)*X(3,I)
        X3=R(3,1)*X(1,I)+R(3,2)*X(2,I)+R(3,3)*X(3,I)
        X(1,I)=X1
        X(2,I)=X2
        X(3,I)=X3
      end do
C
      return
      end
C
C ----------------------------------------------------------------------------
C
      subroutine ORIGIN(X1,X2,N,NC,C)
C
C calculates the center of origin of the data set X1, then
C
      integer N, NC, I, J
      real X1(3,N), X2(3,NC), C(3)
C
C Calculate the center of mass of X1
C
      do I=1,3
        C(I)=0.
      end do
      do I=1,N
        do J=1,3
          C(J)=C(J)+X1(J,I)
        end do
      end do
      do I=1,3
        C(I)=C(I)/real(N)
      end do
C
C Translate X1 to the origin
C
      do I=1,N
        do J=1,3
          X1(J,I)=X1(J,I)-C(J)
        end do
      end do
C
C Translate X2 by the same vector
C
      do I=1,NC
        do J=1,3
          X2(J,I)=X2(J,I)-C(J)
        end do
      end do
C
      return
      end
C
C ----------------------------------------------------------------------------
C
      subroutine MATFIT(X1,X2,RM,N)
C
      integer N, I, J, K
      real UMAT(3,3), RM(3,3), X1(3,4), X2(3,4)
C
      do I=1,3
        do J=1,3
           UMAT(I,J)=0.0
        end do
        do J=1,N
          do K=1,3
            UMAT(I,K)=UMAT(I,K)+X1(I,J)*X2(K,J)
          end do
        end do
      end do
C
C Fit it
C
      call QIKFIT(UMAT,RM)
C
      return
      end
C
C ----------------------------------------------------------------------------
C
      subroutine QIKFIT(UMAT,RM)
C
      integer JMAX, NCYC, NSTEEP, NCC1, NREM, I, J, K, L, M
      real RSUM, UMAT(3,3), ROT(3,3), COUP(3), DIR(3), STEP(3)
      real TURMAT(3,3), C(3,3), V(3), RM(3,3), SMALL, SMALSN
      real RTSUM, DELTA, CLEP, CLE, GFAC, RTSUMP, DELTAP, STP
      real STCOUP, UD, TR, TA, CS, SN, AC
C
      data SMALL  /1.0E-20/
      data SMALSN /1.0E-10/
C
      do L=1,3
        do M=1,3
          ROT(L,M)=0.0
        end do
        ROT(L,L)=1.0
      end do
C
      JMAX=30
      RTSUM=UMAT(1,1)+UMAT(2,2)+UMAT(3,3)
      DELTA=0.0
C
      do NCYC=1,JMAX
C
C modified conjugate gradient for first and every NSTEEP cycles set previous
C step as zero and take steepest descent path
C
        NSTEEP=3
        NCC1=NCYC-1
        NREM=NCC1-(NCC1/NSTEEP)*NSTEEP
        IF (NREM .ne. 0) goto 1
        do I=1,3
          STEP(I)=0.0
        end do
        CLEP=1.0
1       continue
C
C couple
C
        COUP(1)=UMAT(2,3)-UMAT(3,2)
        COUP(2)=UMAT(3,1)-UMAT(1,3)
        COUP(3)=UMAT(1,2)-UMAT(2,1)
        CLE=SQRT(COUP(1)**2+COUP(2)**2+COUP(3)**2)
C
C gradient vector is now -COUP
C
        GFAC=(CLE/CLEP)**2
C
C VALUE OF RTSUM FROM PREVIOUS STEP
C
        RTSUMP=RTSUM
        DELTAP=DELTA
        CLEP=CLE
        IF(CLE.LT.SMALL) GOTO 2
C
C STEP VECTOR CONJUGATE TO PREVIOUS
C
        STP=0.0
        do I=1,3
          STEP(I)=COUP(I)+STEP(I)*GFAC
          STP=STP+STEP(I)**2
        end do
        STP=1.0/SQRT(STP)
C
C NORMALISED STEP
C
        do I=1,3
          DIR(I)=STP*STEP(I)
        end do
C
C COUPLE RESOLVED ALONG STEP DIRECTION
C
        STCOUP=COUP(1)*DIR(1)+COUP(2)*DIR(2)+COUP(3)*DIR(3)
C
C COMPONENT OF UMAT ALONG DIRECTION
C
        UD=0.0
        do L=1,3
          do M=1,3
            UD=UD+UMAT(L,M)*DIR(L)*DIR(M)
          end do
        end do
        TR=UMAT(1,1)+UMAT(2,2)+UMAT(3,3)-UD
        TA=SQRT(TR**2+STCOUP**2)
        CS=TR/TA
        SN=STCOUP/TA
C
C IF(CS.LT.0) THEN PRESENT POSITION IS UNSTABLE SO do NOT STOP
C
        IF ((CS.gt.0.0).and.(ABS(SN).lt.SMALSN)) goto 2
  150   CONTINUE
C
C TURN MATRIX FOR CORRECTING ROTATION
C
C SYMMETRIC PART
C
        AC=1.0-CS
        do L=1,3
          V(L)=AC*DIR(L)
          do M=1,3
            TURMAT(L,M)=V(L)*DIR(M)
          end do
        TURMAT(L,L)=TURMAT(L,L)+CS
        V(L)=DIR(L)*SN
        end do
C
C ASSYMETRIC PART
C
        TURMAT(1,2)=TURMAT(1,2)-V(3)
        TURMAT(2,3)=TURMAT(2,3)-V(1)
        TURMAT(3,1)=TURMAT(3,1)-V(2)
        TURMAT(2,1)=TURMAT(2,1)+V(3)
        TURMAT(3,2)=TURMAT(3,2)+V(1)
        TURMAT(1,3)=TURMAT(1,3)+V(2)
C
C UPDATE TOTAL ROTATION MATRIX
C
        do L=1,3
          do M=1,3
            C(L,M)=0.0
            do K=1,3
              C(L,M)=C(L,M)+TURMAT(L,K)*ROT(K,M)
            end do
          end do
        end do
        do L=1,3
          do M=1,3
            ROT(L,M)=C(L,M)
          end do
        end do
C
C UPDATE UMAT TENSOR
C
        do L=1,3
          do M=1,3
            C(L,M)=0.0
            do K=1,3
              C(L,M)=C(L,M)+TURMAT(L,K)*UMAT(K,M)
            end do
          end do
        end do
        do L=1,3
          do M=1,3
            UMAT(L,M)=C(L,M)
          end do
        end do
        RTSUM=UMAT(1,1)+UMAT(2,2)+UMAT(3,3)
        DELTA=RTSUM-RTSUMP
C
C IF NO IMPROVEMENT IN THIS CYCLE THEN STOP
C
        IF (ABS(DELTA).LT.SMALL) GOTO 2
C
C NEXT CYCLE
C
      end do
2     continue
      RSUM=RTSUM
C
C COPY ROTATION MATRIX FOR OUTPUT
C
      do I=1,3
        do J=1,3
          RM(J,I)=ROT(I,J)
        end do
      end do
C
      return
      end
