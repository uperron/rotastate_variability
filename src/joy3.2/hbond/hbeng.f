      real function HBENG(C,O,H,N)
C
C calculates the energy of a hydrogen bond, according to the method of Kabsch
C and Sander, Biopolymers, 22, 2577 (1983)
C
C E = q1 * q2 (1/r(ON) + 1/r(CH) + 1/r(OH) - 1/r(CN)) * f
C
C for
C             
C
C                             H-------N
C             C=====O
C
C and more generally for any dipole-dipole interaction
C
      real Q1Q2, F, O(3), H(3), N(3), C(3), DIST2
C
      data Q1Q2	/ 0.084 /
      data F	/ 332.0 /
C
      HBENG = Q1Q2 * ( (1.0/SQRT(DIST2(O,N))) + (1.0/SQRT(DIST2(C,H)))
     +        - (1.0/SQRT(DIST2(O,H))) - (1.0/SQRT(DIST2(C,N)))  ) * F
C
      return
      end
