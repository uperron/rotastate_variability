      program HBOND 
C
C To take a standard PDB format data file, i.e. no funny residues, no
C missing residues etc, and calculate all possible H-bonds in the structure.
C No angular criteria are applied to the data, thus the program is not in
C any way a substitute for SSTRUC or DSSP for mainchain hydrogen bonds. It
C is meant to be used primarily for sidechain hydrogen bonds.
C
C Along with the atoms involved in the hydrogen bond atoms and length details
C there is a classification of the type of hydrogen bond. This is:
C
C MM - mainchain to mainchain
C MH - mainchain to hetatom
C MW - mainchain to water
C SO - sidechain to mainchain carbonyl
C SN - sidechain to mainchain amide
C SS - sidechain to sidechain
C SH - sidechain to hetatom
C SW - sidechain to water
C DS - a disulphide bond 
C
C It is important to understand the assumptions involved in the program.
C
C 1) A hydrogen bond can only occur between an acceptor and a donor atom
C 2) No hydrogens are used in any calculations
C 3) A Single cutoff of 3.5 A is used for all hydrogen bond distances
C    (This is definable by the user through the option d)
C 4) The amide of a proline cannot be involved in a hydrogen bond
C 5) The residues that can act as donors are :
C    CYS ASP GLU HIS LYS ASN GLN ARG SER THR TRP TYR
C    (atom names in function donor)
C 6) The residues that can act as acceptors are :
C    CYS ASP GLU HIS MET ASN GLN SER THR TYR
C    (atom names in function acceptor)
C 7) The appearance of CYS and MET in the above lists may surprise you, if
C    it does then read the literature.
C 8) The program is intended to be used mainly as a producer of datafiles
C    for joy.
C 9) You can ignore all h-bonding properties of CYS and MET. (option s)
C 10) Only polar sidechain and non-carbon hetatoms are allowed to form H-bonds.
C 11) The E atom of HETATM records is converted to FE, to aid interpretation.
C 12) An attempt is made to assign true donor/acceptor paris for sidechain
C     sidechain interactions, (where both atoms can be donors or acceptors)
C     There are still problems assigning the real donor in ASN/GLN pairs. This
C     is actually done in PRHB.
C
C VERSION HISTORY
C
C 0.1 - Preliminary version, changed format of INDEX string.
C 0.2 - Fixed bug in length of last residue, added ND2 to donor list
C 0.3 - Added option to include hetatoms in calculations, added include
C       file for parameters. Modified READPDB to correctly handle multiple
C       occupancies, and added more error checking. Increased cutoff to 25 A
C       and moved to parameter. Added treatment of HETATM h-bonds, added two
C       new classifications for H-bonds. Removed CUTOFF criterion, for most
C       files the difference in speed is negligable and it is better to be
C       safe than sorry.
C 0.4 - Added CYS bonding to carbon as a type of bond. Changed distance check
C       to the square of the distance. Fixed new bugs due to square of distance
C       also added SSDIST2, as a parameter for the maximum length of the square
C       of a disulphide bond. Fixed bug in declaration of ISSIDE logical 
C       function. Fixed bug in square of distance for hetatoms.
C 0.5 - Split subroutines up to use make. Removed ignores option. Changed 
C       organization of columns. First column is donor atom the second column 
C       is the acceptor atom. Should make geometry etc. easier to handle and
C       also clean up the loop structure of the program. fixed new bug in ACCEPT
C       tested against old version of hbond. Only difference is that some bonds
C       will be counted twice. (Does not matter to joy, but still should not be
C       there, only seems to affect bonds where both atoms can both acceptors
C       and donors).
C 0.6 - Compressed format of file. Added output of angle added for
C       main chain H-bonds. Requires redo of rdhbd in joy. Undefined
C       angles are assigned as 999.99. Should check for these on reading.
C       Added angle for SMO H-bonds. NOTE: No cutoff is applied on the
C       basis of angle, it is simply reported.
C 0.7 - Changed format of .hbd file, now about half the size of the old version
C       Output sequence information in single letter code. Made corresponding
C       changes in joy. Output all possible known hydrogen positions, added
C       proper handling of chain breaks. angles, etc, look ok but must be
C       checked on graphics. known limitations are that in the angle calculation
C       the alternatives for N/Q (i.e. the AD1, AD2 etc) labelling is not
C       explored, similarly the assignment of atom types in a histidine
C       sidechain are not examined. Optimizer bug fo angle found, need to
C       use opt 2 unless get INVALID_ASIN_ACOS sometimes. Added ability to
C       handle more than one file at a time, N.B. must have command line
C       format: hbond -options [dis] file1 file2 ...
C 0.8   Added new addh1 and added energy field to output file, (not functional
C       yet) but it will be soon. Fixed bugs in CA position for MM and SMN class
C       of hydrogen bonding. Added routines getgeom1 and getgeom2, simplified
C       code, fixed bugs in calculation of ANGLCO for several cases.
C 0.9   Changed confusing addition of Hydrogens
C 1.0   Reordered code, should now be a lot faster (it is, about three times as
C       fast). Added more checking of input, energies and distances all look
C       ok when examined with SYBYL. Still probably require more checking of
C       missing atoms. Are defined at 999.99*3 in GETATM, could check distance
C       in PRHB then set to 9.99 to avoid errors reading. Added proper checking
C       of termini for mainchain H-bonds. Increased distance range for sulphur
C       containing sidechains to 4.0A based upon data in Gregoret et al, 1991.
C       Removed test of CYS/H on hetatom interactions. Tidied code, removed
C       -d option. Removed unused variables. Now can optimize and runs fine
C       (under LPI at least), decreased number of atoms per residue to 14,
C       removed any checking that was previously performed, so now will not
C       run correctly on funny files, use pdb2atm, or jank first. Added
C       any (upto MAXARGS) treatment of files.
C 1.1   Started code for sidechain correction (amides and histidines), checking
C       for anti-hydrogen bonds.
C
C ================================================================================
C
      include 'hbond.h'
C
      integer MAXARGS
      parameter (MAXARGS=256)
      character*(MAXFILELEN) INFILE, OUTFILE, FILES(MAXARGS)
      character*14 OPTIONS
      character*5 RESNUM(MAXRES), HETNUM(MAXHET)
      character*4 VERSION, HETATM(MAXHET)
      character*3 RESNAM(MAXRES), ATM(MAXATS,MAXRES), HETNAM(MAXHET)
      character*2 HBTYPE
      character*1 CHNNAM(MAXRES), SEQ(MAXRES), FLAG, ALT
      integer LENRES(MAXRES), NRES, NATS, NHET, IARGC, IOUT
      integer I, J, K, L, ISPAN, NCHN, IPREV, NCH, NFILES, III
      integer NIOATM
      real COORDS(3,MAXATS,MAXRES), OCCUP(MAXATS,MAXRES)
      real BVAL(MAXATS,MAXRES)
      real HETCOORDS(3,MAXHET), DIST, ANGLCO, ANGLNH, HDIST, ENERGY
      real HATM(3,MAXRES), CATM(3), SHATM(3,MAXSIDE,MAXRES), HBENG
      logical DONOR(MAXATS,MAXRES), ACCEPTOR(MAXATS,MAXRES)
      logical DOHET, DOWAT, VERBOSE, TERMINUS(2,MAXRES)
      logical POSSIBLE(MAXRES,MAXRES), ANTI, IOCOORDS
C
      character*31 IDENT
      data IDENT /'@(#) hbond.f - joy (c) jpo 1991'/
C
      include 'date.h'
C
      data IOUT /2/		! unit number of output file
      data DOHET    /.true./
      data DOWAT    /.true./
      data VERBOSE  /.false./
      data ANTI     /.false./
      data IOCOORDS /.false./
      data NIOATM   /0/
C
C -----------------------------------------------------------------------
C
C Get command line options
C
C Check no of arguments
C
      if (IARGC() .gt. MAXARGS) then
        call ERROR('hbond: argument list too long',' ',1)
      end if
C
C get first argument to see if it is an option, if it is then get all
C filenames
C If first argument not an option then get all filenames
C
      call GETARG(1,OPTIONS)
      if (OPTIONS(1:1) .eq. '-') then
        if (index(OPTIONS,'V') .gt. 0) then
          write (STDERR,'(''hbond - version '',A,'' ('',A,
     -           '') - copyright 1990 jpo, compiled - '',A)')
     -           VERSION(),LIBVERSION(),DATE
          if (IARGC() .lt. 2) then
            call EXIT(0)
          end if
        end if
        if (index(OPTIONS,'C') .gt. 0) then
          IOCOORDS=.true.
          open (12,file='./hbond.out',status='unknown',
     -          form='formatted')
        end if
        if (index(OPTIONS,'v') .gt. 0) then
          VERBOSE=.true.
        end if
        if (index(OPTIONS,'A') .gt. 0) then
          ANTI=.true.
        end if
        if (index(OPTIONS,'h') .gt. 0) then
          call HELP()
          call EXIT(2)
        end if
        NFILES=0
        do I=2,IARGC()
          NFILES=NFILES+1
          call GETARG(I,FILES(NFILES))
        end do
        NFILES=IARGC()-1
      else
        do I=1,IARGC()
          call GETARG(I,FILES(I))
        end do
        NFILES=IARGC()
      end if
C
C -----------------------------------------------------------------------
C
C loop around all files in argument list
C
C control of files
C
      do III=1,NFILES
      INFILE=FILES(III)
C
      if (VERBOSE) then
        call ERROR('hbond: processing file',INFILE,0)
      end if
C
C if file does not have an extension, generate one with .atm extension
C
      if (index(INFILE,'.') .lt. 1) then
        if (FILEEXIST(INFILE(1:LASTCHAR(INFILE))//'.atm')) then
          INFILE=INFILE(1:LASTCHAR(INFILE))//'.atm'
        end if
      end if
      if (.not. FILEEXIST(INFILE)) then
        call ERROR('hbond: cannot find file',INFILE,0)
      end if
C
C output file has same root as input file, and a hbd extension
C
      OUTFILE=INFILE(1:index(INFILE,'.'))//'hbd'
C
C ------------------------------------------------------------------------
C
C Read in the PDB format data
C
      call RDPDB(INFILE,ATM,RESNUM,RESNAM,CHNNAM,COORDS,OCCUP,BVAL,
     -           LENRES,NATS,NRES,NHET,HETCOORDS,
     -           HETNUM,HETNAM,HETATM,VERBOSE)
C
C find chain termini
C
      call CHAINBREAK(ATM,COORDS,LENRES,NRES,TERMINUS,NCHN)
C
C convert three letter code to one letter code
C
      call THRONE(RESNAM,SEQ,NRES)
C
C Add hydrogen atoms to mainchain N's
C
      call ADDMH(COORDS,RESNUM,RESNAM,ATM,LENRES,NRES,TERMINUS,HATM)
C
C Add hydrogen atoms to sidechains
C
      call ADDSH(COORDS,RESNUM,RESNAM,ATM,LENRES,NRES,SHATM)
C
C Assign donor/acceptor states for every atom
C
      call DONACC(RESNAM,ATM,LENRES,NRES,ACCEPTOR,DONOR)
C
C Flag pairs for which interactions are possible, but only if size of protein
C is above a certain size, otherwise savings are outweighed.
C
      if (NRES .gt. NSIZE) then
        call CADIST(RESNAM,RESNUM,ATM,LENRES,COORDS,NRES,POSSIBLE)
      else
        do I=1,NRES
          POSSIBLE(I,I)=.false.
          do J=I+1,NRES
            POSSIBLE(J,I)=.true.
            POSSIBLE(I,J)=.true.
          end do
        end do
      end if
C
C exclude adjacent pairs (i-1, and i+1)
C include interactions across chain breaks (TERMINUS)
C
      do I=1,NRES-1
        POSSIBLE(I,I+1)=.false.
        POSSIBLE(I+1,I)=.false.
        if (TERMINUS(2,I)) then
          POSSIBLE(I,I+1)=.true.
          POSSIBLE(I+1,I)=.true.
        end if
      end do
C
C ------------------------------------------------------------------------
C
      open (file=OUTFILE,unit=IOUT,status='UNKNOWN',form='FORMATTED',
     +      err=5)
C
      write (IOUT,'(''# produced by hbond, version '',A,'' ('',A,
     +       '')'')') VERSION(),LIBVERSION()
      write (IOUT,'(''#   parameterized for '',I4,'' residues, '',I2
     -          ,'' atoms per residue, and '',
     -           I4,'' hetatoms'')') MAXRES, MAXATS, MAXHET
      write (IOUT,'(''#'')')
      write (IOUT,'(''# coordinate data taken from file '',A)')
     -           INFILE(1:LASTCHAR(INFILE))
      write (IOUT,'(''#   number of atoms    = '',I5)') NATS
      write (IOUT,'(''#   number of residues = '',I5)') NRES
      write (IOUT,'(''#   number of chains   = '',I5)') NCHN
      if (NCHN .gt. 1) then
        NCH=1
        IPREV=1
        do I=2,NRES
          if (TERMINUS(1,I)) then
            write (IOUT,'(''#'',24X,''chain '',I2,'' extent from '',I4,
     -                 '' to '',I4,'', of length'',I4)')
     -             NCH, IPREV, I-1, I-IPREV+1
            NCH=NCH+1
            IPREV=I
          end if
        end do
        write (IOUT,'(''#'',24X,''chain '',I2,'' extent from '',I4,
     -             '' to '',I4,'', of length'',I4)')
     -         NCH, IPREV, NRES, NRES-IPREV
        if (NCH .ne. NCHN) then
          call ERROR('hbond: inconsistency error, chain lengths',
     +                ' ',0)
        end if
      end if
      write (IOUT,'(''#   number of hetatoms = '',I5)') NHET
      write (IOUT,'(''#'')')
      write (IOUT,'(''# criteria for hydrogen bond definition :-'')')
      write (IOUT,'(''#  donor-acceptor distance cutoff '',
     +              ''(oxygen and nitrogen) ='',F5.2)') HBDIST
      write (IOUT,'(''#  donor-acceptor distance cutoff (sulphur)'',
     +              ''             ='',F5.2)') HBSULDIST
      write (IOUT,'(''#  hydrogen-acceptor distance cutoff'',
     +              ''                    = none'')')
      write (IOUT,'(''#  angular criteria applied'',
     +              ''                             = none'')')
      write (IOUT,'(''#  energy criteria applied'',
     +              ''                              = none'')')
      write (IOUT,'(''#  include HETATM records'',
     +              ''                               = '',L4)') DOHET
      write (IOUT,'(''#  include WATER records '',
     +              ''                               = '',L4)') DOWAT
      write (IOUT,'(''#'')')
      write (IOUT,'(''#----- Donor ----- ---- Acceptor ---         '',
     +              '' ------- Geometry ------ - Energy -'')')
      write (IOUT,'(''#index - res - atm index - res - atm typ span '',
     +              ''Dd-a Dh-a <d-H-N <a-O=C   kJ/mol'')')
C
C --------------------------------------------------------------------------
C
C should reorganize these loops so they are more efficient
C and also add POSSIBLE at an early stage
C
C maybe: I, J, POSSIBLE(I,J), K, DONOR(K), L, DONOR(L)
C possible will get rid of the need for complicated test of TERMINUS etc
C
C ############### original order
c      do I=1,NRES
c        do K=1,LENRES(I)
c          if (DONOR(K,I)) then
c            do J=1,NRES
c              if ((ABS(J-I).gt.1).or.((TERMINUS(2,I).and.TERMINUS(1,J)).and.(I.ne.J))) then
c                do L=1,LENRES(J)
c                  if (ACCEPTOR(L,J)) then
C ################
C
      do I=1,NRES
        do J=1,NRES
          if (POSSIBLE(J,I)) then !!! new line
            do K=1,LENRES(I)
              if (DONOR(K,I)) then
c                if ((ABS(J-I).gt.1).or.((TERMINUS(2,I).and.TERMINUS(1,J)).and.(I.ne.J))) then
                  do L=1,LENRES(J)
                    if (ACCEPTOR(L,J)) then
C
C find the donor to acceptor distance (use squares because faster)
C
                      DIST=DIST2(COORDS(1,K,I),COORDS(1,L,J))
C
C proceed if an interaction looks possible
C
                      if ( (DIST .le. HBDIST2) .or.
     -                     (
     -                      DIST .le. HBSULDIST2 .and.
     -                      (
     -                       ELEMENT(ATM(K,I)) .eq. 'S' .or.
     -                       ELEMENT(ATM(L,J)) .eq. 'S'
     -                      )
     -                     )
     -                   ) then 
C
C #### mainchain to mainchain
C
                        if (ISMAIN(ATM(K,I)).and.ISMAIN(ATM(L,J))) then
                          HBTYPE='MM'
                          FLAG=' '
                          ALT=' '
                          HDIST=9.99**2
                          ANGLNH=999.99
                          ANGLCO=999.99
                          ENERGY=999.99
C
C handle special cases
C
                          if (.not.TERMINUS(1,I)) then
                            HDIST=DIST2(COORDS(1,L,J),HATM(1,I))
                            ANGLNH=ANGLE(COORDS(1,L,J),HATM(1,I),
     +                                   COORDS(1,K,I))
                            call GETATM('C  ',ATM(1,J),LENRES(J),
     +                                   COORDS(1,1,J),CATM,RESNUM(J),
     +                                   RESNAM(J))
                            ANGLCO=ANGLE(CATM,COORDS(1,L,J),
     +                                   COORDS(1,K,I))
                            ENERGY=HBENG(CATM,COORDS(1,L,J),HATM(1,I),
     +                                   COORDS(1,K,I))
                          else if (TERMINUS(1,I) .and.
     +                             TERMINUS(2,J)) then
                            FLAG='*'
                          else
                            continue
                          end if
                          call PRHB(IOUT,I,RESNUM(I),CHNNAM(I),
     +                              SEQ(I),ATM(K,I),J,RESNUM(J),
     +                              CHNNAM(J),SEQ(J),ATM(L,J),
     -                              HBTYPE,ALT,(J-I),SQRT(DIST),
     +                              SQRT(HDIST),ANGLNH,ANGLCO,FLAG,
     +                              ENERGY)
C
C #### Sidechain to sidechain
C
                        else if (ISSIDE(ATM(K,I)) .and.
     +                           ISSIDE(ATM(L,J))) then
                          HBTYPE='SS'
                          FLAG=' '
                          ALT=' '
                          HDIST=9.99**2
                          ANGLNH=999.99
                          ANGLCO=999.99
                          ENERGY=999.99
C
C handle special cases
C
                          if (RESNAM(I)(1:2).eq.'CY' .and. 
     +                        RESNAM(J)(1:2).eq.'CY') then
                            if (DIST.lt.SSDIST2) then
                              HBTYPE='DS'
                            end if
                          end if
                          if ((ISNEGATIVE(RESNAM(I)) .and. 
     +                         ISPOSITIVE(RESNAM(J))) .or.
     +                        (ISPOSITIVE(RESNAM(I)) .and. 
     +                         ISNEGATIVE(RESNAM(J)))) then
                            FLAG='*'
                          end if
                          if (RESNAM(I).eq.'TRP') then
                            if (ATM(K,I).eq.'NE1') then
                              HDIST=DIST2(COORDS(1,L,J),SHATM(1,1,I))
                              ANGLNH=ANGLE(COORDS(1,L,J),SHATM(1,1,I),
     +                                     COORDS(1,K,I))
                              call PRHB(IOUT,I,RESNUM(I),CHNNAM(I),
     +                                SEQ(I),ATM(K,I),J,RESNUM(J),
     +                                CHNNAM(J),SEQ(J),ATM(L,J),
     -                                HBTYPE,ALT,(J-I),SQRT(DIST),
     +                                SQRT(HDIST),
     -                                ANGLNH,ANGLCO,FLAG,ENERGY)
                            else
                              write (STDERR,'(''hbond: unknown atom '',
     +                               ''(trp) '',A,I4,I4)') ATM(K,I),K,I
                            end if
                          else if (RESNAM(I).eq.'HIS') then
                            if (ATM(K,I).eq.'ND1') then
                              HDIST=DIST2(COORDS(1,L,J),SHATM(1,1,I))
                              ANGLNH=ANGLE(COORDS(1,L,J),SHATM(1,1,I),
     +                                     COORDS(1,K,I))
                              call PRHB(IOUT,I,RESNUM(I),CHNNAM(I),
     +                                  SEQ(I),ATM(K,I),J,RESNUM(J),
     +                                  CHNNAM(J),SEQ(J),ATM(L,J),
     -                                  HBTYPE,ALT,(J-I),SQRT(DIST),
     +                                  SQRT(HDIST),ANGLNH,ANGLCO,FLAG,
     +                                  ENERGY)
                            else if (ATM(K,I).eq.'NE2') then
                             HDIST=DIST2(COORDS(1,L,J),SHATM(1,2,I))
                             ANGLNH=ANGLE(COORDS(1,L,J),SHATM(1,2,I),
     +                                    COORDS(1,K,I))
                             call PRHB(IOUT,I,RESNUM(I),CHNNAM(I),
     +                                 SEQ(I),ATM(K,I),J,RESNUM(J),
     +                                 CHNNAM(J),SEQ(J),ATM(L,J),
     -                                 HBTYPE,ALT,(J-I),SQRT(DIST),
     +                                 SQRT(HDIST),ANGLNH,ANGLCO,FLAG,
     +                                 ENERGY)
                            else
                             write (STDERR,'(''hbond: unknown atom '',
     +                              ''(his) '',A,I4,I4)') ATM(K,I),K,I
                            end if
                          else if (RESNAM(I).eq.'ARG') then
                            if (ATM(K,I).eq.'NE ') then
                              HDIST=DIST2(COORDS(1,L,J),SHATM(1,1,I))
                              ANGLNH=ANGLE(COORDS(1,L,J),SHATM(1,1,I),
     +                                     COORDS(1,K,I))
                              call PRHB(IOUT,I,RESNUM(I),CHNNAM(I),
     +                                  SEQ(I),ATM(K,I),J,RESNUM(J),
     +                                  CHNNAM(J),SEQ(J),ATM(L,J),
     -                                  HBTYPE,ALT,(J-I),SQRT(DIST),
     +                                  SQRT(HDIST),ANGLNH,ANGLCO,FLAG,
     +                                  ENERGY)
                            else if (ATM(K,I).eq.'NH1') then
                              HDIST=DIST2(COORDS(1,L,J),SHATM(1,2,I))
                              ANGLNH=ANGLE(COORDS(1,L,J),SHATM(1,2,I),
     +                                     COORDS(1,K,I))
                              ALT='1'
                              call PRHB(IOUT,I,RESNUM(I),CHNNAM(I),
     +                                SEQ(I),ATM(K,I),J,RESNUM(J),
     +                                CHNNAM(J),SEQ(J),ATM(L,J),
     -                                HBTYPE,ALT,(J-I),SQRT(DIST),
     +                                SQRT(HDIST),ANGLNH,ANGLCO,FLAG,
     +                                ENERGY)
                              HDIST=DIST2(COORDS(1,L,J),SHATM(1,3,I))
                              ANGLNH=ANGLE(COORDS(1,L,J),SHATM(1,3,I),
     +                                     COORDS(1,K,I))
                              ALT='2'
                              call PRHB(IOUT,I,RESNUM(I),CHNNAM(I),
     +                                SEQ(I),ATM(K,I),J,RESNUM(J),
     +                                CHNNAM(J),SEQ(J),ATM(L,J),
     -                                HBTYPE,ALT,(J-I),SQRT(DIST),
     +                                SQRT(HDIST),ANGLNH,ANGLCO,FLAG,
     +                                ENERGY)
                            else if (ATM(K,I).eq.'NH2') then
                              HDIST=DIST2(COORDS(1,L,J),SHATM(1,4,I))
                              ANGLNH=ANGLE(COORDS(1,L,J),SHATM(1,4,I),
     +                                     COORDS(1,K,I))
                              ALT='1'
                              call PRHB(IOUT,I,RESNUM(I),CHNNAM(I),
     +                                SEQ(I),ATM(K,I),J,RESNUM(J),
     +                                CHNNAM(J),SEQ(J),ATM(L,J),
     -                                HBTYPE,ALT,(J-I),SQRT(DIST),
     +                                SQRT(HDIST),ANGLNH,ANGLCO,FLAG,
     +                                ENERGY)
                              HDIST=DIST2(COORDS(1,L,J),SHATM(1,5,I))
                              ANGLNH=ANGLE(COORDS(1,L,J),SHATM(1,5,I),
     +                                     COORDS(1,K,I))
                              ALT='2'
                              call PRHB(IOUT,I,RESNUM(I),CHNNAM(I),
     +                                SEQ(I),ATM(K,I),J,RESNUM(J),
     +                                CHNNAM(J),SEQ(J),ATM(L,J),
     -                                HBTYPE,ALT,(J-I),SQRT(DIST),
     +                                SQRT(HDIST),ANGLNH,ANGLCO,FLAG,
     +                                ENERGY)
                            else
                              write (STDERR,'(''hbond: unknown atom '',
     +                               ''(arg) '',A,I4,I4)') ATM(K,I),K,I
                            end if
                          else if (RESNAM(I)(1:2).eq.'AS') then
                            if (ATM(K,I) .eq. 'OD1' .or.
     +                          ATM(K,I) .eq. 'AD1') then
                              HDIST=DIST2(COORDS(1,L,J),SHATM(1,1,I))
                              ANGLNH=ANGLE(COORDS(1,L,J),SHATM(1,1,I),
     +                                     COORDS(1,K,I))
                              ALT='1'
                              call PRHB(IOUT,I,RESNUM(I),CHNNAM(I),
     +                                SEQ(I),ATM(K,I),J,RESNUM(J),
     +                                CHNNAM(J),SEQ(J),ATM(L,J),
     -                                HBTYPE,ALT,(J-I),SQRT(DIST),
     +                                SQRT(HDIST),ANGLNH,ANGLCO,FLAG,
     +                                ENERGY)
                              HDIST=DIST2(COORDS(1,L,J),SHATM(1,2,I))
                              ANGLNH=ANGLE(COORDS(1,L,J),SHATM(1,2,I),
     +                                     COORDS(1,K,I))
                              ALT='2'
                              call PRHB(IOUT,I,RESNUM(I),CHNNAM(I),
     +                                SEQ(I),ATM(K,I),J,RESNUM(J),
     +                                CHNNAM(J),SEQ(J),ATM(L,J),
     -                                HBTYPE,ALT,(J-I),SQRT(DIST),
     +                                SQRT(HDIST),ANGLNH,ANGLCO,FLAG,
     +                                ENERGY)
                            else if (ATM(K,I) .eq. 'OD2' .or.
     +                               ATM(K,I) .eq. 'ND2' .or.
     +                               ATM(K,I).eq.'AD2') then
                              HDIST=DIST2(COORDS(1,L,J),SHATM(1,3,I))
                              ANGLNH=ANGLE(COORDS(1,L,J),SHATM(1,3,I),
     +                                     COORDS(1,K,I))
                              ALT='1'
                              call PRHB(IOUT,I,RESNUM(I),CHNNAM(I),
     +                                SEQ(I),ATM(K,I),J,RESNUM(J),
     +                                CHNNAM(J),SEQ(J),ATM(L,J),
     +                                HBTYPE,ALT,(J-I),SQRT(DIST),
     +                                SQRT(HDIST),ANGLNH,ANGLCO,FLAG,
     +                                ENERGY)
                              HDIST=DIST2(COORDS(1,L,J),SHATM(1,4,I))
                              ANGLNH=ANGLE(COORDS(1,L,J),SHATM(1,4,I),
     +                                     COORDS(1,K,I))
                              ALT='2'
                              call PRHB(IOUT,I,RESNUM(I),CHNNAM(I),
     +                                SEQ(I),ATM(K,I),J,RESNUM(J),
     +                                CHNNAM(J),SEQ(J),ATM(L,J),
     -                                HBTYPE,ALT,(J-I),SQRT(DIST),
     +                                SQRT(HDIST),ANGLNH,ANGLCO,FLAG,
     +                                ENERGY)
                            else
                              write (STDERR,'(''hbond: unknown atom '',
     +                              ''(asn/asp) '',A,I4,I4)') ATM(K,I),K,I
                            end if
                          else if (RESNAM(I)(1:2).eq.'GL') then
                            if (ATM(K,I) .eq. 'OE1' .or.
     +                          ATM(K,I) .eq. 'AE1') then
                              HDIST=DIST2(COORDS(1,L,J),SHATM(1,1,I))
                              ANGLNH=ANGLE(COORDS(1,L,J),SHATM(1,1,I),
     +                                     COORDS(1,K,I))
                              ALT='1'
                              call PRHB(IOUT,I,RESNUM(I),CHNNAM(I),
     +                                SEQ(I),ATM(K,I),J,RESNUM(J),
     +                                CHNNAM(J),SEQ(J),ATM(L,J),
     -                                HBTYPE,ALT,(J-I),SQRT(DIST),
     +                                SQRT(HDIST),ANGLNH,ANGLCO,FLAG,
     +                                ENERGY)
                              HDIST=DIST2(COORDS(1,L,J),SHATM(1,2,I))
                              ANGLNH=ANGLE(COORDS(1,L,J),SHATM(1,2,I),
     +                                     COORDS(1,K,I))
                            ALT='2'
                            call PRHB(IOUT,I,RESNUM(I),CHNNAM(I),
     +                              SEQ(I),ATM(K,I),J,RESNUM(J),
     +                              CHNNAM(J),SEQ(J),ATM(L,J),
     -                              HBTYPE,ALT,(J-I),SQRT(DIST),
     +                              SQRT(HDIST),ANGLNH,ANGLCO,FLAG,
     +                              ENERGY)
                          else if (ATM(K,I) .eq. 'OE2' .or.
     +                             ATM(K,I) .eq. 'NE2' .or.
     +                             ATM(K,I) .eq. 'AE2') then
                            HDIST=DIST2(COORDS(1,L,J),SHATM(1,3,I))
                            ANGLNH=ANGLE(COORDS(1,L,J),SHATM(1,3,I),
     +                                     COORDS(1,K,I))
                            ALT='1'
                            call PRHB(IOUT,I,RESNUM(I),CHNNAM(I),
     +                              SEQ(I),ATM(K,I),J,RESNUM(J),
     +                              CHNNAM(J),SEQ(J),ATM(L,J),
     -                              HBTYPE,ALT,(J-I),SQRT(DIST),
     +                              SQRT(HDIST),ANGLNH,ANGLCO,FLAG,
     +                              ENERGY)
                            HDIST=DIST2(COORDS(1,L,J),SHATM(1,4,I))
                            ANGLNH=ANGLE(COORDS(1,L,J),SHATM(1,4,I),
     +                                     COORDS(1,K,I))
                            ALT='2'
                            call PRHB(IOUT,I,RESNUM(I),CHNNAM(I),
     +                              SEQ(I),ATM(K,I),J,RESNUM(J),
     +                              CHNNAM(J),SEQ(J),ATM(L,J),
     -                              HBTYPE,ALT,(J-I),SQRT(DIST),
     +                              SQRT(HDIST),ANGLNH,ANGLCO,FLAG,
     +                              ENERGY)
                          else
                            write (STDERR,'(''hbond: unknown atom '',
     +                         ''(glu/gln) '',A,I4,I4)') ATM(K,I),K,I
                          end if
                        else
                          call PRHB(IOUT,I,RESNUM(I),CHNNAM(I),
     +                              SEQ(I),ATM(K,I),J,RESNUM(J),
     +                              CHNNAM(J),SEQ(J),ATM(L,J),
     -                              HBTYPE,ALT,(J-I),SQRT(DIST),
     +                              SQRT(HDIST),ANGLNH,ANGLCO,FLAG,
     +                              ENERGY)
                        end if
C
C #### Sidechain to mainchain carbonyl
C
                      else if (ISMAIN(ATM(L,J)) .and.
     +                         ELEMENT(ATM(L,J)) .eq. 'O') then
                        HBTYPE='SO'
                        FLAG=' '
                        ALT=' '
                        HDIST=9.99**2
                        ANGLNH=999.99
                        ANGLCO=999.99
                        ENERGY=999.99
                        call GETATM('C  ',ATM(1,J),LENRES(J),
     +                           COORDS(1,1,J),CATM,RESNUM(J),RESNAM(J))
                        ANGLCO=ANGLE(CATM,COORDS(1,L,J),COORDS(1,K,I))
C
C handle special cases
C
                        if (TERMINUS(2,J) .and.
     +                      ISPOSITIVE(RESNAM(I))) then
                          FLAG='*'
                        end if
                        if (RESNAM(I).eq.'TRP') then
                          if (ATM(K,I).eq.'NE1') then
                            HDIST=DIST2(COORDS(1,L,J),SHATM(1,1,I))
                            ANGLNH=ANGLE(COORDS(1,L,J),SHATM(1,1,I),
     +                                   COORDS(1,K,I))
                            call PRHB(IOUT,I,RESNUM(I),CHNNAM(I),
     +                              SEQ(I),ATM(K,I),J,RESNUM(J),
     +                              CHNNAM(J),SEQ(J),ATM(L,J),
     -                              HBTYPE,ALT,(J-I),SQRT(DIST),
     +                              SQRT(HDIST),ANGLNH,ANGLCO,FLAG,
     +                              ENERGY)
                          else
                            write (STDERR,'(''hbond: unknown atom '',
     +                             ''(trp) '',A,I4,I4)') ATM(K,I),K,I
                          end if
                        else if (RESNAM(I).eq.'HIS') then
                          if (ATM(K,I).eq.'ND1') then
                            HDIST=DIST2(COORDS(1,L,J),SHATM(1,1,I))
                            ANGLNH=ANGLE(COORDS(1,L,J),SHATM(1,1,I),
     +                                   COORDS(1,K,I))
                            call PRHB(IOUT,I,RESNUM(I),CHNNAM(I),
     +                              SEQ(I),ATM(K,I),J,RESNUM(J),
     +                              CHNNAM(J),SEQ(J),ATM(L,J),
     -                              HBTYPE,ALT,(J-I),SQRT(DIST),
     +                              SQRT(HDIST),ANGLNH,ANGLCO,FLAG,
     +                              ENERGY)
                          else if (ATM(K,I).eq.'NE2') then
                            HDIST=DIST2(COORDS(1,L,J),SHATM(1,2,I))
                            ANGLNH=ANGLE(COORDS(1,L,J),SHATM(1,2,I),
     +                                   COORDS(1,K,I))
                            call PRHB(IOUT,I,RESNUM(I),CHNNAM(I),
     +                              SEQ(I),ATM(K,I),J,RESNUM(J),
     +                              CHNNAM(J),SEQ(J),ATM(L,J),
     -                              HBTYPE,ALT,(J-I),SQRT(DIST),
     +                              SQRT(HDIST),ANGLNH,ANGLCO,FLAG,
     +                              ENERGY)
                          else
                            write (STDERR,'(''hbond: unknown atom '',
     +                            ''(his) '',A,I4,I4)') ATM(K,I),K,I
                          end if
                        else if (RESNAM(I).eq.'ARG') then
                          if (ATM(K,I).eq.'NE ') then
                            HDIST=DIST2(COORDS(1,L,J),SHATM(1,1,I))
                            ANGLNH=ANGLE(COORDS(1,L,J),SHATM(1,1,I),
     +                                   COORDS(1,K,I))
                            call PRHB(IOUT,I,RESNUM(I),CHNNAM(I),
     +                              SEQ(I),ATM(K,I),J,RESNUM(J),
     +                              CHNNAM(J),SEQ(J),ATM(L,J),
     -                              HBTYPE,ALT,(J-I),SQRT(DIST),
     +                              SQRT(HDIST),ANGLNH,ANGLCO,FLAG,
     +                              ENERGY)
                          else if (ATM(K,I).eq.'NH1') then
                            HDIST=DIST2(COORDS(1,L,J),SHATM(1,2,I))
                            ANGLNH=ANGLE(COORDS(1,L,J),SHATM(1,2,I),
     +                                   COORDS(1,K,I))
                            ALT='1'
                            call PRHB(IOUT,I,RESNUM(I),CHNNAM(I),
     +                              SEQ(I),ATM(K,I),J,RESNUM(J),
     +                              CHNNAM(J),SEQ(J),ATM(L,J),
     -                              HBTYPE,ALT,(J-I),SQRT(DIST),
     +                              SQRT(HDIST),ANGLNH,ANGLCO,FLAG,
     +                              ENERGY)
                            HDIST=DIST2(COORDS(1,L,J),SHATM(1,3,I))
                            ANGLNH=ANGLE(COORDS(1,L,J),SHATM(1,3,I),
     +                                   COORDS(1,K,I))
                            ALT='2'
                            call PRHB(IOUT,I,RESNUM(I),CHNNAM(I),
     +                              SEQ(I),ATM(K,I),J,RESNUM(J),
     +                              CHNNAM(J),SEQ(J),ATM(L,J),
     -                              HBTYPE,ALT,(J-I),SQRT(DIST),
     +                              SQRT(HDIST),ANGLNH,ANGLCO,FLAG,
     +                              ENERGY)
                          else if (ATM(K,I).eq.'NH2') then
                            HDIST=DIST2(COORDS(1,L,J),SHATM(1,4,I))
                            ANGLNH=ANGLE(COORDS(1,L,J),SHATM(1,4,I),
     +                                   COORDS(1,K,I))
                            ALT='1'
                            call PRHB(IOUT,I,RESNUM(I),CHNNAM(I),
     +                              SEQ(I),ATM(K,I),J,RESNUM(J),
     +                              CHNNAM(J),SEQ(J),ATM(L,J),
     -                              HBTYPE,ALT,(J-I),SQRT(DIST),
     +                              SQRT(HDIST),ANGLNH,ANGLCO,FLAG,
     +                              ENERGY)
                            HDIST=DIST2(COORDS(1,L,J),SHATM(1,5,I))
                            ANGLNH=ANGLE(COORDS(1,L,J),SHATM(1,5,I),
     +                                   COORDS(1,K,I))
                            ALT='2'
                            call PRHB(IOUT,I,RESNUM(I),CHNNAM(I),
     +                              SEQ(I),ATM(K,I),J,RESNUM(J),
     +                              CHNNAM(J),SEQ(J),ATM(L,J),
     -                              HBTYPE,ALT,(J-I),SQRT(DIST),
     +                              SQRT(HDIST),ANGLNH,ANGLCO,FLAG,
     +                              ENERGY)
                          else
                            write (STDERR,'(''hbond: unknown atom ''
     +                             ''(arg) '',A,I4,I4)') ATM(K,I),K,I
                          end if
                        else if (RESNAM(I)(1:2).eq.'AS') then
                          if (ATM(K,I) .eq. 'OD1' .or.
     +                        ATM(K,I) .eq. 'AD1') then
                            HDIST=DIST2(COORDS(1,L,J),SHATM(1,1,I))
                            ANGLNH=ANGLE(COORDS(1,L,J),SHATM(1,1,I),
     +                                   COORDS(1,K,I))
                            ALT='1'
                            call PRHB(IOUT,I,RESNUM(I),CHNNAM(I),
     +                              SEQ(I),ATM(K,I),J,RESNUM(J),
     +                              CHNNAM(J),SEQ(J),ATM(L,J),
     -                              HBTYPE,ALT,(J-I),SQRT(DIST),
     +                              SQRT(HDIST),ANGLNH,ANGLCO,FLAG,
     +                              ENERGY)
                            HDIST=DIST2(COORDS(1,L,J),SHATM(1,2,I))
                            ANGLNH=ANGLE(COORDS(1,L,J),SHATM(1,2,I),
     +                                   COORDS(1,K,I))
                            ALT='2'
                            call PRHB(IOUT,I,RESNUM(I),CHNNAM(I),
     +                              SEQ(I),ATM(K,I),J,RESNUM(J),
     +                              CHNNAM(J),SEQ(J),ATM(L,J),
     -                              HBTYPE,ALT,(J-I),SQRT(DIST),
     +                              SQRT(HDIST),ANGLNH,ANGLCO,FLAG,
     +                              ENERGY)
                          else if (ATM(K,I) .eq. 'OD2' .or.
     +                             ATM(K,I) .eq. 'ND2' .or.
     +                             ATM(K,I) .eq. 'AD2') then
                            HDIST=DIST2(COORDS(1,L,J),SHATM(1,3,I))
                            ANGLNH=ANGLE(COORDS(1,L,J),SHATM(1,3,I),
     +                                   COORDS(1,K,I))
                            ALT='1'
                            call PRHB(IOUT,I,RESNUM(I),CHNNAM(I),
     +                              SEQ(I),ATM(K,I),J,RESNUM(J),
     +                              CHNNAM(J),SEQ(J),ATM(L,J),
     -                              HBTYPE,ALT,(J-I),SQRT(DIST),
     +                              SQRT(HDIST),ANGLNH,ANGLCO,FLAG,
     +                              ENERGY)
                            HDIST=DIST2(COORDS(1,L,J),SHATM(1,4,I))
                            ANGLNH=ANGLE(COORDS(1,L,J),SHATM(1,4,I),
     +                                   COORDS(1,K,I))
                            ALT='2'
                            call PRHB(IOUT,I,RESNUM(I),CHNNAM(I),
     +                              SEQ(I),ATM(K,I),J,RESNUM(J),
     +                              CHNNAM(J),SEQ(J),ATM(L,J),
     -                              HBTYPE,ALT,(J-I),SQRT(DIST),
     +                              SQRT(HDIST),ANGLNH,ANGLCO,FLAG,
     +                              ENERGY)
                          else
                            write (STDERR,'(''hbond: unknown atom ''
     +                            ''(asn/asp) '',A,I4,I4)') ATM(K,I),K,I
                          end if
                        else if (RESNAM(I)(1:2).eq.'GL') then
                          if (ATM(K,I) .eq. 'OE1' .or.
     +                        ATM(K,I) .eq. 'AE1') then
                            HDIST=DIST2(COORDS(1,L,J),SHATM(1,1,I))
                            ANGLNH=ANGLE(COORDS(1,L,J),SHATM(1,1,I),
     +                                   COORDS(1,K,I))
                            ALT='1'
                            call PRHB(IOUT,I,RESNUM(I),CHNNAM(I),
     +                              SEQ(I),ATM(K,I),J,RESNUM(J),
     +                              CHNNAM(J),SEQ(J),ATM(L,J),
     -                              HBTYPE,ALT,(J-I),SQRT(DIST),
     +                              SQRT(HDIST),ANGLNH,ANGLCO,FLAG,
     +                              ENERGY)
                            HDIST=DIST2(COORDS(1,L,J),SHATM(1,2,I))
                            ANGLNH=ANGLE(COORDS(1,L,J),SHATM(1,2,I),
     +                                   COORDS(1,K,I))
                            ALT='2'
                            call PRHB(IOUT,I,RESNUM(I),CHNNAM(I),
     +                              SEQ(I),ATM(K,I),J,RESNUM(J),
     +                              CHNNAM(J),SEQ(J),ATM(L,J),
     -                              HBTYPE,ALT,(J-I),SQRT(DIST),
     +                              SQRT(HDIST),ANGLNH,ANGLCO,FLAG,
     +                              ENERGY)
                          else if (ATM(K,I) .eq. 'OE2' .or.
     +                             ATM(K,I) .eq. 'NE2' .or.
     +                             ATM(K,I) .eq. 'AE2') then
                            HDIST=DIST2(COORDS(1,L,J),SHATM(1,3,I))
                            ANGLNH=ANGLE(COORDS(1,L,J),SHATM(1,3,I),
     +                                   COORDS(1,K,I))
                            ALT='1'
                            call PRHB(IOUT,I,RESNUM(I),CHNNAM(I),
     +                              SEQ(I),ATM(K,I),J,RESNUM(J),
     +                              CHNNAM(J),SEQ(J),ATM(L,J),
     -                              HBTYPE,ALT,(J-I),SQRT(DIST),
     +                              SQRT(HDIST),ANGLNH,ANGLCO,FLAG,
     +                              ENERGY)
                            HDIST=DIST2(COORDS(1,L,J),SHATM(1,4,I))
                            ANGLNH=ANGLE(COORDS(1,L,J),SHATM(1,4,I),
     +                                   COORDS(1,K,I))
                            ALT='2'
                            call PRHB(IOUT,I,RESNUM(I),CHNNAM(I),
     +                              SEQ(I),ATM(K,I),J,RESNUM(J),
     +                              CHNNAM(J),SEQ(J),ATM(L,J),
     -                              HBTYPE,ALT,(J-I),SQRT(DIST),
     +                              SQRT(HDIST),ANGLNH,ANGLCO,FLAG,
     +                              ENERGY)
                          else
                            write (STDERR,'(''hbond: unknown atom ''
     +                           ''(glu/gln) '',A,I4,I4)') ATM(K,I),K,I
                          end if
                        else
                          call PRHB(IOUT,I,RESNUM(I),CHNNAM(I),
     +                              SEQ(I),ATM(K,I),J,RESNUM(J),
     +                              CHNNAM(J),SEQ(J),ATM(L,J),
     -                              HBTYPE,ALT,(J-I),SQRT(DIST),
     +                              SQRT(HDIST),ANGLNH,ANGLCO,FLAG,
     +                              ENERGY)
                        end if
C
C #### sidechain to mainchain amide
C
                      else if (ISMAIN(ATM(K,I)) .and.
     +                         ELEMENT(ATM(K,I)).eq.'N') then
                        HBTYPE='SN'
                        FLAG=' '
                        ALT=' '
                        ANGLCO=999.99
                        ENERGY=999.99
                        HDIST=9.99**2
                        if (.not. TERMINUS(1,I)) then
                          HDIST=DIST2(COORDS(1,L,J),HATM(1,I))
                          ANGLNH=ANGLE(COORDS(1,K,I),HATM(1,I),
     +                                   COORDS(1,L,J))
                        end if
C
C special cases
C
                        if (TERMINUS(1,I) .and.
     +                      ISNEGATIVE(RESNAM(J))) then
                          FLAG='*'
                        end if
                        if (RESNAM(J)(1:2).eq.'GL') then
                          call GETATM('CD ',ATM(1,J),LENRES(J),
     +                         COORDS(1,1,J),CATM,RESNUM(J),RESNAM(J))
                          ANGLCO=ANGLE(CATM,COORDS(1,L,J),COORDS(1,K,I))
C                         ENERGY=HBENG(CATM,COORDS(1,L,J),HATM(1,I),
C    +                           COORDS(1,K,I))
                        else if (RESNAM(J)(1:2).eq.'AS') then
                          call GETATM('CG ',ATM(1,J),LENRES(J),
     +                         COORDS(1,1,J),CATM,RESNUM(J),RESNAM(J))
                          ANGLCO=ANGLE(CATM,COORDS(1,L,J),COORDS(1,K,I))
C                         ENERGY=HBENG(CATM,COORDS(1,L,J),HATM(1,I),
C    +                         COORDS(1,K,I))
                        end if
                        call PRHB(IOUT,I,RESNUM(I),CHNNAM(I),
     +                         SEQ(I),ATM(K,I),J,RESNUM(J),
     +                         CHNNAM(J),SEQ(J),ATM(L,J),
     -                         HBTYPE,ALT,(J-I),SQRT(DIST),
     +                         SQRT(HDIST),ANGLNH,ANGLCO,FLAG,
     +                         ENERGY)
                      else
                        write (STDERR,'(''hbond: strange bond type'')')
                      end if
                    end if
                  end if
                end do
c              end if
            end if
          end do
          end if !!! new line
        end do
C
C Process HETATMs
C
        if (DOHET) then
          do L=1,LENRES(I)
            if (ACCEPTOR(L,I) .or. DONOR(L,I)) then
              do J=1,NHET
                DIST=DIST2(COORDS(1,L,I),HETCOORDS(1,J))
                if ((DIST .le. HBDIST2) .or.
     +              (DIST .le. HBSULDIST2 .and.
     +               (ELEMENT(ATM(L,I)) .eq. 'S'))) then 
                  ANGLCO=999.99
                  ENERGY=999.99
                  ANGLNH=999.99
                  HDIST=9.99**2
                  ISPAN=0
                  FLAG=' '
                  if (ISSIDE(ATM(L,I))) then
                    HBTYPE='SH'
                  else
                    HBTYPE='MH'
                  end if
                  if (HETNAM(J) .eq. 'HOH') then
                    HBTYPE(2:2)='W'
                  end if
                  if (.not. DOWAT .and. HBTYPE(2:2) .eq. 'W') then
                    continue
                  else
                    write (IOUT,'(1X,I3,1X,A5,1X,A1,1X,A1,1X,A3,
     -              1X,I3,1X,A5,1X,A3,1X,A4,A2,2X,I4,1X,F4.2,
     -              1X,F4.2,1X,F6.2,1X,F6.2,1X,A1,1X,F6.2)')
     -              I,RESNUM(I),CHNNAM(I),SEQ(I),ATM(L,I),
     -              J,HETNUM(J),HETNAM(J),HETATM(J),
     -              HBTYPE,ISPAN,SQRT(DIST),SQRT(HDIST),
     -              ANGLNH,ANGLCO,FLAG,ENERGY
                    if (IOCOORDS) then
C
C write out coordinates of interacting pairs to hbond.out file
C
                      if (HETATM(J)(2:2) .ne. 'C') then
C                        NIOATM=NIOATM+1
                        write (12,'(''HETATM'',I5,1X,A4,1X,A3,2X,A5,
     -                         3X,3F8.3)')
     -                         NIOATM,HETATM(J),HETNAM(J),
     -                         HETNUM(J),(HETCOORDS(K,J),K=1,3)
C                        NIOATM=NIOATM+1
                        write (12,'(''ATOM  '',I5,2X,A3,1X,A3,1X,A1,A5,
     -                         3X,3F8.3)')
     -                         NIOATM,ATM(L,I),RESNAM(I),CHNNAM(I),
     -                           RESNUM(I),(COORDS(K,L,I),K=1,3)
                      end if
                    end if
                  end if
                end if
              end do
            end if
          end do
        end if
C
      end do
C
      close (unit=IOUT)
C
C --------------------------------------------------------------------------
C
C new loop for arguments
C
      end do
C
C --------------------------------------------------------------------------
C
      call EXIT(0)
C
C ---------------------------------------------------------------------------
C
C Catch bad distance value, etc.
C
5     call ERROR('hbond: error opening file',OUTFILE,1)
C
      end 
