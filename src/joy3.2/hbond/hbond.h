C #############################################################################
C
C Parameters for hbond
C
C MAXFILELEN - maximum length of filename
C
      implicit NONE
      integer MAXFILELEN
      parameter (MAXFILELEN=14)
C
C MAXRES - The maximum number of residues 
C MAXATS - The maximum number of atoms that can occur in a residue (14 for
C          C-terminal tryptophan)
C MAXHET - The maximum number of het-atoms in a file
C MAXSIDE - The maximum number of sidechain polar hydrogens (5 for ARG)
C
      integer MAXRES, MAXATS, MAXHET, MAXSIDE
      parameter (MAXRES=1000, MAXATS=20, MAXHET=1000, MAXSIDE=5)
C
C SSDIST - The maximum distance for a disulphide bond interaction
C
      real SSDIST, SSDIST2
      parameter (SSDIST=3.0, SSDIST2=SSDIST*SSDIST)
C
C HBD - The maximum default distance for a hydrogen bond interaction
C (for oxygen and nitrogen atoms), and also for sulphur atoms
C
      real HBDIST, HBDIST2, HBSULDIST, HBSULDIST2
      parameter (HBDIST   =3.5, HBDIST2=HBDIST*HBDIST)
      parameter (HBSULDIST=4.0, HBSULDIST2=HBSULDIST*HBSULDIST)
C
C NSIZE - the size of a protein (in residues) that makes it worthwile
C         calculating the distance matrix, to not consider impossible
C         interactions.
C
      integer NSIZE
      parameter (NSIZE=50)	!! very empirically determined
C
      character*18 DATE
C
C define STDIN, STDOUT, STDERR
C
      integer STDIN, STDOUT, STDERR
      parameter (STDIN=5, STDOUT=6, STDERR=6)
C
C include joylib definitions
C
      include '../joylib/joylib.h'
C
C #############################################################################
