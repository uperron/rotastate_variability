      subroutine HELP()
C
      integer STDERR
      data STDERR /6/
C
      write (STDERR,'(''hbond - a program to find potential '',
     +                ''hydrogen bonds'')')
      write (STDERR,'(''options : h - print help message'')')
      write (STDERR,'(''          V - version'')')
      write (STDERR,'('' '')')
      write (STDERR,'(''usage : hbond -hv <coord.file>'')')
      return
      end
