      real function PKA(RES)
C
C returns the first pKa of the sidechain, non-polar residues are assigned the
C value 99.99 (see below for why this was written!)
C
C data comes from Fersht (all except SER and THR, from App IV of
C Streitweiser and Heathcock)
C
C N.B. this function was written to assign the real (probable) donor and/or
C acceptors for a particular sidechain interaction (where this is ambiguous),
C basically the one with the higher pKa will be the donor.
C Still have problems with ASN and GLN, these must surely be donors for acids,
C and acceptors for bases, so will make up a suitable pKa for these, note it
C has no physical meaning!! May flag other ambiguous cases later, for example
C what is the donor in a SER/THE - ASN/GLN pair (one cannot assume identity
C of amide atoms)
C
C Note: TRP is not assigned.
C Note: CYS is assigned the same value as CYH
C
      implicit NONE
      integer NUMAA
      parameter (NUMAA=21)
C
      integer I
      character*1 RES, RESIDUE(NUMAA)
      real VALUE(NUMAA)
C
c      data RESIDUE / 'ALA', 'CYS', 'ASP', 'GLU', 'PHE', 'GLY', 'HIS',
c     -               'ILE', 'LYS', 'LEU', 'MET', 'ASN', 'PRO', 'GLN',
c     -               'ARG', 'SER', 'THR', 'VAL', 'TRP', 'TYR', 'CYH'/
      data RESIDUE /   'A',   'C',   'D',   'E',   'F',   'G',   'H',
     -                 'I',   'K',   'L',   'M',   'N',   'P',   'Q',
     -                 'R',   'S',   'T',   'V',   'W',   'Y',   'C' /
      data VALUE   / 99.99, 10.46,  3.90,  4.07, 99.99, 99.99,  6.04,
     -               99.99, 10.79, 99.99, 99.99,  9.90, 99.99,  9.91,
     -               12.48, 15.90, 18.00, 99.99, 99.99, 10.13, 10.46/
C
      do I=1,NUMAA
        if (RES.eq.RESIDUE(I)) then
          PKA=VALUE(I)
          return
        end if
      end do
C
      call ERROR('hbond: cannot assign pKa for,',RES,0)
      PKA=99.99
C
      return
      end
