      subroutine PRHB(IOUT,I,RESNUM1,CHNNAM1,SEQ1,ATM1,
     -                     J,RESNUM2,CHNNAM2,SEQ2,ATM2,
     -           HBTYPE,ALT,ISPAN,DIST,HDIST,ANGLNH,ANGLCO,FLAG,ENERGY)
C
      implicit NONE
      integer ISPAN, I, J, IOUT
      real ANGLNH, ANGLCO, ENERGY, DIST , HDIST, PKA
      character*5 RESNUM1, RESNUM2
      character*3 ATM1, ATM2
      character*2 HBTYPE
      character*1 CHNNAM1, CHNNAM2, SEQ1, SEQ2, FLAG, ALT
      logical PRINTLINE, ISDONOR, ISACCEPTOR
C
      character*35 IDENT
      data IDENT /'@(#) prhb.f - (c) jpo 1990         '/
C
      PRINTLINE=.true.
      if (HDIST .gt. 9.99) then
        HDIST=9.99
        ANGLNH=999.99
      end if
      if (DIST .gt. 9.99) then
        DIST=9.99
        call ERROR('hbond: impossible distance',' ',0)
      end if
C
C Assign true donor acceptor pair for sidechain - sidechain bonds
C
      if (HBTYPE .eq. 'SS') then
        if ((ISDONOR(ATM1,SEQ1) .and. ISDONOR(ATM2,SEQ2))) then
C
C questionable code determining which one of two alternatives is likely to
C be correct w.r.t. hydrogen position
C
          if (PKA(SEQ1) .lt. PKA(SEQ2)) then
            PRINTLINE=.false.
          end if
C
          if (SEQ1 .eq. 'N' .or. SEQ2 .eq. 'N') then
            PRINTLINE=.true.
            FLAG='!'
          else if (SEQ1 .eq. 'Q' .or. SEQ2 .eq. 'Q') then
            PRINTLINE=.true.
            FLAG='!'
C
C Add flag for histidine
C
          else if (SEQ1 .eq. 'H' .or. SEQ2 .eq. 'H') then
            PRINTLINE=.true.
            FLAG='!'
          end if
        end if
      end if
C
C Output Hydrogen bonding data
C
      if (PRINTLINE) then
        write (IOUT,'(1X,I3,1X,A5,1X,A1,1X,A1,1X,A3,1X,I3,1X,A5,1X,A1,
     -  1X,A1,1X,A3,1X,A2,1X,A1,I4,1X,F4.2,1X,F4.2,1X,
     -  F6.2,1X,F6.2,1X,A1,1X,F6.2)')I,RESNUM1,CHNNAM1,SEQ1,ATM1,
     -  J,RESNUM2,CHNNAM2,SEQ2,ATM2,HBTYPE,ALT,ISPAN,DIST,HDIST,
     -  ANGLNH,ANGLCO,FLAG,ENERGY
      end if
C
      return
      end
