      subroutine RMSD(X1,X2,RMS,N)
C
C !! N.B. unusual arrays for X1 and X2, no lengths in these, maybe OK, if
C adjacent, but should really change to passed size arrays
C
      implicit NONE
      real X1(3,1), X2(3,1), RMS
      integer N, I
C
      RMS=0.0
      do I=1,N
        RMS=RMS+((X1(1,I)-X2(1,I))**2+
     +           (X1(2,I)-X2(2,I))**2+
     +           (X1(3,I)-X2(3,I))**2)
      end do
      RMS=SQRT(RMS/real(N))
C
      return
      end
