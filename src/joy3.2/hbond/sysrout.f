c --- These routines are here for systems that do not have them.

c --- Could use C routines instead and link them in joy.

c --- Could also use system(cmd) routine and the UNIX date command.

      subroutine exit(i)
        integer i
        write(*,'(a,1x,i4)') 'EXIT: ', i
        stop
      end

      subroutine date(s)
        character s*(*)
        return
      end

      subroutine time(s)
        character s*(*)
        return
      end
