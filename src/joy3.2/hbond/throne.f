      subroutine THRONE(CHAR,CHARNW,NRES)
C
C----------------------------------------------------------------------------
C
C PURPOSE
C
C Converts three letter code to one letter code
C
C----------------------------------------------------------------------------
C
      include 'hbond.h'
C
      character*(*) CHAR(MAXRES), CHARNW(MAXRES)
      character*24  AA1
      character*3   AA3(24), NAM3
      integer I, J, NRES
      logical FOUND
      logical REPORT
C
C ----------------------------------------------------------------------------
C
C Ordered for speed, non-standard code CYH == J
C
      data AA1/'SGVTALDIPKQNFYERCHWMBZXJ'/
      data AA3/'SER','GLY','VAL','THR','ALA',
     -         'LEU','ASP','ILE','PRO','LYS',
     -         'GLN','ASN','PHE','TYR','GLU',
     -         'ARG','CYS','HIS','TRP','MET',
     -         'ASX','GLX','UNK','CYH'/
C
C ---------------------------------------------------------------------------
C
C Set report to identify unknown residues
C
      REPORT=.false.
      do I=1,NRES
        NAM3=CHAR(I)
        FOUND=.FALSE.
        do J=1,24
          if (NAM3.eq.AA3(J)) then
            CHARNW(I)=AA1(J:J)
            FOUND=.TRUE.
            goto 2
          endif
        end do
2       continue
        if (.not.FOUND) then
          CHARNW(I)='X'
          if (REPORT) then
            write (6,'(''throne: unknown residue code '',A)') NAM3
          endif
        endif
      end do
C
      return
      end
