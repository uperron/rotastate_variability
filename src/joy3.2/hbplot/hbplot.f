      program HBPLOT
C
C writes out a grap representation of the hydogren bonding plot of a protein
C
      include '../joy/joy.h'
C
      integer MAXHB
      parameter (MAXHB=(MAXRES*(MAXRES-1))/2)
      character*(MAXFILELEN) FILE
      character*5 NAM1(MAXHB), NAM2(MAXHB)
      character*1 RES1(MAXHB), RES2(MAXHB)
      integer NRES, I, J, ILIST(3,MAXHB), NHB, ITMP
      integer IIN, IIO, IARGC, FSIZE
      logical LABEL
C
C FSIZE is the point size of label letters
C 5 point was crappy bit-mapped with eroff and lj2, changed to 6
C
      data FSIZE /6/
      data IIO /STDOUT/
      data LABEL /.true./
C
      NRES=0
C
      if (IARGC().gt.0) then
        IIN=1
        call GETARG(1,FILE)
        open (file=FILE,unit=1,status='old',form='formatted',err=3)
      else
        IIN=5
      end if
C
      write (IIO,'(A)') FILE
C
C ILIST contains donor, acceptor, and class for HBs
C NHB is the number of HBs found
C
      call RDHBD(IIN,ILIST,NAM1,NAM2,RES1,RES2,NRES,NHB)
C
      if (NRES .lt. 1) then
        call ERROR('hbplot: no residues in file',' ',1)
      end if
C
C Because we don't need direction (d->a) nature of HB
C process list so that ILIST(1,.) is always > ILIST(2,.)
C
C But when labelling, it is useful to know what is donor and what is
C acceptor
C
      if (.not. LABEL) then
        do I=1,NHB
          if (ILIST(1,I) .lt. ILIST(2,I)) then
            ITMP=ILIST(1,I)
            ILIST(1,I)=ILIST(2,I)
            ILIST(2,I)=ITMP
          end if
        end do
      end if
C
      call STARTGRAP(IIO,NRES)
C
      if (LABEL) then	! label + sequence, not folded
C
C 1 is bold   - to NH
C 2 is roman  - to CO
C 3 is italic - to sidechain
C
        do I=1,NHB
          if (ILIST(3,I) .eq. 1) then
            write (IIO,'(''"\fB'',A,A,''\(->'',A,A,''\fP" size '',I1,
     +             '' at '',I4,'','',I4)')
     +             NAM1(I)(FIRSTCHAR(NAM1(I)):LASTCHAR(NAM1(I))),
     +             RES1(I),
     +             NAM2(I)(FIRSTCHAR(NAM2(I)):LASTCHAR(NAM2(I))),
     +             RES2(I),
     +             FSIZE,ILIST(1,I),ILIST(2,I)
          else if (ILIST(3,I) .eq. 2) then
            write (IIO,'(''"\fR'',A,A,''\(->'',A,A,''\fP" size '',I1,
     +             '' at '',I4,'','',I4)')
     +             NAM1(I)(FIRSTCHAR(NAM1(I)):LASTCHAR(NAM1(I))),
     +             RES1(I),
     +             NAM2(I)(FIRSTCHAR(NAM2(I)):LASTCHAR(NAM2(I))),
     +             RES2(I),
     +             FSIZE,ILIST(1,I),ILIST(2,I)
          else if (ILIST(3,I) .eq. 3) then
            write (IIO,'(''"\fI'',A,A,''\(->'',A,A,''\fP" size '',I1,
     +             '' at '',I4,'','',I4)')
     +             NAM1(I)(FIRSTCHAR(NAM1(I)):LASTCHAR(NAM1(I))),
     +             RES1(I),
     +             NAM2(I)(FIRSTCHAR(NAM2(I)):LASTCHAR(NAM2(I))),
     +             RES2(I),
     +             FSIZE,ILIST(1,I),ILIST(2,I)
          else 
            continue
          end if
        end do
      else	! symbols
C
C 1 is sidechain to NH         - delta
C 2 is sidechain to CO         - times
C 3 is sidechain to sidechain  - square
C 4 is mainchain to mainchain  - bullet
C 5 is disulphide              - box
C 6 is mainchain to het
C 7 is mainchain to water
C 8 is sidechain to het
C 9 is sidechain to water
C
        do I=1,NHB
          if (ILIST(3,I) .eq. 1) then
            write (IIO,'(''delta at '',I4,'','',I4)')
     +             ILIST(1,I), ILIST(2,I)
          else if (ILIST(3,I) .eq. 2) then
            write (IIO,'(''times at '',I4,'','',I4)')
     +             ILIST(1,I), ILIST(2,I)
          else if (ILIST(3,I) .eq. 3) then
            write (IIO,'(''square at '',I4,'','',I4)')
     +             ILIST(1,I), ILIST(2,I)
          else if (ILIST(3,I) .eq. 4) then
            write (IIO,'(''bullet at '',I4,'','',I4)')
     +             ILIST(1,I), ILIST(2,I)
          else if (ILIST(3,I) .eq. 5) then
            write (IIO,'(''box at '',I4,'','',I4)')
     +             ILIST(1,I), ILIST(2,I)
          else 
            continue
          end if
        end do
      end if
C
      call ENDGRAP(IIO)
C
      call EXIT(0)
C
3     call ERROR('hbplot: cannot open file',FILE,1)
C
      end
C
C ########################################################
C
      subroutine STARTGRAP(IIO,NRES)
C
      integer IIO
      write (IIO,'(''.G1'')')
      write (IIO,'(''ticks off'')')
      write (IIO,'(''ticks bot out'')')
      write (IIO,'(''ticks right out'')')
      write (IIO,'(''frame ht 6 wid 6 solid left invis top invis'')')
      write (IIO,'(''coord x 0,'',I4,'' y 0,'',I4)') NRES+1,NRES+1
C
      return
      end
C
      subroutine ENDGRAP(IIO)
C
      integer IIO
      write (IIO,'(''.G2'')')
C
      return
      end
C
C ########################################################
C
      subroutine RDHBD(IIN,ILIST,NAM1,NAM2,RES1,RES2,NRES,NHB)
C
C hacked from RDHBD in joy
C
      include '../joy/joy.h'
C
      integer MAXHB
      parameter (MAXHB=(MAXRES*(MAXRES-1))/2)
      integer NRES, I, J, ILIST(3,MAXHB), NHB, IIN
      character*80 TEXT
      character*3 AT1, AT2, TYPE
      character*5 NAM1(MAXHB), NAM2(MAXHB)
      character*1 RES1(MAXHB), RES2(MAXHB)
      logical FOUND
C
      FOUND=.FALSE.
      NHB=0
      do I=1,(MAXRES*(MAXRES-1))/2
        do J=1,3
          ILIST(J,I)=0
        end do
      end do
C
1     read (IIN,'(A)',end=3,err=4) TEXT
      if (index(TEXT,'number of residues') .gt. 1) then
        read (TEXT,'(26X,I4)') NRES
        goto 1
      else if (INDEX(TEXT(2:7),'index ').lt.1) then
        goto 1
      else
2       read (IIN,'(A)',end=3,err=4) TEXT
        NHB=NHB+1
C
        if (NHB .gt. MAXHB) then
          call ERROR('hbplot: too many hydrogen bonds',' ',1)
        end if
C
        if (TEXT(1:10) .ne. '          ' .and..not. FOUND) then
          read (TEXT,'(1X,I3,1X,A5,3X,A1,1X,A3,1X,I3,
     +                 1X,A5,3X,A1,1X,A3,1X,A3)',
     -          err=4) ILIST(1,NHB),NAM1(NHB),RES1(NHB),AT1,
     -                 ILIST(2,NHB),NAM2(NHB),RES2(NHB),AT2,TYPE
C
        if (TYPE .eq. 'SMN' .or. TYPE .eq. 'SN ') then
          ILIST(3,NHB)=1
        else if (TYPE .eq. 'SMO' .or. TYPE .eq. 'SO ') then
          ILIST(3,NHB)=2
        else if (TYPE .eq. 'SS ') then
          ILIST(3,NHB)=3
        else if (TYPE .eq. 'MM ') then
          ILIST(3,NHB)=4
        else if (TYPE .eq. 'DS ') then
          ILIST(3,NHB)=5
        else if (TYPE .eq. 'MH ') then
            ILIST(3,NHB)=6
        else if (TYPE .eq. 'MW ') then
          ILIST(3,NHB)=7
        else if (TYPE.EQ.'SH ') then
          ILIST(3,NHB)=8
        else if (TYPE .eq. 'SW ') then
          ILIST(3,NHB)=9
        else
          call ERROR('hbplot: unknown class of H-bond ',TYPE,1)
        end if 
        goto 2
      else
        FOUND=.TRUE.
      end if
      goto 2
      end if
3     continue
C
      close (UNIT=1)
C
      return
C
4     call ERROR('hbplot: corrupted data file',' ',1)
C
      end
