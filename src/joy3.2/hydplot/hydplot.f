      program HYDPLOT
C
C produce a Kyte and Doolittle type plot from a sequence (PIR format)
C
C input (of sst file) either STDIN, or file name on command line
C
C logical SMOOTH controls whether averaging is used or not
C
      implicit NONE
C
C ----------------------------------------------------------------------------
C
C PARAMETERS
C
C Parameter file for joy. Used as an include file.
C
C MAXSEQ    The maximum number of structures 
C MAXRES    The maximum number of positions in an alignment 
C MAXTITLE  The maximum number of substitution table titles
C MAXNEWSEQ The maximum number of sequences
C MAXLAB    The maximum number of labels (must include structures!!)
C MAXTEXT   The maximum number of text strings 
C NUMFEAT   The number of features (attributes) considered per residue
C NUMEXAM   The number of examined features (not related to anything above)
C MAXOPT    The maximum number of single character options
C SMALL     Arbitrarily small number
C
C MAXHET    Max no of heterogen atoms (used by grcons only)
C MAXATS    Max no of atoms in a sidechain (used by grcons only)
C
C MAXTITLE is obtained by multiplication of the size of all the dimensions of
C NUMFEAT. i.e. at the moment it is 4*2*2*2*2.
C
C Last changed version 2.3e 
C
      integer MAXSEQ, MAXRES, MAXTITLE, MAXNEWSEQ, MAXOPT, MAXLAB,
     -        MAXTEXT, NUMEXAM, NUMFEAT, MAXFILELEN, MAXSHEET, MAXHELIX
      integer MAXHET, MAXATS
      integer STDOUT, STDIN, STDERR
      real SMALL
      parameter (MAXSEQ=60, MAXRES=3500, MAXTITLE=64, MAXOPT=10,
     -           MAXNEWSEQ=10, MAXLAB=3+MAXSEQ, MAXTEXT=6, NUMFEAT=13,
     -           NUMEXAM=12)
      parameter (STDOUT=6, STDIN=5, STDERR=6)
c      parameter (STDOUT=6, STDIN=5, STDERR=0)
      parameter (MAXFILELEN=256, SMALL=1.E-15)
Ccap  parameter (MAXSHEET=40, MAXHELIX=40)
      parameter (MAXATS=30, MAXHET=500)
C
C ----------------------------------------------------------------------------
C
C Declarations for date.h
C
      character*40 DATE
      character*4 VERSION
C
C declarations for VERBOSE variable, reports more information than usual
C
      logical VERBOSE
      common /VERBOSE/VERBOSE
C
C ----------------------------------------------------------------------------
C
      character*50 LBIN
C
C ----------------------------------------------------------------------------
C
C definitions for library functions
C
      include '../joylib/joylib.h'
C
      character*(MAXFILELEN) FILE
      integer ISEQ(MAXRES)
      integer NRES, IIN, IIO, IARGC, I, J
      real KYT(20), EIS(20), PRI(20), JPO(20)
      real AVEK(MAXRES), AVEJ(MAXRES), OFFSET
      logical SMOOTH
C
C values hacked from Dan Donnellys perscan suite
C
C Kyte and Doolittle
C
C                  A     C     D     E     F
C                  G     H     I     K     L
C                  M     N     P     Q     R
C                  S     T     V     W     Y
C
      data KYT / 1.80,  2.50, -3.50, -3.50,  2.80,
     +          -0.40, -3.20,  4.50, -3.90,  3.80,
     +           1.90, -3.50, -1.60, -3.50, -4.50,
     +          -0.80, -0.70,  4.20, -0.90, -1.30/
C
C Eisenberg consensus
C
      data EIS / 0.62,  0.29, -0.90, -0.74,  1.20,
     +           0.48, -0.40,  1.40, -1.50,  1.10,
     +           0.64, -0.78,  0.12, -0.85, -2.50,
     +          -0.18, -0.05,  1.10,  0.81,  0.26/
C
C Prift scale ?
C
      data PRI / 0.22,  4.07, -3.08, -1.81,  4.44,
     +           0.00,  0.46,  4.77, -3.04,  5.66,
     +           4.23, -0.46, -2.23, -2.81,  1.42,
     +          -0.45, -1.90,  4.67,  1.04,  3.23/
C
C Data from my analysis of a residues propensity to be buried
C
      data JPO / 1.35,  2.30,  0.38,  0.16,  1.66,
     +           1.11,  0.82,  1.95,  0.06,  1.82,
     +           1.60,  0.31,  0.56,  0.36,  0.15,
     +           0.62,  0.71,  1.88,  1.57,  0.86/
C
      data SMOOTH /.true./
      data IIO /STDOUT/
      data OFFSET /3.0/
C
      NRES=0
C
      if (IARGC() .eq. 1) then
        IIN=1
        call GETARG(1,FILE)
        open (file=FILE,unit=IIN,form='formatted',err=3)
      else
        IIN=5
      end if
C
      call RDSEQ2(IIN,ISEQ,NRES)
C
C smoothing is simply averaging over every seven residues SOAP-7 of K+D
C
      if (NRES .gt. 2) then
        if (SMOOTH) then
          write (IIO,'(''SOAP-7 of Kyte and Doolittle'')')
          call STARTGRAP(IIO,NRES,'Hyd')
          do I=4,NRES-3
            AVEJ(I)=0.0
            AVEK(I)=0.0
            do J=I-3,I+3
              AVEK(I)=AVEK(I)+KYT(ISEQ(J))
              AVEJ(I)=AVEJ(I)+JPO(ISEQ(J))
            end do
            AVEK(I)=AVEK(I)/7.0
            AVEJ(I)=AVEJ(I)/7.0
          end do
          do I=4,NRES-4
            write (IIO,'(''line from '',I4,'','',F5.2,'' to '',I4,
     +             '','',F5.2)') I, AVEJ(I)+OFFSET, I+1, AVEJ(I+1)+OFFSET
            write (IIO,'(''line from '',I4,'','',F5.2,'' to '',I4,
     +             '','',F5.2)') I, AVEK(I), I+1, AVEK(I+1)
          end do
          write (IIO,'(''line from 0, 0 to '',I4,'', 0'')') NRES+1
          write (IIO,'(''line from 0, '',F5.2,'' to '',I4,'', '',
     +          F5.2)') OFFSET+1.0,NRES+1,OFFSET+1.0
          call ENDGRAP(IIO)
        else
          write (IIO,'(''SOAP-1 of Kyte and Doolittle'')')
          call STARTGRAP(IIO,NRES,'Hyd')
          do I=1,NRES-1
            write (IIO,'(''line from '',I4,'','',F5.2,'' to '',I4,
     +             '','',F5.2)') I, KYT(ISEQ(I)), I+1, KYT(ISEQ(I+1))
          end do
          call ENDGRAP(IIO)
        end if
      end if
C
      call EXIT(0)
C
3     call ERROR('hydplot: cannot open file',FILE,1)
C
      end
C
C ########################################################
C
      subroutine STARTGRAP(IIO,NRES,LABEL)
C
      character*(*) LABEL
      integer IIO, NRES
C
      write (IIO,'(''.G1'')')
      write (IIO,'(''label left "'',A,''"'')') LABEL
      write (IIO,'(''frame wid 6.5 solid right invis top invis'')')
      write (IIO,'(''coords x 0, '',I4)') NRES+1
C
      return
C
      end
C
C ####################################################################
C
      subroutine ENDGRAP(IIO)
C
      write (IIO,'(''.G2'')')
C
      return
      end
C
C ######################################################
C
      subroutine RDSEQ(IO,ISEQ,LENSEQ)
C
C reads in one sequence from unit IO
C
      include '../joy/joy.h' 
C
      integer MAXLENLINE
      parameter (MAXLENLINE=80)
C
      character*(MAXLENLINE) LINE
      character*1 SEQ
      integer ISEQ(MAXRES), LENSEQ, IO, LENLINE, N, I
      logical FEND
      FEND=.false.
C
      N=0
1     read (IO,'(A)',end=2,err=9) LINE
      LENLINE=lastchar(LINE)
      if (index(LINE,'*') .ge. 1) then
        LENLINE=LENLINE-1
        FEND=.true.
      end if
      do I=1,LENLINE
        N=N+1
        if (N .gt. MAXRES) then
          call ERROR('too many residues in sequence',' ',1)
        end if
        read (LINE(I:I),'(A)') SEQ
        if (SEQ .eq. '-' .or. SEQ .eq. '/') then
          N=N-1
        else
          ISEQ(N)=SEQ2I(SEQ)
        end if
      end do
      LENSEQ=N
C
      if (FEND) then
        return
      else
        goto 1
      end if
C
      return
C
2     close (UNIT=IO)
      LENSEQ=N
      return
C
9     call ERROR('error reading line:',LINE(1:20),1)
      return
      end
C
C ############################################################################
C
      subroutine RDSEQ2(IO,ISEQ,NRES)
C
      include '../joy/joy.h'
C
      character*14 CARD, TITLE
      integer IO, NRES, ISEQ(MAXRES)
C
2     read (IO,'(A)',end=1,err=901) CARD
      if (CARD(1:4) .eq. '>P1;') then
        read (CARD(5:),'(A)') TITLE
        read (IO,'(A)',end=3,err=901) CARD	! do nothing
        call RDSEQ(IO,ISEQ,NRES)
      end if
      goto 2
1     return
C
3     call ERROR('hydplot: error reading sequence',' ',1)
901   call ERROR('hydplot: error reading ',' ',1)
      end
