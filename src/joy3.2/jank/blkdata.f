      BLOCK data
C
      include 'jank.h'
C
      data (STDDIS(I),I=1,3)/1.32,1.47,1.53/
      data (STDANG(I),I=1,3)/114.,123.,109.47/
C
      data (RSNM(I),I=1,27)/'HIS','ASN','GLN','ALA','CYS','ASP',
     +'GLU','PHE','GLY','ILE','LYS','LEU','MET','PRO','ARG','SER',
     +'THR','VAL','TRP','TYR','HSX','ASX','GLX','UNK','PCA','INI',
     +'SEC'/
C
      data (NUMTOR(I),I=1,27)
     +/2,2,3,0,1,2,3,2,0,2,4,2,3,0,4,1,1,1,2,2,2,2,3,4*0/
C
      data (NMSDCH(I),I=1,27)/6,4,5,1,2,4,5,7,0,4,5,4,4,3,7,2,3,3,10,8,
     +6,4,5,1,4,5,4/
C
      data MAIN/' N  ',' CA ',' C  ',' O  '/
C
      data (ATONAM(1,I),I=1,6)/' CB ',' CG ',' ND1',' CD2',' CE1',
     +' NE2'/
      data(ATONAM(2,I),I=1,4)/' CB ',' CG ',' OD1',' ND2'/
      data(ATONAM(3,I),I=1,5)/' CB ',' CG ',' CD ',' OE1',' NE2'/
      data(ATONAM(4,I),I=1,1)/' CB '/
      data(ATONAM(5,I),I=1,2)/' CB ',' SG '/
      data(ATONAM(6,I),I=1,4)/' CB ',' CG ',' OD1',' OD2'/
      data(ATONAM(7,I),I=1,5)/' CB ',' CG ',' CD ',' OE1',' OE2'/
      data(ATONAM(8,I),I=1,7)/' CB ',' CG ',' CD1',' CD2',' CE1',
     +' CE2',' CZ '/
      data(ATONAM(10,I),I=1,4)/' CB ',' CG1',' CG2',' CD1'/
      data(ATONAM(11,I),I=1,5)/' CB ',' CG ',' CD ',' CE ',' NZ '/
      data(ATONAM(12,I),I=1,4)/' CB ',' CG ',' CD1',' CD2'/
      data(ATONAM(13,I),I=1,4)/' CB ',' CG ',' SD ',' CE '/
      data(ATONAM(14,I),I=1,3)/' CB ',' CG ',' CD '/
      data(ATONAM(15,I),I=1,7)/' CB ',' CG ',' CD ',' NE ',' CZ ',
     +' NH1',' NH2'/
      data(ATONAM(16,I),I=1,2)/' CB ',' OG '/
      data(ATONAM(17,I),I=1,3)/' CB ',' OG1',' CG2'/
      data(ATONAM(18,I),I=1,3)/' CB ',' CG1',' CG2'/
      data(ATONAM(19,I),I=1,10)/' CB ',' CG ',' CD1',' CD2',' NE1',
     +' CE2',' CE3',' CZ2',' CZ3',' CH2'/
      data(ATONAM(20,I),I=1,8)/' CB ',' CG ',' CD1',' CD2',' CE1',
     +' CE2',' CZ ',' OH '/
      data(ATONAM(21,I),I=1,6)/' CB ',' CG ',' AD1',' AD2',' AE1',
     +' AE2'/
      data(ATONAM(22,I),I=1,4)/' CB ',' CG ',' AD1',' AD2'/
      data(ATONAM(23,I),I=1,5)/' CB ',' CG ',' CD ',' AE1',' AE2'/
      data(ATONAM(24,I),I=1,1)/' CB '/
      data(ATONAM(25,I),I=1,4)/' CB ',' CG ',' CD ',' OE '/
      data(ATONAM(26,I),I=1,5)/' CB ',' CG ',' CD ',' CE ',' NZ '/
      data(ATONAM(27,I),I=1,4)/' CB ','SEG ',' OD1',' OD2'/
C
      end
