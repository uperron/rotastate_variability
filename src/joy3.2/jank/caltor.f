      subroutine caltor

c  may add checks of dihedrals:
c  ----------------------------
c
c  (Chi21 - Chi22) for Asn,Asp,Asx,His,Hix,Phe,Trp,Tyr
c  (Chi31 - Chi32) for Gln,Glu,Glx
c
c  within RINGS of His,Hix,Phe,Pro,Trp,Tyr

      include 'jank.h'

      character*5 mangls(7)
      logical atok(4),stok(4),gap
      dimension x4(3,4),y4(3,4),z4(3,4)

      crd=180./acos(-1.)

      do n=1,nres

        ires=iseq(n)
c ------------------------------------------- mainchain: OMEGA,PHI,PSI

        mang=0
        do i=1,7
          oppcok(i,n)=.false.
        enddo

        if (n.eq.1.and.n.ne.nres) then   ! 1ST residue (only PSI)

          gap=.false.
          do i=1,3
            atok(i)=atomok(i,1)
            do j=1,3
              x4(j,i)=xyz(j,i,1)  ! load N,Ca,C'
            enddo
          enddo
          atok(4)=atomok(1,2)
          do i=1,3
            x4(i,4)=xyz(i,1,2)    ! load N(+1)
          enddo

          if (atok(3).and.atok(4)) then
            cn=(x4(1,3)-x4(1,4))**2+(x4(2,3)-x4(2,4))**2+
     +         (x4(3,3)-x4(3,4))**2
            if (cn.le.sqgap) then
              if (atok(1).and.atok(2)) then
                call torch(x4,ang)
                oppchi(3,1)=ang
                oppcok(3,1)=.true.
              else
                mang=1
                mangls(mang)=' Psi '
              endif
            else
              gap=.true.
              write (*,100) resids(n),resids(n+1)
            endif
          else
            mang=1
            mangls(mang)=' Psi '
          endif

        else
          do ii=1,3
            do i=1,3
              atok(i)=atok(i+1)
              do j=1,3             ! load Ca(-1),C'(-1),N / C'(-1),N,Ca
                x4(j,i)=x4(j,i+1)  ! or N,Ca,C'
              enddo
            enddo

            if (ii.lt.3) then    ! OMEGA, PHI

              atok(4)=atomok(ii+1,n)
              do i=1,3
                x4(i,4)=xyz(i,ii+1,n)   ! load  Ca / C'
              enddo

              if (.not.gap) then
                if (atok(1).and.atok(2).and.atok(3).and.atok(4)) then
                  call torch(x4,ang)
                  oppchi(ii,n)=ang
                  oppcok(ii,n)=.true.

                  if (ii.eq.1) then
c ----------------------------------------- check omega
                    dang=ang*crd
                    aang=abs(dang)
                    if (aang.le.taudif.or.(180.-aang).le.taudif) then
                      if (aang.le.taudif) write (*,'(2a,1x,2a)')
     +                 'jank: peptide preceeding ',rsnm(ires),
     +                 resids(n),' is in cis-conformation'
                    else
                      write (*,'(4a,1x,2a,f6.1)')
     +                'jank: unusual omega between residues ',
     +                resids(n-1),' and ',rsnm(ires),resids(n),': ',dang
                    endif
c ----------------------------------------- check chirality at Ca-atom
                  else if (ii.eq.2.and.ires.ne.9.and.atomok(5,n)) then
                    do i=1,3
                      y4(i,4)=xyz(i,5,n)
                      do j=1,3
                        y4(j,i)=x4(j,i)
                      enddo
                    enddo
                    call torch(y4,ang1)
                    dang=(ang1-ang)*crd
                    if (abs(dang).gt.180.) dang=dang-
     +                                      sign(360.,dang)
                    if ((122.+dang).gt.taudif)
     +                write (*,'(2a,1x,2a,1x,f6.1,1x,a)')
     +               'jank: check chirality at CA ',rsnm(ires),
     +               resids(n),': (C-N-CA-CB)-Phi=',dang,'(expect -122)'
                  endif

                else
                  mang=mang+1
                  if (ii.eq.1) then
                    mangls(mang)='Omega'
                  else
                    mangls(mang)=' Phi '
                  endif
                endif
              endif

            else                ! PSI

              if (n.ne.nres) then

                gap=.false.
                atok(4)=atomok(1,n+1)
                do i=1,3
                  x4(i,4)=xyz(i,1,n+1)   ! N(+1)
                enddo

                if (atok(3).and.atok(4)) then
                  cn=(x4(1,3)-x4(1,4))**2+(x4(2,3)-x4(2,4))**2+
     +               (x4(3,3)-x4(3,4))**2
                  if (cn.le.sqgap) then
                    if (atok(1).and.atok(2)) then
                      call torch(x4,ang)
                      oppchi(3,n)=ang
                      oppcok(3,n)=.true.
                    else
                      mang=mang+1
                      mangls(mang)=' Psi '
                    endif
                  else
                    gap=.true.
                    write (*,100) resids(n),resids(n+1)
                  endif
                else
                  mang=mang+1
                  mangls(mang)=' Psi '
                endif

                if (.not.gap.and.(.not.atok(1).or..not.atok(2).or.
     +              .not.atok(3))) write (*,'(4a)')
     +            'jank: assume gap between residues ',resids(n-1),
     +            ' and ',resids(n+1)

              endif

            endif
          enddo

        endif

c -------------------------------------- check dihedral N-CA-C'=O
        if (n.ne.nres.and.oppcok(3,n).and.atomok(4,n)) then
          do i=1,3
            y4(i,4)=xyz(i,4,n)
            do j=1,3
              y4(j,i)=x4(j,i)
            enddo
          enddo
          call torch(y4,ang1)
          dang=(ang1-ang)*crd
          if (abs(dang).gt.180.) dang=dang-sign(360.,dang)
          if (abs(180.-abs(dang)).gt.taudif)
     +    write (*,'(2a,1x,2a,1x,f6.1,1x,a)') 'jank: check O(=C) of residue '
     +    ,rsnm(ires),resids(n),': (N-CA-C=O)-Psi=',dang,'(expect 180)'
        endif

c -------------------------------------------- sidechain dihedrals

        m=numtor(ires)
        if (m.gt.0) then

          stok(2)=atok(1)
          stok(3)=atok(2)
          stok(4)=atomok(5,n)
          do i=1,3
            do j=1,2
              y4(i,j+1)=x4(i,j)      ! load N,Ca
            enddo
            y4(i,4)=xyz(i,5,n)       ! load Cb
          enddo

          do ii=1,m        ! CHI1 ... CHIm

            do i=1,3
              stok(i)=stok(i+1)
              do j=1,3
                y4(j,i)=y4(j,i+1)
              enddo
            enddo

            if (ires.eq.10.and.ii.eq.2) then     ! Chi2 of Ile
              nat=8
            else
              nat=5+ii
            endif

            stok(4)=atomok(nat,n)
            do i=1,3
              y4(i,4)=xyz(i,nat,n)   ! load forth atom
            enddo

            if (stok(1).and.stok(2).and.stok(3).and.stok(4)) then
              call torch(y4,ang)
              oppchi(3+ii,n)=ang
              oppcok(3+ii,n)=.true.
            else
              mang=mang+1
              mangls(mang)(1:5)='Chi  '
              write (mangls(mang)(4:4),'(i1)') ii
            endif

c --------------- check chirality for Ile(Cb),Leu(Cg),Thr(Cb),Val(Cb)

            if ((ii.eq.1.and.(ires.eq.10.or.ires.eq.17.or.ires.eq.18))
     +      .or.(ii.eq.2.and.ires.eq.12).and.oppcok(3+ii,n)) then
              if (ii.eq.2) then
                nat=8  ! Leu
              else
                nat=7
              endif
              if (atomok(nat,n)) then
                do i=1,3
                  z4(i,4)=xyz(i,nat,n)
                  do j=1,3
                    z4(j,i)=y4(j,i)
                  enddo
                enddo
                call torch(z4,ang1)
                dang=crd*(ang1-ang)
                if (abs(dang).gt.180.) dang=dang-sign(360.,dang)
                if (ires.eq.10.or.ires.eq.17) then  ! Ile,Thr
                  aang=-120.
                else                                ! Val,Leu
                  aang=120.
                endif
                if (abs(aang-dang).gt.taudif)
     +           write (*,'(2a,1x,2a,2(i1,a),2(f5.0,a))')
     +           'jank: check chirality in     ',rsnm(ires),resids(n),
     +           ': Chi',ii,'2-Chi',ii,'1= ',dang,'     (expect',aang,')'
              endif
            endif

          enddo

c ---------------------------------- check Chi51,Chi52 for Arg
          if (ires.eq.15.and.oppcok(7,n).and.atomok(10,n).and.
     +     atomok(11,n)) then
            do i=1,3
              do j=1,3
                y4(j,i)=y4(j,i+1)
              enddo
            enddo
            do i=1,2
              ii=i+9
              do j=1,3
                y4(j,4)=xyz(j,ii,n)
              enddo
              call torch(y4,ang)
              if (i.eq.1) then
                ang1=ang*crd
              else
                ang=ang*crd

                if (abs(ang1).le.taudif) then
                  dang=ang-180.
                  if (dang.lt.-180.) dang=dang+360.
                  if (abs(dang).gt.taudif) write (*,'(3a,2(f5.0,a))')
     +              'jank: unusual Chi51/Chi52 in ARG ',resids(n),
     +              ': ',ang1,'/',ang,'            (expect 0/180)'
                else
                  if (abs(ang).le.taudif) then
                    dang=ang1-180.
                    if (dang.lt.-180.) dang=dang+360.
                    if (abs(dang).gt.taudif) then
                      write (*,'(3a,2(f5.0,a))')
     +                'jank: unusual Chi51/Chi52 in ARG ',resids(n),
     +                ': ',ang1,'/',ang,'            (expect 0/180)'
                    else
                      write (*,'(3a,2(f5.0,a))')
     +                'jank: swapped NH1 & NH2 in   ARG ',resids(n),
     +                ': Chi51/Chi52=',ang1,'/',ang,'(expect 0/180)'
                    endif
                  else
                    write (*,'(3a,2(f5.0,a))')
     +              'jank: unusual Chi51/Chi52 in ARG ',resids(n),
     +              ': ',ang1,'/',ang,'            (expect 0/180)'
                  endif
                endif

              endif
            enddo
          endif

        endif

        if (mang.gt.0) write (*,200) rsnm(ires),resids(n),
     +                               (mangls(i),i=1,mang)

      enddo

  100 format ('jank: gap between residues ',a,' and ',a,
     +        ' - cannot calculate Psi/Omega/Phi')
  200 format ('jank: ',a,1x,a,
     +' - due to missing atom(s) cannot calculate:',/,8x,7(1x,a))

      return
      end
