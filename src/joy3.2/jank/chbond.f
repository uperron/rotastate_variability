      subroutine chbond

      include 'jank.h'

      logical atok1,atok2,atok3,gap
      character*4 dist(3)
      character*6 angl(3)
      dimension x(3),y(3),z(3),c(3)
      data (dist(i),i=1,3)/' C-N','N-CA','CA-C'/
      data (angl(i),i=1,3)/'CA-C-N','C-N-CA','N-CA-C'/

      crd=180./acos(-1.)

      do n=1,nres

        ires=iseq(n)

        if (n.eq.1) then   ! 1ST residue

          atok1=atomok(1,1)
          atok2=atomok(2,1)
          atok3=atomok(3,1)

          if (atok1.and.atok2) then
            do i=1,3
              x(i)=xyz(i,1,1)-xyz(i,2,1)  ! N <- Ca
            enddo
            dx=sqrt(x(1)**2+x(2)**2+x(3)**2)

            if (abs(dx-stddis(2)).gt.disdif)
     #       write (*,'(5a,2(f5.2,a))')
     #        'jank: check bond length ',dist(2),' in residue ',
     #        resids(n),': ',dx,' (expect ',stddis(2),')'

          endif

          if (atok3) then
            do i=1,3
              c(i)=xyz(i,3,1)
            enddo
            if (atok2) then
              do i=1,3
                y(i)=c(i)-xyz(i,2,1)        ! C'<- Ca
              enddo
              dy=sqrt(y(1)**2+y(2)**2+y(3)**2)

              if (abs(dy-stddis(3)).gt.disdif)
     #         write (*,'(5a,2(f5.2,a))')
     #         'jank: check bond length ',dist(3),' in residue',
     #         resids(n),': ',dy,' (expect ',stddis(3),')'

              if (atok1) then
                ang=acos((x(1)*y(1)+x(2)*y(2)+x(3)*y(3))/(dx*dy))*crd

                if (abs(ang-stdang(3)).gt.angdif)
     #           write (*,'(5a,2(f5.1,a))')
     #         'jank: check bond angle ',angl(3),' in residue ',
     #           resids(n),': ',ang,' (expect ',stdang(3),')'

              endif
            endif
          endif

        else

          gap=.false.

          do ii=1,3

            atok1=atok2
            atok2=atok3
            atok3=atomok(ii,n)

            if (atok3) then
              do i=1,3
                z(i)=c(i)         ! C'(-1),N ,CA
                c(i)=xyz(i,ii,n)  ! N     ,CA,C'
              enddo
              if (atok2) then
                do i=1,3
                  x(i)=-y(i)
                  y(i)=c(i)-z(i)
                enddo
                dx=dy
                dy=y(1)**2+y(2)**2+y(3)**2

                if (ii.eq.1.and.dy.gt.sqgap) gap=.true.

                if ((ii.eq.1.and..not.gap).or.ii.ne.1) then
                  dy=sqrt(dy)

                  if (abs(dy-stddis(ii)).gt.disdif)
     #             write (*,'(4a,1x,2a,2(f5.2,a))')
     #           'jank: check bond length ',dist(ii),' in ',
     #           rsnm(ires),resids(n),': ',dy,' (expect ',stddis(ii),')'

                endif

                if (atok1.and.(((ii.eq.1.or.ii.eq.2).and..not.gap)
     #                       .or.ii.eq.3)) then
                  ang=acos((x(1)*y(1)+x(2)*y(2)+x(3)*y(3))/(dx*dy))*crd

                  if (abs(ang-stdang(ii)).gt.angdif) then
                    if (ii.eq.1) then
                      na=n-1
                    else
                      na=n
                    endif

                    write (*,'(5a,2(f5.1,a))')
     #             'jank: check bond angle ',angl(ii),' in residue ',
     #              resids(na),': ',ang,' (expect ',stdang(ii),')'

                  endif
                endif
              endif
            endif

          enddo

        endif
      enddo

      return
      end
