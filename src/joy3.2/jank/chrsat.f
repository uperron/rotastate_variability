      subroutine chrsat(resid,seq,atnams,altids,x,atmok,ires,nat)

      include 'jank.h'

      parameter (mxatm=100)

      character altids(1),altid(1),alt
      character*3 seq
      character*4 matoms(maxatm),atnams(1),atnam
      character*5 resid
      logical ignat,atmok(1)
      dimension xx(3,mxatm),x(3,1)

c -------------------------------------- check residue-name
      do i=1,24   !!!
        if (seq.eq.rsnm(i)) then
          ires=i
          goto 1
        endif
      enddo

      if (seq.eq.'ACE'.or.seq.eq.'FOR') then
        write (*,'(2a,1x,a)') 'jank: ignoring ',seq,resid
        nat=0
        return
      else
        ires=24  !!!
        write (*,'(2a,1x,2a)') 'jank: treat ',seq,resid,
     #                         ' as UNK'
      endif

c ------------------------- look for alternative atom-locations
    1 ialt=0
      do 2 i=1,nat
        alt=altids(i)
        if (alt.ne.' ') then
          do j=1,ialt
            if (alt.eq.altid(j)) goto 2
          enddo
          ialt=ialt+1
          altid(ialt)=alt
        endif
    2 continue
      if (ialt.eq.0) then
        alt=' '
      else
c -------------------------- take only 1st alternative location ...
        alt=altid(1)
        write (*,'(3a)') 'jank: residue ',resid,
     #   ' : alternative location of atom(s) - take only 1st one'
      endif

c -------------------------- ... and get rid of hydrogens & 'OXT'
      n=0
      do i=1,nat
        atnam=atnams(i)

        if (atnam(2:2).ne.'H'.and.atnam(2:2).ne.'D'.and.atnam
     *     .ne.' OXT') then

          if (altids(i).eq.' '.or.altids(i).eq.alt) then
            n=n+1
            atnams(n)=atnam
            do j=1,3
              xx(j,n)=x(j,i)
            enddo
          endif
        endif
      enddo

      matm=0
c --------------------------- check backbone
      do 3 i=1,4
        atnam=main(i)
        do j=1,n
          if (atnam.eq.atnams(j)) then
            do k=1,3
              x(k,i)=xx(k,j)
            enddo
            atmok(i)=.true.
            goto 3
          endif
        enddo
c ---------------------------- missing atom
        atmok(i)=.false.
        matm=matm+1
        matoms(matm)=atnam
    3 continue

c ------------------------------- check sidechain

      if (ires.lt.4) then
        do i=1,n
          if (atnams(i)(2:2).eq.'A') then
            ires=ires+20

            write (*,'(2a,1x,4a)') 'jank: consider ',seq,resid,
     #      ' as ',rsnm(ires),' due to undefined atom name(s)'

            goto 4
          endif
        enddo
      endif

    4 nm=nmsdch(ires)
      do 5 i=1,nm
        ii=i+4
        atnam=atonam(ires,i)
        do j=1,n
          if (atnam.eq.atnams(j)) then
            do k=1,3
              x(k,ii)=xx(k,j)
            enddo
            atmok(ii)=.true.
            goto 5
          endif
        enddo
c -------------------------------- missing atom
        atmok(ii)=.false.
        matm=matm+1
        matoms(matm)=atnam

    5 continue

      nat=nm+4

      if (matm.gt.0) write (*,'(2a,1x,2a,14(1x,a))')
     #'jank: missing atom(s) in ',rsnm(ires),resid,' :',
     #(matoms(i),i=1,matm)

      return
      end
