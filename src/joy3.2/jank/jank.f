      program FRANK
C
      include 'jank.h'
      character*14 NAME
C
      if (IARGC() .eq. 1) then
        call GETARG(1,NAME)
      else
        write (6,'(''jank: called with incorrect parameters'')')
        call EXIT(1)
      end if
C
      call redfil(NAME)
C
      if (NRES .eq. 0) then
        write (*,'(''jank: no residues found in '',A)') NAME
      else
        call chbond()
        call caltor()
      endif
C
      call EXIT(0)
      end
