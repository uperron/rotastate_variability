      parameter (MAXRES=600, MAXATM=14)
C
      character*1 CHAIN
      character*4 CODE
      character*5 RESIDS
      logical ATOMOK
      common /PROT/ XYZ(3,MAXATM,MAXRES),ATOMOK(MAXATM,MAXRES),
     +              ISEQ(MAXRES),RESIDS(MAXRES),NRES,CODE,CHAIN
C
      character*3 RSNM
      character*4 MAIN, ATONAM
      common /STAND/ RSNM(27),NMSDCH(27),NUMTOR(27),MAIN(4),
     +               ATONAM(27,10),STDDIS(3),STDANG(3)
C
      parameter (SQGAP=6.25, TAUDIF=25., ANGDIF=10., DISDIF=.1)
C
      logical OPPCOK
      common /TORS/ OPPCHI(7,MAXRES),OPPCOK(7,MAXRES)
