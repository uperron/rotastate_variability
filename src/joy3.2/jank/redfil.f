      subroutine redfil(NAME)

      include 'jank.h'
      character*(*) NAME

      parameter (mxatm=100)

      character altids(mxatm)
      character*100 line
      character*5 resold
      character*4 atnams(mxatm)
      character*3 seq, NAMOLD
      logical atmok(mxatm)
      dimension x(3,mxatm)

      open(unit=10,file=NAME,status='old',err=901)
C
C initialize RESOLD, and NAMOLD
C
      resold='#####'
      namold='###'
      nres=0

    1 read (10,'(i,a)',end=2) l,line(1:l)
C
C check on 54 is that coordinates are present (l = length of line)
C
      if (l.ge.54.and.line(1:6).eq.'ATOM  ') then

        if (line(23:27) .ne. RESOLD) then
          if (nres.gt.0) then
            call chrsat(resold,seq,atnams,altids,x,atmok,ires,nat)
            if (nat.gt.0) then
              if (nat.gt.maxatm) then
                write (*,'(2a)') 'jank: too many atoms in residue '
     #                          ,resold
                close(10)
                call EXIT(1)
              else
                resids(nres)=resold
                iseq(nres)=ires
                do i=1,nat
                  atomok(i,nres)=atmok(i)
                  do j=1,3
                    xyz(j,i,nres)=x(j,i)
                  enddo
                enddo
              endif
            else
              nres=nres-1
            endif
          endif

          resold=line(23:27)
          seq=line(18:20)
          NAMOLD=LINE(18:20)
          nres=nres+1
          if (nres .gt. maxres)  then
            write (*,'(a)') 'jank: too many residues'
            close (10)
            stop
          endif
          nat=0
        endif
C
C check to see if sequence is different but name the same (surprisingly
C common error in PDB files
C 
C       if (line(18:20) .ne. NAMOLD)) then
C
        nat=nat+1
        if (nat .gt. mxatm) then
          write (*,'(2a)') 'jank: too many atoms in residue '
     #                    ,resold
          close(10)
          stop
        endif

        atnams(nat)=line(13:16)
        altids(nat)=line(17:17)
        read (line(31:54),'(3f8.3)') (x(i,nat),i=1,3)

      endif
      goto 1

    2 close(10)
      if (nres.gt.0) then
        call chrsat(resold,seq,atnams,altids,x,atmok,ires,nat)
        if (nat.gt.0) then
          if (nat.gt.maxatm) then
            write (*,'(2a)') 'jank: too many atoms in residue '
     #                      ,resold
            stop
          else
            resids(nres)=resold
            iseq(nres)=ires
            do i=1,nat
              atomok(i,nres)=atmok(i)
              do j=1,3
                xyz(j,i,nres)=x(j,i)
              enddo
            enddo
          endif
        else
          nres=nres-1
        endif
      endif

      return
C
901   write (6,'(''jank: cannot open file '',A)') NAME
      call EXIT(1)
C
      end
