      subroutine torch(x,ang)
C
C calculates a torsion angle
C
      real x(3,4)
C
      dx1=x(1,2)-x(1,1)
      dy1=x(2,2)-x(2,1)
      dz1=x(3,2)-x(3,1)
      dx2=x(1,3)-x(1,2)
      dy2=x(2,3)-x(2,2)
      dz2=x(3,3)-x(3,2)
C
      ux1=dy1*dz2-dz1*dy2
      uy1=dz1*dx2-dx1*dz2
      uz1=dx1*dy2-dy1*dx2
C
      dx1=x(1,4)-x(1,3)
      dy1=x(2,4)-x(2,3)
      dz1=x(3,4)-x(3,3)
C
      ux2=dz1*dy2-dy1*dz2
      uy2=dx1*dz2-dz1*dx2
      uz2=dy1*dx2-dx1*dy2
C
      dx1=ux1*ux2+uy1*uy2+uz1*uz2
      dy1=(ux1*ux1+uy1*uy1+uz1*uz1)*(ux2*ux2+uy2*uy2+uz2*uz2)
C
      if (dy1 .ne. 0.) then
        dx1=dx1/sqrt(dy1)
        dx1=amax1(dx1,-1.)
        dx1=amin1(dx1,1.)
        if (dx1.gt..99999) then
          r1=1./sqrt(ux1*ux1+uy1*uy1+uz1*uz1)
          r2=1./sqrt(ux2*ux2+uy2*uy2+uz2*uz2)
          c=(ux1*r1-ux2*r2)**2+(uy1*r1-uy2*r2)**2+(uz1*r1-uz2*r2)**2
          ang=sqrt(c+c*c/12.+c*c*c/72.)
        else
          ang=acos(dx1)
        endif
        if (ux1*(uy2*dz2-uz2*dy2)+uy1*(uz2*dx2-ux2*dz2)+uz1*(ux2*dy2
     *          -uy2*dx2).lt.0.) ang=-ang
        return
      else
        write (6,'(''jank: error in call to torch'')')
        call EXIT(1)
      endif
C
      end
