      subroutine ANALSEQ(LEN,RDISC,STRNAM,LENSTRNAM,NENVIR)
C
C copyright (c) 1988-1991 John Overington
C part of joy
C
C ----------------------------------------------------------------------------
C
C Counts the occurence of features per residue per sequence.
C
C Key to indices is :
C
C 1  total        
C 2  alpha       
C 3  beta        
C 4  + phi       
C 5  coil        
C 6  inaccessible
C 7  accessible   
C 8  H-bond side  
C 9  H-bond main O
C 10 H-bond main N
C 11 H-bond HET   
C 12 cis peptide 
C
C added small code to total charges for protein, assume that E and D are -1,
C R and K are +1, N- and C-termini are +1 and -1 respectively, (they thus
C cancel completely), then also report a nominal total charge
C
C ----------------------------------------------------------------------------
C
      include 'joy.h'
C
      character*(*) STRNAM
      integer LENRULER
      parameter (LENRULER=69)
      character*(LENRULER) RULER
      integer RDISC(NUMFEAT,MAXRES), LENSTRNAM, NRES(NUMEXAM,21)
      integer NRESTOT(NUMEXAM), NENVIR(NUMEXAM,21), I, J, LEN, K
C
      integer NPOSIT, NNEGAT
C
      do I=1,LENRULER
        RULER(I:I)='-'
      end do
C
C Initialize counters 
C
      do I=1,21
        do J=1,NUMEXAM
          NRES(J,I)=0
        end do
      end do
      do J=1,NUMEXAM
        NRESTOT(J)=0
      end do
C
      NPOSIT=0
      NNEGAT=0
C
C ----------------------------------------------------------------------------
C
C
      do I=1,LEN
        NRES(1,RDISC(1,I))=NRES(1,RDISC(1,I))+1
        if (RDISC(2,I).eq.1) then
          NRES(2,RDISC(1,I))=NRES(2,RDISC(1,I))+1
          NRESTOT(2)=NRESTOT(2)+1
        else if (RDISC(2,I).eq.2) then
          NRES(3,RDISC(1,I))=NRES(3,RDISC(1,I))+1
          NRESTOT(3)=NRESTOT(3)+1
        else if (RDISC(2,I).eq.3) then
          NRES(4,RDISC(1,I))=NRES(4,RDISC(1,I))+1
          NRESTOT(4)=NRESTOT(4)+1
        else if (RDISC(2,I).eq.4) then
          NRES(5,RDISC(1,I))=NRES(5,RDISC(1,I))+1
          NRESTOT(5)=NRESTOT(5)+1
        end if
        if (RDISC(3,I).eq.2) then
          NRES(6,RDISC(1,I))=NRES(6,RDISC(1,I))+1
          NRESTOT(6)=NRESTOT(6)+1
        else
          NRES(7,RDISC(1,I))=NRES(7,RDISC(1,I))+1
          NRESTOT(7)=NRESTOT(7)+1
        end if
        if (RDISC(4,I).eq.1) then
          NRES(8,RDISC(1,I))=NRES(8,RDISC(1,I))+1
          NRESTOT(8)=NRESTOT(8)+1
        end if
        if (RDISC(5,I).eq.1) then
          NRES(9,RDISC(1,I))=NRES(9,RDISC(1,I))+1
          NRESTOT(9)=NRESTOT(9)+1
        end if
        if (RDISC(6,I).eq.1) then
          NRES(10,RDISC(1,I))=NRES(10,RDISC(1,I))+1
          NRESTOT(10)=NRESTOT(10)+1
        end if
        if (RDISC(7,I).eq.1) then
          NRES(11,RDISC(1,I))=NRES(11,RDISC(1,I))+1
          NRESTOT(11)=NRESTOT(11)+1
        end if
        if (RDISC(8,I).eq.1) then
          NRES(12,RDISC(1,I))=NRES(12,RDISC(1,I))+1
          NRESTOT(12)=NRESTOT(12)+1
        end if
      end do
      NRESTOT(1)=LEN
C
C ----------------------------------------------------------------------------
C
C get charge of proteins
C
      NPOSIT=NRES(1,9)+NRES(1,15)
      NNEGAT=NRES(1,3)+NRES(1,4)
C
C ----------------------------------------------------------------------------
C
C Output of sequence composition statistics
C
      write (STDOUT,'(/,''structure '',A)') STRNAM(1:LENSTRNAM)
      write (STDOUT,'(12X,''A  C  D  E  F  G  H  I  K  L  M  N'',
     +                    ''  P  Q  R  S  T  V  W  Y  J total'')')
      write (STDOUT,'(10X,A)') RULER
      write (STDOUT,'(''total     '',21I3,I6,3X,A)')
     +       (NRES(1,K),K=1,21),NRESTOT(1),STRNAM(1:LENSTRNAM)
      write (STDOUT,'(10X,A)') RULER
      write (STDOUT,'(''alpha     '',21I3,I6,3X,A)')
     +       (NRES(2,K),K=1,21),NRESTOT(2),STRNAM(1:LENSTRNAM)
      write (STDOUT,'(''beta      '',21I3,I6,3X,A)')
     +       (NRES(3,K),K=1,21),NRESTOT(3),STRNAM(1:LENSTRNAM)
      write (STDOUT,'(''+ phi     '',21I3,I6,3X,A)')
     +       (NRES(4,K),K=1,21),NRESTOT(4),STRNAM(1:LENSTRNAM)
      write (STDOUT,'(''coil      '',21I3,I6,3X,A)')
     +       (NRES(5,K),K=1,21),NRESTOT(5),STRNAM(1:LENSTRNAM)
      write (STDOUT,'(''cis       '',21I3,I6,3X,A)')
     +       (NRES(12,K),K=1,21),NRESTOT(12),STRNAM(1:LENSTRNAM)
      write (STDOUT,'(10X,A)') RULER
      write (STDOUT,'(''inaccess. '',21I3,I6,3X,A)')
     +       (NRES(6,K),K=1,21),NRESTOT(6),STRNAM(1:LENSTRNAM)
      write (STDOUT,'(''access.   '',21I3,I6,3X,A)')
     +       (NRES(7,K),K=1,21),NRESTOT(7),STRNAM(1:LENSTRNAM)
      write (STDOUT,'(10X,A)') RULER
      write (STDOUT,'(''H-b side  '',21I3,I6,3X,A)')
     +       (NRES(8,K),K=1,21),NRESTOT(8),STRNAM(1:LENSTRNAM)
      write (STDOUT,'(''H-b main O'',21I3,I6,3X,A)')
     +       (NRES(9,K),K=1,21),NRESTOT(9),STRNAM(1:LENSTRNAM)
      write (STDOUT,'(''H-b main N'',21I3,I6,3X,A)')
     +       (NRES(10,K),K=1,21),NRESTOT(10),STRNAM(1:LENSTRNAM)
      write (STDOUT,'(''H-b HET   '',21I3,I6,3X,A)')
     +       (NRES(11,K),K=1,21),NRESTOT(11),STRNAM(1:LENSTRNAM)
C
C ----------------------------------------------------------------------------
C
      write (STDOUT,'(/,''           +    -  total'')') 
      write (STDOUT,'(''charge: '',3I5,2X,F7.3,2X,F7.3,3X,A)') NPOSIT,-1*NNEGAT,
     +       NPOSIT-NNEGAT,real(NPOSIT+NNEGAT)/real(NRESTOT(1)),
     +       real(NPOSIT-NNEGAT)/real(NRESTOT(1)),STRNAM(1:LENSTRNAM)
      write (STDOUT,'(/)') 
C
C ----------------------------------------------------------------------------
C
C Increment the total environment counter NENVIR
C
      do K=1,21
        NENVIR(1,K)=NENVIR(1,K)+NRES(1,K)
        NENVIR(2,K)=NENVIR(2,K)+NRES(2,K)
        NENVIR(3,K)=NENVIR(3,K)+NRES(3,K)
        NENVIR(4,K)=NENVIR(4,K)+NRES(4,K)
        NENVIR(5,K)=NENVIR(5,K)+NRES(5,K)
        NENVIR(6,K)=NENVIR(6,K)+NRES(6,K)
        NENVIR(7,K)=NENVIR(7,K)+NRES(7,K)
        NENVIR(8,K)=NENVIR(8,K)+NRES(8,K)
        NENVIR(9,K)=NENVIR(9,K)+NRES(9,K)
        NENVIR(10,K)=NENVIR(10,K)+NRES(10,K)
        NENVIR(11,K)=NENVIR(11,K)+NRES(11,K)
        NENVIR(12,K)=NENVIR(12,K)+NRES(12,K)
      end do
C
      return
      end
C
C ############################################################################
C
      subroutine ANALALI(NENVIR,NSTR,NAVELEN)
C
C Prints out totals from ANALSEQ for entire alignment
C
      include 'joy.h'
C
      integer LENRULER
      parameter (LENRULER=69)
      character*(LENRULER) RULER
      integer NENVIR(NUMEXAM,21), NTOT(NUMEXAM), I, J, K, NSTR
      integer NAVELEN
C
      do I=1,LENRULER
        RULER(I:I)='-'
      end do
C
      do I=1,NUMEXAM
        NTOT(I)=0
        do J=1,21
          NTOT(I)=NTOT(I)+NENVIR(I,J)
        end do
      end do
C
C Output of alignment composition statistics
C
      write (STDOUT,'(/,''total alignment '')') 
      write (STDOUT,'(12X,''A  C  D  E  F  G  H  I  K  L  M'',
     +                    ''  N  P  Q  R  S  T  V  W  Y  J total'')')
      write (STDOUT,'(10X,A)') RULER
      write (STDOUT,'(''total     '',21I3,I6)')
     +       (NENVIR(1,K),K=1,21),NTOT(1)
      write (STDOUT,'(10X,A)') RULER
      write (STDOUT,'(''alpha     '',21I3,I6)')
     +       (NENVIR(2,K),K=1,21),NTOT(2)
      write (STDOUT,'(''beta      '',21I3,I6)')
     +       (NENVIR(3,K),K=1,21),NTOT(3)
      write (STDOUT,'(''+ phi     '',21I3,I6)')
     +       (NENVIR(4,K),K=1,21),NTOT(4)
      write (STDOUT,'(''coil      '',21I3,I6)')
     +       (NENVIR(5,K),K=1,21),NTOT(5)
      write (STDOUT,'(''cis       '',21I3,I6)')
     +       (NENVIR(12,K),K=1,21),NTOT(12)
      write (STDOUT,'(10X,A)') RULER
      write (STDOUT,'(''inacc.    '',21I3,I6)')
     +       (NENVIR(6,K),K=1,21),NTOT(6)
      write (STDOUT,'(''access.   '',21I3,I6)')
     +       (NENVIR(7,K),K=1,21),NTOT(7)
      write (STDOUT,'(10X,A)') RULER
      write (STDOUT,'(''H-b side  '',21I3,I6)')
     +       (NENVIR(8,K),K=1,21),NTOT(8)
      write (STDOUT,'(''H-b main O'',21I3,I6)')
     +       (NENVIR(9,K),K=1,21),NTOT(9)
      write (STDOUT,'(''H-b main N'',21I3,I6)')
     +       (NENVIR(10,K),K=1,21),NTOT(10)
      write (STDOUT,'(''H-b HET   '',21I3,I6)')
     +       (NENVIR(11,K),K=1,21),NTOT(11)
C     
      NAVELEN=NTOT(1)/NSTR
C
      return
      end
