      subroutine FINDCAP(FILE,SEQ,NRES,NSEQ)
C
C subroutine to search an alignment and find possible N-cap residues
C
      include 'joy.h'
      character*(*) FILE
      character*1 SEQ(MAXRES,MAXSEQ), STATE(MAXRES), FFTHEL(MAXRES)
      integer NRES, NSEQ, NPOSS, I, J, BEGHEL(MAXHELIX), ENDHEL(MAXHELIX), NHEL
      integer K
      logical ISHYDPHB, NCAPRES, CAPPOSS(MAXRES), INHELIX
      real VAL(MAXRES)
C
      do I=1,NRES
        CAPPOSS(I)=.false.
        FFTHEL(I)='-'
      end do
C
C open prediction file
C
      open (unit=20,status='unknown',form='formatted',
     -      file=FILE(1:index(FILE,'.'))//'prd',err=901)
C
C find the N-cap positions
C
      do I=1,NRES
        STATE(I)='-'
C
C 1) Class 1 -- ampipathic, NSDT cap, hydrophobic at i+4
C define these as positions where the N-cap residues make up half or more 
C of the possible residues (very preliminary)
C
        NPOSS=0
        do J=1,NSEQ
          if (NCAPRES(SEQ(I,J))) then
            NPOSS=NPOSS+1
          end if 
        end do
        if (NPOSS.gt.(NSEQ/2)) then
          NPOSS=0
          do J=1,NSEQ
            if (ISHYDPHB(SEQ(I+4,J))) then
              NPOSS=NPOSS+1
            end if 
          end do
          if (NPOSS.gt.(NSEQ/2)) then
            write (20,'(''possible N-cap at position i = '',I3,
     -               '' and hydrophobic at i+4 !!'')') I
          else
            write (20,'(''possible N-cap at position i = '',I3)') I
          end if
          CAPPOSS(I)=.true.
        end if
C
C Class 2) -- proline at N-terminus
C
        NPOSS=0
        do J=1,NSEQ
          if (SEQ(I,J).eq.'P') then
            NPOSS=NPOSS+1
          end if 
        end do
        if (NPOSS.gt.(NSEQ/2)) then
          NPOSS=0
          do J=1,NSEQ
            if (ISHYDPHB(SEQ(I+4,J))) then
              NPOSS=NPOSS+1
            end if 
          end do
          if (NPOSS.gt.(NSEQ/2)) then
            write (20,'(''possible proline-cap at position i = '',I3,
     -                 '' and hydrophobic at I+4 !!'')') I
          else
            write (20,'(''possible proline-cap at position i = '',I3)') I
          end if
          CAPPOSS(I)=.true.
        end if
C
C class 3) -- glycine capped C-terminus, no hydrophobic required !!
C
        NPOSS=0
        do J=1,NSEQ
          if (SEQ(I,J).eq.'G') then
            NPOSS=NPOSS+1
          end if 
        end do
        if (NPOSS.gt.(NSEQ/2)) then
          NPOSS=0
          write (20,'(''possible glycine-cap at position i = '',I3)') I
          CAPPOSS(I)=.true.
        end if
      end do
C
C use helix prediciton data
C
      write (20,'(/,''reading in Fourier conservation data'',/)')
      call RDFFT(FILE,NRES,VAL)
C
C Scan list for helical segments
C
      NHEL=0
      do I=2,NRES
        if (VAL(I).gt.2.0.and.VAL(I-1).le.2.0) then
          BEGHEL(NHEL+1)=I
          NHEL=NHEL+1
        end if
        if (VAL(I).le.2.0.and.VAL(I-1).gt.2.0) then
          ENDHEL(NHEL)=I
        end if
      end do
C
      write (20,'(''found '',I3,'' helical segments'')') NHEL
C
C scan backwards from center of element (10 residues at most)
C
      if (NHEL.lt.1) then
        close (unit=20)
        return
      end if
C
C set up Fourier predicted helices
C
      do I=1,NHEL
        do J=BEGHEL(I),ENDHEL(I)
          FFTHEL(J)='H'
        end do
      end do
C
      do I=1,NHEL
C
        write (6,*) 'helix ',i,beghel(i),endhel(i)
C
C First of all the N-terminal caps
C
        do J=((ENDHEL(I)-BEGHEL(I))/2)+BEGHEL(I), ((ENDHEL(I)-BEGHEL(I))/2)+BEGHEL(I)-10, -1
          write (6,*) J,I,'   N '
          if (J.lt.1) goto 1
          if (CAPPOSS(J)) then
            write (6,*) 'Cap found and helical periodicity at position ', J
            do K=J,((ENDHEL(I)-BEGHEL(I))/2)+BEGHEL(I)
              STATE(K)='H'
            end do
          end if
        end do
1       continue
C
C Now the C-terminal caps
C
        do J=((ENDHEL(I)-BEGHEL(I))/2)+BEGHEL(I), ((ENDHEL(I)-BEGHEL(I))/2)+BEGHEL(I)+10
          if (J.gt.NRES) goto 2
          if (CAPPOSS(J)) then
            write (20,*) 'Cap found and helical periodicity at position ', J
            do K=J,((ENDHEL(I)-BEGHEL(I))/2)+BEGHEL(I),-1
              STATE(K)='H'
            end do
          end if
        end do
2       continue
C
      end do
C
      write (20,'(''prediction of helix extents '')')
      do I=1,NRES
        write (20,'(I3,1X,A,1X,A)') I, STATE(I),FFTHEL(I)
      end do
      close (unit=20)
C
      return
C
901   call ERROR('joy: cannot open prediction file',' ',1)
C
      end
C
C ==============================================================================
C
      logical function NCAPRES(RES)
C
      character*1 RES, RESID(4) 
      integer NRES
C
      data RESID /'N','D','S','T'/
      data NRES /4/
C
      NCAPRES=.false.
C
      do I=1,NRES
        if (RES.eq.RESID(I)) then
          NCAPRES=.true.
        end if
      end do
C
      return
      end
C
C ========
C
      logical function ISHYDPHB(RES)
C
      character*1 RES, RESID(6) 
      integer NRES
C
      data RESID /'A','F','I','L','M','V'/
      data NRES /6/
C
      ISHYDPHB=.false.
C
      do I=1,NRES
        if (RES.eq.RESID(I)) then
          ISHYDPHB=.true.
        end if
      end do
C
      return
      end
C
C ###########################################################################
C
      subroutine RDFFT(FILE,NRES,VAL)
C
C reads in file containing helix prediction by fourier transform of conservation
C
      character*(*) FILE
      integer NRES, N, I
      real VAL(MAXRES)
C
      open (file=FILE,unit=14,status='old',err=999,form='formatted')
C
      N=1
1     read (14,'(I6,F5.2)',end=2,err=998) I, VAL(N)
      N=N+1
      goto 1
2     close (unit=14)
C
      if (N-1.ne.NRES) then
        call ERROR('joy: bad extent of Fourier file',FILE,1)
      end if
C
      return
998   write (6,*) N, I, VAL(N)
      call ERROR('joy: bad format of Fourier file',FILE,1)
999   call ERROR('joy: cannot open Fourier file',FILE,1)
C
      end
