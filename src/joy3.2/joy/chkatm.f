      program CHKATM
C
C copyright (c) 1988-1991 John Overington
C part of joy
C
C Checks the atom names in a file, to see if they are standard PDB
C
C If files on command line then process these
C
      parameter (MAXERR=25, NATOMS=41)
      character*80 card, FILENAME
      character*3 ATOMS(NATOMS)
      integer NARG, IIN, NREC, NERR
      logical FOUND
C
      data ATOMS /'N  ','CA ','C  ','O  ','AD1',
     +            'AD2','AE1','AE2','CB ','CD ',
     -            'CD1','CD2','CE ','CE1','CE2',
     +            'CE3','CG ','CG1','CG2','CH2',
     -            'CZ ','CZ2','CZ3','ND1','ND2',
     +            'NE ','NE1','NE2','NH1','NH2',
     -            'NZ ','OD1','OD2','OE1','OE2',
     +            'OG ','OG1','OH ','SD ','SG ',
     -            'OXT'/
C
      narg=iargc()
      if (NARG.gt.0) then
        IIN=1
        call GETARG(1,FILENAME)
        open (file=FILENAME,unit=1,status='old',form='formatted',err=3)
      else
        IIN=5
      end if
C
      NREC=0
      NERR=0
1     read (IIN,'(A)',err=2,end=9) card
      NREC=NREC+1
      if (CARD(1:6).eq.'ATOM  ') then
        found=.false.
        do I=1,NATOMS
          if (CARD(14:16).eq.ATOMS(I)) then
            found=.true.
            goto 5
          end if
        end do
5       continue
        if (.not.FOUND) then
          write (6,'(''chkatm: unknown atom type '',A,'' on line '',
     +           I4)') CARD(14:16),NREC
          NERR=NERR+1
          if (NERR.gt.MAXERR) then
            write (6,'(''chkatm: too many errors !'')')
            call EXIT(1)
          end if
        end if
      end if
      goto 1
9     continue
      if (IIN.eq.1) then
        close (unit=1)
      end if
C
      call EXIT(1)
C
2     write (6,'(''chkatm: error reading input file'')')
      call EXIT(1)
3     write (6,'(''chkatm: error opening input file'')')
      call EXIT(1)
C
      end
