      program CHKLEN
C
C copyright (c) 1988-1991 John Overington
C part of joy
C
C Checks the length of a residue in a PDB format atom file
C
      parameter (MAXERR=25, NRES=21, MAXATS=14)
      character*80 CARD, FILENAME, BUFFER(MAXATS)
      character*6 NEWRES, OLDRES
      character*3 RES(NRES), ATM(MAXATS,NRES)
      integer NARG, IIN, NERR, LENRES(NRES)
      logical FOUND
C
      data RES    /'GLY','SER','VAL','ALA','THR',
     +             'LEU','ASP','LYS','ILE','ASN',
     -             'PRO','GLU','GLN','PHE','TYR',
     +             'ARG','HIS','MET','TRP','CYS',
     -             'CYH'/
      data LENRES /    4,    6,    7,    5,    7,
     +                 8,    8,    9,    8,    8,
     -                 7,    9,    9,   11,   12,
     +                11,   10,    8,   14,    6,
     -                 6/
c      data (ATM(K,1), K=1,MAXATS) /'N  ','CA ','C  ','O  '
c      data (ATM(K,2), K=1,MAXATS) /'N  ','CA ','C  ','O  ','CB ','OG '
c      data (ATM(K,3), K=1,MAXATS) /'N  ','CA ','C  ','O  ','CB ','CG1','CG2'
c      data (ATM(K,4), K=1,MAXATS) /'N  ','CA ','C  ','O  ','CB '
c      data (ATM(K,5), K=1,MAXATS) /'N  ','CA ','C  ','O  ','CB ','OG1','CG2'
c      data (ATM(K,6), K=1,MAXATS) /'N  ','CA ','C  ','O  ','CB ','CG ','CD1','CD2'
c      data (ATM(K,7), K=1,MAXATS) /'N  ','CA ','C  ','O  ','CB ','CG ','OD1','OD2'
c      data (ATM(K,8), K=1,MAXATS) /'N  ','CA ','C  ','O  ','CB ','CG ','CD ','CE '
c      data (ATM(K,9), K=1,MAXATS) /'N  ','CA ','C  ','O  ','CB ','CG1','CG2','CD1','NZ '
c      data (ATM(K,10),K=1,MAXATS) /'N  ','CA ','C  ','O  ','CB ','CG ','OD1','ND2'
c      data (ATM(K,11),K=1,MAXATS) /'N  ','CA ','C  ','O  ','CB ','CG ','CD '
c      data (ATM(K,12),K=1,MAXATS) /'N  ','CA ','C  ','O  ','CB ','CG ','CD ','OE1','OE2'
c      data (ATM(K,13),K=1,MAXATS) /'N  ','CA ','C  ','O  ','CB ','CG ','CD ','OE1','NE2'
c      data (ATM(K,14),K=1,MAXATS) /'N  ','CA ','C  ','O  ','CB ','CG ','CD1','CD2','CE1','CE2','CZ '
c      data (ATM(K,15),K=1,MAXATS) /'N  ','CA ','C  ','O  ','CB ','CG ','CD1','CD2','CE1','CE2','CZ ','OH '
c      data (ATM(K,16),K=1,MAXATS) /'N  ','CA ','C  ','O  ','CB ','CG ','CD ','NE ','CZ ','NH1','NH2'
c      data (ATM(K,17),K=1,MAXATS) /'N  ','CA ','C  ','O  ','CB ','CG ','ND1','CD2','CE1','NE2'
c      data (ATM(K,18),K=1,MAXATS) /'N  ','CA ','C  ','O  ','CB ','CG ','SD ','CE '
c      data (ATM(K,19),K=1,MAXATS) /'N  ','CA ','C  ','O  ','CB ','CG ','CD1','CD2','NE1','CE2','CE3','CZ2','CZ3','CH2'
c      data (ATM(K,20),K=1,MAXATS) /'N  ','CA ','C  ','O  ','CB ','SG '
c      data (ATM(K,21),K=1,MAXATS) /'N  ','CA ','C  ','O  ','CB ','SG '
C
      narg=iargc()
      if (NARG.gt.0) then
        IIN=1
        call GETARG(1,FILENAME)
        open (file=FILENAME,unit=1,status='old',form='formatted',err=3)
      else
        IIN=5
      end if
C
      NRESID=0
      NERR=0
      NEWRES='     '
      OLDRES='     '
      N=0
C
1     read (IIN,'(A)',err=2,end=9) card
      if (CARD(1:6).eq.'ATOM  ') then
        if (CARD(22:27).ne.OLDRES) then
          LENGTH=N
          NRESID=NRESID+1
          if (NRESID.gt.1) then
            if (LENGTH.lt.LENRES(IRESTYP)) then
              write (6,'(''chklen: '',I2,'' too few atoms in  '',
     +               A,1X,A)')
     -               LENRES(IRESTYP)-LENGTH,RES(IRESTYP),OLDRES
            else if (LENGTH.gt.LENRES(IRESTYP)) then
              write (6,'(''chklen: '',I2,'' too many atoms in '',
     +               A,1X,A)')
     -               LENGTH-LENRES(IRESTYP),RES(IRESTYP),OLDRES
            end if
          end if
          N=1
          OLDRES=CARD(22:27)
        else
          do I=1,NRES
            if (CARD(18:20).eq.RES(I)) then
              IRESTYP=I
              goto 5
            end if
          end do
5         continue
          read (CARD,'(A)') BUFFER(N)
          N=N+1
          if (CARD(14:16).eq.'OXT') then
            N=N-1
          end if
        end if
      end if
      goto 1
9     continue
C
C the last residue
C
      LENGTH=N
      NRESID=NRESID+1
      if (NRESID.gt.1) then
        if (LENGTH.lt.LENRES(IRESTYP)) then
          write (6,'(''chklen: '',I2,'' too few atoms in  '',A,1X,A)')
     -           LENRES(IRESTYP)-LENGTH,RES(IRESTYP),OLDRES
        else if (LENGTH.gt.LENRES(IRESTYP)) then
          write (6,'(''chklen: '',I2,'' too many atoms in '',A,1X,A)')
     -           LENGTH-LENRES(IRESTYP),RES(IRESTYP),OLDRES
        end if
      end if
C
      if (IIN.eq.1) then
        close (unit=1)
      end if
C
      call EXIT(1)
C
2     write (6,'(''chkres: error reading input file'')')
      call EXIT(1)
3     write (6,'(''chkres: error opening input file'')')
      call EXIT(1)
C
      end
