      program CHKRES
C
C copyright (c) 1988-1991 John Overington
C part of joy
C
C Removes all the ATOM hydrogen records from a Brookhaven coordinate file
C If files on command line then process these
C
      parameter (MAXERR=25, NRES=21)
      character*80 card, FILENAME
      character*3 RES(NRES)
      integer NARG, IIN, NREC, NERR
      logical FOUND
C
      data RES /'GLY','SER','VAL','ALA','THR',
     +          'LEU','ASP','LYS','ILE','ASN',
     -          'PRO','GLU','GLN','PHE','TYR',
     +          'ARG','HIS','MET','TRP','CYS',
     -          'CYH'/
C
      narg=iargc()
      if (NARG.gt.0) then
        IIN=1
        call GETARG(1,FILENAME)
        open (file=FILENAME,unit=1,status='old',form='formatted',err=3)
      else
        IIN=5
      end if
C
      NREC=0
      NERR=0
1     read (IIN,'(A)',err=2,end=9) card
      NREC=NREC+1
      if (CARD(1:6).eq.'ATOM  ') then
        found=.false.
        do I=1,NRES
          if (CARD(18:20).eq.RES(I)) then
            found=.true.
            goto 5
          end if
        end do
5       continue
        if (.not.FOUND) then
          write (6,'(''chkres: unknown residue type '',A,'' on line '',
     +           I4)') CARD(18:20),NREC
          NERR=NERR+1
          if (NERR.gt.MAXERR) then
            write (6,'(''chkres: too many errors !'')')
            call EXIT(1)
          end if
        end if
      end if
      goto 1
9     continue
      if (IIN.eq.1) then
        close (unit=1)
      end if
C
      call EXIT(1)
C
2     write (6,'(''chkres: error reading input file'')')
      call EXIT(1)
3     write (6,'(''chkres: error opening input file'')')
      call EXIT(1)
C
      end
