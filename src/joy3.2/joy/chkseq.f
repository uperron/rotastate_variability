      subroutine CHKLEN(NRES1,NRES2,ROOT,FILNAM1,FILNAM2)
C
C copyright (c) 1988-1991 John Overington
C part of joy
C
C Trivially checks for an unequal number of residues in the alignment and data
C files, 1 and 2 are horribly mixed up
C
      integer STDERR, STDOUT
      parameter (STDERR=0, STDOUT=6)
      character*(*) FILNAM1, FILNAM2, ROOT
      integer NRES1, NRES2
C
      if (NRES1.ne.NRES2) then
        write (STDERR,'(''chklen: error -- bad lengths'',/,
     -             ''  sequence '',A,'' is '',I4,
     +             '' residues long from alignment file '',A,/,
     -             ''  sequence '',A,'' is '',I4,
     +             '' residues long from data file '',A)')
     -             ROOT(1:LASTCHAR(ROOT)),NRES2,FILNAM1(1:lastchar(FILNAM1)),
     -             ROOT(1:LASTCHAR(ROOT)),NRES1,FILNAM2(1:lastchar(FILNAM2))
        call EXIT(1) 
      end if
C
      return
      end
C
C #############################################################################
C
      subroutine CHKSEQ(SEQ1,SEQ2,LENGTH,ROOT,FILNAM1,FILNAM2)
C
C Checks for consistency between the alignment sequence and the .PSA file
C sequence. Allow mismatches in second sequence only for unknown residues (X)
C Allow for ambiguity of C and J.
C
      include 'joy.h'
C
      character*(*) FILNAM1, FILNAM2, ROOT
      character*1 SEQ1(MAXRES), SEQ2(MAXRES)
      integer M, I, LENGTH
C
C Exclude positions where CYH/CYS ambiguity occurs
C
      M=0
      do I=1,LENGTH
        if (.not. ISPUNCT(SEQ1(I))) then
          M=M+1
          if ((SEQ1(I).ne.SEQ2(M)).and.(SEQ2(M).ne.'X')) then
            if (.not.(SEQ1(I).eq.'C'.and.SEQ2(M).eq.'J')) then
              if (.not.(SEQ1(I).eq.'J'.and.SEQ2(M).eq.'C')) then
                write (STDERR,'(''chkseq: error -- bad sequences'',/,
     -                 ''  residue '',I3,'' is '',A,
     +                 '' in sequence '',A,'' from alignment file '',A,/,
     -                 ''  residue '',I3,'' is '',A,
     +                 '' in sequence '',A,'' from data file '',A)')
     -                     I,SEQ1(I),ROOT(1:LASTCHAR(ROOT)),FILNAM1,
     -                     M,SEQ2(M),ROOT(1:LASTCHAR(ROOT)),FILNAM2
                call EXIT(1)
              end if
            end if
          end if
        end if
      end do
C
      return
      end
