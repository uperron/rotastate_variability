      subroutine CHKSTR(STRNAM,RESNAM,RDISC,NRES,TOTACC)
C
C copyright (c) 1988-1991 John Overington
C part of joy
C
C -----------------------------------------------------------------------------
C
C reads in the envrionment description vectors and analyses a structure to
C point to any unusual residue environments.
C
C Could also print out the frequency of each of the unusual classes wrt the
C known database of structures
C
C Tests at present are:
C A) The unusual environments are
C    1) positive phi for other than gly and asparagine
C    2) cis-peptide non-prolines
C    3) buried non-hydrogen bonded polar residues
C B) Then check the ratio of buried polar to buried non-polar residues
C C) A number of the Baumann, Frommel and Sander tests
C
C -----------------------------------------------------------------------------
C
      include 'joy.h'
C
      character*(*) STRNAM
      character*5 RESNAM(MAXRES)
      character*1 RES(21)
      integer NRES, I, NSURHYD, NSURPOL, NBURHYD, NBURPOL
      integer RDISC(NUMFEAT,MAXRES)
      real MW, MWTOT, TOTACC, SURF, POLSURF, SURFTOT, POLSURFTOT, OFFSET
      logical ISIPOL
C
      data RES /'A','C','D','E','F','G','H','I','K','L',
     -          'M','N','P','Q','R','S','T','V','W','Y','J'/
      data OFFSET /1178.0/
C
      NSURPOL=0
      NSURHYD=0
      NBURPOL=0
      NBURHYD=0
C
      write (STDOUT,'(/,''unusual residue environments for '',A)')
     +       STRNAM
C
      do I=1,NRES
C
C code to get environment from RDISC (not used at present)
C
C     IENV(I)=IMAT(RDISC(1,I))
C
C 1) the + phi angle, 11 is PHI desciptor, 1 is sequence
C
        if (RDISC(11,I).eq.1) then
          if ((RDISC(1,I).ne.6).and.(RDISC(1,I).ne.12)) then
            write (STDOUT,'(''  residue '',A,1X,A,
     -                 '' is in the unusual +phi conformation'')')
     -                 RES(RDISC(1,I)),RESNAM(I)
          end if
        end if
C
C 2) the cis-peptide, 8 is CIS desciptor, 1 is sequence
C
        if (RDISC(8,I).eq.1) then
          if (RDISC(1,I).ne.13) then
            write (STDOUT,'(''  residue '',A,1X,A,
     -            '' is in the unusual cis-peptide conformation'')')
     -                 RES(RDISC(1,I)),RESNAM(I)
          end if
        end if
C
C 3) the buried polar non-hb residues, 3 is ACC desciptor, 1 is sequence,
C    4 is side-side, 5 is side-CO, 6 is side-NH, 7 is side-HET
C polar are DEHKNQRSTWYJ
C
        if (RDISC(3,I).eq.2) then
          if ((RDISC(4,I).eq.2).and.(RDISC(5,I).eq.2).and.
     -        (RDISC(6,I).eq.2).and.(RDISC(7,I).eq.2)) then
            if (ISIPOL(RDISC(1,I))) then
              write (STDOUT,'(''  residue '',A,1X,A,
     -                   '' is buried but not hydrogen bonded'')')
     -                   RES(RDISC(1,I)),RESNAM(I)
            end if
          end if
        end if
C
C Now the buried polar to buried non-polar ratios
C
        if (RDISC(3,I).eq.2) then
          if (ISIPOL(RDISC(1,I))) then
            NBURPOL=NBURPOL+1
          else
            NBURHYD=NBURHYD+1
          end if
        else
          if (ISIPOL(RDISC(1,I))) then
            NSURPOL=NSURPOL+1
          else
            NSURHYD=NSURHYD+1
          end if
        end if
      end do
C
      write (STDOUT,'(/,20X,''buried  surface    total'',/,
     -             ''polar            '',3(3X,I6),/,
     -             ''non-polar        '',3(3X,I6),/,
     -             ''polar / non-polar'',3(3X,F6.2))')
     - NBURPOL,NSURPOL,NBURPOL+NSURPOL,
     - NBURHYD,NSURHYD,NBURHYD+NSURHYD,
     - real(NBURPOL)/(real(NBURHYD)+SMALL),
     - real(NSURPOL)/(real(NSURHYD)+SMALL),
     - real(NBURPOL+NSURPOL)/real(NBURHYD+NSURHYD)
      write (STDOUT,'(''buried / surface '',3X,F6.2,/)')
     + real(NBURPOL+NBURHYD)/real(NSURPOL+NSURHYD)
C
C taken from Sanders work (Prot Eng), SURFMW
C
      MWTOT=0.0
      SURFTOT=0.0
      POLSURFTOT=0.0
      do I=1,NRES
        MWTOT=MWTOT+MW(RDISC(1,I))
        SURFTOT=SURFTOT+SURF(RDISC(1,I))
        POLSURFTOT=POLSURFTOT+POLSURF(RDISC(1,I))
      end do
      write (STDOUT,'(''molecular weight    = '',F9.2)') MWTOT
      write (STDOUT,'(''Sanders MWRES       = '',F9.2,
     -                '' (expect between 97 to 118)'')')
     +        MWTOT/real(NRES)
      write (STDOUT,'(''Sanders SURFMW      = '',F9.3,
     -                '' (expect between 0.32 and 0.52) NOT WORKING!'')')
     -                (TOTACC-OFFSET)/MWTOT
      write (STDOUT,'(''Sanders POLFRAC_MAX = '',F9.3,
     -                '' (expect between 0.16 and 0.19)'',/)')
     -                POLSURFTOT/SURFTOT
C
      return
      end
C
C #############################################################################
C
      logical function ISIPOL(IRES)
C
      integer IRES
C
C Takes the integer index of a residue and returns if it is polar or not
C
      if ((IRES.ne.1) .and. (IRES.ne.2) .and.
     +    (IRES.ne.5) .and. (IRES.ne.6) .and.
     -    (IRES.ne.8) .and. (IRES.ne.10) .and.
     +    (IRES.ne.11) .and. (IRES.ne.13).and.
     -    (IRES.ne.18)) then
        ISIPOL=.true.
      else
        ISIPOL=.false.
      end if
C
      return
      end
C
C ############################################################################
C
      integer function IMAT(RDISC)
C
      include 'joy.h'
C
      integer RDISC(NUMFEAT)
C
      IMAT=1
C
      if (RDISC(2).eq.1) then
        IMAT=IMAT
      else if (RDISC(2).eq.2) then
        IMAT=IMAT+16
      else if (RDISC(2).eq.3) then
        IMAT=IMAT+32
      else if (RDISC(2).eq.4) then
        IMAT=IMAT+48
      else
        call ERROR('imat: bad sec str class',' ',1)
      end if
C
      if (RDISC(3).eq.1) then
        IMAT=IMAT
      else if (RDISC(3).eq.2) then
        IMAT=IMAT+8
      else
        call ERROR('imat: bad acc class',' ',1)
      end if
C
      if (RDISC(4).eq.1) then
        IMAT=IMAT
      else if (RDISC(4).eq.2) then
        IMAT=IMAT+4
      else
        call ERROR('imat: bad sshb class',' ',1)
      end if
C
      if (RDISC(5).eq.1) then
        IMAT=IMAT
      else if (RDISC(5).eq.2) then
        IMAT=IMAT+2
      else
        call ERROR('imat: bad sohb class',' ',1)
      end if
C
      if (RDISC(6).eq.1) then
        IMAT=IMAT
      else if (RDISC(6).eq.2) then
        IMAT=IMAT+1
      else
        call ERROR('imat: bad snhb class',' ',1)
      end if
C
      return
      end
C
C =======================================================================
C
      real function MW(IRES)
C
      integer IRES
      real MWDATA(22)
      data MWDATA / 71.08, 103.14, 115.09, 129.12, 147.18,
     -              57.06, 137.15, 113.17, 128.18, 113.17,
     -             131.21, 114.11,  97.12, 128.18, 156.20,
     -              87.08, 101.11,  99.14, 186.21, 163.18,
     -             103.14,   0.00/
C
      if (IRES .ge. 1 .and. IRES .le. 22) then
        MW=MWDATA(IRES)
      else
        call ERROR('joy: bad pointer to MW',' ',1)
      end if
C
      return
      end
C
      real function SURF(IRES)
C
C =======================================================================
C
      integer IRES
      real SURFDATA(22)
C
      data SURFDATA / 106.3,  82.7, 149.5, 182.8, 200.9,
     -                 83.6, 182.2, 171.5, 200.8, 163.6,
     -                193.7, 149.8, 135.9, 186.6, 239.5,
     -                123.1, 141.7, 148.4, 245.4, 212.4,
     -                138.5,   0.0/
C
      if (IRES .ge. 1 .and. IRES .le. 22) then
        SURF=SURFDATA(IRES)
      else
        call ERROR('joy: bad pointer to SURF',' ',1)
      end if
C
      return
      end
C
      real function POLSURF(IRES)
C
C =======================================================================
C
      integer IRES
      real POLSURFDATA(22)
C
      data POLSURFDATA / 16.5, 14.4, 44.3, 51.3, 15.7,
     -                   19.6, 27.7, 17.2, 28.5, 16.7,
     -                   21.5, 42.7, 13.2, 45.2, 59.0,
     -                   25.2, 23.5, 16.7, 24.7, 28.2,
     -                   15.7,  0.0/
C
      if (IRES .ge. 1 .and. IRES .le. 22) then
        POLSURF=POLSURFDATA(IRES)
      else
        call ERROR('joy: bad pointer to POLSURFMW',' ',1)
      end if
C
      return
      end
