      integer function CONSEN(TYPE,NSEQ,NCLASS,CUTOFF)
C
C copyright (c) 1988-1991 John Overington
C part of joy
C 
C Takes a number of secondary structural assignments and then decides if there
C is a consensus assignment at a position. At present for a consensus to be
C assigned the same secondary structural class needs to be present in at least 
C 0.7 of the sample
C
C Changed routine to be general, (accessibility), NCLASS is now the number
C of classes, the catch all class
C
      include   'joy.h'
      integer MAXCLASS
      parameter (MAXCLASS=22)
C
      integer TYPE(MAXSEQ,MAXRES), NCLASS, NSEQ, N(MAXCLASS), I, J
      real CUTOFF
C
      if (NCLASS.gt.MAXCLASS) then
        call ERROR('joy: too many classes',' ',1)
      end if
      if ((CUTOFF.gt.1.0).or.(CUTOFF.lt.0.0)) then
        call ERROR('joy: bad cutoff',' ',1)
      end if
      do I=1,MAXCLASS
        N(I)=0
      end do
C
C Count the number of times each class occurs at the position
C
      do I=1,NSEQ
        if (TYPE(1,I).gt.NCLASS) then
          call ERROR('joy: bad type id',' ',1)
        end if
        do J=1,NCLASS
          if (TYPE(I,1).eq.J) then
            N(J)=N(J)+1
          end if
        end do
      end do
C
      CONSEN=0		! bug fix - jpo, returned wild values sometimes
      do J=1,NCLASS
        if ((real(N(J))/real(NSEQ)).ge.CUTOFF) then
          CONSEN=J
          return
        end if
      end do
C
      return
      end
