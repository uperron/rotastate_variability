      subroutine ELEMCAP(IELEM,SEQ,LENGTH,RDISC,IELM,NUMELEM,FILE)
C
C copyright (c) 1988-1991 John Overington
C part of joy
C
C To print out features associated with the ends of secondary structure elements
C
C LONGELEM is the cutoff at which an element is considered to be `long'
C
      parameter (MAXELEM=50, LONGELEM=5)
C
      include 'joy.h'
C
      character*(*) FILE
      character*9 ACCSTR,SEQSTR,HBOSTR,HBNSTR,CHGSTR,PHISTR
      character*7 SHTSTR,HELSTR,TYPSTR
      character*1 SEQ(MAXRES), RES(22), ENDSTR
      integer IELEM, LENGTH, RDISC(NUMFEAT,MAXRES)
      integer NNCAP(MAXELEM), NUMNCAP, NUMCCAP, NCCAP(MAXELEM), NRES
      integer LENELEM(MAXELEM), IELM(3,MAXELEM), NUMELEM, I, M, J, IPTR, IP
      logical INELEM, WASINELEM
C
      data RES /'A','C','D','E','F','G','H','I','K','L',
     -          'M','N','P','Q','R','S','T','V','W','Y','J','-'/
      data ENDSTR /'U'/
      data SHTSTR /'sheets '/
      data HELSTR /'helices'/
C
C ----------------------------------------------------------------------------
C
C Find the N and C-termini
C reference to RDISC used to be to feature 2 but now to real DSSP feature 10
C
      INELEM=.false.
      WASINELEM=.false.
      NUMNCAP=0
      IP=0
      do I=1,LENGTH
        if (SEQ2I(SEQ(I)).lt.22) then	! check if a real residue
          IP=IP+1
          if (RDISC(10,IP).eq.IELEM) then
            if (.not.INELEM) then
              NUMNCAP=NUMNCAP+1
              NNCAP(NUMNCAP)=IP
              WASINELEM=.true.
            end if
            INELEM=.true.
          else
            if (WASINELEM) then
              NCCAP(NUMNCAP)=IP-1
              WASINELEM=.false.
            end if
            INELEM=.false.
          end if 
        end if
      end do
      NRES=IP
C
C check that we have not found too many elements
C
      if (NUMNCAP.gt.MAXELEM) then
        call ERROR('joy: too many elements',' ',1)
      end if
C
      do I=1,NUMNCAP
        LENELEM(I)=NCCAP(I)-NNCAP(I)+1
c        write (6,'(4I8)') I,NNCAP(I),NCCAP(I),LENELEM(I)
      end do
C
C find those of length longer than LONGELEM (5 is 1 more than no. of
C considered posns)
C
      NUMELEM=0
      do I=1,NUMNCAP
        if (LENELEM(I).ge.LONGELEM) then
          NUMELEM=NUMELEM+1
          IELM(1,NUMELEM)=NNCAP(I)
          IELM(2,NUMELEM)=NCCAP(I)
        end if
      end do
C
      if (IELEM.eq.1) then
        TYPSTR=HELSTR
      else if (IELEM.eq.2) then
        TYPSTR=SHTSTR
      else
        call ERROR('joy: unknown element type',' ',1)
      end if
C
      if (NUMELEM.gt.0) then
        do I=1,NUMELEM
          IELM(3,I)=IELM(2,I)-IELM(1,I)+1
C
C Dodgy way to handle overlap correctly (need variable format)
C make sure not at N or C terminus
C
          if ((IELM(1,I).ge.5).and.((NRES-IELM(2,I)).ge.5)) then
C
C First the N-terminus of the fragment
C
            SEQSTR='========='
            IPTR=0
            do J=IELM(1,I)-4,IELM(1,I)+4
              IPTR=IPTR+1
              SEQSTR(IPTR:IPTR)=RES(RDISC(1,J))
            end do
            CHGSTR='         '
            IPTR=0
            do J=IELM(1,I)-4,IELM(1,I)+4
              IPTR=IPTR+1
              if (RDISC(1,J).eq.3.or.RDISC(1,J).eq.4) then
                CHGSTR(IPTR:IPTR)='-'
              else if (RDISC(1,J).eq.7.or.RDISC(1,J).eq.9
     -                 .or.RDISC(1,J).eq.15) then
                CHGSTR(IPTR:IPTR)='+'
              end if
            end do
            ACCSTR='         '
            IPTR=0
            do J=IELM(1,I)-4,IELM(1,I)+4
              IPTR=IPTR+1
              if (RDISC(3,J).eq.2) then
                ACCSTR(IPTR:IPTR)='#'
              end if
            end do
            HBOSTR='         '
            IPTR=0
            do J=IELM(1,I)-4,IELM(1,I)+4
              IPTR=IPTR+1
              if (RDISC(5,J).eq.1) then
                HBOSTR(IPTR:IPTR)='#'
              end if
            end do
            HBNSTR='         '
            IPTR=0
            do J=IELM(1,I)-4,IELM(1,I)+4
              IPTR=IPTR+1
              if (RDISC(6,J).eq.1) then
                HBNSTR(IPTR:IPTR)='#'
              end if
            end do
            PHISTR='         '
            IPTR=0
            do J=IELM(1,I)-4,IELM(1,I)+4
              IPTR=IPTR+1
              if (RDISC(11,J).eq.1) then
                PHISTR(IPTR:IPTR)='#'
              end if
            end do
            ENDSTR='N'
            write (9,'(A5,1X,A1,A1,I3,3I4,2X,''|'',A9,''|'',1X,''|'',A9,''|'',1X,''|'',
     -             A9,''|'',1X,''|'',A9,''|'',1X,''|'',A9,''|'',1X,''|'',A9,''|'')')
     -             FILE(1:5),TYPSTR(1:1),ENDSTR,I,IELM(1,I),IELM(2,I),IELM(3,I),
     -             SEQSTR,CHGSTR,ACCSTR,HBOSTR,HBNSTR,PHISTR
c            write (6,*) ' finished with N-term'
C
C Now the C-terminus of the element
C
            SEQSTR='========='
            IPTR=0
            do J=IELM(2,I)-4,IELM(2,I)+4
c              write (6,*) j
              IPTR=IPTR+1
              SEQSTR(IPTR:IPTR)=RES(RDISC(1,J))
            end do
            CHGSTR='         '
            IPTR=0
            do J=IELM(2,I)-4,IELM(2,I)+4
              IPTR=IPTR+1
              if (RDISC(1,J).eq.3.or.RDISC(1,J).eq.4) then
                CHGSTR(IPTR:IPTR)='-'
              else if (RDISC(1,J).eq.7.or.RDISC(1,J).eq.9
     -                 .or.RDISC(1,J).eq.15) then
                CHGSTR(IPTR:IPTR)='+'
              end if
            end do
            ACCSTR='         '
            IPTR=0
            do J=IELM(2,I)-4,IELM(2,I)+4
              IPTR=IPTR+1
              if (RDISC(3,J).eq.2) then
                ACCSTR(IPTR:IPTR)='#'
              end if
            end do
            HBOSTR='         '
            IPTR=0
            do J=IELM(2,I)-4,IELM(2,I)+4
              IPTR=IPTR+1
              if (RDISC(5,J).eq.1) then
                HBOSTR(IPTR:IPTR)='#'
              end if
            end do
            HBNSTR='         '
            IPTR=0
            do J=IELM(2,I)-4,IELM(2,I)+4
              IPTR=IPTR+1
              if (RDISC(6,J).eq.1) then
                HBNSTR(IPTR:IPTR)='#'
              end if
            end do
            PHISTR='         '
            IPTR=0
            do J=IELM(2,I)-4,IELM(2,I)+4
              IPTR=IPTR+1
              if (RDISC(11,J).eq.1) then
                PHISTR(IPTR:IPTR)='#'
              end if
            end do
            ENDSTR='C'
            write (9,'(A5,1X,A1,A1,I3,3I4,2X,''|'',A9,''|'',1X,''|'',A9,''|'',1X,''|'',
     -             A9,''|'',1X,''|'',A9,''|'',1X,''|'',A9,''|'',1X,''|'',A9,''|'')')
     -             FILE(1:5),TYPSTR(1:1),ENDSTR,I,IELM(1,I),IELM(2,I),IELM(3,I),
     -             SEQSTR,CHGSTR,ACCSTR,HBOSTR,HBNSTR,PHISTR
C
          end if
        end do
      end if
C
      return
      end
C
C ##############################################################################
C
      subroutine CAPSUBST(SEQ1,SEQ2,LENGTH,R1,LENSEQ1,NMUTHELN,NMUTHELC,
     -                    NMUTSHTN,NMUTSHTC,HEL,SHT,NHEL,NSHT)
C
C Subroutine to count the substitutions for helical (and sheet) capping residues
C
      parameter (MAXELEM=50, LONGELEM=5)
      include 'joy.h'
C
      character*1 SEQ1(MAXRES), SEQ2(MAXRES)
      integer HEL(3,MAXELEM), SHT(3,MAXELEM)
      integer NMUTHELN(22,22,9), NMUTHELC(22,22,9), NMUTSHTN(22,22,9)
      integer NMUTSHTC(22,22,9), I, J, K, L, M, LENGTH, NHEL, NSHT 
      integer R1(NUMFEAT,MAXRES), LENSEQ1
      logical ONEONLY
C
      L=0
C
      do I=1,LENGTH
        if (ISAA(SEQ1(I))) then
          L=L+1
          if (L.ge.5.and.LENSEQ1-L.ge.5) then
            do J=1,NHEL
              if (L.eq.HEL(1,J)) then
                if (HEL(3,J).ge.LONGELEM) then
                  do K=1,9
                    NMUTHELN(R1(1,L-5+K),SEQ2I(SEQ2(I-5+K)),K)=
     -              NMUTHELN(R1(1,L-5+K),SEQ2I(SEQ2(I-5+K)),K)+1
                  end do
                end if
              end if
              if (L.eq.HEL(2,J)) then
                if (HEL(3,J).ge.LONGELEM) then
                  do K=1,9
                    NMUTHELC(R1(1,L-5+K),SEQ2I(SEQ2(I-5+K)),K)=
     -              NMUTHELC(R1(1,L-5+K),SEQ2I(SEQ2(I-5+K)),K)+1
                  end do
                end if
              end if
            end do
            do J=1,NSHT
              if (L.eq.SHT(1,J)) then
                if (SHT(3,J).ge.LONGELEM) then
                  do K=1,9
                    NMUTSHTN(R1(1,L-5+K),SEQ2I(SEQ2(I-5+K)),K)=
     -              NMUTSHTN(R1(1,L-5+K),SEQ2I(SEQ2(I-5+K)),K)+1
                  end do
                end if
              end if
              if (L.eq.SHT(2,J)) then
                if (SHT(3,J).ge.LONGELEM) then
                  do K=1,9
                    NMUTSHTC(R1(1,L-5+K),SEQ2I(SEQ2(I-5+K)),K)=
     -              NMUTSHTC(R1(1,L-5+K),SEQ2I(SEQ2(I-5+K)),K)+1
                  end do
                end if
              end if
            end do
          end if
        end if
      end do
C
C ----------------------------------------------------------------------------
C
      return
      end
