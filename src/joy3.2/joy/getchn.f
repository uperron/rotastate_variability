      program GETCHN
C
C copyright (c) 1988-1991 John Overington
C part of joy
C
C Searches for just a particular chain within a PDB format file, if no
C chain argument then copies input to output
C
      character*80 card, FILENAME
      character*1 CHNNAM
      integer NARG, IIN
      logical DOCHN
      data DOCHN /.true./
      data IIN /1/
C
      narg=iargc()
      if (NARG.eq.2) then
        call GETARG(1,CHNNAM)
        call GETARG(2,FILENAME)
        open (file=FILENAME,unit=IIN,status='old',form='formatted',
     +        err=3)
      else if (NARG.eq.1) then
        call GETARG(1,CHNNAM)
        IIN=5
      else 
        IIN=5
        DOCHN=.false.
      end if

      if (CHNNAM.eq.'@') then
        DOCHN=.false.
      end if
C
1     read (IIN,'(A)',err=2,end=9) card
      if (DOCHN) then
        if (CARD(22:22).eq.CHNNAM) then
          write (6,'(A)') card
        end if
      else
        write (6,'(A)') card
      end if
      goto 1
9     continue
      if (IIN.eq.1) then
        close (unit=IIN)
      end if
C
      call EXIT(1)
C
2     write (6,'(''getchn: error reading input file'')')
      call EXIT(1)
3     write (6,'(''getchn: error opening input file'')')
      call EXIT(1)
C
      end
