      program GETRNG
C
C copyright (c) 1988-1991 John Overington
C part of joy
C
C Searches for residues between a particular range within a PDB format file
C if no arguments then copies input to output
C expects 0, 2 or 3 args
C if 0 then just a wire
C if 2 then args are start and finish, if not bound then define as @@@@@
C if 3 as 2 with arg 3 the filename
C
      character*80 card, FILENAME
      character*5 START, END
      character*1 CHNNAM
      integer NARG, IIN
      logical DORNG, FSTART, FEND
      data DORNG /.true./
      data IIN /1/
C
      narg=iargc()
      if (NARG.eq.3) then
        call GETARG(1,START)
        call GETARG(2,END)
        call GETARG(3,FILENAME)
        open (file=FILENAME,unit=IIN,status='old',form='formatted',
     +        err=3)
      else if (NARG.eq.2) then
        call GETARG(1,START)
        call GETARG(2,END)
        IIN=5
      else if (NARG.eq.0) then
        IIN=5
        DORNG=.false.
      else
        write (6,'(''getrng: wrong number of arguments'')')
        call EXIT(1)
      end if
C
      if (START.eq.'@@@@@') then
        FSTART=.true.
      else 
        FSTART=.false.
      end if
      FEND=.false.
C
      do I=1,5
        if (START(I:I).eq.'_') then
          START(I:I)=' '
        end if
        if (END(I:I).eq.'_') then
          END(I:I)=' '
        end if
      end do
C
1     read (IIN,'(A)',err=2,end=9) card
      if (DORNG) then
        if (CARD(23:27).eq.START) then
          FSTART=.true.
        end if
        if (CARD(23:27).eq.END) then
          FEND=.true.
        end if
        if ((CARD(23:27).ne.END).and.(FEND)) then
          goto 9
        end if
        if (FSTART) then
          write (6,'(A)') card
        end if
      else
        write (6,'(A)') card
      end if
      goto 1
9     continue
      if (IIN.eq.1) then
        close (unit=IIN)
      end if
C
      call EXIT(1)
C
2     write (6,'(''getrng: error reading input file'')')
      call EXIT(1)
3     write (6,'(''getrng: error opening input file'')')
      call EXIT(1)
C
      end
