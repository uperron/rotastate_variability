      subroutine INITBL(NMUT,NMUTALL,NNN,NENVIR)
C
C copyright (c) 1988-1991 John Overington
C part of joy
C
C Simple subroutine to tidy up initialization of substitution matrices
C
      include 'joy.h'
      integer NMUT(22,22,4,2,2,2,2), NMUTALL(22,22)
      integer NNN(21,4,2,2,2,2), NENVIR(NUMEXAM,21), K, I, J, L, M, N, O
C
      do I=1,21
        do J=1,NUMEXAM
          NENVIR(J,I)=0
         end do
      end do
C
      do I=1,22
        do J=1,22
          NMUTALL(J,I)=0
        end do
      end do
      do I=1,2
        do J=1,2
          do K=1,2
            do L=1,2
              do M=1,4
                do N=1,21
                  NNN(N,M,L,K,J,I)=0
                end do
                do N=1,22
                  do O=1,22
                    NMUT(O,N,M,L,K,J,I)=0
                  end do
                end do
              end do 
            end do
          end do
        end do
      end do
C
      return
      end
C
C ############################################################################
C
      subroutine INITIT(TITLE)
C
C Subroutine to initialize various title strings and also to return the
C number of tiles (and therefor substitution matrices to be considered).
C
      include 'joy.h'
C
      character*(*) TITLE(MAXTITLE)
      integer       NMAT,I,J
C
C Messy assignment of labels
C
C     TITLE(1)= '1234567890123456789012345678901234567890123456789012345678901'
      TITLE(1)= 'Alpha   accessible    side-side    side-main O    '//
     +          'side-main N'
      TITLE(2)= 'Alpha   accessible    side-side    side-main O no '//
     +          'side-main N'
      TITLE(3)= 'Alpha   accessible    side-side no side-main O    '//
     +          'side-main N'
      TITLE(4)= 'Alpha   accessible    side-side no side-main O no '//
     +          'side-main N'
      TITLE(5)= 'Alpha   accessible no side-side    side-main O    '//
     +          'side-main N'
      TITLE(6)= 'Alpha   accessible no side-side    side-main O no '//
     +          'side-main N'
      TITLE(7)= 'Alpha   accessible no side-side no side-main O    '//
     +          'side-main N'
      TITLE(8)= 'Alpha   accessible no side-side no side-main O no '//
     +          'side-main N'
      TITLE(9)= 'Alpha inaccessible    side-side    side-main O    '//
     +          'side-main N'
      TITLE(10)='Alpha inaccessible    side-side    side-main O no '//
     +          'side-main N'
      TITLE(11)='Alpha inaccessible    side-side no side-main O    '//
     +          'side-main N'
      TITLE(12)='Alpha inaccessible    side-side no side-main O no '//
     +          'side-main N'
      TITLE(13)='Alpha inaccessible no side-side    side-main O    '//
     +          'side-main N'
      TITLE(14)='Alpha inaccessible no side-side    side-main O no '//
     +          'side-main N'
      TITLE(15)='Alpha inaccessible no side-side no side-main O    '//
     +          'side-main N'
      TITLE(16)='Alpha inaccessible no side-side no side-main O no '//
     +          'side-main N'
      TITLE(17)='Beta    accessible    side-side    side-main O    '//
     +          'side-main N'
      TITLE(18)='Beta    accessible    side-side    side-main O no '//
     +          'side-main N'
      TITLE(19)='Beta    accessible    side-side no side-main O    '//
     +          'side-main N'
      TITLE(20)='Beta    accessible    side-side no side-main O no '//
     +          'side-main N'
      TITLE(21)='Beta    accessible no side-side    side-main O    '//
     +          'side-main N'
      TITLE(22)='Beta    accessible no side-side    side-main O no '//
     +          'side-main N'
      TITLE(23)='Beta    accessible no side-side no side-main O    '//
     +          'side-main N'
      TITLE(24)='Beta    accessible no side-side no side-main O no '//
     +          'side-main N'
      TITLE(25)='Beta  inaccessible    side-side    side-main O    '//
     +          'side-main N'
      TITLE(26)='Beta  inaccessible    side-side    side-main O no '//
     +          'side-main N'
      TITLE(27)='Beta  inaccessible    side-side no side-main O    '//
     +          'side-main N'
      TITLE(28)='Beta  inaccessible    side-side no side-main O no '//
     +          'side-main N'
      TITLE(29)='Beta  inaccessible no side-side    side-main O    '//
     +          'side-main N'
      TITLE(30)='Beta  inaccessible no side-side    side-main O no '//
     +          'side-main N'
      TITLE(31)='Beta  inaccessible no side-side no side-main O    '//
     +          'side-main N' 
      TITLE(32)='Beta  inaccessible no side-side no side-main O no '//
     +          'side-main N'
      TITLE(33)='+ Phi   accessible    side-side    side-main O    '//
     +          'side-main N'
      TITLE(34)='+ Phi   accessible    side-side    side-main O no '//
     +          'side-main N'
      TITLE(35)='+ Phi   accessible    side-side no side-main O    '//
     +          'side-main N'
      TITLE(36)='+ Phi   accessible    side-side no side-main O no '//
     +          'side-main N'
      TITLE(37)='+ Phi   accessible no side-side    side-main O    '//
     +          'side-main N'
      TITLE(38)='+ Phi   accessible no side-side    side-main O no '//
     +          'side-main N'
      TITLE(39)='+ Phi   accessible no side-side no side-main O    '//
     +          'side-main N'
      TITLE(40)='+ Phi   accessible no side-side no side-main O no '//
     +          'side-main N'
      TITLE(41)='+ Phi inaccessible    side-side    side-main O    '//
     +          'side-main N'
      TITLE(42)='+ Phi inaccessible    side-side    side-main O no '//
     +          'side-main N'
      TITLE(43)='+ Phi inaccessible    side-side no side-main O    '//
     +          'side-main N'
      TITLE(44)='+ Phi inaccessible    side-side no side-main O no '//
     +          'side-main N'
      TITLE(45)='+ Phi inaccessible no side-side    side-main O    '//
     +          'side-main N'
      TITLE(46)='+ Phi inaccessible no side-side    side-main O no '//
     +          'side-main N'
      TITLE(47)='+ Phi inaccessible no side-side no side-main O    '//
     +          'side-main N'
      TITLE(48)='+ Phi inaccessible no side-side no side-main O no '//
     +          'side-main N'
      TITLE(49)='Coil    accessible    side-side    side-main O    '//
     +          'side-main N'
      TITLE(50)='Coil    accessible    side-side    side-main O no '//
     +          'side-main N'
      TITLE(51)='Coil    accessible    side-side no side-main O    '//
     +          'side-main N'
      TITLE(52)='Coil    accessible    side-side no side-main O no '//
     +          'side-main N'
      TITLE(53)='Coil    accessible no side-side    side-main O    '//
     +          'side-main N'
      TITLE(54)='Coil    accessible no side-side    side-main O no '//
     +          'side-main N'
      TITLE(55)='Coil    accessible no side-side no side-main O    '//
     +          'side-main N'
      TITLE(56)='Coil    accessible no side-side no side-main O no '//
     +          'side-main N'
      TITLE(57)='Coil  inaccessible    side-side    side-main O    '//
     +          'side-main N'
      TITLE(58)='Coil  inaccessible    side-side    side-main O no '//
     +          'side-main N'
      TITLE(59)='Coil  inaccessible    side-side no side-main O    '//
     +          'side-main N'
      TITLE(60)='Coil  inaccessible    side-side no side-main O no '//
     +          'side-main N'
      TITLE(61)='Coil  inaccessible no side-side    side-main O    '//
     +          'side-main N'
      TITLE(62)='Coil  inaccessible no side-side    side-main O no '//
     +          'side-main N'
      TITLE(63)='Coil  inaccessible no side-side no side-main O    '//
     +          'side-main N'
      TITLE(64)='Coil  inaccessible no side-side no side-main O no '//
     +          'side-main N'
C
      return
      end
