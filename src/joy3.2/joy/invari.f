C copyright (c) 1988-1991 John Overington
C part of joy
      subroutine INVARI(SEQ,NSTR,RDISC,LENGTH)
C
C reports on the number of invariant residues in an alignment
C
C new version should be easier to add analysis of other features via RDISC
C
      include 'joy.h'
C
      character*80 STRING
      character*21 RES
      character*1 SEQ(MAXRES,MAXSEQ)
      integer SEQTYP(MAXSEQ,MAXRES)
      integer RDISC(NUMFEAT,MAXRES,MAXSEQ), LENGTH, KK, I, NSTR, J
      integer CONSEN, CONSENTMP
C
      data RES /'ACDEFGHIKLMNPQRSTVWYJ'/
C
      do I=1,MAXRES
        do J=1,MAXSEQ
          SEQTYP(J,I)=22
        end do
      end do
C
C copied from WRTEX
C
      do I=1,NSTR
        KK=0
        do J=1,LENGTH
          if (ISAA(TOUPPER(SEQ(J,I)))) then
            KK=KK+1
            SEQTYP(I,J)=RDISC(1,KK,I)
          end if
        end do
      end do
C
      write (STDOUT,'(/,'' invariant residues'',/)')
      do J=1,LENGTH
        CONSENTMP=CONSEN(SEQTYP(1,J),NSTR,22,1.0)
        if (CONSENTMP.lt.22) then
          write (STDOUT,'('' position '',I4,'' residue '',A)')
     -           J,RES(CONSENTMP:CONSENTMP)
        end if
      end do
C
      return
      end
