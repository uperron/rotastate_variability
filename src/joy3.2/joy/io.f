cf ----------------------------------------------------------------------------
cf
cf    OPENF3() routine opens a possibly compressed file for various
cf    I/O operations. It is equivalent to VMS open with
cf    carriagecontrol='list', ie the write format statement should 
cf    omit 1x as the first item if you do not want a blank as the 
cf    first character on the line.
cf
cf    The arguments are:
cf
cf    iochn             I/O stream
cf
cf    fname             filename
cf
cf    if stat='old'     if the file does not exist then it complains;
cf           ='unknown' the file may or may not exist;
cf           ='new'     the file must not exist;
cf
cf    if stat='old' then the format is determined automatically and returned
cf                  irrespective of the input value; -- does not work with f2c
cf          <>'old' then the input format is used; 
cf
cf    if access='append' then the file pointer is positioned at the end
cf                       (irrespective of the file's existence or not);
cf                       only for fmt='FORMATED'; the other possibility
cf                       if SEQUENTIAL.
cf
cf    format       can be FORMATTED or UNFORMATTED.
cf
cf    if verbose=  0 No messages written out;
cf                 1 Error messages written out;
cf                 2 Error and Warning messages written out;
cf                 3 Error, Warning, and Note messages written out;
cf                 4 Note messages written out;
cf
cf    if estop=.true.  then it will stop on Error.
cf
cf    An output status variable IERR is <> 0 if something was wrong.
cf
cf    An output argument COMPRESSED is returned .T. if the file was 
cf    compressed before opened.
cf
cf    subroutine openf3(iochn,fname,stat,access,format,verbose,
cf   -                  estop,ierr,compressed)
cf
cf ----------------------------------------------------------------------------

      subroutine openf3(iochn,fname,stat,access,format,verbose,
     &                  estop,ierr,compressed)
        implicit none
        integer iochn, ierr, verbose
        character fname*(*), stat*(*), access*(*), format*(*)
        character nozf*(255)
        logical estop, compressed

c ----- possibly uncompress the file if it is compressed
        call prepfil(fname,compressed,nozf)

        call openf2(iochn,nozf,stat,access,format,verbose,estop,
     &              ierr)

        return
      end


cf ----------------------------------------------------------------------------
cf
cf    OPENF2() is the same as OPENF3() except that the file must not be
cf    compressed.
cf
cf    subroutine openf2(iochn,fname,stat,access,format,verbose,
cf   -                  estop,ierr)
cf
cf ----------------------------------------------------------------------------

      subroutine openf2(iochn,fname,stat,access,format,verbose,
     &                  estop,ierr)
        implicit none
        integer maxlen
        parameter (maxlen=255)
        integer iochn, lr, lenr, ierr, verbose
        character fname*(*), stat*(*), stat2*(20) 
        character access*(*), access2*(20), ios*3
        character dummy*(1), format*(*), format2*(20), fname2*(maxlen)
        logical exst, estop

        lr = lenr(fname)
c ----- always stop on this error message:
        if (lr .gt. maxlen) then
          write(*,'(a)') 'openf2__E> increase maxlen'
          stop
        end if

        if (verbose.ge.3) then
          if (lr .eq. 0) then
            fname2='fort.'
            write(ios, '(i3)') iochn
            call addstr(fname2, ios)
            lr = lenr(fname2)
          else
            fname2 = fname
          end if

          if (lr .gt. 50) then
            write(*,'(a,i2,8a)') 
     &      'openf2___> ',iochn,
     &      ',',stat(1:3),
     &      ',',access(1:3),
     &      ',',format(1:4),
     &      ', ... ',fname2(lr-45:lr)
          else
            write(*,'(a,i2,8a)') 
     &      'openf2___> ',iochn,
     &      ',',stat(1:3),
     &      ',',access(1:3),
     &      ',',format(1:4),
     &      ', ',fname2(1:lr)
          end if

        end if

        format2 = format
        access2 = access
        stat2 = stat
        call upper(access2)
        call upper(stat2)
        call upper(format2)

c ----- does the file exist?
        inquire(file=fname2, exist=exst)
        if ((.not. exst) .and. (stat2.eq.'OLD')) then
          if (verbose.gt.0 .and. verbose .le. 3) then 
            write(*,'(a)') 'openf2__E> file does not exist'
            if (estop) stop
          end if
          ierr = 1
          return
        end if
         
        if (exst .and. (stat2.eq.'NEW')) then
          if (verbose.gt.0 .and. verbose .le. 3) then
            write(*,'(a)') 'openf2__E> file exists'
            if (estop) stop 
          end if
          ierr = 2
          return
        end if

c ----- determine the format if neccessary -- I do not know why it does not
c       work with f2c
c        if (stat2.eq.'OLD') then
c          inquire(file=fname2, formatted=format)
c           if (format.eq.'YES') then
c             format = 'FORMATTED'
c           else
c             format = 'UNFORMATTED'
c           end if
c        end if
c        call upper(format)
c        if (format.ne.format2) then
c           if (verbose.gt.0 .and. verbose .le. 3) 
c     -     write(*,'(2a)')'openf2__w> format changed to: ',format
c         end if

        if((format2.ne.'FORMATTED').and.(access2.eq.'APPEND')) then
c        if((format.ne.'FORMATTED').and.(access2.eq.'APPEND')) then
          if (verbose.gt.0 .and. verbose .le. 3) then
            write(*,'(a)')
     &      'openf2__E> access="APPEND" not allowed for UNFORMATTED'
            if (estop) stop 
          end if
          ierr = 3
          return
        end if
      
c ----- open the file (always in access='sequential')
        open(iochn, form=format, file=fname2, status=stat, err=10)

c ----- go to the end if opened for appending
        if (access2.eq.'APPEND') then
5         read(iochn, '(a)', end=7, err=10) dummy
            go to 5
7         continue
        end if

        ierr = 0
        return
 
10      if (verbose.gt.0 .and. verbose .le. 3) then
          write(*,'(a)')'openf2__E> error opening/forwarding file'
          if (estop) stop 
        end if
        ierr = 4
        return
      end


cf ----------------------------------------------------------------------------
cf
cf    OPENF() uses OPENF2 assuming the sequential access, formatted form,
cf    VERBOSE=3, and a stop on error.
cf
cf    subroutine openf(iochn, fname, stat)
cf
cf ----------------------------------------------------------------------------

      subroutine openf(iochn, fname, stat)
        implicit none
        integer iochn, ierr
        character fname*(*), stat*(*)

        call openf2(iochn,fname,stat,'SEQUENTIAL','FORMATTED',3,.true.,
     &              ierr)
      
        return
      end


cf ----------------------------------------------------------------------------
cf
cf    PREPFIL() finds whether INFIL with or without .Z exists, and possibly 
cf    uncompresses the compressed file and so prepares it for the I/O 
cf    operations. COMPRESSED returns .T. if the file was compressed.
cf    FNAME returns INFIL without .Z.
cf
cf    subroutine prepfil(infil,compressed,fname)
cf
cf ----------------------------------------------------------------------------

      subroutine prepfil(infil,compressed,fname)
        implicit none 
        character fname*(*), zfname*(100), cmd*(112), infil*(*)
        logical compressed, filexs

        call zfnam(infil, zfname)
        call nozfnam(infil, fname)

        if (filexs(fname)) then
          compressed = .false.
        else
          if (filexs(zfname)) then
            cmd = 'uncompress '
            cmd(12:) = zfname
            call system(cmd)
            compressed = .true.
          end if
        end if

        return
      end


cf ----------------------------------------------------------------------------
cf
cf    UNPREPFIL() puts the file in the original state; if it was compressed,
cf    (COMPRESSED = .T.) it compresses it. FNAME can be with or without the
cf    .Z extension.
cf
cf    subroutine unprepfil(fname,compressed)
cf
cf ----------------------------------------------------------------------------

      subroutine unprepfil(fname,compressed)
        implicit none
        character fname*(*), cmd*(256), nozf*(255)
        logical compressed, filexs

        if (compressed) then
          cmd = 'compress -f '
          call nozfnam(fname,nozf)
          cmd(13:) = nozf
          if (filexs(nozf)) call system(cmd)
        end if

        return
      end


cf ----------------------------------------------------------------------------
cf
cf    FILEXS() returns .T. if file FNAME exists.
cf
cf    logical function filexs(fname)
cf
cf ----------------------------------------------------------------------------

      logical function filexs(fname)
        implicit none
        character fname*(*)
        logical exst
        inquire(file=fname, exist=exst)
        filexs=exst
        return
      end


      subroutine zfnam(infil,zfname)
        implicit none 
        integer lenr, lr
        character infil*(*), zfname*(*)

        lr = lenr(infil)
        if (lr .ge. 2) then
          if (infil(lr-1:lr) .eq. '.Z') then
            zfname=infil(1:lr)
          else
            zfname=infil(1:lr) // '.Z'
          end if
        else
          zfname=infil(1:lr) // '.Z'
        end if

        return
      end


      subroutine nozfnam(infil,fname)
        implicit none 
        integer lenr, lr
        character infil*(*), fname*(*)

        lr = lenr(infil)
        if (lr .ge. 2) then
          if (infil(lr-1:lr) .eq. '.Z') then
            fname=infil(1:lr-2)
          else
            fname=infil(1:lr)
          end if
        else
          fname=infil(1:lr)
        end if

        return
      end
