<H1>Joy - protein structure and alignment analysis</H1>
<H2>SYNOPSIS</H2>
<B>joy</B> <I>-options</I> <I>file</I>
<H2>DESCRIPTION</H2>
<P>
<I>Joy</I> is an analysis and formatting program for multiple protein sequence alignments
or single protein structures. It produces a number of files that are used
either directly or by other programs.
There are a large number of options, but the defaults are usually what you will
want to do to a basic alignment. the way that the program is used is;
<P>
<PRE>
joy -options file
</PRE>
<P>
The options may be omitted if you are happy with the default ones.
(see below for a description of these).
<P>
<UL>
<LI>The default extension for the alignment file is
<I>.ali</I>,
<LI>the default extension for a <B>raw</B> PDB format coordinate file is
<I>.atm</I>,
<LI>the default extension for a <B>preprocessed</B> PDB format coordinate file is
<I>.atm</I>
<LI>the extension for a template file is
<I>.tem.</I>
</UL
<P>
It is your responsibility to make sure that the PDB file is o.k.,
<I>i.e</I>.
it should not have alternate atoms, hydrogens
<I>etc</I>.
for this reason it is recommended that your PDB file has a
<I>.pdb</I>
extension as input to 
<I>joy</I>,
this means that the PDB file will be preprocessed (to a
<I>.atm</I>
file) before use.
<P>
If your files follow this simple naming convention then you can drop the
extension from the command line, as in.
<P>
<PRE>
joy gamma
</PRE>
<P>
What
<I>joy</I>
does next depends on the existence of various files, but you should get
something on standard output.
In the above example the first file searched for is the alignment
file
<I>gamma.ali</I>
If this file exists then it is read and processed. If the
<I>.ali</I>
file does not exist then
<I>joy</I>
will look for the file
<I>gamma.seq</I>
if this exists it is used, if this does not exist then
<I>Joy</I>
searches for
<I>gamma.atm,</I>
in the current directory, this file should be a standard
PDB format file. If this
<I>.atm</I>
file is missing
<I>joy</I>
will check for a file with a
<I>.pdb</I>
extension, this file will then be converted to a
<I>.atm</I>
file with the filter
<I>pdb2atm.</I>
The program will then create a
<I>.seq</I>
file from its contents and do the processing as before. Obviously any existing
<I>.seq</I>
file is overwritten if you explicitly specify
<I>gamma.atm</I>
file from the command line.
<P>
If you want to include structural information into the analysis the program
is smart enough to try to calculate any missing data as
long as you have the corresponding 
<I>.atm</I>
for all entries in the alignment in the current directory (and 
<I>joy</I>
is correctly installed). This
feature relies on the presence of the programs
<I>psa, hbond, sstruc, pdb2atm,</I>
and
<I>atm2seq</I>
in your path. See below in the section on data files for more details.
</P>
The default file extensions used by 
<I>joy</I>
are detailed below.
<P>
<CENTER>
<TABLE BORDER>
<TR><TD><B>extension</B><TD><B>contents</B></TR>
<TR><TD>pdb<TD>Raw PDB format coordinates</TR>
<TR><TD>atm<TD>Processed PDB format coordinates</TR>
<TR><TD>hbd<TD>Hydrogen-bonding data</TR>
<TR><TD>psa<TD>Accessibility data</TR>
<TR><TD>sst<TD>Secondary structure data</TR>
<TR><TD>seg<TD>Segment definition file</TR>
<TR><TD>lbl<TD>Label data for a structure</TR>
<TR><TD>ali<TD>Alignment</TR>
<TR><TD>seq<TD>Sequence</TR>
<TR><TD>sub<TD>Substitution data</TR>
<TR><TD>tem<TD>File containing a `template' representation of structure</TR>
<TR><TD>tex<TD>LaTeX file containing alignment</TR>
</TABLE>
</CENTER>
<P>
One of the files that 
<I>joy</I>
produces is a file with a 
<I>.sub</I>
extension, this contains a breakdown of residue substitutions classified
according to the local environment. As you would expect this data is quite
sparse, so there is an ancillary program called 
<I>summer</I>
to merge the data from many datasets.
The data produced by 
<I>summer</I>
is then used by a number of other programs.
<P>
Another file produced by the program, usually with a 
<I>.tex</I>
extension, can be used to produce a pretty alignment on a typesetter.
This file is then simply processed with 
<I>latex</I>
to get a nicely formatted alignment.
The 
<I>.tem</I>
file is the main input to the
<I>qslave,</I>
and
<I>pslave</I>
template alignment programs.
<P>
<I>Joy</I>
has a large number of options, to see the current ones, simply type joy at
the command line and the options will be listed, some of the more important
options are:
<P>
<UL>
<LI>
<B>-f</B>
This option tells the program to use all structural data.
It should be obvious
that this option is incompatible with the <B>s</B> option. With this option you
will produce files with
<I>.tem</I>
and
<I>.sub</I>
extensions, their contents are self documenting. The
<I>.out</I>
file contains a breakdown of the composition of the sequences
and alignment, this should be examined before you do
anything further with the data; it should be very obvious if anything is wrong.
The file also contains a list of the unusual residue environments within a
structure.
<LI>
<B>-g</B>
Using this option it is possible to print only a small portion of the alignment,
but still display each residue in its proper structural environment. The control data comes from a separate file with the same root as the alignment file and a 
<I>.seg</I>
extension. The format of the
<I>.seg</I>
file is: (A10, 1X, A10, 1X, A5, 1X, A5), with the first
field being the sequence title to be used in the alignment,
the second field being the source of the data,
the third field being the segment start residue number and
the final field being the segment end residue number.
You should be very careful to check the output of this option.
An example of a segment file is:
<PRE>
1gcr       1gcr          1    45 
2gcr       2gcr          1    45 
</PRE>
<LI>
<B>-h</B>
Print a help message on the standard output. This contains many options not
documented here so if you can't do something try this.
<LI>
<B>-l</B>
Have 
.I joy
produce a `typesettable' form of the alignment in
the \*(LX typesetting language. This alignment is formatted in
blocks of 50 positions and the program tries to optimize the layout on the page.
Structural properties can be shown on the alignment (depending on the options
<B>s\fR or <B>f\fR). A key for the character coding is shown later in this
document. Earlier versions of 
.I joy
required that \*(LX style files were in the current directory when
processing the 
.I .tex
file. This is
.B not
the case now. If they are present they will ruin everything.
<LI>
<B>-n</B>
Run the program without consistency checks on the data.
Usually the program checks that all the data files agree with the corresponding sequence in the alignment file.
This can be useful if you are in a hurry to ignore any inconsistencies.
Use this option with caution, the results may be incorrect and it will be
your fault.
<LI>
<B>-s</B>
Use only sequence data for the formatting and analysis, this should be used
for simple, but nicely formatted and numbered, sequence alignments.
<LI>
<B>-H</B>
Use Helvetica type fonts in \*(LX file. The default is to use Times-Roman fonts.
<LI>
<B>-L</B>
Format \*(LX file in landscape type format, this is incompatible with the
<B>-P</B> option.
<LI>
<B>-P</B>
Format \*(LX file in portrait type format, this is incompatible with the
<B>-L\fP option. (This is the default behavoir of the program).
<LI>
<B>-V</B>
Print the version number of 
.I joy
on the standard output.
<LI>
<B>-v</B>
Verbose mode, tell the user what is going on at each stage of the program.
You should try this option if things are not working, it should allow you
to identify quite precisely where your mistake is.
</UL>

<H2>FORMAT OF THE ALIGNMENT FILE</H2>
.P
The program uses a 
.I pir(1)
type format for alignment files, with a few
extensions to allow easy labelling of the alignment.
The only restriction is that all sequences (including N- and C-terminal
insertion codes) should be the same length, and that they are formatted
in blocks of 75. This restriction will be removed shortly.
See the documentation of
.I pir
for further details.
A blank line (or end of file) acts as a signal to stop
reading sequences.
Remember the program does no alignment; what you put in is what you get out.
An example alignment file (for the crystallin family) is:
<PRE>
>P1;1gcr
structure
--GKITFYEDRGFQGHCYECSSDCPNLQP-YFSRCNSIRVDSGCWMLYERPNYQGHQYFLRRGDYPDYQQWMGF-
-NDSIRSCRLIPQHTGTFRMRIYERDDFRGQMSEITD-DCPSLQDRFHLSEVHSLNVLEGSWVLYEMPSYRGRQY
LLRPGEYRRYLDWGAMNAKVGSLRRVMDFY-*
>P1;2gcr
structure
--GKITFYEDRGFQGRHYECSSDHSNLQP-YFSRCNSIRVDSGCWMLYEQPNFTGCQYFLRRGDYPDYQQWMGF-
-SDSVRSCRLIP-HTSSHRLRIYEREDYRGQMVEITE-DCSSLQDRFHFSDIHSFHVMEGYWVLYEMPNYRGRQY
LLRPGDYRRYLDWGAANARVGSLRRAVDFY-*
>P1;1bb2      
structure
LNPKIIIFEQENFQGHSHELNGPCPNLKETGVEKAGSVLVQAGPWVGYEQANCKGEQFVFEKGEYPRWDSWTSSR
RTDSLSSLRPIKVDSQEHKITLYENPNFTGKKMEVIDDDVPSFHAHGYQEKVSSVRVQSGTWVGYQYPGYRGLQY
LLEKGDYKDSGDFGAPQPQVQSVRRIRDMQW*
</PRE>
By default, underneath the alignment is the consensus secondary structure.
It should be obvious what it all means, (if it isn't, then what can you
expect to gain from using the program).
The definition of `consensus' is that a fraction of greater than 0.7 is
in a particular conformational state at a position. If you want to
change this fraction there is a hidden flag so you can fiddle things.
Also underneath the alignment is a series of bullets showing the positions
of consensus buried residues, You can turn this feature off if you want to.
.P
The current limitations on the size of various things the user is likely to encounter are:
.P
.TS
center tab(:);
l l.
total length of alignment : 1000
total number of structures : 35
total number of `plain' sequences : 30
number of text strings : 6
number of label strings : 3
.TE
You may want to mix `featured' and `plain' sequence in the formatted alignment,
to do this you simply prefix the title of the sequence with a `*', this marks
the sequence as simply a string of characters, and no data files are required.
A comment line may be added to the alignment file by preceding it with a `#'.
.P
<H2>KEY TO FORMATTED ALIGNMENT</H2>
.P
The key for the featured alignment is as follows:
.sp 0.5
.P
UPPERCASE :	solvent inaccessible
.br
lowercase :	solvent accessible
.br
<B>Bold\fP :	H-bond to amide proton
.br
.us "underline"
:	H-bond to mainchain carbonyl
.br
tilde (~) :	H-bond to other sidechain
.br
dot (\u.\d) :	H-bond to heterogen
.br
breve (\(be) :	cis-peptide bond
.br
cedilla (\(c,) :	half cystine
.sp 0.5
.P
So, for example, the residue $fat D under$ is an aspartic acid that is
buried and hydrogen bonded to both a mainchain amide proton and a
mainchain carbonyl; the residue $italic s tilde$ is a surface serine
in a positive \(*f conformation hydrogen bonded to another sidechain.
<H2>DATA FILES</H2>
.P
To produce a `featured' alignment you must have a set of data files for each
sequence in the alignment. These must have the same name as the title of the
sequence in the alignment and be present in the same directory as the
alignment file. See notes concerning the use of the <B>g\fP option.
If needed joy can create all the data files automatically (see earlier).
.P
The hydrogen bond data comes from a file with a 
.I .hbd
extension produced
by the program 
.I hbond.
You should not try to directly interpret the
contents of this file, as there are some non-valid data in these files that is
filtered out by 
.I joy.
See the relevant documentation of 
.I hbond
for further information.
.P
The accessibility data comes from a file with a 
.I .psa
extension, produced the 
.I psa
program. By default the cutoff value for deciding if a residue is inaccessible
is a relative total sidechain accessibility of 7%; this can be changed by
using a command line flag to
.I joy.
.P
The secondary structure and \(*W data comes from the 
.I .sst
file produced by the 
.I sstruc
written by David Smith.
.P
<H2>CAVEATS</H2>
.P
There is a bug in the \*(LX output of residues that have many `features', for
example if a residue is hydrogen bonded to another sidechain and has
a \fIcis\fP-peptide bond then the tilde and breve will not line up above the
letter. This is probably due to a problem with nesting of parentheses.
If it gives you problems edit the 
.I .tex
file.
.P
There is a bug in the numbering routine, if you try to number more
than 1 structure then the number labels may appear in the wrong rows
of the table, this should be fixed.
.P
Occasionally you will run out of memory in \*(LX, if this happens you will
have to put a line \\clearpage, just before the alignment block that causes
latex to run out of memory, in the \fI.tex\fP file manually.
.P
There are bugs, many things do not probably work now. If you find anything
like this I will try to fix things by email (overingtonj@pfizer.com). The
parts of the code that almost certainly have broken are the substitution
table generation routines. I would exercsie extreme caution in using these.
.P
<H2>REFERENCING</H2>
.P
If you use
.I joy
and publish anything with it, it would be nice to be referenced. The
reference you should use is:
.P
J.P. Overington, M.S. Johnson, A. \(Svali and T.L. Blundell, 
(1990) ``Tertiary structural constraints on protein evolutionary diversity:
Templates, key residues and structure prediction'', \fIProc. Roy. Soc.
Lond.\fP, <B>241\fP, pp. 132-145.
.P
I will also write a small paper specifically on joy in the near future.
.P
<H2>SEE ALSO</H2>
\fIsummer(1), psa(1), orgasmus(1), sstruc(1),
atm2seq(1), hbond(1), pdb2atm(1), pir(4)\fP
.P
The program 
.I joy
is highly complementary to the profile programs of Mark Johnson, he should
be contacted for details of these.

<H2>RELEASE LEVEL</H2>
This document describes 
<I>joy</I>
version 2.7 and later. (The manual page has not been updated since then).
