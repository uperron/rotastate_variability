      subroutine KABWUV(SEQ,LENALI,NSEQ,SITEFLAG)
C
C copyright (c) 1988-1991 John Overington
C part of joy
C
C Calculates the Kabat and Wu variability index for an alignment
C also prints out entropy measure of variance
C
C Handling of gapes is kludgy, if site of lone insertion then variability
C is low even though intuitively the position is variable
C
C also prints out alignment and various statistics as to residues occuring at
C each position
C
      include 'joy.h'
C
      character*22 STRING
      character*1 SEQ(MAXRES,MAXSEQ), RES(22), INV
      character*2 SITEFLAG(MAXRES)
      integer LENALI, NSEQ, NOBS(22), NFOUND, I, J, K, NUMAA, NOBSMAX
      real VAR(MAXRES), ENT(MAXRES), POBS(22), ENTROPY, VARAVE, ENTAVE
C
      data NUMAA /22/
      data RES /'A','C','D','E','F','G','H','I','K','L','M',
     -          'N','P','Q','R','S','T','V','W','Y','J','-'/
C
      ENTAVE=0.0
      VARAVE=0.0
C
      if (LENALI .gt. MAXRES) then
        call ERROR('joy: alignment too long for variability calculation',' ',1)
      end if
      if (NSEQ .gt. MAXSEQ) then
        call ERROR('joy: too many sequences for variability calculation',' ',1)
      end if
C
C find no of different aa's at each position also their frequencies
C
      write (STDOUT,'(/,
     + ''analysis of positional residue variability'')')
      write (STDOUT,
     +'(''pos  ent   K+W inv Nobs ------ observed ------  alignment'',
     +/)')
      do I=1,LENALI
        STRING='                      '
        VAR(I)=0.0
        NFOUND=0
        do J=1,NUMAA
          NOBS(J)=0
        end do
        NOBSMAX=-1
        do J=1,NSEQ
          do K=1,NUMAA
            if (SEQ(I,J).eq.RES(K)) then
              NOBS(K)=NOBS(K)+1
              goto 1
            end if
          end do
1         continue
        end do
        do J=1,NUMAA
          if (NOBS(J).gt.0) then
            STRING(J:J)=RES(J)
            NFOUND=NFOUND+1
            if (NOBS(J).gt.NOBSMAX) then
              NOBSMAX=NOBS(J)
            end if
          end if
        end do
        VAR(I)=real(NFOUND)/(real(NOBSMAX)/real(NSEQ))
        do J=1,NUMAA
          POBS(J)=real(NOBS(J))/real(NSEQ)
        end do
        ENT(I)=ENTROPY(POBS,NUMAA)
        if (ENT(I).lt.1.0E-8) then
          INV=SEQ(I,1)
        else
          INV=' '
        end if
C
C new stuff for site analysis
C
        write (STDOUT,'(I3,F5.2,F6.2,2X,A,1X,1X,I2,3X,A,2X,80A,1X,A)')
     -        I,ENT(I),VAR(I),INV,NFOUND,STRING,(SEQ(I,K),K=1,NSEQ),SITEFLAG(I)
        VARAVE=VARAVE+VAR(I)
        ENTAVE=ENTAVE+ENT(I)
      end do
      VARAVE=VARAVE/real(LENALI)
      ENTAVE=ENTAVE/real(LENALI)
      write (STDOUT,'(''ave'',F5.2,F6.2)') ENTAVE, VARAVE
C
      return
      end
C
C #############################################################################
C
      real function ENTROPY(P,N)
C
C calculates the entropy of a distribution P of length N
C
      real P(N), SMALL
C
      data SMALL /1.0E-15/
C
      entropy=0.0
C
      do I=1,N
        if (P(I).gt.SMALL) then
          ENTROPY=ENTROPY+(P(I)*(LOG(P(I))))
        end if
      end do
      ENTROPY=-ENTROPY
C
      return
      end
