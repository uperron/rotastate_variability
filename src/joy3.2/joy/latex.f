C ########################################################################
C
C Latex specific subroutines for joy
C
C copyright (c) 1988-1991 John Overington
C part of joy
C
C ########################################################################
C
      subroutine LABLFI(NSPACE)
C
C Writes necessary text to unit 1 to prepare for end of LATEX table
C
      integer NSPACE
C
      do I=1,NSPACE
        write (1,'(''\\'')')
      end do
      write (1,'(''\end{tabular}'')')
C
      return
      end
C
C ###########################################################################
C
      subroutine LABLST(LANDSCAPE)
C
C Writes necessary text to unit 1 to prepare for the LATEX table
C [p] at end means put all floats on separate page, don't worry about page
C breaks
C
      logical LANDSCAPE
C
      if (LANDSCAPE) then
        write (1,'(''\begin{tabular} [p] {p{2cm} *{82}{p{0.3cm}} }'')')
      else
        write (1,'(''\begin{tabular} [p] {p{2cm} *{52}{p{0.3cm}} }'')')
      end if
C
      return
      end
C
C =============================================================================
C
      subroutine LAEND()
C
      write (1,'(''\end{document}'')')
C
      return
      end
C
C =============================================================================
C
      subroutine LAPREP(FILENAME,LANDSCAPE,HELVETICA)
C
C Writes text to unit 1 to prepare the necessary environment for the alignment
C
C PMBFIX defines a bold italic macro
C
      implicit none
      character*(*) FILENAME
      character*22 EMAIL
      character*4 VERSION
      character*9 SDATE
      character*8 STIME
      integer LASTCHAR
      logical LANDSCAPE, HELVETICA
C
      data EMAIL /'overingtonj@pfizer.com'/
C
      call DATE(SDATE)
      call TIME(STIME)
      write (1,'(''% produced by joy - version '',A,
     + '' - from file '',A,'' on '',A,1X,A)',err=943)
     + VERSION(), FILENAME(1:LASTCHAR(FILENAME)), SDATE, STIME
      write (1,'(''%'')')
      write (1,'(''% copyright 1989-1995 - John Overington'')')
      write (1,'(''% email: '',A)') EMAIL
      write (1,'(''%'')')
      write (1,'(''% start of setup'')')
      call PMBFIX()
      write (1,'(''\documentstyle{article}'')')
      write (1,'(''\voffset -0.5in'')')
      write (1,'(''\textheight 9.5in'')')
      write (1,'(''\linewidth 6.7in'')')
      write (1,'(''\parindent 0in'')')
      write (1,'(''\oddsidemargin 0.0in'')')
      write (1,'(''\evensidemargin 0.0in'')')
      write (1,'(''\def\baselinestretch{1.4}'')')
      write (1,'(''\newcommand {\subs}[1] '',
     +           ''{\mbox{${}_{\mbox{\tiny #1}}$}}'')')
C
C if landscape format then have to fanny about with defaults
C
      if (LANDSCAPE) then
        write (1,'(''%'')')
        write (1,'(''% file is in landscape format'')')
        write (1,'(''\setlength{\topmargin}{-2cm}'')')
        write (1,'(''\setlength{\textheight}{22cm}'')')
        write (1,'(''\setlength{\oddsidemargin}{-1.5cm}'')')
        write (1,'(''\setlength{\evensidemargin}{-1.5cm}'')')
        write (1,'(''\setlength{\textwidth}{26cm}'')')
      else
        write (1,'(''%'')')
        write (1,'(''% file is in portrait format'')')
      end if
C
      write (1,'(''\begin{document}'')')
      write (1,'(''\pagestyle{empty}'')')
      write (1,'(''\tabcolsep 0.00in'')')
C
C new bit for helvetica font (maybe funny for bold)
C
      write (1,'(''%'')')
      if (HELVETICA) then
      write (1,'(''% file is in Helvetica-style fonts'')')
      write (1,'(''\sf'')')
      else
        write (1,'(''% file is in Times Roman-style fonts'')')
      end if
C
      write (1,'(''%'')')
      write (1,'(''% end of setup'')')
      write (1,'(''% ============================================='')')
C
      return
C
943   call ERROR('joy: cannot write to LaTeX file',' ',0)
      end
C
C =============================================================================
C
      subroutine PMBFIX()
C
C Provides a fix for LATEXs inability to do a bold italic character
C
      write (1,'(''% define a macro to get italic bold'')')
      write (1,'(''\def\pmb#1{\setbox0=\hbox{#1}%'',/,
     -           3X,''\kern-.025em\copy0\kern-\wd0'',/,
     -           3X,''\kern.05em\copy0\kern-\wd0'',/,
     -           3X,''\kern-.025em\raise0.0433em\box0 }'')')
C
      return    
      end
C
C ############################################################################
C
      subroutine JOYKEY()
C
C Writes out a key to joy on a spearate page
C
      write (1,'(''% key to alignment'')')
      write (1,'(''\newpage'')')
      write (1,'(''\tabcolsep 0.1in'')')
      write (1,'(''\begin{center}'')')
      write (1,'(''\begin{tabular} [t] {llc}'')')
      write (1,'(''\multicolumn {3}{c}{Key to {\bf J}\u{o}{\it Y} '',
     +           ''alignments} \\'')')
      write (1,'(''\\'')')
      write (1,'(''solvent inaccessible & UPPER CASE & X \\'')')
      write (1,'(''solvent accessible & lower case & x \\'')')
      write (1,'(''positive $\phi$ & {\it italic} & {\it x} \\'')')
      write (1,'(''{\it cis}-peptide & breve & \u{x} \\'')')
      write (1,'(''hydrogen bond to other sidechain & tilde& '',
     +           ''\~{x} \\'')')
      write (1,'(''hydrogen bond to mainchain amide & {\bf bold}& '',
     +           ''{\bf x} \\'')')
      write (1,'(''hydrogen bond to mainchain carbonyl & '',
     +           ''\underline{underline} & \underline{x} \\'')')
      write (1,'(''disulphide bond & cedilla & \c{x} \\'')')
      write (1,'(''\end{tabular}'')')
      write (1,'(''\end{center}'')')
C
      return
      end
C
C ========================================================================
C
      subroutine TR2LATEX(IN)
C
C translates a limited number of troff controls to their corresponding
C LaTeX equivalents
C
C N.B. Input string is overwritten
C
C A list of characters (and their LaTeX replacements are)
C
C \(*a     $\alpha$
C \(*b     $\beta$
C \(em     ---
C \(fm     $'$
C \d\s-2   \subs{
C \u\s0    }
C \f2      {\it 
C \f1      }
C 
      character*(*) IN
      character*80 TMP
C
      TMP(1:)=IN(1:)
C
C deal with things one at a time
C
C alpha
C
      NP=INDEX(IN,'\(*a')
      if (NP .gt. 0) then
        TMP(1:NP-1)=IN(1:NP-1)
        TMP(NP:NP+7)='$\alpha$'
        TMP(NP+8:)=IN(NP+4:)
        IN(1:)=TMP(1:)
      end if
C
C beta
C
      NP=INDEX(IN,'\(*b')
      if (NP .gt. 0) then
        TMP(1:NP-1)=IN(1:NP-1)
        TMP(NP:NP+6)='$\beta$'
        TMP(NP+7:)=IN(NP+4:)
        IN(1:)=TMP(1:)
      end if
C
C em
C
      NP=INDEX(IN,'\(em')
      if (NP .gt. 0) then
        TMP(1:NP-1)=IN(1:NP-1)
        TMP(NP:NP+2)='---'
        TMP(NP+3:)=IN(NP+4:)
        IN(1:)=TMP(1:)
      end if
C
C subscript start
C
      NP=INDEX(IN,'\d\s-2')
      if (NP .gt. 0) then
        TMP(1:NP-1)=IN(1:NP-1)
        TMP(NP:NP+5)='\subs{'
        TMP(NP+6:)=IN(NP+6:)
        IN(1:)=TMP(1:)
      end if
C
C subscript end
C
      NP=INDEX(IN,'\s0\u')
      if (NP .gt. 0) then
        TMP(1:NP-1)=IN(1:NP-1)
        TMP(NP:NP)='}'
        TMP(NP+1:)=IN(NP+5:)
        IN(1:)=TMP(1:)
      end if
C
C italic
C
      NP=INDEX(IN,'\f2')
      if (NP .gt. 0) then
        TMP(1:NP-1)=IN(1:NP-1)
        TMP(NP:NP+4)='{\it '
        TMP(NP+5:)=IN(NP+3:)
        IN(1:)=TMP(1:)
      end if
C
C roman
C
      NP=INDEX(IN,'\f1')
      if (NP .gt. 0) then
        TMP(1:NP-1)=IN(1:NP-1)
        TMP(NP:NP)='}'
        TMP(NP+1:)=IN(NP+3:)
        IN(1:)=TMP(1:)
      end if
C
C footmark
C
      NP=INDEX(IN,'\(fm')
      if (NP .gt. 0) then
        TMP(1:NP-1)=IN(1:NP-1)
        TMP(NP:NP+2)='$''$'
        TMP(NP+3:)=IN(NP+4:)
        IN(1:)=TMP(1:)
      end if
C
      return
      end
