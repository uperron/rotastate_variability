      subroutine LINKER(M,D,Q,P,T)
C
      include 'joy.h'
C
      real D(MAXSEQ,MAXSEQ), T(MAXSEQ-1)
      integer P(MAXSEQ-1), Q(MAXSEQ-1), R, S
      logical ENDE
      integer M, K, M1, J, I, L, LL, N
      real V, U, H
C
      if (M .le. 1) return
      M1=M-1
      V=1.E12
C
C convert PIDs to distances
C
      do K=1,M
        do I=1,M
          D(K,I)=-100.0*LOG(D(K,I)/100.0)
        end do
      end do
C
      do K=1,M1
        Q(K)=0
        P(K)=0
        T(K)=V
      end do
      J=M
C
      do 4 I=1,M1
        U=V
        do 3 K=1,M1
          if (Q(K) .ne. 0) goto 3
          R=min0(J,K)
          S=max0(J,K)
          H=D(R,S)
          if (H .ge. T(K)) then
            goto 2
          end if
          T(K)=H
          P(K)=j
 2        if (T(K) .ge. U) then
            goto 3
          end if
          U=T(K)
          N=K
 3      continue
C John: J is 0 with 'joy tst-1.ali' so I have this 'fix' here because I am
C       to lazy to find out the problem:
        J=N
        Q(max(J,1))=1
c        Q(J)=1
4     continue
C
      do I=1,M1
        Q(I)=I
      end do
C
      ENDE=.false.
      do I=2,M1
        if (ENDE) return
        ENDE= .true.
        LL=M1-I+1
        do 6 L=1,LL
          N=L+1
          if (T(L) .le. T(N)) goto 6
          V=T(N)
          T(N)=T(L)
          T(L)=V
          J=Q(N)
          Q(N)=Q(L)
          Q(L)=J
          J=P(N)
          P(N)=P(L)
          P(L)=J
          ENDE=.false.
6       continue
      end do
C
      return
      end
