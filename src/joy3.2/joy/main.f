       program JOY 
C 
C To read an alignment from COMPARER (or other sources) and add structural
C information to it, later versions allow a flexible analysis of the
C conservation of various structural features across a family of proteins.
C Associated programs to create the data files required are:             
C
C HBOND   --- JPO (Unpublished)
C SSTRUC  --- DKS (Unpublished)
C PSA     --- AS  (Unpublished)
C
C The program can create substitution matrices from the data, 
C there is a further suite of analysis programs to accompany this program. 
C The most important output file is probably .sub, this contains all the 
C environment specific substitution matrices that are used by later programs, 
C notably SUMMER, a summation program, for the merging of many differing 
C datasets, and HOPE, an ab initio protein structure program.
C If you want to you can format your alignment either as the plain one-letter
C code, or as an alignment that shows the environment of the residue.
C
C The program tries quite hard to trap bad data and it should tell you where
C it went wrong. The data files should be as produced by the relevant programs
C if you put extra lines or delete anything you will end up in trouble. The 
C one annoying thing about the program is the format of the alignment file. This
C is described later (see RDALI), but to do anything else would require a 
C lot of work and error trapping would be unnecessarily complex. To process the
C output featured alignment files a rudimentary knowledge of latex or troff is
C required.
C
C See the files README and ChangeLog for more details
C
C To enable the calculation of cap substitution data uncomment the lines
C starting with Ccap
C
      include 'joy.h'
C
      character*(MAXOPT) OPTSTR
      character*(MAXFILELEN) NUMFILE(MAXSEQ), ALFILE, TEXFILE
      character*(MAXFILELEN) SUBFILE, TEMFILE
      character*255 RESFNAM
C PDB code, residue range, and residue range chain id's for -d option:
      character*32 FAMCLASS
      character*10 PDBCOD(MAXSEQ)
      character*5 RANGE(2,MAXSEQ)
      character*1 ALNCHN(2,MAXSEQ)
      character*80 STRING, FAMNAME
      character*61 TITLE(MAXTITLE)
      character*30 BUFFER
      character*21 RES
      character*40 COMPDATE
      character*15 DEFOPT
      character*10 SEQNAM(MAXNEWSEQ), STRNAM(MAXSEQ)
      character*10 FILBIG(MAXSEQ)
      character*8 ITOA ! function, careful !!
      character*6 ORDER(MAXSEQ+MAXNEWSEQ+MAXLAB+MAXTEXT)
      character*5 RESBEG(MAXSEQ), RESEND(MAXSEQ)
      character*5 RESLBL(MAXRES,MAXSEQ), LBL(MAXRES,MAXSEQ)
      character*4 VERS, LIBVERS
      character*3 LABEL(MAXRES,MAXLAB)
      character*1 RAWSEQ(MAXRES,MAXSEQ), NEWSEQ(MAXRES,MAXNEWSEQ)
      character*1 SEQ(MAXRES,MAXSEQ), TEXT(MAXRES,MAXTEXT)
      character*1 SEQ2(MAXRES,MAXSEQ), SHEET(MAXRES,MAXSEQ)
C
C new bit for fall off
      character*60 TEXTBIT
      character*5 BITC1, BITC2
      real BIT1, BIT2
C
      integer IARGC, LENFIL(MAXSEQ)
      integer LENSEQ(MAXSEQ), NHBSIDE(MAXRES,MAXSEQ)
      integer NHBMAIN(MAXRES,MAXSEQ)
      integer RDISC(NUMFEAT,MAXRES,MAXSEQ), RNULL(NUMFEAT,MAXRES)
      integer NSUBSTSTRNEW(MAXSEQ,MAXNEWSEQ)
      integer NENVIR(NUMEXAM,21), IRESBEG(MAXSEQ), IRESEND(MAXSEQ)
      integer NLBL(MAXSEQ)
      integer NMUTID(22,22,10)
      integer NMUTIDAC(22,22,10,2)
      integer NMUT(22,22,4,2,2,2,2), NMUTALL(22,22)
      integer NNN(21,4,2,2,2,2), NSUBST(MAXSEQ,MAXSEQ)
      integer NSUBTMP(4), NSUBSTSTR(4,MAXSEQ,MAXSEQ)
      integer NSUBTMP2(2), NSUBSTACC(2,MAXSEQ,MAXSEQ)
      integer NRESTOT, LENROOT, NOPT, LENGTH, NUMENT, NUMNEWSEQ, NNP
      integer NSTR, NROPT, NIFIL, NUMNUMFILE
      integer I, J, K, L, M, N, II, KK
      integer OOI(MAXRES,MAXSEQ)
      integer EFITYP(MAXRES,MAXSEQ), EFIMOV, NAVELEN
C
      logical ALPHA(MAXRES,MAXSEQ), BETA(MAXRES,MAXSEQ)
      logical POSPHI(MAXRES,MAXSEQ), CIS(MAXRES,MAXSEQ)
      logical SCHBO(MAXRES,MAXSEQ), SCHBN(MAXRES,MAXSEQ)
      logical SSHB(MAXRES,MAXSEQ), SHETHB(MAXRES,MAXSEQ)
      logical MMOBOND(MAXRES,MAXSEQ)
      logical SSBOND(MAXRES,MAXSEQ), MMNBOND(MAXRES,MAXSEQ)
      logical ACCESS(MAXRES,MAXSEQ), AUTOLABEL(MAXSEQ)
      logical USENUMFILE(MAXSEQ)
      logical USENUMBER, USESTRUCT, USENEWSEQ
      logical FOUNDNUM, ALLLABEL, SWAPLBL
      logical ANALYZE, PRINT, TEMPLATE, TIDY, AUTONUM, AUTOSEC
      logical AUTOACC, CHECK, HETTOSIDE, DOKEY
      logical DOCAP, DOSUBST, DOINVARI, VERSOUT, ALLSEC
      logical DOCHKSTR, DOENV, DOPDBCHK, THRTEN(MAXRES,MAXSEQ)
      logical PIHEL(MAXRES,MAXSEQ), DOEFI, DOKABWU, LANDSCAPE
      logical HELVETICA, FORCE, ONEPERPAGE, ACCMCH, KITSCHORDER
C
C new stuff
C
      logical SITE(MAXRES,MAXSEQ), USESITE
C
      logical PCPROPDIFF
C
      real PIDENT(MAXSEQ,MAXSEQ), PIDENTSTR(4,MAXSEQ,MAXSEQ)
      real TOTACC(MAXSEQ)
      real PIDENTACC(2,MAXSEQ,MAXSEQ), PIDTMP(4), PIDTMP2(2)
      real PIDSTRNEW(MAXSEQ,MAXNEWSEQ), PDF, DEFACC, ACCVAL, ROPT(10)
      real PHI(MAXRES,MAXSEQ), PSI(MAXRES,MAXSEQ), RELACC(MAXRES,MAXSEQ)
      real DEFSITE
C
      real TOTAADIFF(MAXSEQ,MAXSEQ), TOTACCAADIFF(MAXSEQ,MAXSEQ), TOTSITDIFF(MAXSEQ,MAXSEQ)
      real SITEVAL(MAXRES,MAXSEQ), TOTSIT(MAXSEQ), SITVAL
      integer NCOMP,NSITECOMPID,NSITEACCCOMPID,NIDSITECOMP,NIDACCSITECOMP
      integer NSITE(5,MAXSEQ,MAXSEQ)
      character*2 SITEFLAG(MAXRES)
C
      integer INDSTR(MAXSEQ)
C
C The stuff for capping analysis
C
Ccap  character*(MAXFILELEN) CAPFILE
Ccap  integer NMUTHELNCAP(22,22,9), NMUTHELCCAP(22,22,9)
Ccap  integer NMUTSHTNCAP(22,22,9), NMUTSHTCCAP(22,22,9)
Ccap  integer IHEL(3,MAXHELIX,MAXSEQ), ISHT(3,MAXSHEET,MAXSEQ)
Ccap  integer NHEL(MAXSEQ), NSHT(MAXSEQ)
C
      character*34 IDENT
      data IDENT /'@(#) main.f - joy - (c) jpo - 1991'/
C
C Include data files
C
      LBIN=''
C
C ----------------------------------------------------------------------------
C
C Variable  Default       Description
C PRINT      FALSE     Output typesetter file
C USESTRUCT  TRUE      Add structural features to alignment
C ANALYZE    TRUE      Output analysis to output file
C TEMPLATE   TRUE      Output template creation file
C TIDY       TRUE      Remove blanks at end of sequences or not
C AUTONUM    TRUE      Automatically number the alignment
C AUTOSEC    TRUE      Automatically label secondary structure assignments
C AUTOACC    TRUE      Automatically label buried positions 
C USENEWSEQ  FALSE     Whether to try and improve matrices by counting residues
C                      just in the sequences.
C ALLLABEL   FALSE     Whether to add both structure and alignment labels
C SWAPLBL    FALSE     Whether to put structure label after structure
C CHECK      TRUE      Whether to stop on error in datafiles
C HETTOSIDE  TRUE      Treat h-bonds to het groups as sidechain-sidechain bonds
C DEFACC      7.0      default value for % cutoff in accessibility calculations
C                      (For reasons of this choice of 7% see Hubbard and
C                      Blundell 1987) Moved to USER
C KITSCHORDER FALSE    Use kitsch program to generate a sensible order for the
C                      alignment (will cause problems with fancy labels etc).
C
      data USESTRUCT /.true./
      data ANALYZE /.true./
      data TEMPLATE /.true./
      data TIDY /.true./
      data AUTONUM /.true./
      data AUTOSEC /.true./
      data AUTOACC /.true./
      data CHECK /.true./
      data DOKEY /.true./
      data HETTOSIDE /.true./
      data PRINT /.false./
      data USENEWSEQ /.false./
      data USENUMBER /.false./
      data ALLLABEL /.false./
      data SWAPLBL /.false./
      data ALLSEC /.false./
      data DOCAP /.false./
      data DOINVARI /.false./
      data DOSUBST /.false./
      data DOCHKSTR /.false./
      data DOPDBCHK /.false./
      data DOENV /.false./
      data DOKABWU /.false./
      data LANDSCAPE /.false./
      data HELVETICA /.false./
      data FORCE /.false./
      data ONEPERPAGE /.false./
      data ACCMCH /.false./
      data KITSCHORDER /.false./
      data NRESTOT /0/
      data DEFACC /7.0/
      data DEFSITE /14.0/                    ! 7.0 just for now
      data PCPROPDIFF /.true./
      data RES /'ACDEFGHIKLMNPQRSTVWYJ'/
C
      data OPTSTR /'          '/
      data DEFOPT /'flP       '/
C
      data USESITE /.false./
C
C Set global verbose option
C
      VERBOSE=.false.
C
      do I=1,MAXSEQ
	AUTOLABEL(I)=.false.
	USENUMFILE(I)=.false.
      end do
C
C Get user options and construct filenames etc.
C N.B. Be very careful about the input of a value for the sidechain area cutoff,
C it must be remembered that FORTRAN requires a decimal point to force the
C magnitude of the value (well it does the way that I've written it).
C
C New option code, N.B. some options are now only hardwired, but can always
C run interactively to do everything that you want. (Not any more !!)
C
C Changed code so that there are a default set of options, currently
C If the argument is a filename then these default options are used.
C
C Default for accessibility calculations and site
C
      ACCVAL=DEFACC
      SITVAL=DEFSITE
C
C program sepcific variables
C
      VERS=VERSION()
      LIBVERS=LIBVERSION()
      DATE=COMPDATE()
C
C If no arguments then print help message and exit
C Walk through the argument list
C Assume anything that does not begin with a '-' is a file name
C Load up the real options into OPTSTR (maximum of MAXOPT options)
C If find a number of the command line then store that in order
C potential bug -- Assume that a real filename will not have just numbers in it
C if it does then it will be read as a real argument. tough
C
      NOPT=0
      NROPT=0
      if (IARGC().eq.0) then
	call HELP()
	call EXIT(1)
      else
	do I=1,IARGC()
	  call GETARG(I,BUFFER)
	  if (BUFFER(1:1).eq.'-') then
	    do J=2,MAXOPT
	      if (ISPRINT(BUFFER(J:J))) then
		OPTSTR(NOPT+1:NOPT+1)=BUFFER(J:J)
		NOPT=NOPT+1
		if (NOPT .gt. MAXOPT) then
		  call ERROR('joy: too many options',ITOA(NOPT),3)
		end if
	      end if
	    end do
	  else if (ISDIGIT(BUFFER(1:1))) then
	    NROPT=NROPT+1
	    read (BUFFER,'(F5.2)',err=901) ROPT(NROPT)
	  else 
901         ALFILE=BUFFER
	  end if
	end do
      end if
C
C Initialize NMUTID matrices
C
      do I=1,10
        do J=1,22
          do K=1,22
            NMUTID(K,J,I)=0
          end do
        end do
      end do
      do L=1,2
        do I=1,10
          do J=1,22
            do K=1,22
              NMUTIDAC(K,J,I,L)=0
            end do
          end do
        end do
      end do
C
      do I=1,MAXRES
        SITEFLAG(I)='  '
      end do
C
C If no options then use default set
C
      if (NOPT .lt. 1) then
	OPTSTR=DEFOPT
      end if
C
      call SETOPT(OPTSTR,VERS,LIBVERS,DATE,NOPT,DEFACC,ACCVAL,SITVAL,USESTRUCT,
     +            ANALYZE,TEMPLATE,CHECK,AUTONUM,AUTOSEC,PRINT,
     +            USENEWSEQ,ALLLABEL,SWAPLBL,NROPT,ROPT,
     +            DOINVARI,DOCAP,DOSUBST,VERSOUT,ALLSEC,DOKEY,DOCHKSTR,
     +            DOPDBCHK,AUTOACC,DOEFI,LANDSCAPE,HELVETICA,FORCE,
     +            ONEPERPAGE,ACCMCH,KITSCHORDER,USESITE,DOKABWU)
C
      if (index(ALFILE,'.') .lt. 1) then
	if ((.not.ISPRINT(ALFILE(1:1))).and.(.not.VERSOUT)) then
	  call ERROR('joy: no file in command line',' ',1)
	end if
	if (index(ALFILE,' ').lt.1) then
	  call EXIT(0)
	end if
C
C first try .ali extension
C then a .seq extension
C then a .atm extension
C then a .pdb extension
C
	ALFILE=ALFILE(1:(index(ALFILE,' ')-1))//'.ali'
	if ( .not. FILEEXIST(ALFILE)) then
	  ALFILE=ALFILE(1:index(ALFILE,'.'))//'seq'
	  if (FILEEXIST(ALFILE)) then
	    ALFILE=ALFILE(1:index(ALFILE,'.'))//'seq'
	  else
	    ALFILE=ALFILE(1:index(ALFILE,'.'))//'atm'
	    if (FILEEXIST(ALFILE)) then
	      STRING=LBIN(1:LASTCHAR(LBIN))//'atm2seq '//
     +               ALFILE(1:index(ALFILE,'.'))//'atm > '//
     +               ALFILE(1:index(ALFILE,'.'))//'seq'//char(0)
	      write (STDERR,'(8X,A)') STRING(1:LASTCHAR(STRING))
	      call system(STRING)
	      ALFILE=ALFILE(1:index(ALFILE,'.'))//'seq'
	    else
	      ALFILE=ALFILE(1:index(ALFILE,'.'))//'pdb'
	      if (FILEEXIST(ALFILE)) then
		STRING=LBIN(1:LASTCHAR(LBIN))//'pdb2atm '//
     +                 ALFILE(1:index(ALFILE,'.'))//'pdb'//char(0)
		write (STDERR,'(8X,A)') STRING(1:LASTCHAR(STRING))
		call system(STRING)
		STRING=LBIN(1:LASTCHAR(LBIN))//'atm2seq '//
     +                 ALFILE(1:index(ALFILE,'.'))//'atm > '//
     +                 ALFILE(1:index(ALFILE,'.'))//'seq'//char(0)
		call system(STRING)
		ALFILE=ALFILE(1:index(ALFILE,'.'))//'seq'
	      end if
	    end if
	  end if
	end if
      else if (index(ALFILE,'.pdb').gt.0) then
	STRING='pdb2atm '//ALFILE(1:index(ALFILE,'.'))//'pdb'//char(0)
	write (STDERR,'(8X,A)') STRING(1:LASTCHAR(STRING))
	call system(STRING)
	STRING=LBIN(1:LASTCHAR(LBIN))//'atm2seq '//
     +         ALFILE(1:index(ALFILE,'.'))//'atm > '//
     +         ALFILE(1:index(ALFILE,'.'))//'seq'//char(0)
	write (STDERR,'(8X,A)') STRING(1:LASTCHAR(STRING))
	call system(STRING)
	ALFILE=ALFILE(1:index(ALFILE,'.'))//'seq'
      else if (index(ALFILE,'.atm').gt.0) then
	STRING=LBIN(1:LASTCHAR(LBIN))//'atm2seq '//
     +         ALFILE(1:index(ALFILE,'.'))//'atm > '//
     +         ALFILE(1:index(ALFILE,'.'))//'seq'//char(0)
	write (STDERR,'(8X,A)') STRING(1:LASTCHAR(STRING))
	call system(STRING)
	ALFILE=ALFILE(1:index(ALFILE,'.'))//'seq'
      else if (index(ALFILE,'.seq').gt.0) then
	continue
      else if (index(ALFILE,'.ali').gt.0) then
	continue
      else
	call ERROR('joy: bad file suffix',ALFILE,1)
      end if
C
C Catch various errors
C
      NNP=index(ALFILE,'.')
      if (NNP.gt.MAXFILELEN-3) then
	call ERROR('joy: filename too long',ALFILE,1)
      end if
      if (NNP.eq.0) then
	call ERROR('joy: no file in command line',' ',0)
	call MESSAGE('usage: joy -options align.file')
      end if
      if (( .not. FILEEXIST(ALFILE)) .and. ( .not. VERSOUT)) then
	call ERROR('joy: file does not exist (a)',ALFILE,0)
      end if
C
C Set filenames
C
      TEXFILE=ALFILE(1:NNP)//'tex'
      SUBFILE=ALFILE(1:NNP)//'sub'
      TEMFILE=ALFILE(1:NNP)//'tem'
C
C ////////////////////////////////////////////////////////////////////////////
C
      do J=1,MAXRES
	do K=1,NUMFEAT
	  RNULL(K,J)=0
	end do
      end do
      do I=1,MAXSEQ
	do J=1,MAXRES
	  NHBSIDE(J,I)=0
	  NHBMAIN(J,I)=0
	  CIS(J,I)=.false.
	end do
      end do
C
C Read in raw alignment from the file ALFILE (don't read it if use a template 
C file)
C
      call RDALI(ALFILE,NSTR,RAWSEQ,LENGTH,LENSEQ,STRNAM,LENFIL,LABEL,
     +           TEXT,ORDER,NUMENT,NEWSEQ,SEQNAM,NUMNEWSEQ,
     +           USENUMBER,NUMFILE,NUMNUMFILE,CHECK,FAMNAME,FAMCLASS,
     +           ACCMCH,PDBCOD,RANGE,ALNCHN)
      if (NSTR.lt.2) then
	DOSUBST=.false.
      end if
C
C Initialize the special access mechanism:
C
	if (ACCMCH) then
	  call ipdbnam
C These two statements can be deleted if rdpdbas() is replaced.
	  call getenv('RESLIB', RESFNAM)
	  call inires(11, RESFNAM)
	end if
C
C If the user wants to use one of the structures to label the alignment then
C process this. Decision goes like this.
C If find .lbl then use labels there,
C If no .lbl then number every 10th residue
C
C Clear '.' and trailing chars from NUMFILE string
C See if each requested numbered structure really has a structure
C
      if (USENUMBER) then
	do J=1,NUMNUMFILE
	  if (index(NUMFILE(J),'.').gt.0) then
	    do I=index(NUMFILE(J),'.'),15
	      NUMFILE(J)(I:I)=' '
	    end do
	  end if
	end do
	do J=1,NUMNUMFILE
	  FOUNDNUM=.false.
	  do I=1,NSTR
	    if (NUMFILE(J)(1:LASTCHAR(NUMFILE(J))).eq.
     +          STRNAM(I)(1:LASTCHAR(STRNAM(I)))) then
	      FOUNDNUM=.true.
	      NIFIL=I
	    end if
	  end do
	  if (.not.FOUNDNUM) then
	    call ERROR('joy: label has no structure',NUMFILE(J),1)
	  end if
	  if (FILEEXIST(NUMFILE(J)(1:(LASTCHAR(NUMFILE)))//'.lbl')) then
	    call RDLBL(NUMFILE(J)(1:(LASTCHAR(NUMFILE)))//'.lbl',
     +                LBL(1,NIFIL),NLBL(NIFIL))
	    USENUMFILE(NIFIL)=.true.
	  else
	    AUTOLABEL(NIFIL)=.true.
	  end if
	end do
      end if
C
C Read in all the required data for each protein dataset. Accessibility files
C must have a .psa extension, DSSP files a .all extension and H-bond files a
C .hbd extension. Errors are trapped for any inconsistencies in the data, so
C program can stop in the relevant subroutine. Small change to allow  reading
C of .sst files as a replacement for .all files (allows program to run
C completely under UNIX.)
C
      if (USESTRUCT) then
	DO I=1,NSTR
	  RESBEG(I)='@@@@@'
	  RESEND(I)='@@@@@'
	  FILBIG(I)=STRNAM(I)
	end do
C
C to indicate all of the structural data files to be read in:
C 
C This data is then used by RDPSA, RDHBD, and RDSST to read in only the
C required part of the molecule. Code may need to be modified for 
C different chains.
C
C Assumption: the topologies of the psa and hbd files are the same as
C             they would be if the same atom files used to calculate them:
C             IRESBEG and IRESEND from RDPSA are used in RDHBD
C
	do I=1,NSTR
c          LENROOT=index(FILBIG(I),'.')-1
	  LENROOT=min((index(FILBIG(I),' ')-1),LASTCHAR(FILBIG(I)))
          if (USESITE) then
            CALL RDPSS(ALFILE,FILBIG(I)(1:LENROOT),SITVAL,
     +                 SITE(1,I),
     +                 RAWSEQ(1,I),LENSEQ(I),CHECK,RESBEG(I),RESEND(I),
     +                 IRESBEG(I),IRESEND(I),RESLBL(1,I),LBIN,
     +                 SITEVAL(1,I),TOTSIT(I),FORCE,
     +                 ACCMCH,RANGE(1,I),ALNCHN(1,I),PDBCOD(I))
          end if
	  CALL RDPSA(ALFILE,FILBIG(I)(1:LENROOT),ACCVAL,
     +               ACCESS(1,I),
     +               RAWSEQ(1,I),LENSEQ(I),CHECK,RESBEG(I),RESEND(I),
     +               IRESBEG(I),IRESEND(I),RESLBL(1,I),LBIN,
     +               RELACC(1,I),TOTACC(I),FORCE,
     +               ACCMCH,RANGE(1,I),ALNCHN(1,I),PDBCOD(I))
	  CALL RDHBD(ALFILE,FILBIG(I)(1:LENROOT),SCHBO(1,I),
     +               SCHBN(1,I),SSHB(1,I),SHETHB(1,I),NHBSIDE(1,I),
     +               NHBMAIN(1,I),RAWSEQ(1,I),LENSEQ(I),CHECK,
     +               HETTOSIDE,
     +               SSBOND(1,I),MMNBOND(1,I),MMOBOND(1,I),RESBEG(I),
     +               RESEND(I),IRESBEG(I),IRESEND(I),LBIN,FORCE,
     +               ACCMCH,RANGE(1,I),ALNCHN(1,I),PDBCOD(I))
	  call RDSST(ALFILE,FILBIG(I)(1:LENROOT),SHEET(1,I),
     +               POSPHI(1,I),ALPHA(1,I),THRTEN(1,I),PIHEL(1,I),
     +               BETA(1,I),CIS(1,I),RAWSEQ(1,I),
     +               LENSEQ(I),CHECK,RESBEG(I),RESEND(I),
     +               LBIN,OOI(1,I),PHI(1,I),PSI(1,I),FORCE,
     +               ACCMCH,RANGE(1,I),ALNCHN(1,I),PDBCOD(I))
	  do K=1,LENSEQ(I)
	    EFITYP(K,I)=EFIMOV(PHI(K,I),PSI(K,I))
	  end do
	end do
      end if
C
C Start analysis if required by the previous assignment of ANALYZE
C To keep things consistent it is safer to convert all sequences to
C upper case, just in case they have been changed, or were inputted in lower
C case.
C Old code to handle lower case characters in sequence, should now be caught
C earlier
C
      do I=1,NSTR
	do J=1,LENGTH
	  SEQ(J,I)=TOUPPER(RAWSEQ(J,I))
	end do
      end do
      if (ANALYZE) then
	call INITIT(TITLE)
        call INITBL(NMUT,NMUTALL,NNN,NENVIR)
	write (STDOUT,'(''produced by joy, version '',A,
     +             '' copyright 1989-1995 john overington'')')
     +             VERSION()
	if (DOSUBST) then
	  open (unit=4,file=SUBFILE,form='FORMATTED',status='UNKNOWN',
     +          err=903)
	  write (4,'(''produced by joy, version '',A)') VERSION()
	  write (4,'(10X,''Dataset '',2X,A)') ALFILE(1:NNP-1)
	  write (4,'(''Number of sequences '',I4)') NSTR
	  do I=1,NSTR
	    NRESTOT=NRESTOT+LENSEQ(I)
	  end do
	  write (4,'(''Number of residues  '',I4)') NRESTOT
	  write (4,'(''  '')')
	end if
      end if
C
C Count the number of times each feature occurs for each residue for each
C sequence, this data is stored in a seven dimensional array (NNN).
C Also create a feature discriptor vector for each residue (RDISC).
C Total the NNN feature matrix for each residue.
C If required check the environments of each of the residues
C
      do I=1,NSTR
	M=1
	DO J=1,LENGTH
	  if (ISAA(SEQ(J,I))) then
	    SEQ2(M,I)=SEQ(J,I)
	    M=M+1
	  end if
	end do
	call NVEC(SEQ2(1,I),LENSEQ(I),LENGTH,ALPHA(1,I),THRTEN(1,I),
     +            PIHEL(1,I),BETA(1,I),POSPHI(1,I),CIS(1,I),
     +            ACCESS(1,I),SSHB(1,I),SCHBO(1,I),SCHBN(1,I),
     +            SHETHB(1,I),SSBOND(1,I),MMNBOND(1,I),MMOBOND(1,I),
     +            SITE(1,I),NNN,RDISC(1,1,I))
      end do
C
C First analyze each of the sequences on an individual basis, i.e. residue 
C composition and composition wrt USESTRUCT
C
      if (ANALYZE) then
	do I=1,NSTR
	  call ANALSEQ(LENSEQ(I),RDISC(1,1,I),STRNAM(I),
     +                 LENFIL(I),NENVIR)
	  if (DOCHKSTR) then
	    call CHKSTR(STRNAM(I),RESLBL(1,I),RDISC(1,1,I),
     +                  LENSEQ(I),TOTACC(I))
	    write (STDOUT,'(''unusual mainchain conformations '')')
	    do K=1,LENSEQ(I)
	      if (EFITYP(K,I) .eq. 7) then
		write (STDOUT,'(2X,
     +              ''bad Efimov conformational state for '',A,1X,A,
     +              '' in '',A)') RAWSEQ(K,I),RESLBL(K,I),STRNAM(I)
	      end if
	    end do
	  end if
	end do
C
C Now analyze all the sequences pairs
C First analyze the pairs where both are structurally known, then examine the
C cases where only one in the pair are structurally known.
C
C Analyze cap residues
C
Ccap    if (DOCAP) then
Ccap      open (unit=9,file=CAPFILE,status='unknown',form='formatted',
Ccap +          err=904,recl=200)
Ccap      write (9,'(''file typ  # beg end len   sequence    charge      '',
Ccap +               ''access      H-bond CO   H-bond NH   + phi'')')
Ccap      do I=1,NSTR
Ccap        call ELEMCAP(1,SEQ(1,I),LENGTH,RDISC(1,1,I),IHEL(1,1,I),NHEL(I),
Ccap +                   STRNAM(I))
Ccap        call ELEMCAP(2,SEQ(1,I),LENGTH,RDISC(1,1,I),ISHT(1,1,I),NSHT(I),
Ccap +                   STRNAM(I))
Ccap      end do
Ccap      close (unit=9)
Ccap    end if
C
C output totals for residues in different classes for whole alignment
C Analyze the variability observed in the alignment, also output invariant res
C Call PAIR2, even though DOSUBST may be false to get the sequence ID values
C
	if (NSTR .gt. 1) then
	  call ANALALI(NENVIR,NSTR,NAVELEN)
	  do I=1,NSTR
	    do J=I+1,NSTR
C
C ---------------------------------
C look at falloff figures as well with pairfall subroutine
C
C out of range variable here 29-4-99
C
Cjpo              call PAIRFALL(SEQ(1,I),SEQ(1,J),LENGTH,RDISC(1,1,I),
Cjpo     +                      RDISC(1,1,J),NMUTID,NMUTIDAC)
C
C ---------------------------------
	      call PAIR2(SEQ(1,I),SEQ(1,J),LENGTH,RDISC(1,1,I),
     +                  RDISC(1,1,J),
     +                  NMUT,NMUTALL,PIDENT(I,J),.false.,NSUBST(I,J),
     +                  PIDTMP,NSUBTMP,NSUBTMP2,PIDTMP2,
     +                  TOTAADIFF(I,J),TOTSITDIFF(I,J),TOTACCAADIFF(I,J),USESITE,
     +                  NCOMP,NSITECOMPID,NSITEACCCOMPID,NIDSITECOMP,NIDACCSITECOMP,
     +                  STRNAM(I),STRNAM(J),SITVAL,SITEFLAG)
	      PIDENT(J,I)=PIDENT(I,J)
	      do K=1,4
		PIDENTSTR(K,I,J)=PIDTMP(K)
		NSUBSTSTR(K,I,J)=NSUBTMP(K)
	      end do
	      do K=1,2
		PIDENTACC(K,I,J)=PIDTMP2(K)
		NSUBSTACC(K,I,J)=NSUBTMP2(K)
	      end do
              NSITE(1,I,J)=NCOMP
              NSITE(2,I,J)=NSITECOMPID
              NSITE(3,I,J)=NSITEACCCOMPID
              NSITE(4,I,J)=NIDSITECOMP
              NSITE(5,I,J)=NIDACCSITECOMP
Ccap          if (DOCAP) then
Ccap             call CAPSUBST(SEQ(1,I),SEQ(1,J),LENGTH,RDISC(1,1,I),LENSEQ(I),
Ccap +                         NMUTHELNCAP,NMUTHELCCAP,NMUTSHTNCAP,NMUTSHTCCAP,
Ccap +                         IHEL(1,1,I),ISHT(1,1,I),NHEL(I),NSHT(I))
Ccap             call CAPSUBST(SEQ(1,J),SEQ(1,I),LENGTH,RDISC(1,1,J),LENSEQ(J),
Ccap +                         NMUTHELNCAP,NMUTHELCCAP,NMUTSHTNCAP,NMUTSHTCCAP,
Ccap +                         IHEL(1,1,J),ISHT(1,1,J),NHEL(J),NSHT(J))
Ccap          end if
	    end do
	    PIDENT(I,I)=100.0
	  end do
C
C DOKABWU controlled by USESITE
C
	  if ((NSTR+NUMNEWSEQ .gt. 2) .and. DOKABWU) then
	    call KABWUV(SEQ,LENGTH,NSTR,SITEFLAG)
	  end if
C
C set NAVELEN for NSTR=1 case
C
	else
	  NAVELEN=LENSEQ(1)
	end if
	if (USENEWSEQ .and. NUMNEWSEQ .gt. 0) then
	  do I=1,NSTR
	    do J=1,NUMNEWSEQ
              write (STDERR,'(''pair2: bad parameters - needs repair, near line 678 main.f'')')
C             call PAIR2(SEQ(1,I),NEWSEQ(1,J),LENGTH,RDISC(1,1,I),
C     +                   RNULL(1,1),NMUT,NMUTALL,PIDSTRNEW(I,J),
C     +                   .true.,NSUBSTSTRNEW(I,J),
C     +                   PDF,NSUBTMP,NSUBTMP2,PIDTMP2,USESITE)
	    end do
	  end do
	end if
	call PIDSTR(NSTR,STRNAM,PIDENT,NSUBST,PIDENTSTR,PIDENTACC,
     +              NUMNEWSEQ,SEQNAM,PIDSTRNEW,NSUBSTSTRNEW,
     +              NEWSEQ,LENGTH,LENFIL,NSUBSTSTR,NSUBSTACC,NAVELEN,
     +              FAMNAME,FAMCLASS,USESITE,TOTAADIFF,TOTSITDIFF,TOTACCAADIFF,SITVAL,
     +              NSITE)
      end if
C
C ================================= KITSCH
C
C run kitsch and send output to /dev/null (if required)
C
      if (KITSCHORDER) then
	call SYSTEM('kitsch < DIST.mat > /dev/null') 
	call PARSETREE(NSTR,STRNAM,INDSTR)
      end if
c     write (6,*) kitschorder,' kitschorder'
c     do i=1,nstr
c       write (6,*) indstr,i
c     end do
C
C ================================= KITSCH
C
C Write out frequencies for residues in each environment
C maybe problems here if you don't try to look for 0112 case. note format
C
      if (DOENV) then
	write (STDOUT,'(/,''environment vectors for '',A)')
     +         ALFILE(1:NNP-1)
	do II=1,21
	  write (STDOUT,'(A1,'': '',64I3)') RES(II:II),
     +        (((((NNN(II,J,K,L,M,N),J=1,4),K=1,2),L=1,2),M=1,2),N=1,2)
	end do
      end if
C 
C Write out the new alignment if required by the previous assignment of PRINT
C Tidy up the N and C-termini of the sequences, regardless if USESTRUCT are
C to be displayed. For the output of the template these N and C termini
C insertions are required.
C Write out simplified formatted alignment to .out file
C
      if (PRINT) then
	if (TIDY) then
	  do I=1,NSTR
	    call TIDYALI(RAWSEQ(1,I),LENGTH)
	  end do
	  do I=1,NUMNEWSEQ
	    call TIDYALI(NEWSEQ(1,I),LENGTH)
	  end do
	end if
C
C This routine does all the fancy LaTeX formatting for the alignment
C
c       write (6,*) ' indstr before call '
c       do i=1,nstr
c         write (6,*) indstr(i),i
c       end do
	call WRTEX(TEXFILE,RAWSEQ,FAMNAME,SHEET,RDISC,LENGTH,NSTR,
     +             ALFILE,
     +             STRNAM,NUMNEWSEQ,NEWSEQ,SEQNAM,LABEL,TEXT,
     +             ORDER,NUMENT,AUTONUM,AUTOSEC,AUTOLABEL,ALLLABEL,
     +             SWAPLBL,USENUMFILE,NUMFILE,NUMNUMFILE,LBL,NLBL,
     +             RESLBL,DOINVARI,ALLSEC,DOKEY,AUTOACC,DOEFI,EFITYP,
     +             LANDSCAPE,HELVETICA,ONEPERPAGE,KITSCHORDER,INDSTR)
C
C ============================
C
C enable the following lines if you have LaTeX in a usable state
C
C obv. need to add params here (for things like landscape etc (do this when
C I have LaTeX working
C
C        STRING='latex'//FAMNAME(1:lastchar(FAMNAME))
C        call SYSTEM(STRING)
C        STRING='dvips'//FAMNAME(1:lastchar(FAMNAME))
C        call SYSTEM(STRING)
C
C ============================
C
	call WRPRN(STRNAM,RAWSEQ,LENGTH,NSTR,NUMNEWSEQ,NEWSEQ,
     +             SEQNAM,50)
C
C cluster the sequence identity levels (now use kitsch to do this properly
C
c       if (NSTR .gt. 2) then
c         call LINKER(NSTR,PIDENT,Q,P,T)
c         write (STDOUT,'(/,''   clustering:'')')
c         do I=1,NSTR-1
c           write (STDOUT,'(I3,1X,A,1X,I3,'' - '',A,1X,I3,2X,F8.3)')
c    +             I,STRNAM(Q(I)),Q(I),STRNAM(P(I)),P(I),T(I)
c         end do
c         write (STDOUT,'('' '')')
c       end if
C
      end if
C
C Output the NNN feature counter matrix and substitution data to the .SUM file.
C
      if (ANALYZE .and. DOSUBST) then
	do II=1,21
	  write (4,'(17X,A,/,8(8I4,/),/)') RES(II:II),
     +        (((((NNN(II,J,K,L,M,N),J=1,4),K=1,2),L=1,2),M=1,2),N=1,2)
	end do
C
C new code for falloff in subst
C
        do M=1,10
          BIT1=(10.0*M)-0.01
          BIT2=(10.0*M)-10.0
          write (BITC1,'(f5.2)') BIT1
          write (BITC2,'(f5.2)') BIT2
          TEXTBIT='all classes: - pid range '//BITC2//' -> '//BITC1
          call PRTBL(NMUTID(1,1,M),TEXTBIT)
          TEXTBIT='all classes: - pid range '//BITC2//' -> '//BITC1//' accessible'
          call PRTBL(NMUTIDAC(1,1,M,1),TEXTBIT)
          TEXTBIT='all classes: - pid range '//BITC2//' -> '//BITC1//' inaccessible'
          call PRTBL(NMUTIDAC(1,1,M,2),TEXTBIT)
        end do
C
	call PRTBL(NMUTALL,'All classes')
	KK=0
	do M=1,4
	  do L=1,2
	    do K=1,2
	      do J=1,2
		do I=1,2
		  KK=KK+1
		  call PRTBL(NMUT(1,1,M,L,K,J,I),TITLE(KK))
		end do
	      end do
	    end do
	  end do
	end do
Ccap    if (DOCAP) then
Ccap      do I=1,9
Ccap        call PRTBL(NMUTHELNCAP(1,1,I),'helix N-cap')
Ccap      end do
Ccap      do I=1,9
Ccap        call PRTBL(NMUTHELCCAP(1,1,I),'helix C-cap')
Ccap      end do
Ccap      do I=1,9
Ccap        call PRTBL(NMUTSHTNCAP(1,1,I),'sheet N-cap')
Ccap      end do
Ccap      do I=1,9
Ccap        call PRTBL(NMUTSHTCCAP(1,1,I),'sheet C-cap')
Ccap      end do
Ccap    end if
	close (unit=4)
      end if
C
C Create template creation files if desired
C
      if (TEMPLATE) then
	if (NSTR .gt. 0) then
	  call WRTEM(TEMFILE,NSTR,RAWSEQ,STRNAM,LENFIL,LENGTH,
     +               ALPHA,THRTEN,PIHEL,BETA,ACCESS,POSPHI,
     +               SCHBO,SCHBN,SSHB,CIS,SHETHB,SSBOND,
     +               MMNBOND,MMOBOND,RELACC,OOI)
	end if
      end if
C
C Normal exit
C
      call EXIT(0)
C
C fatal errors i/o errors
C
903   call ERROR('joy: cannot open file',SUBFILE,3)
Ccap 904   call ERROR('joy: cannot open file',CAPFILE,3)
C
      end
