cf ----------------------------------------------------------------------------
cf
cf    Create a specified data file, using a specified PDB file, in the 
cf    current or in the depository directory specified by the environment
cf    variable. Only a segment RNG,CHNRNG from the PDB file is used.
cf    PDB file name is PDBCOD, PDBCOD.atm, PDBCOD.pdb, or it is found
cf    in the list of PDB code <--> PDB file pairs in the file ${PDBENT}.
cf
cf    subroutine mkfile(dirpath,pdbcod,rng,chnrng,type,fname,
cf   &                  compressed,istop)
cf
cf ----------------------------------------------------------------------------

      subroutine mkfile(dirpath,pdbcod,rng,chnrng,type,fname,
     &                  compressed,istop)
        implicit none
        integer ioout,ntypes
        parameter (ioout=19, ntypes = 5)
        character delim*(1)
        parameter (delim='_')
        integer mdir
        parameter (mdir=20)

c ----------------------------------------------------------------------------
c
c ----- rdpdb() definitions for the calling routine:
c
        include 'io.cst'
        include 'protsize.cst'

        integer ierr,iresatm(maxatm)
        integer natm,nres,iatmr1(maxres)
        real x(maxatm),y(maxatm),z(maxatm),biso(maxatm)
        character rng(2)*(5),chnrng(2)*(1),header*(80)
        character chain(maxres)*(1),atmnam(maxatm)*(4)
        character resnam(maxres)*(3),resnum(maxres)*(5)
c        character brkname*(255)
        logical hetatm,water,hydrogen

c        data rng,chnrng /'@@@@@','@@@@@','@','@'/
c
c -----------------------------------------------------------------------------

        integer itype,ifindword,lenr,ndir,i,ipdbfnd,istop
        character pdbcod*(*), type*(*),types(ntypes)*(10)
        character dirpath*(*), dir(mdir)*(255), direnv*(255)
        character envs(ntypes)*(20),fname*(*),dirout*(255)
        character pdbloc*(100), cmd*(512)
        character cmds(ntypes)*(255),root*(255)
        logical filexs,compressed

c ----- it looks as if JOY reads in the HETATMs, so set that to .T.
        data hetatm,water,hydrogen /.true.,.false.,.false./
        data types /'ngh', 'sst', 'psa', 'dih', 'hbd'/
        data envs  /'PDBNGH', 'PDBSST', 'PDBPSA', 'PDBDIH', 'PDBHBD'/
      
c ----- too long for the data statement in 72 columns: 

c ----- AS neighbours program:
        cmds(1)='(ngh2 OFF 6.0 tmp.ent;'//
     &          'mv tmp.ngh OUTFIL;compress OUTFIL;rm tmp.ent)'
c ----- DS secondary structure program:
        cmds(2)='(sstruc f tmp.ent;'//
     &          'mv tmp.sst OUTFIL;compress OUTFIL;rm tmp.ent)'
c ----- AS & JPO accessibility program:
        cmds(3)='(psa -p1.4 -e0.05 -r -c -V tmp.ent;'//
     &          'mv tmp.psa OUTFIL;compress OUTFIL;rm tmp.ent)'
c ----- AS dihedrals program:
        cmds(4)='(dih3 tmp.ent;'//
     &          'mv tmp.dih OUTFIL;compress OUTFIL;rm tmp.ent)'
c ----- JPO H-bond program:
        cmds(5)='(hbond tmp.ent;'//
     &          'mv tmp.hbd OUTFIL;compress OUTFIL;rm tmp.ent)'

        itype = ifindword(type, types, ntypes)

c ----- get the basename file name with extension:
        call concat(pdbcod, delim, root)
        call addstr(root, rng(1))
        call addstr(root, delim)
        call addstr(root, chnrng(1))
        call addstr(root, delim)
        call addstr(root, rng(2))
        call addstr(root, delim)
        call addstr(root, chnrng(2))
        call addstr(root, '.')
        call addstr(root, types(itype))

c ----- get all directories:
        call str_sn3(dirpath, dir, mdir, ndir)
        if (ndir .eq. 0) then
          dir(1) = ' '
          ndir = 1
        end if

c ----- try all directories; uncompressed and compressed:
        do  i = 1, ndir
          call adddir(dir(i))
          call concat(dir(i), root, fname)
          compressed = .false.
          if (filexs(fname)) return
          call addstr(fname, '.Z')
          compressed = .true.
          if (filexs(fname)) return
        end do

c ----- get the depository directory for the type ITYPE:
        call getenv(envs(itype), direnv)

c ----- get the full file name
        call adddir(direnv)
        call concat(direnv, root, fname)

c ----- see if the uncompressed/compressed file already exists:
        compressed = .false.
        if (filexs(fname)) return
        compressed = .true.
        call addstr(fname, '.Z')
        if (filexs(fname)) return

c ----- file does not exist anywhere on the specified path; 
c       try to create the compressed file from the PDB atom file:

c ----- first, extract the correct range of ATOMs from the PDB file:
        call pdbnam(dirpath,pdbcod,pdbloc,ipdbfnd,compressed,0)
        if (pdbloc .eq. 'file-not-found') then
          write(*,'(2a)') 'mkfile__E> non-existing PDB file: ',
     &    pdbloc(1:lenr(pdbloc))
          if (istop .eq. 1) stop
          return
        end if
        call rdpdbas(ioinp,pdbloc,water,hetatm,hydrogen,x,y,z,maxatm,
     &             maxres,natm,nres,resnam,resnum,atmnam,iatmr1,
     &             chain,header,rng,chnrng,iresatm,biso,ierr)
        if (ierr .ne. 0) then
          write(*,'(a)') 'mkfile__E> reading PDB'
          if (istop .eq. 1) stop
          return
        end if

c ----- get the directory for the resulting file:
        if (ipdbfnd .eq. 7 .or. ipdbfnd .eq. 8) then
c ------- the standard depository directory for data type ITYPE:
          dirout = direnv
        else
c ------- the directory of the PDB atom file:
          call direct(pdbloc, dirout)
          call adddir(dirout)
        end if

c ----- the temporary PDB file:
        call wrpdbas(ioout,'tmp.ent',x,y,z,natm,nres,resnam,
     &             resnum,atmnam,iresatm,chain)
c ----- get the final output file name (without .Z):
        call concat(dirout, root, fname)
        cmd = cmds(itype)
        call subs2(cmd, 'OUTFIL', fname(1:lenr(fname)))
        call subs2(cmd, 'OUTDIR', dirout(1:lenr(dirout)))
        write(*,'(2a)') 'mkfile__N> Executing: ',cmd(1:lenr(cmd))
c ----- make the compressed data file:
        call system(cmd)
        compressed = .true.
        call addstr(fname, '.Z')

        return
      end



cf ----------------------------------------------------------------------------
cf
cf    ADDDIR() adds a '/' at the end of the directory name if necessary.
cf
cf    subroutine adddir(dir)
cf
cf ----------------------------------------------------------------------------

      subroutine adddir(dir)
        implicit none
        integer lenr, lr
        character dir*(*)

        lr = lenr(dir)
        if (lr.gt.0) then
          if (dir(lr:lr) .ne. '/') dir(lr+1:lr+1) = '/'
        end if

        return
      end
