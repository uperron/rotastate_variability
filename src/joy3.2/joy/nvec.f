      subroutine NVEC(SEQ,NRES,LENGTH,ALPHA,THREE10,PIHEL,BETA,POSPHI,
     -                CIS,ACCESS,SSHB,MOHB,MNHB,SHETHB,DSBOND,MMNBOND,
     -                MMOBOND,SITERES,N,RDISC)
C
C copyright (c) 1988-1991 John Overington
C part of joy
C
C A subroutine to bin a residue into a seven dimensional array that describes
C the number of times that this particular combination of features exists.
C Also returns a vector that describes each residue in the sequence (RDISC)
C Added SHETHB, N.B. This is not in the N matrix.
C Added CIS, again not in the N matrix.
C Strange compiler bugs here -- maybe something to do with high
C         dimension arrays -opt3 fills N with 0 bu -noopt gives correct results
C         fixed bug by rearranging code
C Removed intialization of N on each call
C Added mainchain to mainchain hydrogen bonds
C Changed Secondary structure to be old definition, added posphi and
C         DSSP classes
C
      include 'joy.h'
C
      character*1 SEQ(MAXRES)
      logical POSPHI(MAXRES), ACCESS(MAXRES), SSHB(MAXRES), MOHB(MAXRES)
      logical BETA(MAXRES), MNHB(MAXRES), ALPHA(MAXRES), SHETHB(MAXRES)
      logical CIS(MAXRES), DSBOND(MAXRES), MMNBOND(MAXRES)
      logical THREE10(MAXRES), PIHEL(MAXRES), MMOBOND(MAXRES)
      logical SITERES(MAXRES)
C
      integer N(21,4,2,2,2,2), RDISC(NUMFEAT,MAXRES)
      integer I, J, NDS, NMMN, NMMO, NDSSP, NPHI, LENGTH
      integer NRES, NS, NSEC, NACC, NSSHB, NMOHB, NMNHB, NSHETHB, NCIS, NSITE
C
C Initialize the descriptor vector
C
      do I=1,MAXRES
        do J=1,NUMFEAT
          RDISC(J,I)=0
        end do
      end do
C
C Now process each residue in the sequence to dump it in the correct bin
C NSEC is the old type of secondary structure
C
      do I=1,NRES
C
        NS=SEQ2I(SEQ(I))
C
        if (POSPHI(I)) then
          NSEC=3
        else if (ALPHA(I)) then
          NSEC=1
        else if (BETA(I)) then
          NSEC=2
        else
          NSEC=4
        end if
C
        if (ACCESS(I)) then
          NACC=1
        else
          NACC=2
        end if
C
        if (SITERES(I)) then
          NSITE=1
        else
          NSITE=2
        end if
C
        if (SSHB(I)) then
          NSSHB=1
        else
          NSSHB=2
        end if
C
        if (MOHB(I)) then
          NMOHB=1
        else
          NMOHB=2
        end if 
C
        if (MNHB(I)) then
          NMNHB=1
        else
          NMNHB=2
        end if
C
        if (SHETHB(I)) then
          NSHETHB=1
        else
          NSHETHB=2
        end if
C
        if (CIS(I)) then
          NCIS=1
        else
          NCIS=2
        end if
C
C get priorities right here
C
        if (THREE10(I)) then
          NDSSP=3
        else if (PIHEL(I)) then
          NDSSP=5
        else if (ALPHA(I)) then
          NDSSP=1
        else if (BETA(I)) then
          NDSSP=2
        else
          NDSSP=4
        end if
C
        if (POSPHI(I)) then
          NPHI=1
        else
          NPHI=2
        end if
C
C Trick here if DS then sequence has to be C if not and sequence was C
C then change decriptor to correct class i.e. J (free thiol)
C
        if (DSBOND(I)) then
          NDS=1
          if (NS.ne.2) then
            call ERROR('joy: non-cystine in disulphide !',' ',0)
          end if
        else
          NDS=2
          if (NS.eq.2) then
            NS=21
          end if
        end if
C
        if (MMNBOND(I)) then
          NMMN=1
        else
          NMMN=2
        end if
        if (MMOBOND(I)) then
          NMMO=1
        else
          NMMO=2
        end if
C
C ----------------------------------------------------------------------
C
C Construct the discriptor vector for the I'th residue
C     
        RDISC(1,I)=NS
        RDISC(2,I)=NSEC
        RDISC(3,I)=NACC
        RDISC(4,I)=NSSHB
        RDISC(5,I)=NMOHB
        RDISC(6,I)=NMNHB
        RDISC(7,I)=NSHETHB
        RDISC(8,I)=NCIS
        RDISC(9,I)=NDS
        RDISC(10,I)=NDSSP
        RDISC(11,I)=NPHI
        RDISC(12,I)=NMMN
        RDISC(13,I)=NMMO
        RDISC(14,I)=NSITE
C
C Increment the N, matrix counting number of residues observed of each class
C
        N(NS,NSEC,NACC,NSSHB,NMOHB,NMNHB)=
     +   N(NS,NSEC,NACC,NSSHB,NMOHB,NMNHB)+1
C
      end do
C
C ----------------------------------------------------------------------------
C
      return
      end
