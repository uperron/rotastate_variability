      subroutine PAIR2(SEQ1,SEQ2,LENGTH,R1,R2,NMUT,NMUTALL,PIDENT,
     +                 ONEONLY,NSUBST,PIDENTSTR,NSUBSTSTR,
     +                 NSUBSTACC,PIDENTACC,TOTAADIFF,TOTSITDIFF,TOTACCAADIFF,USESITE,
     +                 NCOMP,NSITECOMP,NSITEACCCOMP,NIDSITECOMP,NIDACCSITECOMP,
     +                 NAME1,NAME2,SITEVAL,SITEFLAG)
C
C copyright (c) 1988-1991 John Overington
C part of joy
C
C A subroutine to analyze an alignment of TWO sequences to identify conserved
C residues, both in terms of residue identity and in terms of other features.
C Later version counts the number of times that a residue is converted into 
C another. Note a full analysis of this substitution propensity will require the
C use of the NNN matrix (see earlier).
C Restructured reporting of conserved features, and of identities, etc. Added
C ability to treat an insertion as another type of residue.
C NOTE the changes to allow the inclusion of insertions only is reflected in 
C the arrays NMUT and NMUTALL.
C Must be careful with the insertion code substitutions, since these do not have
C residue discriptors associated with them (RDISC)
C Changed to allow scoring of a pair where only one of sequences has structural
C information associated with it. (Controlled by logical ONEONLY)
C
C NOTE should change this routine to only consider one structured sequence at
C a time, this will allow a more sensible call from main, and be more flexible
C
C N.B. Cysteine and cystine are not considered identical residues !!
C
C Changed use of NMUT etc tables, now not initialized every call to routine
C
C Talk about complicated
C
      include 'joy.h'
C
      character*(*) NAME1,NAME2
      character*1 SEQ1(MAXRES), SEQ2(MAXRES)
      character*2 FLAG, SITEFLAG(MAXRES)
      integer LENGTH, NMUT(22,22,4,2,2,2,2), NMUTALL(22,22)
      integer R1(NUMFEAT,MAXRES), R2(NUMFEAT,MAXRES)
      integer NIDENTSTR(4), NSTR(4), NIDENTACC(2), NACC(2)
      integer NSUBSTSTR(4),NSUBSTACC(2), NBOTH, NIDENT
      integer I, J, K, L, M, NSUBST
      logical ONEONLY
      logical USESITE
      real PIDENTSTR(4), PIDENT, PIDENTACC(2)
C
      real TOTAADIFF, TOTACCAADIFF, TOTSITDIFF, AADIFF, SITEVAL
      integer NCOMP,NSITECOMP,NSITEACCCOMP,NIDSITECOMP,NIDACCSITECOMP
      real W1,W2,W3,W4,W5
C
C ----------------------------------------------------------------------------
C
      do I=1,4
        NSUBSTSTR(I)=0
        NIDENTSTR(I)=0
        NSTR(I)=0
      end do
      do I=1,2
        NIDENTACC(I)=0
        NSUBSTACC(I)=0
        NACC(I)=0
      end do
      M=0
      L=0
      NSUBST=0
      NBOTH=0
      NIDENT=0
C
C ----------------------------------------------------------------------------
C
C Examine each alignment position
C   Increment the real sequence pointers
C   also count substitutions between pairs
C
      do I=1,LENGTH
C
C Convert C to J where needed
C Count substitutions
C
        if (ISAA(SEQ1(I))) then
          NSUBST=NSUBST+1
          L=L+1
          if (R1(1,L).eq.21) then
            SEQ1(I)='J'
          end if
          do J=1,4
            if (R1(2,L).eq.J) then
              NSUBSTSTR(J)=NSUBSTSTR(J)+1
            end if
          end do
          do J=1,2
            if (R2(3,L).eq.J) then
              NSUBSTACC(J)=NSUBSTACC(J)+1
            end if
          end do
        end if
        if (ISAA(SEQ2(I))) then
          M=M+1
          if (R2(1,M).eq.21) then
            SEQ2(I)='J'
          end if
          NSUBST=NSUBST+1
          do J=1,4
            if (R2(2,M).eq.J) then
              NSUBSTSTR(J)=NSUBSTSTR(J)+1
            end if
          end do
          do J=1,2
            if (R2(3,M).eq.J) then
              NSUBSTACC(J)=NSUBSTACC(J)+1
            end if
          end do
        end if
C
C Count the number of positions equivalenced for each secondary
C structural class, if the same in both count once if different
C count each class
C
        if (ISAA(SEQ1(I)).and.ISAA(SEQ2(I))) then
          NBOTH=NBOTH+1
          NSTR(R1(2,L))=NSTR(R1(2,L))+1
          if (R2(2,M).ne.R1(2,L)) then
            NSTR(R2(2,M))=NSTR(R2(2,M))+1
          end if
          NACC(R1(3,L))=NACC(R1(3,L))+1
          if (R2(3,M).ne.R1(3,L)) then
            NACC(R2(3,M))=NACC(R2(3,M))+1
          end if
        end if
C
        if (ISAA(SEQ1(I)).or.ISAA(SEQ2(I))) then
          if (SEQ1(I).eq.SEQ2(I)) then
            NIDENT=NIDENT+1
            NIDENTSTR(R1(2,L))=NIDENTSTR(R1(2,L))+1
            if (R2(2,M).ne.R1(2,L)) then
              NIDENTSTR(R2(2,M))=NIDENTSTR(R2(2,M))+1
            end if
            NIDENTACC(R1(3,L))=NIDENTACC(R1(3,L))+1
            if (R2(3,M).ne.R1(3,L)) then
              NIDENTACC(R2(3,M))=NIDENTACC(R2(3,M))+1
            end if
          end if
C
C Get overall substitution data in NMUTALL
C
          if (ISAA(SEQ1(I)).and.ISAA(SEQ2(I))) then
            NMUTALL(R1(1,L),R2(1,M))=NMUTALL(R1(1,L),R2(1,M))+1
            NMUTALL(R2(1,M),R1(1,L))=NMUTALL(R2(1,M),R1(1,L))+1
          else if (ISAA(SEQ1(I))) then
            NMUTALL(R1(1,L),22)=NMUTALL(R1(1,L),22)+1
          else if (ISAA(SEQ2(I))) then
            NMUTALL(R2(1,M),22)=NMUTALL(R2(1,M),22)+1
          end if
C
C Structure 1 - Do cis-peptide in separate table
C
          if (ISAA(SEQ1(I))) then
            if (ISAA(SEQ2(I))) then
              NMUT(R1(1,L),R2(1,M),R1(2,L),R1(3,L),R1(4,L),R1(5,L),
     +                     R1(6,L))=
     -        NMUT(R1(1,L),R2(1,M),R1(2,L),R1(3,L),R1(4,L),R1(5,L),
     +                     R1(6,L))+1
            else
              NMUT(R1(1,L),22,R1(2,L),R1(3,L),R1(4,L),R1(5,L),
     +                     R1(6,L))=
     -        NMUT(R1(1,L),22,R1(2,L),R1(3,L),R1(4,L),R1(5,L),
     +                     R1(6,L))+1
            end if
          end if
C
C Structure 2 but only if it is a structure
C
          if (ISAA(SEQ2(I)).and.(.not.ONEONLY)) then
            if (ISAA(SEQ1(I))) then
              NMUT(R2(1,M),R1(1,L),R2(2,M),R2(3,M),R2(4,M),R2(5,M),
     +                     R2(6,M))=
     -        NMUT(R2(1,M),R1(1,L),R2(2,M),R2(3,M),R2(4,M),R2(5,M),
     +                     R2(6,M))+1
            else
              NMUT(R2(1,M),22,R2(2,M),R2(3,M),R2(4,M),R2(5,M),
     +                     R2(6,M))=
     -        NMUT(R2(1,M),22,R2(2,M),R2(3,M),R2(4,M),R2(5,M),
     +                     R2(6,M))+1
            end if
          end if
        end if
      end do
C
C Calculate the percentage identity over each secondary structure range
C
      PIDENT=(REAL(NIDENT)/(REAL(NBOTH)+SMALL))*100.0
      do I=1,4
        PIDENTSTR(I)=(REAL(NIDENTSTR(I))/(REAL(NSTR(I))+SMALL))*100.0
      end do
      do I=1,2
        PIDENTACC(I)=(REAL(NIDENTACC(I))/(REAL(NACC(I))+SMALL))*100.0
      end do
C
C new code for distance physicochemical distance for compared residues
C
      TOTAADIFF=0.0
      TOTSITDIFF=0.0
      TOTACCAADIFF=0.0
      NCOMP=0
      NSITECOMP=0
      NSITEACCCOMP=0
      NIDSITECOMP=0
      NIDACCSITECOMP=0
C
C default weights
C
      W1=1.0
      W2=1.0
      W3=1.0
      W4=1.0
      W5=0.5             ! Wold states in paper not of importance in elastase set, so downweight
C
      if (USESITE) then
        write (STDOUT,'(''site: ================================================= '',f5.2)') SITEVAL
        L=0
        M=0
        do I=1,LENGTH
          if (ISAA(SEQ1(I))) then
            L=L+1
            if (SEQ1(I) .eq. 'J') then
              SEQ1(I)='C'
            end if
            if (ISAA(SEQ2(I))) then
              M=M+1
              if (SEQ2(I) .eq. 'J') then
                SEQ2(I)='C'
              end if
              NCOMP=NCOMP+1
              TOTAADIFF=TOTAADIFF+AADIFF(SEQ1(I),SEQ2(I),W1,W2,W3,W4,W5)
C
C if either are site residues.....
C
              if ((R1(14,L) .eq. 1) .or. (R2(14,M) .eq. 1)) then
                write (6,*) ' debug: found a site ',L,R1(14,L),M,R2(14,M)
                TOTSITDIFF=TOTSITDIFF+AADIFF(SEQ1(I),SEQ2(I),W1,W2,W3,W4,W5)
                NSITECOMP=NSITECOMP+1
                if (SEQ1(I) .eq. SEQ2(I)) then
                  NIDSITECOMP=NIDSITECOMP+1
                end if
                if ((R1(3,L) .eq. 1) .or. (R2(3,M) .eq. 1)) then
                  write (6,*) ' debug: found an acc site ',L,R1(3,L),M,R2(3,M)
                  TOTACCAADIFF=TOTACCAADIFF+AADIFF(SEQ1(I),SEQ2(I),W1,W2,W3,W4,W5)
                  NSITEACCCOMP=NSITEACCCOMP+1
                  if (SEQ1(I) .eq. SEQ2(I)) then
                    NIDACCSITECOMP=NIDACCSITECOMP+1
                  end if
                end if
                if (SEQ1(I) .eq. SEQ2(I)) then
                  FLAG='  '
                  if (SITEFLAG(I)(1:1) .ne. '*') then
                    SITEFLAG(I)(1:1)='%'
                  end if
                else 
                  SITEFLAG(I)(1:1)='*'
                  if ((R1(3,L) .eq. 1) .or. (R2(3,M) .eq. 1)) then
                    FLAG='*S'
                    SITEFLAG(I)(2:2)='+'
                  else
                    FLAG='*B'
                    if (SITEFLAG(I) .ne. '*-') then
                      SITEFLAG(I)(2:2)='-'
                    end if
                  end if
                end if
                write (STDOUT,'(''site: alignment position '',I4,1X,A,1X,A,'' -- '',A,1X,A,1X,A)')
     +                 I,NAME1,SEQ1(I),NAME2,SEQ2(I),FLAG
              end if 
            end if
          end if
        end do 
      end if
C
      return
      end
