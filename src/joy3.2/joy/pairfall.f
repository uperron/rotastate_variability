      subroutine PAIRFALL(SEQ1,SEQ2,LENGTH,R1,R2,NMUTID,NMUTIDAC)
C
C copyright (c) 1988-1995 John Overington
C part of joy
C
C A subroutine to calculate a series of substitution tables as a function of
C residue identity between the compared set.
C
C binned into 10% intervals 100-90, 90-80, etc.
C first of all calc PID, over length of alignment between pair !!
C then simply fill the relevent table.
C
      include 'joy.h'
C
      character*1 SEQ1(MAXRES), SEQ2(MAXRES)
      integer LENGTH, NID, NPOS, NMUTID(22,22,10), NMUTIDAC(22,22,10,2)
      integer R1(NUMFEAT,MAXRES), R2(NUMFEAT,MAXRES)
      integer I, J, K, L, M, IBIN
      real PID
C
C ----------------------------------------------------------------------------
C
C get PID of sequences (C and J should have been sorted out by now)
C
      NPOS=0
      NID=0
      do I=1,LENGTH
        if ( ISAA(SEQ1(I)) .and. ISAA(SEQ2(I)) ) then
          NPOS=NPOS+1
          if (SEQ1(I) .eq. SEQ2(I)) then
            NID=NID+1
          end if
        end if
      end do
C
      PID=(real(NID)/real(NPOS))*100.0
C
C ----------------------------------------------------------------------------
C
C now calc the substitutions
C
      L=0
      M=0
      IBIN=int((PID+0.1)/10.0)+1
C
      do I=1,LENGTH
        if (ISAA(SEQ1(I))) then
          L=L+1
          J=R1(1,L)
        else
          J=22
        end if
        if (ISAA(SEQ2(I))) then
          M=M+1
          K=R2(1,M)
        else
          K=22
        end if
        NMUTID(J,K,IBIN)=NMUTID(J,K,IBIN)+1
        NMUTID(K,J,IBIN)=NMUTID(K,J,IBIN)+1
      end do
C
C handle inaccessible bit
C
      L=0
      M=0
      do I=1,LENGTH
        if (ISAA(SEQ1(I))) then
          L=L+1
          J=R1(1,L)
        else
          J=22
        end if
        if (ISAA(SEQ2(I))) then
          M=M+1
          K=R2(1,M)
        else
          K=22
        end if

        if (R1(3,L) .eq. 1) then
          NMUTIDAC(J,K,IBIN,1)=NMUTIDAC(J,K,IBIN,1)+1
        else
          NMUTIDAC(J,K,IBIN,2)=NMUTIDAC(J,K,IBIN,2)+1
        end if 
        if (R2(3,M) .eq. 1) then
          NMUTIDAC(K,J,IBIN,1)=NMUTIDAC(K,J,IBIN,1)+1
        else
          NMUTIDAC(K,J,IBIN,2)=NMUTIDAC(K,J,IBIN,2)+1
        end if
      end do
C
      return
      end
