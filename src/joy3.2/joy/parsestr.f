      subroutine PARSESTR(STRING,DELIM,NFIELDS,FIELDS)
C
C converts STRING into NFIELDS of DELIM delimitted FIELDS
C must have at least one field
C
      implicit NONE
      integer MAXFIELD
      parameter (MAXFIELD=30)
      character*(*) FIELDS(MAXFIELD)
      character*(*) STRING
      character*1 DELIM
      integer NFIELDS, LASTCHAR, I, J
C
      do I=1,MAXFIELD
        do J=1,len(FIELDS(I))
          FIELDS(I)(J:J)=' '
        end do
      end do
C
C get out fields
C
      NFIELDS=0
1     I=index(STRING,DELIM)
      if (I .lt. 1) goto 2
      NFIELDS=NFIELDS+1 
      if (NFIELDS .gt. MAXFIELD) then
        call ERROR('joy: too many fields',' ',1)
      end if
      FIELDS(NFIELDS)(1:I-1)=STRING(1:I-1)
      do J=1,I
        STRING(J:J)=' '
      end do
      call LJUST(STRING) 
      goto 1
2     continue
C
C handle terminal field
C
      NFIELDS=NFIELDS+1
      FIELDS(NFIELDS)(1:LASTCHAR(STRING))=STRING(1:LASTCHAR(STRING))
C
      return
      end
