      subroutine PARSETREE(NSTR,STRNAM,INDX)
C
C a routine to read in treefile and obtain a better order for the tree
C
C watch out for overflow errors on BUFFER
C
      integer MAXSTR, STDOUT, STDIN, STDERR
      parameter (MAXSTR=40, STDIN=5, STDOUT=6, STDERR=0)
C
      integer BUFFLEN
      parameter (BUFFLEN=1024)
C
      character*10 STRNAM(MAXSTR)
      integer INDSTR(MAXSTR), NSTR, I, INDX(MAXSTR)
C
      character*(BUFFLEN) BUFFER
C
C assume it is all on one line !
C
      open (unit=71,file='treefile',status='old',form='formatted',
     +      err=900)
      read (71,'(A)',err=901) BUFFER
      close (unit=71)
C
C how to do it --- get index of all of names then sort these
C
      do I=1,NSTR
        INDSTR(I)=index(BUFFER,STRNAM(I)(1:lastchar(STRNAM(I))))
        if (INDSTR(I) .le. 0) then
          write (STDERR,'(''kitschorder: code not found in treefile '',A)')
     +           STRNAM
        end if
C        write (6,*) i,' ',indstr(i),' ',strnam(i)
      end do
C
C now need to sort these by index (routine from Numerical recipes)
C
      call INDEXX(NSTR,INDSTR,INDX)
C
C     do I=1,NSTR
C       write (6,*) i,' ',indx(i),' ',strnam(indx(i))
C     end do
C
      return
C
900   call ERROR('joy: cannot read treefile',' ',0)
      return
C
901   call ERROR('joy: error reading treefile',' ',1)
      return
      end
C
C ========================================================================
C
      SUBROUTINE INDEXX(N,ARRIN,INDX)
      DIMENSION ARRIN(N),INDX(N)
      DO 11 J=1,N
        INDX(J)=J
11    CONTINUE
      L=N/2+1
      IR=N
10    CONTINUE
        IF(L.GT.1)THEN
          L=L-1
          INDXT=INDX(L)
          Q=ARRIN(INDXT)
        ELSE
          INDXT=INDX(IR)
          Q=ARRIN(INDXT)
          INDX(IR)=INDX(1)
          IR=IR-1
          IF(IR.EQ.1)THEN
            INDX(1)=INDXT
            RETURN
          ENDIF
        ENDIF
        I=L
        J=L+L
20      IF(J.LE.IR)THEN
          IF(J.LT.IR)THEN
            IF(ARRIN(INDX(J)).LT.ARRIN(INDX(J+1)))J=J+1
          ENDIF
          IF(Q.LT.ARRIN(INDX(J)))THEN
            INDX(I)=INDX(J)
            I=J
            J=J+J
          ELSE
            J=IR+1
          ENDIF
        GO TO 20
        ENDIF
        INDX(I)=INDXT
      GO TO 10
      END
