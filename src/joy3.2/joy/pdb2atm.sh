#!/bin/sh
# from /u/jpo/src/joy/pdb2atm 
#
# converts a raw PDB file to a nicer atm file 
#
# added jank to tests (written by Frank Eisenmenger)

PATH=BINDIR:/bin:/usr/bin
CHECK=true
DOGET=false
CHN="@"
END="@@@@@"
START="@@@@@"

STR=stripper
CR=chngres
GC=getchn
GR=getrng

if [ $# -lt 1 ] ; then
  echo "usage: pdb2atm [ options ] files"
  exit 1
fi

while getopts c:E:S: d
do
  case $d in
    c) CHN=$OPTARG ; DOGET=true ;;
    E) END=$OPTARG ; DOGET=true ;;
    S) START=$OPTARG ; DOGET=true ;;
    *) echo $USAGE ; exit 2 ;;
  esac
done
shift `expr $OPTIND - 1`

if [ $# -lt 1 ] ; then
  echo "usage: pdb2atm [ options ] files"
  exit 1
fi

for INFP in $* ; do

  INF=`basename $* .pdb`

  if [ ! \( -r $INF.pdb -a -f $INF.pdb \) ] ; then
    echo "$0: file does not exist $INF.pdb" > /dev/tty
    continue
  fi

  $STR $INF.pdb > $INF.tmp

  if $DOGET ; then
    $GC $CHN $INF.tmp | $GR $START $END > $INF.atm
  else
    mv $INF.tmp $INF.atm
  fi

  if $CHECK ; then
#
# these should now be done by jank
#    chkres $INF.atm
#    chkatm $INF.atm
#    chklen $INF.atm
#
    jank   $INF.atm
  fi
  rm -f $INF.tmp

done

exit 0

