c     assume that the first four characters of the protein code are the same
c     as the PDB code (which can only be 4 characters):

      subroutine pdbnam(dirpath, code, pdbloc, ipdbfnd, compressed, 
     &                  istop)
        implicit none
        integer mpdbfil
        parameter (mpdbfil = 2000)
        integer mdir
        parameter (mdir=20)
        integer npdbfil, i, ifindword, lenr, ipos, ipdbfnd, ndir, istop
        character dirpath*(*), dir(mdir)*(255)
        character pdbfil(mpdbfil)*(100), pdbcod(mpdbfil)*(5)
        character code*(*), pdbloc*(*), dcode*(512)
        logical filexs,compressed
        common /intpdb/ npdbfil
        common /chrpdb/ pdbfil,pdbcod

        call str_sn3(dirpath, dir, mdir, ndir)
        if (ndir .eq. 0) then
          dir(1) = ' '
          ndir = 1
        end if

        do  i = 1, ndir

          call concat(dir(i), code, dcode)

c ------- check first if the code is not actually a full filename:
          ipdbfnd = 1
          pdbloc = dcode
          compressed = .false.
          if (filexs(pdbloc)) return

          ipdbfnd = 2
          call addstr(pdbloc, '.Z')
          compressed = .true.
          if (filexs(pdbloc)) return

          ipdbfnd = 3
c ------- append .atm to the code and see if it exists in the current directory
          call concat(dcode, '.atm', pdbloc)
          compressed = .false.
          if (filexs(pdbloc)) return

          ipdbfnd = 4
          call addstr(pdbloc, '.Z')
          compressed = .true.
          if (filexs(pdbloc)) return

          ipdbfnd = 5
c ------- append .pdb to the code and see if it exists in the current directory
          call concat(dcode, '.pdb', pdbloc)
          compressed = .false.
          if (filexs(pdbloc)) return

          ipdbfnd = 6
          call addstr(pdbloc, '.Z')
          compressed = .true.
          if (filexs(pdbloc)) return

        end do

c ----- code is not a full filename, try to find it in the library 
c       of codes:

        ipos = ifindword(code(1:4), pdbcod, npdbfil)

        if (ipos .gt. 0) then
          ipdbfnd = 7
          pdbloc = pdbfil(ipos)
          compressed = .false.
          if (filexs(pdbloc)) return

          ipdbfnd = 8
          call concat(pdbfil(ipos), '.Z', pdbloc)
          compressed = .true.
          if (filexs(pdbloc)) return
        end if

        write(*,'(2a)') 'pdbnam__W> filename for code not found: ',
     &       code(1:lenr(code))
        pdbloc = 'file-not-found'

        if (istop .eq. 1) stop

        return
      end




c --- assume: pdbxxxxc.ent, where xxxx is PDB code, and c is an optional
c             chain id.

      subroutine ipdbnam
        implicit none
        integer mpdbfil
        parameter (mpdbfil = 2000)
        integer npdbfil, i, ierr, l, lenr
        character pdbfil(mpdbfil)*(100), pdbcod(mpdbfil)*(5)
        character root*(20),pdblst*(100)
        common /intpdb/ npdbfil
        common /chrpdb/ pdbfil,pdbcod

        call getenv('PDBENT', pdblst)
        if (pdblst(1:1).eq.' ')
     &  write(*,'(a)')'ipdbnam_W> environment variable PDBENT undefined'

        call openf2(90,pdblst,'OLD','SEQUENTIAL','FORMATTED',3,
     &              .false.,ierr)
        if (ierr .eq. 0) then
          npdbfil = 1
10        read(90, '(a)', end=100) pdbfil(npdbfil)
            npdbfil = npdbfil + 1
            if (npdbfil .gt. mpdbfil)stop 'ipdbnam_E> increase MPDBFIL'
            go to 10
100       continue
          npdbfil = npdbfil - 1
          close(90)
        else
          npdbfil = 0
        end if

c ----- get the protein PDB codes from the pdb file list; PDB code is
c       xxxx in pdbxxxx.ent.Z, where pdb is an arbitrary 3-letter string,
c       xxxx is an arbitrary string at least one character long, and 
c       everything after the first period is arbitrary altogether.
        do  i = 1, npdbfil
          call rootfn2(pdbfil(i), root)
          l = lenr(root)

          if (l.lt.4) then
            write(*,'(2a)') 'ipdbnam_E> PDB name non conforming: ',
     &      pdbfil(i)(1:lenr(pdbfil(i)))
            stop
          end if
 
          pdbcod(i) = root(4:)
        end do

        return
      end

