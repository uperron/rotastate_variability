      real function PID(SEQ1,SEQ2,LENGTH)
C
C copyright (c) 1988-1991 John Overington
C part of joy
C
C Returns pairwise percentage identity between two character sequence arrays
C Note strings include insertion codes. Identity is calculated over 100
C equivalenced positions, even if an amino acid is equivalenced with an
C insertion. However matches of - with - are not counted
C
      include 'joy.h'
C
      character*1 SEQ1(MAXRES), SEQ2(MAXRES)
      integer NID, NBOTH, LENGTH, I
C
      PID=0.0
      NID=0
      NBOTH=0
C
      do I=1,LENGTH
        if ((SEQ1(I).ne.'-').and.(SEQ2(I).ne.'-')) then
          NBOTH=NBOTH+1
          if (SEQ1(I).eq.SEQ2(I)) then
            NID=NID+1
          end if
        end if
      end do
      PID=(REAL(NID)/REAL(NBOTH))*100.0
C
      return
      end
C
C ############################################################################
C
      subroutine PIDSTR(NSTR,STRNAM,PIDENT,NSUBST,PIDENTSTR,PIDENTACC,
     -                  NSEQ,SEQNAM,PIDSTRNEW,NSUBSTSTRNEW,
     -                  NEWSEQ,LEN,LENFIL,NSUBSTSTR,NSUBSTACC,NAVELEN,
     +                  FAMNAME,FAMCLASS,USESITE,TOTAADIFF,TOTSITDIFF,TOTACCAADIFF,SITVAL,
     +                  NSITE)
C     
C Handles all %ID stuff
C changed format of PID line
C
      include 'joy.h'
C
      character*(*) STRNAM(MAXSEQ), SEQNAM(MAXNEWSEQ)
      character*(*) FAMNAME, FAMCLASS
      character*5 TMPBUF(MAXSEQ)
      character*1 NEWSEQ(MAXRES,MAXNEWSEQ)
      integer NSTR, NCOMP, K, I, J, NSUBST(MAXSEQ,MAXSEQ)
      integer NSUBSTSTRNEW(MAXSEQ,MAXNEWSEQ), NSEQ, LEN, LENFIL(MAXSEQ)
      real PIDAVE, PID, PIDSEQ(MAXNEWSEQ,MAXNEWSEQ)
      real PIDENT(MAXSEQ,MAXSEQ)
      real PIDAVESTR(4), PIDAVEACC(2), PIDSTRNEW(MAXRES,MAXNEWSEQ)
      real PHIGH, PLOW
      real PIDENTACC(2,MAXSEQ,MAXSEQ), PIDENTSTR(4,MAXSEQ,MAXSEQ)
      integer NSUBSTSTR(4,MAXSEQ,MAXSEQ), NSUBSTACC(2,MAXSEQ,MAXSEQ)
C
      real TOTAADIFF(MAXSEQ,MAXSEQ), TOTACCAADIFF(MAXSEQ,MAXSEQ), TOTSITDIFF(MAXSEQ,MAXSEQ), SITVAL
      real TMPTOT
      logical USESITE
      integer NSITE(5,MAXSEQ,MAXSEQ)
C
      integer NLOW1, NLOW2, NHIGH1, NHIGH2, NAVELEN
C
      logical PHYLIP
      PHYLIP=.true.
C
      if (NSTR.gt.MAXSEQ) then
        call ERROR('joy: too many structures',' ',1)
      end if
C     
C     Structures with structrues
C     
      if (NSTR.gt.1) then
        write (STDOUT,'(/,
     +''Sequence identity of structures with structures'',/,
     +''str1'',9x,''str2'',13x,''total  alpha   beta  + phi'',
     +''   coil    acc  inacc  pcprop pcpropS pcpropA %id sit'')')
C
        PHIGH=-1.0
        PLOW=101.0
        PIDAVE=0.0
        NCOMP=0
C
        do K=1,4
          PIDAVESTR(K)=0.0
        end do
        do K=1,2
          PIDAVEACC(K)=0.0
        end do
C
        do I=1,NSTR
          do J=I+1,NSTR
            NCOMP=NCOMP+1
            write (STDOUT,'(A,''-- '',A,F5.2,7(F7.2),4f8.2)') STRNAM(I),STRNAM(J),SITVAL,
     -            PIDENT(I,J),PIDENTSTR(1,I,J),PIDENTSTR(2,I,J),
     -            PIDENTSTR(3,I,J),PIDENTSTR(4,I,J),PIDENTACC(1,I,J),
     -            PIDENTACC(2,I,J),TOTAADIFF(I,J),TOTSITDIFF(I,J),TOTACCAADIFF(I,J),
     -            (real(NSITE(4,I,J)))/(real(NSITE(2,I,J)))*100.0
            write (STDOUT,'(28X,7(I7),5(I5),1X)') NSUBST(I,J),NSUBSTSTR(1,I,J),
     -            NSUBSTSTR(2,I,J),NSUBSTSTR(3,I,J),NSUBSTSTR(4,I,J),
     -            NSUBSTACC(1,I,J),NSUBSTACC(2,I,J),
     -            NSITE(1,I,J),NSITE(4,I,J),NSITE(2,I,J),NSITE(5,I,J),NSITE(3,I,J)
            PIDAVE=PIDAVE+PIDENT(I,J)
            do K=1,4
              PIDAVESTR(K)=PIDAVESTR(K)+PIDENTSTR(K,I,J)
            end do
            do K=1,2
              PIDAVEACC(K)=PIDAVEACC(K)+PIDENTACC(K,I,J)
            end do
            if (PIDENT(I,J).gt.PHIGH) then
              NHIGH1=I
              NHIGH2=J
              PHIGH=PIDENT(I,J)
            end if
            if (PIDENT(I,J).lt.PLOW) then
              NLOW1=I
              NLOW2=J
              PLOW=PIDENT(I,J)
            end if
          end do
        end do
C
        if (NCOMP.gt.1) then
           PIDAVE=PIDAVE/REAL(NCOMP)
           do K=1,4
              PIDAVESTR(K)=PIDAVESTR(K)/REAL(NCOMP)
           end do
           do K=1,2
             PIDAVEACC(K)=PIDAVEACC(K)/REAL(NCOMP)
           end do
           write (STDOUT,'(15x,''average '',7(f7.2),/)') PIDAVE,
     -          (PIDAVESTR(K),K=1,4),(PIDAVEACC(K),K=1,2)
           write (STDOUT,'(''most similar  - '',A,''-- '',A,2X,F6.2)')
     -           STRNAM(NHIGH1),STRNAM(NHIGH2),PIDENT(NHIGH1,NHIGH2)
           write (STDOUT,'(''least similar - '',A,''-- '',A,2X,F6.2)')
     -           STRNAM(NLOW1),STRNAM(NLOW2),PIDENT(NLOW1,NLOW2)
        end if
      end if
      if (LASTCHAR(FAMNAME) .lt. 2) then
        FAMNAME='untitled'
      end if
      write (STDOUT,'(''# line for families: '',
     +       A,'':'',I3,'':'',I4,'':'',F6.2)')
     +       FAMNAME(1:LASTCHAR(FAMNAME)),NSTR,NAVELEN,PIDAVE
      write (STDOUT,'(''# line for classes: '',
     +       A)') FAMCLASS(1:lastchar(FAMCLASS))
C     
C     Structures with sequences
C
      if (NSEQ.gt.1.and.NSTR.gt.1) then
         write (STDOUT,'(/,''Sequence identity of structures '',
     +          ''with sequences'',//,
     -        13X,''structure    sequence      %i.d.     N'')')
         PIDAVE=0.0
         NCOMP=0
         do i=1,NSTR
            do j=1,NSEQ
               write (STDOUT,'(8X,6X,A,''-- '',A,2X,F6.2,2X,I5)')
     +                STRNAM(I),
     -              SEQNAM(j),PIDSTRNEW(I,J),NSUBSTSTRNEW(I,J)
               PIDAVE=PIDAVE+PIDSTRNEW(I,J)
               NCOMP=NCOMP+1
            end do
         end do
         PIDAVE=PIDAVE/REAL(NCOMP)
         write (STDOUT,'(15x,''average  '',f6.2,4(6x,f6.2))') PIDAVE
      end if
C     
C     Sequences with sequences
C     
      if (NSEQ.gt.1) then
        PIDAVE=0.0
        NCOMP=0
        write (STDOUT,'(/,''Sequence identity of sequences with sequences'',//,
     -       13X,''sequence     sequence      %i.d.'')')
        do i=1,NSEQ
          do j=i+1,NSEQ
            pidseq(i,j)=PID(NEWSEQ(1,I),NEWSEQ(1,J),LEN)
            write (STDOUT,'(8X,6X,A,''-- '',A,2X,F6.2)') SEQNAM(i),
     -           SEQNAM(j),PIDSEQ(i,j)
            PIDAVE=PIDAVE+PIDSEQ(I,J)
            NCOMP=NCOMP+1
          end do
        end do
        PIDAVE=PIDAVE/REAL(NCOMP)
        write (STDOUT,'(15x,''average  '',f6.2,4(6x,f6.2))') PIDAVE
      end if
C
C generate TMPBUF as hack for titles, I could not get this to work simpl
C
      do I=1,NSTR
        TMPBUF(I)=STRNAM(I)(1:5)
      end do
C
C removed restriction of 12 on table printing, watch out for line lengths
C actually 50 for format statement
C
      if (NSTR .gt. 1) then
        write (STDOUT,'(/,''pairwise sequence identities'')')
        write (STDOUT,'(''pid      '',5X,50(1X,A5))') (TMPBUF(K),K=1,NSTR)
        do I=1,NSTR
           write (STDOUT,'(''pid      '',A5,50F6.1)') STRNAM(I)(1:LENFIL(I)),
     +          (PIDENT(K,I),K=1,NSTR)
        end do
        write (STDOUT,'(/,''pairwise sequence distances '',
     +         ''(-100*ln(ID/100))'')')
        write (STDOUT,'(''distance '',5X,50(1X,A5))')
     +         (TMPBUF(K),K=1,NSTR)
        do I=1,NSTR
          TMPTOT=0.0
          do K=1,NSTR
            TMPTOT=TMPTOT+(-100.0*LOG((PIDENT(K,I)/100.0)))
          end do
          write (STDOUT,'(''distance '',A5,50(F6.1))') 
     +           STRNAM(I)(1:LENFIL(I)),
     +          (-100.0*LOG((PIDENT(K,I)/100.0)),K=1,NSTR),TMPTOT
        end do
C
        if (USESITE) then
          do I=1,NSTR
            do J=I+1,NSTR
              TOTAADIFF(J,I)=TOTAADIFF(I,J)
              TOTSITDIFF(J,I)=TOTSITDIFF(I,J)
              TOTACCAADIFF(J,I)=TOTACCAADIFF(I,J)
            end do
          end do
          write (STDOUT,'(/,''pairwise PC-based differences '')')
          write (STDOUT,'(''PC distance       '',5X,50(2X,A5))') (TMPBUF(K),K=1,NSTR)
          do I=1,NSTR
             TMPTOT=0.0
             do K=1,NSTR
               TMPTOT=TMPTOT+TOTAADIFF(K,I)
             end do
             write (STDOUT,'(''PC distance      '',A5,50F7.1)') STRNAM(I)(1:LENFIL(I)),
     +            (TOTAADIFF(K,I),K=1,NSTR),TMPTOT
          end do
          write (STDOUT,'(/,''pairwise PC-based site differences '',f5.2)') SITVAL
          write (STDOUT,'(''PC site distance       '',5X,50(2X,A5))') (TMPBUF(K),K=1,NSTR)
          do I=1,NSTR
             TMPTOT=0.0
             do K=1,NSTR
               TMPTOT=TMPTOT+TOTSITDIFF(K,I)
             end do
             write (STDOUT,'(''PC site distance      '',A5,50F7.1)') STRNAM(I)(1:LENFIL(I)),
     +            (TOTSITDIFF(K,I),K=1,NSTR),TMPTOT
          end do
          write (STDOUT,'(/,''pairwise PC-based accessible site distances '',f5.2)') SITVAL
          write (STDOUT,'(''PC site (acc) distance  '',5X,50(2X,A5))')
     +           (TMPBUF(K),K=1,NSTR)
          do I=1,NSTR
             TMPTOT=0.0
             do K=1,NSTR
               TMPTOT=TMPTOT+TOTACCAADIFF(K,I)
             end do
             write (STDOUT,'(''PC site (acc) distance '',A5,50(F7.1))') STRNAM(I)(1:LENFIL(I)),
     +            (TOTACCAADIFF(K,I),K=1,NSTR),TMPTOT
          end do
        end if
C
C if one wants a phylip output file then write one
C
        if (PHYLIP) then
          open (unit=41,file='DIST.mat',status='unknown',form=
     +          'formatted',err=900)
          write (41,'(I4,''   Y'')',err=901) NSTR
          do I=1,NSTR
             write (41,'(A10,50(F6.1))',err=901) STRNAM(I),
     -            (-100.0*LOG((PIDENT(K,I)/100.0)),K=1,NSTR)
          end do
          close (unit=41)
        end if
      end if
C     
      return
C
900   write (STDERR,'(''joy: cannot open DIST.mat file'')')
      return
901   write (STDERR,'(''joy: cannot write to DIST.mat file'')')
      return
C
      end
