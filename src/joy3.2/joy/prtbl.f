      subroutine PRTBL(NMUT,STRING)
C
C copyright (c) 1988-1991 John Overington
C part of joy
C
C To output the raw scores, probabilities and distances for the substitution 
C matrix NMUT. 
C If required output is also sent to unit 4 for further analysis 
C (controlled by the logical LATER). Output of distance matrix information
C controlled by logical DISDIS. Later version does not output data to the 
C analysis output file, but data simply goes to .SUM file. (V 16). Some of the
C redundant bits were kept in source for future use
C Later version (V 16) changed to allow the treatment of an insertion as a
C type of residue. (so routine is fed with a 21*21 matrix will only output as
C a 20*21 though as last column contains only garbage).
C
C Removed disdis, removed later, changed to 22 * 22 table. Renamed to prtbl
C
      implicit      NONE
      character*(*) STRING
      character*22  RES
      integer       NMUT(22,22)
      integer       I,J
C
      data RES    /'ACDEFGHIKLMNPQRSTVWYJ-'/
C
C Output total score substitution data
C
      write (4,'(/,5x,a)') STRING
      write (4,'(9x,21(a,3x))') (RES(I:I),I=1,21)
      do I=1,22
        write (4,'(5x,a,21i4)') RES(I:I),(NMUT(J,I),J=1,21)
      end do
C
      return
      end
