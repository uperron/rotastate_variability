      subroutine RDALI(FILE,NSTR,STRSEQ,LENGTH,LENSTR,STRNAM,
     +                 LENSTRNAM,LABEL,TEXT,ORDER,NENTRY,SEQ,
     +                 SEQNAM,NSEQ,USENUMBER,NUMFILE,NNUMFILE,
     +                 CHECK,FAMNAME,FAMCLASS,ACCMCH,PDBCOD,RANGE,
     +                 ALNCHN)
C
C copyright (c) 1988-1991 John Overington
C part of joy
C
C Routine to  read in a multiple alignment, with optional numbers, labels, etc
C and return length etc of alignment etc.
C Default (suggested) filename is .PAP, produced by COMPARER
C N.B. format of files have changed after v 2.5 (default .ali)
C
C Format of the alignment file is very important.
C The sequences must appear as the upper case one letter code aligned in columns
C so that equivalenced residues lie above and below one another. Deletions in
C one sequence must be shown as the '-' character and the N- and C-termini
C must be blocked. Sequences must appear in one block, and must be the first
C block of text encountered in the file. The sequences must start in column 11
C and appear as one long string. N.B. This places a number of restrictions on
C the program. The major one is that for large alignments the compiler must
C support long character variables, e.g. character*500.
C New version allows the reading of up to ten sequences that do not have
C features attached to them but which may subsequently be included in the
C alignment.
C
C Later version that is simplified with respect to the input of labels and
C text strings in the alignment. Added ability to deal with structurally
C unknown sequences. Removed strange restraint on the ordering of individual
C labels, sequences and text strings.
C
C Stopped the user using funny characters as text strings, stored in the array
C ILLEGAL. Made change to allow all alignment to be 'new' sequences.
C
C Increased length of CARD to 1024
C
C Changed format of input file, see RDTEM, WRTEM, Changed names of lots of
C variables so that the source is readable. Decreased length of card to 80
C chars.
C
C Changed handling of insertion chars, so that ASDF/-----ASDF can replace
C ASDF//////ASDF. This is so the data is compatable with modeller.
C
C format of file is
C
C C;                 - introduce a comment
C R;                 - introduce a reference (like a comment)
C #                  - introduces a comment
C
C with few exceptions, thes comments are ignored.
C
C >T1;text           - indicates text data will follow
C text               - must follow previous line
C text data in blocks of length N terminated by a star '*'
C jpo -- changed blanks in text to ., this should allow better handling of
C free format (coming soon !) 19-7-95
C Andrej actually did the free format conversion 20-7-96
C
C >P1;title          - denotes a sequence of name title
C sequence/structure - sequence means just a sequence, structure means
C                      that there is structural information for the sequence
C                      If ACCMCH=.T., then structure can also be structureM,
C                      structureX, and structureN (model, x-ray, NMR)
C sequence data in blocks of length N terminated by a star '*'
C
C >N1;number         - denotes a numbering scheme for the alignment
C label data in blocks of length N terminated by a star'*'
C
C modified program to read F1 entries equivalently to >P1 entries
C
C increased length of CARD to 132, and added checking of alignment file
C fields.
C
C Can now have / in seqeunce files, this is translated to a blank in the
C final alignment (useful for internal cleavages, domain alignments etc).
C
C ----------------------------------------------------------------------------
C
      include 'joy.h'
C
      integer MAXFIELDS
      parameter (MAXFIELDS=30)
C
      character*(*) FILE, STRNAM(MAXSEQ), SEQNAM(MAXNEWSEQ)
      character*(*) NUMFILE(MAXSEQ), FAMNAME
C PDB code, residue range, and residue range chain id's for -d option:
      character*(*) PDBCOD(MAXSEQ), RANGE(2,MAXSEQ), ALNCHN(2,MAXSEQ)
      character*(MAXRES) BIGBUFF
      character*132 CARD, CARDTMP
      character*32 FAMCLASS
      character*20 FIELDS(MAXFIELDS)
      character*10 BUFFER
      character*6 ORDER(MAXSEQ+MAXNEWSEQ+MAXLAB+MAXTEXT)
      character*3 LABEL(MAXRES,MAXLAB)
      character*1 STRSEQ(MAXRES,MAXSEQ), SEQ(MAXRES,MAXNEWSEQ)
      character*1 TEXT(MAXRES,MAXTEXT)
      integer NSTR, LENSEQ(MAXNEWSEQ), LENSTR(MAXSEQ), LENBUFF
      integer REPLENSTR(MAXSEQ), LCARD
      integer LENSTRNAM(MAXSEQ), LENSEQNAM(MAXNEWSEQ), NCHAR(MAXRES)
      integer LABPOS(MAXRES), NSEQ, NNUMFILE, NENTRY, NFIELD
      integer I, J, K, L, M, N, NVAL, NITL, LENGTH, NTEXT, NLABEL
      logical FOUND, USENUMBER, CHECK, ACCMCH
      logical INGAP
C
C ----------------------------------------------------------------------------
C
C Logical and other initializations
C
      data FOUND /.false./
C
      NTEXT=0
      NLABEL=0
      N=0
      NENTRY=1
      NSTR=0
      NSEQ=0
      NNUMFILE=0
      FAMCLASS='undefined family class !        '
C
      do I=1,MAXLAB
        do J=1,MAXRES
          LABEL(J,I)='   '
        end do
      end do
      do I=1,MAXTEXT
        do J=1,MAXRES
          TEXT(J,I)=' '
        end do
      end do
      do I=1,MAXNEWSEQ
        SEQNAM(I)='          '
      end do
      do I=1,MAXSEQ
        NUMFILE(I)='               '
        STRNAM(I)='          '
      end do
      do I=1,MAXSEQ+MAXNEWSEQ+MAXLAB+MAXTEXT
        ORDER(I)='      '
      end do
C
C Open alignment/sequence file
C
      if (VERBOSE) then
        write (STDERR,'(''joy: reading alignment file '',A,1X,L)')
     +  FILE,ACCMCH
      end if
      open (unit=1,file=FILE,status='OLD',form='FORMATTED',err=900)
C
C Read in each line of the alignment file, check for an alphanumeric character
C in the first column, if one is found then  read in all following lines as the
C alignment data until a blank line is found.
C If two labels are found with the same identifier then only the first one
C encountered is read
C
C 10 is the main control label
C
10    read (1,'(A)',end=100,err=901) CARD
        if (CARD(1:5).eq.'     '.and.FOUND) then
          goto 100
C
C ----------------------------------------------------------------------------
C
C Handle the text strings, these print out essentially any printable characters
C in the relevant column of the alignment. Exceptions (errors) occur when the
C character is defined with some special meaning in the typesetter language
C
C String input is signalled by the string 'text', in lower case letters
C after the pseudo PIR identifier >T1; followed by a number (optional)
C
        else if (CARD(1:4).eq.'>T1;') then
          NTEXT=NTEXT+1
          if (NTEXT.gt.MAXTEXT) then
            call ERROR('joy: too many text strings',' ',3)
          end if
          ORDER(NENTRY)='text  '
          NENTRY=NENTRY+1
          read (1,'(A)',err=901) CARD
          if (CARD(1:4).ne.'text') then
            call ERROR('joy: bad format alignment (text) -',
     +                 CARD(1:10),1)
          end if
          N=1
2         read (1,'(A)',err=904) CARD
          LCARD = lenr(card)
          if (INDEX(CARD,'*').gt.0) then
            read (CARD,'(999A1)')
     +            (TEXT(I,NTEXT),I=N,N+INDEX(CARD,'*')-2)
        goto 3
          else
            read (CARD,'(999A1)') (TEXT(I,NTEXT),I=N,N+LCARD-1)
          end if
          N=N+LCARD
        goto 2
3       continue
        if (ISSPEC(TEXT)) then
          call ERROR('joy: special LaTeX character in',
     +               TEXT(I,NTEXT),1)
        end if
        do I=1,N
          if (TEXT(I,NTEXT) .eq. '.') then
            TEXT(I,NTEXT)=' '
          end if
        end do
      goto 10
C
C comment and command lines
C
C 2 cases, first is #
C
        else if (CARD(1:1) .eq. '#') then
          if (CARD(3:9) .eq. 'family:') then
            read (CARD(11:),'(A)') FAMNAME
          else if (CARD(3:8) .eq. 'class:') then
            read (CARD(10:),'(A)') FAMCLASS
          end if
      goto 10
C
C          second is C;
C
C This section of code can be used for all sorts of alignment type information
C
C N.B. Only family and class are currently implemented.
C
        else if (CARD(1:2) .eq. 'C;') then
          if (CARD(4:10)      .eq. 'family:') then
            read (CARD(12:),'(A)') FAMNAME
          else if (CARD(4:9)  .eq. 'class:') then
            read (CARD(11:),'(A)') FAMCLASS
          else if (CARD(4:13) .eq. 'alignment:') then
            continue
          else if (CARD(4:8)  .eq. 'date:') then
            continue
          else if (CARD(4:14) .eq. 'disulphide:') then
            continue
          end if
      goto 10
C
C Reading in sequences, have essentially two choices, either the sequences
C will have structural information available or they will be simple sequences.
C Handle these with the same mechanism, use standard >P1; identifier followed
C by a line containing either 'sequence' or 'structure'
C
        else if (CARD(1:4) .eq. '>P1;' .or.
     +           CARD(1:4) .eq. '>F1;') then
          read (CARD(5:14),'(A)',err=901) BUFFER
          read (1,'(A)',err=901) CARD
C
C first of all the sequence option
C
          if ((index(CARD,'sequence') .gt. 0) .or. (index(CARD,'structureM') .gt. 0)) then
            NSEQ=NSEQ+1
            if (NSEQ.gt.MAXNEWSEQ) then
              call ERROR('joy: too many sequences',' ',3)
            end if
            SEQNAM(NSEQ)=BUFFER
            LENSEQNAM(NSEQ)=INDEX(CARD(11:),' ')
            if (ISSPEC(SEQNAM(NSEQ))) then
              call ERROR('joy: special LaTeX character in',
     +                   SEQNAM(NSEQ),1)
            end if
            FOUND=.true.
            ORDER(NENTRY)='newseq'
            NENTRY=NENTRY+1
C
            N=1
12          read (1,'(A)',err=901) CARD
            LCARD = lenr(card)
            if (INDEX(CARD,'*').gt.0) then
              read (CARD,'(999A1)')
     +         (SEQ(I,NSEQ),I=N,N+INDEX(CARD,'*')-2)
          goto 13
              else
                read (CARD,'(999A1)') (SEQ(I,NSEQ),I=N,N+LCARD-1)
              end if
              N=N+LCARD
            goto 12
13          continue
            LENSEQ(NSEQ)=I-1
            if (LENSEQ(NSEQ).gt.MAXRES) then
              call ERROR('joy: sequence too long',SEQNAM(NSEQ),3)
            end if
            if (CHECK) then
              do I=1,LENSEQ(NSEQ)
                if ((.not.ISAA(SEQ(I,NSEQ))) .and.
     +              (.not.ISPUNCT(SEQ(I,NSEQ)))) then
                  write (STDERR,'(''joy: non-amino acid '',A,
     -                            ''at position '',I4,'' in '',A)')
     -                   SEQ(I,NSEQ),I,SEQNAM(NSEQ)
                  call EXIT(1)
                end if
              end do
            end if
C
C Now the structure case
C
          else if ((index(CARD,'structureX') .gt. 0) .or. (index(CARD,'structureN') .gt. 0)) then
            NSTR=NSTR+1
            if (NSTR.gt.MAXSEQ) then
              call ERROR('joy: too many structures',' ',3)
            end if
C
C =========================================
C
C New section to check on figure for sequence length
C
            CARDTMP=CARD
            call PARSESTR(CARDTMP,':',NFIELD,FIELDS)
            if (NFIELD .lt. 6) then
c              call ERROR('joy: bad number of fields',CARDTMP(1:20),0)
            end if
c            read (FIELDS(5),'(I4)',err=910) REPLENSTR(NSTR)
c911         continue
C
C =========================================
C
C
C Store the additional information that you need when special access 
C mechanism is selected (-d):
C
            if (ACCMCH) then 
              PDBCOD(NSTR)=FIELDS(2)
              if (LASTCHAR(FIELDS(3)).gt.0) then
                RANGE(1,NSTR)=FIELDS(3)
              else
                RANGE(1,NSTR)='@@@@@'
              end if
              if (LASTCHAR(FIELDS(4)).gt.0) then
                ALNCHN(1,NSTR)=FIELDS(4)
              else
                ALNCHN(1,NSTR)='@'
              end if
              if (LASTCHAR(FIELDS(5)).gt.0) then
                RANGE(2,NSTR)=FIELDS(5)
              else
                RANGE(2,NSTR)='@@@@@'
              end if
              if (LASTCHAR(FIELDS(6)).gt.0) then
                ALNCHN(2,NSTR)=FIELDS(6)
              else
                ALNCHN(2,NSTR)='@'
              end if
              write(*,*)buffer,' : ',range(1,nstr),' : ',alnchn(1,nstr),
     +                  ' : ',range(2,nstr),' : ',alnchn(2,nstr)
            end if
C
            STRNAM(NSTR)=BUFFER
            if (ISSPEC(STRNAM(NSTR))) then
              call ERROR('joy: special LaTeX character in',
     +                   STRNAM(NSTR),1)
            end if
            FOUND=.TRUE.
            ORDER(NENTRY)='seq   '
            NENTRY=NENTRY+1
            LENSTRNAM(NSTR)=INDEX(STRNAM(NSTR),' ')-1
C
            N=1
22          read (1,'(A)',err=901) CARD
            LCARD = lenr(card)
            if (INDEX(CARD,'*').gt.0) then
              read (CARD,'(999A1)')
     +         (STRSEQ(I,NSTR),I=N,N+INDEX(CARD,'*')-2)
          goto 23
            else
              read (CARD,'(999A1)') (STRSEQ(I,NSTR),I=N,N+LCARD-1)
            end if
            N=N+LCARD
          goto 22
23        continue
            LENSTR(NSTR)=I-1
            if (LENSTR(NSTR).gt.MAXRES) then
              call ERROR('joy: sequence too long',STRNAM(NSTR),3)
            end if
            if (CHECK) then
              do I=1,LENSTR(NSTR)
                if ((.not.ISAA(STRSEQ(I,NSTR))).and.
     -              (.not.ISPUNCT(STRSEQ(I,NSTR)))) then
                  write (STDERR,'(''joy: non-amino acid '',A,
     -                          ''at position '',I4,'' in '',A)')
     -                 STRSEQ(I,NSTR),I,STRNAM(NSTR)
                  call EXIT(1)
                end if
              end do
            end if
      goto 10
          else
            call ERROR('joy: corrupted data (sequence) -',CARD(1:20),1)
          end if
      goto 10
C
C Handle the arbitrary number labels for each position in the alignment
C
C Almost certainly this will be broken in the new free format thing !!!!! jpo
C
        else if (CARD(1:4).eq.'>N1;') then
          if (CARD(5:5).eq.'!') then
            USENUMBER=.true.
            NNUMFILE=NNUMFILE+1
            read (CARD(6:15),'(A)') BUFFER
            NUMFILE(NNUMFILE)=BUFFER(1:LASTCHAR(BUFFER))
            if (VERBOSE) then
              write (STDOUT,'(''joy: will label alignment as in '',A)')
     -               NUMFILE(NNUMFILE)
            end if
          else
            read (1,'(A)',err=901) CARD
            if (CARD(1:5).ne.'label') then
              call ERROR('joy: corrupted data (labels) -',CARD(1:10),1)
            end if
            NLABEL=NLABEL+1
            if (NLABEL.gt.MAXLAB) then
              call ERROR('joy: too many labels',' ',3)
            end if
            ORDER(NENTRY)='label '
            if (USENUMBER) then
              ORDER(NENTRY)='labe! '
            end if
            NENTRY=NENTRY+1
C
C Build up the big buffer from the N long sections
C
            N=1
32          read (1,'(A)',err=901) CARD
            LCARD = lenr(card)
            if (INDEX(CARD,'*').gt.0) then
              read (CARD,'(999A1)')
     +         (BIGBUFF(I:I),I=N,N+INDEX(CARD,'*')-2)
          goto 33
            else
              read (CARD,'(999A1)') (BIGBUFF(I:I),I=N,N+LCARD-1)
            end if
            N=N+LCARD
          goto 32
33          continue
C
            LENBUFF=I-1
C
            L=1
            do K=1,LENBUFF
              if (BIGBUFF(K:K).eq.' ') then
                continue
              else
                NCHAR(L)=K
                L=L+1
              end if
            end do
            L=L-1
C
C For all positions in the string find the non last in each cluster
C i.e. element i+1 is greater than element(i+1)+1 (unclear)
C Mark each of these end positions as containing a value
C
            NVAL=1
            do J=1,L
              if ((NCHAR(J+1)-NCHAR(J)).gt.1) then
                LABPOS(NVAL)=NCHAR(J)
                NVAL=NVAL+1
              end if
            end do
C
C Musn't forget last position
C
            LABPOS(NVAL)=NCHAR(L)
C
C Now load up the label array with the appropriate values
C Must first blank out the 'num' string, before num was read in as a label
C
              do J=1,NVAL
                call LJUST(BIGBUFF)
                NITL=INDEX(BIGBUFF,' ')
                LABEL(LABPOS(J),NLABEL)=BIGBUFF(1:NITL-1)
                do K=1,NITL
                  BIGBUFF(K:K)=' '
                end do
            end do
            NLABEL=NLABEL-1
          end if
      goto 10
        end if
      goto 10
C
100   continue
C
C Trap no alignment found in file.
C NSEQ is the number of sequences read in from the file
C NSTR is the number of sequences (with structures) read in from the file
C
      if (NSTR.lt.1.and.NSEQ.lt.1) then
        call ERROR('joy: no alignment found in',FILE,1)
      end if
C
      close (unit=1)
C
C ----------------------------------------------------------------------------
C
C NENTRY is the number of rows that need to be passed to the formatted output
C LENGTH is the length of the longest structurally known sequence, this value is
C very important as it determines the length of the processed section, hence the
C need to make all the sequences blocked. Replaced this section so the
C program is flexible in allowing deviations from a very strict format.
C
C This bit has a bug somewhere
C
      NENTRY=NENTRY-1
      LENGTH=1
C
      do I=1,NSTR
        if (LENSTR(I).gt.LENGTH) then
          LENGTH=LENSTR(I)
        end if
      end do
      do I=1,NSEQ
        if (LENSEQ(I).gt.LENGTH) then
          LENGTH=LENSEQ(I)
        end if
      end do
C
C Find the length (in residues) of each sequence (LENSTR)
C Also process out any chain break (/) characters
C New code to allow '/-----' to be treated as '      '
C
      do I=1,NSTR
        INGAP=.false.
        M=1
C
C Process punctuation
C
        do J=1,LENGTH
          if (STRSEQ(J,I).ne.'-'.and.
     -        STRSEQ(J,I).ne.'*'.and.
     -        STRSEQ(J,I).ne.'/') then
            M=M+1
          end if
C
          if (STRSEQ(J,I).eq.'/') then
            INGAP=.true.
            STRSEQ(J,I)=' '
          else if (STRSEQ(J,I) .eq. '-') then
            if (INGAP) then
              STRSEQ(J,I)=' '
            end if
          else if (STRSEQ(J,I) .ne. ' ') then
            INGAP=.false.
          end if
        end do
C
        LENSTR(I)=M-1
        if (LENSTR(I).lt.1) then
          call ERROR('joy: no sequence for',STRNAM(I),0)
        end if
        if (LENSTR(I) .ne. REPLENSTR(I)) then
c          call ERROR('joy: reported length is bad for',STRNAM(I),0)
        end if
      end do

c      write(*,*) 'LENSTR, SEQ: ', lenstr(1), lenseq(1)
c      do  i = 1, lenstr(1)
c        write(*,'(i5, i5, a1)') i, ichar(strseq(i,1)), strseq(i,1)
c      end do

C
      return
C
C Handle errors in opening file
C
900   call ERROR('joy: cannot find file',FILE,1)
901   call ERROR('joy: error in alignment file -- bad format',
     +           FILE,3)
904   call ERROR('joy: error in alignment file -- missing terminator ?',
     +           FILE,3)
c910   call ERROR('joy: error parsing field',FIELDS(5),0)
c      goto 12
C
      end

