      subroutine RDHBD(SEQFILE,FILE,OHBOND,NHBOND,SHBOND,SHETHB,
     +                 NHBSIDE,NHBMAIN,SEQ1,LEN,CHECK,HETTOSIDE,
     +                 SSBOND,MMNBOND,MMOBOND,RESBEG,RESEND,IRESBEG,
     +                 IRESEND,LBIN,FORCE,ACCMCH,RANGE,ALNCHN,PDBCOD)
C
C copyright (c) 1988-1991 John Overington
C part of joy
C
C
C Subroutine to read a hydrogen bond file produced by the program HBOND and
C associate with each residue a logical property attribute
C N.B. be careful with data concerning interchain H-bonds, It is left to
C the user to interpret these. Also note the limitations in the H-bond program.
C Further addition to routine counts the number of times that a sidechain is
C involved in a hydrogen bond, this may (or may not) be an important factor
C in residue conservation.
C
C The program will ignore several things in the hydrogen bonding file that
C probably do not exist in reality as hydrogen bonds. Firstly no hydrogen atom 
C can bond to any other hydrogen atom, (Histidine can sometimes do this but
C it is very rare). Secondly glutamate and aspartate are assumed to be charged
C and thus cannot form hydrogen bonds to mainchain carbonyls. N.B. if buried
C a carboxyl group may be protonated and thus form hydrogen bonds to oxygens.
C In the latest version, the possibility of forming such H-bonds is included,
C to exclude them see commented out lines. Changed this so the treatment of 
C Glu and Asp is controlled by the logical variable EXCACID, if EXCACID is true
C then H-bonds are not considered between the respective oxygen atoms
C
C Modified hbond, now will treat CYS and MET as donors/acceptors where
C appropriate. Added code here to stop disulphide bonded cystines acting as
C either an acceptor or donor. Due to a small incosistency in the code for 
C hbond.
C 
C For hydrogen bonds to mainchain groups split into 2 categories to NH and to O
C Added warnings for no found hydrogen bonds
C Added new types of H-bond to het-atoms.
C
C How to treat h-bonds to het groups, included HETTOSIDE to convert count to
C the corresponding one for side-sidechain interactions
C
C Modified by Andrej, introduced two new subroutines
C
C Changed format of .hbd file in v0.6 of hbond, now includes angles
C where known and more distances. Also changed MSN to SMN and MSO to SMO.
C
C Bug wherin a hetatm atom is considered to be a residue, gives errors
C in chkseq, fixed.
C
C Changed hbond, altered rdhbd too take account of this, (new id string for
C SMN and SMO)
C
C ----------------------------------------------------------------------------
C
      include 'joy.h'
C
      character*255 DIRPATH, HBDFILE, ZHBDFILE
      character*40 PDBHBD
      character*3 HBDEXT
      character*(*) PDBCOD
      character*(*) ALNCHN(2), RANGE(2)
      logical ACCMCH, COMPRESSED

      character*(MAXFILELEN) ATMFILE
      character*(*) FILE, SEQFILE
      character*80 TEXT
      character*60 STRING
      character*5 RESBEG, RESEND
      character*3 AT1, AT2, TYPE, ATMEXT
      character*1 RES1, RES2, SEQ1(MAXRES), SEQHBD1(MAXRES)
      integer NHBSIDE(MAXRES), NHBMAIN(MAXRES), LEN, NHB, IND1, IND2
      integer I, N, IRESBEG, IRESEND
      integer IERR, SYSTEM
      logical NHBOND(MAXRES), OHBOND(MAXRES), SHBOND(MAXRES)
      logical SSBOND(MAXRES), MMNBOND(MAXRES), MMOBOND(MAXRES)
      logical HB2O(MAXRES), HB2N(MAXRES), MH2OHB(MAXRES)
      logical SHETHB(MAXRES), MHETHB(MAXRES), SH2OHB(MAXRES)
      logical SIDE, FOUND, EXCACID, CHECK, HETTOSIDE, TRIED, FORCE
C
      data HBDEXT /'hbd'/
      data ATMEXT /'atm'/
C
C ----------------------------------------------------------------------------
C
      NHB=0
      TRIED=.false.
      FOUND=.FALSE.
      EXCACID=.FALSE.
      do I=1,MAXRES
        MMNBOND(I)=.false.
        MMOBOND(I)=.false.
        SSBOND(I)=.false.
        NHBOND(I)=.false.
        OHBOND(I)=.false.
        SHBOND(I)=.false.
        SHETHB(I)=.false.
        MHETHB(I)=.false.
        SH2OHB(I)=.false.
        MH2OHB(I)=.false.
        HB2N(I)=.false.
        HB2O(I)=.false.
        NHBMAIN(I)=0
        NHBSIDE(I)=0
        SEQHBD1(I)='X'
      end do
C
C ----------------------------------------------------------------------------
C
      if (ACCMCH) then
        call GETENV('PDBHBD',PDBHBD)
        if (PDBHBD(1:1).eq.' ')
     +  call ERROR('joy: environment variable PDBHBD not defined',' ',1)
        DIRPATH='./ '//PDBHBD
        call MKFILE(DIRPATH,PDBCOD,RANGE,ALNCHN,HBDEXT,ZHBDFILE,
     +              COMPRESSED,1)
        call prepfil(ZHBDFILE,COMPRESSED,HBDFILE)
        TRIED=.true.
      else
        HBDFILE=FILE(1:(LASTCHAR(FILE)))//'.'//HBDEXT
        TRIED=.false.
      end if

11    if ( .not. FILEEXIST(HBDFILE) .and. TRIED) then
        call ERROR('joy: recursive call to make',HBDFILE,1)
      else
        if (FORCE) then
          write (STDERR,'(''joy: forced remake of '',A)') HBDFILE
          goto 999
        end if
        OPEN (UNIT=1,FILE=HBDFILE,STATUS='OLD',FORM='FORMATTED',err=999)
        REWIND (UNIT=1)
C
C ----------------------------------------------------------------------------
C
        N=1
1       read (1,'(A)',end=3,err=4) TEXT
        if (INDEX(TEXT(2:7),'index ').lt.1) then
          goto 1
        else
2         read (1,'(A)',end=3,err=4) TEXT
          if (TEXT(1:10).ne.'          '.and..NOT.FOUND) then
            read (TEXT,'(1X,I3,9X,A1,1X,A3,1X,I3,9X,A1,1X,A3,1X,A3)',
     -            err=4) IND1,RES1,AT1,IND2,RES2,AT2,TYPE
          SIDE=.FALSE.
C
C Handle the three (?) types of hydrogen bond, there are more now.
C Removed checking of N - N H-bonds, beware !
C
C ----> Andrej
C         Enter the processing only if at least one of the residues
C         in the sequence in the alignment is H-bonded:
C         Be careful of the meaning of NHB: the sum now includes 
C         some 'half' H-bonds in addition to 'full' H-bonds where
C         both members are part of the sequence in the alignment.
C
          if (((IND1.ge.IRESBEG).and.(IND1.le.IRESEND)) .or. 
     -        ((IND2.ge.IRESBEG).and.(IND2.le.IRESEND))) then
C
            if ((IND1.GE.IRESBEG).and.(IND1.LE.IRESEND)) then
              if (TYPE(2:2).ne.'H'.and.TYPE(2:2).ne.'W') then
                SEQHBD1(IND1-IRESBEG+1)=RES1
               end if
            end if
            if ((IND2.GE.IRESBEG).and.(IND2.LE.IRESEND)) then
              if (TYPE(2:2).ne.'H'.and.TYPE(2:2).ne.'W') then
                SEQHBD1(IND2-IRESBEG+1)=RES2
              end if
            end if
C
            if (TYPE .eq. 'SMN' .or. TYPE .eq. 'SN ') then
              if (ISSIDE(AT1)) then
                  call SETTRUE(NHBOND,IND1,IRESBEG,IRESEND,MAXRES)
                call UPDATE(NHBSIDE,IND1,IRESBEG,IRESEND,MAXRES)
                call UPDATE(NHBMAIN,IND2,IRESBEG,IRESEND,MAXRES)
                NHB=NHB+1
              else
                call SETTRUE(NHBOND,IND2,IRESBEG,IRESEND,MAXRES)
                call UPDATE(NHBSIDE,IND2,IRESBEG,IRESEND,MAXRES)
                call UPDATE(NHBMAIN,IND1,IRESBEG,IRESEND,MAXRES)
                NHB=NHB+1
              end if
            else if (TYPE .eq. 'SMO' .or. TYPE .eq. 'SO ') then
              if (ISSIDE(AT1)) then
                if (EXCACID) then 
                  if ((RES1.ne.'D').and.(RES1.ne.'E')) then
                    call SETTRUE(OHBOND,IND1,IRESBEG,IRESEND,MAXRES)
                    call UPDATE(NHBSIDE,IND1,IRESBEG,IRESEND,MAXRES)
                    call UPDATE(NHBMAIN,IND2,IRESBEG,IRESEND,MAXRES)
                    NHB=NHB+1
                  end if
                else
                  call SETTRUE(OHBOND,IND1,IRESBEG,IRESEND,MAXRES)
                  call UPDATE(NHBSIDE,IND1,IRESBEG,IRESEND,MAXRES)
                  call UPDATE(NHBMAIN,IND2,IRESBEG,IRESEND,MAXRES)
                  NHB=NHB+1
                end if
              else
                if (EXCACID) then
                  if ((RES2.ne.'D').and.(RES2.ne.'E')) then
                    call SETTRUE(OHBOND,IND2,IRESBEG,IRESEND,MAXRES)
                    call UPDATE(NHBSIDE,IND2,IRESBEG,IRESEND,MAXRES)
                    call UPDATE(NHBMAIN,IND1,IRESBEG,IRESEND,MAXRES)
                    NHB=NHB+1
                  end if
                else
                  call SETTRUE(OHBOND,IND2,IRESBEG,IRESEND,MAXRES)
                  call UPDATE(NHBSIDE,IND2,IRESBEG,IRESEND,MAXRES)
                  call UPDATE(NHBMAIN,IND1,IRESBEG,IRESEND,MAXRES)
                  NHB=NHB+1
                end if
              end if
            else if (TYPE .eq. 'SS ') then
              call SETTRUE(SHBOND,IND1,IRESBEG,IRESEND,MAXRES)
              call SETTRUE(SHBOND,IND2,IRESBEG,IRESEND,MAXRES)
              call UPDATE(NHBSIDE,IND1,IRESBEG,IRESEND,MAXRES)
              call UPDATE(NHBSIDE,IND2,IRESBEG,IRESEND,MAXRES)
              NHB=NHB+1
            else if (TYPE .eq. 'MM ') then
              call SETTRUE(MMNBOND,IND1,IRESBEG,IRESEND,MAXRES)
              call SETTRUE(MMOBOND,IND2,IRESBEG,IRESEND,MAXRES)
              call UPDATE(NHBMAIN,IND1,IRESBEG,IRESEND,MAXRES)
              call UPDATE(NHBMAIN,IND2,IRESBEG,IRESEND,MAXRES)
              NHB=NHB+1
            else if (TYPE .eq. 'DS ') then
              call SETTRUE(SSBOND,IND1,IRESBEG,IRESEND,MAXRES)
              call SETTRUE(SSBOND,IND2,IRESBEG,IRESEND,MAXRES)
            else if (TYPE .eq. 'MH ') then
              call SETTRUE(MHETHB,IND1,IRESBEG,IRESEND,MAXRES)
            else if (TYPE .eq. 'MW ') then
              call SETTRUE(MH2OHB,IND1,IRESBEG,IRESEND,MAXRES)
            else if (TYPE.EQ.'SH ') then
              call SETTRUE(SHETHB,IND1,IRESBEG,IRESEND,MAXRES)
              if (HETTOSIDE) then
                call SETTRUE(SHBOND,IND1,IRESBEG,IRESEND,MAXRES)
              end if
            else if (TYPE .eq. 'SW ') then
              call SETTRUE(SH2OHB,IND1,IRESBEG,IRESEND,MAXRES)
            else
              write (STDERR,'(''joy: unknown class of H-bond '',A,
     +                        '' in '',A)') TYPE,HBDFILE
              call EXIT(1)
            end if 
          end if
          goto 2
        else
          FOUND=.TRUE.
        end if
        goto 2
      end if
3     continue
C
      close (UNIT=1)
      call unprepfil(ZHBDFILE,COMPRESSED)
      end if
C
      if (LEN.gt.MAXRES) then
        write(STDERR,'(''joy: too many residues in file '',A)')HBDFILE
        call EXIT(1)
      end if
C
C ------------------------------------------------------------------------------
C
C Reclassify all the cysteines that have been classified as cystine and h-bonded
C  Should also classify cysteine as cysteine (i.e. SSBOND(i) = false )
C
      do I=1,LEN
        if (SSBOND(I)) then
          NHBSIDE(I)=0
          SHBOND(I)=.FALSE.
          NHBOND(I)=.FALSE.
          OHBOND(I)=.FALSE.
          SH2OHB(I)=.FALSE.
          SHETHB(I)=.FALSE.
        end if
      end do
C
C ---------------------------------------------------------------------------
C
C Check sequence from HBOND file, at least what exists of it. This is about
C all the checking that can be esily done
C   
      if (CHECK) then
        call CHKSEQ(SEQ1,SEQHBD1,LEN,HBDFILE(1:INDEX(HBDFILE,'.')-1),
     +              SEQFILE,HBDFILE)
      end if
C  
C ---------------------------------------------------------------------------
C
C Check to see if any h-bonds have been found
C
      if (NHB.LT.1) then
        write (STDERR,'(''joy: warning - no hydrogen bonds in file '',
     +         A)') HBDFILE
      end if
C
      RETURN
C
C ---------------------------------------------------------------------------
C
C Handle errors in file IO, generate file if missing
C
999   continue
      TRIED=.true.
      ATMFILE=FILE(1:LASTCHAR(FILE))//'.'//ATMEXT
      if (FILEEXIST(ATMFILE)) then
        STRING=LBIN(1:LASTCHAR(LBIN))//'hbond '//ATMFILE//char(0)
        write (STDOUT,'(8X,A)') STRING
        IERR=system(STRING)
        if (IERR .ne. 0) then
          call ERROR('joy: cannot generate data',' ',1)
        end if
        goto 11
      else
        call ERROR('joy: cannot find file',ATMFILE,1)
      end if
C
4     call ERROR('joy: corrupted data file',HBDFILE,1)
901   call ERROR('joy: bad version string',' ',1)
C
      end
C
C ############################################################################
C
      subroutine SETTRUE(VECTOR,IND,IRESBEG,IRESEND,MAXRES)
C
      implicit NONE
      integer MAXRES, IND, I, IRESBEG, IRESEND
      logical VECTOR(MAXRES)
C
      if ((IND.ge.IRESBEG).and.(IND.le.IRESEND)) then
        I=IND-IRESBEG+1
        VECTOR(I)=.TRUE.
      end if
C
      return
      end
C
C ############################################################################
C
      subroutine UPDATE(VECTOR,IND,IRESBEG,IRESEND,MAXRES)
C
      implicit none
      integer MAXRES, VECTOR(MAXRES), IND, IRESBEG, IRESEND, I
C
      if ((IND.ge.IRESBEG).and.(IND.le.IRESEND)) then
        I=IND-IRESBEG+1
        VECTOR(I)=VECTOR(I)+1
      end if
C
      return
      end
