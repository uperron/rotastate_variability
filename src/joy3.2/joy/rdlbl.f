      subroutine RDLBL(FILE,LBL,NLBL)
C
C copyright (c) 1988-1991 John Overington
C part of joy
C
C Subroutine to read a file and set label strings
C
      include 'joy.h'
C
      character*(*) FILE
      character*5 CARD, LBL(MAXRES)
      integer NLBL
C
      if (VERBOSE) then
        write (STDERR,'(''rdlbl: reading label file '',A)') FILE
      end if
C
      open (unit=1,file=FILE,status='old',form='formatted',err=3)
C
      NLBL=1
1     read (1,'(A)',end=2,err=4) CARD
      if (CARD(1:1).ne.'#') then
        read (CARD,'(A)') LBL(NLBL)
        NLBL=NLBL+1
      end if
      goto 1
2     NLBL=NLBL-1
C
      close (unit=1)
C
      return
C
3     call ERROR('joy: cannot find label file',FILE,1)
4     call ERROR('joy: bad format in label file',FILE,1)
C
      end
