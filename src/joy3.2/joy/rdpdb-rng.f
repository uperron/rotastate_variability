      SUBROUTINE RDPDB(FILE,ATM,RESNUM,RESNAM,CHNNAM,COORDS,OCCUP,BVAL,
     -                 LENRES,NUMATS,NUMRES,NUMCHN,NUMHET,HETCOORDS,
     -                 HETNUM,HETNAM,HETATM,CHKATM,ACCMCH,RANGE,ALNCHN)
C
C copyright (c) 1988-1991 John Overington
C part of joy
C
C Subroutine that reads a PDB format file. ATOMS and HETATM are read into
C separate arrays. If two (or more) conformers of the same sidechain exist
C then the A form is taken in preference.
C
C Added parameter CHKATM, controls if checking is done of number and type of
C atoms in a sidechain
C
      include 'joy.h'
C
      CHARACTER*(*) FILE, ALNCHN(2), RANGE(2)
      CHARACTER*66  CARD((MAXATS*MAXRES)+MAXHET)
      CHARACTER*5   RESNUM(MAXRES),
     -              HETNUM(MAXHET)
      CHARACTER*3   RESNAM(MAXRES), HETNAM(MAXHET),
     -              ATM(MAXATS,MAXRES),
     -              HETATM(MAXHET)
      CHARACTER*1   CHNNAM(MAXRES)
      INTEGER       LENRES(MAXRES), NUMRES, NUMCHN, NUMATS, NUMHET
      integer       MAXREC,NUMREC,I,J,LM,M,K,M1,M2
      real          COORDS(3,MAXATS,MAXRES), OCCUP(MAXATS,MAXRES),
     -              BVAL(MAXATS,MAXRES), HETCOORDS(3,MAXHET),
     -              HETBVAL(MAXHET), HETOCCUP(MAXHET)
      logical       CHKATM,ACCMCH
C
C -------------------------------------------------------------------------
C
      MAXREC=(MAXATS*MAXRES)+MAXHET
      DO I=1,MAXRES
        LENRES(I)=0
      END DO
      NUMRES=0
      NUMCHN=0
      NUMATS=0
      NUMHET=0
C
C -------------------------------------------------------------------------
C
      open (unit=1,file=FILE,status='UNKNOWN',form='FORMATTED',err=999)
C
C -------------------------------------------------------------------------
C
C Read all data into CARD buffer, NUMREC items, modified to read both hetatoms
C and normal atom records into buffer. Fixed treatment of NMR data files
C (If find ENDMDL, then skip rest of file)
C
      NUMREC=1
4     READ (1,'(A)',end=3,err=902) CARD(NUMREC)
        IF (CARD(NUMREC)(1:6).EQ.'ATOM  '.OR.
     +      CARD(NUMREC)(1:6).EQ.'HETATM') THEN
          NUMREC=NUMREC+1
          if (numrec.gt.maxrec) then
            write (STDERR,'(''rdpdb: too many records'')')
            call exit(1)
          end if
        else if (CARD(NUMREC)(1:6) .eq. 'ENDMDL') then
          goto 3
        end if
      GOTO 4
3     CONTINUE
      NUMREC=NUMREC-1
      CLOSE (UNIT=1)
      NUMATS=NUMREC
C
C -------------------------------------------------------------------------
C
C If -d selected, find the range in the PDB file whose sequence occurs in
C the alignment file and only process that range. Otherwise process all
C records.
C
      if (ACCMCH) then
        M1 = -1
        M2 = -1
        do  M = 1, NUMREC
          if((CARD(M)(23:27).eq.RANGE(1)).and.
     +       (CARD(M)(22:22).eq.ALNCHN(1))) M1 = M
          if((CARD(M)(23:27).eq.RANGE(2)).and.
     +       (CARD(M)(22:22).eq.ALNCHN(2))) M2 = M
        end do
        if (M1.eq.-1.or.M2.eq.-2) then
          write(STDERR,'(5(a,1x)')
     +    'rdpdb: specified residue range not found: ',
     +    RANGE(1),ALNCHN(1),RANGE(2),ALNCHN(2)
          call EXIT(1)
        end if
      else
        M1 = 1
        M2 = NUMREC
      end if
C
C -------------------------------------------------------------------------
C
      J=1
      I=1
      LM=1
C
C Process all relevant NUMREC records
C
      DO M=M1,M2
        IF (CARD(M)(1:6).EQ.'ATOM  ') THEN
          IF (M .gt. 1) THEN
            IF (CARD(M)(23:27).NE.CARD(M-1)(23:27)) THEN
              LENRES(I)=J-1
              if (lenres(i).gt.maxats) then
                write (STDERR,'(''rdpdb: too many atoms - '',
     -                     ''increase MAXATS'')')
                call exit(1)
              end if
              I=I+1
              J=1
              NUMRES=NUMRES+1
            END IF
          END IF
          READ (CARD(M),'(13X,A3,1X,A3,1X,A1,A5,3X,3F8.3,2F6.2)',
     +          err=903)
     -          ATM(J,I),RESNAM(I),CHNNAM(I),RESNUM(I),
     -          (COORDS(K,J,I),K=1,3),OCCUP(J,I),BVAL(J,I)
          J=J+1
          if (J.gt.MAXRES) then
            write (STDERR,'(''rdpdb: too many residues '',
     +                 ''-- increase MAXRES'')')
            call EXIT(1)
          end if
          NUMATS=NUMATS+1
C
          if (M .gt. 1) then
            if (CARD(M)(22:22).ne.CARD(M-1)(22:22)) then
              NUMCHN=NUMCHN+1
            end if
          end if
C
C -------------------------- HETATMS -----------------------------------
C
        ELSE 
          READ (CARD(M),'(13X,A3,1X,A3,2X,A5,3X,3F8.3,2F6.2)',
     +          err=903)
     -          HETATM(LM),HETNAM(LM),HETNUM(LM),
     +          (HETCOORDS(K,LM),K=1,3),
     -          HETOCCUP(LM),HETBVAL(LM)
          IF (HETATM(LM).EQ.'E  ') THEN
            HETATM(LM)='FE '
          END IF
          LM=LM+1
          if (LM.gt.MAXHET) then
            write (STDERR,'(''rdpdb: too many het atoms '',
     +                 ''-- increase MAXHET'')')
            call EXIT(1)
          end if
          NUMHET=NUMHET+1
        END IF
      END DO
C
C Don't forget the last residue length
C
      LENRES(I)=J-1
      NUMRES=NUMRES+1
      NUMCHN=NUMCHN+1
C
C -----------------------------------------------------------------------
C
C Check residue atom names and lengths
C
      if (CHKATM) then
        do I=1,NUMRES
          call CHKRES(RESNAM(I),LENRES(I),ATM(1,I))
        end do
      end if
C 
C -----------------------------------------------------------------------
C
C Catch array bounds exceeded
C
      if (lenres(i).gt.maxats) then
        write (STDERR,'(''rdpdb: too many atoms'')')
        call exit(1)
      end if
      if (numres.gt.maxres) then
        write (STDERR,'(''rdpdb: too many residues'')')
        call exit(1)
      end if
      if (numhet.gt.maxhet) then
        write (STDERR,'(''rdpdb: too many hetatoms'')')
        call exit(1)
      end if
C
C -------------------------------------------------------------------------
C
      return
C
C -------------------------------------------------------------------------
C
999   write (STDERR,'(''rdpdb: cannot process file '',A)') file
      call exit(1)
902   write (STDERR,'(''rdpdb: bad format in file'')')
      call EXIT(1)
903   write (STDERR,'(''rdpdb: bad read of internal file'')')
      call EXIT(1)
C
      END
C
C ===========================================================================
C
      subroutine CHKRES(RESNAM,LENRES,ATM)
C
C subroutine that does nothing yet
C
      include 'joy.h'
C
      character*3 RESNAM
      character*3 ATM(MAXATS)
      integer     LENRES
C
c      if (.not.CHKLEN(RESNAM,LENRES)) then
c        write (STDERR,'(''chkres: bad length for '')')
c      end if
C
      return
      end
