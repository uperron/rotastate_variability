cf ----------------------------------------------------------------------------
cf
cf    RDPDB() reads a compressed/uncompressed Brookhaven Protein Databank file.
cf
cf    Input:
cf      IOINP     ... I/O input stream ;
cf      BRKNAME   ... file name of a compressed or uncompressed PDB file;
cf      MAXATM    ... maximal number of atoms ;
cf      MAXRES    ... maximal number of residues ;
cf      WATER     ... true, if water to be read in
cf      HETATM    ... true, if HETATMS to be read in
cf      HYDROGEN  ... true, if H atoms to be read in
cf      RNG(2)    ... residue numbers of the first and last residue of a 
cf                    segment to read in ('@@@@@' for all)
cf      CHNRNG(2) ... chain id's of the first and last residue to read in
cf                    ('@' for all)
cf
cf    Output:
cf      NATM      ... number of atoms ;
cf      NRES      ... number of residues ;
cf      RESNAM()  ... vector of residue names (A3);
cf      RESNUM()  ... vector of residue numbers (A5 - 5. ch. is insertion code)
cf      ATMNAM()  ... vector of atom names (A4)
cf      IATMR1(I) ... index of the 1. atom in the I-th resiude ;
cf      IRESATM(I)... residue index of each atom;
cf      CHAIN(I)  ... chain id of residue I
cf      HEADER    ... PDB file header
cf      BISO(i)   ... isotropic temperature factor for each atom
cf      X,Y,Z     ... coordinate vectors ;
cf
cf    Read all atoms no matter what residue they are comming from, except for:
cf    1. B positions for double occupancy atoms.
cf    2. Atoms from water residues when the WATER flag is set to false 
cf       (irrespective of whether water occurs in an ATOM or HETATM record).
cf    3. HETATM atoms when the HETATM flag is set to false,
cf       except when they are waters and WATER is true.
cf    4. No hydrogens, when hydrogen=.false.
cf
cf
cf ............................................................................
cf
cf       rdpdbas() definitions for the calling routine:
cf
cf       integer maxres, ioinp, maxatm
cf       parameter (maxres=5000,maxatm=15*maxres,ioinp=11)
cf
cf       integer ierr,iresatm(maxatm)
cf       integer natm,nres,iatmr1(maxres)
cf       real x(maxatm),y(maxatm),z(maxatm),biso(maxatm)
cf       character rng(2)*(5),chnrng(2)*(1),header*(80)
cf       character chain(maxres)*(1),atmnam(maxatm)*(4)
cf       character resnam(maxres)*(3),resnum(maxres)*(5)
cf       character brkname*(255)
cf       logical hetatm,water,hydrogen,ishydrogen
cf
cf       data hetatm,water,hydrogen /.false.,.false.,.false./
cf       data rng,chnrng /'@@@@@','@@@@@','@','@'/
cf
cf ............................................................................
cf
cf    subroutine rdpdbas(ioinp,brkname,water,hetatm,hydrogen,x,y,z,maxatm,
cf   -           maxres,natm,nres,resnam,resnum,atmnam,iatmr1,
cf   -           chain,header,rng,chnrng,iresatm,biso,ierr)
cf
cf ----------------------------------------------------------------------------

      subroutine rdpdbas(ioinp,brkname,water,hetatm,hydrogen,x,y,z,
     &           maxatm,maxres,natm,nres,resnam,resnum,atmnam,iatmr1,
     &           chain,header,rng,chnrng,iresatm,biso,ierr)
        implicit none
        include 'reslib.cst'
        include 'reslib.cmn'

c ----------------------------------------------------------------------------
c ----- rdpdbas() definitions for the passed variables:
        integer ioinp,maxatm,maxres
        integer ierr,iresatm(maxatm),indexw
        integer natm,nres,iatmr1(maxres)
        real x(maxatm),y(maxatm),z(maxatm),biso(maxatm)
        character rng(2)*(*),chnrng(2)*(*),header*(*)
        character chain(maxres)*(*),atmnam(maxatm)*(*)
        character resnam(maxres)*(*),resnum(maxres)*(*)
        character brkname*(*)
        logical hetatm,water,hydrogen,ishydrogen,reseqv1
c ----------------------------------------------------------------------------

c ----- local variables
        integer istr2int
        character t1*6,t2*3,t3*1,oldnum*5,newnum*5,newnam*3
        character card*80, newid*(1),t4*(4),oldid*(1)
        character atnumb*(5),oldnam*3
        logical p,compressed,inrange
        external reseqv1

        call openf3(ioinp,brkname,'OLD','SEQUENTIAL','FORMATTED',3,
     &              .false.,ierr,compressed)

        if (ierr .gt. 0) then
          ierr = 1
          return
        end if

        natm = 0
        nres = 0
        oldnum = '%%%%%'
        oldid = '%'
        inrange = .false.

5       continue
          read(ioinp, '(a80)', end=20, err=300)card
          t1 = card(1:6)
          t2 = card(18:20)
          t3 = card(17:17)
          t4 = card(13:16)

          if (t1 .eq. 'HEADER') header=card

c ------- special pre-processing (do something about it if it gets too
c         clumsy)
          if (t2.eq.'MSE') then
            t1 = 'ATOM  '
            t2 = 'MET'
          end if
          if((t2.eq.'MEX').or.(t2.eq.'ABU'))then
            t1 = 'ATOM  '
            t2 = 'CYS'
          end if

c ------- ATOM test:
          if((t1.eq.'ATOM  ')) then
            p = (istr2int(t2) .ne. iwattyp) .or. water
          else 
            if (t1.eq.'HETATM') then
              if (istr2int(t2) .eq. iwattyp) then
                p = water 
              else
                p = hetatm
              end if
            else
              p = .false.
            end if
          end if

c ------- do not read any H atoms if so requested:
          if ((.not. hydrogen).and.(ishydrogen(t4))) p = .false.

c ------- occupancy test (note: only positions A will be read in; some 
c         files do not use A but some other combination of chars, so 
c         no atom for double occupancy positions will be read in):
          if((t3 .ne. ' ') .and. (t3 .ne. 'A')) p = .false.

c          write(*,*) nres,p,card(1:50)

          if (p) then
            natm = natm + 1
            if (natm .gt. maxatm) then
              ierr = 2
              write(*,'(a)') 'rdpdbas___E> too many atoms'
              go to 500
            end if
            read(card, 15) atnumb, atmnam(natm), newnam, newid,
     &           newnum, x(natm), y(natm), z(natm), biso(natm)
15          format(6x,a5,1x,a4,1x,a3,1x,a1,a5,3x,3f8.3,6x,f6.2)
c            call ljust(atmnam(natm))
            newnam = t2

c            if (((indexw(newnum,rng(1)).gt.0).or.
c     &            (rng(1).eq.'@@@@@')).and.
c     &          ((newid.eq.chnrng(1)).or.(chnrng(1).eq.'@')))
c     &        inrange = .true.
            if(reseqv1(newnum,newid,rng(1),chnrng(1)))inrange=.true.

            if (inrange) then
              if ((newnum.ne.oldnum) .or. (newnam.ne.oldnam)) then
                if (reseqv1(oldnum,oldid,rng(2),chnrng(2))) then
c                if(((indexw(oldnum,rng(2)).gt.0).and.
c     &              (rng(2).ne.'@@@@@')).and.
c     &             ((oldid.eq.chnrng(2)).and.(chnrng(2).ne.'@'))) then
                  natm = natm - 1
                  go to 20
                end if
                nres = nres + 1
                if (nres .gt. maxres) then
                  ierr = 3
                  write(*,'(a)') 'rdpdbas___E> too many residues'
                  go to 500
                end if
                chain(nres) = newid
                resnam(nres) = newnam
                resnum(nres) = newnum
                oldnum = newnum
                oldnam = newnam
                oldid  = newid
                iatmr1(nres) = natm
              end if
              iresatm(natm) = nres
            else
              natm = natm - 1
            end if


          end if
          go to 5
20      continue
        close(ioinp)

        if (natm .gt. 0) then
          ierr = 0
        else
          ierr = 4
          write(*,'(a)') 'rdpdbas___E> no atoms'
        end if
        go to 500

300     ierr = 5

500     continue
        call unprepfil(brkname, compressed)
        return

      end



cf ----------------------------------------------------------------------------
cf
cf    ISHYDROGEN function returns .T. if the atmnam is the PDB hydrogen
cf    atom code (if the first non-blank character is 'H').
cf
cf    logical function ishydrogen(atmnam)
cf
cf ----------------------------------------------------------------------------

      logical function ishydrogen(atmnam)
        implicit none
        character atmnam*(*), atdum*(5)

        atdum = atmnam 
        call ljust(atdum)
        if (atdum(1:1) .eq. 'H') then
          ishydrogen = .true.
        else
          if (index('1234567890',atmnam(1:1)).gt.0.and.
     -        atdum(2:2).eq.'H') then
            ishydrogen = .true.
          else
            ishydrogen = .false.
          end if
        end if

        return
      end



      logical function reseqv1(resnum1,chnid1,resnum2,chnid2)
        implicit none
        include 'reslib.cst'
        integer indexw
        character resnum1*(*), chnid1*(*)
        character resnum2*(*), chnid2*(*)
        logical res, chn

        res = (indexw(resnum1,resnum2).gt.0) .or.
     &          (index(resnum1,anyrid).gt.0 .or.
     &           index(resnum2,anyrid).gt.0)
        chn = chnid1.eq.chnid2 .or.
     &          (chnid1.eq.anyrid .or. chnid2.eq.anyrid)
        reseqv1 = res .and. chn

        return
      end

