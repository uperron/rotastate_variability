      subroutine RDPSS(SEQFIlE,FILE,PSACUTOFF,ACCESS,SEQ1,LEN,CHECK,
     -                 RESBEG,RESEND,IRESBEG,IRESEND,RESLBL,LBIN,
     -                 RELACC,TOTACC,FORCE,ACCMCH,RANGE,ALNCHN,PDBCOD)
C
C copyright (c) 1988-1991 John Overington
C part of joy
C
C To  read a PSA file and determine whether a residue is solvent accessible
C or not, value of cutoff is passed to routine
C
C Changed keyword in psa to ACCESS, to allow merging with PDB file
C
C Modified psa to produce ! after residue type to denote that atoms
C were missing in the atom file, joy will then set these positions to
C accessible, regardless of the relative accessibility.
C
C PDBPSA is a new env variable to control reading/writing of psa data
C
C simple hack of rdpsa to read in site output files.
C
      include 'joy.h'
C
      character*(255) DIRPATH, PSAFILE, ZPSAFILE
      character*40 PDBPSA
      character*3 PSAEXT
      character*(*) PDBCOD
      character*(*) ALNCHN(2), RANGE(2)
      logical ACCMCH, COMPRESSED

      character*(MAXFILELEN) ATMFILE
      character*(*) SEQFILE, FILE
      character*80 TEXT
      character*60 STRING
      character*12 PSAFLAGS
      character*5 RESBEG, RESEND, RESNUM, RESLBL(MAXRES)
      character*3 SEQPSA3(MAXRES), ATMEXT
      character*1 SEQPSA1(MAXRES), SEQ1(MAXRES), FLAG
      integer LENSEQ, LEN, NINAC, NRES, I, IRESEND, IRESBEG
      integer IERR, system
      real PSACUTOFF, TOTABS, TOTREL, NPSABS, NPSREL, PLSABS, PLSREL
      real SIDABS, SIDREL, MNCABS, MNCREL, RELACC(MAXRES), TOTACC
      logical ACCESS(MAXRES), CHECK, PREBEG, TRIED, FORCE
C
C PSAFLAGS controls the behavour of psa
C
      data PSAEXT /'pss'/
      data ATMEXT /'atm'/
C
C TRIED is used as a flag to signal that an attempt has been made to remake
C the data
C
      TRIED=.false.
      PSAFILE=FILE(1:(LASTCHAR(FILE)))//'.'//PSAEXT

C
C TOTACC is the total accessible surface area for the protein
C
      TOTACC=0.0
      NRES=0
      NINAC=0
      PREBEG=.TRUE.
      IRESBEG=99999
      IRESEND=-99999
      do I=1,MAXRES
        RELACC(I)=0.0
      end do
      I=0
C
1     if ( .not. FILEEXIST(PSAFILE) .and. TRIED) then
        call ERROR('joy: recursive call of make',FILE(1:lastchar(FILE)),1)
      else
        if (FORCE) then
          write (STDERR,'(''joy: forced make of '',A)') PSAFILE
          goto 999
        end if
        open (unit=1,file=PSAFILE,status='OLD',form='FORMATTED',err=999)
C
C Start reading at RESBEG and end reading at RESEND, inclusive
C
10      read (1,'(A)',end=100,err=101) TEXT
        if (TEXT(1:6) .ne. 'ATOM  ' .and.
     +      TEXT(1:6) .ne. 'ACCESS') goto 10
        I=I+1
        RESNUM=TEXT(8:12)
        if (PREBEG) then
          if ((RESNUM.ne.RESBEG).and.(RESBEG.ne.'@@@@@')) goto 10
        end if
        IRESBEG=MIN(IRESBEG,I)
        IRESEND=MAX(IRESEND,I)
        PREBEG=.FALSE.
        NRES=NRES+1
C
        read (TEXT,'(7X,A5,2X,A3,1X,5(1X,F6.2,F5.1))',err=102)
        read (TEXT,'(7X,A5,2X,A3,1X,A1,F6.2,F5.1,4(1X,F6.2,F5.1))',
     +        err=102)
     -        RESLBL(NRES),SEQPSA3(NRES),FLAG,
     -        TOTABS,TOTREL,NPSABS,NPSREL,PLSABS,PLSREL,
     -        SIDABS,SIDREL,MNCABS,MNCREL
        TOTACC=TOTACC+TOTABS
C
C set the real accessibility value
C
        RELACC(NRES)=SIDREL
C
C fix for sidechains with missing atoms
C Assign logical on basis of relative sidechain accessibility (SIDREL)
C
C Assign positions that have missing atoms as accessible
C
C n.b. inversion of original classification
C
        if (SIDREL .gt. PSACUTOFF) then
          ACCESS(NRES)=.false.
          write (6,*) 'non site  ',nres, ' ', SEQPSA3(NRES)
        else
          ACCESS(NRES)=.true.
          NINAC=NINAC+1
          write (6,*) '    site  ',nres, ' ', SEQPSA3(NRES)
        end if
C
        if ((RESNUM.ne.RESEND).or.(RESEND.eq.'@@@@@')) goto 10
C
100     continue
        close (unit=1)
C Possibly compress the file back to the original state:
        call unprepfil(ZPSAFILE,COMPRESSED)
C
C Catch funny data
C
        if (NRES.gt.MAXRES) then
          write(STDERR,'(''joy: too many residues in file '',A)')PSAFILE
          call EXIT(1)
        end if
        if (CHECK) then
          call CHKLEN(NRES,LEN,PSAFILE(1:INDEX(PSAFILE,'.')-1),
     +                SEQFILE,PSAFILE)
          call THRONE(SEQPSA3,SEQPSA1,NRES)
          call CHKSEQ(SEQ1,SEQPSA1,LEN,PSAFILE(1:INDEX(PSAFILE,'.')-1),
     +                SEQFILE,PSAFILE)
        end if
      end if
C
C output a few stats on site residues
C
      write (6,*) ' number of site residues is ',NINAC
      do I=1,NRES
        if (ACCESS(I)) then 
          write (6,'(I4,2X,A)') I,SEQPSA3(I)
        end if
      end do
C
      return
C
999   continue
C
C Attempt to create necessary file
C
      TRIED=.true.
      ATMFILE=FILE(1:(LASTCHAR(FILE)))//'.'//ATMEXT
      if (FILEEXIST(ATMFILE)) then
        STRING=LBIN(1:LASTCHAR(LBIN))//'site -D '//
     +         ATMFILE(1:LASTCHAR(ATMFILE))//
     +         char(0)
        write (STDOUT,'(8X,A)') STRING
        IERR=system(STRING)
        if (IERR .ne. 0) then
          call ERROR('joy: cannot generate data',' ',1)
        end if
        goto 1
      else
        call ERROR('joy: cannot find file',
     +           ATMFILE(1:lastchar(ATMFILE)),1)
      end if
C
C Catch IO errors connected with FILE
C
101   call ERROR('joy: error reading file',
     +           PSAFILE(1:lastchar(PSAFILE)),3)
102   call ERROR('joy: corrupted data file',
     +           PSAFILE(1:lastchar(PSAFILE)),1)
C
      end
