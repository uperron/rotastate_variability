      subroutine RDSEG(FILE,NSEQ,FILNAM,RESBEG,RESEND,FILBIG)
C
C copyright (c) 1988-1991 John Overington
C part of joy
C
      include 'joy.h'
C
      character*(*) FILE
      character*10 FILNAM(MAXSEQ), CODE1, CODE2, FILBIG(MAXSEQ)
      character*5 RESBEG(MAXSEQ), RESEND(MAXSEQ)
      integer I, NSEQ
C
      if (VERBOSE) then
        write (STDERR,'(''rdseg: reading segment file '',A)') FILE
      end if
      if (FILEEXIST(FILE)) then
        open (unit=1,file=FILE,form='FORMATTED',status='OLD',err=1)
        do I=1,NSEQ
          read (1,'(A10,1X,A10,1X,A5,1X,A5)',err=2) CODE1,CODE2,
     +          RESBEG(I),RESEND(I)
          if (CODE1.ne.FILNAM(I)) then
            WRITE(STDERR,'(''rdseg: sequence names differ in '',
     +            ''alignment and segment file '',A10,1X,A10)')
     +            FILNAM(I),CODE1
            call EXIT(1)
          end if
          FILBIG(I)=CODE2
        end do
        close (unit=1)
      else
        write (STDERR,'(''rdseg: cannot find segment file '',A)') FILE
        call EXIT(1)
      end if
C
      return
C
1     write (STDERR,'(''rdseg: error opening file '',A)') FILE
      call EXIT(1)
2     write (STDERR,'(''rdseg: corrupted or bad segment file '',A)')
     +       FILE
      call EXIT(1)
C
      end
