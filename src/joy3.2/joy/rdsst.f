      subroutine RDSST(SEQFILE,FILE,SHEET,POSPHI,ALPHA,THRTEN,PIHEL,
     +                 BETA,CIS,SEQ1,LEN,CHECK,RESBEG,RESEND,
     +                 LBIN,OOI,PHI,PSI,FORCE,
     +                 ACCMCH,RANGE,ALNCHN,PDBCOD)
C
C copyright (c) 1988-1991 John Overington
C part of joy
C
C Subroutine to  read .SST files produced by SSTRUC, flags those residues with a
C positive phi angle, assigns secondary structural state of a residue and
C checks the sequence of the SSTRUC data to make sure that it agrees with the
C sequence from the alignment file.
C Newer version assigns only four possible mainchain states, helix, sheet,
C coil and positive phi. Positive phi has precedance where required. Included
C G and I into helix class.
C Added code to  read psi and omega angles, logical CIS is assigned TRUE if 
C omega is greater than -90. and less than +90.
C Added DSSP variable to control reading exact definition
C Chenged control of POSPHI, no longer overwrites ALPHA and BETA arrays
C Added STRICTALPHA, to control treatment of 3-10 and pi helices
C Added reading of hbonding energies and partners
C Added reading of 14 A Ooi numbers
C Changed the format of the SST file: the leading blank is now missing.
C
C ----------------------------------------------------------------------------
C
      include 'joy.h' 
C
      character*255 DIRPATH, SSTFILE, ZSSTFILE
      character*40 PDBSST
      character*3 SSTEXT
      character*(*) PDBCOD
      character*(*) ALNCHN(2), RANGE(2)
      logical ACCMCH, COMPRESSED

      character*(MAXFILELEN) ATMFILE
      character*(*) FILE, SEQFILE
      character*132 TEXT
      character*60 STRING
      character*10 BLANK
      character*3 ATMEXT
      character*5 RESBEG, RESEND, RESNUM
      character*1 SECSTR, SEQ1(MAXRES), SEQALL1(MAXRES), SHEET(MAXRES)
      real HBE(MAXRES,4), PHI(MAXRES), PSI(MAXRES), OMEGA
      integer LEN, NPHI, NCIS, NRESTOT, NRES, I, J, K, IRES
      integer IERR, SYSTEM
      integer IBP(MAXRES,2), HBP(MAXRES,4), OOI(MAXRES)
      logical POSPHI(MAXRES), ALPHA(MAXRES), BETA(MAXRES), CIS(MAXRES)
      logical CHECK, PREBEG, FEXIST, TRIED, THRTEN(MAXRES)
      logical DSSP, STRICTALPHA, PIHEL(MAXRES), FORCE
C
      data SSTEXT /'sst'/
      data ATMEXT /'atm'/
C
C ----------------------------------------------------------------------------
C
C Define DSSP as true if you want to use strict DSSP definitions for the
C secondary structure, .not.DSSP means that you will read a different
C definition. (see SSTRUC documentation for more details)
C
C Added checking of sequence so that PCA etc records are correctly treated
C
      DSSP=.true.
      STRICTALPHA=.false.
      TRIED=.false.
      NPHI=0
      do I=1,MAXRES
        POSPHI(I)=.false.
        THRTEN(I)=.false.
        PIHEL(I)=.false.
        ALPHA(I)=.false.
        BETA(I)=.false.
        CIS(I)=.false.
        OOI(I)=0
      end do
      do J=1,2
        do I=1,MAXRES
          IBP(I,J)=0
        end do
      end do
      do J=1,4
        do I=1,MAXRES
          HBP(I,J)=0
          HBE(I,J)=0.0
        end do
      end do
C
C ----------------------------------------------------------------------------
C
      if (ACCMCH) then
        call GETENV('PDBSST',PDBSST)
        if (PDBSST(1:1).eq.' ')
     +  call ERROR('joy: environment variable PDBSST not defined',' ',1)
        DIRPATH='./ '//PDBSST
        call MKFILE(DIRPATH,PDBCOD,RANGE,ALNCHN,SSTEXT,ZSSTFILE,
     +              COMPRESSED,1)
        call prepfil(ZSSTFILE,COMPRESSED,SSTFILE)
        TRIED=.true.
      else
        SSTFILE=FILE(1:(LASTCHAR(FILE)))//'.'//SSTEXT
        TRIED=.false.
      end if

11    if ( .not. FILEEXIST(SSTFILE) .and. TRIED) then
        call ERROR('joy: recursive call of make ',SSTFILE,1)
      else
      if (FORCE) then
        write (STDERR,'(''joy: forced remake of '',A)') SSTFILE
        goto 2
      end if
      open (unit=1,file=SSTFILE,form='FORMATTED',status='OLD',err=2)
C
C ---------------------------------------------------------------------------
C
      read (1,'(///18X,I4)',err=1,end=921) NRESTOT
      do I=1,3
        read (1,'(A)',err=4) BLANK
      end do
C
      if (NRESTOT.gt.MAXRES) then
        call ERROR('joy: too many residues in',SSTFILE,1)
      end if
C
      PREBEG=.true.
      NRES=0
      NCIS=0
C
10    read (1,'(A)',end=100,err=4) TEXT
        RESNUM = TEXT(7:11)
        IF (PREBEG) THEN
          IF ((RESNUM.ne.RESBEG).and.(RESBEG.ne.'@@@@@')) GO TO 10
        end if
        PREBEG=.FALSE.
        NRES=NRES+1
      if (NRES.gt.NRESTOT) goto 100
        if (DSSP) then
          read (TEXT,'(I4,14X,A1,3X,A1,4X,A1,7X,2(1X,I4),
     -                 3(1X,F6.1),19X,4(1X,I4,1X,F4.1),3X,I3)',err=3)
     -          IRES,SEQALL1(NRES),SECSTR,SHEET(NRES),
     +          (IBP(NRES,K),K=1,2),PHI(NRES),PSI(NRES),OMEGA,
     +          (HBP(NRES,K),HBE(NRES,K),K=1,4),OOI(NRES)
          if ((SEQALL1(NRES).eq.'-').or.(SEQALL1(NRES).eq.'X')) then
            NRES=NRES-1
            NRESTOT=NRESTOT-1
            goto 10
          end if
        else
          call ERROR('joy: funny read of file',' ',1)
c          read (TEXT,'(I4,14X,A1,5X,A1,2X,A1,7X,2(1X,I4) )')
c     -                 3(1X,F6.1),19X,4(1X,I4,1X,F4.1),3X,I3)',err=3)
c     -          IRES,SEQALL1(NRES),SECSTR,SHEET(NRES),(IBP(NRES,K),K=1,2)
c     -          PHI,PSI,OMEGA,(HBP(NRES,K),HBE(NRES,K),K=1,4),OOI(NRES)
        end if
C
C cis peptides
C
        if (OMEGA.gt.-90.0.and.OMEGA.lt.+90.0) then
          CIS(NRES+1)=.true.
          NCIS=NCIS+1
        end if
C
        if (SECSTR.eq.'E') then
          BETA(NRES)=.true.
        else if ((SECSTR .eq. 'H') .or.
     +           (SECSTR .eq. 'G') .or.
     +           (SECSTR .eq. 'I')) then
          if (SECSTR.eq.'H') then
            ALPHA(NRES)=.true.
          else if (SECSTR.eq.'G') then
            THRTEN(NRES)=.true.
          else if (SECSTR.eq.'I') then
            PIHEL(NRES)=.true.
          end if
          if (.not.STRICTALPHA) then
            ALPHA(NRES)=.true.
          end if
        end if
        if ((PHI(NRES).ge.0.0).and.(PHI(NRES).lt.180.0)) then
          POSPHI(NRES)=.true.
          NPHI=NPHI+1
        end if
      IF ((RESNUM.ne.RESEND).or.(RESEND.eq.'@@@@@')) GOTO 10
C
C ----------------------------------------------------------------------------
C
100   continue
      NRES=NRES-1
      close (unit=1)
      call unprepfil(ZSSTFILE,COMPRESSED)
C
C Check number of residues
C
      if (NRES.ne.NRESTOT) then
        write (STDERR,'(''joy: corrupted data file -'',
     +         '' bad residue count in '',A)') SSTFILE
        call EXIT(1)
      end if
C
C ----------------------------------------------------------------------------
C
C Catch funny data, similarly ignore if using segment file
C
      if (CHECK) then
        call CHKLEN(NRES,LEN,SSTFILE(1:INDEX(SSTFILE,'.')-1),
     +              SEQFILE,SSTFILE)
        call CHKSEQ(SEQ1,SEQALL1,LEN,SSTFILE(1:INDEX(SSTFILE,'.')-1),
     +              SEQFILE,SSTFILE)
      end if
      end if
C
C ----------------------------------------------------------------------------
C
C normal return
C
      return
C
C ----------------------------------------------------------------------------
C
2     continue
C
C Attempt to create missing data
C
      TRIED=.true.
      ATMFILE=FILE(1:(LASTCHAR(FILE)))//'.'//ATMEXT
      if (FILEEXIST(ATMFILE)) then
        STRING=LBIN(1:LASTCHAR(LBIN))//'sstruc s '//ATMFILE//char(0)
c        STRING=LBIN(1:LASTCHAR(LBIN))//'sstruc -s '//ATMFILE//char(0)
        write (STDOUT,'(8X,A)') STRING
        IERR=system(STRING)
        if (IERR .ne. 0) then
          call ERROR('joy: cannot create data',' ',1)
        end if
        goto 11
      else
        call ERROR('joy: cannot find file',ATMFILE,1)
      end if
C
1     call ERROR('joy: corrupted data file (bad integer) in',SSTFILE,1)
3     write (STDERR,'(''joy: corrupted data file (bad data) in '',
     +       A)') SSTFILE
      write (STDERR,'(A)') TEXT(1:79)
      call EXIT(3)
4     call ERROR('joy: error reading file',SSTFILE,3)
C
      return
C
921   call ERROR('joy: corrupt .sst file (empty ?)',' ',1)
      end
