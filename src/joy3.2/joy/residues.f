c --- This file contains the routines for conversion between 1, 3 and 4 letter,
c     and integer residue codes, in scalar and vector versions.
c --- Integer code 0, character '#', '###' and '####' are for unknown 
c     residue types.
c ------------------------------------------------------------------------
c --- SCALAR VERSIONS:
c      subroutine int2chr(ires,res1)
c      subroutine int2str(ires,res3)
c      subroutine int2crm(ires,res4)
c      subroutine chr2str(res1,res3)
c      subroutine chr2crm(res1,res4)
c      subroutine str2chr(res3,res1)
c      subroutine str2crm(res3,res4)
c      subroutine crm2chr(res4,res1)
c      subroutine crm2str(res4,res3)
c      integer function ichr2int(res1)
c      integer function istr2int(res3)
c      integer function icrm2int(res4)
c --- VECTOR VERSIONS:
c      subroutine int2strn(ires, res3, nres)
c      subroutine int2chrn(ires, res1, nres)
c      subroutine chr2strn(res1, res3, nres)
c      subroutine str2chrn(res3, res1, nres)
c      subroutine str2intn(res3, ires, nres)
c      subroutine chr2intn(res1, ires, nres)
c      subroutine str2crmn(res3, res4, nres)
c      subroutine crm2chrn(res4, res1, nres)
c -----------------------------------------------------------------------


cf ----------------------------------------------------------------------------
cf
cf    INIRES() routine initializes the residue type transformation routines.
cf    It reads in the table of residue types in different conventions from 
cf    a library file INFIL.
cf
cf    subroutine inires(ioinp, infil)
cf
cf ----------------------------------------------------------------------------

      subroutine inires(ioinp, infil)
        implicit none
        include 'reslib.cst'
        include 'reslib.cmn'
        integer ioinp,ifind2word
        character infil*(*),line*(80)
        integer ierr,i1,i2,i3,i4

        call openf2(ioinp,infil,'OLD','SEQUENTIAL','FORMATTED',3,
     &              .true.,ierr)
        nrestyp = 0
10      read(ioinp, '(a)', end=100) line
          if (line(1:1) .ne. '#') then
            i1 = index(line, '|')
            i2 = i1 + index(line(i1+1:), '|')
            i3 = i2 + index(line(i2+1:), '|')
            i4 = i3 + index(line(i3+1:), '|')
            nrestyp = nrestyp + 1
            if (nrestyp .gt. mrestyp) then
              write(*,'(a)') 'inires__E> increase MRESTAB'
              stop
            end if
            brkres(nrestyp) = line(i1+1:i2-1)
            call ljust(brkres(nrestyp))
            call str_s(line(i2+1:i3-1), modres(nrestyp))
            call str_s(line(i3+1:i4-1), chmres(nrestyp))
          end if
          go to 10
100     continue
        close(ioinp)
        write(*,'(a,i5)') 'inires___> residue types: ', nrestyp

        igaptyp = ifind2word(gapsym, brkres, nrestyp)
        if (igaptyp.lt.1) 
     &    write(*,'(a)') 'inires__W> gap residue type not found'
        iwattyp = ifind2word(watsym, brkres, nrestyp)
        if (iwattyp.lt.1) 
     &    write(*,'(a)') 'inires__W> water residue type not found'
        icystyp = ifind2word(cyssym, brkres, nrestyp)
        if (icystyp.lt.1) 
     &    write(*,'(a)') 'inires__W> Cys residue type not found'

        return
      end


cf ----------------------------------------------------------------------------
cf
cf    ISTR2INT() function returns an integer type for a three letter
cf    residue code. If a code can not be recognized, 0 is returned;
cf
cf    integer function istr2int(res)
cf
cf ----------------------------------------------------------------------------

      integer function istr2int(res)
      implicit none
      integer ifind2word
      character res*(3)
      include 'reslib.cst'
      include 'reslib.cmn'

      istr2int = ifind2word(res, brkres, nrestyp)

      return
      end


cf ----------------------------------------------------------------------------
cf
cf    INT2STR() routine returns the char*3 residue type for the integer
cf    residue code; if a code can not be recognized, '###' is returned
cf
cf    subroutine int2str(ires, res)
cf
cf ----------------------------------------------------------------------------

      subroutine int2str(ires, res)
      implicit none
      integer ires
      character res*(3)
      include 'reslib.cst'
      include 'reslib.cmn'
      if ((ires .ge. 1).and. (ires.le. nrestyp)) then
        res = brkres(ires)
      else
        res = '###'
      end if
      return
      end


cf ----------------------------------------------------------------------------
cf
cf    ICHR2INT() function returns the residue type index of a 
cf    one letter residue code RES.
cf
cf    integer function ichr2int(res)
cf
cf ----------------------------------------------------------------------------

      integer function ichr2int(res)
        implicit none
        integer ifindword
        character res*1
        include 'reslib.cst'
        include 'reslib.cmn'
        ichr2int = ifindword(res, modres, nrestyp)
        return
      end


cf ----------------------------------------------------------------------------
cf
cf    ICRM2INT() function returns the residue type index of a 
cf    four letter residue code RES.
cf
cf    integer function icrm2int(res)
cf
cf ----------------------------------------------------------------------------

      integer function icrm2int(res)
        implicit none
        integer ifindword
        character res*4
        include 'reslib.cst'
        include 'reslib.cmn'
        icrm2int = ifindword(res, chmres, nrestyp)
        return
      end


cf ----------------------------------------------------------------------------
cf
cf    INT2CHR() function returns one letter residue code given
cf    an integer index of a residue type RES.
cf
cf    subroutine int2chr(ires,res)
cf
cf ----------------------------------------------------------------------------

      subroutine int2chr(ires,res)
        implicit none
        integer ires
        character res*1
        include 'reslib.cst'
        include 'reslib.cmn'
        if ((ires.ge.1).and.(ires.le.nrestyp)) then
          res = modres(ires)
        else
          res = '#'
        end if
        return
      end


cf ----------------------------------------------------------------------------
cf
cf    INT2CRM() is an integer function that returns four letter residue code 
cf    given an integer index of a residue type RES.
cf
cf    subroutine int2crm(ires,res)
cf
cf ----------------------------------------------------------------------------

      subroutine int2crm(ires,res)
        implicit none
        integer ires
        character res*(4)
        include 'reslib.cst'
        include 'reslib.cmn'
        if ((ires.ge.1).and.(ires.le.nrestyp)) then
          res = chmres(ires)
        else
          res = '####'
        end if
        return
      end


cf ----------------------------------------------------------------------------
cf
cf    CHR2STR() routine returns three letter code, given one letter code.
cf
cf    subroutine chr2str(res1,res3)
cf
cf ----------------------------------------------------------------------------

      subroutine chr2str(res1,res3)
        implicit none
        integer ichr2int
        character res1*(1), res3*(3)
        call int2str(ichr2int(res1), res3)
        return
      end


cf ----------------------------------------------------------------------------
cf
cf    CHR2CRM() routine returns four letter code, given one letter code.
cf
cf    subroutine chr2crm(res1,res4)
cf
cf ----------------------------------------------------------------------------

      subroutine chr2crm(res1,res4)
        implicit none
        integer ichr2int
        character res1*(1), res4*(4)
        call int2crm(ichr2int(res1), res4)
        return
      end


cf ----------------------------------------------------------------------------
cf
cf    STR2CHR() routine returns one letter code, given three letter code.
cf
cf    subroutine str2chr(res3,res1)
cf
cf ----------------------------------------------------------------------------

      subroutine str2chr(res3,res1)
        implicit none
        integer istr2int
        character res1*(1), res3*(3)
        call int2chr(istr2int(res3), res1)
        return
      end


cf ----------------------------------------------------------------------------
cf
cf    CRM2CHR() routine returns one letter code, given four letter code.
cf
cf    subroutine crm2chr(res4,res1)
cf
cf ----------------------------------------------------------------------------

      subroutine crm2chr(res4,res1)
        implicit none
        integer icrm2int
        character res1*(1), res4*(4)
        call int2chr(icrm2int(res4), res1)
        return
      end


cf ----------------------------------------------------------------------------
cf
cf    STR2CRM() routine returns four letter code, given three letter code.
cf
cf    subroutine str2crm(res3,res4)
cf
cf ----------------------------------------------------------------------------

      subroutine str2crm(res3,res4)
        implicit none
        integer istr2int
        character res3*(3), res4*(4)
        call int2crm(istr2int(res3), res4)
        return
      end


cf ----------------------------------------------------------------------------
cf
cf    CHR2STR() routine returns three letter code, given four letter code.
cf
cf    subroutine crm2str(res4,res3)
cf
cf ----------------------------------------------------------------------------

      subroutine crm2str(res4,res3)
        implicit none
        integer icrm2int
        character res3*(3), res4*(4)
        call int2str(icrm2int(res4), res3)
        return
      end

c --- The following are the versions of the above routines, but for vectors
c     of residue codes

cf ----------------------------------------------------------------------------
cf
cf    INT2STRN()
cf
cf    subroutine int2strn(ires, res, nres)
cf
cf ----------------------------------------------------------------------------

      subroutine int2strn(ires, res, nres)
        implicit none
        integer i,nres,ires(nres)
        character res(nres)*3
        do  i = 1, nres
          call int2str(ires(i), res(i))
        end do
        return
      end 


cf ----------------------------------------------------------------------------
cf
cf    INT2CHRN()
cf
cf    subroutine int2chrn(ires, res, nres)
cf
cf ----------------------------------------------------------------------------

      subroutine int2chrn(ires, res, nres)
        implicit none
        integer i,nres,ires(nres)
        character res(nres)*1
        do  i = 1, nres
          call int2chr(ires(i), res(i))
        end do
        return
      end 


cf ----------------------------------------------------------------------------
cf
cf    CHR2STRN()
cf
cf    subroutine chr2strn(res1, res3, nres)
cf
cf ----------------------------------------------------------------------------

      subroutine chr2strn(res1, res3, nres)
        implicit none
        integer i, nres
        character res1(nres)*1, res3(nres)*3
        do  i = 1, nres
          call chr2str(res1(i), res3(i))
        end do
        return
      end 


cf ----------------------------------------------------------------------------
cf
cf    STR2CHRN()
cf
cf    subroutine str2chrn(res3, res1, nres)
cf
cf ----------------------------------------------------------------------------

      subroutine str2chrn(res3, res1, nres)
        implicit none
        integer i, nres
        character res1(nres)*1, res3(nres)*3
        do  i = 1, nres
          call str2chr(res3(i), res1(i))
        end do
        return
      end 


cf ----------------------------------------------------------------------------
cf
cf    CRM2CHRN()
cf
cf    subroutine crm2chrn(res4, res1, nres)
cf
cf ----------------------------------------------------------------------------

      subroutine crm2chrn(res4, res1, nres)
        implicit none
        integer i, nres
        character res4(nres)*4, res1(nres)*1
        do  i = 1, nres
          call crm2chr(res4(i), res1(i))
        end do
        return
      end 



cf ----------------------------------------------------------------------------
cf
cf    STR2CRMN()
cf
cf    subroutine str2crmn(res3, res4, nres)
cf
cf ----------------------------------------------------------------------------

      subroutine str2crmn(res3, res4, nres)
        implicit none
        integer i, nres
        character res4(nres)*4, res3(nres)*3
        do  i = 1, nres
          call str2crm(res3(i), res4(i))
        end do
        return
      end 


cf ----------------------------------------------------------------------------
cf
cf    CRM2STRN()
cf
cf    subroutine crm2strn(res4, res3, nres)
cf
cf ----------------------------------------------------------------------------

      subroutine crm2strn(res4, res3, nres)
        implicit none
        integer i, nres
        character res4(nres)*4, res3(nres)*3
        do  i = 1, nres
          call crm2str(res4(i), res3(i))
        end do
        return
      end 


cf ----------------------------------------------------------------------------
cf
cf    STR2INTN()
cf
cf    subroutine str2intn(res, ires, nres)
cf
cf ----------------------------------------------------------------------------

      subroutine str2intn(res, ires, nres)
        implicit none
        integer i,nres,ires(nres),istr2int
        character res(nres)*3
        do  i = 1, nres
          ires(i) = istr2int(res(i))
        end do
        return
      end 


cf ----------------------------------------------------------------------------
cf
cf    CHR2INTN()
cf
cf    subroutine chr2intn(ires, res, nres)
cf
cf ----------------------------------------------------------------------------

      subroutine chr2intn(res, ires, nres)
        implicit none
        integer i,nres,ires(nres),ichr2int
        character res(nres)*1
        do  i = 1, nres
          ires(i) = ichr2int(res(i))
        end do
        return
      end 
