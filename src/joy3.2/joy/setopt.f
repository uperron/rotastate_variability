      subroutine SETOPT(OPTSTR,VERSION,LIBVERSION,DATE,NOPT,DEFACC,
     +                  ACCVAL,SITVAL,FEATURES,ANALYZE,TEMPLATE,DOCHECK,
     +                  AUTONUM,AUTOSEC,PRINT,USENEWSEQ,
     +                  ALLLABEL,DOSWAP,NROPT,ROPT,DOINVAR,
     +                  DOCAP,DOSUBST,VERSOUT,ALLSEC,DOKEY,DOCHKSTR,
     +                  DOPDBCHK,AUTOACC,DOEFI,LANDSCAPE,HELVETICA,
     +                  FORCE,ONEPERPAGE,ACCMCH,KITSCHORDER,USESITE,DOKABWU)
C
C copyright (c) 1988-1991 John Overington
C part of joy
C
C Simple subroutine to read a list of options and set the logical associated
C with them
C
C Assume that flags that expect a parameter (real) have it in the
C corresponding order in the ROPT stack.
C
C Removed -i option, difficult to keep maintained
C
C options are:
C
C 1 - produce one alignment block per page
C o - suppress output of bullets 
C c - output capping data
C d - use smart file access mechanism
C f - use structural data
C g - read segment file
C h - help
C l - typeset alignment
C n - do not check data
C s - ignore structural data
C v - version number
C A - alignment and structure labels
C B - DSSP assignments for all structures
C C - output conserved residues
C D - check environments of residues
C E - write Efimov assignments
C F - force rebuild of all data
C H - Helvetica format
C K - no alignment key
C L - landscape format
C O - Kitsch ordering of alignment
C P - portrait format
C R - reverse order of labels
C S - set accessibility cutoff
C U - calculate substitution data
C V - verbose
C X - add sequences to substitution data
C Zn - extra options
C
C new option G for site data
C
C Set VERSOUT if version number called
C
C ===========================================================================
C
      implicit NONE
      integer STDERR, STDOUT
      parameter (STDERR=0, STDOUT=6)
      character*(*) VERSION, OPTSTR, DATE, LIBVERSION
      integer NOPT, NROPT, NCNT, I
      real DEFACC, ACCVAL, ROPT(10), SITVAL
      logical FEATURES, ANALYZE, TEMPLATE, DOCHECK, AUTONUM, AUTOSEC
      logical USENEWSEQ, ALLLABEL, DOSWAP
      logical VERBOSE, VERSOUT, ALLSEC, DOCAP, DOSUBST, DOKEY
      logical DOPDBCHK, AUTOACC, DOEFI, DOCHKSTR
      logical DOINVAR, PRINT, LANDSCAPE, HELVETICA, FORCE
      logical ONEPERPAGE,ACCMCH,KITSCHORDER, DOKABWU
C
      logical USESITE
C
      common /VERBOSE/ VERBOSE
C
      character*34 IDENT
      data IDENT /'@(#) setopt.f - joy - (c) jpo    '/
C
      NCNT=1 
      VERSOUT=.false.
      if (INDEX(OPTSTR,'h').gt.0) then
        call HELP(.true.)
        call EXIT(0)
      end if
C
C Walk through option string
C
      do I=1,LEN(OPTSTR)
        if (OPTSTR(I:I).eq.'1') then
          ONEPERPAGE=.true.
        else if (OPTSTR(I:I).eq.'c') then
          DOCAP=.true.
        else if (OPTSTR(I:I).eq.'d') then
          ACCMCH=.true.
        else if (OPTSTR(I:I).eq.'f') then
          FEATURES=.true.
          ANALYZE=.true.
          TEMPLATE=.true.
        else if (OPTSTR(I:I).eq.'i') then
          continue
        else if (OPTSTR(I:I).eq.'l') then
          PRINT=.true.
        else if (OPTSTR(I:I).eq.'n') then
          DOCHECK=.false.
          call ERROR('joy: checking disabled',' ',0)
        else if (OPTSTR(I:I).eq.'o') then
          AUTOACC=.false. 
        else if (OPTSTR(I:I).eq.'s') then
          FEATURES=.false.
          ANALYZE=.true.
          TEMPLATE=.false.
        else if (OPTSTR(I:I).eq.'V') then
          write (STDOUT,'(''joy - version '',A,'' ('',A,
     +                    '') - copyright 1989 jpo'')') VERSION,LIBVERSION
          VERSOUT=.true.
        else if (OPTSTR(I:I).eq.'A') then
          ALLLABEL=.true.
        else if (OPTSTR(I:I).eq.'B') then
          ALLSEC=.true.
        else if (OPTSTR(I:I).eq.'C') then
          DOINVAR=.true.
        else if (OPTSTR(I:I).eq.'D') then
          DOCHKSTR=.true.
        else if (OPTSTR(I:I).eq.'E') then
          DOEFI=.true.
        else if (OPTSTR(I:I).eq.'F') then
          FORCE=.true.
        else if (OPTSTR(I:I).eq.'G') then
          USESITE=.true.
          SITVAL=ROPT(NCNT)
          NCNT=NCNT+1
          DOKABWU=.true.
        else if (OPTSTR(I:I).eq.'H') then
          HELVETICA=.true.
        else if (OPTSTR(I:I).eq.'K') then
          DOKEY=.false.
        else if (OPTSTR(I:I).eq.'L') then
          PRINT=.true.
          LANDSCAPE=.true.
        else if (OPTSTR(I:I).eq.'O') then
          KITSCHORDER=.true.
        else if (OPTSTR(I:I).eq.'P') then
          PRINT=.true.
          LANDSCAPE=.false.
        else if (OPTSTR(I:I).eq.'R') then
          DOSWAP=.true.
        else if (OPTSTR(I:I).eq.'S') then
          ACCVAL=ROPT(NCNT)
          NCNT=NCNT+1
        else if (OPTSTR(I:I).eq.'U') then
          DOSUBST=.true.
        else if (OPTSTR(I:I).eq.'v') then
          VERBOSE=.true.
        else if (OPTSTR(I:I).eq.'X') then
          USENEWSEQ=.true.
        else if (OPTSTR(I:I).eq.'Z') then
          continue
        else if (OPTSTR(I:I).eq.' ') then
          continue
        else 
          call ERROR('joy: unrecognized option ',OPTSTR(I:I),2)
        end if
      end do
C
      return
      end
C
C #############################################################################
C
      subroutine HELP(OUTPUT)
C
C A simple help message for joy
C
C
      integer STDOUT
      logical OUTOPT
      parameter (STDOUT=6)
C
C     write (STDOUT,'(/)')
      write (STDOUT,'(''joy - a formatting and analysis program'',
     +                '' -- copyright 1989-2008 john overington'')')
      if (OUTOPT) then
        write (STDOUT,'(/)')
        write (STDOUT,'(''options :'')')
        write (STDOUT,'(''g - read segment file'')')
        write (STDOUT,'(''h - help '')')
        write (STDOUT,'(''l - typeset alignment'')')
        write (STDOUT,'(''n - do not check data'')')
        write (STDOUT,'(''v - verbose'')')
        write (STDOUT,'(''A - alignment and structure labels'')')
        write (STDOUT,'(''B - DSSP assignments for all structures'')')
        write (STDOUT,'(''C - output conserved residues'')')
        write (STDOUT,'(''D - check environments of residues'')')
        write (STDOUT,'(''E - write Efimov assignments'')')
        write (STDOUT,'(''K - no alignment key'')')
        write (STDOUT,'(''R - reverse labels'')')
        write (STDOUT,'(''S - set accessibility cutoff'')')
        write (STDOUT,'(''U - calculate substitution data'')')
        write (STDOUT,'(''V - version number'')')
        write (STDOUT,'(/)')
      end if
      write (STDOUT,'(''usage: joy -options align.file'')')
C     write (STDOUT,'(/)')
      write (STDOUT,'(''try "man joy" for further details'')')
C
      return
      end
