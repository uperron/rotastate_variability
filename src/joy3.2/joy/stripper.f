      program STRIPPER
C
C copyright (c) 1988-1991 John Overington
C part of joy
C
C program that cuts some of the unecessary stuff from a PDB file
C
C currently only keeps in records from ATOM, HETATM, TER and END records
C then a number of other filters are applied:
C
C * alternate positions - only A sites are kept, the A label is then removed
C * hydrogens (and deuteriums) are removed
C * water molecules are removed
C * CYH records are transformed to CYS records
C * PCA, ACE and FOR ATOM records are converted to HETATM records
C * nucleic acids (GACT) are converted to HETATM records
C * altered to keep in header records
C * attempts to keep resolution records
C * extracts first structure from an NMR coordinate file
C * fixes OE records in GLX, (from 1bus)
C
      implicit NONE
      character*80 CARD
      character*14 FILENAME
      integer IIN, IIO, IARGC, STDERR, STDOUT, STDIN, LASTCHAR
      logical FIRST, SEENMODEL
C
      data STDIN  /5/
      data STDOUT /6/
      data STDERR /6/
      data FIRST /.true./
      data SEENMODEL /.false./
C
      if (IARGC().gt.0) then
        IIN=1
        IIO=STDOUT
        call GETARG(1,FILENAME)
        open (file=FILENAME,unit=1,status='old',form='formatted',err=3)
        write (6,'(''REMARK produced by stripper - version 1.0'')')
      else
        IIN=STDIN
        IIO=STDOUT
      end if
C
1     read (IIN,'(A)',err=2,end=9) card
C
C report the source file, if real PDB then give code, else use file name
C
      if (FIRST) then
        if (CARD(73:76).eq.'    ') then
          write (6,'(''REMARK processed from '',A)') FILENAME
        else
          write (6,'(''REMARK processed from '',A)') CARD(73:76)
        end if
        FIRST=.false.
      end if
C
C Only let pass those cards that matter
C
      if ((CARD(1:6) .ne. 'ATOM  ') .and.
     -    (CARD(1:6) .ne. 'HEADER') .and.
     -    (CARD(1:6) .ne. 'HETATM') .and.
     -    (CARD(1:6) .ne. 'TER   ') .and.
     -    (CARD(1:6) .ne. 'MODEL ') .and.
     -    (CARD(1:6) .ne. 'REMARK') .and.
     -    (CARD(1:6) .ne. 'END   ')) then
        goto 1
      end if
C
C handle NMR data sets, these should be delimitted by model statements
C
      if (CARD(1:6) .eq. 'MODEL ') then
        if (SEENMODEL) then
          goto 9
        else
          SEENMODEL=.true. 
          CARD(1:36)='REMARK original file was NMR dataset'
        end if
      end if
C
C check for resolution and R factor fields
C
      if (CARD(1:6) .eq. 'REMARK') then
        if (CARD(12:16) .eq. 'RESOL' .or.
     +      CARD(14:20) .eq. 'R VALUE') then
          continue
        else
          goto 1
        end if
      end if
C
C check for hydrogens and deuteriums
C
      if (((CARD(14:14).eq.'H').or.
     -     (CARD(14:14).eq.'D').or.
     -     (CARD(14:14).eq.'Q')).and.
     -     (CARD(1:6).eq.'ATOM  ')) then
        goto 1
      end if
C
C fix funny atom names (only one case at moment)
C
      if (CARD(1:6) .eq. 'ATOM  ' .and.
     -    CARD(14:16) .eq. 'OE ' .and.
     -    CARD(18:19) .eq. 'GL') then
        CARD(16:16)='1'
      end if
C
C check for alternate atoms and fix them
C
      if (CARD(1:6) .eq. 'ATOM  ') then
        if ((CARD(17:17).ne.' ').and.
     -      (CARD(17:17).ne.'A')) then
          goto 1
        end if
        CARD(17:17) = ' '
      end if
C
C name waters properly
C
      if (CARD(1:6) .eq. 'ATOM  ' .and.
     -    (CARD(18:20).eq.'HOH') .or.
     -    (CARD(18:20).eq.'MOH') .or.
     -    (CARD(18:20).eq.'DOD') .or.
     -    (CARD(18:20).eq.'WAT')) then
        CARD(1:6)='HETATM'
      end if
C
C fix CYH records
C
      if (CARD(18:20).eq.'CYH') then
        CARD(18:20)='CYS'
      end if
C
C convert ACE, FOR, and PCA records to HETATM
C
      if ((CARD(18:20).eq.'ACE').or.
     -    (CARD(18:20).eq.'FOR').or.
     -    (CARD(18:20).eq.'PCA')) then
        CARD(1:6)='HETATM'
      end if
C
C convert nucleic acids to HETATM
C
      if ((CARD(18:20).eq.'  G').or.
     -    (CARD(18:20).eq.'  A').or.
     -    (CARD(18:20).eq.'  T').or.
     -    (CARD(18:20).eq.'  C')) then
        CARD(1:6)='HETATM'
      end if
C
      write (IIO,'(A)') CARD(1:LASTCHAR(CARD))
C
      goto 1
9     continue
C
      if (IIN .eq. 1) then
        close (unit=1)
      end if
C
      call EXIT(0)
C
2     write (STDERR,'(''stripper: error reading input file'')')
      call EXIT(1)
3     write (STDERR,'(''stripper: error opening input file'')')
      call EXIT(1)
C
      end
C
C ================================================================
C
      integer function LASTCHAR(STRING)
C
C Returns the position of the last printable character (not a blank) in the
C string STRING
C
      character*(*)  STRING
      integer J
      logical ISPRINT
C
      LASTCHAR=LEN(STRING)
      do J=LEN(STRING),1,-1
        if (ISPRINT(STRING(J:J))) then
          LASTCHAR=J
          return
        end if
        LASTCHAR=J
      end do
C
      return
      end


      logical function ISPRINT(ACHAR)
C
C Function that returns if a character variable is printable or not
C
      implicit none
      character*1 ACHAR
      integer     N
C
C N.B. This version does not count a blank as printable !!!
C
      N=ICHAR(ACHAR)
      if ((N.ge.33).and.(N.le.126)) then
        ISPRINT=.TRUE.
        return
      end if
      ISPRINT=.FALSE.
C
      return
      end
