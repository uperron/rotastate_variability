c --- the following routines are general purpose string handlers
c
c --- summary: ----------------------------------------------------
c
c
c --- joins s1 and s2 strings into string s in such a way that
c     there are no blanks inbetween; s has the same idententation
c     as s1;
c     subroutine concat(s1, s2, s)
c
c --- joins s1 and s2 strings into string s in such a way that
c     no blanks at the end are taken from s1, but all the beginning
c     blanks in s2 are still put between s1 and s2; s has the same
c     idententation as s1;
c     subroutine concat2(s1, s2, s)
c
c --- shifts text to the right of string
c     subroutine rjust(s)
c
c --- shifts text to the left of string
c     subroutine ljust(s)
c
c --- transforms all characters in string s into lower case
c     subroutine lower(s)
c
c --- transforms all characters in the string s into upper case
c     subroutine upper(s)
c
c --- transforms all characters in string s into lower case except for
c     the characters between two successive primes
c     subroutine lowerp(s)
c
c --- transforms all characters in string s into upper case except for
c     the characters between two successive primes
c     subroutine upperp(s)
c
c --- index of the last printable character different
c     from a blank with no non-printable characters before it;
c     (if no printable character, 0 is returned)
c --- integer function lenr(s)
c
c --- index of the first printable character different from blank;
c     (if no printable character, length+1 is returned)
c     printable characters: ascii>31 and ascii<128 ;
c --- integer function lenl(s)
c
c --- returns the position of the last character of target in the
c     search string (0 if not found); using index()
c     integer function indexr(search, target)
c
c --- this function returns the first occurence of target in str, but
c     only using those parts of string which are not quoted in ''
c     integer function indexs(str, target)
c
c --- returns the position of the last character of target word in the
c     search string (0 if not found); using indexw()
c     integer function indexwr(search, target)

c --- this routine returns the index of the word in str; the word
c     must be bounded by the beginning or end of the str or by a
c     blank; logical word is only the part of the physical word
c     as determined by lenl and lenr
c     integer function indexw(str,word)
c
c --- returns the index of the last contiguous printable character
c     different from blank (scan started at the first such character
c     in line) (if no printable char in the string, 0 is returned):
c     integer function indexb(line)
c
c --- returns the index of the position of the first character 
c     of any of those specified in delim; if not found, 0 is returned;
c     integer function index2(line, delim)
c
c --- converts integer i to string s, write out in the Inpre format.
c     subroutine i_str(i, s, npos)
c
c --- converts real r to string s with npre and npost places for
c     pre-decimal and post-decimal digits, respectively.
c     subroutine r_str(r, s, npre, npost)
c
c --- reads n integers from string line into integer vector ibuffr
c     subroutine str_in(line, ibuffr, n)
c
c --- converts the first number in string s to integer i
c     subroutine str_i(s, i)
c
c --- reads n reals from string line into real vector buffr:
c     subroutine str_rn(line, buffr, n)
c
c --- converts the first number in string s to real r:
c     subroutine str_r(s, r)
c
c --- reads a number of logicals from string line (if type='O',
c     logicals are on and off instead of *t* and *f*);
c     case insensitive;
c     subroutine str_ln(line, buffr, n, type)
c
c --- reads one logical variable from line (see above for t and f):
c     subroutine str_l(line, arg, type)
c
c --- reads n blocks of contigous printable non-blank characters
c     from line and returns them in a string array buffr;
c     subroutine str_sn(line, buffr, n)
c
c --- returns the first contiguous block of printable non-blank
c     characters in line to string buffr;
c     subroutine str_s(line, buffr)
c
c --- returns the directory+root of the unix file name:
c     subroutine diroot(file, rot)
c
c --- returns the root of the unix file name (directory and everything after
c     the last dot is eliminated):
c     subroutine rootfn(file, rot)
c
c --- returns the root of the unix file name (directory and everything after
c     the first after the last '/' is eliminated):
c     subroutine rootfn2(file, rot)
c
c --- returns the directory of the unix file name:
c     subroutine direct(file, dir)
c
c --- returns the number of blocks of contiguous non-blank characters
c     in line:
c     integer function nstr(line)
c
c --- pads the string strin with blanks
c     subroutine padbln(strin,strout)
c
c --- will substitute all occurences of str1 with str2, in str
c     subroutine subs2(str,str1,str2)
c
c --- will substitute all occurences of str1 with str2, in each element
c     of str(n)
c     subroutine subs2n(str,n,str1,str2)
c
c --- will substitute cont1(istart+1:istart+n) for sub
c     subroutine subs(str,istart,n,sub)
c
c --- substitute string str1 with str2 in all n1 x n2 elements of
c     str(n1,n2)*(*) 2D matrix
c     subroutine subs2nn(str,n1,n2,str1,str2)
c
c --- puts blanks into string str
c     subroutine blank(str)
c
c --- returns the positions of target in line in ipos(maxpos); npos
c     such positions are returned; beware: in 'andandand' target
c     andand occurs twice!
c     subroutine getpos(line, target, ipos, maxpos, npos)
c
c --- reads words from line, returns them in buffr and the number of
c     them in n; the word is: if there is a ' character in the line,
c     then the word is everything between the successive ',
c     otherwise it is everything non-blank between two blanks.
c     Non-primed words can also occur between the primed ones.
c     subroutine str_sn2(line, buffr, maxn, n)
c
c --- reads words from line, returns them in buffr and the number of
c     them in n; the word is: everything non-blank between two blanks.
c     subroutine str_sn3(line, buffr, maxn, n)
c
c --- reads words from line, returns them in buffr and the number of
c     them in n; the word is: everything non-delimiter between two 
c     delimiter characters; many of those can be in string delim.
c     subroutine str_sn5(line, buffr, maxn, n, delim)
c
c --- If there are '(' or ')' in line, it deals only with the part
c     between the parentheses, ignoring the parentheses. It then reads
c     words from a selected part of line, returns them in buffr and the
c     number of them in n; the word is: if there is a ' character in the text,
c     then the word is everything between the two successive ',
c     otherwise it is everything non-blank between two blanks. Non-primed
c     words can also occur between the primed ones. This later part is
c     like str_sn2.
c     subroutine str_sn4(line, buffr, maxn, n)
c
c --- returns the first continuous stream of reals from the beginning
c     of the line in vector vec, and n, the number of them; maxn is
c     the dimension of vec
c     subroutine str_rn2(line, vec, maxn, n)
c
c --- returns the first continuous stream of integers from the beginning
c     of the line in vector ivec, and n, the number of them; maxn is
c     the dimension of ivec
c     subroutine str_in2(line, ivec, maxn, n)
c
c --- returns the first continuous stream of logicals from the beginning
c     of the line in vector vec, and n, the number of them; maxn is
c     the dimension of vec
c     subroutine str_ln2(line, vec, maxn, n, type)
c
c --- returns the string item to the left of the strin(ipos:ipos):
c     the string item starts at the first non-blank character to the
c     left of the ipos and ends at the first blank character to
c     the left of ipos or at the beginning of the strin, whichever first.
c     subroutine rdleft(strin, ipos, strout)
c
c --- to remove primes from the string
c      subroutine rmprimes(str)
c
c --- adds string add to the end of string str (using concat; output: str)
c     subroutine addstr(str,add)
c
c --- adds string add to the end of string str (using concat2; output: str)
c     subroutine add2str(str,add)
c
c --- adds string str to the beginning of string add (concat; output: add)
c     subroutine add3str(str,add)
c
c --- adds string str to the beginning of string add (concat2; output: add)
c     subroutine add4str(str,add)
c
c --- to remove chars from the string
c      subroutine rmchrs(str,ch)

c --- finds the index of an array element that is exactly equal to WORD
c     integer function ifindword(word,array,narray)
c
c --- finds the index of an array element that contains WORD (index())
c     integer function ifind3word(word,array,narray)
c
c --- finds an index of an array element that contains a
c     'blank-separated' word WORD;
c     integer function ifind2word(word,array,narray)
c
c --- a vector version of ifindword; exact matches;
c     subroutine ifindwordn(word,nword,array,narray,iword)
c
c
c code: ======================================================================
c
c --- puts blanks into string str
      subroutine blank(str)
        implicit none
        integer l, i
        character str*(*)
        l = len(str)
        if (l .gt. 0) then
          do  i = 1, l
            str(i:i) = ' '
          end do
        else
          stop 'Error[blank]: string 0 length'
        end if
        return
      end


c --- joins s1 and s2 strings into string s in such a way that there are
c     no blanks inbetween; s has the same idententation as s1;
c --- user has to ensure that the string s is long enough;
      subroutine concat(s1, s2, s)
      implicit none
      integer len1r,l1str,len2r,len2l,l2str,l0,i,lenl,lenr
      character*(*) s1, s2, s
c
c --- first right and left printable character indices:
      len1r = lenr(s1)
      if (len1r .lt. 1) then
        s = s2
        return
      end if
c --- number of printable characters:
      l1str = len1r
c
      len2r = lenr(s2)
      if (len2r .lt. 1) then
        s = s1
        return
      end if
      len2l = lenl(s2)
      l2str = len2r - len2l + 1
      l0 = len(s)
c
c --- do tests:
      if ((l1str+l2str) .gt. l0) then
        write(*,*) 'len(s1), len(s2), len(s3): ', l1str, l2str, l0
        write(*,*) 's1: ', s1
        write(*,*) 's2: ', s1
        write(*,*) 's3: ', s1
        stop 'Error:[concat] output string too short'
      end if
c
c --- retain leading blanks in s1:
      s(1:l1str) = s1(1:len1r)
c
      s(l1str+1:l1str+l2str) = s2(len2l:len2r)
c
      do 10  i = l1str+l2str+1, l0
10      s(i:) = ' '
c
      return
      end
c
c --- right justifies card:
      subroutine rjust(card)
      implicit none
      integer i, j, lenc, maxi, npos, lenr
      character blank*1
      parameter (blank = ' ')
      character card*(*)
      lenc = len(card)
      npos = lenr(card)
      j = lenc
      do 10  i = npos, 1, -1
        card(j:j) = card(i:i)
        j = j - 1
10    continue
      maxi = lenc - npos
      do 30 i=1,maxi
        card(i:i)=blank
30    continue
      return
      end


c --- joins s1 and s2 strings into string s in such a way that
c     no blanks at the end are taken from s1, but all the beginning
c     blanks in s2 are still put between s1 and s2; s has the same
c     idententation as s1;
      subroutine concat2(s1, s2, s)
      implicit none
      integer len1r,l1str,len2r,len2l,l2str,l0,i,lenr
      character*(*) s1, s2, s
c
c --- first right and left printable character indices:
      len1r = lenr(s1)
      if (len1r .lt. 1) then
        s = s2
        return
      end if
c --- number of printable characters:
      l1str = len1r
c
      len2r = lenr(s2)
      if (len2r .lt. 1) then
        s = s1
        return
      end if
      len2l = 1
      l2str = len2r - len2l + 1
      l0 = len(s)
c
c --- do tests:
      if ((l1str+l2str) .gt. l0) then
        write(*,*) 'len(s1), len(s2), len(s3): ', l1str, l2str, l0
        stop 'Error:[concat2] output string too short'
      end if
c
c --- retain leading blanks in s1:
      s(1:l1str) = s1(1:len1r)
c
c --- retain leading blanks in s2:
      s(l1str+1:l1str+l2str) = s2(len2l:len2r)
c
      do 10  i = l1str+l2str+1, l0
10      s(i:) = ' '
c
      return
      end


c --- will substitute all occurences of str1 with str2, in each element
c     of str(n)
      subroutine subs2n(str,n,str1,str2)
        implicit none
        integer n, i
        character str(n)*(*), str1*(*), str2*(*)

        do  i = 1, n
          call subs2(str,str1,str2)
        end do

        return
      end


c --- will substitute all occurences of str1 with str2, in str
      subroutine subs2(str,str1,str2)
        implicit none
        integer lr, lenr, lr2, ipos, inew
        character str*(*), str1*(*), str2*(*)

        lr = lenr(str1)
        if (lr .le. 0) stop 'Error[subs2]; str1 is empty'
        lr2 = lenr(str2)

        inew = 1
 10     ipos = index(str(inew:), str1)
          if (ipos .le. 0) return
          call subs(str(inew:),ipos-1,lr,str2)
          inew = inew + ipos + lr2 - 1
        go to 10

      end
c
c
c --- will substitute cont1(istart+1:istart+n) for sub
      subroutine subs(str,istart,n,sub)
        implicit none
        integer maxlen,lstr,lsub,n,j,l,i,lenr,istart
        parameter (maxlen=255)
        character str*(*), sub*(*), buffer*(maxlen)
        lstr = lenr(str)
        lsub = len(sub)
        if (lstr.eq.0) stop 'Error[subs] lenr(str) = 0'
c        if (lsub.eq.0) stop 'Error[subs] len(sub) = 0'
        if (lstr .lt. istart) stop 'Error[subs] istart > lenr(str)'
        if ((lstr-n+lsub) .gt. len(str)) then
          write(*,*) 'Error[subs] str too short: ',len(str),lstr,n,lsub
          stop
        end if
        if ((lstr-istart-n+1) .gt. maxlen)
     &       stop 'Error[subs] increase maxlen'
        j = 1
c ----- str = begin + substituted + end
c ----- save the end
        do  i = istart+n+1, lstr
          buffer(j:j) = str(i:i)
          j = j + 1
        end do
c ----- copy the sub to str after begin
        j = istart+1
        do  i = 1, lsub
          str(j:j) = sub(i:i)
          j = j + 1
        end do
c ----- append the end to str:
        j = istart + lsub + 1
        do  i = 1, lstr-istart-n+1
          str(j:j) = buffer(i:i)
          j = j + 1
        end do
c ----- padd the str with blanks
        l = len(str)
        do  i = lstr-n+lsub+1, l
          str(i:i) = ' '
        end do
        return
      end
c
c --- left justifies card:
      subroutine ljust(card)
      implicit none
      character blank*1
      parameter (blank = ' ')
      integer nl,nr,i,lenr,lenl,lenc
      character card*(*)
      nl = lenl(card)
      nr = lenr(card)
      if (nr .ge. nl) then
        card(1:(nr-nl+1)) = card(nl:nr)
        lenc = len(card)
        do 5  i = nr-nl+2, lenc
5         card(i:i) = blank
      end if
      return
      end
c
c --- transforms all characters in string s into lower case
      subroutine lower(s)
      implicit none
      integer i,lens,ich
      character s*(*), ch*1
      lens = len(s)
      do  i = 1, lens
        ch = s(i:i)
        ich = ichar(ch)
        if ((ich .ge. 65) .and. (ich .le. 90)) ich = ich + 32
        s(i:i) = char(ich)
      end do
      return
      end
c
c --- transforms all characters in string s into lower case except for
c     the characters between two successive primes
      subroutine lowerp(s)
      implicit none
      integer iprime
      parameter (iprime = 39)
      integer lens,i,ich
      logical prime
      character s*(*), ch*1

      lens = len(s)
      prime = .false.
      do  i = 1, lens
        ch = s(i:i)
        ich = ichar(ch)
        if (ich .eq. iprime) prime = .not. prime
        if((ich.ge.65).and.(ich.le.90).and.(.not.prime))ich=ich+32
        s(i:i) = char(ich)
      end do

      return
      end
c
c --- to transform all characters in the input string s into upper case;
      subroutine upper(s)
        implicit none
        integer lcont,lenr,ich,i
        character s*(*), ch*1
        lcont = lenr(s)
        do  i = 1, lcont
          ch = s(i:i)
          ich = ichar(ch)
          if((ich.ge.97).and.(ich.le.122)) ich=ich-32
          s(i:i) = char(ich)
        end do
        return
      end
c
c --- to transform all characters in the input string s into upper case, except
c     for parts between two primes;
      subroutine upperp(s)
        implicit none
        integer iprime
        parameter (iprime = 39)
        integer lcont,lenr,i,ich
        character s*(*), ch*1
        logical prime
        lcont = lenr(s)
        prime = .false.
        do  i = 1, lcont
          ch = s(i:i)
          ich = ichar(ch)
          if (ich .eq. iprime) prime = .not. prime
          if((ich.ge.97).and.(ich.le.122).and.(.not.prime))ich=ich-32
          s(i:i) = char(ich)
        end do
        return
      end
c
c --- the index of the first printable character from the right:
c       (if no printable character, 0 is returned)
c       (if string argument incorrect, it aborts)
      integer function lenr(str)
      implicit none
      integer lens,i,lenl,lenlft,irndsp,ilast
      character str*(*)
      lens = len(str)
      if (lens .gt. 0) then
        lenlft = lenl(str)
        if (lenlft .le. lens) then
          i = lenlft
10        if (irndsp(str(i:i)) .eq. 1) go to 100
            if (str(i:i) .ne. ' ') ilast = i + 1
            i = i + 1
            if (i .le. lens) go to 10
100       continue
          lenr = ilast - 1
        else
          lenr = 0
        end if
      else
c        stop 'Error[lenr]: string has length 0'
        lenr = 0
      end if
      return
      end
c
c --- the index of the first printable character from the left:
c       (if no printable character, length+1 is returned)
c       (if string argument incorrect, it aborts)
      integer function lenl(str)
      implicit none
      integer lens,ilndsp,i
      character str*(*)
      lens = len(str)
      if (lens .gt. 0) then
        i = 1
10      if (ilndsp(str(i:i)) .eq. 0) go to 100
          i = i + 1
          if (i .le. lens) go to 10
100     continue
        lenl = i
      else
c        stop 'Error[lenl]: string has length 0'
        lenl = 0
      end if
      return
      end
c
c
c --- returns the position of the last character of target in the
c     search string (0 if not found)
      integer function indexr(search, target)
      implicit none
      integer i
      character search*(*), target*(*)
c --- due to a bug in lpi-fortran, test for this:
      if (len(search) .ge. len(target)) then
        i = index(search, target)
        if (i .gt. 0) then
          i = i + len(target) - 1
        end if
        indexr = i
      else
        indexr = 0
      end if
      return
      end

c
c --- returns the position of the last character of target word in the
c     search string (0 if not found)
      integer function indexwr(search, target)
      implicit none
      integer i,indexw,lenr
      character search*(*), target*(*)
      i = indexw(search, target)
      if (i .gt. 0) then
        i = i + lenr(target) - 1
      end if
      indexwr = i
      return
      end

c --- returns the index of the position of the first character 
c     of any of those specified in delim; if not found, 0 is returned;
      integer function index2(line, delim)
        implicit none
        integer lenr,npos,i
        character line*(*), delim*(*)

        npos = lenr(line)
        if (npos .gt. 0) then
          do  i = 1, npos
            if (index(delim, line(i:i)) .gt. 0) then
              index2 = i
              return 
            end if
          end do
          index2 = 0
        else
          index2 = 0
        end if

        return
      end

c
c --- returns the index of the last contiguous printable and non-blank
c     character in line, scan starting at the first such character:
c     if no non-blank printable character, 0 is returned;
      integer function indexb(line)
        implicit none
        integer lenr,npos,i,ii,lenl,ilndsp
        character line*(*)
        npos = lenr(line)
        if (npos .gt. 0) then
c ------- (if lenr in range, then lenl in range too -> (if npos>0 -> do
c         loop is executed at least once))
          do 100  i = lenl(line), npos
            ii = i
            if (ilndsp(line(ii:ii)) .eq. 1) go to 200
100       continue
          indexb = ii
          return
200       indexb = ii-1
        else
          indexb = 0
        end if
        return
      end
c
c
c --- converts real r to string s with npre and npost places for
c     pre-decimal and post-decimal digits, respectively.
      subroutine r_str(r, s, npre, npost)
      implicit none
      integer ipre, ipost, npre, npost, l, lenl
      real r
      character s*(*)

      ipre  = ifix(r)
      ipost = abs(ifix((r-ipre)*10.0**npost))

      call i_str(ipre, s, npre)
      if (npost.ge.0) then
        s(npre+1:) = '.'
        if (npost.ge.1) call i_str2(ipost, s(npre+2:), npost)
      end if
 
      if ((ipre.eq.0).and.(r.lt.0.0)) then
        l = lenl(s)
        if (l.le.1) then
          write(*,'(a)') 'i_str___E> no space for - in the string'
          stop
        else
          s(l-1:l-1) = '-'
        end if
      end if

      return
      end


 
c --- converts integer i to string s, write out in the Inpre format.
      subroutine i_str(i, s, npre)
      implicit none
      integer i, npre, l
      character s*(*), fmt*10

      l = len(s)
      if (npre.gt.l) then
        write(*,'(a,i4)') 'i_str___E> string too short: ', l, npre
        stop
      end if

      if ((npre.ge.1).and.(npre.le.9)) then
        write(fmt, '(''(i'',i1,'')'')') npre
      else
        if ((npre.ge.10).and.(npre.le.99)) then
          write(fmt, '(''(i'',i2,'')'')') npre
        else
          write(*,'(a,i4)') 'i_str___E> npre out of range: ', npre
          stop
        end if
      end if

      write(s, fmt) i

      return
      end


      subroutine i_str2(i, s, npre)
      implicit none
      integer i, npre, l
      character s*(*), fmt*10

      l = len(s)
      if (npre.gt.l) then
        write(*,'(a,i4)') 'i_str___E> string too short: ', l, npre
        stop
      end if

      if ((npre.ge.1).and.(npre.le.9)) then
        write(fmt, '(''(i'',i1,''.'',i1,'')'')') npre, npre
      else
        if ((npre.ge.10).and.(npre.le.99)) then
          write(fmt, '(''(i'',i2,''.'',i2,'')'')') npre, npre
        else
          write(*,'(a,i4)') 'i_str___E> npre out of range: ', npre
          stop
        end if
      end if

      write(s, fmt) i

      return
      end
c --- reads a number of integers from string line
      subroutine str_in(line, ibuffr, n)
        implicit none
        integer n,ibuffr(n),ilast,iend,i,indexb
        character line*(*)
        ilast = 0
        do  i = 1, n
          call str_i(line(ilast+1:), ibuffr(i))
          iend = indexb(line(ilast+1:))
          ilast = ilast + iend
        end do
        return
      end
c
c --- converts the first contiguous block of non-blank characters
c     in string str to integer i;
c --- integer form is: [+-] [digits]
      subroutine str_i(s, n)
        implicit none
        integer n, ierr
        character s*(*)
        call wrd_i(s, n, ierr)
        return
      end
c
c --- reads n reals from string line into real array buffr:
      subroutine str_rn(line, buffr, n)
        implicit none
        integer ilast, i, n, iend, indexb
        real buffr(n)
        character line*(*)
        ilast = 0
        do  i = 1, n
          call str_r(line(ilast+1:), buffr(i))
          iend = indexb(line(ilast+1:))
          ilast = ilast + iend
        end do
        return
      end
c
c
c --- converts the first contiguous block of non-blank characters
c     in string s to real e;
c     real number is: [+-] [digits] [.] [digits] [eEdD] [+-] [digits]
      subroutine str_r(s, e)
        implicit none
        integer ierr
        real e
        character s*(*)
        call wrd_r(s, e, ierr)
        return
      end
c
c --- reads a number of logicals from string line:
      subroutine str_ln(line, buffr, n, type)
        implicit none
        integer n, ilast, i, iend, indexb
        character line*(*), type*(*)
        logical buffr(n)
        ilast = 0
        do  i = 1, n
          call str_l(line(ilast+1:), buffr(i), type)
          iend = indexb(line(ilast+1:))
          ilast = ilast + iend
        end do
        return
      end
c
c --- reads one logical variable from line (on/off in upper or
c     lower case are true and false, respectively, if type='O')
      subroutine str_l(line, arg, type)
        implicit none
        integer ierr
        character line*(*), type*(*)
        logical arg
c
        call wrd_l(line, arg, type, ierr)
c
        return
      end
c
c
c --- reads words from line, returns them in buffr and the number of
c     them in n; the word is: everything non-blank between two blanks.
      subroutine str_sn3(line, buffr, maxn, n)
        implicit none
        integer maxn,n,ilast,l,iend,indexb
        character line*(*), buffr(maxn)*(*)
        ilast = 0
        n = 0
        l = len(line)
5       if (ilast+1 .gt. l) go to 10
          iend = indexb(line(ilast+1:))
c ------- is there no string left in the line?
          if (iend .eq. 0) go to 10
          n = n + 1
          call str_s(line(ilast+1:), buffr(n))
          ilast = ilast + iend
          go to 5
10      continue
        return
      end


c
c --- reads words from line, returns them in buffr and the number of
c     them in n; the word is: if there is a ' character in the line,
c     then the word is everything between the successive ',
c     otherwise it is everything non-blank between two blanks.
c     Non-primed words can also occur between the primed ones.
      subroutine str_sn2(line, buffr, maxn, n)
        implicit none
        integer maxpos
        parameter (maxpos = 100)
        integer lenr
        integer ipos(maxpos),maxn,n,ind1,ind2,ip,ni,i1,i2,i,npos,nw
        character line*(*), buffr(maxn)*(*)

        n = 0

        ind1 = 1
        ind2 = len(line)
        if (ind2 .eq. 0) return

        ip = index(line(ind1:ind2), '''')
        if (ip .gt. 0) then
c ------- find positions of all '
          call getpos(line(ind1:ind2), '''', ipos, maxpos, npos)
c ------- testing:
          ni = npos / 2
          if(ni*2 .ne. npos) then 
            write(*,'(a,i4)') 'str_sn2_E> # of '' <> even:, ',npos
            write(*,'(a,i4,1x,a)') 'Len,String: ', lenr(line),
     &                             line(1:max(1,lenr(line)))
            stop
          end if
c ------- words before the first prime:
          i1 = ind1
          i2 = ind1+ipos(1)-2
          if (i2 .ge. i1) call str_sn3(line(i1:i2), buffr, maxn, n)
c ------- words after the first prime
          ipos(npos+1) = 9999
          do i = 1, npos, 2
            i1 = ind1+ipos(i)
            i2 = ind1+ipos(i+1)-2
            n = n + 1
            if (i2.ge.i1) then
              buffr(n)=line(i1:i2)
            else
              call blank(buffr(n))
            end if
c --------- words before the next prime (and after the last prime in the last
c           cycle):
            i1 = ind1+ipos(i+1)
            i2 = min(ind1+ipos(i+2)-2, ind2)
            if(i2.ge.i1) then
              call str_sn3(line(i1:i2), buffr(n+1), maxn, nw)
              n = n + nw
            end if
          end do
        else
          if (ind2.ge.ind1) call str_sn3(line(ind1:ind2),buffr,maxn,n)
        end if
        return
      end
c
c --- reads n blocks of contigous printable non-blank characters from
c     line and returns them in an array buffr;
      subroutine str_sn(line, buffr, n)
        implicit none
        integer n,ilast,i,iend,indexb
        character line*(*),buffr(n)*(*)
        ilast = 0
        do  i = 1, n
          call str_s(line(ilast+1:), buffr(i))
          iend = indexb(line(ilast+1:))
          ilast = ilast + iend
        end do
        return
      end
c
c --- returns the first contiguous block of printable non-blank
c     characters in line to buffr;
      subroutine str_s(line, buffr)
        implicit none
        character blank*1
        parameter (blank=' ')
        integer ibeg,iend,lenb,lenl,i,indexb
        character buffr*(*), line*(*)
        ibeg = lenl(line)
        iend = indexb(line)
        if (iend .ge. ibeg) then
          lenb = len(buffr)
          if ((iend-ibeg+1) .le. lenb) then
            buffr = line(ibeg:iend)
            do 10  i = iend+1, lenb
10            buffr(i:i) = blank
          else
            buffr = line
c            write(*,*)'Warning[str_s]: length of buffr to short; truncated'
          end if
        else
c         stop 'Error[str_s]: string has no contents'
          do 20  i = 1, len(buffr)
20          buffr(i:i) = blank
        end if
        return
      end
c
c --- returns the directory+root of the unix file name:
      subroutine diroot(file, rot)
        implicit none
        integer lenr,lr,ibeg,iend,i
        character file*(*), rot*(*)
        lr = lenr(file)
        ibeg = 0
c ----- position of the last dot:
        do 30  i = lr, 1, -1
          iend = i
          if (file(i:i) .eq. '.') go to 40
30      continue
        iend = lr+1
40      continue
c ----- hidden files:
        if (iend .eq. 1) iend = lr+1
        if (iend-ibeg-1 .le. 0) stop 'Error[root]: length of root <= 0'
        rot = file(ibeg+1:iend-1)
        return
      end
c
c --- returns the root of the unix file name (directory and everything after
c     the first after the last '/' is eliminated):
      subroutine rootfn2(file, rot)
        implicit none
        integer lr,lenr,i,ibeg,iend
        character file*(*), rot*(*)

        lr = lenr(file)
c ----- position of the last slash:
        do 10  i = lr, 1, -1
          ibeg = i
          if (file(i:i) .eq. '/') go to 20
10      continue
        ibeg = 0

20      continue
c ----- position of the first dot after the last slash:
        do 30  i = ibeg+1, lr
          iend = i
          if (file(i:i) .eq. '.') go to 40
30      continue
        iend = lr+1
        go to 50
40      continue
c ----- for files in the form '../file' and '.file'
        if (iend .le. 2) then
          if (file(1:1) .eq. '.') iend = lr+1
        end if
50      continue
        if (iend-ibeg-1 .le. 0) then
          write(*,*) 'file: ', file
          write(*,*) 'iend,ibeg: ', iend, ibeg
          stop 'Error[rootfn]: length of root <= 0'
        end if
        rot = file(ibeg+1:iend-1)
        return
      end


c --- returns the root of the unix file name (directory and everything after
c     the last dot is eliminated):
      subroutine rootfn(file, rot)
        implicit none
        integer lr,lenr,i,ibeg,iend
        character file*(*), rot*(*)
        lr = lenr(file)
c ----- position of the last slash:
        do 10  i = lr, 1, -1
          ibeg = i
          if (file(i:i) .eq. '/') go to 20
10      continue
        ibeg = 0

20      continue
c ----- position of the last dot:
        do 30  i = lr, 1, -1
          iend = i
          if (file(i:i) .eq. '.') go to 40
30      continue
        iend = lr+1
        go to 50
40      continue
c ----- for files in the form '../file' and '.file'
        if (iend .le. 2) then
          if (file(1:1) .eq. '.') iend = lr+1
        end if
50      continue
        if (iend-ibeg-1 .le. 0) then
          write(*,*) 'file: ', file
          write(*,*) 'iend,ibeg: ', iend, ibeg
          stop 'Error[rootfn]: length of root <= 0'
        end if
        rot = file(ibeg+1:iend-1)
        return
      end
c
c
c --- returns the directory of the unix file name:
      subroutine direct(file, dir)
        implicit none
        integer l,lr,lenr,iend,i
        character file*(*), dir*(*)
        lr = lenr(file)
c ----- position of the last  slash:
        do 10  i = lr, 1, -1
          iend = i
          if (file(i:i) .eq. '/') go to 20
10      continue
        iend = 0
20      continue
        if (iend .gt. 0) then
          dir = file(1:iend)
        else
          l = len(dir)
          do 30  i = 1, l
30          dir(i:i) = ' '
        end if
        return
      end
c
c --- returns the number of blocks of contiguous non-blank characters
c     in line:
      integer function nstr(line)
        implicit none
        integer n,i1,i2,indexb
        character line*(*)
        n = 0
        i1 = 0
10      i2 = indexb(line(i1+1:))
        if (i2 .eq. 0) go to 20
          n = n + 1
          i1 = i1 + i2
          go to 10
20      continue
        nstr = n
        return
      end
c
c --- padd the string str with blanks
      subroutine padbln(str)
        implicit none
        character blank*1
        parameter (blank = ' ')
        integer l,i,lenr
        character str*(*)
        l = len(str)
        if (l .eq. 0) stop 'Error[padbln]: len(string) = 0'
        do i = lenr(str)+1, l
          str(i:) = blank
        end do
        return
      end



c
c
c --------------------------------------------------------------
c miscelaneous routines, not very useful on their own:
c --------------------------------------------------------------
c
c --- reads real number in the form: [+-] [digits] [.] [digits]
      subroutine realn(str, r, ierr)
        implicit none
        character plus*1, minus*1, dot*1
        parameter (plus='+', minus='-', dot='.')
        integer lenl, ipre, ipost, npos, idot, ll, lr,ierr,lenr,
     &          ierr1,ierr2
        real r, sgn
        character str*(*)
c
        ierr = 0

        lr = lenr(str)
        if(lr .eq. 0) stop 'Error[realn]: string has length 0'
        ll = lenl(str)
        if(ll .gt. lr) stop 'Error[realn]: lenr < lenl'
c
        if (str(ll:ll) .eq. plus) then
          sgn = 1.0
          ll = ll + 1
        else
          if (str(ll:ll) .eq. minus) then
            sgn = -1.0
            ll = ll + 1
          else
            sgn = 1.0
          end if
        end if
c
        idot = index(str, dot)
        if ((idot .gt. 0) .and. (idot.lt.lr)) then
          call integ(str(ll:idot), ipre, npos, ierr1)
          call integ(str(idot+1:lr), ipost, npos, ierr2)
          if ((ierr1.gt.0) .and. (ierr2.gt.0)) ierr = 1
          r = sgn * (float(ipre) + float(ipost)/10.0**npos)
        else
          call wrd_i(str(ll:lr), ipre, ierr)
          r = sgn * float(ipre)
        end if
c
        return
      end
c
c --- reads positive integer in the form: [digits]
c     it also returns the number of digits in intg in variable lenint
      subroutine integ(str, intg, lenint, ierr)
        implicit none
        character digits*10
        parameter (digits = '1234567890')
        integer ierr, i, intg, lenint, ll, lr, nch, lenr, lenl
        character str*(*), ch*1
c
        ierr = 0

        lr = lenr(str)
        if(lr .eq. 0) stop 'Error[integ]: string has length 0'
        ll = lenl(str)
        if(ll .gt. lr) stop 'Error[integ]: lenr < lenl'
c
        intg = 0
        lenint = 0
        do 10  i = ll, lr
          ch = str(i:i)
          if (index(digits, ch) .eq. 0) go to 30
          nch = ichar(ch) - 48
          intg = intg*10 + nch
          lenint = lenint + 1
10      continue
c
30      continue
c ----- if no digits, signal an error
        if (lenint .eq. 0) ierr = 1
        return
      end


      integer function ilndsp(ch)
        implicit none
        integer ich
        character ch*1
        ich=ichar(ch)
        if((ich.lt.33).or.(ich.gt.127)) then
          ilndsp = 1
        else
          ilndsp = 0
        end if
        return
      end
c
      integer function irndsp(ch)
        implicit none
        integer ich
        character ch*1
        ich=ichar(ch)
        if((ich.lt.32).or.(ich.gt.127)) then
          irndsp = 1
        else
          irndsp = 0
        end if
        return
      end

c --- returns the positions of target in line in ipos(maypos), npos
c     such positions are returned; beware: in 'andandand' target
c     andand occurs twice!
      subroutine getpos(line, target, ipos, maxpos, npos)
        implicit none
        integer maxpos,npos,ipos(maxpos),ll,lenr,lt,i
        character line*(*), target*(*)
        ll = lenr(line)
        lt = lenr(target)
        npos = 0
        do  i = 1, ll-lt+1
          if (line(i:i+lt-1) .eq. target(1:lt)) then
            npos = npos + 1
            if (npos .gt. maxpos) stop 'getpos__E> increase maxpos'
            ipos(npos) = i
          end if
        end do
        return
      end

c --- returns the first continuous stream of reals from the beginning
c     of the line in vector vec, and n, the number of them;
      subroutine str_rn2(line, vec, maxn, n)
        implicit none
        integer maxlen,maxbuf
        parameter (maxlen=60, maxbuf=200)
        integer maxn,n,ierr,i,nwords
        real vec(maxn)
        character buff(maxbuf)*(maxlen), line*(*)

c ----- get the words in the line (no ' taken into account):
        call str_sn3(line, buff, maxbuf, nwords)

c ----- check them for being integers and stop when reaching the first
c       non-integer:
        n = 0
        ierr = 0
        do  i = 1, nwords
          call wrd_r(buff(i), vec(i), ierr)
          if (ierr .gt. 0) return
          n = i
        end do

        return
      end

c --- returns the first continuous stream of integers from the beginning
c     of the line in vector ivec, and n, the number of them;
      subroutine str_in2(line, ivec, maxn, n)
        implicit none
        integer maxlen,maxbuf
        parameter (maxlen=60, maxbuf=200)
        integer maxn,n,ierr,i,nwords
        integer ivec(maxn)
        character buff(maxbuf)*(maxlen), line*(*)

c ----- get the words in the line (no ' taken into account):
        call str_sn3(line, buff, maxbuf, nwords)

c ----- check them for being integers and stop when reaching the first
c       non-integer:
        n = 0
        ierr = 0
        do  i = 1, nwords
          call wrd_i(buff(i), ivec(i), ierr)
          if (ierr .gt. 0) return
          n = i
        end do

        return
      end


c --- the same as str_i, except with error returned as an argument
c     if the contents of s not recognized as an integer variable:
c --- integer form is: [+-] [digits]
      subroutine wrd_i(s, n, ierr)
        implicit none
        character plus*1, minus*1
        parameter (plus='+', minus='-')
        integer n, intg, lr, ll, isgn, lenr, lenint, lenl, ierr
        character s*(*), str*255

        ierr = 0
c
        call str_s(s, str)
c
        lr = lenr(str)
        if(lr .eq. 0) then
          ierr = 1
          return
        end if
        ll = lenl(str)
        if(ll .gt. lr) then
          ierr = 1
          return
        end if
c
        if (str(ll:ll) .eq. plus) then
          isgn = 1
          ll = ll + 1
        else
          if (str(ll:ll) .eq. minus) then
            isgn = -1
            ll = ll + 1
          else
            isgn = 1
          end if
        end if
c
        call integ(str(ll:lr), intg, lenint, ierr)
        n = isgn*intg
c
        return
      end


c --- returns the first continuous stream of integers from the beginning
c     of the line in vector vec, and n, the number of them;
      subroutine str_ln2(line, vec, maxn, n, type)
        implicit none
        integer maxlen,maxbuf
        parameter (maxlen=60, maxbuf=200)
        integer maxn,n,ierr,i,nwords
        logical vec(maxn)
        character buff(maxbuf)*(maxlen), line*(*), type*(*)

c ----- get the words in the line (no ' taken into account):
        call str_sn3(line, buff, maxbuf, nwords)

c ----- check them for being logicals and stop when reaching the first
c       non-logical:
        n = 0
        ierr = 0
        do  i = 1, nwords
          call wrd_l(buff(i), vec(i), type, ierr)
          if (ierr .gt. 0) return
          n = i
        end do

        return
      end


c --- the same as str_r, except with error returned as an argumnent if
c     the contents of s not recognized as a real variable;
c     real number is: [+-] [digits] [.] [digits] [eEdD] [+-] [digits]
      subroutine wrd_r(s, e, ierr)
        implicit none
        integer nexp, lenl, lr, ll, iexp, ierr, lenr
        real e, r
        character s*(*), str*255
c
        ierr = 0
c
        call str_s(s, str)
c
        lr = lenr(str)
        if(lr .eq. 0) then
          ierr = 1
          return
        end if
        ll = lenl(str)
        if(ll .gt. lr) then
          ierr = 1
          return
        end if
c
        iexp = max(index(str,'e'), index(str,'E'), index(str,'d'),
     &             index(str,'D'))
c
        if (iexp .gt. 0) then
c ------- any character positions for the mantissa:
          if (ll .lt. iexp) then
            call realn(str(ll:iexp-1), r, ierr)
            if (ierr .eq. 0) then
c ----------- any digits for the exponent:
              if (iexp .lt. lr) then
                call wrd_i(str(iexp+1:lr), nexp, ierr)
                if (ierr .eq. 0) e = r * 10.0**nexp
              end if
            end if
          else
            ierr = 1
          end if
        else
          call realn(str, e, ierr)
        end if
c
        return
      end
c
c     the same as str_l, except that error is returned as an argument,
c     if the contents of line not recognized as a logical variable:
c --- reads one logical variable from line (on/off in upper or
c     lower case are true and false, respectively, if type='O')
      subroutine wrd_l(line, arg, type, ierr)
        implicit none
        integer ierr,indexw
        character line*(*), str*7, type*(*), type2*1
        logical arg
        ierr = 0
        call str_s(line, str)
        call upper(str)
        type2 = type
        call upper(type2)

c ----- default value is .f.:
        arg = .false.

        if (type2 .eq. 'O') then
          if (indexw(str,'ON') .gt. 0)  then
            arg = .true.
          else
            if (indexw(str, 'OFF') .gt. 0) then
              arg = .false.
            else
              ierr = 1
            end if
          end if
        else
c ------- at least a '.t' or '.T' will mean .T.:
          if (index(str,'.T')  .gt. 0) then
            arg = .true.
          else
            if (index(str, '.F') .gt. 0) then
              arg = .false.
            else
              ierr = 1
            end if
          end if
        end if
        return
      end

c --- this routine returns the index of the word in str; the word
c     must be bounded by the beginning or end of the str or by a
c     blank; logical word is only the part of the physical word
c     as determined by lenl and lenr
      integer function indexw(str,word)
      implicit none
      integer ibeg,iend,ipos,llw,lrw,lrs,i1,i2,lenl,lenr
      character str*(*), word*(*)
      llw = lenl(word)
      lrw = lenr(word)
      lrs = lenr(str)
      iend= lrs - (lrw-llw)
      ibeg = 1
      if ((lrs .lt. ibeg) .or. (lrw.lt.llw)) go to 100
10    if(ibeg .gt. iend) go to 100
        ipos = index(str(ibeg:lrs), word(llw:lrw))
        if (ipos .gt. 0) then
          i1 = ibeg + ipos - 1
          i2 = i1 + lrw - llw
          if (i1 .eq. 1) then
            if (i2 .eq. lrs) go to 200
            if (str(i2+1:i2+1).eq.' ') go to 200
          else
            if (str(i1-1:i1-1).eq.' ') then
              if (i2 .eq. lrs) go to 200
              if (str(i2+1:i2+1).eq.' ') go to 200
            end if
          end if
        else
          go to 100
        end if
        ibeg = ibeg + ipos
      go to 10
100   continue
      indexw = 0
      return
200   continue
      indexw = i1
      return
      end

c --- returns the string item to the left of the strin(ipos:ipos):
c     the string item starts at the first non-blank character to the
c     left of the ipos and ends at the first blank character to
c     the left of ipos or at the beginning of the strin, whichever first.
      subroutine rdleft(strin, ipos, strout)
        implicit none
        integer ipos,i,il,ilndsp,ib
        character strin*(*), strout*(*)

        if ((ipos .ge. 2) .and. (ipos .le. len(strin))) then

c ------- find the last position of the string item to the left:
          do 10  i = ipos-1, 1, -1
            il = i
            if (ilndsp(strin(il:il)) .eq. 0) go to 15
10        continue
c ------- all blanks to the left: return
          strout = ' '
          return
15        continue

c ------- find the first position of the string to the left:
          do 20  i = il, 1, -1
            ib = i
            if (ilndsp(strin(ib:ib)) .eq. 1) go to 30
20        continue
          ib = 0
30        ib = ib + 1

          if (il-ib+1 .gt. len(strout)) then
            write(*,*) 'Error[rdleft]: strout too short.'
            return
          end if

          strout = strin(ib:il)
        else
          strout = ' '
        end if
        return
      end

c --- to remove chars from the string
      subroutine rmchrs(str,ch)
      implicit none
      integer lr,lenr,i,k
      character str*(*),ch*1
      lr = lenr(str)
10    if (lr .eq. 0) return
        i = index(str, ch)
        if (i .eq. 0) return
        lr = lr - 1
        do  k = i, lr
          str(k:k) = str(k+1:k+1)
        end do
        str(lr+1:lr+1) = ' '
      go to 10
      end

c --- to remove primes from the string
      subroutine rmprimes(str)
      implicit none
      integer lr,lenr,i,k
      character str*(*)
      lr = lenr(str)
10    if (lr .eq. 0) return
        i = index(str, '''')
        if (i .eq. 0) return
c        call subs(str, i-1, 1, '')
        lr = lr - 1
        do  k = i, lr
          str(k:k) = str(k+1:k+1)
        end do
        str(lr+1:lr+1) = ' '
      go to 10
      end
c
c --- If there are '(' or ')' in line, it deals only with the part
c     between the parentheses, ignoring the parentheses. It then reads
c     words from a selected part of line, returns them in buffr and the
c     number of them in n; the word is: if there is a ' character in the text,
c     then the word is everything between the two successive ',
c     otherwise it is everything non-blank between two blanks. Non-primed
c     words can also occur between the primed ones. This later part is
c     like str_sn2.
      subroutine str_sn4(line, buffr, maxn, n)
        implicit none
        integer maxpos
        parameter (maxpos = 100)
        integer i,n,maxn,ind1,ind2,lenr,ipos(maxpos),ip,ni,i1,i2,nw,npos
        character line*(*), buffr(maxn)*(*)

        n = 0

c ----- these two are the only occurences of the delimiter symbols:
        ind1 = index(line,'(') + 1
        ind2 = index(line,')')
        if (ind2 .eq. 0) then
          ind2 = lenr(line)
        else
          ind2 = ind2 - 1
        end if
        if (ind2 .eq. 0) return

        ip = index(line(ind1:ind2), '''')
        if (ip .gt. 0) then
c ------- find positions of all '
          call getpos(line(ind1:ind2), '''', ipos, maxpos, npos)
c ------- testing:
          ni = npos / 2
          if(ni*2 .ne. npos) then 
            write(*,'(a,i4)') 'str_sn4_E> # of '' <> even:, ',npos
            write(*,'(a,i4,1x,a)') 'Len,String: ', lenr(line),
     &                             line(1:max(1,lenr(line)))
            stop
          end if
c ------- words before the first prime:
          i1 = ind1
          i2 = ind1+ipos(1)-2
          if (i2 .ge. i1) call str_sn3(line(i1:i2), buffr, maxn, n)
c ------- words after the first prime
          ipos(npos+1) = 9999
          do i = 1, npos, 2
            i1 = ind1+ipos(i)
            i2 = ind1+ipos(i+1)-2
            n = n + 1
            if (i2.ge.i1) then
              buffr(n)=line(i1:i2)
            else
              call blank(buffr(n))
            end if
c --------- words before the next prime (and after the last prime in the last
c           cycle):
            i1 = ind1+ipos(i+1)
            i2 = min(ind1+ipos(i+2)-2, ind2)
            if(i2.ge.i1) then
              call str_sn3(line(i1:i2), buffr(n+1), maxn, nw)
              n = n + nw
            end if
          end do
        else
          if (ind2.ge.ind1)
     &        call str_sn3(line(ind1:ind2), buffr, maxn, n)
        end if
        return
      end
c
c
c
c --- reads words from line, returns them in buffr and the number of
c     them in n; the word is: everything non-delimiter between two delimiters.
      subroutine str_sn5(line, buffr, maxn, n, delim)
        implicit none
        integer maxn,n,ilast,l,iend,index2,i2,lenr
        character line*(*), buffr(maxn)*(*), delim*(*)
        ilast = 0
        n = 0
        l = lenr(line)
5       if (ilast .gt. l) go to 10
c ------- returns the position of the first delim character:
          iend = index2(line(ilast+1:), delim)
          n = n + 1
          if (iend .eq. 0) then
            i2 = l
          else
            i2 = ilast+iend-1
          end if
          if (i2 .gt. ilast) then
            buffr(n) = line(ilast+1:i2)
          else
            buffr(n) = ' '
          end if
c          write(*,*)n,ilast,i2,' ',buffr(n)(1:lenr(buffr(n)))
          ilast = i2+1
          if (iend .gt. 0) go to 5
10      continue
        return
      end
c
c
c --- adds string add to the end of string str (using concat)
      subroutine addstr(str,add)
        implicit none
        integer maxstr
        parameter (maxstr=1024)
        integer lr1,lr2,lenr,lr
        character dummy*(maxstr), str*(*), add*(*)

        lr1 = lenr(str)
        lr2 = lenr(add)
        lr  = len(str)
        if (lr1+lr2 .gt. maxstr) stop 'Error[addstr]; increase maxstr.'
        if (lr1+lr2 .gt. lr)
     &     stop 'Error[addstr]; increase the length of str.'
        call concat(str, add, dummy)
        str = dummy

        return
      end


c --- adds string add to the end of string str (using concat2)
      subroutine add2str(str,add)
        implicit none
        integer maxstr
        parameter (maxstr=1024)
        integer lr1,lr2,lenr,lr
        character dummy*(maxstr), str*(*), add*(*)

        lr1 = lenr(str)
        lr2 = lenr(add)
        lr  = len(str)
        if (lr1+lr2 .gt. maxstr) stop 'Error[addstr]; increase maxstr.'
        if (lr1+lr2 .gt. lr)
     &     stop 'Error[addstr]; increase the length of str.'
        call concat2(str, add, dummy)
        str = dummy

        return
      end


c --- adds string str to the beginning of string add (concat; output: add)
      subroutine add3str(str,add)
        implicit none
        integer maxstr
        parameter (maxstr=1024)
        integer lr1,lr2,lenr,lr
        character dummy*(maxstr), str*(*), add*(*)

        lr1 = lenr(str)
        lr2 = lenr(add)
        lr  = len(add)
        if (lr1+lr2 .gt. maxstr) stop 'Error[addstr]; increase maxstr.'
        if (lr1+lr2 .gt. lr)
     &     stop 'Error[addstr]; increase the length of add.'
        call concat(str, add, dummy)
        add = dummy

        return
      end


c --- adds string str to the beginning of string add (concat2; output: add)
      subroutine add4str(str,add)
        implicit none
        integer maxstr
        parameter (maxstr=1024)
        integer lr1,lr2,lenr,lr
        character dummy*(maxstr), str*(*), add*(*)

        lr1 = lenr(str)
        lr2 = lenr(add)
        lr  = len(add)
        if (lr1+lr2 .gt. maxstr) stop 'Error[addstr]; increase maxstr.'
        if (lr1+lr2 .gt. lr) then
          write(*,*) 'Error[add4str]; increase the length of add: ',
     &    lr1, lr2, lr
          write(*,*) '      str :', str
          write(*,*) '      add :', add
          stop
        end if
        call concat2(str, add, dummy)
        add = dummy

        return
      end


c --- finds the index of an array element that is exactly equal to WORD
      integer function ifindword(word,array,narray)
        implicit none
        integer narray,i
        character array(narray)*(*)
        character word*(*)

        do  i = 1, narray
          if (word .eq. array(i)) then
C --------- found it, return its index ;
            ifindword = i
            return
          end if
        end do

C ----- search not successful; return 0 ;
        ifindword = 0

        return
      end

c --- finds the index of an array element that contains WORD (index())
      integer function ifind3word(word,array,narray)
        implicit none
        integer narray,i
        character array(narray)*(*)
        character word*(*)

        do  i = 1, narray
          if (index(array(i), word) .gt. 0) then
C --------- found it, return its index ;
            ifind3word = i
            return
          end if
        end do

C ----- search not successful; return 0 ;
        ifind3word = 0

        return
      end


c --- finds an index of an array element that contains a
c     'blank-separated' word WORD;
      integer function ifind2word(word,array,narray)
        implicit none
        integer i, narray, indexw
        character array(narray)*(*)
        character word*(*)

        do  i = 1, narray
c ------- old:
c          if (indexw(word, array(i)) .gt. 0) then
c ------- new:
          if (indexw(array(i),word) .gt. 0) then
c --------- found it, return its index ;
            ifind2word = i
            return
          end if
        end do

c ----- search not successful; return 0 ;
        ifind2word = 0

        return
      end

c --- a vector version of ifindword; exact matches;
      subroutine ifindwordn(word,nword,array,narray,iword)
        implicit none
        integer nword,iword(nword),ityp,narray,i,ifindword
        character word(nword)*(*), array(narray)*(*)

        do 200  i = 1, nword
          ityp = ifindword(word(i),array,narray)
          if (ityp.ne.0) then
            iword(i) = ityp
          else
            write(*,190) word(i)
190         format('Error[ifindwordn]: Word not found in array; ', a)
          end if
200     continue

        return
      end


c --- this function returns the first occurence of target in str, but
c     only using those parts of string which are not quoted in ''
      integer function indexs(str, target)
        implicit none
        integer iprime
        parameter (iprime = 39)
        character str*(*), target*(*)
        integer ip1, i, ind, lr, lenr
        logical prime

        prime = .false.
        ip1 = 1
        lr = lenr(str)
        do  i = 1, lr
          if (ichar(str(i:i)) .eq. iprime) then
            prime = .not. prime
            if (prime) then
              if (i .gt. ip1) then
                ind = index(str(ip1:i-1), target)
                if (ind .gt. 0) then
                  indexs = ip1 + ind - 1
                  return
                end if
              end if
            else
              ip1 = i
            end if
          end if
        end do

        if (.not. prime) then
          ind = index(str(ip1:), target)
          if (ind .gt. 0) then
            indexs = ip1 + ind - 1
          else
            indexs = 0
          end if
        else
          indexs = 0
        end if

        return
      end



c c --- extract an integer value from the STRING.
c       subroutine wrd_i(string, intval, error)
c       integer len,lenr
c       integer intval
c       integer error
c       character*(*) string
c       character buffer*80, str*(30)
c 
c       call str_s(string, str)
c       write(buffer, '(2H(I, I2, 1H))') lenr(str)
c       read(string, buffer, err=100) intval
c       error = 0
c       return
c 100   continue
c       error = 1
c       write(*,'(2a/2a)') 'wrd_i___E> ', string, 
c      -                   '           format: ', buffer
c       stop
c       return
c       end
c 
c c --- extract a real value from the STRING.
c       subroutine wrd_r(string, value, error)
c       integer len,lenr
c       integer error
c       real value
c       character*(*) string
c       character buffer*80, str*(30)
c 
c       call str_s(string, str)
c       write(buffer, '(2H(E, I2, 3H.0))') lenr(str)
c       read(string, buffer, err=100) value
c       error = 0
c       return
c 100   continue
c       error = 1
c       write(*,'(2a/2a)') 'wrd_r___E> ', string, 
c      -                   '           format: ', buffer
c       stop
c       return
c       end


c --- substitute string str1 with str2 in all n1 x n2 elements of
c     str(n1,n2)*(*)

      subroutine subs2nn(str,n1,n2,str1,str2)
        implicit none
        integer i1, i2, n1, n2, indexw
        character str(n1,n2)*(*), str1*(*), str2*(*)
        do  i1 = 1, n1
          do  i2 = 1, n2
            if (indexw(str(i1,i2), str1) .gt. 0) str(i1,i2) = str2
          end do
        end do
        return
      end
