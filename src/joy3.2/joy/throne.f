      subroutine THRONE(CHAR,CHARNW,NRES)
C
C copyright (c) 1988-1991 John Overington
C part of joy
C
C Converts three letter code to one letter code
C
      include 'joy.h'
C
      character*(*) CHAR(MAXRES), CHARNW(MAXRES)
      character*24 AA1
      character*3 AA3(24), NAM3
      integer I, J, NRES
      logical FOUND, REPORT
C
      data AA1/'SGVTALDIPKQNFYERCHWMBZXJ'/
      data AA3/'SER','GLY','VAL','THR','ALA',
     -         'LEU','ASP','ILE','PRO','LYS',
     -         'GLN','ASN','PHE','TYR','GLU',
     -         'ARG','CYS','HIS','TRP','MET',
     -         'ASX','GLX','UNK','CYH'/
C
      REPORT=.false.
      do I=1,NRES
        NAM3=CHAR(I)
        FOUND=.FALSE.
        do J=1,24
          if (NAM3.eq.AA3(J)) then
            CHARNW(I)=AA1(J:J)
            FOUND=.TRUE.
            goto 2
          endif
        enddo
2       continue
        if (.not.FOUND) then
          CHARNW(I)='X'
          if (REPORT) then
            call ERROR('joy: unrecognized residue, cannot convert',NAM3,0)
          endif
        endif
      enddo
C
      return
      end
