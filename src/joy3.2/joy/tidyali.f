      subroutine TIDYALI(SEQUENCE,LENSEQ)
C
C copyright (c) 1988-1991 John Overington
C part of joy
C
C ---------------------------------------------------------------------------
C 
C To remove the insertion characters (-) that are present at the N- and 
C C-termini of an sequence
C
       include 'joy.h'
C
C ---------------------------------------------------------------------------
C
      character*1 SEQUENCE(MAXRES)
      logical FOUND
      integer K, LENSEQ
C
C ---------------------------------------------------------------------------
C
C First process the N-terminus
C
      do K=1,LENSEQ,1
        if (ISPUNCT(SEQUENCE(K))) then
          SEQUENCE(K)=' '
        else
          goto 1 
        end if
      end do
1     continue
C
C Now process the C-terminus
C
      do K=LENSEQ,1,-1
        if (ISPUNCT(SEQUENCE(K))) then
          SEQUENCE(K)=' '
        else
          goto 2
        end if
      end do
2     continue
C
      return
      end
