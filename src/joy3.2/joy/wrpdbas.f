cf ----------------------------------------------------------------------------
cf
cf    WRPDBAS() writes out the atom file in the PDB format.
cf
cf    subroutine wrpdbas(ioout,fname,x,y,z,natm,nres,resnam,
cf   -                 resnum,atmnam,iresatm,chain)
cf
cf ----------------------------------------------------------------------------

      subroutine wrpdbas(ioout,fname,x,y,z,natm,nres,resnam,
     &                   resnum,atmnam,iresatm,chain)
        implicit none

        integer natm,nres,iresatm(natm),ioout,i
        real x(natm), y(natm), z(natm)
        character resnam(nres)*(*),resnum(nres)*(*),chain(nres)*(*)
        character atmnam(natm)*(*),fname*(*)

        call openf(ioout, fname, 'unknown')
        do  i = 1, natm
          write(ioout,15) i,atmnam(i),resnam(iresatm(i)),
     &          chain(iresatm(i)),resnum(iresatm(i)),x(i),y(i),z(i),
     &          0.0, 0.0, 'MAIN'
15        format('ATOM  ',i5,1x,a4,1x,a3,1x,a1,a5,3x,3f8.3,2f6.2,6x,a)
        end do
        close(ioout)

        write(*,'(a,2i5)')'wrpdbas____> residues, atoms: ', nres,natm

        return
      end
