      subroutine WRPRN(STRNAM,STRSEQ,LENGTH,NSTR,NSEQ,SEQ,SEQNAM,
     +                 LINELEN)
C
C copyright (c) 1988-1991 John Overington
C part of joy
C
C Writes out a simple representaion of an alignment produced by joy
C removed rule from output
C
      include 'joy.h'
C
      character*(*) STRNAM(MAXSEQ), SEQNAM(MAXNEWSEQ)
      character*1 STRSEQ(MAXRES,MAXSEQ), SEQ(MAXRES,MAXSEQ)
      integer M, J, N, NSTR, NSEQ, K, LENGTH, NTIMES, LINELEN, NEND
C
C funny trap of out of format space
C
      if (LINELEN.gt.100) then
        call error('joy: truncated lines in output, check format',' ',0)
      end if
C
C these otherwise latex will do funny things with the page.
C NTIMES is the number of LINELEN long blocks
C NLINES is the number of lines per page
C NPERPG is the number of blocks of the alignment possible per page
C
      if (MOD(LENGTH,LINELEN).eq.0) then
        NTIMES=(LENGTH/LINELEN)
      else
        NTIMES=(LENGTH/LINELEN)+1
      end if
C
      write (STDOUT,'('' '')')
      do M=1,NTIMES
        N=1+((M-1)*LINELEN)
        if ((N+LINELEN-1).gt.LENGTH) then
          NEND=LENGTH
        else
          NEND=N+LINELEN-1
        end if
        do K=1,NSTR
          write (STDOUT,'(a,1x,100a)') STRNAM(K),(STRSEQ(J,K),J=N,NEND)
        end do
        do K=1,NSEQ
          write (STDOUT,'(a,1x,100a)') SEQNAM(K),(SEQ(J,K),J=N,NEND)
        end do
        write (STDOUT,'('' '')')
      end do
C
      return
      end
