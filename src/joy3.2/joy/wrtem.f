      subroutine WRTEM(FILE,NSTR,SEQ,STRNAM,LENFIL,LEN,
     -                 ALPHA,THREE10,PIHEL,BETA,ACCESS,POSPHI,
     -                 SCHBO,SCHBN,SSHB,CIS,
     -                 SHETHB,SSBOND,MMNBOND,MMOBOND,RELACC,OOI)
C
C copyright (c) 1988-1991 John Overington
C part of joy
C
C Writes out the original alignment along with the logical matrices defined by
C reading of the data files and several flags to control further processing
C
C Added several more classes to the output, mainchain to mainchain hydrogen
C bonding (should really be split into two classes NH and CO), the DSSP state
C of the residue and the positive phi state of the residue, note that these
C last two classes are merged in the older secondary structure class (second in
C output)
C
C Changed format of file to pseudo PIR for GASMUS
C
C Indexes of features is:
C 1) Sequence
C 2) The secondary structural state (helix=H, strand=E, coil=C)
C 3) The accessibility (accessible == TRUE)
C 4) The hydrogen bonds to mainchain O
C 5) The hydrogen bonds to mainchain N
C 6) The hydrogen bonds to other sidechains
C 7) cis-peptides
C 8) sidechain to heterogen H-bonds
C 9) Disulphides
C 10) mainchain to mainchain hydrogen bonding (from N)
C 11) mainchain to mainchain hydrogen bonding (from O)
C 12) The DSSP state (helix=H, strand=E, coil=C)
C 13) The positive phi state state (helix=H, strand=E, coil=C)
C 14) The relative accessibility (0-f)
C 15) The 14A Ooi number (0-f)
C
      include 'joy.h'
C
      character*(MAXRES) BUFFER
      character*(*) FILE
      character*(*) STRNAM(MAXSEQ)
      character*10 BUFF1(MAXSEQ)
      character*1 SEQ(MAXRES,MAXSEQ)
      real RELACC(MAXRES,MAXSEQ)
      integer NSTR, LENFIL(MAXSEQ), LEN,I, J, K, M, N
      integer OOI(MAXRES,MAXSEQ)
      logical POSPHI(MAXRES,MAXSEQ), ALPHA(MAXRES,MAXSEQ)
      logical ACCESS(MAXRES,MAXSEQ), SCHBO(MAXRES,MAXSEQ)
      logical BETA(MAXRES,MAXSEQ), SCHBN(MAXRES,MAXSEQ)
      logical SSHB(MAXRES,MAXSEQ), CIS(MAXRES,MAXSEQ)
      logical SHETHB(MAXRES,MAXSEQ)
      logical SSBOND(MAXRES,MAXSEQ), MMNBOND(MAXRES,MAXSEQ)
      logical MMOBOND(MAXRES,MAXSEQ), THREE10(MAXRES,MAXSEQ)
      logical PIHEL(MAXRES,MAXSEQ)
C
      open (unit=11,file=FILE,status='UNKNOWN',form='FORMATTED',
     -      recl=1024,err=101)
C
      do I=1,NSTR
        BUFF1(I)='          '
      end do
      do I=1,NSTR
        do J=1,LENFIL(I)
          BUFF1(I)(J:J)=STRNAM(I)(J:J)
        end do
      end do
C
C 1) Sequence
C
      do I=1,NSTR
        do J=1,MAXRES
          BUFFER(J:J)=' '
        end do
        do J=1,LEN
          SEQ(J,I)=TOUPPER(SEQ(J,I))
          if (SEQ(J,I).eq.' ') then
            BUFFER(J:J)='-'
          else
            BUFFER(J:J)=SEQ(J,I)
          end if
        end do
        write (11,'(''>P1;'',A)',err=943) BUFF1(I)(1:LASTCHAR(BUFF1))
        write (11,'(''sequence'')')
        N=1
        do K=1,(LEN/75)
          write (11,'(A)') BUFFER(N:N+74)
          N=N+75
        end do
        write (11,'(A)') BUFFER(N:LEN)//'*'
      end do
C
C 2) The secondary structural state (helix=H, strand=E, coil=C)
C
      do I=1,NSTR
        M=0
        do J=1,LEN
          if ((SEQ(J,I).ne.' ').and.(SEQ(J,I).ne.'-')) then
            M=M+1
            if (POSPHI(M,I)) then
              BUFFER(J:J)='P'
            else if (ALPHA(M,I)) then
              BUFFER(J:J)='H'
            else if (BETA(M,I)) then
              BUFFER(J:J)='E'
            else  
              BUFFER(J:J)='C'
            end if
          else if (SEQ(J,I).eq.' ') then
            BUFFER(J:J)='-'
          else
            BUFFER(J:J)=SEQ(J,I)
          end if
        end do
        write (11,'(''>P1;'',A)') BUFF1(I)(1:LASTCHAR(BUFF1))
        write (11,'(''secondary structure and phi angle'')')
        N=1
        do K=1,(LEN/75)
          write (11,'(A)') BUFFER(N:N+74)
          N=N+75
        end do
        write (11,'(A)') BUFFER(N:LEN)//'*'
      end do
C
C 3) The accessibility (accessible == TRUE)
C
      do I=1,NSTR
        M=0
        do J=1,LEN
          if ((SEQ(J,I).ne.' ').and.(SEQ(J,I).ne.'-')) then
            M=M+1
            if (ACCESS(M,I)) then
              BUFFER(J:J)='T'
            else 
              BUFFER(J:J)='F'
            end if
          else if (SEQ(J,I).eq.' ') then
            BUFFER(J:J)='-'
          else
            BUFFER(J:J)=SEQ(J,I)
          end if
        end do
        write (11,'(''>P1;'',A)') BUFF1(I)(1:LASTCHAR(BUFF1))
        write (11,'(''solvent accessibility'')')
        N=1
        do K=1,(LEN/75)
          write (11,'(A)') BUFFER(N:N+74)
          N=N+75
        end do
        write (11,'(A)') BUFFER(N:LEN)//'*'
      end do
C
C 4) The hydrogen bonds to mainchain O
C
      do I=1,NSTR
        M=0
        do J=1,LEN
          if ((SEQ(J,I).ne.' ').and.(SEQ(J,I).ne.'-')) then
            M=M+1
            if (SCHBO(M,I)) then
              BUFFER(J:J)='T'
            else 
              BUFFER(J:J)='F'
            end if
          else if (SEQ(J,I).eq.' ') then
            BUFFER(J:J)='-'
          else
            BUFFER(J:J)=SEQ(J,I)
          end if
        end do
        write (11,'(''>P1;'',A)') BUFF1(I)(1:LASTCHAR(BUFF1))
        write (11,'(''hydrogen bond to mainchain CO'')')
        N=1
        do K=1,(LEN/75)
          write (11,'(A)') BUFFER(N:N+74)
          N=N+75
        end do
        write (11,'(A)') BUFFER(N:LEN)//'*'
      end do
C
C 5) The hydrogen bonds to mainchain N
C
      do I=1,NSTR
        M=0
        do J=1,LEN
          if ((SEQ(J,I).ne.' ').and.(SEQ(J,I).ne.'-')) then
            M=M+1
            if (SCHBN(M,I)) then
              BUFFER(J:J)='T'
            else 
              BUFFER(J:J)='F'
            end if
          else if (SEQ(J,I).eq.' ') then
            BUFFER(J:J)='-'
          else
            BUFFER(J:J)=SEQ(J,I)
          end if
        end do
        write (11,'(''>P1;'',A)') BUFF1(I)(1:LASTCHAR(BUFF1))
        write (11,'(''hydrogen bond to mainchain NH'')')
        N=1
        do K=1,(LEN/75)
          write (11,'(A)') BUFFER(N:N+74)
          N=N+75
        end do
        write (11,'(A)') BUFFER(N:LEN)//'*'
      end do
C
C 6) The hydrogen bonds to other sidechains
C
      do I=1,NSTR
        M=0
        do J=1,LEN
          if ((SEQ(J,I).ne.' ').and.(SEQ(J,I).ne.'-')) then
            M=M+1
            if (SSHB(M,I)) then
              BUFFER(J:J)='T'
            else 
              BUFFER(J:J)='F'
            end if
          else if (SEQ(J,I).eq.' ') then
            BUFFER(J:J)='-'
          else
            BUFFER(J:J)=SEQ(J,I)
          end if
        end do
        write (11,'(''>P1;'',A)') BUFF1(I)(1:LASTCHAR(BUFF1))
        write (11,'(''hydrogen bond to other sidechain/heterogen'')')
        N=1
        do K=1,(LEN/75)
          write (11,'(A)') BUFFER(N:N+74)
          N=N+75
        end do
        write (11,'(A)') BUFFER(N:LEN)//'*'
      end do
C
C 7) cis-peptides
C
      do I=1,NSTR
        M=0
        do J=1,LEN
          if ((SEQ(J,I).ne.' ').and.(SEQ(J,I).ne.'-')) then
            M=M+1
            if (cis(M,I)) then
              BUFFER(J:J)='T'
            else 
              BUFFER(J:J)='F'
            end if
          else if (SEQ(J,I).eq.' ') then
            BUFFER(J:J)='-'
          else
            BUFFER(J:J)=SEQ(J,I)
          end if
        end do
        write (11,'(''>P1;'',A)') BUFF1(I)(1:LASTCHAR(BUFF1))
        write (11,'(''cis-peptide bond'')')
        N=1
        do K=1,(LEN/75)
          write (11,'(A)') BUFFER(N:N+74)
          N=N+75
        end do
        write (11,'(A)') BUFFER(N:LEN)//'*'
      end do
C
C 8) sidechain to heterogen H-bonds
C
      do I=1,NSTR
        M=0
        do J=1,LEN
          if ((SEQ(J,I).ne.' ').and.(SEQ(J,I).ne.'-')) then
            M=M+1
            if (SHETHB(M,I)) then
              BUFFER(J:J)='T'
            else 
              BUFFER(J:J)='F'
            end if
          else if (SEQ(J,I).eq.' ') then
            BUFFER(J:J)='-'
          else
            BUFFER(J:J)=SEQ(J,I)
          end if
        end do
        write (11,'(''>P1;'',A)') BUFF1(I)(1:LASTCHAR(BUFF1))
        write (11,'(''hydrogen bond to heterogen'')')
        N=1
        do K=1,(LEN/75)
          write (11,'(A)') BUFFER(N:N+74)
          N=N+75
        end do
        write (11,'(A)') BUFFER(N:LEN)//'*'
      end do
C
C 9) Disulphides
C
      do I=1,NSTR
        M=0
        do J=1,LEN
          if ((SEQ(J,I).ne.' ').and.(SEQ(J,I).ne.'-')) then
            M=M+1
            if (SSBOND(M,I)) then
              BUFFER(J:J)='T'
            else 
              BUFFER(J:J)='F'
            end if
          else if (SEQ(J,I).eq.' ') then
            BUFFER(J:J)='-'
          else
            BUFFER(J:J)=SEQ(J,I)
          end if
        end do
        write (11,'(''>P1;'',A)') BUFF1(I)(1:LASTCHAR(BUFF1))
        write (11,'(''disulphide'')')
        N=1
        do K=1,(LEN/75)
          write (11,'(A)') BUFFER(N:N+74)
          N=N+75
        end do
        write (11,'(A)') BUFFER(N:LEN)//'*'
      end do
C
C 10) mainchain to mainchain hydrogen bonding (from N)
C
      do I=1,NSTR
        M=0
        do J=1,LEN
          if ((SEQ(J,I).ne.' ').and.(SEQ(J,I).ne.'-')) then
            M=M+1
            if (MMNBOND(M,I)) then
              BUFFER(J:J)='T'
            else 
              BUFFER(J:J)='F'
            end if
          else if (SEQ(J,I).eq.' ') then
            BUFFER(J:J)='-'
          else
            BUFFER(J:J)=SEQ(J,I)
          end if
        end do
        write (11,'(''>P1;'',A)') BUFF1(I)(1:LASTCHAR(BUFF1))
        write (11,'(''mainchain to mainchain hydrogen bonds (amide)'')')
        N=1
        do K=1,(LEN/75)
          write (11,'(A)') BUFFER(N:N+74)
          N=N+75
        end do
        write (11,'(A)') BUFFER(N:LEN)//'*'
      end do
C
C 11) mainchain to mainchain hydrogen bonding (from O)
C
      do I=1,NSTR
        M=0
        do J=1,LEN
          if ((SEQ(J,I).ne.' ').and.(SEQ(J,I).ne.'-')) then
            M=M+1
            if (MMOBOND(M,I)) then
              BUFFER(J:J)='T'
            else 
              BUFFER(J:J)='F'
            end if
          else if (SEQ(J,I).eq.' ') then
            BUFFER(J:J)='-'
          else
            BUFFER(J:J)=SEQ(J,I)
          end if
        end do
        write (11,'(''>P1;'',A)') BUFF1(I)(1:LASTCHAR(BUFF1))
        write (11,
     +        '(''Mainchain to mainchain hydrogen bonds (carbonyl)'')')
        N=1
        do K=1,(LEN/75)
          write (11,'(A)') BUFFER(N:N+74)
          N=N+75
        end do
        write (11,'(A)') BUFFER(N:LEN)//'*'
      end do
C
C 12) The DSSP state (a-helix=H, 3-10-helix=G, strand=E, coil=C)
C
      do I=1,NSTR
        M=0
        do J=1,LEN
          if ((SEQ(J,I).ne.' ').and.(SEQ(J,I).ne.'-')) then
            M=M+1
            if (THREE10(M,I)) then
              BUFFER(J:J)='G'
            else if (PIHEL(M,I)) then
              BUFFER(J:J)='I'
            else if (ALPHA(M,I)) then
              BUFFER(J:J)='H'
            else if (BETA(M,I)) then
              BUFFER(J:J)='E'
            else  
              BUFFER(J:J)='C'
            end if
          else if (SEQ(J,I).eq.' ') then
            BUFFER(J:J)='-'
          else
            BUFFER(J:J)=SEQ(J,I)
          end if
        end do
        write (11,'(''>P1;'',A)') BUFF1(I)(1:LASTCHAR(BUFF1))
        write (11,'(''DSSP'')')
        N=1
        do K=1,(LEN/75)
          write (11,'(A)') BUFFER(N:N+74)
          N=N+75
        end do
        write (11,'(A)') BUFFER(N:LEN)//'*'
      end do
C
C 13) The positive phi state state (helix=H, strand=E, coil=C)
C
      do I=1,NSTR
        M=0
        do J=1,LEN
          if ((SEQ(J,I).ne.' ').and.(SEQ(J,I).ne.'-')) then
            M=M+1
            if (POSPHI(M,I)) then
              BUFFER(J:J)='T'
            else
              BUFFER(J:J)='F'
            end if
          else if (SEQ(J,I).eq.' ') then
            BUFFER(J:J)='-'
          else
            BUFFER(J:J)=SEQ(J,I)
          end if
        end do
        write (11,'(''>P1;'',A)') BUFF1(I)(1:LASTCHAR(BUFF1))
        write (11,'(''positive phi angle'')')
        N=1
        do K=1,(LEN/75)
          write (11,'(A)') BUFFER(N:N+74)
          N=N+75
        end do
        write (11,'(A)') BUFFER(N:LEN)//'*'
      end do
C
C 14) The relative accessibility
C
      do I=1,NSTR
        M=0
        do J=1,LEN
          if ((SEQ(J,I).ne.' ').and.(SEQ(J,I).ne.'-')) then
            M=M+1
            BUFFER(J:J)=REALHEX(RELACC(M,I))
          else if (SEQ(J,I).eq.' ') then
            BUFFER(J:J)='-'
          else
            BUFFER(J:J)=SEQ(J,I)
          end if
        end do
        write (11,'(''>P1;'',A)') BUFF1(I)(1:LASTCHAR(BUFF1))
        write (11,'(''percentage accessibility'')')
        N=1
        do K=1,(LEN/75)
          write (11,'(A)') BUFFER(N:N+74)
          N=N+75
        end do
        write (11,'(A)') BUFFER(N:LEN)//'*'
      end do
C
C 15) The Ooi number
C
      do I=1,NSTR
        M=0
        do J=1,LEN
          if ((SEQ(J,I).ne.' ').and.(SEQ(J,I).ne.'-')) then
            M=M+1
            BUFFER(J:J)=INTHEX(OOI(M,I))
          else if (SEQ(J,I).eq.' ') then
            BUFFER(J:J)='-'
          else
            BUFFER(J:J)=SEQ(J,I)
          end if
        end do
        write (11,'(''>P1;'',A)') BUFF1(I)(1:LASTCHAR(BUFF1))
        write (11,'(''Ooi number'')')
        N=1
        do K=1,(LEN/75)
          write (11,'(A)') BUFFER(N:N+74)
          N=N+75
        end do
        write (11,'(A)') BUFFER(N:LEN)//'*'
      end do
C
      close (unit=11)
C
      return
C
101   call ERROR('joy: error opening file',FILE,3)
943   call ERROR('joy: error writing template file',FILE,0)
C
      end
