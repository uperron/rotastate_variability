      subroutine WRTEX(FILE,RAWSEQ,FAMILY,SHEET,RDISC,LENGTH,NSEQ,
     +           INFILE,
     +           FILNAM,NUMNEWSEQ,NEWSEQ,TITNEWSEQ,LABEL,TEXT,ORDER,
     +           NUMENT,AUTONUM,AUTOSEC,AUTOLABEL,ALLLABEL,SWAPLBL,
     +           USENUMFILE,NUMFILE,NNUMFILE,LBL,NLBL,RESLBL,AUTOCONS,
     +           ALLSEC,DOKEY,AUTOACC,DOEFI,EFITYP,LANDSCAPE,HELVETICA,
     +           ONEPERPAGE,KITSCHORDER,INDSTR)
C
C
C copyright (c) 1988-1991 John Overington
C part of joy
C
C Processes and outputs alignment in either latex format, ordering 
C of output lines is controlled by the CHAR array ORDER, features must match 
C this to be output. Added option to number alignment without having to label 
C the alignment file. Added code to allow output of consensus secondary 
C structure assignments, as a working definition of this used 70% as an 
C arbitrary cutoff for a secondary structure definition to apply at all 
C positions in the alignment. Will modify this later I'm sure. The position 
C of the secondary structure line is immediately after the final featured 
C sequence, (Not user controllable). Simplified code so that instead of all 
C the TEXT and LABEL variable information there are only two arrays. Also the 
C messy arrangement of the non-obvious ordering of the label and text 
C information. Deleted use of logicals for environment definitions and 
C cONsolidated these into the RDISC vector. Removed need for length of 
C sequence titles.
C
C Sample correct nesting for LATEX for all features:
C \rule[-0.8mm]{0mm}{4.0mm}\centering \pmb{\underline{\it \~{d}}} &
C
C Changed structure so can do both types of typesetter
C
C Set up cis formatting, need to decide what to use for this though
C this is done with a crescent shaped moon above the residue e.g. \u{P}
C
C Set up disulphide formatting will use cedilla character for this.
C this is done with \c{c} or \c{C}
C
C Set up hetatom H-bond signal (may be a bit buggy due to a reassignment in
C READHBD. This should only affect those residues that are bonded to a heterogen
C but are not bonded to another sidechain. Residues thus classified will be
C labelled as if they have a sidechain to sidechain H-bond.
C This is done with a dot under the residue, do this with \d{s}
C
C Added AUTOLABEL and user defined label options, moved WRPRN to a separate 
C subroutine
C
C If AUTONUM and AUTOLABEL are true then one will have two lines of numbering
C decided to assign AUTONUM false if AUTOLABEL is true. Found a bug in AUTONUM
C routines prints 101, 102 etc, will have to check that char after zero is not
C a number.
C
C Removed all the stuff for troff, latex now can do everything, and it
C is available on the PC
C
C Added output of conserved buried positions (shown by bullets under column)
C
C Added optional output of secondary structure for all structures (ALLSEC)
C
C Added output of efimov state of residue, fixed bug in output of these.
C
C Chenged output of consensus secondary structure, now true DSSP.
C turn off bullets in alignments by default, flag now toggles display of
C these
C
C It turns out that LaTeX math mode is far more flexible than normal text
C mode (in terms of adding features to letters) Therefore can fix dot bug
C and bug to do with too many features in math mode
C
C have to remember the follwing things
C 1) math mode default is italic (use \rm and \it)
C 2) surround mode with $$
C 3) syntax for features changed
C
C   \hat{a}
C   \check{a}
C   \breve{a}
C   \acute{a}
C   \grave{a}
C   \tilde{a}
C   \bar{a}
C   \vec{a}
C   \dot{a}
C   \ddot{a}
C
C 4) overline with \overline{a}
C 5) underline with \underline{a}
C 6) will need to check compatibility with pmb macro
C 7) will need to check if ok with C-cedilla (no cedilla in math mode)
C    => handle separately. Can nest math mode within \c{} thus
C    \c{$\rm \tilde{c}$}, messy though.
C
C  N.B. But haven't done this yet
C
C Now read in swanky order from kitsch (with -O option). So to get this to work
C will have to change ordering etc. This is not as easy as it first seemed.
C Will in fact use a pointer type mechanism for this. (INDSTR array)
C
C This is not working yet, so do not use it !!
C
C ---------------------------------------------------------------------------
C
C LENBUFFER length of internal buffer
C
      include 'joy.h'
C
      integer LENBUFFER
      parameter (LENBUFFER=30)
C
C ----------------------------------------------------------------------------
C
      character*(LENBUFFER) BUFFER(MAXRES,MAXSEQ)
      character*(*) FAMILY, INFILE
      character*(*) FILE, NUMFILE(MAXSEQ)
      character*80 TRANSLATED
      character*25 RULE
      character*21 RES
      character*12 SCRIPT
      character*11 UNDERLINE, CEN
      character*10 FILNAM(MAXSEQ), LFILNAM(MAXSEQ), TITNEWSEQ(MAXNEWSEQ)
      character*6 ORDER(MAXSEQ+MAXNEWSEQ+MAXTEXT+MAXLAB)
      character*5 PMB, ITALIC, BOLD
      character*5 RESLBL(MAXRES,MAXSEQ), NUMLBL(MAXRES,MAXSEQ)
      character*5 LBL(MAXRES,MAXSEQ), LABEL(MAXRES,MAXLAB)
      character*3 TOP, CED, MOON, DOT, BLANK
      character*1 RAWSEQ(MAXRES,MAXSEQ), NEWSEQ(MAXRES,MAXNEWSEQ)
      character*1 TEXT(MAXRES,MAXTEXT), RTBRC, SEP, SHEET(MAXRES,MAXSEQ)
      character*1 SHEET2(MAXRES,MAXSEQ)
      integer CONSEN, CONSENTMP, NLASC, IJK
      integer RDISC(NUMFEAT,MAXRES,MAXSEQ)
      integer NLBL(MAXSEQ)
      integer SEQTYP(MAXSEQ,MAXRES), SECTYP(MAXSEQ,MAXRES)
      integer ACCTYP(MAXSEQ,MAXRES), SECTYP2(MAXRES,MAXSEQ)
      integer NLABEL, NTEXT, NNSEQ, NNEWSEQ, NENT, NUMNEWSEQ
      integer NTIMES, NUMENT, LENGTH, NSEQ
      integer I, J, K, L, M, N, O, KK, MM, NN
      integer NNUMFILE, LENBLOCK
      integer EFITYP(MAXRES,MAXSEQ), EFITYP2(MAXRES,MAXSEQ)
      integer NSPACE
      logical AUTOLABEL(MAXSEQ), USENUMFILE(MAXSEQ)
      logical AUTONUM, AUTOSEC, ALLLABEL, SWAPLBL, AUTOACC
      logical AUTOCONS, DOKEY, ALLSEC, DOCONS, DOEFI, DOSHEET
      logical DOINSCODE, LANDSCAPE, ONEPERPAGE, HELVETICA
      logical PRFAMNAME, KITSCHORDER
      logical CENTERFAM
      integer INDSTR(MAXSEQ), NJPO
      real CUTOFF
C
C ----------------------------------------------------------------------------
C
C Following are for latex
C
C original rule - vary this to get exactly lined up blocks of alignments
C the present one seems to work
C original RULE /'\rule[-0.8mm]{0mm}{4.0mm}'/
C
      data RULE       /'\\rule[-1.2mm]{0mm}{4.4mm}'/
      data SCRIPT     /'\\scriptsize '/
      data CEN        /'\\centering '/
      data UNDERLINE  /'\\underline{'/
      data PMB        /'\\pmb{'/
      data ITALIC     /'{\\it '/
      data BOLD       /'{\\bf '/
      data TOP        /'\\~{'/
      data CED        /'\\c{'/
      data DOT        /'\\d{'/
      data MOON       /'\\u{'/
      data BLANK      /'   '/
      data RTBRC      /'}'/
      data SEP        /'&'/
C
C DOINSCODE controls whether insertion codes are automatically generated
C
      data DOINSCODE /.true./
C
      data RES /'ACDEFGHIKLMNPQRSTVWYC'/
C
C CUTOFF controls decision of a secondary structural or accessibility class
C
      data CUTOFF /0.7/
C
C DOSHEET controls whether output of sheet information is done or not
C (should be an option)
C
      data DOSHEET /.false./
C
C PRFAMNAME controls whether family name is printed out at top of alignment
C CENTERFAM controls whether this is centered or not
C
      data PRFAMNAME /.true./
      data CENTERFAM /.true./
C
C ----------------------------------------------------------------------------
C
C LENBLOCK is the length of each alignment block, 50 for portrait and
C 80 for landscape
C
      if (LANDSCAPE) then
	LENBLOCK=80
      else
	LENBLOCK=50
      end if
C
C If output invariant residues then also output conserved residues
C
      DOCONS=AUTOCONS
C
C Give structure labels priority over alignment labels, if ANY
C structure labels exist then the alignment numbering will be turned
C off. (NB further controlled by ALLLABEL)
C
      if (.not.ALLLABEL) then
	do I=1,NSEQ
	  if ((AUTOLABEL(I).or.USENUMFILE(I)).and.AUTONUM) then
	    AUTONUM=.false.
	  end if
	end do
      end if
C
C ----------------------------------------------------------------------------
C
      do I=1,MAXSEQ
	do J=1,MAXRES
	  NUMLBL(J,I)='     '
	  do K=1,LENBUFFER
	    BUFFER(J,I)(K:K)=' '
	  end do
	end do
      end do
C
C ----------------------------------------------------------------------------
C
C Output the sequences in blocks of 50 records, must be careful calculating 
C these otherwise latex will do funny things with the page.
C NTIMES is the number of LENBLOCK long blocks
C
      if (MOD(LENGTH,LENBLOCK).eq.0) then
	NTIMES=(LENGTH/LENBLOCK)
      else
	NTIMES=(LENGTH/LENBLOCK)+1
      end if
C
C ------------------------- PROCESSING OF SEQUENCES  -------------------------
C
      open (unit=1,file=FILE,status='UNKNOWN',form='FORMATTED',err=999)
C
C Will need to think very carefully about the INDSTR stuff here
C
C fill a dummy INDSTR array if not KITSCHORDER not wanted
C
      if ( .not. KITSCHORDER) then
	do I=1,NSEQ
	  INDSTR(I)=I
	end do
      end if
c
c     do i =1,nseq
c       write (6,*) indstr(i),i
c     end do
C
      do I=1,NSEQ
	KK=1
	IJK=1                                   ! init ins code
	NJPO=INDSTR(I)                          ! local copy of index
c       write (6,*) Nseq, ' nseq ', njpo , ' njpo'
	LFILNAM(I)=FILNAM(NJPO)
C
	do J=1,LENGTH
	  NN=1
	  if (RAWSEQ(J,NJPO) .eq. '-' .or. 
     +        RAWSEQ(J,NJPO) .eq. ' ' .or.
     +        RAWSEQ(J,NJPO) .eq. '/') then
C
C hack the chain break code to the translation to ' '
C
	    if (RAWSEQ(J,NJPO) .eq. '/') then
	      RAWSEQ(J,NJPO)=' '
	    end if
C
	    write (BUFFER(J,I)(NN:NN),'(A)') RAWSEQ(J,I)
C
	    SECTYP2(J,I)=4
	    ACCTYP(I,J)=2
	    SEQTYP(I,J)=22
	    EFITYP2(J,I)=8              ! use undefinable (not bad)
	    SHEET2(J,I)=' '
C
C If want to auto label a structure I
C
	    if (AUTOLABEL(NJPO)) then
	      if (KK.gt.1) then         ! if not first residue
		if (DOINSCODE) then             ! only do ins code if true
		  NUMLBL(J,I)=char(IJK+64)      ! write out ins code
		end if
		IJK=IJK+1               ! increment insertion code 
	      end if
	    end if
	  else
	    IJK=1                       ! reset insertion numberer
C
C Reset J to C for output
C
	    if (RAWSEQ(J,NJPO).eq.'J') then
	      RAWSEQ(J,NJPO)='C'
	    end if
C
C convert accessible to lower case
C
	    if (RDISC(3,KK,NJPO).eq.1) then
	      RAWSEQ(J,NJPO)=CHAR(ICHAR(RAWSEQ(J,I))+32)
	    end if
C
C set sheet info
C
	    SHEET2(J,I)=SHEET(KK,I)
C
C ------------------------------------------------------------------------
C
C Set up the labelling strings
C
C AUTOLABEL means that should number every tenth residue
C USENUMFILE means that a .lbl file was read for that structure
C
	    if (AUTOLABEL(NJPO)) then
	      do L=1,NNUMFILE
		if (NUMFILE(L)(1:10) .eq. FILNAM(NJPO)(1:10)) then
		  NLASC=(LASTCHAR(RESLBL(KK,NJPO)))
		  if (RESLBL(KK,NJPO)(NLASC:NLASC) .eq. '0') then
		    NUMLBL(J,I)=RESLBL(KK,NJPO)
		  else if (ISUPPER(RESLBL(KK,NJPO)(NLASC:NLASC))) then
		    NUMLBL(J,I)(1:1)=RESLBL(KK,NJPO)(NLASC:NLASC)
		  end if
		end if
	      end do
	    else if (USENUMFILE(NJPO)) then
	      do L=1,NNUMFILE
		if (NUMFILE(L)(1:10) .eq. FILNAM(NJPO)(1:10)) then
		  do O=1,NLBL(NJPO)
		    if (RESLBL(KK,NJPO) .eq. LBL(O,NJPO)) then
		      NUMLBL(J,I)=LBL(O,NJPO)
		    end if
		  end do
		end if
	      end do
	    end if
c        write (6,*) 'at 3'
C
C -------------------------------------------------------------------------
C
C Assign the relavent secondary structural parameters, cis-peptide overides
C all other classes, at least for display
C
C If want to fix to true DSSP type output, then fix this here
C old type was to let phi overide, but this is not too nice, since it
C obscured the extent of helices and strands, made visual identification of
C cap residues difficult, and was inconsistent with the notion of simplicity
C as positive phi angles were amongst the most obvious in the alignment
C anyway. Old RDISC index was 2, true DSSP is 10. Also made change that
C positive phi is replaced by three-ten type helices. Later removed omega
C as printed symbol for cis-peptide positions (was feature 8).
C
C Now 1 is alpha, 2 is beta, 3 is 3-10, 4 is coil, 5 is pi
C
	    if (RDISC(10,KK,NJPO) .eq. 1) then
	      SECTYP2(J,I)=1
	    else if (RDISC(10,KK,NJPO) .eq. 2) then
	      SECTYP2(J,I)=2
	    else if (RDISC(10,KK,NJPO) .eq. 3) then 
	      SECTYP2(J,I)=3
	    else if (RDISC(10,KK,NJPO) .eq. 4) then 
	      SECTYP2(J,I)=4
	    else if (RDISC(10,KK,NJPO) .eq. 5) then 
	      SECTYP2(J,I)=5
	    end if
C
C ------------------------------------------------------------------------
C
C Fix Efimov assignments so at correct place in alignment
C
	    EFITYP2(J,I)=EFITYP(KK,I)
C
C -------------------------------------------------------------------------
C
C Assign sequence array, confusing switch of sense of index!
C
	      if (RDISC(3,KK,NJPO) .eq. 2) then
		ACCTYP(I,J)=1
	      end if
C
C Assign sequence column array
C
	      if (AUTOCONS) then
		SEQTYP(I,J)=RDISC(1,KK,NJPO)
	      end if
C
C Prepare the BUFFER strings for LATEX. If the residue is both positive phi
C and hydrogen bonded to a mainchain nitrogen, then need to handle this
C specially.
C
	      if ((RDISC(2,KK,NJPO) .eq. 3) .and. 
     +            (RDISC(6,KK,NJPO) .eq. 1)) then
		write (BUFFER(J,I)(NN:NN+4),'(A)') PMB
		NN=NN+5
	      end if
	      if (RDISC(5,KK,NJPO) .eq. 1) then
		write (BUFFER(J,I)(NN:NN+10),'(A)') UNDERLINE
		NN=NN+11
	      end if
	      if (RDISC(2,KK,NJPO) .eq. 3) then
		write (BUFFER(J,I)(NN:NN+4),'(A)') ITALIC
		NN=NN+5
	      end if
	      if ((RDISC(6,KK,NJPO) .eq. 1) .and.
     +            (RDISC(2,KK,NJPO) .ne. 3)) then
		write (BUFFER(J,I)(NN:NN+4),'(A)') BOLD
		NN=NN+5
	      end if
	      if (RDISC(4,KK,NJPO) .eq. 1) then
		write (BUFFER(J,I)(NN:NN+2),'(A)') TOP
		NN=NN+3
	      end if
	      if (RDISC(8,KK,NJPO) .eq. 1) then
		write (BUFFER(J,I)(NN:NN+2),'(A)') MOON
		NN=NN+3
	      end if
	      if (RDISC(9,KK,NJPO) .eq. 1) then
		write (BUFFER(J,I)(NN:NN+2),'(A)') CED
		NN=NN+3
	      end if
C
C Write out the residue to the BUFFER
C
	      write (BUFFER(J,I)(NN:NN),'(A)') RAWSEQ(J,NJPO)
	      NN=NN+1
	      N=N+1
C
C Tidy up LATEX syntax, check for exceeded BUFFER length
C 
C Put right brace character for RDISC(4,5,6,7,8,9,) = 1
C
	      if (RDISC(2,KK,NJPO) .eq. 3) then
		write (BUFFER(J,I)(NN:NN),'(A)') RTBRC
		NN=NN+1
		if (NN.gt.LENBUFFER) then
		  call ERROR('joy: buffer overflow','(1)',3)
		end if
	      end if
	      do MM=4,9
		if (RDISC(MM,KK,NJPO) .eq. 1) then
		  if (MM.ne.7) then             ! Hack to fix bug in latex bit
		    write (BUFFER(J,I)(NN:NN),'(A)') RTBRC
		    NN=NN+1
		    if (NN.gt.LENBUFFER) then
		      call ERROR('joy: buffer overflow','(2)',3)
		    end if
		  end if
		end if
	      end do
	      KK=KK+1 
	    end if
	  end do
	end do
C
C Fix SECTYP so array dimensions are swapped (stupid I know)
C
	do I=1,NSEQ
	  do J=1,LENGTH
	    SECTYP(I,J)=SECTYP2(J,I)
	  end do
	end do
C
C -----------------------------------------------------------------------------
C
C Write out header information to the top of the .tex file
C
	call LAPREP(INFILE,LANDSCAPE,HELVETICA)
C
C for the moment don't want bullets at all
C
	AUTOACC=.false.
C
	if (PRFAMNAME) then
	  write (1,'(''%'')',err=943)
	  write (1,'(''% family name from alignment file'')')
	  write (1,'(''%'')')
	  call TR2LATEX(FAMILY)
          if (CENTERFAM) then
	    write (1,'(''\\begin{center}'')')
          end if
	  write (1,'(''{\\Large '',A,''}'')') FAMILY(1:lastchar(FAMILY))
	  write (1,'(''\\vspace{5mm}'')')
          if (CENTERFAM) then
	    write (1,'(''\\end{center}'')')
          end if
	end if
C
C -----------------------------------------------------------------------------
C
	do M=1,NTIMES
C
	  NTEXT=0
	  NLABEL=0
	  NNSEQ=0
	  NNEWSEQ=0
	  NN=1+((M-1)*LENBLOCK)
C
C be a fancy pants with the spacing. Normally do 1 space between blocks
C but at the last block do no spaces
C
	  if (M .eq. NTIMES) then
	    NSPACE=0
	  else
	    NSPACE=1
	  end if
C
	  write (1,'(''% alignment block '',I4)') M
	  if ((M .gt. 1) .and. (ONEPERPAGE)) then
	    write (1,'(''\\clearpage'')')
	  end if
C
	  call LABLST(LANDSCAPE)
C
C -----------------------------------------------------------------------------
C
C Do the automatic labelling if required. Note this simply labels every 10th
C alignment position
C
	  if (AUTONUM) then
	    write (1,'(''\\ \\ \\ '',A)') SEP
	    do J=NN,NN+LENBLOCK-2
	      if (J.gt.LENGTH) then
		write (1,'(A,A,A)') SCRIPT,CEN,SEP
	      else if (MOD(J,10).eq.0) then
		write (1,'(A,A,I3,1X,A)') SCRIPT,CEN,J,SEP
	      else
		write (1,'(A,A,A)') SCRIPT,CEN,SEP
	      end if
	    end do
	    if (J.gt.LENGTH) then
	      write (1,'(A,A,A)') SCRIPT,CEN,SEP
	    else
	      write (1,'(A,A,I3,1X,A)') SCRIPT,CEN,J,SEP
	    end if
C
C decrease spacing after a label line
C
	    write (1,'(''\\ [-1.75mm]'')')
	  end if
C
C ----------------------------------------------------------------------------
C
C Process all entries in the order list
C
	  do NENT=1,NUMENT
C
C ----------------------------------------------------------------------------
C
C Output user defined label information
C
	    if (ORDER(NENT)(1:5).eq.'label') then
	      NLABEL=NLABEL+1
	      write (1,'(''\ \ \ '',A)') SEP
	      do J=NN,NN+LENBLOCK-1
		write (1,'(A,A,A,1X,A)') SCRIPT,CEN,
     -                 LABEL(J,NLABEL)(1:LASTCHAR(LABEL(J,NLABEL))),SEP
	      end do
	      write (1,'(''\\ [-2.5mm]'')')
C
C ----------------------------------------------------------------------------
C
C Output text information
C
	    else if (ORDER(NENT)(1:4).eq.'text') then
	      NTEXT=NTEXT+1
	      write (1,'(''\ \ \ '',A)') SEP
	      do J=NN,NN+LENBLOCK-1
		write (1,'(''\centering '',A,1X,A)') TEXT(J,NTEXT),SEP
	      end do
	      write (1,'(''\\'')')
C
C Output standard sequence information
C
	    else if (ORDER(NENT).eq.'newseq') then
	      NNEWSEQ=NNEWSEQ+1
	      write (1,'(A,'' \ \ \ '',A)') TITNEWSEQ(NNEWSEQ),SEP
	      do J=NN,NN+LENBLOCK-1
		write (1,'(A,A,1X,A)') CEN,NEWSEQ(J,NNEWSEQ),SEP
	      end do
	      write (1,'(''\\'')')
C
C Output featured (or not as the case may be) sequence information
C
	    else if (ORDER(NENT)(1:3).eq.'seq') then
	      NNSEQ=NNSEQ+1
	      if ( .not. SWAPLBL) then
		if (AUTOLABEL(INDSTR(NNSEQ)) .or.
     +              USENUMFILE(INDSTR(NNSEQ))) then
		  write (1,'(''\ \ \ '',A)') SEP
		  do J=NN,NN+LENBLOCK-1
		    write (1,'(A,A,A,1X,A)') SCRIPT,CEN,
     -                  NUMLBL(J,NNSEQ)(1:LASTCHAR(NUMLBL(J,NNSEQ))),
     +                  SEP
		  end do
		  write (1,'(''\\'')')
		end if
	      end if
	      write (1,'(A,A,'' \ \ \ '',A)') RULE,LFILNAM(NNSEQ),SEP
	      do J=NN,NN+LENBLOCK-1
		write (1,'(A,A,A,A)') RULE,CEN,
     -                 BUFFER(J,NNSEQ)(1:LASTCHAR(BUFFER(J,NNSEQ))),SEP
	      end do
	      if ((NNSEQ .eq. NSEQ) .and.
     +            (AUTOSEC .or. AUTOACC .or. AUTOCONS)) then
		write (1,'(''\\ [-2.0mm]'')')
	      else
		write (1,'(''\\'')')
	      end if
C
C if Efimov assignments are wanted for all structures then:
C
	      if (DOEFI) then
		write (1,'(''Efimov \ \ \ '',A)') SEP
		do J=NN,NN+LENBLOCK-1
		  if (EFITYP2(J,NNSEQ).eq.1) then
		    write (1,'(A,''$\alpha$ '',A)') CEN,SEP
		  else if (EFITYP2(J,NNSEQ).eq.2) then
		    write (1,'(A,''$\beta$ '',A)') CEN,SEP
		  else if (EFITYP2(J,NNSEQ).eq.3) then
		    write (1,'(A,''$\pi$ '',A)') CEN,SEP
		  else if (EFITYP2(J,NNSEQ).eq.4) then
		    write (1,'(A,''$\rho$ '',A)') CEN,SEP
		  else if (EFITYP2(J,NNSEQ).eq.5) then
		    write (1,'(A,''$\gamma$ '',A)') CEN,SEP
		  else if (EFITYP2(J,NNSEQ).eq.6) then
		    write (1,'(A,''$\epsilon$ '',A)') CEN,SEP
		  else                          ! 7 and 8
		    write (1,'(A,A)') CEN,SEP
		  end if
		end do
C
C fudged linespacing after Efimov assignments
C
		if (((NNSEQ .eq. NSEQ) .and.
     +              (AUTOSEC .or. AUTOACC .or. AUTOCONS)) .or.
     +               ALLSEC) then
		  write (1,'(''\\ [-2.5mm]'')')
		else
		  write (1,'(''\\ [-2.0mm]'')')
		end if
	      end if
C
C If secondary structure is wanted for all structures then
C NB note missing omega
C
	      if (ALLSEC) then
		write (1,'(''dssp \ \ \ '',A)') SEP
		do J=NN,NN+LENBLOCK-1
		  if (SECTYP2(J,NNSEQ).eq.1) then
		    write (1,'(A,''$\alpha$ '',A)') CEN,SEP
		  else if (SECTYP2(J,NNSEQ).eq.2) then
		    write (1,'(A,''$\beta$ '',A)') CEN,SEP
		  else if (SECTYP2(J,NNSEQ).eq.3) then
		    write (1,'(A,''$\phi$ '',A)') CEN,SEP
		  else if (SECTYP2(J,NNSEQ).eq.5) then
C                   write (1,'(A,''$\omega$ '',A)') CEN,SEP
		  else if (SECTYP2(J,NNSEQ).eq.6) then
		    write (1,'(A,''\scriptsize 3 '',A)') CEN,SEP
		  else
		    write (1,'(A,A)') CEN,SEP
		  end if
		end do
		if ((NNSEQ .eq. NSEQ) .and.
     +              (AUTOSEC .or. AUTOACC .or. AUTOCONS)) then
		  write (1,'(''\\ [-2.5mm]'')')
		else
		  write (1,'(''\\ [-1.5mm]'')')
		end if
C
		if (DOSHEET) then
		  write (1,'(''sheet \ \ \ '',A)') SEP
		  do J=NN,NN+LENBLOCK-1
		    write (1,'(''\scriptsize '',A,A)') 
     +                     SHEET2(J,NNSEQ),SEP
		  end do
		  if ((NNSEQ .eq. NSEQ) .and.
     +                (AUTOSEC .or. AUTOACC .or. AUTOCONS)) then
		    write (1,'(''\\ [-2.5mm]'')')
		  else
		    write (1,'(''\\ [-1.5mm]'')')
		  end if
		end if
	      end if
C
C If labels are to be swapped
C
	      if (SWAPLBL) then
		if (AUTOLABEL(INDSTR(NNSEQ)) .or. 
     +              USENUMFILE(INDSTR(NNSEQ))) then
		  write (1,'(''\ \ \ '',A)') SEP
		  do J=NN,NN+LENBLOCK-1
		    write (1,'(A,A,A,1X,A)') SCRIPT,CEN,
     -                    NUMLBL(J,NNSEQ)(1:LASTCHAR(NUMLBL(J,NNSEQ))),
     +                    SEP
		  end do
		  if ((NNSEQ .eq. NSEQ).and.
     +                (AUTOSEC .or. AUTOACC .or. AUTOCONS)) then
		    write (1,'(''\\ [-2.5mm]'')')
		  else
		    write (1,'(''\\'')')
		  end if
		end if
	      end if
C
C Output completely conserved residues.
C for sequence demand invariance (cutoff = 1.0), output these as upper case
C oupt residues present in CUTOFF (as per secondary structure etc) as lower
C case
C
	      if ((NNSEQ .eq. NSEQ) .and. AUTOCONS) then
		write (1,'(''consensus \ \ \ '',A)') SEP
		do J=NN,NN+LENBLOCK-1
		  CONSENTMP=CONSEN(SEQTYP(1,J),NSEQ,22,1.0)
		  if (CONSENTMP .lt. 22) then
		    write (1,'(A,A,A)') CEN,RES(CONSENTMP:CONSENTMP),
     +                                  SEP
		  else
		    if (DOCONS) then
		      CONSENTMP=CONSEN(SEQTYP(1,J),NSEQ,22,CUTOFF)
		    end if
		    if (CONSENTMP.lt.22) then
		      write (1,'(A,A,A)') CEN,
     +                         TOLOWER(RES(CONSENTMP:CONSENTMP)),SEP
		    else
		      write (1,'(A,A)') CEN,SEP
		    end if
		  end if
		end do
		if (AUTOSEC) then
		  write (1,'(''\\ [-2.5mm]'')')
		else
		  write (1,'(''\\'')')
		end if
	      end if
C
C
C code to output consensus secondary structure
C
	      if ((NNSEQ.eq.NSEQ).and.(AUTOSEC)) then
		write (1,'(''\ \ \ '',A)') SEP
		do J=NN,NN+LENBLOCK-1
		  CONSENTMP=CONSEN(SECTYP(1,J),NSEQ,5,CUTOFF)
		  if (CONSENTMP.eq.1) then
		    write (1,'(A,''$\alpha$ '',A)') CEN,SEP
		  else if (CONSENTMP.eq.2) then
		    write (1,'(A,''$\beta$ '',A)') CEN,SEP
		  else if (CONSENTMP.eq.3) then
		    write (1,'(A,''\scriptsize 3 '',A)') CEN,SEP
		  else if (CONSENTMP.eq.5) then
		    write (1,'(A,''$\pi$ '',A)') CEN,SEP
		  else
		    write (1,'(A,A)') CEN,SEP
		  end if
		end do
		if (AUTOACC) then
		  write (1,'(''\\ [-2.5mm]'')')
		else
		  write (1,'(''\\'')')
		end if
	      end if
C
	      if ((NNSEQ .eq. NSEQ) .and. (AUTOACC)) then
		write (1,'(''\ \ \ '',A)') SEP
		do J=NN,NN+LENBLOCK-1
		  CONSENTMP=CONSEN(ACCTYP(1,J),NSEQ,2,CUTOFF)
		  if (CONSENTMP.eq.1) then
		    write (1,'(A,''$\bullet$ '',A)') CEN,SEP
		  else
		    write (1,'(A,A)') CEN,SEP
		  end if
		end do
		write (1,'(''\\'')')
	      end if
	    end if
	  end do
	  call LABLFI(NSPACE)
	end do
C
C outpur the key block if wanted
C
	if (DOKEY) then
	  call JOYKEY()
	end if
	call LAEND()
C
	close (unit=1,err=998)
C
C ----------------------------------------------------------------------------
C
      return
C
C ----------------------------------------------------------------------------
C
C File errors
C
999   call ERROR('joy: error opening LaTeX file',FILE,3)
998   call ERROR('joy: error closing LaTeX file',FILE,3)
943   call ERROR('joy: error writing LaTeX file',FILE,0)
C
C ----------------------------------------------------------------------------
C
      end
