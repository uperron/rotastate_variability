      logical function ALPNUM(CARD)
C
C 
C To determine if a charcter is alphanumeric or not. If the character is a '*'
C then ALPNUM is set to true. 
C
      character*1 CARD
      integer N
C
      N=ICHAR(CARD)
C
      if ((N.ge.48.and.N.le.57).or.
     -    (N.ge.65.and.N.le.90).or.
     -    (N.ge.97.and.N.le.122).or.
     -    (CARD.eq.'*')) then
        ALPNUM=.TRUE.
        return
      end if
C
      ALPNUM=.FALSE.
      return
      end
