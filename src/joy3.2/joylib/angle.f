      real function ANGLE(X,Y,Z)
C
C Calculates the angle (in degrees) between the points X,Y, and Z,
C returned angle in ANGLE
C
      implicit none
      real RADDEG
      parameter (RADDEG=57.29577951)
      real X(3), Y(3), Z(3), XY(3), YZ(3), RXY, RYZ
      integer I
C
      character*35 IDENT
      data IDENT /'@(#) angle.f - (c) jpo 1990        '/
C
      RXY=0.0
      RYZ=0.0
      do I=1,3
        XY(I)=X(I)-Y(I)
        YZ(I)=Y(I)-Z(I)
        RXY=RXY+(XY(I)*XY(I))
        RYZ=RYZ+(YZ(I)*YZ(I))
      end do
C
C Check for funny points
C
      RXY=SQRT(RXY)
      RYZ=SQRT(RYZ)
C
      if ((RXY.eq.0.0).or.(RYZ.eq.0.0)) then
        call ERROR('angle: vector has no magnitude',' ',1)
      end if
C
      ANGLE=180.00-(RADDEG*acos(((XY(1)/RXY)*(YZ(1)/RYZ))+
     -         ((XY(2)/RXY)*(YZ(2)/RYZ))+
     -         ((XY(3)/RXY)*(YZ(3)/RYZ))))
C
      return
      end
