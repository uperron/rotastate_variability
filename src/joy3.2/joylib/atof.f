      real function ATOF(ACHAR)
C
C returns integer from string variable
C
      character*(*) ACHAR
      logical ISDIGIT
C
      do I=1,LASTCHAR(ACHAR)
        if ( .not. ISDIGIT(ACHAR(I:I)) .or. ACHAR(I:I) .eq. '.') then
          goto 900
        end if
      end do
C
      read (ACHAR,'(F8.2)',err=900) ATOF
      return
C
900   call ERROR('atof: cannot convert',ACHAR,1)
      end
