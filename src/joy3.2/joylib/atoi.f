      integer function ATOI(ACHAR)
C
C returns integer from string variable
C
      character*(*) ACHAR
      logical ISDIGIT
C
      do I=1,LASTCHAR(ACHAR)
        if ( .not. ISDIGIT(ACHAR(I:I))) then
          goto 900
        end if
      end do
C
      read (ACHAR,'(I8)',err=900) ATOI
      return
C
900   call ERROR('atoi: cannot convert',ACHAR,1)
      end
