      real function DIST2(X,Y)
C
C Calculates the the square of the distance between X and Y.
C
      REAL  X(3), Y(3)
C
      DIST2=((X(1)-Y(1))**2)+((X(2)-Y(2))**2)+((X(3)-Y(3))**2)
C
      RETURN
      END
