      character*1 function ELEMENT(ATOM)
C
      character*3 ATOM
C
      if (index(ATOM,'C').gt.0) then
        ELEMENT='C'
      else if (index(ATOM,'N').gt.0) then
        ELEMENT='N'
      else if (index(ATOM,'O').gt.0) then
        ELEMENT='O'
      else if (index(ATOM,'S').gt.0) then
        ELEMENT='S'
      else if (index(ATOM,'P').gt.0) then
        ELEMENT='P'
      else
        ELEMENT='X'
      end if
C
      return
      end
