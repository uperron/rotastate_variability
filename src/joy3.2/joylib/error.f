      subroutine ERROR(TEXT1,TEXT2,CODE)
C
C Simple routine to write an error message on the standard error and then to
C exit or return to calling routine. A code of one or more exits the program.
C
      implicit NONE
      integer STDERR, LASTCHAR
      parameter (STDERR=0)
      character*(*) TEXT1,TEXT2
      integer CODE
C
C catch strange codes
C
      if (CODE .lt. 0 .or. CODE .gt. 4) then
        write (STDERR,'(''error: bad call to error - unknown code'')')
      end if
C
C Output error message
C
      write (STDERR,'(A,1X,A)') TEXT1(1:lastchar(TEXT1)),
     +                          TEXT2(1:lastchar(TEXT2))
C
C Exit if error code is not equal to zero and told to catch fatal errors
C wait for return if code is 4
C
      if (CODE .eq. 0) then 
        continue
      else if (CODE .eq. 4) then 
        PAUSE
      else
        call EXIT(CODE)
      end if
C
      return
      end
