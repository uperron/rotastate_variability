      integer function FIRSTCHAR(STRING)
C
C Returns the position of the first printable character (not a blank) in the
C string STRING
C
      character*(*)  STRING
      integer J
      logical ISPRINT
C
      FIRSTCHAR=1
      do J=1,LEN(STRING)
        if (ISPRINT(STRING(J:J))) then
          FIRSTCHAR=J
          return
        end if
        FIRSTCHAR=J
      end do
C
      return
      end
