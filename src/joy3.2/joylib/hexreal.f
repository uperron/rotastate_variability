      real function HEXREAL(HVAL)
C
C returns a hexadecimal style value (0-f) for values 0-160
C
      character*1 HVAL
C
      if (HVAL .eq. '0') then
          HEXREAL = 0.
      else if (HVAL .eq. '1') then
          HEXREAL = 1.
      else if (HVAL .eq. '2') then
          HEXREAL = 2.
      else if (HVAL .eq. '3') then
          HEXREAL = 3.
      else if (HVAL .eq. '4') then
          HEXREAL = 4.
      else if (HVAL .eq. '5') then
          HEXREAL = 5.
      else if (HVAL .eq. '6') then
          HEXREAL = 6.
      else if (HVAL .eq. '7') then
          HEXREAL = 7.
      else if (HVAL .eq. '8') then
          HEXREAL = 8.
      else if (HVAL .eq. '9') then
          HEXREAL = 9.
      else if (HVAL .eq. 'a') then
          HEXREAL = 10.
      else if (HVAL .eq. 'b') then
          HEXREAL = 11.
      else if (HVAL .eq. 'c') then
          HEXREAL = 12.
      else if (HVAL .eq. 'd') then
          HEXREAL = 13.
      else if (HVAL .eq. 'e') then
          HEXREAL = 14.
      else
          HEXREAL = 15.
      end if
C
      return
      end
