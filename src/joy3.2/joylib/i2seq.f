      character*1 function I2SEQ(I)
C
C returns the one letter amino-acid code
C
      integer I, J, NRESTP
      parameter (NRESTP=20)
      character*1 RESID(0:NRESTP)
C
      data RESID / '-','A','C','D','E','F','G','H','I','K','L',
     -             'M','N','P','Q','R','S','T','V','W','Y' /
      I2SEQ=' '
C
      do J=0,NRESTP
        if (J .eq. I) then
          I2SEQ=RESID(J)
          return
        end if
      end do
C
      write (6,*) 'i2seq ',i
      call ERROR('impossible i2seq',' ',1)
C
      return
      end
