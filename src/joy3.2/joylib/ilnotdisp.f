      integer function ILNOTDISP(CH)
C
      implicit NONE
      character CH*1
      integer ICH
C
      ICH=ICHAR(CH)
      if ((ICH .lt. 33) .or. (ICH .gt. 127)) then
        ILNOTDISP = 1
      else
        ILNOTDISP = 0
      end if
C
      return
      end
