
      integer function indexb(line)
C
C  returns the index of the last contiguous printable and non-blank character
C     in line, scan starting at the first such character: if no non-blank
C     printable character, 0 is returned;
C
        character line*(*)
        npos = lenr(line)
        if (npos .gt. 0) then
C - (if lenr in range, then lenl in range too)
          do 100  i = lenl(line), npos
            ii = i
            if (ilnotdisp(line(ii:ii)) .eq. 1) go to 200
100       continue
          indexb = ii
          return
200       indexb = ii-1
        else
          indexb = 0
        end if
        return
      end  
