      integer function IRNOTDISP(CH)
C
      implicit none
      character CH*1
      integer ICH
C
      ICH=ICHAR(CH)
      if ((ICH .lt. 32) .or. (ICH .gt. 127)) then
        IRNOTDISP = 1
      else
        IRNOTDISP = 0
      end if
C
      return
      end
