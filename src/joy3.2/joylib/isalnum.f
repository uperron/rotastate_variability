      logical function ISALNUM(ACHAR)
C
C Function that returns if a character variable is a upper case letter or not
C
      implicit NONE
      character*1 ACHAR
      logical ISUPPER, ISLOWER, ISDIGIT
C
      if (ISUPPER(ACHAR) .or. ISLOWER(ACHAR) .or. ISDIGIT(ACHAR)) then
        ISALNUM=.true.
        return
      end if
C
      ISALNUM=.false.
      return
      end
