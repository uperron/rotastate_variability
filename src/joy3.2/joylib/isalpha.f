      logical function ISALPHA(ACHAR)
C
C Function that returns if a character variable is a upper case letter or not
C
      implicit NONE
      character*1 ACHAR
      logical ISUPPER, ISLOWER
C
      if (ISUPPER(ACHAR) .or. ISLOWER(ACHAR)) then
        ISALPHA=.true.
        return
      end if
C
      ISALPHA=.false.
      return
      end
