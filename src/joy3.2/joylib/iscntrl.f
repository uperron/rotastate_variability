      logical function ISCNTRL(ACHAR)
C
C Function that returns if a character variable is a upper case letter or not
C
      implicit none
      character*1 ACHAR
      integer N
C
      N=ICHAR(ACHAR)
      if ((N .le. 31) .or. (N .eq. 127)) then
        ISCNTRL=.true.
        return
      end if
C
      ISCNTRL=.false.
      return
      end
