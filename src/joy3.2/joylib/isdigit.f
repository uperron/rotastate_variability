      logical function ISDIGIT(ACHAR)
C
      implicit none
      character*1 ACHAR
      integer N
C
      N=ICHAR(ACHAR)
      if ((N .ge. 48) .and. (N .le. 57)) then
        ISDIGIT=.TRUE.
        return
      end if
C
      ISDIGIT=.FALSE.
      return
      end
