      logical function ISLOWER(ACHAR)
C
C Function that returns if a character variable is a lower case letter or not
C
      implicit none
      character*1 ACHAR
      integer N
C
      N=ICHAR(ACHAR)
      if ((N .ge. 97) .and. (N .le. 122)) then
        ISLOWER=.TRUE.
        return
      end if
C
      ISLOWER=.FALSE.
      return
      end
