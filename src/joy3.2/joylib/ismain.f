      logical function ISMAIN(ATOM)
C
      character*3 ATOM, MAIN(3)
C
      data MAIN/'O  ','N  ','OXT'/
C
      do I=1,3
        if (ATOM.eq.MAIN(I)) then
          ISMAIN=.true.
          return
        end if
      end do
C
      ISMAIN=.false.
C
      return
      end
