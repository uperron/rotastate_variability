      logical function ISPRINT(ACHAR)
C
C Function that returns if a character variable is printable or not
C
      implicit none
      character*1 ACHAR
      integer     N
C
C N.B. This version does not count a blank as printable !!!
C
      N=ICHAR(ACHAR)
      if ((N.ge.33).and.(N.le.126)) then
        ISPRINT=.TRUE.
        return
      end if
      ISPRINT=.FALSE.
C
      return
      end
