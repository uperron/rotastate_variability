      logical function ISPUNCT(TEXT)
C
C function to see if a character is valid punctuation for a sequence
C
      implicit none
      character*1 TEXT
      character*4 PUNCT
      integer I
C
      data PUNCT /' -*/'/
C
      do I=1,LEN(PUNCT)
        if (TEXT.eq.PUNCT(I:I)) then
          ISPUNCT=.true.
          return
        end if
      end do
C
      ISPUNCT=.false.
C
      return
      end
