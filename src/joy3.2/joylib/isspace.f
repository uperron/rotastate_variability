      logical function ISSPACE(ACHAR)
C
C returns true if ACHAR is a tab, space, newline, vertical tab, formfeed or
C return
C
      implicit none
      character*1 ACHAR
      integer N
C
      N=ICHAR(ACHAR)
      if ((N .eq. 9) .or.
     +    (N .eq. 32) .or.
     +    (N .eq. 13) .or.
     +    (N .eq. 11) .or.
     +    (N .eq. 10) .or.
     +    (N .eq. 12)) then
        ISSPACE=.true.
        return
      end if
C
      ISSPACE=.false.
      return
      end
