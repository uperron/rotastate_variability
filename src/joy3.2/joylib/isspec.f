      logical function ISSPEC(TEXT)
C
C Logical function to see if a character can be easily handled by
C Latex
C
      implicit none
      character*(*) TEXT
      character*10 ILLEGAL
      integer I,J
C
C if compiler uses a non standard char set then can have problems with
C backslash char
C
      data ILLEGAL /'#$%&~_^\{}'/
C
      do J=1,LEN(TEXT)
        do I=1,LEN(ILLEGAL)
          if (TEXT(J:J).eq.ILLEGAL(I:I)) then
            ISSPEC=.true.
            return
          end if
        end do
      end do
C
      ISSPEC=.false.
      return
      end
