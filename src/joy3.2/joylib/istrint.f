      integer function ISTRINT(RES)
C
C this function returns the integer type for the three letter i
C residue code; if a code can not be recognized, 21 is returned;
C
      implicit NONE
      integer NTYPES
      parameter (NTYPES = 25)
      integer I
      character RES*3, RESLST(NTYPES)*3
C
C three letter amino acid residue types:
C
      data   RESLST /'ALA', 'CYS', 'ASP', 'GLU', 'PHE', 'GLY',
     +               'HIS', 'ILE', 'LYS', 'LEU', 'MET', 'ASN',
     +               'PRO', 'GLN', 'ARG', 'SER', 'THR', 'VAL',
     +               'TRP', 'TYR', 'ASX', 'GLX', 'UNK', 'PCA', 'INI'/
C
      do I=1,NTYPES
        if (RESLST(I) .eq. RES) then
          ISTRINT=I
          if (I .eq. 21) ISTRINT=12        ! ASX => ASN
          if (I .eq. 22) ISTRINT=14        ! GLX => GLN
          if (I .eq. 23) ISTRINT=6        ! UNK => GLY
          if (I .gt. 23) ISTRINT=21        ! XXX => GAP
          return
        end if
      end do
      ISTRINT = 21                           ! ??? => GAP
      return
      end
