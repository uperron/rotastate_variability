      integer function ISTRTYP(STR, STRARR, NARR)
C
      implicit none
      integer NARR, I
      character*(*) STR, STRARR(NARR)
C
      do I=1,NARR
        ISTRTYP=I
        if (STR .eq. STRARR(I)) return
      end do
      ISTRTYP=0
      return
      end
