      logical function ISUPPER(ACHAR)
C
C Function that returns if a character variable is a upper case letter or not
C
      implicit none
      character*1 ACHAR
      integer N
C
      N=ICHAR(ACHAR)
      if ((N .ge. 65) .and. (N .le. 90)) then
        ISUPPER=.true.
        return
      end if
C
      ISUPPER=.false.
      return
      end
