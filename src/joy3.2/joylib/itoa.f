      character*(*) function ITOA(INT)
C
C returns integer from string variable
C
      integer INT
C
      write (ITOA,'(A8)',err=900) INT
      return
C
900   call ERROR('itoa: cannot convert',' ',1)
      end
