      integer function LASTCHAR(STRING)
C
C Returns the position of the last printable character (not a blank) in the
C string STRING
C
      character*(*)  STRING
      integer J
      logical ISPRINT
C
      LASTCHAR=LEN(STRING)
      do J=LEN(STRING),1,-1
        if (ISPRINT(STRING(J:J))) then
          LASTCHAR=J
          return
        end if
        LASTCHAR=J
      end do
C
      return
      end
