C 
C ###########################################################################
C
      integer FUNCTION LASTDOT(STRING)
C
C returns the position of the last dot (period) in the string
C
      implicit NONE
      character*(*) STRING
      integer I
C
      LASTDOT=0
      do I=LEN(STRING),1,-1
        if (STRING(I:I).eq.'.') then
          LASTDOT=I
          return
        end if
      end do
C
      return
      end
