C
C ############################################################################
C
      integer function LASTSLASH(STRING)
C
C returns position of last slash (/) in the string
C
      implicit NONE
      character*(*) STRING
      integer I
C
      LASTSLASH=0
      do I=LEN(STRING),1,-1
        if (STRING(I:I).eq.'/') then
          LASTSLASH=I
          return
        end if
      end do
C
      return
      end
