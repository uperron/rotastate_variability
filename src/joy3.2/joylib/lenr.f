      integer function LENR(STR)
C
C  the index of the first printable character from the right:
C       (if no printable character, 0 is returned)
C       (if string argument incorrect, it aborts)
C
      implicit none
      character STR*(*)
      integer LENS, LENLFT, I, IRNOTDISP, ILAST, LENL
C
      LENS = len(STR)
      if (LENS. GT. 0) then
        LENLFT = LENL(STR)
        if (LENLFT .le. LENS) then
          I = LENLFT
10        if (irnotdisp(str(i:i)) .eq. 1) go to 100
            if (STR(I:I) .ne. ' ') ILAST = I + 1
            I = I + 1
            if (I .le. LENS) go to 10
100       continue
          LENR = ILAST - 1
        else
          LENR = 0
        end if
      else
        call ERROR('lenr: string has zero length',' ',1)
      end if
C
      return
      end
