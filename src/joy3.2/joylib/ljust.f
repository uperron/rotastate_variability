      subroutine LJUST(CARD)
C 
C Subroutine to left justify a character string CARD
C
C CARD  Input and output character string
C LENGTH   Length of character string
C NCHAR Number of non-blank characters in string
C
      character*(*) CARD
      integer NPOS, I, LENGTH
C
      NPOS=1
      LENGTH=len(CARD)
C
10    if (CARD(NPOS:NPOS).eq.' ') then
        if (NPOS.eq.LENGTH) then
          return
        end if
        NPOS=NPOS+1
        goto 10
      end if
      do I=1,LENGTH-NPOS+1
        CARD(I:I)=CARD(I+NPOS-1:I+NPOS-1)
      end do
      do I=LENGTH-NPOS+2,LENGTH
        CARD(I:I)=' '
      end do
C
      return
      end
