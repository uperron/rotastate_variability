      subroutine MESSAGE(TEXT)
C
C Simple routine to write a message on the standard output and then to
C return to calling routine.
C
      implicit NONE
      integer STDERR, STDOUT
      parameter (STDERR=0, STDOUT=6)
      character*(*) TEXT
C
C Output error message
C
      write (STDERR,'(A)',err=900) TEXT
      return
C
900   call ERROR('message: bad call to message',TEXT,1)
C
      end
