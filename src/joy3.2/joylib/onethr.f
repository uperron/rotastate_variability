      subroutine ONETHR(CHAR,CHARNW,NRES)
C
C Converts one letter code to three letter code
C
      character*(*) CHAR(1), CHARNW(1)
      character*24 AA1
      character*3 AA3(24)
      character*1 NAM1
      integer I, J, NRES
      logical FOUND
C
      data AA1/'SGVTALDIPKQNFYERCHWMBZXJ'/
      data AA3/'SER','GLY','VAL','THR','ALA',
     -         'LEU','ASP','ILE','PRO','LYS',
     -         'GLN','ASN','PHE','TYR','GLU',
     -         'ARG','CYS','HIS','TRP','MET',
     -         'ASX','GLX','UNK','CYH'/
C 
      do I=1,NRES
        NAM1=CHAR(I)(1:1)
        FOUND=.FALSE.
        do J=1,24
          if (NAM1.eq.AA1(J:J)) then
            CHARNW(I)=AA3(J)
            FOUND=.TRUE.
            goto 1
          end if
        end do
1       continue
        if (.not.FOUND) then 
          CHARNW(I)='UNK'
          call ERROR('joy: onethr: unknown residue type',NAM1,1)
        end if
      end do
C
      return
      end
