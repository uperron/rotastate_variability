      character*1 function REALHEX(RVAL)
C
C returns a hexadecimal style value (0-f) for values 0-160
C
        real RVAL
C
	if (RVAL .lt. 10.0) then
          REALHEX = '0'
	else if (RVAL .lt. 20.0) then
          REALHEX = '1'	
	else if (RVAL .lt. 30.0) then
          REALHEX = '2'	
	else if (RVAL .lt. 40.0) then
          REALHEX = '3'	
	else if (RVAL .lt. 50.0) then
          REALHEX = '4'	
	else if (RVAL .lt. 60.0) then
          REALHEX = '5'	
	else if (RVAL .lt. 70.0) then
          REALHEX = '6'	
	else if (RVAL .lt. 80.0) then
          REALHEX = '7'	
	else if (RVAL .lt. 90.0) then
          REALHEX = '8'	
	else if (RVAL .lt. 100.0) then
          REALHEX = '9'	
	else if (RVAL .lt. 110.0) then
          REALHEX = 'a'	
	else if (RVAL .lt. 120.0) then
          REALHEX = 'b'	
	else if (RVAL .lt. 130.0) then
          REALHEX = 'c'	
	else if (RVAL .lt. 140.0) then
          REALHEX = 'd'	
	else if (RVAL .lt. 150.0) then
          REALHEX = 'e'	
	else
          REALHEX = 'f'	
        end if
C
	return
	end
