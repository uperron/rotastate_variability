      SUBROUTINE RJUST(CARD,LEN)
C
C Removes blanks to the right of text in CARD.
C
      implicit NONE
      integer NPOS, LEN, NCHAR, I
      character*1 CARD(LEN)
C
      NPOS=LEN
c
10    if (CARD(NPOS).EQ.' ') then
        NPOS=NPOS-1
        GOTO 10
      end if
C
      do I=NPOS,1,-1
        CARD(I+LEN-NPOS)=CARD(I)
      end do
      do I=1,LEN-NPOS
        CARD(I)=' '
      end do
C
      NCHAR=LEN-NPOS
      return
C
      end      
