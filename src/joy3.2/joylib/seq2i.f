      integer function SEQ2I(CHAR)
C
C returns the one letter amino-acid code
C
      integer J, NRESTP
      parameter (NRESTP=22)
      character*1 RESID(0:NRESTP), CHAR
C
      data RESID / '-','A','C','D','E','F','G','H','I','K','L',
     -             'M','N','P','Q','R','S','T','V','W','Y','J','-' /
      SEQ2I=0
C
      do J=0,NRESTP
        if (RESID(J) .eq. CHAR) then
          SEQ2I=J
          return
        end if
      end do
C
      call ERROR('impossible seq2i',CHAR,1)
C
      return
      end
