      subroutine STR_L(LINE, ARG, TYPE)
C
C  reads one logical variable from line (ON/OFF in upper or lower case
C     are true and false, respectively, if type='O')
C
      implicit NONE
      character LINE*(*), STR*3, TYPE*1, TMP*1
      logical ARG
C
      TMP=TYPE
      call STR_S(LINE, STR)
      call UPPER(STR)
      call UPPER(TMP)
C
      if (TMP .eq. 'O') then
        if (STR(1:2) .eq. 'ON')  ARG = .true.
        if (STR(1:3) .eq. 'OFF') ARG = .false.
      else
        read (STR,'(L80)') ARG
      end if
C
      return
      end
