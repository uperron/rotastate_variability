      subroutine STR_R(S, R)
C
C  converts the first number in string s to real r:
C
      implicit NONE
      real R
      character S*(*), STR*20
      call STR_S(S, STR)
      read (STR,'(f20.0)') R
      return
      end
