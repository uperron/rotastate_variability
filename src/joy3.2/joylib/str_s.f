      subroutine STR_S(LINE, BUFFR)
C
C  returns the first contiguous block of printable non-blank characters
C     in line to buffr;
C
      implicit none
      character BLANK*1
      parameter (blank=' ')
      integer IBEG, IEND, LENB, I, LENL, INDEXB
C
C should have no effect
c     common /ISTRNUM/ IBEG, IEND
C
      character BUFFR*(*), LINE*(*)
C
      IBEG = LENL(LINE)
      IEND = INDEXB(LINE)
      if (IEND .ge. IBEG) then
        LENB = len(BUFFR)
        if ((IEND-IBEG+1) .le. LENB) then
          BUFFR = LINE(IBEG:IEND)
          do I = IEND+1, LENB
            BUFFR(I:I) = BLANK
          end do
        else
          call ERROR('str_s: length of buffr too short',' ',1)
        end if
      else
        call ERROR('STR_S: string has no contents',' ',1)
      end if
      return
      end
