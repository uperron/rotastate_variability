      subroutine THRONE(CHAR,CHARNW,NRES)
C
C Converts three letter amino-acid code to one letter code
C !! note bad practice of length 1 arrays, beware
C
      implicit none
      character*(*) CHAR(1), CHARNW(1)
      character*24 AA1
      character*3 AA3(24), NAM3
      integer I, J, NRES
      logical FOUND
C
      data AA1/'SGVTALDIPKQNFYERCHWMBZXJ'/
      data AA3/'SER','GLY','VAL','THR','ALA',
     -         'LEU','ASP','ILE','PRO','LYS',
     -         'GLN','ASN','PHE','TYR','GLU',
     -         'ARG','CYS','HIS','TRP','MET',
     -         'ASX','GLX','UNK','CYH'/
C
      do I=1,NRES
        NAM3=CHAR(I)
        FOUND=.false.
        do J=1,24
          if (NAM3 .eq. AA3(J)) then
            CHARNW(I)=AA1(J:J)
            FOUND=.true.
            goto 2
          end if
        end do
2       continue
        if (.not.FOUND) then
          CHARNW(I)='X'
          call ERROR('throne: unrecognized residue',NAM3,1)
        end if
      end do
C
      return
      end
