      subroutine TIMEDIFF(TIME1,TIME2,TIMED,TIME3)
C
C calculates the difference in time between two times, result returned as char
C in hours minutes and seconds
C
      integer HR1, HR2, MN1, MN2, SC1, SC2, IHR, IMN, ISC, ITIME
      character*8 TIME1, TIME2, TIME3
      real RTIME1, RTIME2, TIMED
C
      TIMED=0.0
      TIME3='XX:XX:XX'
C
C get minutes, etc from strings
C
      read (TIME1(1:2),'(I2)') HR1
      read (TIME1(4:5),'(I2)') MN1
      read (TIME1(7:8),'(I2)') SC1
      read (TIME2(1:2),'(I2)') HR2
      read (TIME2(4:5),'(I2)') MN2
      read (TIME2(7:8),'(I2)') SC2
C
C convert to seconds
C
      RTIME1=(real(HR1)*3600.0)+(real(MN1)*60.0)+(real(SC1))
      RTIME2=(real(HR2)*3600.0)+(real(MN2)*60.0)+(real(SC2))
C
C 86400 is the number of seconds in a day
C
      TIMED=RTIME2-RTIME1
      if (TIMED.lt.0.0) then
        RTIME2=RTIME2+86400.0
        TIMED=RTIME2-RTIME1
      end if
C
C convert from seconds to h:m:s
C
      ITIME=int(TIMED)
      IHR=ITIME/3600
      ITIME=ITIME-(IHR*3600)
      IMN=ITIME/60
      ITIME=ITIME-(IMN*60)
      ISC=ITIME
C
      write (TIME3(1:2),'(I2.2)') IHR
      write (TIME3(4:5),'(I2.2)') IMN
      write (TIME3(7:8),'(I2.2)') ISC
C
      return
      end
