      character*1 function TOLOWER(CHARA)
C
C converts upper case to lower case
C
      character*1 CHARA
      logical ISUPPER
C
      if (ISUPPER(CHARA)) then
        TOLOWER=CHAR(ICHAR(CHARA)+32)
      else
        TOLOWER=CHARA
      end if
C
      return
      end
