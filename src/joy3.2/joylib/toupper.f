      character*1 function TOUPPER(CHARA)
C
C converts lower case to upper case
C
      character*1 CHARA
      logical ISLOWER
C
      if (ISLOWER(CHARA)) then
        TOUPPER=CHAR(ICHAR(CHARA)-32)
      else
        TOUPPER=CHARA
      end if
C
      return
      end
