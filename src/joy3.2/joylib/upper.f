      subroutine UPPER(LINE)
C
C  to transform all characters in the input file into lower case;
C
      implicit none
      character LINE*(*), CH*1
      integer LCONT, LENR, I, ICH
C
      LCONT = LENR(LINE)
      do I = 1, LCONT
        CH = LINE(I:I)
        ICH = ICHAR(CH)
        if ((ICH .ge. 97) .and. (ICH .lt. 122)) ICH=ICH-32
        LINE(I:I) = char(ICH)
      end do
      return
      end
