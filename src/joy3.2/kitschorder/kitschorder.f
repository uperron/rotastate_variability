      program KITSCHORDER
C
C program to read in treefile from kitsch, and reorder a joy alignment file,
C consistent with the treefile order
C
C BUGS
C will not preserve order (in fact will not process at all, non pIR type records
C i.e. text, number, etc. But it will preserve the comment characters
C
C Had a nasty bug for alignments with only two members, everything got trashed
C because of empty treefile (lucky for backups)
C
      integer STDIN, STDOUT, STDERR, MAXSTR, MAXLIN
      parameter (STDIN=5, STDOUT=6, STDERR=0, MAXSTR=40, MAXLIN=20)
      character*256 BUFFER
      character*256 TITLE(MAXSTR)
      character*75 SEQ(MAXLIN,MAXSTR)
      character*10 CODE(MAXSTR)
      character*4 VERSION
      integer NORDER(MAXSTR), INDX(MAXSTR)
      integer NTSR
      integer LASTCHAR
C
C intitialize things
C
      do I=1,MAXSTR
        NORDER(I)=I
      end do
      VERSION='1.0a'
      NSTR=0
C
C read in alignment file (stdin) write to stdout
C
1     read (STDIN,fmt='(A)',end=2) BUFFER
      if (BUFFER(1:3) .eq. 'C; ') then
        write (STDOUT,'(A)') BUFFER(1:lastchar(BUFFER))
5     else if (BUFFER(1:4) .eq. '>P1;') then
6       continue
        NSTR=NSTR+1
        CODE(NSTR)=BUFFER(5:10)
        read (STDIN,'(A)',end=1) TITLE(NSTR)
        NLIN=0
3       read (STDIN,'(A)',end=1) BUFFER
        NLIN=NLIN+1
        if (BUFFER(1:4) .eq. '>P1;') goto 6
        SEQ(NLIN,NSTR)=BUFFER(1:75)
        goto 3
      else
        write (STDERR,'(''kitschorder: missing data ? '',A,''...'')')
     +         BUFFER(1:10)
      end if
      goto 1
2     continue
C
C read in treefile (must be called treefile)
C
      if (NSTR .gt. 2) then
        call PARSETREE(NSTR,CODE,NORDER)
      end if
C
C redorder
C
      write (STDOUT,'(''C; reordered by kitschorder '',A)') VERSION
      do I=1,NSTR
        J=NORDER(I)
        write (STDOUT,'(''>P1;'',A)') CODE(J)(1:lastchar(CODE(J)))
        write (STDOUT,'(A)') TITLE(J)(1:lastchar(TITLE(J)))
        do K=1,NLIN
          write (STDOUT,'(A)') SEQ(K,J)(1:lastchar(SEQ(K,J)))
        end do 
      end do
C
      call EXIT(0)
C
900   write (STDERR,'(''kitschorder; bad read of alignment file'')')
      call EXIT(1)
C
      end
