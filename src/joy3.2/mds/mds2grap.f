      program MDS2GRAP
C
C A general multi-dimensional scaling program, limited to a MAXDAT by
C MAXDAT distance matrix.
C
C Algorithm Taken from Chatfield and Collins, An introduction to
C Multivariate analysis
C
C reads in a formatted distance file and performs multidimensional scaling
C analysis on the data. writes out graph in a form suitable for grap, a graph
C typesetting utility under unix. Also writes out the percentage contribution
C the eigenvalues to the variance.
C
C COPYRIGHT 1990 jpo, Birkbeck College, ICRF, Pfizer,.....
C
C ----------------------------------------------------------------------------
C
      integer      STDOUT, STDERR
      PARAMETER (MAXDAT=300)
      parameter (STDOUT=6, STDERR=0)
C
C ----------------------------------------------------------------------------
C
      character*20 FORM
      character*256 INFILE, OUTFILE, TMPBUF, PRGNAM
      character*10 NAMES(MAXDAT)
      character*4  VERSION
      character*1  CHN
      real         DIST(MAXDAT,MAXDAT), F(MAXDAT,MAXDAT), P(MAXDAT), AVE, VAR, AVAR
      real         DNEW(MAXDAT,MAXDAT), DREF(MAXDAT,MAXDAT)
      logical      EXIST, USEGRAP, FORMATTED, PDBOUT
C
C ----------------------------------------------------------------------------
C             
      data SCALE     /3.5/
      data VERSION   /' 1.0'/
      data FORMATTED /.false./
      data USEGRAP   /.false./
C
C hardwired IO at the moment
C
      data IO /STDOUT/
C
C ----------------------------------------------------------------------------
C
C Valid options are -u for unformatted datafile
C
C Force one trip of do
C
      if (IARGC().eq.0) then
        NARG=1
      else
        NARG=IARGC()
      end if
C
      call getarg(0,PRGNAM)
      if (index(PRGNAM,'mds2pdb') .gt. 0) then
        PDBOUT=.true.
      else
        PDBOUT=.false.
      end if
C
      do I=1,NARG
        call getarg(I,TMPBUF)
        if (INDEX(TMPBUF,'-u').ge.1) then
          FORMATTED=.false.
        else if (INDEX(TMPBUF,'-f').ge.1) then
          FORMATTED=.true.
        else if (INDEX(TMPBUF,'.').ge.1) then
          INFILE=TMPBUF
        else
          write (STDERR,'(''mds2grap - copyright 1990 jpo'')')
          write (STDERR,'(''version: '',A)') VERSION
          write (STDERR,'(''usage: mds2grap -u data.file'')')
          write (STDERR,'(''       -u for unformatted data file (default)'')')
          write (STDERR,'(''       -f for formatted data'')')
          write (STDERR,'(''          format of formatted data is I5/n(A10,nF8.3)'')')
          call exit(1)
        end if
      end do
C
      inquire (FILE=INFILE,EXIST=EXIST)
      if (.NOT.EXIST) then
        write (STDERR,'(''mds2grap: file not found '',A)') INFILE
        call exit(1)
      end if
      OUTFILE=INFILE(1:INDEX(INFILE,'.'))//'grap'
C
C ----------------------------------------------------------------------------
C
      if (FORMATTED) then
        open (UNIT=1,FILE=INFILE,STATUS='OLD',FORM='FORMATTED',
     +        recl=512,err=908)
      else
        open (UNIT=1,FILE=INFILE,STATUS='OLD',FORM='FORMATTED',
     +        err=909)
      end if
C
      if (FORMATTED) then
        read (1,'(I4)',err=920) N
        DO I=1,N
           read (1,'(A10,100F8.3)',err=920) NAMES(I),(DIST(J,I),J=1,N)
        end do
      else
        read (1,*,err=921) N
        DO I=1,N
           read (1,*,err=921) NAMES(I),(DIST(J,I),J=1,N)
        end do
      end if
C 
C Check bounds
C
      if (N .gt. MAXDAT) then
        write (STDERR,'(''mds2grap: too many data sets '',//)')
        call exit(2)
      else if (N .lt. 3) then
        write (STDERR,'(''mds2grap: too few data sets !!'',//)')
        call exit(2)
      end if
C
      close (unit=1)
C
C Check data for metric nature, D(i,j) == D(j,i) and D(i,i)=0
C also check for D(i,j)=0 => i and j are identical
C
      do I=1,N
        if (DIST(I,I).NE.0.0) then
          write (STDERR,'(''mds2grap: data not a distance matrix'')')
          write (STDERR,'(''element: ('',I4,'','',I4,'')  value: '',
     +                    F8.3,'' should be 0.00'')') I,I,DIST(I,I)
          call exit(1)
        end if
        do J=I+1,N
          if (DIST(J,I) .ne. DIST(I,J)) then
            write (STDERR,'(''mds2grap: data is not symmetrical'')')
            write (STDERR,'(''element: ('',I4,'','',I4,'') value: '',
     +             F8.3)') I,J,DIST(I,J)
            write (STDERR,'(''element: ('',I4,'','',I4,'') value: '',
     +             F8.3)') J,I,DIST(J,I)
c            call exit(1)
          end if
          if (DIST(J,I) .eq. 0.0) then
            write (STDERR,'(''mds2grap: identical variates ?'')')
            write (STDERR,'(''element: ('',I4,'','',I4,'')'')') I,J
          end if
        end do
      end do
C
C echo the input distances
C
      write (STDOUT,'(/,''distance data'')')
      do I=1,N
        write (STDOUT,'(A10,300F8.3)') NAMES(I)(1:LASTCHAR(NAMES(I),10)),
     +          (DIST(J,I),J=1,N)
      end do
      do I=1,N
        do J=1,N
          DREF(J,I)=DIST(J,I)
        end do
      end do
C
C calcalute some metrics on the data itself
C
C first the mean
C
      write (STDOUT,'(''number of off diagonal terms:             '',F8.3)') 0.5*real(N**2 - N)
      AVE=0.0
      do I=1,N
        do J=I+1,N
           AVE=AVE+DIST(J,I)
        end do
      end do
      AVE = AVE / (0.5*real(N**2 - N))
      write (STDOUT,'(''mean of off diagonal terms:               '',F8.3)') AVE
C
C now the variance
C
      VAR=0.0
      AVAR=0.0
      do I=1,N
        do J=I+1,N
          VAR=VAR+((DIST(J,I)-AVE)**2)
          AVAR=AVAR+abs(DIST(J,I)-AVE)
        end do
      end do
      VAR = VAR / (0.5*(N**2 - N) - 1.0)
      AVAR = AVAR / (0.5*(N**2 - N))
      write (STDOUT,'(''variance of off diagonal terms:           '',F8.3)') VAR
      write (STDOUT,'(''standard deviation of off diagonal terms: '',F8.3)') SQRT(VAR)
      write (STDOUT,'(''average deviation of off diagonal terms:  '',F8.3)') AVAR
C
C -----------------------------------------------------------------------------
C
      open (unit=1,file=OUTFILE,status='UNKNOWN',form='FORMATTED')
C
C Do the scaling itself
C
      call MDSCALE(DIST,N,MAXDAT,F,P)
C
C ------------------------------------------------------------------------------
C
C output stuff
C
C Find range of data
C
C X and Y are the principle and secondary ordinates respectively
C Z is tertiary
C
      call BOUNDS(F(1,1),N,XMIN,XMAX)
      delx=xmax-xmin
      if (ABS(DELX).lt.1.0E-2) then
        write (STDERR,'(''mds2grap: zero range for primary ordinate''
     +          )')
      end if
      call BOUNDS(F(1,2),N,YMIN,YMAX)
      dely=ymax-ymin
      if (ABS(DELY).lt.1.0E-2) then
        write (STDERR,'(''mds2grap: zero range for secondary ordinate''
     +          )')
      end if
      call BOUNDS(F(1,3),N,ZMIN,ZMAX)
      DELZ=ZMAX-ZMIN
      if (ABS(DELZ).lt.1.0E-2) then
        write (STDERR,'(''mds2grap: zero range for tertiary ordinate''
     +          )')
      end if
C
C Find aspect ratio
C
      aspect=(dely/delx)*scale
      aspect2=(DELZ/DELX)*SCALE
C
C Start grap
C
      if (USEGRAP) then
        write (IO,'(''.DS'')')
        write (IO,'(''.G1'')')
        write (IO,'(''frame invis ht '',f9.2,'' wid '',f9.2)')
     +               aspect,scale
        do I=1,N
          write (IO,'(''"'',A,''" size-2 at '',F9.4,'','',F9.4)') 
     -           names(i)(1:LASTCHAR(names(i),10)),f(i,1),f(i,2)
        end do
        write (IO,'(''line from '',F9.2,'', 0 to '',F9.2,'',0'')')
     +               xmin,xmax
        write (IO,'(''line from 0,'',F9.2,'' to 0,'',F9.2)')
     +             ymin,ymax
        write (IO,'(''.G2'')')
        write (IO,'(''.FG "scaling (1st and 2nd eigenvalues)"'')')
        write (IO,'(''.DE'')')
C
        write (IO,'(''.DS'')')
        write (IO,'(''.G1'')')
        write (IO,'(''frame invis ht '',f9.2,'' wid '',f9.2)')
     +               aspect2,scale
        do I=1,N
          write (IO,'(''"'',A,''" size-2 at '',F9.4,'','',F9.4)') 
     -           names(i)(1:LASTCHAR(names(i),10)),f(i,1),f(i,3)
        end do
        write (IO,'(''line from '',F9.2,'', 0 to '',F9.2,'',0'')')
     +               xmin,xmax
        write (IO,'(''line from 0,'',F9.2,'' to 0,'',F9.2)')
     +               ZMIN,ZMAX
        write (IO,'(''.G2'')')
        write (IO,'(''.FG "scaling (1st and 3rd eigenvalues)"'')')
        write (IO,'(''.DE'')')
      else
        if (PDBOUT) then
          write (IO,'(''REMARK written by mds2grap'')')
          write (IO,'(''REMARK'')')
          do I=1,N
            if (I .le. 26) then
              CHN=char(ichar('A')-1+I)
            else
              CHN=' '
            end if
            write (IO,'(''HETATM'',I5,''  CA '',1X,A4,A1,I4,4X,3F8.3,''  1.00'',F6.2)')
     +            I,NAMES(I)(1:4), CHN, I, F(I,1), F(I,2), F(I,3), F(I,4)
            write (IO,'(''TER'')')
          end do
          write (IO,'(''END'')')
        else
          do I=1,N
            write (IO,'(A,'','',F8.3,'','',F8.3,'',''F8.3)')
     +            names(I)(1:lastchar(NAMES(I),10)), F(I,1), F(I,2), F(I,3)
          end do
        end if
      end if
C
C ----------------------------------------------------------------------
C
C calculate new distance matrices
C
      do I=1,N
        DNEW(I,I)=0.000
        do J=I+1,N
          DNEW(I,J)=((F(I,1)-F(J,1))**2)  + ((F(I,2)-F(J,2))**2)
          DNEW(I,J)=sqrt(DNEW(I,J))
          DNEW(J,I)=DNEW(I,J)
        end do
      end do
      write (IO,'(//,''projected distances'')')
      do I=1,N
        write (IO,'(A10,2X,300F8.3)') names(I)(1:lastchar(NAMES(I),10)),(DNEW(I,J),J=1,N)
      end do
      write (IO,'(//,''residual errors'')')
      do I=1,N
        DSUM=0.00
        do J=1,N
          DSUM=DSUM+abs(DNEW(I,J)-DREF(I,J))
        end do
        write (IO,'(A10,2X,300F8.3)') names(I)(1:lastchar(NAMES(I),10)),(DNEW(I,J)-DREF(I,J),J=1,N),DSUM
      end do
C
C ----------------------------------------------------------------------
C
C Normal exit
C
      if (IO .eq. 1) then
        CLOSE (UNIT=1)
      end if
      call exit(0)
C
C ----------------------------------------------------------------------
C
C Errors
C
999   write (STDERR,'(''mds2grap: user requested exit'',//)')
      call exit(1) 
991   write (STDERR,'(''mds2grap: incomplete data set'',//)')
      call exit(1)
123   write (STDERR,'(''mds2grap: error parsing scale'')')
      call exit(1)
908   write (STDERR,'(''mds2grap: error opening formatted file'')')
      call exit(1)
909   write (STDERR,'(''mds2grap: error opening unformatted file'')')
      call exit(1)
920   write (STDERR,'(''mds2grap: error reading formatted file'')')
      call exit(1)
921   write (STDERR,'(''mds2grap: error reading unformatted file'')')
      call exit(1)
C
      end
C 
C ****************************************************************************
C
      subroutine MDSCALE(DIST,N,NP,F,P)
C
      parameter (MAXDAT=300)
C
C ----------------------------------------------------------------------------
C
      real DIST(NP,NP), E(MAXDAT), V(MAXDAT,MAXDAT), F(NP,NP), P(MAXDAT)
C
C ----------------------------------------------------------------------------
C
C
      if (N .gt. MAXDAT) then
        write (STDERR,'(''mdscale: too many variables '')')
      end if
C
      do I=1,N
        do J=1,N
          DIST(J,I)=DIST(J,I)**2
        end do
      end do
C
      do I=1,N
        TOT=0.0
        do J=1,N
          TOT=TOT+DIST(I,J)
        end do
        AVE=TOT/REAL(N)
        do J=1,N
          DIST(I,J)=DIST(I,J)-AVE
        end do
      end do
C
      do I=1,N
        TOT=0.0
        do J=1,N
          TOT=TOT+DIST(J,I)
        end do
        AVE=TOT/REAL(N)
        do J=1,N
          DIST(J,I)=DIST(J,I)-AVE
        end do
      end do
C
      do I=1,N
        do J=1,N
          DIST(J,I)=-0.5*DIST(J,I)
        end do
      end do
C
C ----------------------------------------------------------------------------
C
C Find eigenvalues and eigenvectors
C
      call JACOBI(DIST,N,NP,E,V,NROT)
C
C Sort on the basis of eigenvalues
C
      call EIGSRT(E,V,N,NP)
C
      do I=1,N
        SCALE=SQRT(ABS(E(I)))
        DO J=1,N
          F(J,I)=SCALE*V(J,I)
        end do
      end do
C
C ----------------------------------------------------------------------------
C
      write (STDOUT,'(/,''sorted Eigenvalues'')')
      write (STDOUT,'(i3,2x,f12.3)') (i,e(i),i=1,n)
      SUM=0.0
      do I=1,N
        SUM=SUM+ABS(E(I))
      end do
      write (STDOUT,'(/,''Eigenvectors'')')
      do I=1,N
        write (STDOUT,'(i3,2x,300f12.3)') i,(f(j,i),j=1,n)
      end do
C
C Check for negative eigenvalues
C
      if (E(N).lt.0.0) then
        write (STDOUT,'(/,''WARNING - negative Eigenvalues !!'')')
      end if
C
C Output percentage contribution to variance 
C
      write (STDOUT,'(/,''percentage contribution to variance'')')
      do I=1,n
        P(I)=(ABS(E(I))/SUM)*100.0
      end do
      write (STDOUT,'(i3,2x,f12.3)') (i,(abs(e(i))/sum)*100.0,i=1,n)
C
C ----------------------------------------------------------------------------
C
      return
      end
C
C ****************************************************************************
C
      subroutine JACOBI(A,N,NP,D,V,NROT)
C
C ----------------------------------------------------------------------------
C
      parameter (NMAX=300, MAXITER=60)
C
C ----------------------------------------------------------------------------
C
      real A(NP,NP), D(NP), V(NP,NP), B(NMAX), Z(NMAX)
C
C ----------------------------------------------------------------------------
C
      do IP=1,N
        do IQ=1,N
          V(IP,IQ)=0.
        end do
        V(IP,IP)=1.
      end do
C
      do IP=1,N
        B(IP)=A(IP,IP)
        D(IP)=B(IP)
        Z(IP)=0.
      end do
      NROT=0
C
      do I=1,MAXITER
        SM=0.
        do IP=1,N-1
          do IQ=IP+1,N
            SM=SM+ABS(A(IP,IQ))
          end do
        end do
C
        if (SM .eq. 0.) return
C
        if (I .lt. 4) then
          TRESH=0.2*SM/N**2
        else
          TRESH=0.
        end if
C
        do IP=1,N-1
          do IQ=IP+1,N
            G=100.*ABS(A(IP,IQ))
            if ((I.GT.4).AND.(ABS(D(IP))+G.EQ.ABS(D(IP))).AND.
     +      (ABS(D(IQ))+G.EQ.ABS(D(IQ)))) then
              A(IP,IQ)=0.
            else if (ABS(A(IP,IQ)).GT.TRESH) then
              H=D(IQ)-D(IP)
              if (ABS(H)+G.EQ.ABS(H)) then
                T=A(IP,IQ)/H
              else
                THETA=0.5*H/A(IP,IQ)
                T=1./(ABS(THETA)+SQRT(1.+THETA**2))
                if (THETA.LT.0.) then 
                  T=-T
                end if
              end if
              C=1./SQRT(1+T**2)
              S=T*C
              TAU=S/(1.+C)
              H=T*A(IP,IQ)
              Z(IP)=Z(IP)-H
              Z(IQ)=Z(IQ)+H
              D(IP)=D(IP)-H
              D(IQ)=D(IQ)+H
              A(IP,IQ)=0.
              do J=1,IP-1
                G=A(J,IP)
                H=A(J,IQ)
                A(J,IP)=G-S*(H+G*TAU)
                A(J,IQ)=H+S*(G-H*TAU)
              end do
              do J=IP+1,IQ-1
                G=A(IP,J)
                H=A(J,IQ)
                A(IP,J)=G-S*(H+G*TAU)
                A(J,IQ)=H+S*(G-H*TAU)
              end do
              do J=IQ+1,N
                G=A(IP,J)
                H=A(IQ,J)
                A(IP,J)=G-S*(H+G*TAU)
                A(IQ,J)=H+S*(G-H*TAU)
              end do
              do J=1,N
                G=V(J,IP)
                H=V(J,IQ)
                V(J,IP)=G-S*(H+G*TAU)
                V(J,IQ)=H+S*(G-H*TAU)
              end do
              NROT=NROT+1
            end if
          end do
        end do
        do IP=1,N
          B(IP)=B(IP)+Z(IP)
          D(IP)=B(IP)
          Z(IP)=0.
        end do
      end do
C
C ----------------------------------------------------------------------------
C
C Trap too many iterations
C
      write (STDERR,'(''mds2grap: too many iterations in JACOBI '')')
      stop '    '
C 
      end
C
C ****************************************************************************
C
      subroutine EIGSRT(D,V,N,NP)
C
C ----------------------------------------------------------------------------
C
      real D(NP), V(NP,NP)
C
C ----------------------------------------------------------------------------
C
      do I=1,N-1
        K=I
        P=D(I)
        do J=I+1,N
          if (D(J) .GE. P) then
            K=J
            P=D(J)
          endIF
        end do
        if (K .NE. I) then
          D(K)=D(I)
          D(I)=P
          DO J=1,N
            P=V(J,I)
            V(J,I)=V(J,K)
            V(J,K)=P
          end do
        end if
      end do
C
C ----------------------------------------------------------------------------
C
      return
      end
C
C ****************************************************************************
C
      SUBROUTINE BOUNDS(VESTOR,N,VMIN,VMAX)
      save
C
      real VESTOR(N), VMIN, VMAX
C
      VMIN=1.E20
      VMAX=-1.E20
C     
      do I=1,N
        if (VESTOR(I).GT.VMAX) then
          VMAX=VESTOR(I)
        end if
        if (VESTOR(I).LT.VMIN) then
          VMIN=VESTOR(I)
        end if
      end do
C
      return
      end

C ****************************************************************************

      integer function LASTCHAR(STRING,LENSTRING)
C
      character*(*)  STRING
      integer        LENSTRING,J
      logical        ISPRINT
C
      LASTCHAR=LENSTRING
      do J=LENSTRING,1,-1
        if (ISPRINT(STRING(J:J))) then
          LASTCHAR=J
          return
        end if
        LASTCHAR=J
      end do
C
      return
      end

C ****************************************************************************

      logical function ISPRINT(CHAR)
C
      character*1 CHAR
      integer     N
C
      N=ICHAR(CHAR)
      if ((N.ge.33).and.(N.le.126)) then
        ISPRINT=.TRUE.
        return
      end if
      ISPRINT=.FALSE.
C
      return
      end
