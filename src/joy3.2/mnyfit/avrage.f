      subroutine avrage()
C
      include 'mnyfit.h'
      include 'het.h'
C
      character*7 FNAM(MAXMOL)
      real AM(3,3)
C
C --------------------------------------------------------------------------------
C
      mmols=1
      TOLER=0.00001
C
C --------------------------------------------------------------------------------
C
      do k=1,nm
        fnam(k) = fname(k)(1:7)
        do j=1,nf
          do I=1,3
            xf(I,j,k) = coords(I,na(J,k),k)
          end do
        end do
C
C this is where the coords are translated to the origin, need to do a 
C similar thing for the hetatoms
C N.B. This calls a variant of the usual origin routine
C
        call origin2(xf(1,1,k), coords(1,1,k), hcoords(1,1,k),
     +               nf, natoms(k), nhatoms(k))
      end do
      fnam(nm+1)='Framewo'
C
      if (mmols .eq. 0) then
        mmols = nm
      end if
C
      rn = 1.0/real(nm)
C
      do i=1, mmols
        do k=1, nm
          do j=1, nf
            wt(j,k) = wta(k)
          end do
        end do
        do j = 1, nf
          do k=1,3
            xb1(k,j) = xf(k,j,i)
          end do
        end do
        call mnyfit()
        do j = 1, nf
          do k=1,3
            tmp(k,j,i) = xb1(k,j)
          end do
        end do
        do k = 1, nm
          call matfit(xb1, xf(1,1,k), am, nf, 1, wt(1,k))
          call rotfit(xf(1,1,k), am, nf)
          call rmsfit(xb1, xf(1,1,k), rms, nf, 1, wt(1,k))
          dists(i,k) = rms
        end do
        do k=1,nm
          rms=0.
          do j=1,nf
            rms=rms + (wt(j,k) * ((((xb1(1,j) - xf(1,j,k)) ** 2) +
     +                             ((xb1(2,j) - xf(2,j,k)) ** 2)) +
     +                             ((xb1(3,j) - xf(3,j,k)) ** 2)))
          end do
          distm(i,k)=sqrt(rms/real(nf))
        end do
      end do
C
      do i=1, mmols
        do j=1, mmols
          call matfit(tmp(1,1,j), tmp(1,1,i), am, nf, 0, 0.)
          call rotfit(tmp(1,1,i), am, nf)
          call rmsfit(tmp(1,1,j), tmp(1,1,i), dists(i,j), nf, 0, 0.)
        end do
      end do
C
      nm1=nm+1
      fname(nm1) = 'FRAMEWORK'
      do j=1,nf
        do k=1,3
          xf(k,j,nm1) = xb1(k,j)   ! was xf(1,j,nm1) = xb1(1,j)  jpo 9-5-96
        end do
      end do
C
C fit each structure to each structure in a pairwise manner
C
      do i = 1, nm1
        dists(i,i) = 0.
        do j = i + 1, nm1
          call matfit(xf(1,1,i), xf(1,1,j), am, nf, 0, 0.)
          call rotfit(xf(1,1,j), am, nf)
          call rmsfit(xf(1,1,i), xf(1,1,j), rms, nf, 0, 0.)
          dists(i,j) = rms
          dists(j,i) = rms
        end do
      end do
C
      write (STDOUT,'(''# Pairwise Root Mean Square fits'')')
      call matout(fnam, dists, nm1, NF)
C
      do k = 1, nm
        do j = 1, nf
          do i=1,3
            xf(i,j,k) = coords(i,na(j,k),k)
          end do
        end do
      end do
C
      do i=1, nm
        call matfit(tmp(1,1,1), xf(1,1,i), am, nf, 1, wt(1,i))
        call rotfit(xf(1,1,i), am, nf)
        call rotfit(coords(1,1,i), am, natoms(i))
        call rotfit(hcoords(1,1,i), am, nhatoms(i))
      end do
C
      return
C
      end
