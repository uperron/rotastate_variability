      subroutine dismat(k1,k2,dmat,smat,chk1,chk2,contr,nmb1,nmb2)
c
c     *************************************************************
c
c     subroutine to calculate the distance matrix for two 
c     superposed co-ordinate sets
c
c     PJT 26-04-90
c
c     -------------------------------------------------------------
c
      include 'mnyfit.h'
c
      integer dmat(MAXRES,MAXRES)
      integer smat(MAXRES,MAXRES)
      integer contr(MAXMOL)
      integer NMB1, NMB2, NMB3, CHK1, CHK2
      real xtem, ytem, ztem
c
c     -------------------------------------------------------------------
c
c     smat and dmat are the inter ca distance matrices for comparisons
c                   of real structures or frameworks.
c     contr         are the structures contributing to a framework if
c                   one is to be used to get the distance matrix.
c     nmb1 and nmb2 are the number of structures contributing to a
c                   framework where one is used.
c     chk1 and chk2 describe if the structures to be compared are 
c                   structures or frameworks.
c     x,y,ztem      are temporary variables used to calculate framework
c                   co-ords.
c
c     --------------------------------------------------------------------
c     determine number of positions to be compared (depends if whole 
c     or framework is used to derive the matrix.
c     --------------------------------------------------------------------
c
      ind1=nfitca(k1) + 1
      ind2=nfitca(k2) + 1
      nmb3=nmb2
      if( chk1 .ne. 0 .or. chk2 .ne. 0) then
        nmb3=nmb2-nmb1
      end if
      do i=1,ind1
        do j=1,ind2
          smat(i,j)=0
          dmat(i,j)=0
        end do
      end do
c
c     --------------------------------------------------------------------
c     determine co-ords to be used (actual if a single structure is used
c     average if a framework is used) and calculate the distance matrix.
c     --------------------------------------------------------------------
c
      do i = 1, ind1
        if (chk1 .eq. 0) then
          x1 = xf(1,i,k1)
          y1 = xf(2,i,k1)
          z1 = xf(3,i,k1)
        else
          do ii = 1,nmb1
            xtem=xtem+xf(1,na(i,contr(ii)),contr(ii))
            ytem=ytem+xf(2,na(i,contr(ii)),contr(ii))
            ztem=ztem+xf(3,na(i,contr(ii)),contr(ii))
          end do
          x1 = xtem/nmb1
          y1 = ytem/nmb1
          z1 = ztem/nmb1
          xtem=0
          ytem=0
          ztem=0
        end if
        do j = 1, ind2
          if (i.eq.ind1.or.j.eq.ind2) then
            smat(i,j) = 0
            dmat(i,j) = 0
          end if
          if(chk2.eq.0) then
            x2 = xf(1,j,k2)
            y2 = xf(2,j,k2)
            z2 = xf(3,j,k2)
          else
            do jj = (nmb1+1),nmb2
              xtem=xtem+xf(1,na(j,contr(jj)),contr(jj))
              ytem=ytem+xf(2,na(j,contr(jj)),contr(jj))
              ztem=ztem+xf(3,na(j,contr(jj)),contr(jj))
            end do
            x2=xtem/nmb3
            y2=ytem/nmb3
            z2=ztem/nmb3
            xtem=0
            ytem=0
            ztem=0
          end if
c
c     --------------------------------------------------------------------
c     calculate entry in distance matrix as 1000*(10-Dij) for Dij < 10
c     --------------------------------------------------------------------
c 
          tmpd = (((x1 - x2) * (x1 - x2)) +
     +            ((y1 - y2) * (y1 - y2)) + 
     +            ((z1 - z2) * (z1 - z2)))
          if (tmpd .gt. 100.0) then
            tmpd = 10.0
          else
            tmpd=sqrt(tmpd)
          end if
          smat(i,j) = int(1000.0 * (10.0 - tmpd))
          dmat(i,j) = int(1000.0 * (10.0 - tmpd))
        end do
      end do
C
      return 
      end
