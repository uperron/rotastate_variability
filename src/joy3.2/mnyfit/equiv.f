      subroutine equiv()
C
      include 'mnyfit.h'
c
      integer nb1(maxats)
C
      write(IO,'(''# Number of equivalences = '',i4)') NF
C
      do k = 1, nm
        m = 0
        do j = 1, nf
          n = 1
          l = 1
          if (na(j,k) .eq. n) then
            if ((select .eq. 'CA  ') .and. (atnam(l,k) .eq. ' CA ')) then
              m = m + 1
              nb1(m) = l
            else
              if (select .eq. 'MAIN') then
                if ((((atnam(l,k) .eq. ' CA ') .or. (atnam(l,k) .eq. ' C  '))
     &           .or. (atnam(l,k) .eq. ' N  ')) .or. (atnam(l,k) .eq. ' O  ')) then
                  m = m + 1
                  nb1(m) = l
                end if
              end if
            end if
          end if
C
          do l = 2, natoms(k)
            if (numres(l,k) .ne. numres(l - 1,k)) n = n + 1
              if (na(j,k) .eq. n) then
                if ((select .eq. 'CA  ') .and. (atnam(l,k) .eq. ' CA ')) then
                  m = m + 1
                  nb1(m) = l
                else
                  if (select .eq. 'MAIN') then
                    if ((((atnam(l,k) .eq. ' CA ') .or. (atnam(l,k) .eq. ' C  '))
     &               .or. (atnam(l,k) .eq. ' N  ')) .or. (atnam(l,k) .eq. ' O  ')) then
                      m = m + 1
                      nb1(m) = l
                  end if
                end if
              end if
            end if
          end do
        end do
        do j = 1, m
          na(j,k) = nb1(j)
        end do
      end do
C
      if (nf .le. 2) then
        write(STDERR,'(''mnyfit: number of points being fitted < 3'')')
        call EXIT(1)
      end if
C
      end
