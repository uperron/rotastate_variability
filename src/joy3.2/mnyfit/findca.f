      subroutine FINDCA()
c
c Only use CA's to determine residues with one and only one 
c nearest neighbour.
c
      include 'mnyfit.h'
c
      do k=1, NM
        L=0
        do J=1, NATOMS(K)
          if (atnam(j,K) .eq. ' CA ') then
            L=L+1
            rnum(L,K)=numres(J,K)(2:5)
            na(L,K)=L
            do I=1,3
              xf(I,L,K)=coords(I,J,K)
            end do
          end if
        end do
        if (L .gt. MAXRES) then
          write (STDERR,'(''mnyfit: too many residues in '',A)') FNAME(K)
          call EXIT(1)
        end if
        numbca(K) = L
        nfitca(K) = L
      end do
C
      return
      end
C
C ============================================================================
C
      subroutine fndca2()
c
c Only use CA's to determine residues with one and only one 
c nearest neighbour.
c
      include 'mnyfit.h'
c
      do k=1,nm
        l=0
        do j=1, natoms(k)
          if (atnam(j,k) .eq. ' CA ') then
            l=l+1
          end if
        end do
        numbca(k)=l
        nfitca(k)=l
      end do
C
      return
      end
