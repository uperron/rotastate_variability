      program MNYFIT
C
C This program performs a simultaneous structural alignment of many
C structures.
C
C Originally written by:
C
C   M.J. Sutcliffe
C
C While in the group of Tom Blundell at Birkbeck College
C
C Substantial modifications, extensions and bug-fixes by:
C
C  J.P. Overington
C  F. Eisenmenger
C  P.J. Thomas (and M.S. Johnson)
C  
C There is additionally code from 
C
C  I. Haneef
C  S. Remmington
C
C ==================================================================
C
      include 'mnyfit.h'
      include 'het.h'
C
C MAXITER is the maximum number of iterations around the big fitting loop
C
      integer MAXITER
      parameter (MAXITER=20)
C
      character*256 string
      character*78 SPACER
      character*(MAXFILELEN) FILIN, seqfil, BUFFER
      character*8 test, BLANK
      integer nff(maxmol), prevna(maxats), istruc(maxres)
      logical MULTI, bkfi, bkffi, EXIST
      logical DOREPAIR, USEREF, OPHORIZ
C
      integer P(MAXMOL-1),Q(MAXMOL-1)
      real T(MAXMOL-1)
C
      integer NSET
      character*12 SET(MAXMOL)
      logical INSET
C
      include 'version.h'
C
C =====================================================================
C
      MAXCYCLE=100
C
      FILEXT='brk       '
      select='CA  '
      i = 0
      j = 0
      k = 0
      nm = 0
      MULTI=.false.
      bkfi=.false.
      bkffi=.false.
      DOREPAIR=.false.
      USEREF=.false.
      OPHORIZ=.false.
      COMPMIN=3.0
      SPACER='# ============================================================================'
      do I=1,MAXMOL
        WTA(I) = 1.0
        ERR(I) = 0.1
      end do
      NSET=0
      INSET=.false.
C
C ==========================================================================
C
C by default have stdin as input stream
C
      FILIN='stdin'
      IIN=STDIN
C
C load and parse options
C
      NARG=IARGC()
C
C jpo - added new options 3-7-97
C       -x to set extension for fitted pdb files (default remains brk)
C       -R to treat first file as reference data set, all files after fitting
C          will be fitted to this.
C jpo - removed -R option, left some residual code
C
      do I=1,NARG
        call GETARG(I,BUFFER)
        if (BUFFER(1:1) .eq. '-') then
          if (BUFFER(2:2) .eq. 'u') then
            MULTI=.true.
          else if (BUFFER(2:2) .eq. 'n') then
            MULTI=.false.
          else if (BUFFER(2:2) .eq. 'h') then
            call HELP()
          else if (BUFFER(2:2) .eq. 'f') then
            BKFI=.true. 
          else if (BUFFER(2:2) .eq. 'A') then
            OPHORIZ=.true. 
          else if (BUFFER(2:2) .eq. 'F') then
            BKFFI=.true. 
          else if (BUFFER(2:2) .eq. 'r') then
            DOREPAIR=.true. 
          else if (BUFFER(2:2) .eq. 'x') then
            FILEXT='          '
            read (BUFFER(3:),'(A)',err=907) FILEXT
            if (FILEXT(1:1) .eq. ' ') then
              FILEXT='brk       '
              write (STDERR,'(''mnyfit: warning - fitted filename extension reset to brk'')')
            end if
          else if (BUFFER(2:2) .eq. 'd') then
            read (BUFFER(3:),'(f5.2)',err=906) COMPMIN
            if (COMPMIN .eq. 0.0) then
              write (STDERR,'(''mnyfit: zero equivalence cutoff (format -dN.n  no space between d and N)'')')
              call EXIT(1)
            end if
          else
            write (STDERR,'(''mnyfit: unknown option '',A)') BUFFER(1:lastchar(BUFFER))
          end if
        else
          FILIN=BUFFER
          IIN=191
        end if
      end do
C
C ============================================================================
C
C  Open file containing input data.
C
      if (IIN .eq. 191) then
        open(unit=IIN, file=filin, status='OLD', form='FORMATTED',err=900) 
      end if
C
      call setdat(date)
      write (IO,'(''# produced by mnyfit - version '',A)') VERSON
      write (IO,'(''#   compiled '',A)') DATE
      write (IO,'(''#'')')
C
      call GETENV('MNYDAT',DIRDAT)
      if (DIRDAT(1:1) .eq. ' ') then
        write (STDERR,'(''mnyfit: environment variable MNYDAT is not defined'')')
        call EXIT(3)
      end if
C
C Main input loop, the format for this is quite strange and pernickety
C
  500 read (IIN,fmt='(A)',end=1000) string
      test = string(1:8)
C
C COMMENTS
C
      if (TEST .eq. '........') then
        goto 500
      else if (TEST(1:1) .eq. '#') then
        goto 500
C
C WEIGHT
C
      else if (test(1:6) .eq. 'WEIGHT') then
        read (string(9:),fmt='(F5.3)') wta(nm)
        goto 500
C
C CHAINS
C
      else if (test(1:6) .eq. 'CHAINS') then
        nchn=0
        do jj=1, 10
          jc=7+((jj-1)*2)
          if (string(jc:jc) .eq. '*') goto 502
          if (string(jc+1:) .ne. '*') then
            nchn=nchn+1
            if (nchn .gt. 1) then
cmchn              mchn(nm)=.true.
              write (STDERR,'(''mnyfit: multiple chains nchn = '',I4)') nchn
            endif
            chains(nchn,nm)=string(jc+1:)
          end if
        end do
  502   jc = 0
        do jj = 1, nchn
          do kk = jj + 1, nchn
            if (chains(jj,nm) .eq. chains(kk,nm)) goto 5021
          end do
          jc = jc + 1
          chains(jc,nm) = chains(jj,nm)
 5021     continue
        end do
        nchn = jc
        if (jc .gt. 0) then
          selchn(nm) = .true.
          nchain(nm) = nchn
        end if
        goto 500
C
C MOLECULE
C
      else if (test(1:3) .eq. 'MOL') then
        nm = nm + 1
        if (nm .gt. maxmol) then
          write (STDERR,'(''mnyfit: too many molecules '')')
          call EXIT(1)
        end if
        selchn(nm) = .false.
        read (string,'(8X,A)',err=904,end=902) fname(nm)
        j = i
        i = 0
C
C EQUIVALENCE RANGE
C
      else if (test .eq. 'RANGE A=' .or. test .eq. 'A       ') then
        i = i + 1
        read (string,'(9X,A6,1X,A6)',end=902) range(i,1,nm), range(i,2,nm)
C
C SET
C
      else if (test(1:4) .eq. 'SET=') then
        NSET = NSET + 1
        read (string,'(A)',end=902) SET(NSET)
        write (STDOUT,'(''found set '',I3,'' named '',A)') NSET,SET(NSET)
        INSET=.true.
C
C END SET
C
      else if (test(1:7) .eq. 'END SET') then
        INSET=.false.
        continue
C
C END OF INPUT
C
      else if (test(1:3) .eq. 'END') then
          goto 510
C
C unhandled cases
C
      else
        write (STDERR,'(''mnyfit: unrecognized keyword '',A,
     +                  '' in '',A)') TEST, FILIN(1:lastchar(FILIN))
      end if
      goto 500
C
C Process input, sanity check, etc.
C
  510 numprn = k
      numran = i
C
      if ((nm .ge. 2) .and. (i .ne. j)) then
        write (STDERR,'(''mnyfit: inconsistent number of ranges in '',A)') FILIN
        call EXIT(1)
      end if
C
C produce a summary of data read so far
C
      write (IO,'(A)') SPACER
      write (IO,'(''# no of coordinate sets:      '',I3))') NM
      do I=1,NM
        write (IO,'(''#   '',I3,1X,A)') I,FNAME(I)(1:LASTCHAR(FNAME(I)))
      end do
      write (IO,'(''# mnyfit data directory:       '',A)') DIRDAT(1:LASTCHAR(DIRDAT))
      write (IO,'(''# fitted filename extension:   '',A)') FILEXT(1:LASTCHAR(FILEXT))
      write (IO,'(''# reference data set fitting: '',L2)') USEREF
      write (IO,'(''# update equivalences:        '',L2)') MULTI
      write (IO,'(''# produce fitted coordinates: '',L2)') BKFI
      write (IO,'(''# produce fitted framework:   '',L2)') BKFFI
      write (IO,'(''# repair sidechains:          '',L2)') DOREPAIR
      write (IO,'(''# equivalence cutoff:         '',F5.2,'' Angstrom'')') COMPMIN
      write (IO,'(''# number of sets:             '',I3)') NSET

      write (IO,'(A)') SPACER
C
C ======================================================================
C
C read in coordinates
C
      write (IO,'(''# reading coordinate data:'')')
      do I=1, NM
        write (IO,'(''# reading dataset '',I4,2X,A)') I,FNAME(I)
        call mchain(i,fname(i),nff,DOREPAIR)
      end do
C
C check for equal no of residues in frameworks
C
      do i=1,nm-1
        if (nff(i+1) .ne. nff(i)) then
          write (STDERR,'(''mnyfit: unequal number of residues '',/,
     +                    I4,'' in '',A,/,I4,'' in '',A)')
     +                    nff(i), FNAME(I), nff(i + 1), fname(i + 1)
          call EXIT(1)
        end if
      end do
      write (IO,'(A)') SPACER
C
C =======================================================================
C
      write (IO,'(''# fitting:'')')
      write (IO,'(''#'')')
C
C Avrage is called which performs the alignment.
C
      call avrage()
C
C refine equivalences if requested
C
      if (MULTI) then
        write (STDOUT,'(''# refining equivalences'')')
        write (IO,'(''#'')')
        do i = 1, MAXITER
          nf1 = nf
          call findca()
          call nearest()
          call equiv()
          call avrage()
          if (nf .eq. nf1) then
            nftot = 0
            do j=1, nf
              if (prevna(j) .eq. na(j,1)) nftot = nftot + 1
            end do
            if (nftot .eq. nf) goto 991
          end if
          do j = 1, nf
            prevna(j) = na(j,1)
          end do
        end do
        write (STDERR,'(''mnyfit: warning:: reached iteration limit'')')
C
  991   continue
C
      end if
C
C ======================================================================================
C
C linkage data
C
      write (IO,'(A)') SPACER
      write (IO,'(''# cluster linkage:'')')
      call LINKER(NM,DISTS,Q,P,T)
      do I=1,NM-1
        write (STDOUT,'(''# '',I3,1X,A5,1X,I3,'' - '',A5,1X,I3,2X,F8.3)')
     +         I,FNAME(Q(I)),Q(I),
     +           FNAME(P(I)),P(I),T(I)
      end do
C
C MDS data
C
      write (IO,'(A)') SPACER
      write (IO,'(''# multidimensional scaling:'')')
      call MDSCALE(DISTS,FNAME,NM+1,MAXMOL)
      write (IO,'(A)') SPACER
C
C equivalence list
C
      call lequiv(OPHORIZ)
C
C if want fitted coordinates write them out
C
      if (bkfi) then
        if (USEREF) then
C
C if want reference set fitting, then need to read in coord set 1, and fit
C first fitted set to this, require rotation, then translation, then apply to
C all sets.
C
          write (STDERR,'(''mnyfit: reference fitting not fully implemented'')')
C
C read in first coord set (again)
C
          call MCHAIN(NM+2,fname(1),nff,DOREPAIR)
C
C origin reference set, remembering offset vector
C
          
          call FOUT()
        else
          call FOUT()
        end if
      end if
C
C if want fitted frameworks then
C
      call fndca2()
      call mremove()
      if (bkffi) then
        call fout2()
      end if
C
 1000 continue
C
      if (IIN .ne. STDIN) then
        close (unit=IIN)
      end if
      if (IO .ne. STDOUT) then
        close(unit=IO) 
      end if
C
C Normal program exit      
C
      call EXIT(0)
C
C IO errors
C
900   write (STDERR,'(''mnyfit: cannot open file '',A)') FILIN(1:lastchar(FILIN))
      call EXIT(1)
902   write (STDERR,'(''mnyfit: premature EOF on input stream'')')
      call EXIT(1)
903   write (STDERR,'(''mnyfit: error parsing string for logical'',A)') STRING
      call EXIT(1)
904   write (STDERR,'(''mnyfit: error reading file name'')')
      call EXIT(1)
906   write (STDERR,'(''mnyfit: error parsing real value '',A)') STRING
      call EXIT(1)
907   write (STDERR,'(''mnyfit: error reading file extension '',A)') STRING
      call EXIT(1)
C
      end
