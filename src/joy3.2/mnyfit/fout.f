      subroutine fout()
C
C writes out a PDB file, with extension .brk
C
      include 'mnyfit.h'
      include 'het.h'
C
      do n=1, nm
        nchar = index(fname(n),'.')
        open (99, file=fname(n)(1:nchar)//FILEXT(1:lastchar(FILEXT)), 
     +  status='UNKNOWN', form='FORMATTED', err=900) 
        write (99,'(''REMARK Produced by mnyfit - version '',A5)') VERSON
        write (99,'(''REMARK'')')
        write (99,'(''REMARK fitted coordinates derived from '',A)')
     +         FNAME(N)(1:lastchar(FNAME(N)))
        write (99,'(''REMARK'')')
        do j=1, natoms(n)
          write (99,'(''ATOM  '',i5,1x,a4,1x,a3,1x,a1,a5,3x,3f8.3,2f6.2)')
     +           j, atnam(j,n), namres(j,n), chain(j,n), numres(j,n),
     +           coords(1,j,n), coords(2,j,n), coords(3,j,n), occup(j,n),
     +           tfact(j,n)
        end do
C
C handle case of terminal OXT
C
c       if (atnam(natoms(n)+1,n) .eq. ' OXT') then
c         j=natoms(n)+1
c         write (99,'(''ATOM  '',i5,1x,a4,1x,a3,1x,a1,a5,3x,3f8.3,2f6.2)')
c    +           j, atnam(j,n), namres(j,n), chain(j,n), numres(j,n),
c    +           coords(1,j,n), coords(2,j,n), coords(3,j,n), occup(j,n),
c    +           tfact(j,n)
c       end if
        write (99,'(''TER'')')
C
C write out the hetatoms
C
        if (NHATOMS(N) .gt. 0) then
          do J=1,NHATOMS(N)
            write (99,'(''HETATM'',I5,1x,a4,1x,a3,1x,a1,a5,3x,3f8.3,2f6.2)')
     +             NATOMS(N)+J, hatnam(j,n), hnamres(j,n), hchain(j,n),
     +             hnumres(j,n), hcoords(1,j,n), hcoords(2,j,n), hcoords(3,j,n),
     +             hoccup(j,n), htfact(j,n)
          end do          
          write (99,'(''TER'')')
        end if
        write (99,'(''END'')')
        close (99) 
      end do
C
      return 
C
900   write (STDERR,'(''mnyfit: error opening fitted file for '',A)')
     +       fname(n)(1:nchar)
C
      end
C
C ============================================================================
C
      subroutine fout2(dirout)
C
C writes out framework regions from mnyfit
C
C jpo 7-4-99 - added temperature factor output
C
c     Common blocks for mnyfit  
c
      include 'mnyfit.h'
c
      do n = 1, nm
        nchar = index(fname(n),'.')
        open(unit=99, file=fname(n)(1:nchar - 1)// '.frm',
     &  status='UNKNOWN', form='FORMATTED', err=900) 
c
        write (99,'(''REMARK Produced by mnyfit - version '',A5)') VERSON
        write (99,'(''REMARK'')')
        write (99,'(''REMARK framework coordinates'',A)')
        write (99,'(''REMARK'')')
        write (6,*) 'writing out nf ', nf
        do k = 1, nf
          j = na(k,n)
          write (99,'(''ATOM  '',a5,1x,a4,1x,a3,2x,a5,3x,3f8.3,2f6.2)')
     +    atnum(j,n), atnam(j,n), namres(j,n), 
     +    numres(j,n), coords(1,j,n), coords(2,j,n), coords(3,j,n),
     +    occup(j,n), tfact(j,n)
        end do
        write (99,'(''TER'')')
        close (99) 
      end do
c 
c Now output the framework itself
C
      open (unit=99, file='framework.frm', status='UNKNOWN',
     +      form='FORMATTED',err=901) 
      write (99,'(''REMARK Produced by mnyfit - version '',A5)') VERSON
      write (99,'(''REMARK'')')
      write (99,'(''REMARK framework coordinates'',A)')
      write (99,'(''REMARK'')')
      do i = 1, nf
        write (99,'(''ATOM  '',i5,1x,4h CA ,1x,''FRM'',1x,i5,4x,3f8.3,2f6.3)')
     +         i, i, (tmp(j,i,1),j = 1, 3), aver(i), sdev(i)
      end do
      write (99,'(''TER'')')
      close (99) 
C
      return 
C
900   write (STDERR,'(''mnyfit: error opening file '',A)')
     +       FNAME(N)(1:NCHAR-1)//'.frm'
      call exit(1)
901   write (STDERR,'(''mnyfit: error opening file framework.frm'')')
      call exit(1)
C
      end
