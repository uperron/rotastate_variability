      subroutine HELP()
C
C simple help message for mnyfit
C
      include 'mnyfit.h'
C
      write (STDERR,'(''mnyfit - a program for multiple protein structure superposition'')')
      write (STDERR,'('' '')')
      write (STDERR,'(''usage mnyfit -options input-file/stdin'')')
      write (STDERR,'(''      options: -u    update equivalences'')')
      write (STDERR,'(''               -n    do not update equivalences'')')
      write (STDERR,'(''               -h    print help message'')')
      write (STDERR,'(''               -f    produce fitted coordinates'')')
      write (STDERR,'(''               -F    produce fitted frameworks'')')
      write (STDERR,'(''               -dN.n set equivalence cutoff'')')
      call EXIT(0)
      end
