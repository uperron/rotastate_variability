C assignments for hetatom arrays in MNYFIT
C
      integer MAXHET
      parameter (MAXHET=8000)
C
      common /H1/  HATNUM, HATNAM, HNAMRES, HNUMRES, HCOORDS, NHATOMS, HOCCUP,
     +             HTFACT, HCHAIN
C
      real HCOORDS(3, MAXHET, MAXMOL)
      real HOCCUP(MAXHET,MAXMOL), HTFACT(MAXHET,MAXMOL)
      integer NHATOMS(MAXMOL)
      character*5 HNUMRES(MAXHET, MAXMOL), HATNUM(MAXHET, MAXMOL)
      character*4 HATNAM(MAXHET, MAXMOL)
      character*3 HNAMRES(MAXHET, MAXMOL)
      character*1 HCHAIN(MAXHET, MAXMOL)
