      subroutine lequiv(OPHORIZ)
C
C jpo 24-7-97  want to intorduce code to list horizontal style alignments
C              from data
C 
      include 'mnyfit.h'
C
      character*1024 BUFFER
      character*1  ALBUFF(50,1000), SPACE
      integer RES2IND
      character*1 THR2ONE, INV, FLAG
      integer NG(MAXMOL)		! the length of each internal loop
      integer LENBREAK(40),LENCORE(40)	        ! hardcoded !!! max length of internal loops
      logical BREAK, OPHORIZ
C
      SPACE='-'
C
      write (IO,'(''# Sequence alignment corresponding to '',
     +      ''structurally conserved regions'')')
      write (IO,'(''#'')')
C
C want to run through the list of equiv posn's
C NM is no of molecules
C
      write (IO,'(''# N av ds st dv mx dv mn dv '',60(A6,1X))')
     +       (FNAME(J)(1:INDEX(FNAME(J),'.')-1),J=1,NM)
C
C get the N-terminal loop lengths
C
C flag the loops that are different lengths
C
      FLAG='|'
      do K=1,NM
        MAXNTERM=0
        NG(K)=RES2IND(NUMRES(NA(1,K),K),K)-1
        if (NG(K) .gt. MAXNTERM) then
          MAXNTERM=NG(K)
        end if
        if (K .gt. 1) then
          if (NG(K) .ne. NG(1)) then
            FLAG='X'
          end if
        end if
      end do
      write (IO,'(''# = break ============='',60(A,I3,A),I3)') ('=(',NG(K),')=',K=1,NM),FLAG,MAXNTERM
C
C NB is the number of breaks found in equivalences
C
      NB=0
      IIND=1
C
      do I=1,NF
        FLAG='|'
        BREAK=.false.
        if (I .ge. 2) then
          do K=1,NM
            NG(K)=0
            if (NUMRES(NA(I-1,K),K) .ne. NUMRES(NA(I,K)-4,K)) then
              NG(K)=RES2IND(NUMRES(NA(I,K)-4,K),K) - RES2IND(NUMRES(NA(I-1,K),K),K)
              if (K .gt. 1) then
                if (NG(K) .ne. NG(1)) then
                  FLAG='X'
                end if
              end if
              BREAK=.true.
            end if
          end do
        end if
C
        if (BREAK) then
          NB=NB+1
          LENBREAK(NB)=0
          do K=1,NM
            if (NG(K) .gt. LENBREAK(NB)) then
              LENBREAK(NB)=NG(K)
            end if
          end do
          write (IO,'(''# = break ============='',60(A,I3,A),I3,I3)')
     +           ('=(',NG(K),')=',K=1,NM),FLAG,LENBREAK(NB)
          do K=1,NM
            ALBUFF(K,IIND)=SPACE
          end do
          IIND=IIND+1
        end if
C
C flag the invariant positions
C
        INV='*'
        do K=2,NM
          if (NAMRES(NA(I,K),K) .ne. NAMRES(NA(I,1),1)) then
            INV=' '
            goto 1
          end if
        end do
1       continue
C
        write (IO,'(I3,F6.3,F6.3,F6.3,F6.3,1X,A1,60(1X,A1,A5))')
     +         I,AVER(I),SDEV(I),MDEV(I),MINDEV(I),INV,
     +         (THR2ONE(NAMRES(NA(I,K),K)),NUMRES(NA(I,K),K),K=1,NM)
        do K=1,NM
          ALBUFF(K,IIND)=THR2ONE(NAMRES(NA(I,K),K))
c          write (6,*) ' filling ',K,IIND,ALBUFF(K,IIND)
        end do
      IIND=IIND+1
      end do
C
C finally the C-terminal length loops
C
      FLAG='|'
      do K=1,NM
        MAXCTERM=0
        NG(K)=NRESI(K)-RES2IND(NUMRES(NA(NF,K),K),K)
        if (NG(K) .gt. MAXCTERM) then
          MAXCTERM=NG(K)
        end if
        if (K .gt. 1) then
          if (NG(K) .ne. NG(1)) then
            FLAG='X'
          end if
        end if
      end do
      write (IO,'(''# = break ============='',60(A,I3,A),I3)')
     +       ('=(',NG(K),')=',K=1,NM),FLAG,MAXCTERM
C

C
C -----------------------------------------------------------------------
C
C new code for horizontal format output, surprisingly tough !
C
      if (OPHORIZ) then
        write (IO,'(''#'')')
        write (IO,'(''# draft alignment based on overlap'')')
        write (IO,'(''#'')')
C
        do K=1,NM
          write (6,'(''>P1;''A)') FNAME(K)
          write (6,'(A)') FNAME(K)
          write (6,'(1000A)') (ALBUFF(K,I),I=1,IIND),'*'
        end do
C
C
cd cd       do K=1,NM
cd          NB=0
cd          write (BUFFER(1:10),'(A10)') FNAME(K)
cd          N=11
cd          if (MAXNTERM .gt. 0) then
cd            do J=1,MAXNTERM
cd              BUFFER(N:N)='.'
cd              N=N+1
cd            end do
cd          end if
C
C insertion bug
C
cd          do I=1,NF
cd            if (I .ge. 2) then
cd              if (NUMRES(NA(I-1,K),K) .ne. NUMRES(NA(I,K)-4,K)) then
cd                NB=NB+1
cd                do J=1,LENBREAK(NB)
cd                  BUFFER(N:N)='.'
cd                  N=N+1
cd                end do
cd              else
cd                do J=K+1,NM
cd                  if (NUMRES(NA(I-1,J),J) .ne. NUMRES(NA(I,J)-4,J)) then
cd                    NB=NB+1
cd                    do M=1,LENBREAK(NB)
cd                      BUFFER(N:N)='.'
cd                      N=N+1
cd                    end do
cd                  end if
cd                end do
cd              end if
cd            end if
cd            write (BUFFER(N:N),'(A)') THR2ONE(NAMRES(NA(I,K),K))
cd            N=N+1
cd          end do
C
cd          if (MAXCTERM .gt. 0) then
cdcd            do J=1,MAXCTERM
cd              BUFFER(N:N)='.'
cd              N=N+1
cd            end do
cd          end if

cd          write (IO,'(A)') buffer(1:lastchar(buffer))       
cd        end do
      end if
C
      return
      end
C
C =========================================================
C
      character*1 function THR2ONE(INPUT)
C
      implicit none
      character*3 INPUT
      character*24 AA1
      character*3 AA3(24)
      integer J
C
      data AA1/'SGVTALDIPKQNFYERCHWMBZXJ'/
      data AA3/'SER','GLY','VAL','THR','ALA',
     -         'LEU','ASP','ILE','PRO','LYS',
     -         'GLN','ASN','PHE','TYR','GLU',
     -         'ARG','CYS','HIS','TRP','MET',
     -         'ASX','GLX','UNK','CYH'/
C
      do J=1,24
        if (INPUT .eq. AA3(J)) then
          THR2ONE=AA1(J:J)
          return
        end if
      end do
C
      THR2ONE='X'
C
      return
      end
