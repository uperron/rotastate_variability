      subroutine matfit(x1, x2, rm, n, iw, wt1)
c
c     SUBROUTINE TO FIT THE COORD SET X2(3,N) TO THE SET X1(3,N)
c     IN THE SENSE OF:
c            XA= R*XB +V
c     R IS A UNITARY 3.3 RIGHT HANDED ROTATION MATRIX
c     AND V IS THE OFFSET VECTOR. THIS IS AN ANALYTIC SOLUTION
c
c     IF ENTRY IS LOGICALLY FALSE ONLY THE RMS COORDINATE ERROR
c     WILL BE RETURNED (BUT QUICKLY)
c
c    THIS SUBROUTINE IS THAT ACCORDING TO MCLACHLAN (1982). SEE:
c     MCLACHAN, A.D., ACTA CRYST.(1982),A38,871-873.
c
c     AMMENDED BY M.J. SUTCLIFFE   DECEMBER 1985
c
c
      include 'mnyfit.h'
c     Common blocks for mnyfit  
c
      dimension umat(3,3), rm(3,3), wt1(maxats), x1(3,maxats), x2(3,maxats)
C
      if (nf .lt. 3) then
        write(STDERR,'(''mnyfit: number of points < 3 : '',I2)') NF
        call EXIT(1)
      end if
C
      if (iw .le. 0) then
        do i = 1, 3
          do j = 1, 3
            umat(i,j) = 0.0
          end do
          do j = 1, n
            do k = 1, 3
              umat(i,k) = umat(i,k) + (x1(i,j) * x2(k,j))
            end do
          end do
        end do
      else
        do i = 1, 3
          do j = 1, 3
            umat(i,j) = 0.0
          end do
          do j = 1, n
            do k = 1, 3
              umat(i,k) = umat(i,k) + ((wt1(j) * x1(i,j)) * x2(k,j))
            end do
          end do
        end do
c
      end if
C
      call qikfit(umat, rm)
C
      return 
      end
