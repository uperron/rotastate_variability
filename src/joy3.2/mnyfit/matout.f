      subroutine MATOUT(NAMES, MAT, N, NFRAMEWORK)
C
C prints out pairwise distance matrix of superposition
C
      include 'mnyfit.h'
C
      character*(*) NAMES(MAXMOL)
      character*1 COMMENT
      integer I, K, NLL, N, NFRAMEWORK
      real mat(MAXMOL, MAXMOL)
C
      COMMENT='#'
C
C ====================================================================================
C
C problems here with lengths of names etc. Will have to live with truncation (for now)
C note hardcoded format length here (100 on 3-7-97)
C
      write (STDOUT,'(A)') COMMENT
      do i=1,n-1
        NLL=index(NAMES(I),'.')-1
        if (NLL .lt. 1) then
          NLL=7
        end if
        write (STDOUT,'(''  '',I3,1X,A9,100(1X,F5.3))') I,NAMES(I)(1:NLL),(MAT(I,K),K=1,N) 
      end do
      write (STDOUT,'(''      Framework'',100(1X,F5.3))') (MAT(N,K),K=1,N) 
      write (STDOUT,'(''  '')') 
C
C new section (9-10-01) for output of normalised RMSD scores as per Prot Sci article 2001
C
      write (STDOUT,'(A)') COMMENT
      write (STDOUT,'(''  RMSD100 values'')') 
      do i=1,n-1
        NLL=index(NAMES(I),'.')-1
        if (NLL .lt. 1) then
          NLL=7
        end if
        write (STDOUT,'(''  '',I3,1X,A9,100(1X,F6.3))')
     +         I,NAMES(I)(1:NLL),((MAT(I,K)/(1.0+log(sqrt(real(NFRAMEWORK)/100.0)))),K=1,N)
      end do
      write (STDOUT,'(''      Framework'',100(1X,F5.3))') ((MAT(N,K)/(1.0+log(sqrt(real(NFRAMEWORK)/100.0)))),K=1,N) 
      write (STDOUT,'(''  '')') 
C
      return
      end
