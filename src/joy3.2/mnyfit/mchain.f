      subroutine mchain(ION, fil, nff, DOREPAIR)
C
C =====================================================================
C
      include 'mnyfit.h'
      include 'het.h'
C
C =====================================================================
C
      integer ranlen(20), nff(maxmol), nats(maxres)
C
C =====================================================================
C
      logical endran, staran
      logical DOREPAIR
      character*80 card
      character*24 fil
      character*6 resnum(2, maxres), test
      character*4 resnam(2, maxres)
C
C Open the PDB format file
C
      ii=ION+10
C
      open (unit=ii, form='FORMATTED', file=fil, status='OLD', err=900) 
C
      i = 0
      m = 0
      j = 0
      k = 0
C
C Main Read loop
C
    1 read (ii, '(A80)', end=300) card
C
      test = card(1:6)
      if (test .eq. 'ATOM  ') then
       if (selchn(ION)) then
         do jj = 1, nchain(ION)
           if (card(22:22) .eq. chains(jj,ION)) goto 6031
         end do
         goto 1
       end if
 6031  j = j + 1
       read (card, 604) atnum(j,ION), atnam(j,ION), namres(j,ION), chain(j,ION),
     +       numres(j,ION), coords(1,j,ION), coords(2,j,ION), coords(3,j,ION),
     +       OCCUP(J,ION), TFACT(J,ION)
  604  format(6x,a5,1x,a4,1x,a3,1x,a1,a5,3x,3f8.3,2f6.2)
C
C new stuff for hetatoms, read in all of them
C
      else if (TEST .eq. 'HETATM') then
        m = m + 1
        if (M .lt. MAXHET) then
          read (card, 604) hatnum(m,ION), hatnam(m,ION), hnamres(m,ION),
     +                     hchain(m,ion), hnumres(m,ION), hcoords(1,m,ION),
     +                     hcoords(2,m,ION), hcoords(3,m,ION), HOCCUP(M,ION),
     +                     HTFACT(M,ION)
        else 
          write (STDERR,'(''mnyfit: too many hetatoms - list trucated'')')
        end if
      end if
c
c NATOMS is the number of atoms in a particular file
C NHATOMS is the number of hetatoms in a particular file
c
      goto 1
c
c sort the atoms
c
  300 natoms(ION) = j
c    write (6,*) natoms(ion), ion, ' natoms(ion), ion'
      NHATOMS(ION) = M
c
      call sort(ION, DOREPAIR)
      endran = .false.
      staran = .false.
      i1 = 1
C
      do ij = 1, numran
        ranlen(ij) = 0
      end do
C
      do 200 j = 1, natoms(ION)
        if (i1 .gt. numran) goto 7
        if (staran .and. endran) then
          if (numres(j,ION) .ne. range(i1,2,ION)(2:6)) then
            staran = .false.
c           WRITE (STDERR,606)I1,RANGE(I1,1,ION),RANGE(I1,2,ION),RANLEN(I1)
            endran = .false.
  606       format(//5hRANGE,2x,i2,5x,a6,6h  TO  ,a6,5x,i4,6h ATOMS)
            i1 = i1 + 1
          end if
        end if
        if (range(i1,1,ION)(2:6) .eq. numres(j,ION)) then
          staran = .true.
        end if
        if (range(i1,2,ION)(2:6) .eq. numres(j,ION)) then
          endran = .true.
        end if
    7   if (select .eq. 'CA  ') then
          if (atnam(j,ION) .eq. ' CA ') then
            continue
          else
            goto 200
          end if
        else if (select .eq. 'MAIN') then
        if ((((atnam(j,ION) .eq. ' N  ') .or. (atnam(j,ION) .eq. ' CA '))
     &   .or. (atnam(j,ION) .eq. ' C  ')) .or. (atnam(j,ION) .eq. ' O  ')) 
     &    then
            continue
          else
            goto 200
          end if
        end if
        if (staran) then
          ranlen(i1) = ranlen(i1) + 1
          k = k + 1
          na(k,ION) = j
c         write(STDERR, fmt=*) k, numres(j,ION), namres(j,ION), atnam(j,ION), 
c         &ranlen(i1)
        end if
c
  200 continue
c
c Test to check that if select = main that whole of NATS(I)
c contains a value of 4 and return error if it does not
c
      if ((i1 .gt. numran) .or. ((i1 .eq. numran) .and. (numres(natoms(
     &ION),ION) .eq. range(i1,2,ION)(2:6)))) then
      if (select .eq. 'MAIN') then
      do 310 i1 = 1, i
      if (nats(i1) .ne. 4) then
        write (STDERR,'(''mnyfit: residue '',A,A,'' has missing mainchain atoms'')') resnum(ION,i1), resnam(ION,i1)
       call EXIT(1) 
      end if
  310 continue
      end if
      else
        write(STDERR,'(''mnyfit: range '',I3,''not found in '',A)') I1,FIL
        call EXIT(1)
      end if
C
      nf = k
      nff(ION) = nf
c
      close(unit=ii) 
      return 
C
900   write (STDERR,'(''mnyfit: cannot open file '',A)') FIL
      call EXIT(1)
C
      end
