      SUBROUTINE MDSCALE(DIST,TITLE,N,NP)
C
C MAXMOL should really be coupled to mnyfit.h
C
      include 'mnyfit.h'
c      PARAMETER (MAXMOL=30)
C ----------------------------------------------------------------------------
C
      character*(*) TITLE(MAXMOL)
      real DIST(NP,NP), E(MAXMOL), V(MAXMOL,MAXMOL), F(MAXMOL,MAXMOL), P(MAXMOL)
C
C ----------------------------------------------------------------------------
C
C the last structure is, of course, the framework
C
      TITLE(N)(1:5)='frame'
C
      if (N .gt. MAXMOL) then
        write (STDERR,'(''mdscale: too many variables '')')
        return
      end if
C
      DO I=1,N
        DO J=1,N
          DIST(J,I)=DIST(J,I)**2
        end do
      end do
C
      DO I=1,N
        TOT=0.0
        DO J=1,N
          TOT=TOT+DIST(I,J)
        end do
        AVE=TOT/REAL(N)
        DO J=1,N
          DIST(I,J)=DIST(I,J)-AVE
        end do
      end do
C
      DO I=1,N
        TOT=0.0
        DO J=1,N
          TOT=TOT+DIST(J,I)
        end do
        AVE=TOT/REAL(N)
        DO J=1,N
          DIST(J,I)=DIST(J,I)-AVE
        end do
      end do
C
      DO I=1,N
        DO J=1,N
          DIST(J,I)=-0.5*DIST(J,I)
        end do
      end do
C
C ----------------------------------------------------------------------------
C
C Find eigenvalues and eigenvectors
C
      call JACOBI(DIST,N,NP,E,V,NROT)
C
C Sort on the basis of eigenvalues
C
      call EIGSRT(E,V,N,NP)
C
      DO I=1,N
        SCALE=SQRT(ABS(E(I)))
        DO J=1,N
          F(J,I)=SCALE*V(J,I)
        end do
      end do
C
C ----------------------------------------------------------------------------
C
      write (STDOUT,'(''# sorted Eigenvalues'')')
      write (STDOUT,'(''#       '',30I7)') (I,i=1,n)
      write (STDOUT,'(''#         '',30f7.3)') (e(i),i=1,n)
      SUM=0.0
      DO I=1,N
        SUM=SUM+ABS(E(I))
      end do
      write (STDOUT,'(''# Eigenvectors'')')
      do I=1,N
        NL=index(TITLE(I),'.')-1
        if (NL .lt. 0) then
          NL=lastchar(TITLE(I))
        end if
        write (STDOUT,'(''# '',i2,1X,A5,60f7.3)')
     +         I,TITLE(I)(1:NL),(F(I,J),J=1,N)
      end do
C
C Output percentage contribution to variance 
C
      write (STDOUT,'(''# percentage contribution to variance'')')
      DO I=1,n
        P(I)=(ABS(E(I))/SUM)*100.0
      end do
      write (STDOUT,'(''#         '',30f7.3)') ((abs(e(i))/sum)*100.0,i=1,n)
C
C ----------------------------------------------------------------------------
C
      return
      end
C
C ****************************************************************************
C
      SUBROUTINE JACOBI(A,N,NP,D,V,NROT)
C
C ----------------------------------------------------------------------------
C
      PARAMETER (NMAX=100,
     -           MAXITER=50)
      integer STDERR
      parameter (STDERR=0)
C
C ----------------------------------------------------------------------------
C
      real A(NP,NP),
     -     D(NP),
     -     V(NP,NP),
     -     B(NMAX),
     -     Z(NMAX)
C
C ----------------------------------------------------------------------------
C
      DO IP=1,N
        DO IQ=1,N
          V(IP,IQ)=0.
        end do
        V(IP,IP)=1.
      end do
C
      DO IP=1,N
        B(IP)=A(IP,IP)
        D(IP)=B(IP)
        Z(IP)=0.
      end do
      NROT=0
C
      DO I=1,MAXITER
        SM=0.
        DO IP=1,N-1
          DO IQ=IP+1,N
            SM=SM+ABS(A(IP,IQ))
          end do
        end do
C
        if (SM.EQ.0.) return
C
        if (I.LT.4) then
          TRESH=0.2*SM/N**2
        ELSE
          TRESH=0.
        end if
C
        DO IP=1,N-1
          DO IQ=IP+1,N
            G=100.*ABS(A(IP,IQ))
            if ((I.GT.4).AND.(ABS(D(IP))+G.EQ.ABS(D(IP))).AND.(ABS(D(IQ))+G.EQ.ABS(D(IQ)))) then
              A(IP,IQ)=0.
            ELSE if (ABS(A(IP,IQ)).GT.TRESH) then
              H=D(IQ)-D(IP)
              if (ABS(H)+G.EQ.ABS(H)) then
                T=A(IP,IQ)/H
              ELSE
                THETA=0.5*H/A(IP,IQ)
                T=1./(ABS(THETA)+SQRT(1.+THETA**2))
                if (THETA.LT.0.) then 
                  T=-T
                end if
              end if
              C=1./SQRT(1+T**2)
              S=T*C
              TAU=S/(1.+C)
              H=T*A(IP,IQ)
              Z(IP)=Z(IP)-H
              Z(IQ)=Z(IQ)+H
              D(IP)=D(IP)-H
              D(IQ)=D(IQ)+H
              A(IP,IQ)=0.
              DO J=1,IP-1
                G=A(J,IP)
                H=A(J,IQ)
                A(J,IP)=G-S*(H+G*TAU)
                A(J,IQ)=H+S*(G-H*TAU)
              end do
              DO J=IP+1,IQ-1
                G=A(IP,J)
                H=A(J,IQ)
                A(IP,J)=G-S*(H+G*TAU)
                A(J,IQ)=H+S*(G-H*TAU)
              end do
              DO J=IQ+1,N
                G=A(IP,J)
                H=A(IQ,J)
                A(IP,J)=G-S*(H+G*TAU)
                A(IQ,J)=H+S*(G-H*TAU)
              end do
              DO J=1,N
                G=V(J,IP)
                H=V(J,IQ)
                V(J,IP)=G-S*(H+G*TAU)
                V(J,IQ)=H+S*(G-H*TAU)
              end do
              NROT=NROT+1
            end if
          end do
        end do
        DO IP=1,N
          B(IP)=B(IP)+Z(IP)
          D(IP)=B(IP)
          Z(IP)=0.
        end do
      end do
C
C ----------------------------------------------------------------------------
C
C Trap too many iterations
C
      write (STDERR, '(''mdscale: too many iterations'')')
      call EXIT(1)
C 
C ----------------------------------------------------------------------------
C
      end
C
C ****************************************************************************
C
      SUBROUTINE EIGSRT(D,V,N,NP)
C
C ----------------------------------------------------------------------------
C
      real D(NP),
     -     V(NP,NP)
C
C ----------------------------------------------------------------------------
C
      DO I=1,N-1
        K=I
        P=D(I)
        DO J=I+1,N
          if (D(J).GE.P) then
            K=J
            P=D(J)
          endIF
        end do
        if (K.NE.I) then
          D(K)=D(I)
          D(I)=P
          DO J=1,N
            P=V(J,I)
            V(J,I)=V(J,K)
            V(J,K)=P
          end do
        endIF
      end do
C
C ----------------------------------------------------------------------------
C
      return
      end
