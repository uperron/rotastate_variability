C     Common blocks for mnyfit
C
      integer MAXRES, MAXATS, MAXMOL, MAXFILELEN, MAXCYCLE, MAXCHAIN
      integer STDERR, STDOUT, STDIN, IO
      parameter (STDERR=0, STDOUT=6, STDIN=5)
      parameter (MAXRES=1100, MAXATS=10*MAXRES, MAXMOL=10, 
     -           MAXCHAIN=5, MAXFILELEN=256)
      parameter (IO=STDOUT)
C
      common /ATMLST/ COORDS, TMP, NM
      common /FIT1/ NF, NA, NATOMS
      common /FIT2/ XF, NANEW
      common /FIT3/ WT, WTA, W1
      common /FIT4/ XB1, XB2, ERR
      common /FIT5/ NUMBCA, NFITCA, COMPMIN
      common /FIT6/ DISTS, DISTM
      common /FIT7/ RN, TOLER, MAXCYCLE
      common /FIT8/ NBLOCK, LENGTH, NSTART, DIRDAT
      common /LOGI/ WEIGHT
      common /NAM/ ATNAM, ATNUM, NAMRES, NUMRES, LETT, RNUM
      common /OUTP/ FNAME, DATE, VERSON
      common /RANGN/ NUMRAN, RANGE, SELECT, SDEV, AVER, MDEV, MINDEV
      common /CHAIN1/ SELCHN, NCHAIN, CHAINS, CHAIN
      common /FILBIT/ FILEXT
      common /OCCTFAC/ OCCUP, TFACT
C
cmchn      common /CHAIN/ SELCHN, NCHAIN, CHAINS, MCHN, STCHN
C
C jpo
C
      common /JPO1/ NRESI, RESID
C
      character*80 DIRDAT
      character*24 FNAME(MAXMOL)
      character*10 FILEXT
      character*9 DATE
      character*6 RANGE(20, 2, MAXMOL)   ! up to 20 separate chain ranges
      character*5 VERSON
      character*5 NUMRES(MAXATS, MAXMOL)
      character*5 RESID(MAXRES,MAXMOL)
      character*4 ATNAM(MAXATS, MAXMOL)
      character*4 SELECT
      character*4 RNUM(MAXRES,MAXMOL)
      character*3 NAMRES(MAXATS, MAXMOL)
      character*1 CHAINS(MAXCHAIN, MAXMOL)
      character*1 LETT(MAXATS, MAXMOL), CHAIN(MAXATS, MAXMOL)
C
      real COORDS(3, MAXATS, MAXMOL)
      real OCCUP(MAXATS, MAXMOL), TFACT(MAXATS, MAXMOL)
      real TMP(3, MAXATS, MAXMOL)
      real XF(3,MAXATS,MAXMOL)
      real WT(MAXATS,MAXMOL), WTA(MAXMOL), W1(MAXATS)
      real XB1(3,MAXATS), XB2(3,MAXATS), ERR(MAXMOL)
      real DISTS(MAXMOL, MAXMOL), DISTM(MAXMOL, MAXMOL)
      real COMPMIN
      real SDEV(MAXATS), MDEV(MAXATS), MINDEV(MAXATS)
      real AVER(MAXATS)
      real RN
      real TOLER
C
      integer NM
      integer NF
      integer NRESI(MAXMOL)
      integer NA(MAXATS, MAXMOL)
      integer NATOMS(MAXMOL)
      integer NANEW(MAXATS,MAXMOL)
      integer NUMBCA(MAXMOL)
      integer NFITCA(MAXMOL)
      integer NBLOCK
      integer LENGTH(50)
      integer NSTART(50)
      integer NUMRAN
      integer NCHAIN(MAXCHAIN)
      integer ATNUM(MAXATS, MAXMOL)
      integer STCHN(MAXCHAIN,MAXMOL)
C
      logical WEIGHT
      logical SELCHN(MAXMOL)
      logical MCHN(MAXMOL)
C
