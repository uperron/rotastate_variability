      subroutine mnyfit()
c
      include 'mnyfit.h'
c
      real am(3, 3), d(maxmol)
c
      do j = 1, nf
        do K=1,3
          xb2(K,j) = xb1(K,j)
        end do
      end do
C
      oldfun = 0.
      icyc = 0
      rnm = 1.0 / float(nm)
c
    2 icyc = icyc + 1
      if (icyc .le. MAXCYCLE) then
        do j = 1, nf
         ww = 0.
         do i = 1, nm
           ww = ww + wt(j,i)
         end do
         w1(j) = 1. / ww
        end do
        do i = 1, nm
          call matfit(xb2, xf(1,1,i), am, nf, 1, wt(1,i))
          call rotfit(xf(1,1,i), am, nf)
        end do
        do j = 1, nf
          do K=1,3
            xb1(K,j) = 0.
          end do
        end do
        do i = 1, nm
          do j = 1, nf
            w = wt(j,i)
            xb1(1,j) = xb1(1,j) + (w * xf(1,j,i))
            xb1(2,j) = xb1(2,j) + (w * xf(2,j,i))
            xb1(3,j) = xb1(3,j) + (w * xf(3,j,i))
          end do
        end do
    8   continue
        do 9 j = 1, nf
         w = w1(j)
         xb1(1,j) = w * xb1(1,j)
         xb1(2,j) = w * xb1(2,j)
         xb1(3,j) = w * xb1(3,j)
    9   continue
        call matfit(xb2, xb1, am, nf, 0, 0.)
        call rotfit(xb1, am, nf)
        call rmsfit(xb2, xb1, rms, nf, 0, 0.)
        fun = 0.
        do 11 i = 1, nm
          do 111 j = 1, nf
           fun = fun + (wt(j,i) * ((xb1(1,j) - xf(1,j,i)) ** 2
     +                           + (xb1(2,j) - xf(2,j,i)) ** 2
     +                           + (xb1(3,j) - xf(3,j,i)) ** 2))
  111     continue
   11   continue
        if (abs(fun - oldfun) .le. toler) then
           goto 17
        end if
c
c Find the distance of each point from the framework, for each residue in
c the framework
c
c Mean distance AVER contains the average distance and SDEV the standard
c dev'n
c
        oldfun = fun
        do j = 1, nf
          dmean = 0.
          do i = 1, nm
            d(i) = SQRT(((xb1(1,j) - xf(1,j,i)) ** 2)
     +                + ((xb1(2,j) - xf(2,j,i)) ** 2)
     +                + ((xb1(3,j) - xf(3,j,i)) ** 2))
            dmean = dmean + d(i)
          end do
          dmean = dmean * rnm
          aver(j) = dmean
          sd = 0.
          do i=1, nm
            sd=sd+ ((d(i) - dmean) **2)
          end do
          sd=sd*rnm
          sdev(j) = sqrt(sd)
          do i = 1, nm
            wt(j,i) = wta(i) / (err(i) + (d(i) * sd))
          end do
        end do
C
C MDEV is the maximum deviation
C 
        do j = 1, nf
          mdev(j)=0.000
          mindev(j)=99.99
          do i=1,nm
            do k=i+1,nm
              dd=SQRT(((xf(1,j,k) - xf(1,j,i)) ** 2)
     +              + ((xf(2,j,k) - xf(2,j,i)) ** 2)
     +              + ((xf(3,j,k) - xf(3,j,i)) ** 2))
              if (dd .gt. MDEV(j)) then
                MDEV(j)=dd
              end if
              if (dd .lt. mindev(j)) then
                mindev(j)=dd
              end if
            end do
          end do
        end do
C
        do j = 1, nf
          x = 0.
          y = 0.
          z = 0.
          ww = 0.
          do i = 1, nm
            w = wt(j,i)
            x = x + (w * xf(1,j,i))
            y = y + (w * xf(2,j,i))
            z = z + (w * xf(3,j,i))
            ww = ww + w
          end do
          rw = 1. / ww
          xb1(1,j) = rw * x
          xb1(2,j) = rw * y
          xb1(3,j) = rw * z
        end do
C
        do j = 1, nf
          xb2(1,j) = xb1(1,j)
          xb2(2,j) = xb1(2,j)
          xb2(3,j) = xb1(3,j)
        end do
        goto 2
c
      end if
C
   17 write(IO,'(''#'')')
      write(IO,'(''# convergence after '',i3,'' cycles in mnyfit'')') ICYC
C
      return
      end
