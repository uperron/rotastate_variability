      subroutine mremove()
c
c-- Removes initial set of equivalences where the block is less than 
c-- three residues long.
c
      include 'mnyfit.h'
c
      integer gap(50)
c  Increased dimension of gap from 21
c
      do 5 k = 1, nm
      ncalph = 0
      nnf = 1
      do 5 j = 1, natoms(k)
      if (atnam(j,k) .eq. ' CA ') then
      ncalph = ncalph + 1
      nanew(ncalph,k) = j
      if (nnf .le. nf) then
      if (na(nnf,k) .eq. j) then
      na(nnf,k) = ncalph
      nnf = nnf + 1
      end if
      end if
      end if
c
c     call onelet
c
c-- Determine where the discontinuities are.
c
    5 continue
      len = 1
      nblock = 1
      nstart(1) = 1
      do 10 j = 2, nf
      if (na(j,1) .eq. (na(j - 1,1) + 1)) then
      len = len + 1
      else
      length(nblock) = len
      len = 1
      nblock = nblock + 1
      gap(nblock) = (na(j,1) - na(j - 1,1)) - 1
      nstart(nblock) = j
      end if
   10 continue
c
c-- Remove equivalences where LENGTH .LE. 2 for subsequent sequence alig
cnment.
c
      length(nblock) = len
      nbl = 0
      nequiv = 0
      nrem = 0
      do 100 ncyc = 1, nblock
      nbl = nbl + 1
      if (nbl .gt. nblock) goto 100
      if (length(nbl) .le. 2) then
      nrem = nrem + 1
      if (nequiv .gt. 0) then
      do 20 m = nequiv + 1, nf
      tmp(1,m,1) = tmp(1,m + length(nbl),1)
      tmp(2,m,1) = tmp(2,m + length(nbl),1)
      tmp(3,m,1) = tmp(3,m + length(nbl),1)
   20 continue
      else
      do 30 m = 1, nf - length(nbl)
      tmp(1,m,1) = tmp(1,m + length(nbl),1)
      tmp(2,m,1) = tmp(2,m + length(nbl),1)
      tmp(3,m,1) = tmp(3,m + length(nbl),1)
   30 continue
      end if
      else
      do 90 j = 1, length(nbl)
      nequiv = nequiv + 1
      do 90 k = 1, nm
      nanew(nequiv,k) = (na(nstart(nbl),k) + j) - 1
   90 continue
      length(nbl - nrem) = length(nbl)
c      WRITE(6,*) 'NCYC = ',NCYC,' NBL = ',NBL,' NEQUIV = ',NEQUIV,
c     -           ' NREM =',NREM
      end if
c      WRITE(6,*) 'NEQUIV = ',NEQUIV,' NBLOCK =',NBLOCK,
c     -           ' NREM = ',NREM
  100 continue
      nf = nequiv
      nblock = nblock - nrem
      do 110 k = 1, nm
      do 110 j = 1, nf
      na(j,k) = nanew(j,k)
  110 continue
      end
