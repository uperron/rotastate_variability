      subroutine nearest()
c
c     neardev    -- replacement for the routine nearest which 
c                   performs the search for topological equivalence
c                   requires the subroutines dismat and salign.
c                   Dismat calculates the inter CA distance matrix
c                   which is used by salign
c                   Salign uses the Needlemann and Wunsch algorithm
c                   to perform a structural alignment based on the
c                   matrix of inter CA distances.
c
c                   Initially all the structures are compared in a
c                   pairwise manner and a scoring matrix is 
c                   constructed which contains all the pairwise 
c                   scores. At this time an alignment is not
c                   performed this is controlled by the logical 
c                   variable FIRST.
c                   The scoring matrix is then used to determine the
c                   order in which structures should be combined to
c                   deduce the framework.
c                   
c     author     -- 2-4-90 PJT 
c                   based on ideas from M.S.Johnson and
c                   Fitch and Margoliash (1967) Science vol 15
c                   279-284
c     -------------------------------------------------------------
c
      include 'mnyfit.h'
c
      integer smat(MAXRES, MAXRES)
      integer dmat(MAXRES, MAXRES)
      integer score(MAXMOL,MAXMOL)
      integer scr
      integer count
      integer noc(MAXMOL)
      integer contr(MAXMOL,MAXMOL)
      integer prev(MAXMOL)
      real    tcomp
      logical rem(MAXRES), FIRSTTIME
c
      FIRSTTIME=.true.
      if (FIRSTTIME) then
        FIRSTTIME=.false.
        do I=1,MAXRES
          REM(I)=.false.
        end do
      end if
C
c     -------------------------------------------------------------------
c
c     smat and dmat are copies of the inter CA distance matrix for a
c                   pairwise comparison of structures either whole
c                   structures or intermediate frameworks.
c     count         this holds the current internode number.
c     noc           holds the number of structure contributing to a
c                   given internode.
c     contr         holds the molecule numbers of the structures 
c                   contributing to a given internode.
c     prev          for each structure holds the number of the last
c                   internode to which its structure contributed.
c     score         this is the scoring matrix for all pairwise
c                   comparisons.
c     rem           logical which is set to true for residues which
c                   appear in the framework but which are not real
c                   topological equivalences. This flags these
c                   residues for removal from the framework.
c     -------------------------------------------------------------------
c 
c     print*,' nf = ',nf
      do 1 i=1,nm
        prev(i)=0
        noc(i)=0
1     continue
      do 51 i=1,10
        do 52 j=1,10
          contr(i,j)=0
52      continue
51    continue
c
c     -------------------------------------------------------------------
c     set up scoring matrix for all pairwise structural alignments
c     -------------------------------------------------------------------
c
      do 1000 k1 = 1, nm
      do 1001 k2 = k1, nm
      if (k1 .eq. k2) goto 1001
      call dismat(k1,k2,dmat(1,1),smat(1,1),0,0,1,1,1)
      call salign(smat(1,1), dmat(1,1), numbca(k1), numbca(k2), 
     & .TRUE., scr) 
      score(k1,k2)=scr
 1001 continue
 1000 continue
      count=0
c
c     -------------------------------------------------------------------
c     search scoring matrix for next highest pairwise score
c     -------------------------------------------------------------------
c
c
27    max=0
      do 21 i=1,(nm-1)
        do 22 j=i,nm
          if(score(i,j).gt.max) then
            max=score(i,j)
            ind1=i
            ind2=j
          endif
22      continue
21    continue
c
c     -------------------------------------------------------------------
c     set highest scoring element to 0 so it is not found next time
c     -------------------------------------------------------------------
c
      if(max.eq.0) goto 28
      score(ind1,ind2)=0
c
c     -------------------------------------------------------------------
c     perform alignments and update equivalences
c
c     (1) For the case where neither structure has been 'used' before
c     -------------------------------------------------------------------
c
      if(prev(ind1).eq.0.and.prev(ind2).eq.0) then
        count=count+1
        noc(count)=2
        contr(1,count)=ind1
        contr(2,count)=ind2
        call dismat(ind1,ind2,dmat(1,1),smat(1,1),prev(ind1),prev(ind2),
     -contr(1,count),1,1)
        call salign(smat(1,1),dmat(1,1),numbca(ind1),numbca(ind2),
     -.FALSE.,scr,contr(1,count),noc(count),noc(count),prev(ind1),
     -prev(ind2))
        prev(ind1)=count
        prev(ind2)=count
        if(noc(count).eq.nm) then
          goto 28
        else
          goto 27
        endif
      endif
c
c     -------------------------------------------------------------------
c     (2) Where structure i has been 'used' before
c     -------------------------------------------------------------------
c
      if(prev(ind1).ne.0.and.prev(ind2).eq.0) then
        count=count+1
        noc(count)=noc(prev(ind1)) + 1
        do 23 k=1,noc(prev(ind1))
          contr(k,count)=contr(k,prev(ind1))
23      continue
        contr(noc(count),count)=ind2
        call dismat(ind1,ind2,dmat(1,1),smat(1,1),prev(ind1),prev(ind2),
     -contr(1,count),noc(prev(ind1)),noc(count))
        call salign(smat(1,1),dmat(1,1),nfitca(ind1),numbca(ind2),
     -.FALSE.,scr,contr(1,count),noc(count),noc(prev(ind1)),
     -prev(ind1),prev(ind2))
        do 43 ll=1,noc(count)
        prev(contr(ll,count))=count
43      continue
        if(noc(count).eq.nm) then
          goto 28
        else
          goto 27
        endif
      endif
c
c     -------------------------------------------------------------------
c     (3) where structure j has been used before
c     -------------------------------------------------------------------
c
      if(prev(ind1).eq.0.and.prev(ind2).ne.0) then
        count=count+1
        noc(count)=noc(prev(ind2)) + 1
        contr(1,count)=ind1
        do 24 k=2,noc(count)
          contr(k,count)=contr((k-1),prev(ind2))
24      continue
        call dismat(ind1,ind2,dmat(1,1),smat(1,1),prev(ind1),prev(ind2),
     -contr(1,count),1,noc(count))
        call salign(smat(1,1),dmat(1,1),numbca(ind1),nfitca(ind2),
     -.FALSE.,scr,contr(1,count),noc(count),noc(prev(ind2)),
     -prev(ind1),prev(ind2))
        do 42 ll=1,noc(count)
        prev(contr(ll,count))=count
42      continue
        if(noc(count).eq.nm) then
          goto 28
        else
          goto 27
        endif
      endif
c
c     -------------------------------------------------------------------
c     (4) where both structures i and j have been 'used' before
c         but where the 'used' structures comprising the internodes
c         to be joined have no common members between them.
c     -------------------------------------------------------------------
c
      if(prev(ind1).ne.0.and.prev(ind2).ne.0) then
      do 31 ll=1,noc(prev(ind1))
      do 32 mm=1,noc(prev(ind2))
      if(contr(ll,prev(ind1)).eq.contr(mm,prev(ind2))) 
     -then
        if(prev(ind1).gt.prev(ind2)) then
          prev(ind2)=prev(ind1)
          noc(prev(ind2))=noc(prev(ind1))
        endif
        if(prev(ind1).lt.prev(ind2)) then
          prev(ind1)=prev(ind2)
          noc(prev(ind1))=noc(prev(ind2))
        endif
        goto 27
      endif
32    continue
31    continue
        count=count+1
        noc(count)=noc(prev(ind1)) + noc(prev(ind2))
        do 25 k=1,noc(prev(ind1))
          contr(k,count)=contr(k,prev(ind1))
25      continue
        do 26 k=1,noc(prev(ind2))
          contr((k+noc(prev(ind1))),count)=contr(k,prev(ind2))      
26      continue
        call dismat(ind1,ind2,dmat(1,1),smat(1,1),prev(ind1),prev(ind2),
     -contr(1,count),noc(prev(ind1)),noc(count))
        call salign(smat(1,1),dmat(1,1),nfitca(ind1),nfitca(ind2),
     -.FALSE.,scr,contr(1,count),noc(count),noc(prev(ind1)),
     -prev(ind1),prev(ind2))
        do 41 ll=1,noc(count)
        prev(contr(ll,count))=count
41      continue
        if(noc(count).eq.nm) then
          goto 28
        else
          goto 27
        endif
      endif
c
c     ----------------------------------------------------------------------
c     remove those residues from the framework which have been incorrectly
c     determined as topologically equivalent due to the use of frameworks
c     in the determination of the distance matrix
c     ----------------------------------------------------------------------
c
28    tcomp=compmin*compmin
      do 36 j=1,(nm-1)
        do 33 k=(j+1),nm
          if(j.eq.k) goto 33
          do 37 i=1,nf
            x1=xf(1,na(i,j),j)-xf(1,na(i,k),k) 
            x2=xf(2,na(i,j),j)-xf(2,na(i,k),k) 
            x3=xf(3,na(i,j),j)-xf(3,na(i,k),k) 
            dist=((x1*x1)+(x2*x2)+(x3*x3))
            if(dist.gt.tcomp) then
              rem(i)=.TRUE.
            endif
37        continue
33      continue
36    continue
      nft=nf
      nrm=0
      do 34 i=1,nft
        if(rem(i)) then
          rem(i)=.FALSE.
          do 35 j=(i-nrm),(nf-1)
            do 38 l=1,nm
              na(j,l)=na((j+1),l)
38          continue
35        continue
        nf=nf-1
        nrm=nrm+1
        endif
34    continue
      do 39 i=1,nm
        nfitca(i)=nf
39    continue
      return 
      end
