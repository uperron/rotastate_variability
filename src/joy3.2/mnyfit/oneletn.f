      subroutine onelet()
c
c Converts a three letter amino acid code to 
c a 1 letter code
c
c Unknown acids are assigned one letter code of X
c
      include 'mnyfit.h'
c     Common blocks for mnyfit  
c
      character acids3(23)*3, nam*3
c
      character acids1*23
c
      data acids1 / 'ARNDCQEGHILKMFPSTWYVBZX' /
      data acids3 / 'ALA', 'ARG', 'ASN', 'ASP', 'CYS', 'GLN', 'GLU', 
     &'GLY', 'HIS', 'ILE', 'LEU', 'LYS', 'MET', 'PHE', 'PRO', 'SER', 
     &'THR', 'TRP', 'TYR', 'VAL', 'ASX', 'GLX', 'UNK' /
      do 20 k = 1, nm
        l = 1
        nam = namres(1,k)
        do 11 i = 1, 23
          if (nam .eq. acids3(i)) then
            lett(l,k) = acids1(i:i)
            goto 21
          end if
   11   continue
        lett(l,k) = 'X'
   21   continue
        do 20 j = 2, natoms(k)
        if (numres(j,k) .ne. numres(j - 1,k)) then
        l = l + 1
        nam = namres(j,k)
        end if
        do 10 i = 1, 23
          if (nam .eq. acids3(i)) then
            lett(l,k) = acids1(i:i)
            goto 20
          end if
   10   continue
        lett(l,k) = 'X'
   20 continue
C
      return 
      end
