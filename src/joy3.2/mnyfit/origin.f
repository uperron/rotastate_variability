      subroutine origin(x1, x2, N1, N2)
C
C Note dodgy use of 3,1 arrays I am sure this will fall over one day
C
C jpo 23-4-96 (added mnyfit.h dependency)
C
c      implicit none
      include 'mnyfit.h'
C
      real x1(3, 1), x2(3, 1)
      real c(3)
      integer I, J, N1, N2
C
      do j=1,3
        c(j)=0.
        do i=1, N1
          c(j)=c(j)+x1(j,i)
        end do
        c(j)=c(j)/real(N1)
      end do
C
      do i=1,N1
        do j=1,3
          x1(j,i)=x1(j,i)-c(j)
        end do
      end do
      do i=1,N2
        do j=1,3
          x2(j,i)=x2(j,i)-c(j)
        end do
      end do
C
      return
      end
C
C =======================================================================
C 
C A horrible hack to allow translation etc of another set of coords
C
      subroutine origin2(x1, x2, X3, n, nc, N3)
C
C jpo 23-4-96 adfded het.h and mnyfit.h
C
c      implicit none
      include 'mnyfit.h'
      include 'het.h'
C
      real x1(3, MAXATS, MAXMOL), x2(3, MAXATS, MAXMOL), X3(3, MAXHET, MAXMOL)
      real c
      integer I, J, N, NC, N3
C
      do j=1, 3
        c=0.
        do i=1, n
          c=c+x1(j,i,1)
        end do
        c=c/float(n)
        do i=1, n
          x1(j,i,1)=x1(j,i,1)-c
        end do
        do i = 1, nc
          x2(j,i,1)=x2(j,i,1)-c
        end do
        do i = 1, n3
          x3(j,i,1)=x3(j,i,1)-c
        end do
      end do
C
      return
      end
