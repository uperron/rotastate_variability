      subroutine probin(xref, dirdat)
c
c READs in the most probable conformations.
c
      parameter (maxlen = 14)
c
      real xref(3, 20, maxlen, 3)
c
      character dirdat*(*)
      integer lr(20)
C
      data lr / 5, 11, 2*8, 6, 2*9, 4, 10, 2*8, 9, 8, 11, 7, 6, 7, 14, 
     &         12, 7 /
c
      open(1, file=dirdat(1:lastchar(dirdat)) // 'probh.dat', status='OLD', 
     & form='FORMATTED',err=900) 
      do i = 1, 4
        read(1, '(30X,3F8.3)',err=902) (xref(j,8,i,1),j = 1, 3)
    1   format(30x,3f8.3)
      end do
      rewind(1) 
      do i = 1, 7
        do j = 1, lr(i)
          read(1, '(30X,3F8.3)',err=902) (xref(k,i,j,1),k = 1, 3)
        end do
      end do
      do i = 9, 20
        do j = 1, lr(i)
          read(1, '(30X,3F8.3)',err=902) (xref(k,i,j,1),k = 1, 3)
        end do
      end do
      close(1) 
c
c read in data for sheets.
c
      open  (1, file=dirdat // 'probs.dat', status='OLD',
     &       form='FORMATTED',err=900) 
      do i = 1, 4
        read(1, '(30X,3F8.3)',err=902) (xref(j,8,i,2),j = 1, 3)
      end do
      rewind(1) 
      do i = 1, 7
        do j = 1, lr(i)
          read(1, '(30X,3F8.3)',err=902) (xref(k,i,j,2),k = 1, 3)
        end do
      end do
      do i = 9, 20
        do j = 1, lr(i)
          read(1, '(30X,3F8.3)',err=902) (xref(k,i,j,2),k = 1, 3)
        end do
      end do
      close(1) 
c
c Now for the loops
c
      open (1, file=dirdat // 'probl.dat', status='OLD',
     &     form='FORMATTED',err=900) 
      do i = 1, 4
        read (1, '(30X,3F8.3)',err=902) (xref(j,8,i,3),j = 1, 3)
      end do
      rewind (1) 
      do i = 1, 7
        do j = 1, lr(i)
          read (1, '(30X,3F8.3)',err=902) (xref(k,i,j,3),k = 1, 3)
        end do
      end do
      do i = 9, 20
        do j = 1, lr(i)
          read (1, '(30X,3F8.3)',err=902) (xref(k,i,j,3),k = 1, 3)
        end do
      end do
      close (1) 
C
      return 
C
900   write (STDERR,'(''mnyfit: error opening data file'')')
      call EXIT(1)
902   write (STDERR,'(''mnyfit: error reading data file'')')
      call EXIT(1)
      end
