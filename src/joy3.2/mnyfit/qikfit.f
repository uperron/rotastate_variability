      subroutine qikfit(umat, rm)
c
c     USES CONJUGATE GRADIENT METHOD TO CALCULATE
c     TRANSFORMATION TO FIT SETS OF ATOMS.
c     FROM A SUBROUTINE WRITTEN BY A.D. MCLACHLAN
c     DETAILED IN ACTA CRYST.(1982),A38,871-873
c
c              M.J. SUTCLIFFE, NOV. 1985.
c
c---------------------------------------------------------
c
      real rsum
C
      dimension umat(3, 3), rot(3, 3), coup(3), dir(3), step(3)
      dimension turmat(3, 3), c(3, 3), v(3), rm(3, 3)
c
   20 format(1x,17h CONVERGED AFTER ,i5,7h CYCLES,6hRTSUM=,f12.5,
     &7h DELTA=,f14.5)
   21 format(1x,8h COUPLE ,3f14.5,5h CLE ,f14.5,6h CLEP ,f14.5,
     &8h GRATIO ,f14.5)
   22 format(1x,8f14.5)
   23 format(1x,6h STEP ,3f14.5)
   24 format(1x,5h DIR ,3f14.5)
   25 format(1x,8h STCOUP ,f14.5,4h UD ,f14.5,4h TR ,f14.5,4h TA ,f14.5)
   26 format(1x,4h CS ,f14.5,4h SN ,f14.5)
   27 format(1x,14h CYCLE NUMBER ,i5,24h------------------------)
   28 format(1x,8h RTSUMP ,f14.5,8h DELTAP ,f14.5)
   29 format(1x,6h UMAT )
   30 format(1x,13h TURN MATRIX )
c
c--   ROTATE REPEATEDLY TO REDUCE COUPLE ABOUT INITIAL DIRECTION
c--   TO ZERO. CLEAR ROTATION MATRIX.
c
   31 format(1x,22h ACCUMULATED ROTATION )
      data small / 1.0e-20 /
      data smalsn / 1.0e-10 /
      do 105 l = 1, 3
      do 106 m = 1, 3
      rot(l,m) = 0.0
  106 continue
      rot(l,l) = 1.0
c
c--   COPY VMAT (SINGLE PRECISION) INTO UMAT (DOUBLE)
c
  105 continue
      jmax = 30
      rtsum = (umat(1,1) + umat(2,2)) + umat(3,3)
      delta = 0.0
c
c--     MODIFIED CONJUGATE GRADIENT
c--     FOR FIRST AND EVERY NSTEEP CYCLES SET PREVIOUS STEP AS
c--     ZERO AND TAKE STEEPEST DESCENT PATH.
c
      do 250 ncyc = 1, jmax
      nsteep = 3
      ncc1 = ncyc - 1
      nrem = ncc1 - ((ncc1 / nsteep) * nsteep)
      if (nrem .ne. 0) goto 115
      do 110 i = 1, 3
      step(i) = 0.0
  110 continue
      clep = 1.0
c
c--     COUPLE
c
  115 continue
      coup(1) = umat(2,3) - umat(3,2)
      coup(2) = umat(3,1) - umat(1,3)
      coup(3) = umat(1,2) - umat(2,1)
c
c--     GRADIENT VECTOR IS NOW -COUP
c
      cle = sqrt(((coup(1) ** 2) + (coup(2) ** 2)) + (coup(3) ** 2))
c
c--     VALUE OF RTSUM FROM PREVIOUS STEP
c
      gfac = (cle / clep) ** 2
      rtsump = rtsum
      deltap = delta
      clep = cle
c
c--     STEP VECTOR CONJUGATE TO PREVIOUS
c
      if (cle .lt. small) goto 300
      stp = 0.0
      do 120 i = 1, 3
      step(i) = coup(i) + (step(i) * gfac)
      stp = stp + (step(i) ** 2)
  120 continue
c
c--     NORMALISED STEP
c
      stp = 1.0 / sqrt(stp)
      do 130 i = 1, 3
      dir(i) = stp * step(i)
c
c--     COUPLE RESOLVED ALONG STEP DIRECTION
c
  130 continue
c
c--     COMPONENT OF UMAT ALONG DIRECTION
c
      stcoup = ((coup(1) * dir(1)) + (coup(2) * dir(2))) + (coup(3) * 
     &dir(3))
      ud = 0.0
      do 140 l = 1, 3
      do 140 m = 1, 3
      ud = ud + ((umat(l,m) * dir(l)) * dir(m))
  140 continue
      tr = ((umat(1,1) + umat(2,2)) + umat(3,3)) - ud
      ta = sqrt((tr ** 2) + (stcoup ** 2))
      cs = tr / ta
c
c--     IF(CS.LT.0) THEN PRESENT POSITION IS UNSTABLE SO DO NOT STOP
c
      sn = stcoup / ta
      if ((cs .gt. 0.0) .and. (abs(sn) .lt. smalsn)) goto 300
c
c--     TURN MATRIX FOR CORRECTING ROTATION
c
c--     SYMMETRIC PART
c
  150 continue
      ac = 1.0 - cs
      do 160 l = 1, 3
      v(l) = ac * dir(l)
      do 155 m = 1, 3
      turmat(l,m) = v(l) * dir(m)
  155 continue
      turmat(l,l) = turmat(l,l) + cs
      v(l) = dir(l) * sn
c
c--     ASSYMETRIC PART
c
  160 continue
      turmat(1,2) = turmat(1,2) - v(3)
      turmat(2,3) = turmat(2,3) - v(1)
      turmat(3,1) = turmat(3,1) - v(2)
      turmat(2,1) = turmat(2,1) + v(3)
      turmat(3,2) = turmat(3,2) + v(1)
c
c--     UPDATE TOTAL ROTATION MATRIX
c
      turmat(1,3) = turmat(1,3) + v(2)
      do 170 l = 1, 3
      do 170 m = 1, 3
      c(l,m) = 0.0
      do 170 k = 1, 3
      c(l,m) = c(l,m) + (turmat(l,k) * rot(k,m))
  170 continue
      do 180 l = 1, 3
      do 180 m = 1, 3
      rot(l,m) = c(l,m)
c
c--     UPDATE UMAT TENSOR
c
  180 continue
      do 190 l = 1, 3
      do 190 m = 1, 3
      c(l,m) = 0.0
      do 190 k = 1, 3
      c(l,m) = c(l,m) + (turmat(l,k) * umat(k,m))
  190 continue
      do 200 l = 1, 3
      do 200 m = 1, 3
      umat(l,m) = c(l,m)
  200 continue
      rtsum = (umat(1,1) + umat(2,2)) + umat(3,3)
c
c--     IF NO IMPROVEMENT IN THIS CYCLE THEN STOP
c
      delta = rtsum - rtsump
c
c--     NEXT CYCLE
c
      if (abs(delta) .lt. small) goto 300
  250 continue
  300 continue
c
c--   COPY ROTATION MATRIX FOR OUTPUT
c
      rsum = rtsum
      do 310 i = 1, 3
      do 310 j = 1, 3
      rm(j,i) = rot(i,j)
  310 continue
      return 
      end
