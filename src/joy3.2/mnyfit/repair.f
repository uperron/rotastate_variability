      subroutine repair(ityp, lenr, numf, xx, xref, ns)
c
c Calculate coordinates for missing sidechain atoms
c
c ITYP - residue type (1-20)
c LENR - number of atoms in this residue
c NS   - type of secondary structure
c NUMF - number of atom beeing not found first
c        ('first' with respect to standard order)
c LMX  - maximal number for NUMF for each residue
c XX   - residue-coordinates as obtained from pdb-file
c BDIS - square of maximal bond-length in side chain
c XREF - library of most probable residue-conformations
c
c
      parameter (maxlen = 14, bdis = 6.25)
      real xref(3, 20, maxlen, 3)
      real xx(3, maxlen), x(3), y(3), z(3), rn(3), rm(3), am(3, 3), an(3
     &, 3), tr(3, 3)
c
      integer lmx(20), in(3)
c
c
      data lmx / 5, 10, 2*7, 6, 2*8, 4, 7, 6, 7, 9, 8, 7, 5, 2*6, 2*7, 6
     & /
      if ((numf .le. 4) .or. (ityp .eq. 8)) return 
      num = numf
c
c Test bond lengths for atoms present in the sidechain
c
      if (numf .gt. lmx(ityp)) num = lmx(ityp)
      do i = 5, num - 1
      ii = i - 1
      if (ii .eq. 4) ii = 2
      dis = 0.
      do j = 1, 3
      dis = dis + ((xx(j,i) - xx(j,ii)) ** 2)
      end do
      if (dis .gt. bdis) then
      write(unit=*, fmt='(3X,A,2I3,A,I3,A)') 
     &' Goofy bond between atoms ', i, ii, ' ,will replace from atom ', 
     &ii, ' on'
      goto 1
      end if
      end do
      goto 2
c
c Indices for atoms to set up coordinate systems
c (do not take carbonyl oxygen of mainchain)
c
    1 num = ii
    2 i0 = num
      do i = 1, 3
      i0 = i0 - 1
      if (i0 .eq. 4) i0 = i0 - 1
      in(i) = i0
c
c Set up coord. system for standard side-chain (='old' system)
c
      end do
      do i = 1, 3
      rm(i) = xref(i,ityp,in(1),ns)
      x(i) = rm(i) - xref(i,ityp,in(2),ns)
      y(i) = xref(i,ityp,in(3),ns) - xref(i,ityp,in(2),ns)
      end do
      dx = sqrt(((x(1) * x(1)) + (x(2) * x(2))) + (x(3) * x(3)))
      z(1) = (x(2) * y(3)) - (x(3) * y(2))
      z(2) = (x(3) * y(1)) - (x(1) * y(3))
      z(3) = (x(1) * y(2)) - (x(2) * y(1))
      dz = sqrt(((z(1) * z(1)) + (z(2) * z(2))) + (z(3) * z(3)))
      y(1) = (z(2) * x(3)) - (z(3) * x(2))
      y(2) = (z(3) * x(1)) - (z(1) * x(3))
      y(3) = (z(1) * x(2)) - (z(2) * x(1))
      dy = sqrt(((y(1) * y(1)) + (y(2) * y(2))) + (y(3) * y(3)))
      do i = 1, 3
      am(i,1) = x(i) / dx
      am(i,2) = y(i) / dy
      am(i,3) = z(i) / dz
c
c Set up system for sidechain which has missing atoms (=new system)
c
      end do
      do i = 1, 3
      rn(i) = xx(i,in(1))
      x(i) = rn(i) - xx(i,in(2))
      y(i) = xx(i,in(3)) - xx(i,in(2))
      end do
      dx = sqrt(((x(1) * x(1)) + (x(2) * x(2))) + (x(3) * x(3)))
      z(1) = (x(2) * y(3)) - (x(3) * y(2))
      z(2) = (x(3) * y(1)) - (x(1) * y(3))
      z(3) = (x(1) * y(2)) - (x(2) * y(1))
      dz = sqrt(((z(1) * z(1)) + (z(2) * z(2))) + (z(3) * z(3)))
      y(1) = (z(2) * x(3)) - (z(3) * x(2))
      y(2) = (z(3) * x(1)) - (z(1) * x(3))
      y(3) = (z(1) * x(2)) - (z(2) * x(1))
      dy = sqrt(((y(1) * y(1)) + (y(2) * y(2))) + (y(3) * y(3)))
      do i = 1, 3
      an(i,1) = x(i) / dx
      an(i,2) = y(i) / dy
      an(i,3) = z(i) / dz
c
c Calculate transformation matrix TR()
c
      end do
      do i = 1, 3
      do j = 1, 3
      tr(i,j) = 0.d0
      do k = 1, 3
      tr(i,j) = tr(i,j) + (an(i,k) * am(j,k))
      end do
      end do
c
c Transform coordinates for missing atoms into NEW system
c
      end do
      do i = num, lenr
      do j = 1, 3
      x(j) = xref(j,ityp,i,ns) - rm(j)
      end do
      do j = 1, 3
      xx(j,i) = rn(j)
      do k = 1, 3
      xx(j,i) = xx(j,i) + (tr(j,k) * x(k))
      end do
      end do
c
      end do
      return 
      end
