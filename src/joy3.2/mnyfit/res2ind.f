      integer function RES2IND(STRING,J)
C
      character*5 STRING
      include 'mnyfit.h'
C
C translates a residue name to an index in a sequence in molecule J
C
      do I=1,NRESI(J)
        if (STRING .eq. RESID(I,J)) then
          RES2IND=I
          return
        end if
      end do
C
      write (STDERR,'(''mnyfit: impossible conversion of resid '',A)') STRING
C
      return
      end
