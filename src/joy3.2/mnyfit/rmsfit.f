      subroutine rmsfit(x1, x2, rms, n, iw, wtjpo)
C
C 25-4-96: jpo added includes and redimensioned relevant arrays
C
      include 'mnyfit.h'
C
      real X1(3, MAXATS, MAXMOL), X2(3, MAXATS, MAXMOL), WTJPO(MAXATS)
      real WW
C
      if (IW .le. 0) then
        RMS = 0.0
        do I=1, N
          RMS=((RMS+((x1(1,i,1) - x2(1,i,1)) ** 2)) +
     -              ((x1(2,i,1) - x2(2,i,1)) ** 2)) +
     -              ((x1(3,i,1) - x2(3,i,1)) ** 2)
        end do
        RMS=sqrt(RMS / float(N))
      else
        WW=0.
        RMS=0.
        do I=1,N
          WW=WW+WTJPO(I)
          RMS=RMS+(WTJPO(I) * ((((x1(1,i,1) - x2(1,i,1)) ** 2)
     -                        + ((x1(2,i,1) - x2(2,i,1)) ** 2))
     -                        + ((x1(3,i,1) - x2(3,i,1)) ** 2)))
        end do
        RMS=sqrt(RMS / WW)
      end if
C
      return
C
      end

