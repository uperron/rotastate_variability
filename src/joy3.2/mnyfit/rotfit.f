      subroutine rotfit(x2, r, n)
C
      real x2(3, n), r(3, 3)
C
      do 1 i = 1, n
        x21=((r(1,1)*x2(1,i))+(r(1,2)*x2(2,i)))+(r(1,3)*x2(3,i))
        x22=((r(2,1)*x2(1,i))+(r(2,2)*x2(2,i)))+(r(2,3)*x2(3,i))
        x23=((r(3,1)*x2(1,i))+(r(3,2)*x2(2,i)))+(r(3,3)*x2(3,i))
        x2(1,i)=x21
        x2(2,i)=x22
        x2(3,i)=x23
    1 continue
C
      end
