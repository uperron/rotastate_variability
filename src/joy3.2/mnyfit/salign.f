      subroutine salign(smat,dmat,lena,lenb,first,lrgst,contr,numb,
     -nmb,ind1,ind2)
c
c     salign.f 
c      
c     author --  PJT 27/02/90
c                Needlemann and Wunsch J. Mol. Biol. (1970) vol 48
c                443-453
c
c     purpose -- uses algorithm of Needleman and Wunsch to trace
c                topological equivalences between homologous proteins
c                superimposed by the algorithms of MNYFIT (MJS).
c
c     modifications
c
c     28-03-90 - PJT modified basic N-W algorithm using algorithm of
c                Murata et al. (PNAS (1985) vol 82 3073-77).
c
c     ------------------------------------------------------------------
c
c     declarations
c
      include 'mnyfit.h'
c
      integer smat(MAXRES,MAXRES)
      integer dmat(MAXRES,MAXRES)
      integer lmat(MAXRES,MAXRES)
      integer umat(MAXRES,MAXRES)
      integer lrgst
      integer test
      integer aseq1(MAXRES)	! was 400
      integer aseq2(MAXRES)	! was 400
      integer contr(MAXMOL)
      integer numb
      integer nmb
      integer tna(MAXRES,MAXMOL)	! was 400
      integer lena
      integer lenb
      integer pen
      logical first
      real norm
c
c     ------------------------------------------------------------------
c     smat and dmat are two copies of the inter CA distance matrix
c                   determined by dismat smat is used to construct the
c                   matrices umat and lmat but is destroyed in the 
c                   process, dmat is then required to determine the
c                   topological equivalence (dmat(i,j)>= (1000*(10-compmin)) 
c                   for the aligned residue positions.
c     lmat and umat are constructed from smat and are used to trace
c                   a path backwards through the matrix smat.
c     lrgst         holds the value of the optimum score found
c                   following the trace back.
c     aseq1 and 2   record the numbers of the aligned positions when
c                   the matrix is traced forward.
c     contr         records the numbers of the structures which 
c                   contibuted to the construction of the distance
c                   matrix
c     numb          records the number of structures contributing to
c                   the framework for index i
c     nmb           records the total number of structures used in
c                   constructing the current distance matrix
c     tna           is a temporary holding array from which na(,) 
c                   will be updated
c     lena and lenb are the lengthes of the structures being compared
c     pen           is the gap penalty 
c     norm          is used to normalise the optimum score to be
c                   recorded in the scoring matrix in neardev. It is
c                   used so that small structures are not down weighted
c                   simply because of their length.
c     ------------------------------------------------------------------
c
      pen=3000
c
      do 101 i=1,(lena+1)
        do 102 j=1,(lenb+1)
          umat(i,j)=0
          lmat(i,j)=0
102     continue
101   continue
      norm=min(lena,lenb)
c
c     trace optimal path backwards through distance matrix
c
      umat((lena+1),(lenb+1))=smat((lena+1),(lenb+1))
      lmat((lena+1),(lenb+1))=smat((lena+1),(lenb+1))
      do 1 i=lena,1,-1
        do 2 j=lenb,1,-1
          lmat(i,j)=smat(i,j)+(max(lmat((i+1),(j+1)),
     -(umat((i+1),(j+1))-pen)))
          umat(i,j)=max(lmat(i,j),umat((i+1),j),umat(i,(j+1)))
2       continue
1     continue
c
c       find start point for forward trace of matrix
c
      lrgst=0
      do 5 i=1,lena
        j=1
        if(lmat(i,j).gt.lrgst) then
          aseq1(1)=i
          aseq2(1)=j
          lrgst=lmat(i,j)
        endif
5     continue
      do 6 j=1,lenb
        i=1
        if(lmat(i,j).gt.lrgst) then
          aseq1(1)=i
          aseq2(1)=j
          lrgst=lmat(i,j)
        endif
6     continue
      lrgst=lrgst/norm
      if(first) then
        return
      endif
c
c     trace forward through matrix to define equivalences along optimal
c     path
c
      i=aseq1(1)
      j=aseq2(1)
      m=2
7     test=lmat((i+1),(j+1))
      aseq1(m)=i+1
      aseq2(m)=j+1
      do 8 k=(i+1),lena
        l=j+1
        if((lmat(k,l)-pen).gt.test) then
          aseq1(m)=k
          aseq2(m)=l
          test=lmat(k,l)
        endif
8     continue
      do 9 k=(j+1),lenb
        l=i+1
        if((lmat(l,k)-pen).gt.test) then
          aseq1(m)=l
          aseq2(m)=k
          test=lmat(l,k)
        endif
9     continue
      i=aseq1(m)
      j=aseq2(m)
      m=m+1
      if(i.eq.lena.or.j.eq.lenb) goto 10
      goto 7
c
c     determine topologically equivalent residues
c
10    nalign=m-1
      nftca=0
      comt=1000*(10-compmin)
      do 30 i=1,nalign
        if(dmat(aseq1(i),aseq2(i)).ge.comt) then
          nftca=nftca+1
          if(numb.eq.2) then
            tna(nftca,contr(1))=na(aseq1(i),contr(1))
            tna(nftca,contr(2))=na(aseq2(i),contr(2))
            goto 30
          endif
          if(ind1.ne.0.and.ind2.eq.0) then
            do 60 l=1,(numb-1)
              tna(nftca,contr(l))=na(aseq1(i),contr(l))
60          continue
            tna(nftca,contr(numb))=na(aseq2(i),contr(numb))
            goto 30
          endif
          if(ind1.eq.0.and.ind2.ne.0) then
            tna(nftca,contr(1))=na(aseq1(i),contr(1))
            do 61 l=2,numb
              tna(nftca,contr(l))=na(aseq2(i),contr(l))
61          continue
            goto 30
          endif
          if(ind1.ne.0.and.ind2.ne.0) then
            do 62 l=1,nmb
              tna(nftca,contr(l))=na(aseq1(i),contr(l))
62          continue
            do 63 l=(nmb+1),numb
              tna(nftca,contr(l))=na(aseq2(i),contr(l))
63          continue
            goto 30
          endif
        endif
30    continue
      nf=nftca
      do 32 j=1,numb
        nfitca(contr(j))=nftca
        do 33 i=1,nf
          na(i,contr(j))=tna(i,contr(j))
33      continue
32    continue
      return
      end
