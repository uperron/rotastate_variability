C
C a replacement for the previos read statements in sortn
C
      data (ORDER( 1,J),J=1,10) /'CB ','   ','   ','   ','   ',
     +                           '   ','   ','   ','   ','   '/
      data (ORDER( 2,J),J=1,10) /'CB ','CG ','CD ','NE ','CZ ',
     +                           'NH1','NH2','   ','   ','   '/
      data (ORDER( 3,J),J=1,10) /'CB ','CG ','OD1','ND2','   ',
     +                           '   ','   ','   ','   ','   '/
      data (ORDER( 4,J),J=1,10) /'CB ','CG ','OD1','OD2','   ',
     +                           '   ','   ','   ','   ','   '/
      data (ORDER( 5,J),J=1,10) /'CB ','SG ','   ','   ','   ',
     +                           '   ','   ','   ','   ','   '/
      data (ORDER( 6,J),J=1,10) /'CB ','CG ','CD ','OE1','NE2',
     +                           '   ','   ','   ','   ','   '/
      data (ORDER( 7,J),J=1,10) /'CB ','CG ','CD ','OE1','OE2',
     +                           '   ','   ','   ','   ','   '/
      data (ORDER( 8,J),J=1,10) /'   ','   ','   ','   ','   ',
     +                           '   ','   ','   ','   ','   '/
      data (ORDER( 9,J),J=1,10) /'CB ','CG ','ND1','CD2','CE1',
     +                           'NE2','   ','   ','   ','   '/
      data (ORDER(10,J),J=1,10) /'CB ','CG1','CG2','CD1','   ',
     +                           '   ','   ','   ','   ','   '/
      data (ORDER(11,J),J=1,10) /'CB ','CG ','CD1','CD2','   ',
     +                           '   ','   ','   ','   ','   '/
      data (ORDER(12,J),J=1,10) /'CB ','CG ','CD ','CE ','NZ ',
     +                           '   ','   ','   ','   ','   '/
      data (ORDER(13,J),J=1,10) /'CB ','CG ','SD ','CE ','   ',
     +                           '   ','   ','   ','   ','   '/
      data (ORDER(14,J),J=1,10) /'CB ','CG ','CD1','CD2','CE1',
     +                           'CE2','CZ ','   ','   ','   '/
      data (ORDER(15,J),J=1,10) /'CB ','CG ','CD ','   ','   ',
     +                           '   ','   ','   ','   ','   '/
      data (ORDER(16,J),J=1,10) /'CB ','OG ','   ','   ','   ',
     +                           '   ','   ','   ','   ','   '/
      data (ORDER(17,J),J=1,10) /'CB ','OG1','CG2','   ','   ',
     +                           '   ','   ','   ','   ','   '/
      data (ORDER(18,J),J=1,10) /'CB ','CG ','CD1','CD2','NE1',
     +                           'CE2','CE3','CZ2','CZ3','CH2'/
      data (ORDER(19,J),J=1,10) /'CB ','CG ','CD1','CD2','CE1',
     +                           'CE2','CZ ','OH ','   ','   '/
      data (ORDER(20,J),J=1,10) /'CB ','CG1','CG2','   ','   ',
     +                           '   ','   ','   ','   ','   '/
C
