      subroutine sort(ION, DOREPAIR)
c
c PURPOSE
c
c Sorts out ATOMs into order determined by the data statement.
c
c MAXLEN is the maximum number of atoms in a residue , 
c NOTE this probably will produce errors when the C-terminal residue is 
c a TRP
c (Someone else can fix this)
C
C I do not sort chain ids, which, one assumes will be constant within a residue
c
      parameter (maxlen = 14)
c
      include 'mnyfit.h'
c     Common blocks for mnyfit  
c
      character main(4)*4
      character*3 ORDER(20,10)
      character restyp(20)*3, tnamres(maxats, maxmol)*3
      character tnumres(maxats, maxmol)*5
      character tatnam(maxats, maxmol)*4
      character*1 THR2ONE
      integer rlen(maxres, maxmol), len(20)
      logical found, DOREPAIR
      real xtemp(3, maxlen)
      real xref(3, 20, maxlen, 3)
c
      real tcoords(3, maxats, maxmol)
      real occuptemp(maxats), toccup(maxats, maxmol)
      real tfacttemp(maxats), ttfact(maxats, maxmol)
C
C get side chain atom names
C
      include 'side.h'
c
c Return if the file is just a CA file.
c
      data main / ' N  ', ' CA ', ' C  ', ' O  ' /
      data restyp / 'ALA', 'ARG', 'ASN', 'ASP', 'CYS', 'GLN', 'GLU', 
     +              'GLY', 'HIS', 'ILE', 'LEU', 'LYS', 'MET', 'PHE',
     +              'PRO', 'SER', 'THR', 'TRP', 'TYR', 'VAL' /
      data len /        5,    11,     8,     8,     6,     9,     9,
     +                  4,    10,     8,     8,     9,     8,    11,
     +                  7,     6,     7,    14,    12,     7 /
C
      do i = 1, 5
        if (atnam(i,ION) .ne. ' CA ') goto 10
      end do
      write (IO,'(''# file contains only CA coordinates '',A)')
     +      fname(ION)
      return 
c
c Set up ATOMs within side chains in same order in all cases.
c
   10 i = ION
      nat = 1
      nres = 1
c
c CALCULATE NUMBER OF RESIDUES AND NUMBER OF ATOMS IN EACH
c
      do j = 2, natoms(i) - 1
        if (numres(j,i) .ne. numres(j - 1,i)) then
          rlen(nres,i) = nat
          RESID(NRES,I)=numres(j-1,i)
          nat = 1
          nres = nres + 1
        else
          nat = nat + 1
        end if
      end do
      RESID(NRES,I)=numres(j,i)
c
c NUMBER OF ATOMS IN THE LAST RESIDUE
c DO NOT TAKE 'OXT' IN THE LAST RESIDUE
c
      if (atnam(natoms(i),i) .eq. ' OXT') then
c        write (6,*) ' found oxt ', atnam(natoms(i),i), natoms(i), i
        rlen(nres,i) = nat
      else
        rlen(nres,i) = nat + 1
      end if
C
      nresi(i) = nres
C
c      open (unit=1,file=dirdat(1:LASTCHAR(DIRDAT))//'side.dat', status='OLD',
c     &      form='FORMATTED', err=900) 
c      do i = 1, 20
c        read (1,'(11(a3,1x))', end=20) (order(i,j),j = 1, 10)
c      end do
c   20 close (1) 
c
c READ MOST PROBABLE CONFORMATIONS FOR SIDECHAINS
c
c FOR ALL RESIDUES OF ENTRY M=ION:
c
c CHECK RESIDUE-NAME
c
cjpo     call probin(xref, dirdat(1:LASTCHAR(DIRDAT)))
      m = ION
      nat = 0
      nato = 1
      do i = 1, nresi(m)
      natr = 0
      do j = 1, 20
      if (restyp(j) .eq. namres(nato,m)) goto 30
c
      end do
      write(STDOUT, fmt=2) numres(nato,m), namres(nato,m), fname(ION)
    2 format('mnyfit: residue-name ',2A,' not recognised in ',A)
c
      goto 100
c
c CHECK MAINCHAIN
c
   30 nr = j
      do j = 1, 4
        found = .false.
        do k = 1, rlen(i,m)
          if (main(j) .eq. atnam((nato - 1) + k,m)) then
            xtemp(1,j) = coords(1,(nato - 1) + k,m)
            xtemp(2,j) = coords(2,(nato - 1) + k,m)
            xtemp(3,j) = coords(3,(nato - 1) + k,m)
            tfacttemp(j) = tfact((nato - 1) + k,m)
            occuptemp(j) = occup((nato - 1) + k,m)
            found = .true.
            goto 40
          end if
        end do
   40   if (.not. found) then
          write (STDOUT,3) FNAME(ION)(1:index(FNAME(ION),'.')-1), MAIN(j),
     +            THR2ONE(namres(nato,m)), numres(nato,m)
    3     format('#    ',A,': ',A,' not found in ',A,1X,A)
          goto 100
        else
          continue
        end if
      end do
c
c CHECK SIDECHAIN
c (NOTFND is the number of the first sidechain atom not found)
c
      natr = 4
      notfnd = 0
      do j = 5, len(nr)
        found = .false.
        do k = 1, rlen(i,m)
          if (order(nr,j - 4)(1:3) .eq. atnam((nato - 1) + k,m)(2:4)) then
            natr = natr + 1
            xtemp(1,j) = coords(1,(nato - 1) + k,m)
            xtemp(2,j) = coords(2,(nato - 1) + k,m)
            xtemp(3,j) = coords(3,(nato - 1) + k,m)
            tfacttemp(j) = tfact((nato - 1) + k,m)
            occuptemp(j) = occup((nato - 1) + k,m)
            found = .true.
            goto 50
          end if
        end do
   50   if (.not. found) then
          if (notfnd .eq. 0) notfnd = j
            write (STDOUT,3) FNAME(ION)(1:index(FNAME(ION),'.')-1),
     +             order(NR,j-4), THR2ONE(namres(nato,m)), numres(nato,m)
        end if
      end do
c
c "repair"  missing sidechain atoms
c
      if (DOREPAIR) then
        if (notfnd .gt. 0) then
          write (STDOUT,'(''#       this sidechain will be repaired from '',A4)')
     +           order(nr,notfnd - 4)
          call repair(nr, len(nr), notfnd, xtemp, xref, 1)
          natr = len(nr)
        end if
      end if
c
c ACCEPT COORDINATES AND ATOM-NAMES FOR MAINCHAIN
c
      do j = 1, 4
        nat = nat + 1
        tatnam(nat,m) = main(j)
        tnamres(nat,m) = namres(nato,m)
        tnumres(nat,m) = numres(nato,m)
        do k = 1, 3
          tcoords(k,nat,m) = xtemp(k,j)
        end do
        ttfact(nat,m)=tfacttemp(j)
        toccup(nat,m)=occuptemp(j)
      end do
c
c ACCEPT COORDINATES AND ATOM-NAMES FOR STANDARD SIDECHAIN-ATOMS
c
      do j = 5, natr
        nat = nat + 1
        tatnam(nat,m) = ' ' // order(nr,j - 4)
        tnamres(nat,m) = namres(nato,m)
        tnumres(nat,m) = numres(nato,m)
        do k = 1, 3
          tcoords(k,nat,m) = xtemp(k,j)
        end do
        ttfact(nat,m)=tfacttemp(j)
        toccup(nat,m)=occuptemp(j)
      end do
C
  100 nato = nato + rlen(i,m)
c
      rlen(i,m) = natr
c
c GET RID OF GOOFY RESIDUE-LENGTHS
c RLEN=0: if residue-type not recognised or not enough main-chain
c atoms
c
      end do
      ii = 0
      do i = 1, nresi(ION)
        if (rlen(i,ION) .gt. 0) then
          ii = ii + 1
          rlen(ii,ION) = rlen(i,ION)
        end if
      end do
      nresi(ION) = ii
c
      natoms(ION) = nat
C
      do l = 1, natoms(ION)
        atnam(l,ION) = tatnam(l,ION)
        numres(l,ION) = tnumres(l,ION)
        namres(l,ION) = tnamres(l,ION)
        do k = 1, 3
          coords(k,l,ION) = tcoords(k,l,ION)
        end do
        occup(l,ion) = toccup(l,ion)
        tfact(l,ion) = ttfact(l,ion)
      end do
C
      return 
      end
