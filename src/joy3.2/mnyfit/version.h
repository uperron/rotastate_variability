      data verson /' 6.4a'/
C
C Version history
C 6.0 --- Bug fix and change of IO control
C 6.1 --- Addition of hetatom manipulation
C 6.2 --- Sorted array sizes for varous routines (use of X1(3,1))
C 6.3 --- Added extension and reference fitting options
C 6.4 --- removed reference fitting options, added carry through of temperature
C         factors and occupancies, also chain ids.
C 6.5 --- Added output of maximal difference at equivalenced sites
C
