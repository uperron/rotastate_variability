      program MS
C
C calculate the expected mass of a peptide sequence
C
      implicit NONE
C
C ----------------------------------------------------------------------------
C
      integer MAXSEQ, MAXRES, MAXTITLE, MAXNEWSEQ, MAXOPT, MAXLAB,
     -        MAXTEXT, NUMEXAM, NUMFEAT, MAXFILELEN, MAXSHEET, MAXHELIX
      integer MAXHET, MAXATS
      integer STDOUT, STDIN, STDERR
      real SMALL, TMASS
      parameter (MAXSEQ=60, MAXRES=3500, MAXTITLE=64, MAXOPT=10,
     -           MAXNEWSEQ=10, MAXLAB=3+MAXSEQ, MAXTEXT=6, NUMFEAT=13,
     -           NUMEXAM=12)
      parameter (STDOUT=6, STDIN=5, STDERR=0)
      parameter (MAXFILELEN=256, SMALL=1.E-15)
      parameter (MAXATS=30, MAXHET=500)
C
C ----------------------------------------------------------------------------
C
C Declarations for date.h
C
      character*40 DATE
      character*4 VERSION
C
C residues
C
      character*1 RESIDUE(20)
      real        MASS(20)
C
C declarations for VERBOSE variable, reports more information than usual
C
      logical VERBOSE
      common /VERBOSE/VERBOSE
C
C ----------------------------------------------------------------------------
C
      character*50 LBIN
C
      data RESIDUE /'A','C','D','E','F','G','H','I','K','L',
     +              'M','N','P','Q','R','S','T','V','W','Y'/
C
C MASS data, from Methods Enzymol. 193, p888, 1990
C
C data from 
C
      data MASS /  71.03712, 103.00919, 115.02695, 129.04260, 147.0682,
     +             57.02147, 137.05891, 113.08407, 128.09497, 113.08407,
     +            131.04049, 114.04293,  97.05277, 128.05858, 156.10112,
     +             87.03203, 101.04768,  99.06842, 186.07932, 163.06333/
C
C ----------------------------------------------------------------------------
C
C definitions for library functions
C
      include '../joylib/joylib.h'
C
      character*(MAXFILELEN) FILE
      integer ISEQ(MAXRES)
      integer NRES, IIN, IIO, IARGC, I, J
C
      integer NTOT(20)
C
      data IIO /STDOUT/
C
C
      NRES=0
C
      if (IARGC() .eq. 1) then
        IIN=1
        call GETARG(1,FILE)
        open (file=FILE,unit=IIN,form='formatted',err=3)
      else
        IIN=5
      end if
C
      call RDSEQ2(IIN,ISEQ,NRES)
C
      do I=1,20
        NTOT(I)=0
      end do
      do I=1,20
        do J=1,NRES
          if (ISEQ(J) .eq. I) then
            NTOT(I)=NTOT(I)+1
          end if
        end do
      end do
      TMASS=0.000000
      do I=1,20
        TMASS=TMASS+(NTOT(I)*MASS(I))
      end do
C
C add H2O (to account for N and C-termini
C
      TMASS=TMASS+17.9992+1.0078+1.0078
      write (STDOUT,'(I6,3X,F12.6)') NRES,TMASS
C
      call EXIT(0)
C
3     call ERROR('ms: cannot open file',FILE,1)
C
      end
C
C ######################################################
C
      subroutine GRAPSTART()
      integer IIO
      write (1,'(''.G1'')')
      write (1,'(''label left "composition"'')')
      write (1,'(''label bot "residue"'')')
      write (1,'(''frame solid right invis top invis wid 5.0 ht 3.0'')')
      write (1,'(''coord x 1, 21 y 0, 0.15'')')
      write (1,'(''ticks bot off'')')
      write (1,'(''copy "compo.dat" thru X'')')
      write (1,'(''  line from $1,0 to $1,$3'')')
      write (1,'(''  line from $1,$3 to $1+1,$3'')')
      write (1,'(''  line from $1+1,$3 to $1+1,0'')')
      write (1,'(''  "$2" at $1+0.5,-0.01'')')
      write (1,'(''  line dotted from $1,$4 to $1+1,$4'')')
      return
      end
C
      subroutine GRAPEND()
      integer IIO
      write (1,'(''X'')')
      write (1,'(''.G2'')')
      return
      end

C
C ######################################################
C
      subroutine RDSEQ(IO,ISEQ,LENSEQ)
C
C reads in one sequence from unit IO
C
      include 'ms.h' 
C
      integer MAXLENLINE
      parameter (MAXLENLINE=80)
C
      character*(MAXLENLINE) LINE
      character*1 SEQ
      integer ISEQ(MAXRES), LENSEQ, IO, LENLINE, N, I
      logical FEND
      FEND=.false.
C
      N=0
1     read (IO,'(A)',end=2,err=9) LINE
      LENLINE=lastchar(LINE)
      if (index(LINE,'*') .ge. 1) then
        LENLINE=LENLINE-1
        FEND=.true.
      end if
      write (6,*) lenline
      do I=1,LENLINE
        N=N+1
        if (N .gt. MAXRES) then
          call ERROR('too many residues in sequence',' ',1)
        end if
        read (LINE(I:I),'(A)') SEQ
        if (SEQ .eq. '-' .or. SEQ .eq. '/') then
          N=N-1
        else
          ISEQ(N)=SEQ2I(SEQ)
        end if
      end do
      LENSEQ=N
C
      if (FEND) then
        return
      else
        goto 1
      end if
C
      return
C
2     close (UNIT=IO)
      LENSEQ=N
      return
C
9     call ERROR('error reading line:',LINE(1:20),1)
      return
      end
C
C ############################################################################
C
      subroutine RDSEQ2(IO,ISEQ,NRES)
C
      include 'ms.h'
C
      character*14 CARD, TITLE
      integer IO, NRES, ISEQ(MAXRES)
C
2     read (IO,'(A)',end=1,err=901) CARD
      if (CARD(1:4) .eq. '>P1;') then
        read (CARD(5:),'(A)') TITLE
        read (IO,'(A)',end=3,err=901) CARD	! do nothing
        call RDSEQ(IO,ISEQ,NRES)
      end if
      goto 2
1     return
C
3     call ERROR('hydplot: error reading sequence',' ',1)
901   call ERROR('hydplot: error reading ',' ',1)
      end
