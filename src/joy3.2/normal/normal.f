      program NORMAL
C
      include '../joy/joy.h'
C
      character*(MAXFILELEN) FILE, BUFFER
      real CA(3,MAXRES), CUTOFF1, D1
      integer NRES, I, J, IARGC, IIN, IIO, K, NN
      real NEIGHBOUR(3,MAXRES), AVE(3), DIFF(3), PROJECT(3,MAXRES), CUTOFF
C
      data IIO /STDOUT/			! output to stdout
      data CUTOFF1 /64.0/		! 8.0**2
C
      if (IARGC() .eq. 2) then
        IIN=1
        call GETARG(1,BUFFER)
        if (BUFFER(1:2) .eq. '-d') then
          read (BUFFER(3:),'(F5.2)',err=900) CUTOFF
          CUTOFF1=CUTOFF**2
        else
          write (STDERR,'(''normal: error parsing cutoff value '',A)') BUFFER(1:10)
          call EXIT(2)
        end if
        call GETARG(2,FILE)
        open (file=FILE,unit=1,status='old',form='formatted',err=3)
      else if (IARGC() .eq. 1) then
        call GETARG(1,BUFFER)
        if (BUFFER(1:2) .eq. '-d') then
          read (BUFFER(3:),'(F5.2)',err=900) CUTOFF
          CUTOFF1=CUTOFF**2
        else
          write (STDERR,'(''normal: error parsing cutoff value '',A)') BUFFER(1:10)
          call EXIT(2)
        end if
        IIN=5
      else
        write (STDERR,'(''normal: usage normal -dcutoff filename'')')
        call EXIT(3)
      end if
      call RDCA(IIN,CA,NRES)
      close (unit=IIN)
C
C for each CA, build list of neighbours (within cutoff distance)
C find center of mass of neighbour list
C project vector to 'normal mode'
C
      do I=1,NRES
        NN=0
        do J=1,NRES
          if (DIST2(CA(1,I),CA(1,J)) .lt. CUTOFF1) then
            NN=NN+1
            do K=1,3
              NEIGHBOUR(K,NN)=CA(K,J)
            end do
          end if
        end do
        do K=1,3
          AVE(K)=0.0
        end do
        do J=1,NN
          do K=1,3
            AVE(K)=AVE(K)+NEIGHBOUR(K,J)
          end do
        end do
        do K=1,3
           AVE(K)=AVE(K)/(real(NN))
        end do 
        do K=1,3
          DIFF(K)=CA(K,I)-AVE(K)
          PROJECT(K,I)=CA(K,I)+DIFF(K)
        end do
      end do
C
      call WRCA(STDOUT,PROJECT,NRES)
C
      call EXIT(0)
C
3     call ERROR('caplot: cannot open file',FILE,1)
900   write (STDERR,'(''normal: error parsing cutoff value'')') BUFFER(1:10)
C
      end
C
C #########################################################################
C
      subroutine RDCA(IIN,CA,NRES)
C
      include '../joy/joy.h'
C
      character*66  CARD
      integer M, K, IIN, NRES
      real CA(3,MAXRES)
C
      M=0
4     read (IIN,'(A)',end=3,err=902) CARD
        if (CARD(1:6) .eq. 'ATOM  ' .and. CARD(14:15) .eq. 'CA') then
          M=M+1
          if (M .gt. MAXRES) then
            call ERROR('rdca: too many residues',' ',1)
          end if
          read (CARD,'(13X,3X,1X,3X,1X,1X,5X,3X,3F8.3)',err=903)
     -          (CA(K,M),K=1,3)
        end if
      goto 4
3     continue
      NRES=M
C
      return
C
902   call ERROR('caplot: bad format in file',' ',1)
903   call ERROR('caplot: bad read of internal file',' ',1)
C
      end

      subroutine WRCA(IIO,CA,NRES)
C
      include '../joy/joy.h'
C
      character*66  CARD
      integer M, K, IIO, NRES, I
      real CA(3,MAXRES)
C
      do I=1,NRES
          write (IIO,'(''ATOM  '',I5,''  CA  GLY'',I6,4X,3F8.3)')  I,I, (CA(K,I),K=1,3)
      end do
      write (IIO,'(''TER'')')
      write (IIO,'(''END'')')

      return
C
      end

