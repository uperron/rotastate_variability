      program OOIPLOT
C
C produce a Ramachandran type map from .sst file in grap language
C code taken from joy and hbplot
C
C input (of sst file) either STDIN, or file name on command line
C
      include '../joy/joy.h'
C
      character*(MAXFILELEN) FILE
      character*10 MODE
      character*5 LABEL(MAXRES)
      character*2 OPTION
      character*1 SEQ(MAXRES)
      integer NRES, IIN, IIO, IARGC, I
      integer OOI(MAXRES)
C
      data IIO /STDOUT/
C
      NRES=0
C
      if (IARGC() .eq. 1) then
        IIN=1
        call GETARG(1,FILE)
        open (file=FILE,unit=IIN,form='formatted',err=3)
      else
        IIN=5
      end if
C
      call RDSST(IIN,OOI,NRES)
C
      if (NRES .gt. 2) then
        call STARTGRAP(IIO,NRES,'Ooi')
        do I=1,NRES-1
          write (IIO,'(''line from '',I4,'','',I4,
     +                 '' to '',I4,'','',I4)')
     +                 I, OOI(I), I+1, OOI(I+1)
        end do
        call ENDGRAP(IIO)
      end if
C
      call EXIT(0)
C
3     call ERROR('ooiplot: cannot open file',FILE,1)
C
      end
C
C ########################################################
C
      subroutine STARTGRAP(IIO,NRES,LABEL)
C
      character*(*) LABEL
      integer IIO, NRES
C
      write (IIO,'(''.G1'')')
      write (IIO,'(''label left "'',A,''"'')') LABEL
      write (IIO,'(''frame wid 6 solid right invis top invis'')')
      write (IIO,'(''coords x 0, '',I4)') NRES+1
C
      return
C
      end
C
C ####################################################################
C
      subroutine ENDGRAP(IIO)
C
      write (IIO,'(''.G2'')')
C
      return
      end
C
C ####################################################################
C
      subroutine RDSST(IIN,OOI,NRESTOT)
C
      include '../joy/joy.h' 
C
      character*132 TEXT
      character*5 LABEL(MAXRES)
      character*1 SECSTR, SEQ(MAXRES), SHEET(MAXRES), NULL
      real HBE(MAXRES,4), PHI(MAXRES), PSI(MAXRES), OMEGA
      integer NRES, I, K, IRES, NRESTOT, IIN
      integer IBP(MAXRES,2), HBP(MAXRES,4), OOI(MAXRES)
C
      read (IIN,'(/,/,/,19X,I3)',err=1) NRESTOT
      do I=1,3
        read (IIN,'(A)') NULL
      end do
C
      if (NRESTOT .gt. MAXRES) then
        call ERROR('ooiplot: too many residues in file',' ',1)
      else if (NRESTOT .lt. 1) then
        call ERROR('ooiplot: too few residues in file',' ',1)
      end if
C
      do I=1,NRESTOT
        read (IIN,'(128X,I3)',err=3) OOI(I)
      end do
C
      return
C
1     call ERROR('rdsst: corrupted data file (bad integer)',' ',1)
3     call ERROR('rdsst: corrupted data file (bad data)',' ',3)
C
      end
