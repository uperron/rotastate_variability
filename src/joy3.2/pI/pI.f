      program pI
C
C ----------------------------------------------------------------------------
C
C calculate the pI profile for a given sequence
C
C V1.0 jpo  May 1996 A simple hack to reproduce something in GCG
C  1.1 jpo June 1996 Fixed termini problem (internal chain breaks) and
C                    also fixed sign bug for one of termini
C  1.2 jpo July 1996 Improved format, and noted problem with cysteines
C                    need to use with care in these cases.
C  1.3 jpo July 1996 Added data from Gilson 1996 paper. This has become the
C                    default. Also added parsing of disulphides in sequence
C  1.4 jpo Sept 1996 Added -C flag to state that cysteines are as disulphides
C
C ----------------------------------------------------------------------------
C
      implicit NONE
C
      integer MAXRES, MAXFILELEN
      integer STDOUT, STDIN, STDERR
      parameter (MAXRES=3500)
      parameter (STDOUT=6, STDIN=5, STDERR=0)
      parameter (MAXFILELEN=256)
C
      character*40 DATE
      character*4 VERSION
C
C definitions for library functions
C
      include '../joylib/joylib.h'
C
      character*(MAXFILELEN) FILE, GRAPFILE, DATFILE
      character*79 RULE
      character*10 OPTIONS
      integer ISEQ(MAXRES)
      integer NRES, IIN, IIO, IARGC, I, J, BREAK
      logical DISULP
      real PKA(20), NTERM, CTERM, PH
      real PKA2(20), NTERM2, CTERM2
      real F1, F2, F3, F4, F5, F6, F7, F8, F9
      real FRACION, CTOT, FPOS, FNEG
      integer NNTER, NCTER, NTOT(20), NFREEC
C
C data is from Fersht, Enzyme Structure and Mechanism, 2nd ed., p 156
C
C                   A      C      D      E      F
C                   G      H      I      K      L
C                   M      N      P      Q      R
C                   S      T      V      W      Y
C
      data PKA / 0.00,  9.10,  4.00,  4.50,  0.00,  
     +           0.00,  6.40,  0.00, 10.40,  0.00,  
     +           0.00,  0.00,  0.00,  0.00, 12.00,  
     +           0.00,  0.00,  0.00,  0.00,  9.70 /
      data NTERM / 7.80 /
      data CTERM / 3.60 /
C
C PKA2 is data from Biochem, 35 p 7831, 1996, empirically measured
C values, looks very interesting, n.b. C and R are as above
C
C                   A      C      D      E      F
C                   G      H      I      K      L
C                   M      N      P      Q      R
C                   S      T      V      W      Y
C
      data PKA2 / 0.00,  9.10,  2.70,  4.00,  0.00,  
     +            0.00,  6.90,  0.00, 10.10,  0.00,  
     +            0.00,  0.00,  0.00,  0.00, 12.00,  
     +            0.00,  0.00,  0.00,  0.00, 10.70 /
      data NTERM2 / 8.20 /
      data CTERM2 / 2.70 /
C
      data IIO /STDOUT/
C
C ---------------------------------------------------------------------------
C
      GRAPFILE='./grapfile'
      DATFILE='./pI.dat'
      do I=1,LEN(RULE)
        RULE(I:I)='='
      end do
      NRES=0
      NNTER=1
      NCTER=1
      NFREEC=-1
      do I=1,20
        NTOT(I)=0
      end do
      DISULP=.false.
C
C ----------------------------------------------------------------------------
C
C No options, either a paramter interpreted as a file, or stdin, not very
C general handling of input and output.
C
      if (IARGC() .eq. 1) then
        IIN=1
        call GETARG(1,FILE)
        open (file=FILE,unit=IIN,form='formatted',err=3)
      else if (IARGC() .eq. 2) then
        call GETARG(1,OPTIONS)
        if (OPTIONS(1:2) .eq. '-C') then
          DISULP=.true.
        end if
        call GETARG(2,FILE)
        IIN=1
        open (file=FILE,unit=IIN,form='formatted',err=3)
      else
        IIN=5
      end if
C
      call RDSEQ2(IIN,ISEQ,NRES,BREAK,NFREEC)
C
C get number of termini right
C
      if (NRES .eq. 0) then
        NNTER=0
        NCTER=0
      end if
      NNTER=NNTER+BREAK
      NCTER=NCTER+BREAK
C
C count each residue type
C
      do I=1,20
        do J=1,NRES
          if (ISEQ(J) .eq. I) then
            NTOT(I)=NTOT(I)+1
          end if
        end do
      end do
C
C adjust number of thiol cysteine by NFREEC
C
      if (NFREEC .ge. 0) then
        NTOT(2)=NFREEC
      end if
C
C then adjust according to command line flag
C
      if (DISULP) then
        NTOT(2)=0
      end if
C
C these will overwrite existing files of the same name !
C
      open (unit=1,file=GRAPFILE,status='unknown',form='formatted')
      open (unit=2,file=DATFILE,status='unknown',form='formatted')
C
      call GRAPSTART()
C
      write (IIO,'('' '')')
      write (IIO,'(/,''   pH    R     K     H     Y     C     E     D amino carboxy total   +      -'')')
      write (IIO,'(  ''  pKa'',9F6.2)') PKA2(15),PKA2(9),PKA2(7),PKA2(20),PKA2(2),
     +                                   PKA2(4), PKA2(3), NTERM2, CTERM2
      write (IIO,'(''    N'',9I6)') NTOT(15), NTOT(9), NTOT(7), NTOT(20), NTOT(2),
     +                       NTOT(4), NTOT(3), NNTER, NCTER
      write (IIO,'(A)') RULE
C
C work out fraction of charge for each residue at each pH
C
      do I=0,24   ! 24 comes from pH range ( 0 - 12) * 2
        PH=real(I)/2.0
        F1=real(NTOT(15))*(1.0-FRACION(PKA2(15),PH))
        F2=real(NTOT(9))*(1.0-FRACION(PKA2(9),PH))
        F3=real(NTOT(7))*(1.0-FRACION(PKA2(7),PH))
        F8=real(NNTER)*(1.0-FRACION(NTERM2,PH))
        F4=-1.0*real(NTOT(20))*FRACION(PKA2(20),PH)
        F5=-1.0*real(NTOT(2))*FRACION(PKA2(2),PH)
        F6=-1.0*real(NTOT(4))*FRACION(PKA2(4),PH)
        F7=-1.0*real(NTOT(3))*FRACION(PKA2(3),PH)
        F9=-1.0*real(NCTER)*FRACION(CTERM2,PH)
        FPOS=F1+F2+F3+F8
        FNEG=F4+F5+F6+F7+F9
        CTOT=F1+F2+F3+F4+F5+F6+F7+F8+F9
        write (IIO,'(F5.1,10F6.2,2F7.2)') PH, F1, F2, F3, F4, F5, F6, F7, F8, F9, CTOT, FPOS, FNEG
        write (2,'(4F8.3)') PH, CTOT, FPOS, FNEG
      end do
C
      call GRAPEND()
      close (1)
C
      call EXIT(0)
C
3     call ERROR('pI: cannot open file',FILE,1)
C
      end
C
C ######################################################
C
      real function FRACION(PKA,PH)
C
      real PKA, PH
C
      FRACION=1.0
C
      FRACION=1.0/(1.00+(10.0**(PKA-PH)))
C
      return
      end
C
C ===========================================================
C
      subroutine GRAPSTART()
      integer IIO
      write (1,'(''.G1'')')
      write (1,'(''label left "charge"'')')
      write (1,'(''label bot "pH"'')')
      write (1,'(''frame solid right invis top invis wid 5.0 ht 3.0'')')
      write (1,'(''coord x 0, 14'')')
      write (1,'(''ticks bot out from 0 to 14 by 1'')')
      write (1,'(''line from 0, 0 to 14, 0'')')
      write (1,'(''draw of solid'')')
      write (1,'(''draw af dotted'')')
      write (1,'(''draw ef dashed'')')
      write (1,'(''copy "pI.dat" thru X'')')
      write (1,'(''  next of at $1, $2'')')
      write (1,'(''  next af at $1, $3'')')
      write (1,'(''  next ef at $1, $4'')')
      return
      end
C
      subroutine GRAPEND()
      integer IIO
      write (1,'(''X'')')
      write (1,'(''.G2'')')
      return
      end

C
C ######################################################
C
      subroutine RDSEQ(IO,ISEQ,LENSEQ,BREAK)
C
C reads in one sequence from unit IO
C
      include 'pI.h' 
C
      integer MAXLENLINE
      parameter (MAXLENLINE=80)
C
      character*(MAXLENLINE) LINE
      character*1 SEQ
      integer ISEQ(MAXRES), LENSEQ, IO, LENLINE, N, I, BREAK
      logical FEND
      FEND=.false.
C
      BREAK=0
      N=0
1     read (IO,'(A)',end=2,err=9) LINE
      LENLINE=lastchar(LINE)
      if (index(LINE,'*') .ge. 1) then
        LENLINE=LENLINE-1
        FEND=.true.
      end if
      do I=1,LENLINE
        N=N+1
        if (N .gt. MAXRES) then
          call ERROR('too many residues in sequence',' ',1)
        end if
        read (LINE(I:I),'(A)') SEQ
        if (SEQ .eq. '-') then
          N=N-1
        else if (SEQ .eq. '/') then
          N=N-1
          BREAK=BREAK+1
        else
          ISEQ(N)=SEQ2I(SEQ)
        end if
      end do
      LENSEQ=N
C
      if (FEND) then
        return
      else
        goto 1
      end if
C
      return
C
2     close (UNIT=IO)
      LENSEQ=N
      return
C
9     call ERROR('error reading line:',LINE(1:20),1)
      return
      end
C
C ############################################################################
C
      subroutine RDSEQ2(IO,ISEQ,NRES,BREAK,NFREEC)
C
      include 'pI.h'
C
      character*14 TITLE
      character*80 CARD
      integer IO, NRES, ISEQ(MAXRES), BREAK
      integer NFREEC
C
2     read (IO,'(A)',end=1,err=901) CARD
      if (CARD(1:4) .eq. '>P1;') then
        read (CARD(5:),'(A)') TITLE
        read (IO,'(A)',end=3,err=901) CARD
        if (CARD(1:16) .eq. 'Free cysteines =') then
          read (CARD(17:),'(I4)',err=900) NFREEC
        end if
        call RDSEQ(IO,ISEQ,NRES,BREAK)
      end if
      goto 2
1     return
C
3     call ERROR('rdseq2: error reading sequence',' ',1)
900   call ERROR('rdseq2: error parsing cysteine value',' ',1)
901   call ERROR('rdseq2: error reading ',' ',1)
      end
