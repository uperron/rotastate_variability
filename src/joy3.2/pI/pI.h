      implicit NONE
C
C ----------------------------------------------------------------------------
C
C PARAMETERS
C
C Parameter file for joy. Used as an include file.
C
C MAXSEQ    The maximum number of structures 
C MAXRES    The maximum number of positions in an alignment 
C MAXTITLE  The maximum number of substitution table titles
C MAXNEWSEQ The maximum number of sequences
C MAXLAB    The maximum number of labels (must include structures!!)
C MAXTEXT   The maximum number of text strings 
C NUMFEAT   The number of features (attributes) considered per residue
C NUMEXAM   The number of examined features (not related to anything above)
C MAXOPT    The maximum number of single character options
C SMALL     Arbitrarily small number
C
C MAXHET    Max no of heterogen atoms (used by grcons only)
C MAXATS    Max no of atoms in a sidechain (used by grcons only)
C
C MAXTITLE is obtained by multiplication of the size of all the dimensions of
C NUMFEAT. i.e. at the moment it is 4*2*2*2*2.
C
C Last changed version 2.3e 
C
      integer MAXSEQ, MAXRES, MAXTITLE, MAXNEWSEQ, MAXOPT, MAXLAB,
     -        MAXTEXT, NUMEXAM, NUMFEAT, MAXFILELEN, MAXSHEET, MAXHELIX
      integer MAXHET, MAXATS
      integer STDOUT, STDIN, STDERR
      real SMALL
      parameter (MAXSEQ=60, MAXRES=1000, MAXTITLE=64, MAXOPT=10,
     -           MAXNEWSEQ=10, MAXLAB=3+MAXSEQ, MAXTEXT=6, NUMFEAT=13,
     -           NUMEXAM=12)
      parameter (STDOUT=6, STDIN=5, STDERR=6)
c      parameter (STDOUT=6, STDIN=5, STDERR=0)
      parameter (MAXFILELEN=256, SMALL=1.E-15)
Ccap  parameter (MAXSHEET=40, MAXHELIX=40)
      parameter (MAXATS=30, MAXHET=500)
C
C ----------------------------------------------------------------------------
C
C Declarations for date.h
C
      character*40 DATE
      character*4 VERSION
C
C declarations for VERBOSE variable, reports more information than usual
C
      logical VERBOSE
      common /VERBOSE/VERBOSE
C
C ----------------------------------------------------------------------------
C
      character*50 LBIN
C
C ----------------------------------------------------------------------------
C
C definitions for library functions
C
      include '../joylib/joylib.h'
