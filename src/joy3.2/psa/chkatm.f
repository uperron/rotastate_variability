      subroutine CHKATM(IATMR1,IATMR2,NRES,RESNAM,ATMNAM,NATMS,MISSING)
C
      implicit NONE
      integer NRES, NATMS
      character*3 RESNAM(NRES)
      character*4 ATMNAM(NATMS)
      character*1 RESTYP(20)
      integer IATMR1(NRES), IATMR2(NRES), NUMATM(20), STDERR
      integer I, ITYP, ISTRINT, NA_LIB, NA_BRK
      logical MISSING(NRES), TERMINUS
C
      data STDERR /0/
C
C Residue order below is A,C,D,E,F,G,H,I,K,L,M,N,P,Q,R,S,T,V,W,Y.
C numbers of atoms in each residue type:
C
      data RESTYP /'A','C','D','E','F','G','H','I','K','L',
     +             'M','N','P','Q','R','S','T','V','W','Y'/
      data NUMATM /5,6,8,9,11,4,10,8,9,8,8,8,7,9,11,6,7,7,14,12/
C
C increase for 1 if this the terminal residue (COO !):
C Hack for UNKs
C
      do I=1,NRES
        MISSING(I)=.false.
        TERMINUS=.false.
        if (RESNAM(I) .ne. 'UNK') then    
          ITYP=ISTRINT(RESNAM(I))
        else
          ITYP=21
        end if
C
C ITYP .ne .21 for all the amino acids, effectively hetatoms are
C ignored in this routine
C
        if (ITYP .ne. 21) then
          NA_LIB=NUMATM(ITYP)
C
C this assumes that the last atom name in a residue will be OXT, if at
C C-terminus of chain, not always valid, decided to remove this for the
C moment
C
          if (ATMNAM(IATMR2(I)) .eq. ' OXT') then
            TERMINUS=.true.
            NA_LIB=NA_LIB+1
          end if
          NA_BRK=IATMR2(I)-IATMR1(I)+1
C
          if (NA_BRK .lt. NA_LIB) then
            write (STDERR,'(''psa: missing atoms in '',A,
     +                      '' '',I4,'' ('',I3,
     +                      ''/'',I3,'')'')')
     +                      RESTYP(ITYP),I,NA_BRK,NA_LIB
c           if ( .not. TERMINUS) then
              MISSING(I)=.true.
c           end if
          else if (NA_BRK .gt. NA_LIB) then
            write (STDERR,'(''psa: too many atoms in '',A,
     +                      '' position '',I4,'' -- found '',I3,
     +                      '' expected '',I3)')
     +                      RESTYP(ITYP),I,NA_BRK,NA_LIB
          end if
        end if
      end do
C
      return
      end
