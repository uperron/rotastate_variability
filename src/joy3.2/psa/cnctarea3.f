      subroutine cnctarea(ERRORPAR,PROBE,x,y,z,RADX,NATMS,ACCSS)
C
C a paralleliped spanned by the molecule is divided into cubes of 2*rmax,
C where RMAX is max (rad atom + probe radius):
C
C preprocessed by pfa
C
c     implicit none    ! enable if you dare
      integer MAXATS, MAXCUBES, MAXINTERSECT, MAXATOMSCUBE
      real PI, PIX2
      parameter (MAXATS=10000,
     -           MAXCUBES=10000,
     -           MAXINTERSECT=2000,
     -           MAXATOMSCUBE=100,
     -           PI=3.141592653,
     -           PIX2=2.0*PI)
C
C was real x(NATMS), y(NATMS), z(NATMS), RADX(NATMS), ACCSS(NATMS),
      real x(MAXATS), y(MAXATS), z(MAXATS), RADX(MAXATS), ACCSS(MAXATS),
     -     rad(MAXATS), radsq(MAXATS),
     -     arci(MAXINTERSECT), arcf(MAXINTERSECT),
     -     dx(MAXINTERSECT),dy(MAXINTERSECT),d(MAXINTERSECT),
     -     dsq(MAXINTERSECT),PROBE,ERRORPAR
      real XMIN, YMIN, ZMIN, XMAX, YMAX, ZMAX
      real RMAX
      integer NATMS, NZP, KARC, I
      integer inov(MAXINTERSECT), TAG(MAXINTERSECT), CUBE(MAXATS),
     -        iatm(MAXATOMSCUBE,MAXCUBES),
     -        itab(MAXCUBES)
      integer II1, II2
C
C Various initializations
C
      if (NATMS .gt. MAXATS) then
        call ERROR('psa: cnctarea: too many atoms',
     +             '- increase MAXATS',1)
      end if
C
C assign defaults, if not set
C
      if (ERRORPAR .eq. 0.0) then
        ERRORPAR = 0.05
      end if
C
C number of integration layers along z-axis:
C
      NZP=nint(1./ERRORPAR+0.5)
C
      if (PROBE .eq. 0.0) then
        PROBE = 1.4
      end if
 
      xmin= 9999.9
      ymin= 9999.9
      zmin= 9999.9
      xmax=-9999.9
      ymax=-9999.9
      zmax=-9999.9
C
      RMAX=0.0
C
      karc=MAXINTERSECT
C
C Find bounding box for atoms
C
      do i=1,NATMS
        RAD(i)=RADX(i)+PROBE
        RADSQ(i)=RAD(i)*RAD(i)
        RMAX=MAX(RMAX, RAD(I))
        XMIN=MIN(XMIN, X(I))
        YMIN=MIN(YMIN, Y(I))
        ZMIN=MIN(ZMIN, Z(I))
        XMAX=MAX(XMAX, X(I))
        YMAX=MAX(YMAX, Y(I))
        ZMAX=MAX(ZMAX, Z(I))
      end do
C
      RMAX=RMAX*2.0
C
C Cubicals containing the atoms are setup. the dimension of an edge equals
C the radius of the largest atom sphere
C The cubes have a single index
C
      idim=(xmax-xmin)/RMAX+1.
      IDIM=max(IDIM, 3)
      jidim=(ymax-ymin)/RMAX+1.
      JIDIM=max(JIDIM, 3)
      jidim=idim*jidim
      kjidim=(zmax-zmin)/RMAX+1.
      KJIDIM=max(KJIDIM, 3)
      kjidim=jidim*kjidim
C
      if (kjidim.gt.MAXCUBES) then
        call ERROR('psa: cnctarea: too many cubes',
     +             '- increase MAXATS',1)
      end if
C
C Prepare upto ncube cubes each containing upto MAXATOMSCUBE atoms.
C the cube index is KJI. the number of atoms in each cube is in ITAB;
C the cube index for each atom is in CUBE;
C
C$DOACROSS SHARE(ITAB),LOCAL(L)
      DO 2 L=1,MAXCUBES
        itab(l)=0
    2 CONTINUE
C
      do 4 l=1,NATMS
        i=(x(l)-xmin)/RMAX+1.
        j=(y(l)-ymin)/RMAX
        k=(z(l)-zmin)/RMAX
        kji=k*jidim+j*idim+i
        n=itab(kji)+1
C
        if (n .gt. MAXATOMSCUBE) then
          call ERROR('psa: cnctarea: too many atoms per cube',
     +               '- increase MAXATOMSCUBE',1)
        end if
C
        itab(kji)=n
        iatm(n,kji)=l
        cube(l)=kji
4     continue
C
C Process each atom
C
      do 5 ir=1,NATMS
        kji=cube(ir)
        io=0
        area=0.0
        xr=x(ir)
        yr=y(ir)
        zr=z(ir)
        rr=rad(ir)
        rrx2=rr*2.
        rrsq=radsq(ir)
C
C Find the MKJI cubes neighboring the KJI cube
C
        do kk=1,3
          k=kk-2
          do jj=1,3
            j=jj-2
            do i=1,3
              mkji=kji+k*jidim+j*idim+i  -2
              if (MKJI .ge. 1) then
                if (mkji.gt.kjidim) goto 14
                nm=itab(mkji)
C
C Record the atoms in INOV that neighbor atom IR
C
                do 12 M=1,NM
                  in=iatm(m,mkji)
                  if (IN .ne. IR) then
                    io=io+1
                    if (io.gt.MAXINTERSECT) then
                      call ERROR('psa: cnctarea: too many intersections',
     +                           '- increase MAXINTERSECT',1)
                    end if
                    dx(io)=xr-x(in)
                    dy(io)=yr-y(in)
                    dsq(io)=dx(io)*dx(io)+dy(io)*dy(io)
                    d(io)=sqrt(dsq(io))
                    inov(io)=in
                  end if
12              continue
              end if
            end do
          end do
        end do
C
   14   if (IO .eq. 0) then
          area=pix2*rrx2
        else
C
C Z resolution determined
C
17      zres=rrx2/NZP
        zgrid=z(ir)-rr-zres/2.
C
C Section atom spheres perpendicular to the z axis
C
        do 9 i=1,NZP
          zgrid=zgrid+zres
C
C Find the radius of the circle of intersection of the IR sphere
c on the current z-plane
C
          rsec2r=rrsq-(zgrid-zr)*(zgrid-zr)
          rsecr=sqrt(rsec2r)
C$DOACROSS IF(KARC .GT. 133),SHARE(KARC,ARCI),LOCAL(II1)
          DO 34 II1=1,KARC
            ARCI(II1) = 0.0
34        continue
          karc=0
          do 10 j=1,io
            in=inov(j)
C
C Find radius of circle locus
C
            rsec2n=radsq(in)-(zgrid-z(in))*(zgrid-z(in))
            if (RSEC2N .gt. 0.0) then
              rsecn=sqrt(rsec2n)
C
C Find intersections of N.circles with IR circles in section
C
              if (D(J) .lt. RSECR + RSECN) then
C
C Do the circles intersect, or is one circle completely inside the other?
C
                b=rsecr-rsecn
                if (D(J) .le. ABS (B)) then
                  if (b.le.0.0) goto 9
                else
C
C If the circles intersect, find the points of intersection
C
20                karc=karc+1
C
                  if (karc .ge. MAXINTERSECT) then
                    call ERROR('psa: cnctarea: too many intersections',
     +                 '- increase MAXINTERSECT',1)
                  end if
C
C Initial and final arc endpoints are found for the IR circle intersected
C by a neighboring circle contained in the same plane. the initial endpoint
C of the enclosed arc is stored in arci, and the final arc in arcf
C law of cosines
C
                  alpha=acos((dsq(j)+rsec2r-rsec2n)/(2.*d(j)*rsecr))
C
C Alpha is the angle between a line containing a point of intersection and
C the reference circle center and the line containing both circle centers
C
                  beta=atan2(dy(j),dx(j))+pi
C
C Beta is the angle between the line containing both circle centers and the
C x-axis
C
                  ti=beta-alpha
                  tf=beta+alpha
                  if (ti .lt. 0.0) ti=ti+pix2
                  if (tf .gt. pix2) tf=tf-pix2
                  arci(karc)=ti
                  if (TF .lt. TI) then
C
C If the arc crosses zero, then it is broken into two segments.
c the first ends at PIX2 and the second begins at zero
C
                    arcf(karc)=pix2
                    karc=karc+1
                  end if
    3             arcf(karc)=tf
                end if
              end if
            end if
10    continue
C
C Find the accssible contact surface area for the sphere IR on
C this section
C
        if (KARC .eq. 0) then
          arcsum=pix2
        else
C
C The arc endpoints are sorted on the value of the initial arc endpoint
C
19        call sortag(arci,karc,tag)
C
C Calculate the accssible area
C
          arcsum=arci(1)
          t=arcf(tag(1))
          if (KARC .ne. 1) then
            do II1=2,KARC
              if (T .lt. ARCI(II1)) ARCSUM=ARCSUM+ARCI(II1)-T
              TT=ARCF(TAG(II1))
              T=MAX(T,TT)
            end do
          end if
11        arcsum=arcsum+pix2-t
        end if
C
C The area/radius is equal to the accssible arc length X the section thickness.
C
25      parea=arcsum*zres
C
C Add the accssible area for this atom in this section to the area for this
C atom for all the section encountered thus far
C
        area=area+parea
9       continue
      end if
C
C Scale to VDW shell
C
18    ACCSS(ir)=area*(rr-PROBE)*(rr-PROBE)/rr
C
5     continue
C
      return
      end
