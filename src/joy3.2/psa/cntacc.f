      subroutine cntacc(accss, natms, vdwrad, probe_size)
C
C --- converts from contact to surface areas (about 3 times larger):
C
      real vdwrad(natms), accss(natms)
      do i = 1, natms
        dummy = vdwrad(i) + probe_size
        accss(i) = (accss(i)*dummy*dummy) / (vdwrad(i)*vdwrad(i))
      end do
      return
      end
