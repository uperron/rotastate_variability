C ######################### ANDREJ SALI'S LIBRARY #############################
C
C  joins s1 and s2 strings into string s in such a way that there are
C       no blanks inbetween; s has the same idententation as s1;

C     concat(s1, s2, s)

C  shifts text to the right of string

C     rjust(s)


C  transforms all characters in string s into lower case

C     lower(s)

C  transforms all characters in the string s into upper case

C     upper(s)

C  function lenr(s)    ... index of the last printable character different 
C                             from a blank with no non-printable characters 
C                             before it;
C                             (if no printable character, 0 is returned)

C  function lenl(s)    ... index of the first printable character different
C                             from a blank;
C                             (if no printable character, length+1 is returned)
C                             printable characters: ascii>31 and ascii<128 ;

C  returns the position of the last character of target in the search string
C     (0 if not found)

C     integer function indexr(search, target)

C  returns the index of the last contiguous printable character different
C     from blank (scan started at the first such character in line) (if
C     no printable character in the string, 0 is returned):

C     integer function indexb(line)

C  converts integer i to string s right justified in npos positions

C     i_str(i, s, npos)

C  converts real r to string s with npre and npost places for
C     pre-decimal and post-decimal digits respectively

C     r_str(r, s, npre, npost)

C  reads n integers from string line into integer vector ibuffr

C     str_in(line, ibuffr, n)

C  converts the first number in string s to integer i

C     str_i(s, i)

C  reads n reals from string line into real vector buffr:

C     str_rn(line, buffr, n)

C  converts the first number in string s to real r:

C     str_r(s, r)

C  reads a number of logicals from string line (if type='O', logicals
C     are ON and OFF instead of .T. and .F.):

C     str_ln(line, buffr, n, type)

C  reads one logical variable from line (ON/OFF in upper or lower case 
C     stand for true and false, respectively, if type='O')

C     str_l(line, arg, type)

C  reads n blocks of contigous printable non-blank characters from line and 
C     returns them in a string array buffr;

C     str_sn(line, buffr, n)

C  returns the first contiguous block of printable non-blank characters 
C     in line to string buffr; 

C     str_s(line, buffr) 

C ============================================================================
C
      subroutine concat(s1, s2, s)
C
C  joins s1 and s2 strings into string s in such a way that there are
C     no blanks inbetween; s has the same idententation as s1;
C  user has to ensure that the three strings are of appropriate length ;
C
C -
C
      character*(*) s1, s2, s

C  length of the string variable:
      l1 = len(s1)
C  first right and left printable characters' indices:
      len1r = lenr(s1)
      len1l = lenl(s1)
C  number of printable characters:
      l1str = len1r

      l2 = len(s2)
      len2r = lenr(s2)
      len2l = lenl(s2)
      l2str = len2r - len2l + 1
      l0 = len(s)

C  do tests:
      if ((l1str+l2str) .gt. l0) then  
        write(*,*) 'len(s1), len(s2), len(s3): ', l1str, l2str, l0
        stop 'ERROR:[concat] output string too short'
      end if

C  retain leading blanks in s1:
      s(1:l1str) = s1(1:len1r)

      s(l1str+1:l1str+l2str) = s2(len2l:len2r)

      do 10  i = l1str+l2str+1, l0
10      s(i:) = char(0)

      return
      end
