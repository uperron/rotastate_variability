      subroutine getrad(iatmr1,iatmr2,nres,resnam,atmnam,NATMS,radx,
     *                  atmtyp,rtyp,radtyp,NATYP,nrtyp)
C
C default radius, 1.6A is the radius of a carbon atom
C
      include 'psa.h'
C
      integer NRES, I, IRTYP, ISTRTYP, IAT, IATYP, NRTYP, NATMS
      character*4 atmnam(NATMS), atmtyp(MAXATYP,MAXRTYP)
      character*3 rtyp(nrtyp), resnam(nres)
      integer iatmr1(nres), iatmr2(nres), NATYP(nrtyp)
      real radx(NATMS), radtyp(MAXATYP,MAXRTYP)
C
      do i=1,nres
        irtyp=istrtyp(resnam(I), rtyp, nrtyp)
        if (irtyp .eq. 0) then
          write (STDERR,'(4X,''unknown residue type: '',A,
     +                  '' using infered radii data'')') resnam(I)
          do iat=iatmr1(I), iatmr2(I)
            if (atmnam(iat)(2:2) .eq. 'N') then
              radx(iat)=1.50
            else if (atmnam(iat)(2:2) .eq. 'H') then
              radx(iat)=1.20
            else if (atmnam(iat)(2:2) .eq. 'O') then
              radx(iat)=1.40
            else if (atmnam(iat)(2:2) .eq. 'F') then
              radx(iat)=1.35
            else if (atmnam(iat)(2:2) .eq. 'S') then
              radx(iat)=1.85
            else if (atmnam(iat)(1:2) .eq. 'CL') then
              radx(iat)=1.80
            else if (atmnam(iat)(1:2) .eq. 'BR') then
              radx(iat)=1.95
            else if (atmnam(iat)(2:2) .eq. 'I') then
              radx(iat)=2.15
            else if (atmnam(iat)(2:2) .eq. 'C') then
              radx(iat)=1.75
            else 
              radx(iat)=DEFRAD
            end if
            write (6,*) ' set radius of ',atmnam(iat),' in i ',i,
     +       ' to ',radx(iat)
          end do
        else
          do iat=iatmr1(I),iatmr2(I)
            iatyp=istrtyp(atmnam(iat), atmtyp(1,irtyp), NATYP(irtyp)) 
            if (iatyp .eq. 0) then
              radx(iat)=DEFRAD
            else
              radx(iat)=radtyp(iatyp,irtyp)
            end if
          end do
        end if
      end do
C
      return
      end
