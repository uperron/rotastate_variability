      integer function imnch(atmnam)
        parameter (nm=3)
        character atmnam*4, mnchatm(nm)*4
        data (mnchatm(i),i=1,nm) /' N  ', ' C  ', ' O  '/
        do i = 1, nm
          imnch = i
          if (atmnam .eq. mnchatm(i)) return
        end do
        imnch = 0
        return
      end
