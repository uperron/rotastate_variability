      integer function IPOLSDCH(ATMNAM)
C
      parameter (NP=19)
      character*(*) ATMNAM
      character*4 POLATM(NP)
      data (POLATM(I),I=1,NP) /' AD1', ' AD2', ' AE1', ' AE2', 
     -                         ' ND1', ' ND2', ' NE ', ' NE1',
     -                         ' NE2', ' NH1', ' NH2', ' NZ ',
     -                         ' OD1', ' OD2', ' OE1', ' OE2', 
     -                         ' OG ', ' OG1', ' OH '/
C
      do I=1,NP
        if (ATMNAM .eq. POLATM(I)) then
          IPOLSDCH=I
          return
        end if
      end do
C
      IPOLSDCH=0
C
      return
      end
