      real function PERCENTAGE(x,y)
      real x, y
      if (y.gt.0.0) then
        percentage = 100.0 * x/y
      else
        percentage = 0.0
      end if
      return
      end

c --- this function returns the integer type for the three letter i
c     residue code; if a code can not be recognized, 21 is returned;

      integer function ICODE(res)
      parameter (ntypes = 25)
      integer i
      character res*3, reslst(ntypes)*3
c --- three letter amino acid residue types:
      data reslst /'ALA','ARG','ASN','ASP','CYS','GLN',
     -             'GLU','GLY','HIS','ILE','LEU','LYS',
     -             'MET','PHE','PRO','SER','THR','TRP',
     -             'TYR','VAL','ASX','GLX','UNK','PCA','INI'/
      do 20  i = 1, ntypes
        if (reslst(i) .eq. res) then 
          icode = i
          if (i .eq. 21) icode =  3        ! ASX => ASN
          if (i .eq. 22) icode =  6        ! GLX => GLN
          if (i .gt. 23) icode = 21        ! XXX => GAP
          return
        end if
20    continue
      icode = 21                           ! ??? => GAP
      return
      end

