      program PSA
C
C written by Andrej Sali
C
C     This program will read all atoms from the input Brookhaven file 
C     no matter what residue they are comming from, except for:
C
C     1. B positions for double occupancy atoms.
C     2. Atoms from WAT, HOH or MOH residues when the WATER flag is 
C        set to false (irrespective of whether water occurs in an ATOM or 
C        HETATM record).
C     3. HETATM atoms when the HETATM flag is set to false,
C        except when they are waters and WATER is true.
C
C     It will then assign radii to each of the atoms read in
C     (results will be more precise if all the residues and
C     their atoms are in the library of radii; if they are not, the
C     default radii of 1.8A is assigned). Then, contact or surface
C     area for each atom (waters, if any, including) will be 
C     calculated and BRK format file with accessibilities will be 
C     written out, if so selected. Finally, percentage accessibilities 
C     for standard residues can be calculated (20 standard types plus 
C     ASX, and GLX are treated as ASN, and GLN respectively) and 
C     written out. In residue areas file, all other atoms/residues 
C     will be absent in the output (of course, they will influence the 
C     printed residue contact areas).
C
C Special thanks to Simon Hubbard who provided a better radii library
C     and self-consistent standard contact areas.
C
C Running the program: you need two user 'steering' files:
C
C         1)   psa.inp   : contains commands for PSA (self-explanatory):
C
C                          PROBE_RADIUS       = 1.4
C                          INTEGRATION_STEP   = 0.05
C                          ATM_FILE_CHECK_ONLY= OFF
C                          ATOM_ACC_OUTPUT    = OFF
C                          RESIDUE_ACC_OUTPUT = ON
C                          BRK_FILES_LIST     = psa.lst
C                          INCLUDE_WATER      = ON
C                          INCLUDE_HETATM     = ON
C                          ACCESSIBILITY_TYPE = CONTACT
C
C                          CHECK option is useful if you do not want to wait 
C                          hours to find out that you have to edit the BRK
C                          file and run PSA again. The other accessibility
C                          type is SURFACE. (case insensitive, as it is ON/OFF)
C                          
C         2)   psa.lst   : contains names of Brookhaven atom files to
C                          be processed by the psa program; blank lines
C                          are allowed.
C
C Naming conventions: 
C        Brookhaven files are expected to contain a '.' ;
C        Atom areas file has the same root and '.sol' extension ;
C        Residue areas file has the same root and '.psa' extension ;
C
C jpo - removed check option.
C     - added command line options, no need for steering files now
C     - added logical MISSING, this is set true if atoms are missing from
C       a side chain, it seemed to be quite common for a residue with many
C       atoms missing to be classed as inaccessible, when normalization
C       took place. A partial fix for OXT records, these are often missing
C       from a data set, so don't chuck away dat if this is the case. The
C       best thing to do here would be to add C-terminus records.
C
C Removed all that nasty steering file stuff
C
C     - Changed interface, now a nice unix program. added -t option for stdout
C       output. Read stdin if no filenames.
C
      include 'psa.h'
C
      character*60 OUTFILE, PSADEFDAT, PSADAT, FILE(MAXFIL), STRING
      character*9 DATESTR
      character*8 TIMESTR1, TIMESTR2, TIMESTR3
      character*7 ACCTYP
      character*5 RESNUM(MAXRES), NUMACC(MAXRES)
      character*4 atmtyp(MAXATYP,MAXRTYP), atmnam(MAXATS), VERSION
      character*3 resnam(MAXRES), resacc(MAXRES)
      character*3 extatom, extres
      character*3 RTYP(MAXRTYP)
      integer NATMS, NRES, NACCESS, IOUT, IIN
      integer I, NFILE, NRTYP, NF, LASTCHAR, IARGC
      integer iatmr1(MAXRES), iatmr2(MAXRES), natyp(MAXRTYP)
      integer FPIN, FPOUT
      real STEP, PROBE, TDIFF
      real x(MAXATS), y(MAXATS), z(MAXATS), radx(MAXATS)
      real radtyp(MAXATYP,MAXRTYP), accss(MAXATS), raccss(10,MAXRES)
      logical WATER, HETATM, atompsa, respsa, VERBOSE
      logical FILEEXIST, MISSING(MAXRES), OUTTOSTD
C
      character*30 IDENT
C
      include 'files.h'
C
      data IDENT /' (#) - psa.f - psa            '/
C
C default values for contact area calculation:
C verbose controls the amount of output from psa
C default values for types of output
C default extensions for atom and residue accessibility files
C
      data STEP   /0.05/
      data PROBE  /1.4/
      data WATER  /.false./
      data HETATM /.true./
      data VERBOSE /.false./
      data RESPSA  /.true./
      data ATOMPSA /.false./
      data ACCTYP  /'CONTACT'/
      data EXTRES  /'psa'/
      data EXTATOM /'sol'/
      data OUTTOSTD /.false./
C
      data FPOUT /13/
      data FPIN  /12/
C
      if (VERBOSE) then
        call time(TIMESTR1)
        call date(DATESTR)
        write (STDOUT,'(/''starting contact area calculations '',
     +   A,1X,A,/)') DATESTR,TIMESTR1
      end if
C
C ===============================================================
C
C Command line processing
C
C if there is an argument list then assume that this contains the files to
C be processed. If there are no arguments then read the psa.lst file
C
C arguments overide the defaults found in the input file, and are processed
C in the order they appear in the command line, (later ones overide earlier
C ones
C
C ! in the string denote negation of the option (obviously meaningless for
C -p and -e options. A catch is that ! may mean something special to the
C shell, if this is so then escape ! with \
C
C -pN.n set probe size
C -eN.n set integration step
C -a    set atom accessibility output to true
C -r    set residue accessibility output to true    (default)
C -w    include water atoms
C -h    include hetatms                             (default)
C -c    contact type surface                        (default)
C -s    accessible type surface
C -R    radius library file
C -V    print version
C -t    send output to stdout
C
      NF=1
      do I=1,IARGC()
        call GETARG(I,STRING)
        if (STRING(1:1).ne.'-') then
          FILE(NF)=STRING
          NF=NF+1
        else if (STRING(1:1).eq.'-') then
          if (STRING(2:2) .eq. 'v') then
            VERBOSE=.true.
          else if (STRING(2:2) .eq. 'R') then
            read (STRING(3:),'(A)',err=920) PSADAT
          else if (STRING(2:2) .eq. 'V') then
            write (STDERR,'(''psa - version '',A)') VERSION()
          else if (STRING(2:2) .eq. 'p') then
            read (STRING(3:),'(F4.1)',err=910) PROBE
          else if (STRING(2:2) .eq. 'e') then
            read (STRING(3:),'(F4.1)',err=910) STEP
          else if (STRING(2:2) .eq. 'a') then
            ATOMPSA=.true.
          else if (STRING(2:2) .eq. 'r') then
            RESPSA=.true.
          else if (STRING(2:2) .eq. 't') then
            OUTTOSTD=.true.
            IOUT=STDOUT
          else if (STRING(2:2) .eq. 'w') then
            WATER=.true.
          else if (STRING(2:2) .eq. 'h') then
            HETATM=.true.
          else if (STRING(2:2) .eq. 'c') then
            ACCTYP='CONTACT'
          else if (STRING(2:2) .eq. 's') then
            ACCTYP='SURFACE'
          else if (STRING(2:2) .eq. '!') then
            if (STRING(3:3) .eq. 'a') then
              ATOMPSA=.false.
            else if (STRING(3:3) .eq. 'r') then
              RESPSA=.false.
            else if (STRING(3:3) .eq. 'w') then
              WATER=.false.
            else if (STRING(3:3) .eq. 'h') then
              HETATM=.false.
            else if (STRING(3:3) .eq. 'c') then
              ACCTYP='SURFACE'
            else if (STRING(3:3) .eq. 's') then
              ACCTYP='CONTACT'
            end if
          else
            call ERROR('psa: unknown option ',
     +                 STRING(1:LASTCHAR(STRING)),0)
          end if
        end if
      end do
      NFILE=NF-1
C
C read from stdin if required
C
      if (NFILE .lt. 1) then
        NFILE=1		! dummy to do loop once only
        IIN=STDIN
        IOUT=STDOUT	! have to write to stdout if stdin
      else
        IIN=FPIN
        IOUT=FPOUT
      end if
      if (OUTTOSTD) then
        IOUT=STDOUT
      end if
C
      if (NFILE .gt. MAXFIL) then
        call ERROR('psa: too many files - increase MAXFIL',' ',1)
      end if
C
C Now check to see if the numerical options are sensible
C
      if (PROBE .lt. 0.001) then
        call ERROR('psa: probe size to small',' ',1)
      else if (PROBE .ge. 10.0) then
        call ERROR('psa: unusually large probe size ?',' ',0)
      end if
C
      if (STEP .lt. 0.00001) then
        call ERROR('psa: step size to small',' ',1)
      else if (STEP .ge. 1.0) then
        call ERROR('psa: unusually large step size',' ',1)
      end if
C
C -------------------------------------------------------------------------
C
C JPO - changed this a lot. first of all try -Rfilename, then ./psa.dat, 
C       then system default
C
      if (FILEEXIST(PSADAT)) then
        continue
      else if (FILEEXIST('psa.dat')) then
        PSADAT='psa.dat'
      else
        PSADAT=PSADEFDAT
      end if
      call rdvdw(PSADAT,atmtyp,MAXATYP,rtyp,MAXRTYP,natyp,
     +           nrtyp,radtyp,VERBOSE)
C
C -------------------------------------------------------------------------
C
      do I=1,NFILE
C
C read the Brookhaven file:
C
        if (VERBOSE) then
          write (STDERR,'(/,''processing file: '',i3,1x,A)') I, FILE(I)
        end if
        call RDBRK(IIN,FILE(I),WATER,HETATM,x,y,z,MAXATS,MAXRES,NATMS,
     -             nres,resnam,resnum,atmnam,iatmr1,iatmr2,VERBOSE)
C
C check whether there is an expected number of atoms for each
C        of the 20 residue types: if not then set missing to true
C        for that residue.
C associate radii with each atom:
C
C       call CHKATM(iatmr1,iatmr2,nres,resnam,atmnam,NATMS,MISSING)
        call GETRAD(iatmr1,iatmr2,nres,resnam,atmnam,NATMS,radx,
     -              atmtyp,rtyp,radtyp,natyp,nrtyp)
C
        if (VERBOSE) then
          call TIME(TIMESTR1)
          write (STDERR,'(4X,''surface area calculation started at '',
     +           A)') TIMESTR1
        end if
C
C despite some effort, the following write seems to be important before the
C call to CNCTAREA, this is probably specific to the LPI compiler
C
        open (file='.psa.log',unit=17,form='formatted',err=901)
C
C do the surface calculations for individual atoms:
C
        call CNCTAREA(step,probe,x,y,z,radx,NATMS,accss)
C
        close (unit=17,status='DELETE')
C
C ==========================================================================
C
        if (VERBOSE) then
          call TIME(TIMESTR2)
          write (STDERR,'(4X,''surface area calculation completed at '',
     +            A)') TIMESTR2
c          call TIMEDIFF(TIMESTR1,TIMESTR2,TDIFF,TIMESTR3)
        end if
C
C change to surface areas if so selected:
C
        if (acctyp.eq.'SURFACE') then
          call cntacc(accss, NATMS, radx, probe)
        end if
C
C write out the atoms accessibilities file, if so selected
C input atom file name must have a '.'
C
        if (atompsa) then
          outfile=file(i)(1:index(file(i),'.')) // extatom
          if (VERBOSE) then
            write (STDERR,'(4X,''writing atom areas to '',
     +             A)') outfile
          end if
          call WRBRK(IOUT, file(i), outfile, accss, NATMS, probe, 
     -               step, WATER, HETATM, acctyp)
        end if
C
C write out composite residue contact areas
C calculate various sorts of residue contact areas:
C input atom file name must have a '.'
C
        if (respsa) then
          outfile=file(i)(1:index(file(i),'.')) // extres
          if (VERBOSE) then
            write (STDERR,'(4X,''writing out residue contact '',
     +       ''areas to '',A)') OUTFILE
          end if
          call resarea(iatmr1,iatmr2,nres,atmnam,NATMS,accss,resnam,
     -                 resnum,resacc,numacc,naccess,raccss,
     +                 MAXRES,acctyp)
C
          call wrpsa(IOUT,outfile,raccss,naccess,numacc,resacc,
     -               probe,step,WATER,HETATM,acctyp,MISSING,PSADAT)
        end if
      end do
C
      if (VERBOSE) then
        call time(TIMESTR1)
        call date(DATESTR)
        write (STDERR,'(/,''finished contact area calculations '',
     +         A,1X,A,/)') DATESTR,TIMESTR1
      end if
C
C normal exit
C
      call exit(0)
C
901   call ERROR('psa: cannot open log file',' ',1)
910   call ERROR('psa: error reading option, require real number',
     +           STRING,1)
920   call ERROR('psa: error reading option, require valid string',
     +           STRING,1)
C
      end
