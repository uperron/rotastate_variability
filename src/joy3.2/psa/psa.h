C
C include file for psa
C
      implicit none
      integer MAXATS, MAXRES, MAXATYP, MAXRTYP, MAXFIL
      parameter (MAXATS=10000, MAXRES=1000, MAXATYP=70,
     +           MAXRTYP=200, MAXFIL=20)
      integer STDOUT, STDERR, STDIN
      parameter (STDOUT=6, STDERR=6, STDIN=5)
C
      real DEFRAD
      parameter (DEFRAD=1.6)
C
