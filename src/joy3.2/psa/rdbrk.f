      subroutine RDBRK(IIN,BRKNAME,WATER,HETATM,X,Y,Z,MAXATM,MAXRES,
     +                 NATM,NRES,RESNAM,RESNUM,ATMNAM,IATMR1,IATMR2,
     +                 VERBOSE)
C
      implicit none
      integer MAXATM, MAXRES
      integer STDERR, STDIN
C
      parameter (STDERR=0, STDIN=5)
C
      character*(*) BRKNAME
      character*60 CARD
      character*6 T1
      character*5 RESNUM(MAXRES), ATNUMB, OLDNUM, NEWNUM
      character*4 ATMNAM(MAXATM)
      character*3 RESNAM(MAXRES), OLDNAM, NEWNAM, T2
      character*1 T3
      integer NATM, NRES, IATMR1(MAXRES), IATMR2(MAXRES), LASTCHAR
      integer IIN
      real X(MAXATM), Y(MAXATM), Z(MAXATM)
      logical WATER, HETATM, P, VERBOSE
C
      if (IIN .ne. STDIN) then
        open (unit=IIN, file=BRKNAME, status='OLD', err=200)
      end if
C
      NATM=0
      NRES=0
      OLDNUM='@@@@@'
5     continue
        read (IIN,'(A60)',end=20,err=300)CARD
        T1=CARD(1:6)
        T2=CARD(18:20)
        T3=CARD(17:17)
C
        if ((T1 .eq. 'ATOM  ')) then
            P=((T2 .ne. 'WAT') .and.
     +         (T2 .ne. 'HOH') .and.
     +         (T2 .ne. 'MOH')) .or. WATER
        else
          if (T1 .eq. 'HETATM') then
            if ((T2 .eq. 'WAT') .or.
     +          (T2 .eq. 'HOH') .or.
     +          (T2 .eq. 'MOH')) then
              P=WATER
            else
              P=HETATM
            end if
          else
            P=.false.
          end if
        end if
C
C  occupancy test:
C
        if ((T3 .ne. ' ') .and. (T3 .ne. 'A')) P=.false.
C
        if (P) then
          NATM=NATM + 1
          if (NATM .gt. MAXATM) then
            call ERROR('psa: too many atoms - increase MAXATM',' ',1)
          end if
          read (CARD, 15) ATNUMB, ATMNAM(NATM), NEWNAM,
     +                   NEWNUM, X(NATM), Y(NATM), Z(NATM)
15        format (6x,a5,1x,a4,1x,a3,2x,a5,3x,3f8.3)
        if (NEWNUM .ne. OLDNUM) then
          NRES=NRES+1
          if (NRES .gt. MAXRES) then
            call ERROR('psa: too many residues - increase MAXRES',' ',1)
          end if
            RESNAM(NRES)=NEWNAM
            RESNUM(NRES)=NEWNUM
            OLDNUM=NEWNUM
            OLDNAM=NEWNAM
            IATMR1(NRES)=NATM
            if (NRES .gt. 1) IATMR2(NRES-1)=NATM-1
          end if
        end if
        go to 5
20    continue
      if (NRES .gt. 0) then
        IATMR2(NRES)=NATM
      end if
C
      if (IIN .ne. STDIN) then
        close (unit=IIN)
      end if
C
      if (VERBOSE) then
        write (STDERR,'(4X,''coordinate file read in : '',A,
     +           /,4X,''number of residues      : '',I5,
     +           /,4X,''number of atoms         : '',I5)')
     +        BRKNAME(1:LASTCHAR(BRKNAME)), NRES, NATM
      end if
C
      if (NATM .lt. 1) then
        call ERROR('psa: no atoms in file',' ',1)
      end if
C
      return
C
200   call ERROR('psa: error opening file',BRKNAME(1:LASTCHAR(BRKNAME)),
     +           1)
300   call ERROR('psa: error reading file',BRKNAME(1:LASTCHAR(BRKNAME)),
     +           1)
C
      end
