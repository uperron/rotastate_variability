      subroutine RDLIST(IOINP,FILLST,FILE,NFILE)
C
      include 'psa.h'
C
      integer NFILE, IOINP, I, LASTCHAR
      character*(*) FILE(MAXFIL), FILLST
C
      open (IOINP, file=FILLST, status='OLD', err=901)
      NFILE=1
C
10    read (ioinp,'(A)',end=100) file(NFILE)
        if (LASTCHAR(FILE(NFILE)) .gt. 0) then
          NFILE=NFILE+1
        end if
        go to 10
100   continue
C
      close (IOINP)
      NFILE=NFILE-1
      if (NFILE .lt. 1) then
        write (STDERR,'(''psa: no coordinate files in '',A)') FILLST
        call EXIT(1)
      end if
C
      return
C
901   write (6,'(''psa: error opening list file '',A)') FILLST
      call EXIT(1)
C
      end
