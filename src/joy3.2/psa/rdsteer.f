      subroutine rdsteer(steerfile,probe_radius,step_integr,
     *                   atompsa,respsa,water,hetatm,acctyp)
C
      character CODE*20, cntnts*60, line*80, steerfile*(*)
      character acctyp*(*)
      logical atompsa, respsa, hetatm, water

      open (11, file=steerfile, status='old', err=900)
C
10    read (11,'(A)',end=100) line
        CODE = line(1:20)
        cntnts = line(21:80) 
        if (CODE .eq.'PROBE_RADIUS       =')then
          call STR_R(cntnts,probe_radius)
        else if (CODE .eq. 'INTEGRATION_STEP   =') then
          call STR_R(cntnts,step_integr)
        else if (CODE .eq. 'ATOM_ACC_OUTPUT    =') then
          call STR_L(cntnts,atompsa,'o')
        else if (CODE .eq. 'INCLUDE_WATER      =') then
          call STR_L(cntnts,water,'o')
        else if (CODE .eq. 'INCLUDE_HETATM     =') then
          call STR_L(cntnts,hetatm,'o')
        else if (CODE .eq. 'RESIDUE_ACC_OUTPUT =') then
          call STR_L(cntnts,respsa,'o')
        else if (CODE .eq. 'ACCESSIBILITY_TYPE =') then
          call STR_S(cntnts,acctyp)
C        else if (CODE .eq. 'BRK_FILES_LIST     =') then
C          call STR_S(cntnts,fillst)
        else
          call ERROR('rdsteer: unknown command ',CODE,0)
        end if
        go to 10
100   continue
C
      close (unit=11)
      call UPPER(ACCTYP)
C 
      return
C
900   call ERROR('psa: cannot open steering file',' ',1)
C
      end
