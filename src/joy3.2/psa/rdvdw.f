      subroutine RDVDW(PSADAT,ATMTYP,MAXATYP,RTYP,MAXRTYP,NATYP,NRTYP,
     +                 RADTYP,VERBOSE)
C
      integer STDERR, STDOUT, STDIN
C
      real RADTYP(MAXATYP,MAXRTYP)
      integer NATYP(MAXRTYP)
      character ATMTYP(MAXATYP,MAXRTYP)*4, RTYP(MAXRTYP)*3
      character*(*) PSADAT
      character*20 CARD
      logical WRLIB, VERBOSE
C
      STDERR=0
      STDIN=5
      STDOUT=6
C
C set to true to debug the library
C
      WRLIB=.false.
C
C added # as comment, only between entries though
C
      if (VERBOSE) then
        call ERROR('psa: reading radii data from - ',PSADAT,0)
      end if 
      open (unit=12,file=PSADAT,status='OLD',err=3)
C
      I=1
10    read (12,'(A)',end=100) card
        if (CARD(1:1) .eq. '#') go to 10
        read (CARD,'(A3,I3)',err=901,end=100) RTYP(I), NATYP(I)
        if (NATYP(I) .gt. MAXATYP) then
          write (STDERR,'(''psa: too many atom types in residue '',
     +           A,1X,A)') PSADAT,RTYP(I)
          call EXIT(1)
        end if
        do J=1,NATYP(I)
          read (12,'(A4,F6.2)',err=902) ATMTYP(J,I), RADTYP(J,I)
        end do
        I = I + 1
        if (I .gt. MAXRTYP) then
          write (STDERR,'(''psa: too many residue types '',I3,
     +               '' in library file '',A)') MAXRTYP,PSADAT
          call EXIT(1)
        end if
        go to 10
100   continue
      NRTYP=I-1
C
      close (12)
C
      if (VERBOSE) then
        write (STDOUT,'(''read in atomic radii definitions for '',I3,
     +             '' residues'')')
     +         NRTYP
        if (WRLIB) then
          do I=1,NRTYP
            write (STDOUT,'(I3,''*'',A,''*'',I3,20A)') I,RTYP(I),
     +             NATYP(I),(ATMTYP(K,I),K=1,20)
          end do
        end if
      end if
C
      return
C
3     write (STDERR,'(''psa: rdvdw: cannot open radii library file '',
     +       A)') psadat
      call EXIT(1)
901   write (STDERR,'(''rdvdw: bad format in vdw file '',A)') CARD
      call EXIT(1)
902   write (STDERR,'(''rdvdw: bad atom count in vdw file '',A)')
     +       RTYP(I)
      call EXIT(1)
C
      end
