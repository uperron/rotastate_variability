      subroutine SORTAG(A,N,TAG)
C
      implicit NONE
      integer TAG(N), TG, I, J, IJ, K, N, M, L, IL(16), IU(16)
      real A(N), TT, T
      INTEGER II1, II2
C
      do I=1,N
        TAG(I)=I
      end do
C
      M=1
      I=1
      J=N
C
 5    if (I .ge. J) then
         go to 70
      END IF
C
 10   K=I
      IJ=(J+I)/2
      T=A(IJ)
      IF (A(I) .GT. T) THEN
C
        A(IJ)=A(I)
        A(I)=T
        T=A(IJ)
        TG=TAG(IJ)
        TAG(IJ)=TAG(I)
        TAG(I)=TG
      END IF
 20   L=J
      if (A(J) .ge. T) then
        go to 40
      END IF
C
      A(IJ)=A(J)
      A(J)=T
      T=A(IJ)
      TG=TAG(IJ)
      TAG(IJ)=TAG(J)
      TAG(J)=TG
      if (A(I) .le. T) then
         go to 40
      END IF
C
      A(IJ)=A(I)
      A(I)=T
      T=A(IJ)
      TG=TAG(IJ)
      TAG(IJ)=TAG(I)
      TAG(I)=TG
      go to 40
C
30    A(L)=A(K)
      A(K)=TT
      TG=TAG(L)
      TAG(L)=TAG(K)
      TAG(K)=TG
C
 40   L=L-1
      if (A(L) .gt. T) then
        go to 40
      END IF
      TT=A(L)
C
 50   K=K+1
      if (A(K) .lt. T) then
         go to 50
      END IF
      if (K .le. L) then
         go to 30
      END IF
      if (L-I .le. J-K) then
        go to 60
      END IF
      IL(M)=I
      IU(M)=L
      I=K
      M=M+1
      go to 80
C
 60   IL(M)=K
      IU(M)=J
      J=L
      M=M+1
      go to 80
C
 70   M=M-1
      if (M .eq. 0) return
      I=IL(M)
      J=IU(M)
C
 80   if (J-I .ge. 1) go to 10
      if (I .eq. 1) go to 5
      I=I-1
C
 90   I=I+1
      if (I .eq. J) go to 70
      T=A(I+1)
      if (A(I) .le. T) go to 90
      TG=TAG(I+1)
      K=I
C
  100 II2 = 1
      DO   WHILE ( II2 .EQ. 1 )
        A(K+1) = A(K)
        TAG(K+1)=TAG(K)
        K=K-1
        IF (T .GE. A(K)) II2 = 0
      END DO
      A(K+1)=T
      TAG(K+1)=TG
      go to 90
C
      end
