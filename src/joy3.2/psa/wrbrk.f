      subroutine wrbrk(IO, infile, outfile, accss, natms, probe_radius, 
     *                 step_integr, water, hetatm, acctyp)
C
      integer STDOUT, STDERR
      parameter (IIN=12, STDOUT=6, STDERR=0)
      real accss(natms)
      character infile*(*), outfile*(*), line*80, test1*6, acctyp*(*)
      logical water, hetatm
C
      open (IIN, file=infile, status='old')
C
      if (IO .eq. STDOUT) then
        IOUT=STDOUT
      else
        IOUT=13
        open (IOUT, file=outfile, status='unknown')
      end if
C
      write (IOUT,'(''REMARK - produced by psa, version XXX'')')
      write (IOUT,'(''REMARK   PROBE RADIUS       = '',F8.3,/,
     +              ''REMARK   INTEGRATION STEP   = '',F8.3,/,
     +              ''REMARK   WATER INCLUDED     = '',L8,/,
     +              ''REMARK   HETATM INCLUDED    = '',L8,/,
     +              ''REMARK   ACCESSIBILITY TYPE = '',A)')
     +       probe_radius, step_integr, water, hetatm, acctyp
C
      iatm = 0
10    read (IIN,'(A)', end=100) line
        test1 = line(1:6)
        if ((test1.eq.'ATOM  ').or.(test1.eq.'HETATM')) then
          iatm = iatm + 1
          if (iatm .gt. natms) then
           write (STDERR,'(''psa: surface not calculated for all'',
     +           '' atoms in coordinate file: '',I5)') natms
           return
          end if
          write (IOUT,'(A66,F6.2)') line(1:66), accss(iatm)
        else
          write (IOUT,'(A)') line
        end if
        go to 10
100   continue
      close(IIN)
C
      if (IOUT .ne. STDOUT) then
        close(IOUT)
      end if
C
      if (iatm .lt. natms) then
        call ERROR('psa: more surface than atoms',' ',1)
      end if
C
      return
      end
