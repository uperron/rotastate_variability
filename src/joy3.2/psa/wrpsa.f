      subroutine wrpsa(IOUT,outfile,raccss,naccess,numacc,resacc,
     +                 prob_size,step_integr,water,hetatm,acctyp,
     +                 MISSING,PSADAT)
C
      include 'psa.h'
C
      integer NACCESS
      character*(*) outfile, acctyp,PSADAT
      character*5 numacc(naccess)
      character*4 VERSION
      character*3 resacc(naccess)
      character*1 FLAG
      integer IO, IOUT, IUNIT, I, J
      real raccss(10,naccess), PROB_SIZE, STEP_INTEGR
      logical hetatm, water, MISSING(NACCESS)
C
C IO is IO channel, could be STDOUT, etc (it can be now !)
C
      data IUNIT /13/
C
C   OUTPUT TO FILE; COLUMNS FORMAT:
C   1. # or ACCESS          : (A6),1X
C   2. RESIDUE NUMBER       : (A5),2X
C   3. RESIDUE TYPE         : (A3),1X,A1,1X
C   4. ALL ATOMS            : SUM (F5.2); PERCENTAGE (F5.1),2X
C   5. NON POLAR SIDE CHAINS: the same
C   6. POLAR SIDE CHAINS    : the same
C   7. TOTAL SIDE CHAIN     : the same
C   8. MAIN CHAIN           : the same
C
C modified (16-1-92) so that residues with missing atoms are marked as
C such, with a ! after the residue type (see above)
C
      if (IOUT .eq. STDOUT) then
        IO=STDOUT
      else
        IO=IUNIT
        open (unit=IO, file=OUTFILE, status='unknown', err=900,
     +        form='formatted')
      end if
C
      write (IO,'(''# produced by psa, version '',A)') VERSION()
      write (IO,'(''#'')')
      write (IO,'(''# File of summed (Sum) and % (per.)'',
     +            '' accessibilities'')')
      write (IO,'(''# probe radius       : '',F7.3)') PROB_SIZE
      write (IO,'(''# integration step   : '',F7.3)') STEP_INTEGR
      write (IO,'(''# water included     : '',L7)') WATER
      write (IO,'(''# hetatom included   : '',L7)') HETATM
      write (IO,'(''# accessibility type : '',A)') ACCTYP
      write (IO,'(''# number of residues : '',I7)') NACCESS
      write (IO,'(''# vdw data file      : '',A)') PSADAT
      write (IO,'(''#'')')
      write (IO,'(''#       Res   Res   All atoms   Non P side'',
     +            ''  Polar Side  Total Side  Main Chain'')')
      write (IO,'(''#       Num  type    Sum  Per.   Sum  Per.'',
     +            ''   Sum  Per.   Sum  Per.   Sum  Per.'')')
C
      do i=1,naccess
C
C Hack for UNKs
C
        if (RESACC(I) .eq. 'UNK') then
          continue
        else
          if (MISSING(I)) then
            FLAG='!'
          else
            FLAG=' '
          end if
c          write (IO,'(''ACCESS '',A5,2X,1X,A,1X,1X,A1,F6.2,F5.1,
          write (IO,'(''ACCESS '',A5,2X,A,1X,A1,F6.2,F5.1,
     +           4(1x,f6.2,f5.1))') 
C         write (IO,'(''ACCESS '',a5,2x,a3,1x,5(1x,f6.2,f5.1))') 
C         write (IO,'(''ATOM   '',a5,2x,a3,1x,5(1x,f6.2,f5.1))') 
c    +                NUMACC(I), THREE21(RESACC(I)), FLAG, (RACCSS(J,I),J=1,10)
     +                NUMACC(I), RESACC(I), FLAG, (RACCSS(J,I),J=1,10)
        end if
      end do
C
      if (IO .ne. STDOUT) then
        close (unit=IO)
      end if
C
      return
C
900   call ERROR('psa: error opening file ',OUTFILE,1)
C
      end
