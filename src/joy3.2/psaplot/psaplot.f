      program PSAPLOT
C
C plots out sidechain accessibility (%), can easily be changed though
C
C should add options -s[RA] and -t[RA] for sidechain, total, relative, abs
C
C input (of psa file) either STDIN, or file name on command line
C
      include '../joy/joy.h'
C
      character*(MAXFILELEN) FILE
      character*10 MODE
      character*5 LABEL(MAXRES)
      character*2 OPTION
      character*1 SEQ(MAXRES)
      integer NRES, IIN, IIO, IARGC, I
      real RELSIDE(MAXRES), ABSSIDE(MAXRES)
      real RELTOT(MAXRES), ABSTOT(MAXRES)
      logical SIDEREL, SIDEABS, TOTALABS, TOTALREL
C
      data IIO /STDOUT/
      data SIDEREL /.true./
      data SIDEABS /.false./
      data TOTALREL /.false./
      data TOTALABS /.false./
C
      NRES=0
C
      if (IARGC() .eq. 1) then
        IIN=1
        call GETARG(1,FILE)
        open (file=FILE,unit=IIN,form='formatted',err=3)
      else
        IIN=5
      end if
C
      call RDPSA(IIN,RELSIDE,ABSSIDE,RELTOT,ABSTOT,NRES)
C
C do all the types of accessibility (as specified by options), default
C is for relative sidechain
C
      if (NRES .gt. 2) then
        if (SIDEABS) then
          write (IIO,'(/,''absolute sidechain'',/)')
          call STARTGRAP(IIO,NRES,'\(Ao')
          do I=1,NRES-1
            write (IIO,'(''line from '',I4,'','',F6.2,'' to '',
     +                   I4,'','',F6.2)')
     +                   I, ABSSIDE(I), I+1, ABSSIDE(I+1)
          end do
          call ENDGRAP(IIO)
        else if (SIDEREL) then
          write (IIO,'(/,''relative sidechain'',/)')
          call STARTGRAP(IIO,NRES,'%')
          do I=1,NRES-1
            write (IIO,'(''line from '',I4,'','',F6.2,'' to '',
     +                   I4,'','',F6.2)')
     +                   I, RELSIDE(I), I+1, RELSIDE(I+1)
          end do
          call ENDGRAP(IIO)
        end if
      end if
C
      call EXIT(0)
C
3     call ERROR('psaplot: cannot open file',FILE,1)
C
      end
C
C ########################################################
C
      subroutine STARTGRAP(IIO,NRES,LABEL)
C
      character*(*) LABEL
      integer IIO, NRES
      write (IIO,'(''.G1'')')
      write (IIO,'(''label left "'',A,''"'')') LABEL
      write (IIO,'(''frame wid 6 solid right invis top invis'')')
      write (IIO,'(''coords x 0, '',I4)') NRES+1
C
      return
C
      end
C
C ####################################################################
C
      subroutine ENDGRAP(IIO)
C
      write (IIO,'(''.G2'')')
C
      return
      end
C
C ###################################################
C
      subroutine RDPSA(IIN,SIDREL,SIDABS,TOTREL,TOTABS,NRES)
C
      include '../joy/joy.h'
C
      character*80 TEXT
      integer IIN, LEN, NRES, I
      real TOTABS(MAXRES), TOTREL(MAXRES)
      real SIDREL(MAXRES), SIDABS(MAXRES)
      real NPSABS, NPSREL
      real PLSABS, PLSREL
      real MNCABS, MNCREL
C
      NRES=0
      do I=1,MAXRES
        SIDREL(I)=0.0
        SIDABS(I)=0.0
        TOTREL(I)=0.0
        TOTABS(I)=0.0
      end do
      I=0
C
C Start reading at RESBEG and end reading at RESEND, inclusive
C
10    read (IIN,'(A)',end=100,err=101) TEXT
      if (TEXT(1:6) .ne. 'ATOM  ' .and.
     +    TEXT(1:6) .ne. 'ACCESS') goto 10
      NRES=NRES+1
C
      read (TEXT,'(7X,5X,2X,3X,1X,5(1X,F6.2,F5.1))',err=102)
     -      TOTABS(NRES),TOTREL(NRES),NPSABS,NPSREL,PLSABS,PLSREL,
     -      SIDABS(NRES),SIDREL(NRES),MNCABS,MNCREL
C
      goto 10
C
100   continue
C
C Catch funny data
C
      if (NRES .gt. MAXRES) then
        call ERROR('psaplot: too many residues in file',' ',1)
      end if
C
      return
C
101   call ERROR('psaplot: error reading psa file',' ',1)
102   call ERROR('psaplot: error reading internal file',' ',1)
      end
