      program RAMAPLOT
C
C produce a Ramachandran type map from .sst file in grap language
C code taken from joy and hbplot
C
C input (of sst file) either STDIN, or file name on command line
C
      include '../joy/joy.h'
C
      character*(MAXFILELEN) FILE
      character*10 MODE
      character*5 LABEL(MAXRES)
      character*2 OPTION
      character*1 SEQ(MAXRES)
      integer NRES, IIN, IIO, IARGC
      real PHI(MAXRES), PSI(MAXRES)
      logical CONTOR, EFICON, XPLORCON
C
      data IIO /STDOUT/
      data MODE /'CROSS     '/
      data CONTOR /.true./
      data EFICON /.true./
      data XPLORCON /.false./
C
      NRES=0
C
C must have option describing plotmode
C
      if (IARGC() .eq. 2) then
        call GETARG(1,OPTION)
        if (OPTION(1:1) .ne. '-') then
          call ERROR('ramaplot: missing option',' ',1)
        end if
        call GETARG(2,FILE)
        IIN=1
        open (file=FILE,unit=IIN,status='old',form='formatted',err=3)
      else if (IARGC() .eq. 1) then
        call GETARG(1,OPTION)
        if (OPTION(1:1) .ne. '-') then
          call ERROR('ramaplot: missing option',' ',1)
        end if
        IIN=5
      else
        call ERROR('ramaplot: choose option from -[xdinsl]',' ',1)
      end if
C
C sort out options
C
      if (OPTION(2:2) .eq. 'x') then
        MODE = 'CROSS     '
      else if (OPTION(2:2) .eq. 'd') then
        MODE = 'DOT       '
      else if (OPTION(2:2) .eq. 'i') then
        MODE = 'INDEX     '
      else if (OPTION(2:2) .eq. 'n') then
        MODE = 'NAME      '
      else if (OPTION(2:2) .eq. 's') then
        MODE = 'SEQUENCE  '
      else if (OPTION(2:2) .eq. 'l') then
        MODE = 'LABEL     '
      else
        call ERROR('ramaplot: bad option',OPTION,1)
      end if
C
      call RDSST(IIN,LABEL,SEQ,PHI,PSI,NRES)
      call STARTGRAP(IIO)
      call GRAPPHIPSI(IIO,LABEL,MODE,SEQ,PHI,PSI,NRES)
      if (CONTOR) then
        if (XPLORCON) then
          call XPLORCONTOR(IIO)
        else if (EFICON) then
          call EFICONTOR(IIO)
        end if
      end if
      call ENDGRAP(IIO)
C
      call EXIT(0)
C
3     call ERROR('ramaplot: cannot open file',FILE,1)
C
      end
C
C ########################################################
C
      subroutine STARTGRAP(IIO)
C
C sets up grap for phi/psi map
C
      integer IIO
      write (IIO,'(''.G1'')')
      write (IIO,'(''frame ht 5.5 wid 5.5'')')
      write (IIO,'(''label top "\(*f"'')')
      write (IIO,'(''label right "\(*q"'')')
      write (IIO,'(''label left "\(*q"'')')
      write (IIO,'(''label bot "\(*f"'')')
      write (IIO,'(''coord x -180, 180 y -180, 180'')')
      write (IIO,'(''ticks right out from -180 to 180 by 30'')')
      write (IIO,'(''ticks left out from -180 to 180 by 30'')')
      write (IIO,'(''ticks bot out from -180 to 180 by 30'')')
      write (IIO,'(''ticks top out from -180 to 180 by 30'')')
      write (IIO,'(''line from -180, 0 to 180, 0'')')
      write (IIO,'(''line from 0, -180 to 0, 180'')')
C
      return
C
      end
C
C ####################################################################
C
      subroutine ENDGRAP(IIO)
C
      integer IIO
C
      write (IIO,'(''.G2'')')
C
      return
      end
C
C ####################################################################
C
      subroutine GRAPPHIPSI(IIO,LABEL,MODE,SEQ,PHI,PSI,NRES)
C
C subroutine to write out a grap representation of the phi-psi map for
C a set of coordinates, what is plotted is controlled by the MODE variable.
C Currently recognized are:
C   CROSS    - a cross for non-pro/non-gly, a circle for gly and a square
C              for pro.
C   DOT      - a small dot for each residue
C   INDEX    - the poisition in the sequence
C   SEQUENCE - the one letter amino acid code
C   LABEL    - residue number from PDB file
C   NAME     - sequence and residue number from PDB file
C
      include '../joy/joy.h'
C
      character*10 MODE
      character*5 LABEL(MAXRES)
      character*1 SEQ(MAXRES)
      real PHI(MAXRES), PSI(MAXRES)
      integer I, NRES, IIO
C
      do I=1,NRES
        if (MODE .eq. 'CROSS     ') then
          if (PHI(I) .lt. 180.1 .and. PSI(I) .lt. 180.1) then
            if (SEQ(I) .eq. 'G') then
              write (IIO,'(''circle at '',F8.2,'','',F8.2)')
     +               PHI(I), PSI(I)
            else if (SEQ(I) .eq. 'P') then
              write (IIO,'(''square at '',F8.2,'','',F8.2)')
     +               PHI(I), PSI(I)
            else
              write (IIO,'(''times at '',F8.2,'','',F8.2)')
     +               PHI(I), PSI(I)
            end if
          end if
        else if (MODE .eq. 'DOT       ') then
          if (PHI(I) .lt. 180.1 .and. PSI(I) .lt. 180.1) then
            write (IIO,'(''dot at '',F8.2,'','',F8.2)') 
     +               PHI(I), PSI(I)
          end if
        else if (MODE .eq. 'INDEX     ') then
          if (PHI(I) .lt. 180.1 .and. PSI(I) .lt. 180.1) then
            write (IIO,'(I4,''size 6 at '',F8.2,'','',F8.2)')
     +               I, PHI(I), PSI(I)
          end if
        else if (MODE .eq. 'SEQUENCE  ') then
          if (PHI(I) .lt. 180.1 .and. PSI(I) .lt. 180.1) then
            write (IIO,'(''"'',A,''" size 6 at '',F8.2,'','',F8.2)')
     +             SEQ(I), PHI(I), PSI(I)
          end if
        else if (MODE .eq. 'LABEL     ') then
          if (PHI(I) .lt. 180.1 .and. PSI(I) .lt. 180.1) then
            write (IIO,'(''"'',A,''" size 6 at '',F8.2,'','',F8.2)')
     +             LABEL(I), PHI(I), PSI(I)
          end if
        else if (MODE .eq. 'NAME      ') then
          if (PHI(I) .lt. 180.1 .and. PSI(I) .lt. 180.1) then
            write (IIO,'(''"'',A,A,''" size 6 at '',F8.2,'','',F8.2)')
     +             SEQ(I), LABEL(I)(1:LASTCHAR(LABEL(I))), PHI(I),
     +             PSI(I)
          end if
        else
          call ERROR('ramaplot: unrecognized mode for phipsiplot',
     +               MODE,1)
        end if
      end do
C
      return
      end 
C
C ####################################################################
C
      subroutine RDSST(IIN,LABEL,SEQ,PHI,PSI,NRES)
C
      include '../joy/joy.h' 
C
      character*132 TEXT
      character*5 LABEL(MAXRES)
      character*1 SECSTR, SEQ(MAXRES), SHEET(MAXRES), NULL
      real HBE(MAXRES,4), PHI(MAXRES), PSI(MAXRES), OMEGA
      integer NRES, I, K, IRES, NRESTOT, IIN
      integer IBP(MAXRES,2), HBP(MAXRES,4), OOI(MAXRES)
C
      read (IIN,'(/,/,/,20X,I3)',err=1) NRESTOT
      do I=1,3
        read (IIN,'(A)') NULL
      end do
C
      if (NRESTOT .gt. MAXRES) then
        call ERROR('ramaplot: too many residues in file',' ',1)
      else if (NRESTOT .lt. 1) then
        call ERROR('ramaplot: too few residues in file',' ',1)
      end if
C
      NRES=0
      do I=1,NRESTOT
        NRES=NRES+1
        read (IIN,'(I5,2X,A5,7X,A1,3X,A1,4X,A1,7X,2(1X,I4),
     -        3(1X,F6.1),19X,4(1X,I4,1X,F4.1),3X,I3)',err=3)
     -        IRES,LABEL(NRES),SEQ(NRES),SECSTR,SHEET(NRES),
     +        (IBP(NRES,K),K=1,2),PHI(NRES),PSI(NRES),OMEGA,
     +        (HBP(NRES,K),HBE(NRES,K),K=1,4),OOI(NRES)
        if ((SEQ(NRES).eq.'-').or.(SEQ(NRES).eq.'X')) then
          NRES=NRES-1
        end if
      end do
C
      return
C
1     call ERROR('rdsst: corrupted data file (bad integer)',' ',1)
3     call ERROR('rdsst: corrupted data file (bad data)',' ',3)
C
      end
C
C ####################################################
C
      subroutine XPLORCONTOR(IIO)
C
C attempts to draw some rough contours on the Ramachandran map
C these values were taken from the X-PLOR manual (2.0) by Axel
C Brunger, I do not know where he got them from, they appear to be 
C as good as any set though
C
      integer IIO
C
C divided into different blocks
C
      write (IIO,'(''line from -156.5,  91.3 to  -70.4,  91.3'')')
      write (IIO,'(''line from  -70.4,  91.3 to  -54.7, 112.8'')')
      write (IIO,'(''line from  -54.7, 112.8 to  -54.7, 173.2'')')
      write (IIO,'(''line from  -54.7, 173.2 to -136.9, 173.2'')')
      write (IIO,'(''line from -136.9, 173.2 to -136.9, 155.8'')')
      write (IIO,'(''line from -136.9, 155.8 to -156.5, 135.6'')')
      write (IIO,'(''line from -156.5, 135.6 to -156.5,  91.3'')')
C
      write (IIO,'(''line from -180.0,  42.9 to -140.8,  16.1'')')
      write (IIO,'(''line from -140.8,  16.1 to  -86.0,  16.1'')')
      write (IIO,'(''line from  -86.0,  16.1 to  -74.3,  45.6'')')
      write (IIO,'(''line from  -74.3,  45.6 to  -74.3,  72.5'')')
      write (IIO,'(''line from  -74.3,  72.5 to  -44.3, 102.0'')')
      write (IIO,'(''line from  -44.3, 102.0 to  -44.3, 161.1'')')
      write (IIO,'(''line from  -44.3, 161.1 to  -46.9, 179.9'')')
C
      write (IIO,'(''line from -180.0, -34.9 to -164.3, -42.9'')')
      write (IIO,'(''line from -164.3, -42.9 to -133.0, -42.9'')')
      write (IIO,'(''line from -133.0, -42.9 to -109.5, -32.2'')')
      write (IIO,'(''line from -109.5, -32.2 to -106.9, -21.4'')')
      write (IIO,'(''line from -106.9, -21.4 to  -44.3, -21.4'')')
      write (IIO,'(''line from  -44.3, -21.4 to  -44.3, -71.1'')')
      write (IIO,'(''line from  -44.3, -71.1 to -180.0, -71.1'')')
C
      write (IIO,'(''line from -156.5, -60.4 to -54.7,  -60.4'')')
      write (IIO,'(''line from  -54.7, -60.4 to -54.7,  -40.2'')')
      write (IIO,'(''line from  -54.7, -40.2 to -100.4, -40.2'')')
      write (IIO,'(''line from -100.4, -40.2 to -123.9, -51.0'')')
      write (IIO,'(''line from -123.9, -51.0 to -156.5, -51.0'')')
      write (IIO,'(''line from -156.5, -51.0 to -156.5, -60.4'')')
C
      write (IIO,'(''line from -180.0, -163.8 to -75.6, -163.8'')')
      write (IIO,'(''line from  -75.6, -163.8 to -46.9, -180.0'')')
C
      write (IIO,'(''line from   62.6,   14.7 to  62.6,   96.7'')')
      write (IIO,'(''line from   62.6,   96.7 to  45.6,   79.2'')')
      write (IIO,'(''line from   45.6,   79.2 to  45.6,   26.8'')')
      write (IIO,'(''line from   45.6,   26.8 to  62.6,   14.7'')')
C
      return
      end
C
C %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
C
      subroutine EFICONTOR(IIO)
C
C Attempts to add contours to a phi/psi map, uses the efimov type
C classification, as defined in the paper by wilmott and Thornton
C and as also used by joy
C
      integer IIO
C
C beta
C
      write (IIO,'(''line from  -90.0,   60.0 to  -90.0,  180.0'')')
      write (IIO,'(''line from -150.0,   60.0 to  -60.0,   60.0'')')
      write (IIO,'(''line from -150.0,   60.0 to -170.0,   90.0'')')
      write (IIO,'(''line from -170.0,   90.0 to -180.0,  160.0'')')
      write (IIO,'(''line from  -90.0, -180.0 to  -90.0, -160.0'')')
      write (IIO,'(''line from  -90.0, -160.0 to -100.0, -150.0'')')
      write (IIO,'(''line from -100.0, -150.0 to -150.0, -150.0'')')
      write (IIO,'(''line from -150.0, -150.0 to -180.0, -160.0'')')
C
C polypro
C
      write (IIO,'(''line from  -40.0,  180.0 to -30.0,  150.0'')')
      write (IIO,'(''line from  -30.0,  150.0 to -30.0,  100.0'')')
      write (IIO,'(''line from  -30.0,  100.0 to -50.0,  100.0'')')
      write (IIO,'(''line from  -50.0,  100.0 to -60.0,   80.0'')')
      write (IIO,'(''line from  -60.0,   80.0 to -60.0,   60.0'')')
      write (IIO,'(''line from  -90.0, -160.0 to -60.0, -160.0'')')
      write (IIO,'(''line from  -60.0, -160.0 to -50.0, -170.0'')')
      write (IIO,'(''line from  -50.0, -170.0 to -50.0, -180.0'')')
C
C epsilon
C
      write (IIO,'(''line from  70.0,  180.0 to  70.0,  160.0'')')
      write (IIO,'(''line from  70.0,  160.0 to 100.0,  140.0'')')
      write (IIO,'(''line from 100.0,  140.0 to 100.0,  120.0'')')
      write (IIO,'(''line from 100.0,  120.0 to 140.0,  120.0'')')
      write (IIO,'(''line from 140.0,  120.0 to 140.0,  150.0'')')
      write (IIO,'(''line from 140.0,  150.0 to 180.0,  150.0'')')
      write (IIO,'(''line from 180.0, -140.0 to 120.0, -140.0'')')
      write (IIO,'(''line from 120.0, -140.0 to 110.0, -120.0'')')
      write (IIO,'(''line from 110.0, -120.0 to  90.0, -120.0'')')
      write (IIO,'(''line from  90.0, -120.0 to  70.0, -100.0'')')
      write (IIO,'(''line from  70.0, -100.0 to  50.0, -100.0'')')
      write (IIO,'(''line from  50.0, -100.0 to  40.0, -110.0'')')
      write (IIO,'(''line from  40.0, -110.0 to  40.0, -140.0'')')
      write (IIO,'(''line from  40.0, -140.0 to  70.0, -180.0'')')
C
C alpha left
C
      write (IIO,'(''line from 50.0, 80.0 to 40.0, 60.0'')')
      write (IIO,'(''line from 40.0, 60.0 to 40.0, 40.0'')')
      write (IIO,'(''line from 40.0, 40.0 to 60.0, 20.0'')')
      write (IIO,'(''line from 60.0, 20.0 to 60.0, 10.0'')')
      write (IIO,'(''line from 60.0, 10.0 to 80.0, 10.0'')')
      write (IIO,'(''line from 80.0, 10.0 to 80.0,  0.0'')')
      write (IIO,'(''line from 80.0,  0.0 to 90.0,  0.0'')')
      write (IIO,'(''line from 90.0,  0.0 to 90.0, 40.0'')')
      write (IIO,'(''line from 90.0, 40.0 to 80.0, 40.0'')')
      write (IIO,'(''line from 80.0, 40.0 to 80.0, 50.0'')')
      write (IIO,'(''line from 80.0, 50.0 to 70.0, 70.0'')')
      write (IIO,'(''line from 70.0, 70.0 to 60.0, 80.0'')')
      write (IIO,'(''line from 60.0, 80.0 to 50.0, 80.0'')')
C
C gamma
C
      write(IIO,'(''line from   70.0,  70.0 to  90.0,  70.0'')')
      write(IIO,'(''line from   90.0,  70.0 to 100.0,  60.0'')')
      write(IIO,'(''line from  100.0,  60.0 to 100.0,  40.0'')')
      write(IIO,'(''line from  100.0,  40.0 to 130.0,  10.0'')')
      write(IIO,'(''line from  130.0,  10.0 to 140.0,  10.0'')')
      write(IIO,'(''line from  140.0,  10.0 to 140.0,   0.0'')')
      write(IIO,'(''line from  140.0,   0.0 to 130.0,   0.0'')')
      write(IIO,'(''line from  130.0,   0.0 to 130.0, -20.0'')')
      write(IIO,'(''line from  130.0, -20.0 to 110.0, -40.0'')')
      write(IIO,'(''line from  110.0, -40.0 to 100.0, -40.0'')')
      write(IIO,'(''line from  100.0, -40.0 to 100.0, -30.0'')')
      write(IIO,'(''line from  100.0, -30.0 to  80.0, -30.0'')')
      write(IIO,'(''line from   80.0, -30.0 to  80.0, -10.0'')')
      write(IIO,'(''line from   80.0, -10.0 to  60.0,   0.0'')')
      write(IIO,'(''line from   60.0,   0.0 to  60.0,  10.0'')')
C
C alpha
C
      write (IIO,'(''line from  -140.0,  60.0 to -140.0,  40.0'')')
      write (IIO,'(''line from  -140.0,  40.0 to -150.0,  40.0'')')
      write (IIO,'(''line from  -150.0,  40.0 to -150.0,  20.0'')')
      write (IIO,'(''line from  -150.0,  20.0 to -140.0,  20.0'')')
      write (IIO,'(''line from  -140.0,  20.0 to -140.0,   0.0'')')
      write (IIO,'(''line from  -140.0,   0.0 to -130.0,   0.0'')')
      write (IIO,'(''line from  -130.0,   0.0 to -130.0, -60.0'')')
      write (IIO,'(''line from  -130.0, -60.0 to -120.0, -80.0'')')
      write (IIO,'(''line from  -120.0, -80.0 to  -10.0, -80.0'')')
      write (IIO,'(''line from   -10.0, -80.0 to  -10.0, -40.0'')')
      write (IIO,'(''line from   -10.0, -40.0 to  -30.0,   0.0'')')
      write (IIO,'(''line from   -30.0,   0.0 to  -40.0,   0.0'')')
      write (IIO,'(''line from   -40.0,   0.0 to  -40.0,  20.0'')')
      write (IIO,'(''line from   -40.0,  20.0 to  -60.0,  20.0'')')
      write (IIO,'(''line from   -60.0,  20.0 to  -70.0,  30.0'')')
      write (IIO,'(''line from   -70.0,  30.0 to  -70.0,  40.0'')')
      write (IIO,'(''line from   -70.0,  40.0 to  -60.0,  40.0'')')
      write (IIO,'(''line from   -60.0,  40.0 to  -60.0,  60.0'')')
C
      return
      end
