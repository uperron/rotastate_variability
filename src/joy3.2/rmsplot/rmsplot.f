      program RMSPLOT
C
C program to take two molecules and fit them, then write out a grap
C representation of the position by position variation, interfaces with
C caplot, etc.
C
      parameter   (MAXATS=500)
C
      character*80 TEXT
      character*30 TEXT1(MAXATS),TEXT2(MAXATS)
      character*14 FILE1,FILE2,OPTIONS
      character*5 RESNM1(MAXATS),RESNM2(MAXATS)
      character*4 VERSION
      character*3 RESTP1(MAXATS),RESTP2(MAXATS)
      real X1(3,MAXATS), X2(3,MAXATS), ROT(3,3), C1(3), C2(3),
     +             WT(MAXATS), TRANS(3)
      integer NATS1, NATS2, IIO
      logical FILEEXIST
C
      data WT      /MAXATS*1.0/
      data VERSION /' 0.1'/
      data IIO /6/
C
      if (IARGC() .ne. 2) then
        call ERROR('rmsplot: incorrect no of arguments',' ',1)
      else
        call GETARG(1,FILE1)
        call GETARG(2,FILE2)
        if ( .not. FILEEXIST(FILE1)) then
          call ERROR('rmsplot: file does not exist ',FILE1,1)
        else if ( .not. FILEEXIST(FILE2)) then
          call ERROR('rmsplot: file does not exist ',FILE2,1)
        end if
      end if
C
C Data for X1
C
      open (unit=1,file=FILE1,status='OLD',form='FORMATTED',err=234)
      NATS1=1
10    read (1,'(A)',end=100) TEXT
      if (TEXT(1:6) .eq. 'ATOM  ' .and. TEXT(13:16) .eq. ' CA ') then
	READ (TEXT,'(A30,3F8.3)') TEXT1(NATS1),(X1(I,NATS1),I=1,3)
	READ (TEXT,'(17X,A3,2X,A5)') RESTP1(NATS1),RESNM1(NATS1)
        NATS1=NATS1+1
      end if
      go to 10
100   NATS1=NATS1-1
      close (unit=1)
C
C Data for X2
C
      open (unit=1,file=FILE2,status='OLD',form='FORMATTED',err=234)
      NATS2=1
20    read (1,'(A)',end=200) TEXT
      if (TEXT(1:6) .eq. 'ATOM  ' .and. TEXT(13:16) .eq. ' CA ') then
	READ (TEXT,'(A30,3F8.3)') TEXT2(NATS2),(X2(I,NATS2),I=1,3)
	READ (TEXT,'(17X,A3,2X,A5)') RESTP2(NATS2),RESNM2(NATS2)
        NATS2=NATS2+1
      end if
      go to 20
200   NATS2=NATS2-1
      close (unit=1)
C
C Check bounds
C
      if (NATS1 .lt. 3) then
        call ERROR('rmsplot: less than three points in molecule 1',
     +              ' ',1)
      else if (NATS2 .lt. 3) then
        call ERROR('rmsplot: less than three points in molecule 2',
     +              ' ',1)
      else if (NATS1 .gt. MAXATS) then
        call ERROR('rmsplot: more tahn MAXATS in molecule 1',' ',1)
      else if (NATS2 .gt. MAXATS) then
        call ERROR('rmsplot: more tahn MAXATS in molecule 2',' ',1)
      else if (NATS1 .ne. NATS2) then
        call ERROR('rmsplot: unequal no of residues in two files',
     +              ' ',1)
      end if
C
C Calculate COG of first set of coordinates
C Calculate COG of second set of coordinates
C Find the rotation matrix that maps second set of coords onto first
C Apply this transformation to the second set of coordinates
C Find RMS between two coordinate sets
C
      call ORIGIN(X1(1,1),TEMP,NATS2,C1)
      call ORIGIN(X2(1,1),TEMP,NATS2,C2)
      call MATFIT(X1(1,1),X2(1,1),ROT,NATS2,0,0.)
      call ROTATE(X2,ROT,NATS2)
      call RMSFIT(X1,X2,RMSD,NATS2)
C
      write (IIO,'(A,2X,A)') FILE1(1:LASTCHAR(FILE1)),
     +                       FILE2(1:LASTCHAR(FILE2))
      call GRAPSTART(IIO,NATS2)
      do I=1,NATS2-1
        write (IIO,'(''line from '',I4,'', '',F6.3,'' to '',
     +           I4,'','',F6.3)')
     +          I,sqrt(DIST2(X1(1,I),X2(1,I))),
     +          I+1,sqrt(DIST2(X1(1,I+1),X2(1,I+1)))
      end do
      call GRAPEND(IIO)
C
      call exit(0)
234   call ERROR('rmsplot: error opening file',' ',1)
      end
C
C ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
C
      subroutine GRAPSTART(IIO,NRES)
      integer IIO
      write (IIO,'(''.G1'')')
      write (IIO,'(''ticks off'')')
      write (IIO,'(''ticks left out'')')
      write (IIO,'(''ticks bot out'')')
      write (IIO,'(''frame wid 6 solid top invis right invis'')')
      write (IIO,'(''coord x 0,'',I4)') NRES+1
      return
      end
C
      subroutine GRAPEND(IIO)
      integer IIO
      write (IIO,'(''.G2'')')
      return
      end
C
C %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
C
      SUBROUTINE RMSFIT(X1,X2,RMS,N)
C
C Subroutine that returns the RMSD between two sets of coordinates
C
      REAL X1(3,1),X2(3,1)
C
      RMS=0.
      DO I=1,N
        RMS=RMS+(X1(1,I)-X2(1,I))**2+
     -        (X1(2,I)-X2(2,I))**2+
     -        (X1(3,I)-X2(3,I))**2
      end do
      RMS=SQRT(RMS/FLOAT(N))
C
      END
C
      subroutine ROTATE(X,R,N)
C
C  Rotates contents of X using rotation matrix R.
C
      integer I, N
      real X(3,N), R(3,3), X1, X2, X3
C
      do I=1,N
        X1=R(1,1)*X(1,I)+R(1,2)*X(2,I)+R(1,3)*X(3,I)
        X2=R(2,1)*X(1,I)+R(2,2)*X(2,I)+R(2,3)*X(3,I)
        X3=R(3,1)*X(1,I)+R(3,2)*X(2,I)+R(3,3)*X(3,I)
        X(1,I)=X1
        X(2,I)=X2
        X(3,I)=X3
      end do
C
      return
      end
C
C ----------------------------------------------------------------------------
C
      subroutine ORIGIN(X1,X2,N,C)
C
C calculates the center of origin of the data set X1, then
C
      integer N, I, J
      real X1(3,N), X2(3,N), C(3)
C
C Calculate the center of mass of X1
C
      do I=1,3
        C(I)=0.
      end do
      do I=1,N
        do J=1,3
          C(J)=C(J)+X1(J,I)
        end do
      end do
      do I=1,3
        C(I)=C(I)/real(N)
      end do
C
C Translate X1 to the origin
C
      do I=1,N
        do J=1,3
          X1(J,I)=X1(J,I)-C(J)
        end do
      end do
C
C Translate X2 by the same vector
C
      do I=1,N
        do J=1,3
          X2(J,I)=X2(J,I)-C(J)
        end do
      end do
C
      return
      end
C
C ----------------------------------------------------------------------------
C
      subroutine MATFIT(X1,X2,RM,N)
C
      integer N, I, J, K
      real UMAT(3,3), RM(3,3), X1(3,4), X2(3,4)
C
      do I=1,3
        do J=1,3
           UMAT(I,J)=0.0
        end do
        do J=1,N
          do K=1,3
            UMAT(I,K)=UMAT(I,K)+X1(I,J)*X2(K,J)
          end do
        end do
      end do
C
C Fit it
C
      call QIKFIT(UMAT,RM)
C
      return
      end
C
C ----------------------------------------------------------------------------
C
      subroutine QIKFIT(UMAT,RM)
C
      integer JMAX, NCYC, NSTEEP, NCC1, NREM, I, J, K, L, M
      real RSUM, UMAT(3,3), ROT(3,3), COUP(3), DIR(3), STEP(3)
      real TURMAT(3,3), C(3,3), V(3), RM(3,3), SMALL, SMALSN
      real RTSUM, DELTA, CLEP, CLE, GFAC, RTSUMP, DELTAP, STP
      real STCOUP, UD, TR, TA, CS, SN, AC
C
      data SMALL  /1.0E-20/
      data SMALSN /1.0E-10/
C
      do L=1,3
        do M=1,3
          ROT(L,M)=0.0
        end do
        ROT(L,L)=1.0
      end do
C
      JMAX=30
      RTSUM=UMAT(1,1)+UMAT(2,2)+UMAT(3,3)
      DELTA=0.0
C
      do NCYC=1,JMAX
C
C modified conjugate gradient for first and every NSTEEP cycles set previous
C step as zero and take steepest descent path
C
        NSTEEP=3
        NCC1=NCYC-1
        NREM=NCC1-(NCC1/NSTEEP)*NSTEEP
        IF (NREM .ne. 0) goto 1
        do I=1,3
          STEP(I)=0.0
        end do
        CLEP=1.0
1       continue
C
C couple
C
        COUP(1)=UMAT(2,3)-UMAT(3,2)
        COUP(2)=UMAT(3,1)-UMAT(1,3)
        COUP(3)=UMAT(1,2)-UMAT(2,1)
        CLE=SQRT(COUP(1)**2+COUP(2)**2+COUP(3)**2)
C
C gradient vector is now -COUP
C
        GFAC=(CLE/CLEP)**2
C
C VALUE OF RTSUM FROM PREVIOUS STEP
C
        RTSUMP=RTSUM
        DELTAP=DELTA
        CLEP=CLE
        IF(CLE.LT.SMALL) GOTO 2
C
C STEP VECTOR CONJUGATE TO PREVIOUS
C
        STP=0.0
        do I=1,3
          STEP(I)=COUP(I)+STEP(I)*GFAC
          STP=STP+STEP(I)**2
        end do
        STP=1.0/SQRT(STP)
C
C NORMALISED STEP
C
        do I=1,3
          DIR(I)=STP*STEP(I)
        end do
C
C COUPLE RESOLVED ALONG STEP DIRECTION
C
        STCOUP=COUP(1)*DIR(1)+COUP(2)*DIR(2)+COUP(3)*DIR(3)
C
C COMPONENT OF UMAT ALONG DIRECTION
C
        UD=0.0
        do L=1,3
          do M=1,3
            UD=UD+UMAT(L,M)*DIR(L)*DIR(M)
          end do
        end do
        TR=UMAT(1,1)+UMAT(2,2)+UMAT(3,3)-UD
        TA=SQRT(TR**2+STCOUP**2)
        CS=TR/TA
        SN=STCOUP/TA
C
C IF(CS.LT.0) THEN PRESENT POSITION IS UNSTABLE SO do NOT STOP
C
        IF ((CS.gt.0.0).and.(ABS(SN).lt.SMALSN)) goto 2
  150   CONTINUE
C
C TURN MATRIX FOR CORRECTING ROTATION
C
C SYMMETRIC PART
C
        AC=1.0-CS
        do L=1,3
          V(L)=AC*DIR(L)
          do M=1,3
            TURMAT(L,M)=V(L)*DIR(M)
          end do
        TURMAT(L,L)=TURMAT(L,L)+CS
        V(L)=DIR(L)*SN
        end do
C
C ASSYMETRIC PART
C
        TURMAT(1,2)=TURMAT(1,2)-V(3)
        TURMAT(2,3)=TURMAT(2,3)-V(1)
        TURMAT(3,1)=TURMAT(3,1)-V(2)
        TURMAT(2,1)=TURMAT(2,1)+V(3)
        TURMAT(3,2)=TURMAT(3,2)+V(1)
        TURMAT(1,3)=TURMAT(1,3)+V(2)
C
C UPDATE TOTAL ROTATION MATRIX
C
        do L=1,3
          do M=1,3
            C(L,M)=0.0
            do K=1,3
              C(L,M)=C(L,M)+TURMAT(L,K)*ROT(K,M)
            end do
          end do
        end do
        do L=1,3
          do M=1,3
            ROT(L,M)=C(L,M)
          end do
        end do
C
C UPDATE UMAT TENSOR
C
        do L=1,3
          do M=1,3
            C(L,M)=0.0
            do K=1,3
              C(L,M)=C(L,M)+TURMAT(L,K)*UMAT(K,M)
            end do
          end do
        end do
        do L=1,3
          do M=1,3
            UMAT(L,M)=C(L,M)
          end do
        end do
        RTSUM=UMAT(1,1)+UMAT(2,2)+UMAT(3,3)
        DELTA=RTSUM-RTSUMP
C
C IF NO IMPROVEMENT IN THIS CYCLE THEN STOP
C
        IF (ABS(DELTA).LT.SMALL) GOTO 2
C
C NEXT CYCLE
C
      end do
2     continue
      RSUM=RTSUM
C
C COPY ROTATION MATRIX FOR OUTPUT
C
      do I=1,3
        do J=1,3
          RM(J,I)=ROT(I,J)
        end do
      end do
C
      return
      end
