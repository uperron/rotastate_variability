      logical function COV(COORDS,LENRES,HCOORDS,DISTVAL2,
     +                     RESNAM,RESNUM,CHNNAM,ATM)
C
C returns if a given residue is within sqrt(DISTVAL2 of) a given coordinate
C used for detecting likely covalent bonds to a ligand.
C
      include 'site.h'
C
      integer LENRES, I
      real COORDS(3,MAXATS)
      real HCOORDS(3)
      real BONDVAL2
      character*5 RESNUM
      character*3 RESNAM, ATM(MAXATS)
      character*1 CHNNAM
C
      BONDVAL2=4.0
C
      do I=1,LENRES
        if (DIST2(COORDS(1,I),HCOORDS) .le. BONDVAL2) then
            write (6,'(''site: covalent link ? '',A,1X,A,1X,A,1X,A)')
     +              ATM(I),RESNAM,RESNUM,CHNNAM
        end if
      end do 
C
      return
      end
