      logical function NEIGHBOUR(COORDS,LENRES,HCOORDS,DISTVAL2)
C
C returns if a given residue is within DISTVAL2 of a given coordinate
C
      include 'site.h'
C
      integer LENRES, I
      real COORDS(3,MAXATS)
      real HCOORDS(3)
C
      NEIGHBOUR=.false.
C
      do I=1,LENRES
        if (DIST2(COORDS(1,I),HCOORDS) .le. DISTVAL2) then
          NEIGHBOUR=.true. 
          return
        end if  
      end do 
C
      return
      end
C
C =======================================================================
C
      logical function NEIGHBOURM(COORDS,LENRES,HCOORDS,DISTVAL2)
C
C returns if a given HETATM is within DISTVAL2 of a given residue
C
C should really invert this routine for speed, but....
C
      include 'site.h'
C
      integer LENRES, I
      real COORDS(3,MAXATS)
      real HCOORDS(3)
C
      NEIGHBOURM=.false.
C
      do I=1,LENRES
        if (DIST2(COORDS(1,I),HCOORDS) .le. DISTVAL2) then
          NEIGHBOURM=.true.
          return
        end if
      end do
C
      return
      end
C
C ========================================================================
C
      real function CLOSEST(COORDS,LENRES,HCOORDS,NHET)
C
C returns closest approach of any atom in a residue to any HETATM
C
      include 'site.h'
C
      integer LENRES, I
      real COORDS(3,MAXATS), MINVAL
      real HCOORDS(3,MAXHET)
      integer NHET, J
C
C note as always, operate with squared values
C
C initial minval should be big enough but you never know.
C
      MINVAL=9999.99**2
C
      do I=1,NHET
        do J=1,LENRES
          if (DIST2(COORDS(1,J),HCOORDS(1,I)) .lt. MINVAL) then
            MINVAL=DIST2(COORDS(1,J),HCOORDS(1,I))
          end if
        end do 
      end do
C
      CLOSEST=sqrt(MINVAL)
C
      return
      end
