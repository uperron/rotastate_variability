      subroutine RDPDB(FILE,ATM,RESNUM,RESNAM,CHNNAM,COORDS,OCCUP,BVAL,
     +                 LENRES,NATM,NRES,NHET,HETCOORDS,HETNUM,HETNAM,
     +                 HETATM,VERBOSE)
C
C Subroutine that reads a PDB format file. ATOMS and HETATM are read into
C separate arrays. If two (or more) conformers of the same sidechain exist
C then the A form is taken in preference.
C
C checking of data is minimal, should run through a program like pdb2atm
C
C VERBOSE echos back a lot of information to the user
C
      include 'site.h'
C
C -------------------------------------------------------------------------
C
      character*(*) FILE
      character*66 CARD, PREVCARD
      character*8 ITOA
      character*5 RESNUM(MAXRES), HETNUM(MAXHET)
      character*4 HETATM(MAXHET)
      character*3 RESNAM(MAXRES), HETNAM(MAXHET), ATM(MAXATS,MAXRES)
      character*1 CHNNAM(MAXRES)
      integer LENRES(MAXRES), K, I, LINE, NATM, NATS, NHET, NRES, IERR
      real COORDS(3,MAXATS,MAXRES), OCCUP(MAXATS,MAXRES)
      real BVAL(MAXATS,MAXRES), HETCOORDS(3,MAXHET), HETBVAL(MAXHET)
      real HETOCCUP(MAXHET)
      logical VERBOSE
C
      character*35 IDENT
      data IDENT /'@(#) rdpdb.f - (c) 1990            '/
C
C -------------------------------------------------------------------------
C
      do I=1,MAXRES
        LENRES(I)=0
      end do
C
C -------------------------------------------------------------------------
C
C changed status to old to prevent errors
C
      open (unit=1,file=FILE,status='old',form='formatted',err=999,
     -      iostat=IERR)
C
C -------------------------------------------------------------------------
C
C only recognized tokens are 'ATOM  ' and 'HETATM'
C
      LINE=0
      NATM=0
      NHET=0
      NRES=1
      NATS=1
C
1     read (1,'(A)',end=100,err=999,iostat=IERR) CARD
      LINE=LINE+1
C
C atom records
C
      if (CARD(1:6).eq.'ATOM  ') then
        NATM=NATM+1
        if (NATM.gt.1) then
          if (CARD(23:27).ne.PREVCARD(23:27)) then
            NRES=NRES+1
            if (NRES .gt. MAXRES) then
              call ERROR('site: too many residues in file',' ',1)
            end if
            NATS=1
          end if
        end if
        read (CARD,'(13X,A3,1X,A3,1X,A1,A5,3X,3F8.3,2F6.2)',err=903)
     +        ATM(NATS,NRES),RESNAM(NRES),CHNNAM(NRES),RESNUM(NRES),
     +        (COORDS(K,NATS,NRES),K=1,3),OCCUP(NATS,NRES),
     +        BVAL(NATS,NRES)
        LENRES(NRES)=NATS
        NATS=NATS+1
        if (NATS .gt. MAXATS) then
          call ERROR('site: too many atoms in residue',
     +               '- increase MAXATS',1)
        end if
        PREVCARD=CARD
        go to 1
C
C hetatm records
C
      else if (CARD(1:6).eq.'HETATM') then
        NHET=NHET+1
        if (NHET.gt.MAXHET) then
          call ERROR('site: too many het atoms','- increase MAXHET',1)
        end if
        read (CARD,'(12X,A4,1X,A3,2X,A5,3X,3F8.3,2F6.2)',err=903)
     +        HETATM(NHET),HETNAM(NHET),HETNUM(NHET),
     +        (HETCOORDS(K,NHET),K=1,3),
     +        HETOCCUP(NHET),HETBVAL(NHET)
        go to 1
C
C all other records
C
      else
        go to 1
      end if
C
100   continue
C
      if (VERBOSE) then
        write (6,'(''number of atoms read    = '',I4)') NATM
        write (6,'(''number of residues read = '',I4)') NRES
        if (NRES .gt. 0) then
          write (6,'('' index type number chn len'')')
          do I=1,NRES
            write (6,'(I6,2X,A3,2X,A5,3X,A1,I4)')
     -             I,RESNAM(I),RESNUM(I),CHNNAM(I),LENRES(I)
          end do
        end if
        write (6,'(''number of hetatoms read = '',I4)') NHET
        if (NHET .gt. 0) then
          write (6,'('' index atom  name number'')')
          do I=1,NHET
            write (6,'(I6,2X,A4,2X,A3,2X,A5,3X,A1,I7)')
     -             I,HETATM(I),HETNAM(I),HETNUM(I)
          end do
        end if
      end if
      return
C
C errors
C
903   call ERROR('site: error reading internal file at line ',
     +           ITOA(LINE),1)
999   write (6,'(''site: error reading PDB file at line '',I5,
     +           '' iostat is '',I5)') LINE, IERR
      call EXIT(1)
C
      end
