      program SITE 
C
C 1.0a read a PDB file, and write out residues that are within a given range of
C      the HETATM records (n.b. including water)
C
C 1.0b added writing out of sequence data
C
C 1.0c added checking on number of residues to prevent null output
C      increased size of OPTIONS string (maybe too small ?) also
C      increased length of MAXFILELEN parameter
C
C 1.0d added psa style output option , and also reporting of minimum distance
C      of any atom in a residue to any atom in the hetatom set. This is meant to
C      be used in site analysis within joy
C
C 1.1a added list style output of residue indices (designed for input to other programs)
C      initial format is blank delimited (maybe better is packed csv, but we will see
C      what is required.
C
C ============================================================================
C
      include 'site.h'
C
      integer MAXARGS
      parameter (MAXARGS=256)
      character*(MAXFILELEN) INFILE, OUTFILE, FILES(MAXARGS), PSSFILE
      character*28 OPTIONS
      character*5 RESNUM(MAXRES), HETNUM(MAXHET)
      character*4 VERSION, HETATM(MAXHET)
      character*3 RESNAM(MAXRES), ATM(MAXATS,MAXRES), HETNAM(MAXHET)
      character*2 HBTYPE
      character*1 CHNNAM(MAXRES), SEQ(MAXRES), FLAG, ALT
      integer LENRES(MAXRES), NRES, NATS, NHET, IARGC, IOUT
      integer I, J, K, L, ISPAN, NCHN, IPREV, NCH, NFILES, III, N
      integer NDOT
      integer NSITE, NINDEX(MAXRES)
      real COORDS(3,MAXATS,MAXRES), OCCUP(MAXATS,MAXRES)
      real BVAL(MAXATS,MAXRES)
      real HETCOORDS(3,MAXHET), DIST, ANGLCO, ANGLNH, HDIST, ENERGY
      real HATM(3,MAXRES), CATM(3), SHATM(3,MAXSIDE,MAXRES), HBENG
      real VAL(MAXRES), CLOSEST
      logical DONOR(MAXATS,MAXRES), ACCEPTOR(MAXATS,MAXRES)
      logical DOHET, DOWAT, VERBOSE, TERMINUS(2,MAXRES)
      logical NEIGHBOUR, YES, DOCOV, COV, MUTUAL, NEIGHBOURM, DAVE
      logical SEQUENCE, SITERES(MAXRES)
      logical MARCEL
C
      character*31 IDENT
      data IDENT /'@(#)  site.f - joy (c) jpo 1996'/
C
      include 'date.h'
C
      data DOHET    /.true./
      data DOWAT    /.true./
      data DOCOV    /.false./
      data DAVE     /.false./
      data VERBOSE  /.false./
      data MUTUAL   /.false./
      data SEQUENCE /.true./
      data MARCEL   /.false./
C
C jpo 17-5-96: added mutual to define sites a little better
C jpo 15-5-98: added output of sequence at bottom of PDB file
C jpo 30-3-99: added output of resicude index
C jpo 7-10-99: added Marcel option (m)
C
C -----------------------------------------------------------------------
C
C Get command line options
C
C Check no of arguments
C
      if (IARGC() .gt. MAXARGS) then
        call ERROR('site: argument list too long',' ',1)
      end if
C
C get first argument to see if it is an option, if it is then get all
C filenames
C If first argument not an option then get all filenames
C
C default output stream is 2 (IOUT)
C
      IOUT=2
C
      do I=1,MAXRES
        SITERES(I)=.false.
      end do
C
C default distance cutoff
C
      DISTVAL=4.5
      DISTVAL2=DISTVAL*DISTVAL
C
      call GETARG(1,OPTIONS)
      if (OPTIONS(1:1) .eq. '-') then
        if (index(OPTIONS,'V') .gt. 0) then
          write (STDERR,'(''site - version '',A,'' ('',A,
     -           '') - copyright 1996 jpo, compiled - '',A)')
     -           VERSION(),LIBVERSION(),DATE
          if (IARGC() .lt. 2) then
            call EXIT(0)
          end if
        end if
        if (index(OPTIONS(2:),'c') .gt. 0) then
          DOCOV=.true.
        end if
        if (index(OPTIONS(2:),'-') .gt. 0) then
          IOUT=STDOUT
        end if
        if (index(OPTIONS(2:),'D') .gt. 0) then
          DAVE=.true.
        end if
        if (index(OPTIONS(2:),'M') .gt. 0) then
          MUTUAL=.true.
        end if
        if (index(OPTIONS(2:),'m') .gt. 0) then
          MARCEL=.true.
          DISTVAL=6.5
          DISTVAL2=DISTVAL*DISTVAL
        end if
        if (index(OPTIONS(2:),'d') .gt. 0) then
          read (OPTIONS(3:),'(F5.3)',err=904) DISTVAL
          DISTVAL2=DISTVAL*DISTVAL
        end if
        NFILES=0
        do I=2,IARGC()
          NFILES=NFILES+1
          call GETARG(I,FILES(NFILES))
        end do
        NFILES=IARGC()-1
      else
        do I=1,IARGC()
          call GETARG(I,FILES(I))
        end do
        NFILES=IARGC()
      end if
C
C -----------------------------------------------------------------------
C
C loop around all files in argument list
C
C control of files
C
      do III=1,NFILES
        INFILE=FILES(III)
C
C if file does not have an extension, generate one with .atm extension
C
        if (index(INFILE,'.') .lt. 1) then
          if (FILEEXIST(INFILE(1:LASTCHAR(INFILE))//'.atm')) then
            INFILE=INFILE(1:LASTCHAR(INFILE))//'.atm'
          end if
        end if
        if (.not. FILEEXIST(INFILE)) then
          call ERROR('site: cannot find file',INFILE,0)
        end if
C
C output file has same root as input file, and a 'sit' extension
C
        NDOT=index(INFILE,'.')
        if (NDOT .lt. 1) then
          OUTFILE=INFILE(1:LASTCHAR(INFILE))
          OUTFILE(LASTCHAR(INFILE)+1:)='.sit'
          PSSFILE=INFILE(1:LASTCHAR(INFILE))
          PSSFILE(LASTCHAR(INFILE)+1:)='.pss'
        else
          OUTFILE=INFILE(1:NDOT)//'sit'
          PSSFILE=INFILE(1:NDOT)//'pss'
        end if
C
C ------------------------------------------------------------------------
C
C Read in the PDB format data
C
        call RDPDB(INFILE,ATM,RESNUM,RESNAM,CHNNAM,COORDS,OCCUP,BVAL,
     -             LENRES,NATS,NRES,NHET,HETCOORDS,
     -             HETNUM,HETNAM,HETATM,VERBOSE)
        if (NRES .eq. 0) then
          call ERROR('site: no residues in file !',INFILE,0)
        end if
        if (NHET .eq. 0) then
          call ERROR('site: no ligands in file !',INFILE,0)
        end if
C
C ------------------------------------------------------------------------
C
        if (IOUT .ne. STDOUT) then
          open (file=OUTFILE,unit=IOUT,status='UNKNOWN',form='FORMATTED',
     +          err=5)
        end if
C
        write (IOUT,'(''REMARK produced by site (c) jpo, Pfizer Ltd, 1996, version '',A,'' ('',A,
     +         '')'')') VERSION(),LIBVERSION()
        write (IOUT,'(''REMARK parameterized for '',I4,'' residues, '',I3
     +            ,'' atoms per residue, and '',
     +             I4,'' hetatoms'')') MAXRES, MAXATS, MAXHET
        write (IOUT,'(''REMARK coordinate data taken from file '',A)')
     +             INFILE(1:LASTCHAR(INFILE))
        write (IOUT,'(''REMARK coordinate cutoff  = '',F5.2)') DISTVAL
        write (IOUT,'(''REMARK number of atoms    = '',I5)') NATS
        write (IOUT,'(''REMARK number of residues = '',I5)') NRES
        write (IOUT,'(''REMARK number of hetatoms = '',I5)') NHET
        write (IOUT,'(''REMARK'')')
C
C --------------------------------------------------------------------------
C
        N=0
        do I=1,NRES
          YES=.false.
          do K=1,NHET
            if (NEIGHBOUR(COORDS(1,1,I),LENRES(I),HETCOORDS(1,K),DISTVAL2)) then
              YES=.true.
              SITERES(I)=.true.
            end if
          end do
          if (YES) then
            do J=1,LENRES(I)
              N=N+1
              write (IOUT,'(''ATOM  '',I5,1X,A4,1X,A3,1X,A,A5,3X,3F8.3)',err=903)
     +            N,ATM(J,I),RESNAM(I),CHNNAM(I),RESNUM(I),(COORDS(K,J,I),K=1,3)
            end do
          end if
        end do
        write (IOUT,'(''TER'')',err=903) 
C
C look for COVALENT attachments
C
        if (DOCOV) then
          do I=1,NRES
            do K=1,NHET
              if (COV(COORDS(1,1,I),LENRES(I),HETCOORDS(1,K),DISTVAL2,
     +            RESNAM(I),RESNUM(I),CHNNAM(I),ATM(1,I))) continue
            end do
          end do
        end if
C
C write out HETATM itself (as ATOM record (for rasmol)
C
        if (.not. MUTUAL) then
          do I=1,NHET
            N=N+1
            write (IOUT,'(''HETATM'',I5,1X,A4,1X,A3,2X,A5,3X,3F8.3)',err=903)
     +            N,HETATM(I),HETNAM(I),HETNUM(I),(HETCOORDS(K,I),K=1,3)
          end do
        else
          do I=1,NHET
            do J=1,NRES
              if (NEIGHBOURM(COORDS(1,1,J),LENRES(J),HETCOORDS(1,I),DISTVAL2)) then
                N=N+1
                write (IOUT,'(''HETATM'',I5,1X,A4,1X,A3,2X,A5,3X,3F8.3)',err=903)
     +                 N,HETATM(I),HETNAM(I),HETNUM(I),(HETCOORDS(K,I),K=1,3)
                goto 10
              end if
            end do
10        end do
        end if
C
        if (MARCEL) then
          do I=1,NHET
            if (HETNAM(I) .eq. ' ZN') then
              N=N+1
              write (IOUT,'(''MARCEL'',I5,1X,A4,1X,A3,2X,A5,3X,3F8.3)',err=903)
     +               N,HETATM(I),HETNAM(I),HETNUM(I),(HETCOORDS(K,I),K=1,3)
              do J=1,NHET
                if (I .ne. J) then
                  if (HETNAM(J) .ne. 'HOH') then
                    if (dist2(HETCOORDS(1,J),HETCOORDS(1,I)) .lt. distval2) then
                      N=N+1
                      write (IOUT,'(''MARCEL'',I5,1X,A4,1X,A3,2X,A5,3X,3F8.3)',err=903)
     +                       N,HETATM(J),HETNAM(J),HETNUM(J),(HETCOORDS(K,J),K=1,3)
                    end if
                  end if
                end if
              end do
            end if
          end do
        end if
C
        write (IOUT,'(''TER'')',err=903) 
C
C new section for sequence
C
        write (IOUT,'(''REMARK'')')
        NSITE=0
        do I=1,NRES
          if (SITERES(I)) then
            NSITE=NSITE+1
            NINDEX(NSITE)=I
          end if 
        end do
C
C this line could get quite long, 512 seems a sensible limit
C
          write (IOUT,'(''REMARK SITE INDEX      distance '',F6.2,2x,512(I4,1X))') DISTVAL,(NINDEX(I),I=1,NSITE)
        write (IOUT,'(''REMARK'')')
C
        if (SEQUENCE) then
          write (IOUT,'(''REMARK SITE SEQUENCE   distance '',F6.2)') DISTVAL
          if (NRES .gt. 0) then
            do I=1,MAXRES
              SEQ(I)=' '
            end do
            call THRONE(RESNAM,SEQ,NRES)
            do I=1,NRES 
              SEQ(I)=TOLOWER(SEQ(I))
              if (SITERES(I)) then
                SEQ(I)=TOUPPER(SEQ(I)) 
              end if
            end do
            write (IOUT,'(''REMARK  '',50A)') (SEQ(I),I=1,NRES)
            write (IOUT,'(''REMARK  '')')
          end if
        end if
C
        write (IOUT,'(''END'')',err=903) 
C
        if (IOUT .ne. STDOUT) then
          close (unit=IOUT)
        end if
C
C cheap hack for site, do not regularly use...
C based on .psa output file format (column headers mean nothing !!)
C
        if (DAVE) then
          open (file=PSSFILE,unit=13,status='UNKNOWN',form='FORMATTED',err=6)
          write (13,'(''# probe radius       : null'')')
          write (13,'(''# integration step   : null'')')
          write (13,'(''# water included     : null'')')
          write (13,'(''# hetatom included   : null'')')
          write (13,'(''# accessibility type : null'')')
          write (13,'(''# '')')
          write (13,'(''# N.B. Accessibility values are meaningless in this file (created by site)'')')
          write (13,'(''# number of residues :       0'')')
          write (13,'(''# vdw data file      : null'')')
          write (13,'(''#'')')
          write (13,'(''#       Res   Res   All atoms   Non P side  Polar Side  Total Side  Main Chain'')')
          write (13,'(''#       Num  type    Sum  Per.   Sum  Per.   Sum  Per.   Sum  Per.   Sum  Per.'')')
          do I=1,NRES
            VAL(I)=CLOSEST(COORDS(1,1,I),LENRES(I),HETCOORDS,NHET)
            write (13,'(''ACCESS '',A5,2X,A3,''    0.00  0.0   0.00  0.0   0.00  0.0   0.00'',
     +                       F5.1,''   0.00  0.0'')') RESNUM(I),RESNAM(I),VAL(I)
          end do
        end if
C
      end do
C
C --------------------------------------------------------------------------
C
      call EXIT(0)
C
C ---------------------------------------------------------------------------
C
C Catch bad distance value, etc.
C
5     call ERROR('site: error opening file',OUTFILE,1)
6     call ERROR('site: error opening file',PSSFILE,1)
903   call ERROR('site: error writing output !',' ',1)
904   call ERROR('site: error parsing options',OPTIONS,1)
C
      end 
