      program sstruc
*
*     copyright by David Keith Smith, 1989
*
*     global variables
*
      INCLUDE 'sstruc.par'
*
      character aacod1*(abetsz), aacod3*(3*abetsz), atname*(4*mnchna-4),
     ~          ssbond(nstruc,maxres)*1, sssumm(maxres)*1,
     ~          summpl(maxres)*1, seqbcd(maxres)*6, 
     ~          autsum(maxres)*1, ssord*21, brksym(maxres)*1,
     ~          headln*132, altcod(maxres)*1, aastd(maxres)*1,
     ~          atpres(3,maxres)*1, thmprs(maxres)*1, hetcod*(nhet*3),
     ~          outsty*1, dsdef(maxres)*1, dudcod*105, modcod*(nmod*3),
     ~          stdmod*(nmod+1), nstdmd*(nmod+1), modpl*(nmod)
      integer seqcod(maxres), seqlen, hbond(maxbnd,maxres),
     ~        chnsz(maxchn), chnct, bridpt(2,maxres), hedlen,
     ~        dspart(maxres), chnend(maxchn), oois(2,maxres)
      real mcaaat(ncoord,mnchna,maxres), hbonde(maxbnd,maxres),
     ~     scaaat(ncoord,sdchna,maxres), mcangs(nmnang,maxres),
     ~     scangs(nsdang,maxres), cadsts(11,maxres), dsdsts(3,maxres)
      logical atomin(mnchna,maxres), scatin(sdchna,maxres), caonly, 
     ~        readok, astruc
c
c     local variables
c
      integer finish, opnera, opnerb, opnerc, fiend, ok, endnfi
      character*1 indirn
      parameter (finish=2, opnera=1, opnerb=3, fiend=4, ok=0,
     ~           indirn='@', opnerc = 5, endnfi = 6
     ~           )
      logical finuse
      integer fistat, namlen, Iocode, I, Iostat, filety,
     ~        corepl, corend, logpl, dirpl, nodepl, outlen
      character*(fnamln) finam, brknam, outnam
*
      data aacod1(1:29)  /'ABCDEFGHIXKLMNXPQRSTXVWXYZX--'/
      data aacod3( 1:39) /'ALAASXCYSASPGLUPHEGLYHISILEXXXLYSLEUMET'/,
     ~     aacod3(40:78) /'ASNXXXPROGLNARGSERTHRXXXVALTRPXXXTYRGLX'/,
     ~     aacod3(79:87) /'UNKPCAINI'/
      data hetcod(1:42)  /'AIBPHLSECALMMPRFRDPYRLYMGLMPPHPGLOLETPHABA'/
      data modcod(1:39)  /'ACEANIBZOCBZCLTFORMSUMYRNH2PHORHATFATOS'/
      data dudcod( 1:39) /'  A  C  G  T  U1MA5MCOMC1MG2MGM2G7MGOMG'/,
     ~     dudcod(40:78) /' YG  I +UH2U5MUPSU +C +AGALAGLGCUASGMAN'/
     ~     dudcod(79:105) /'GLCCEGG4SAGSNAGGLSNGSNAMEXC'/
      data stdmod /'ABCDEFGHIJKLMZ'/
      data nstdmd /'abcdefghijklmz'/
      data modpl  /'+0++++++0++++'/
      data ssord /'HEheBbGgIiJKLMNOPTtS '/
      data atname /' N   CA  C   O  '/
      data atomin /allatm * .false./, mcaaat /allcrd * 0/
*
      data fistat/0/, finuse/.false./
*
*     input file control loop
*
c      Write(*,*)'Secondary structure calculation program - ',
c     ~          'copyright by David Keith Smith, 1989'
c      Write(*,*)
c      Write(*,*)'Do you require Standard or Full output (<S> or F)'
c      Read(*,'(A1)')outsty
      if (iargc() .ne. 2) then
	write(*,*)'usage: sstruc <s/f> <',indirn,'file.list/coord.file>'
	call exit(1)
      end if
      call getarg(1,outsty)
      If (outsty.eq.'F'.or.outsty.eq.'f') then
        outsty = 'F'
      else if (outsty.eq.'S'.or.outsty.eq.'s') then
        outsty = 'S'
      else
	write(*,*)'usage: sstruc <s/f> <',indirn,'file.list/coord.file>'
	call exit(1)
      end if
c100   continue
      fistat = ok
*
*     get the next brk file or file of names from the user
*     finish is set when they've had enough
*
c      write(*,*)'Enter the name of the next file to be processed.'
c      write(*,*)'  Just a file name for a Brookhaven format file'
c      write(*,'(a,a1,a,a)')'   ',Indirn,'file name for a file holding '
c     ~            ,'names of Brookhaven format files.'
c      write(*,*)'  If you''ve had enough a blank line let''s',
c     ~          ' you escape!'
c      read(*,'(A)')finam
      call getarg(2,finam)
      if ((Indexa(finam,'.').eq.0).and.(Indexa(finam,' ').eq.0)) then
	write(*,*) 'usage: sstruc <s/f> <',indirn,'file.list/coord.file>'
	call exit(1)
      end if
      Namlen = Indexa(finam,space) - 1
      If (Namlen .le. 0) then
        Fistat = Finish
      else
        If (Finam(1:1) .eq. indirn) then
          Finuse = .true.
          do 180 I = 1,Namlen-1
            Finam(i:i) = Finam(I+1:I+1)
  180     continue
          Finam(Namlen:Namlen) = space
          Open(Unit=namfi,File=finam,status='old',Iostat=Iocode)
          If (iocode .ne. 0) fistat = opnera
        end if
      end if
*
*     get the brookhaven file name into the appropriate place
*     either from the file of names or the input name
*
  200 continue
      if (fistat .eq. ok) then
        if (finuse) then
          read(Namfi,'(A)',end=210,err=220) Brknam
          Namlen = Indexa(brknam,space) - 1
          if (namlen .le. 0) then
c           ignore empty lines (Andrej)
            fistat = -1
          end if
        else
          brknam = finam
        end if
      end if
*
*     open the brookhaven file if everything ok
*
      If (fistat .eq. ok) then
cjpo       write(*,*)brknam(1:namlen)
        open (Unit=brkfi, file=brknam, status='old', iostat=iocode)
        if (iocode .ne. ok) fistat = opnerb
      end if
      If (fistat .eq. ok) then
        nodepl = Indexa(brknam,'::')
        if (nodepl .ne. 0) then
          logpl = nodepl + 1 + Indexa(brknam(nodepl+2:),':')
        else
          logpl = Indexa(brknam,':')
        end if
        dirpl = Indexa(brknam,']')
        corepl = max(logpl,dirpl) + 1
        corend = corepl - 1 + Indexa(brknam(corepl:),'.') - 1
        if (corend .eq. corepl - 2) corend = namlen
        if (corend .lt. corepl) then
          fistat = opnerc
        else
          outnam = brknam(corepl:corend) // '.sst'
          outlen = corend - corepl + 5
          open(unit=outfi,file=outnam,status='unknown',iostat=iocode)
c     ~         recl=133)
          If (iocode .ne. 0) fistat = opnerc
        end if
      end if
*
*     if survived this far have a file to process otherwise
*     provide error messages
*
      If (fistat .eq. ok) then
*       **Process file
        open(unit = errfi, status = 'SCRATCH')
        readok = .true.
        caonly = .true.
        astruc = .false.
        Call RdBrk(mcaaat, atname, atomin, aacod3, hetcod, seqcod,
     ~             seqbcd,  scaaat, scatin, altcod, aastd, atpres,
     ~             thmprs, caonly, astruc, headln, hedlen, readok,
     ~             dudcod, modcod, stdmod, nstdmd, modpl, seqlen)
        If (readok .and. .not. caonly) then
*           ** look for breaks accross the peptide bonds
          Call Chnbrk(mcaaat, atomin, seqbcd, brksym, chnsz, chnct,
     ~                chnend, caonly, seqlen)
*           ** determine the calpha distances i+2 to i+11
          if (outsty .eq. 'F')
     ~      Call mkcadt(mcaaat, atomin, cadsts, seqbcd, chnsz, chnct,
     ~                  seqlen)
*           ** create hydrogen atoms on the main chain nitrogens
          Call CreatH(Mcaaat, atomin, chnsz, chnct, seqlen)
*           ** determine all hydrogen bonds
          Call MkHBnd(Mcaaat, atomin, Hbond, Hbonde, seqcod, chnend, 
     ~                seqlen)
*           ** calculate OOI numbers
          Call Ooi(atomin, mcaaat, seqbcd, oois, seqlen)
*           ** find main chain torsion angles
          Call MkAngl(Mcaaat, mcangs, atomin, chnsz, chnct, caonly,
     ~                seqlen)
*           ** find sidechain chi angles
          if (outsty .eq. 'F')
     ~      call Mksang(mcaaat, atomin, scaaat, scangs, scatin, seqcod,
     ~                  aacod3, hetcod, seqlen)
*           ** determine turns, bridges and sheets
          Call MkTB(Hbond, ssbond, mcangs, bridpt, chnend, seqlen)
*           ** determine bend residues
          Call Mkbend(mcangs, ssbond, seqlen)
*           ** make Kabsch and sander type 2ndry structure summary
          Call MkSumm(ssbond, sssumm, summpl, seqlen)
*           ** make the authors atructure
          Call MkAuth(seqbcd, autsum, ssord, astruc, seqlen)
*           ** find all disulphide bonds
          if (outsty .eq. 'F')
     ~      Call disulf(seqbcd, dsdsts, scangs, scaaat, scatin, atomin,
     ~                  mcaaat, seqcod, dsdef, dspart, astruc, seqlen)
*           ** produce the output file
          Call Result(hbonde, hbond, aacod3, aacod1, seqcod, seqbcd, 
     ~                brksym, ssbond, bridpt, sssumm, summpl, autsum, 
     ~                mcangs, altcod, aastd, atpres, thmprs, scangs,
     ~                cadsts, brknam, headln, hedlen, outsty, dsdsts,
     ~                dsdef, dspart, hetcod, oois, seqlen)
          close(unit=outfi)
        else
          close(unit=outfi)
          if (readok .and. caonly .and. outsty .eq. 'F') then
            outnam = outnam(1:outlen-3) // 'sca'
            open(unit=outfi,file=outnam, status='new', iostat=iocode)
c     ~           recl=133)
            If (iocode .ne. ok) then
              Write(*,*)'Unable to open output file ',outnam(1:outlen)
              close(unit=errfi)
            else
              Call Chnbrk(mcaaat, atomin, seqbcd, brksym, chnsz, chnct,
     ~                    chnend, caonly, seqlen)
              Call mkcadt(mcaaat, atomin, cadsts, seqbcd, chnsz, chnct,
     ~                    seqlen)
              Call Ooi(atomin, mcaaat, seqbcd, oois, seqlen)
              Call MkAngl(Mcaaat, mcangs, atomin, chnsz, chnct, caonly,
     ~                    seqlen)
              Call MkAuth(seqbcd, autsum, ssord, astruc, seqlen)
              call carslt(aacod3, aacod1, seqcod, seqbcd, mcangs, 
     ~                    cadsts, brksym, altcod, aastd, brknam, headln,
     ~                    hedlen, autsum, hetcod, oois, seqlen)
            end if
          else
            close(unit=errfi)
          end if
          if (astruc) close(unit=authfi)
          If (caonly .and. readok) write(*,*)'C-alpha only file'
          if (.not. readok) write(*,*)'Error while reading file'
        end if
      else
        If (fistat .eq. opnera) then
          Write(*,*)'Unable to open the file of file names ',
     ~               finam(1:namlen)
          finuse = .false.
        else          
          If (fistat .eq. opnerb) then
            Write(*,*)'Unable to open the Brookhaven format file ',
     ~                 brknam(1:namlen)
          else
            If (fistat .eq. opnerc) then
              Write(*,*)'Unable to open output file ',outnam(1:outlen)
            end if
          end if
        end if
      end if
*
*     if they want more we'll cooperate
*
      If (fistat .ne. finish) then
        fistat = ok
        if (finuse) then
          go to 200
c        else
c          go to 100
        end if
      end if
      stop
*
210   write(*,*) 'END OF INPUT FILE.'
      stop
220   write(*,*) 'ERROR READING INPUT FILE.'
      stop

      END

      SUBROUTINE RDBRK( mcaaat, atname, atomin, aacod3, hetcod, seqcod,
     ~                  seqbcd, scaaat, scatin, altcod, aastd, atpres,
     ~                  thmprs, caonly, astruc, headln, hedlen, readok,
     ~                  dudcod, modcod, stdmod, nstdmd, modpl, seqlen)

      INCLUDE 'sstruc.par'

      integer seqlen, seqcod(maxres), hedlen
      Character aacod3*(abetsz*3), atname*(mnchna*4-4),seqbcd(maxres)*6,
     ~          headln*132, altcod(maxres)*1, aastd(maxres)*1,
     ~          atpres(3,maxres)*1, thmprs(maxres)*1, hetcod*(nhet*3),
     ~          modcod*(nmod*3), stdmod*(nmod+1), nstdmd*(nmod+1),
     ~          modpl*(nmod), dudcod*105
      Real mcaaat(ncoord,mnchna,maxres), scaaat(ncoord,sdchna,maxres)
      Logical Atomin(mnchna,maxres), scatin(mnchna,maxres), Caonly,
     ~        astruc, readok
      integer aaconv
*
*     local variables
*
      integer aapln
      character allspc*(16)
      parameter (aapln = 13, allspc = '                ')
      real acoord(ncoord), oldocc, occup, therm, oldthm
      logical altset
      integer nrecs, aathln, rderr, altype, atcnt, nxaa,
     ~        nxrec, attype, I, j, aacnt, skipaa, wkpl,
     ~        clslen, cmplen, soulen, sctype, altpos(sideaa), respl,
     ~        scatct, thmcnt, aacode, nscats(sideaa), hedend, sqrsct
      character aain(maxres)*3, atmnam*4, resnam*3, chid*1, altloc*1,
     ~          chnid(maxchn)*1, brkrec*80, oldchn*1, seqnum*6,
     ~          innum*6, keywd*(keylen), oldatm*4, setloc*1,
     ~          class*40, compnd*50, source*50, brcode*4, cbet*4,
     ~          scname(sideaa)*16, altnam(3)*16, contch*1, incode*4,
     ~          wkstr*50, inseqr*150, setatm*4
      integer strlen, resfnd
      data scname(1)  /allspc/, scname(2) /allspc/
      data scname(3)  /' SG             '/
      data scname(4)  /' CG  OD1 OD2    '/
      data scname(5)  /' CG  CD  OE1 OE2'/
      data scname(6)  /' CG  CD1 CD2    '/, scname(7) /allspc/
      data scname(8)  /' CG  ND1 CD2    '/
      data scname(9)  /' CG1 CD1        '/, scname(10) /allspc/
      data scname(11) /' CG  CD  CE  NZ '/
      data scname(12) /' CG  CD1 CD2    '/
      data scname(13) /' CG  SD  CE     '/
      data scname(14) /' CG  OD1 ND2    '/, scname(15) /allspc/
      data scname(16) /' CG             '/
      data scname(17) /' CG  CD  OE1 NE2'/
      data scname(18) /' CG  CD  NE  CZ '/
      data scname(19) /' OG             '/
      data scname(20) /' OG1 CG2        '/, scname(21) /allspc/
      data scname(22) /' CG1 CG2        '/
      data scname(23) /' CG  CD1 CD2    '/, scname(24) /allspc/
      data scname(25) /' CG  CD1 CD2    '/
      data scname(26) /allspc/, scname(27) /allspc/
      data scname(28) /' CG  CD  OE     '/
      data scname(29) /' CG  CD  CE  NZ '/
      data scname(30) /' CB1 CB2        '/
      data scname(31) /' CG  CD1 CD2    '/
      data scname(32) /'SEG  OD1 OD2    '/
      data scname(33) /' CM             '/
      data scname(34) /' SG             '/
      data scname(35) /' CG  CD1 CD2    '/
      data scname(36) /'                '/
      data scname(37) /' CG  CD  CE  NZ '/
      data scname(38) /' CM             '/
      data scname(39) /' CG  CD1 CD2    '/
      data scname(40) /'                '/
      data scname(41) /' CG  CD1 CD2    '/
      data scname(42) /' CG  CD1 CD2    '/
      data scname(43) /' CG             '/
      data altpos / 7*0, 1, 5*0, 2, 2*0, 3, 12*0, 14*0/
      data altnam(1)  /' CG  AD1 AD2    '/
      data altnam(2)  /' CG  AD1 AD2    '/
      data altnam(3)  /' CG  CD  AE1 AE2'/
      data nscats / 1, 4, 2, 4, 5, 7, 0, 6, 4, -1, 5, 4, 4, 4, -1,
     ~              3, 5, 7, 2, 3, -1, 3, 10, -1, 8, 5, -1, 4, 12,
     ~              2, 7, 4, 2, 2, 7, 8, 6, 1, 10, 3, 5, 9, 2/
      data cbet /' CB '/
c      save scname, altnam, altpos, nscats, cbet

*
*     Routine to read the brookhaven data file and extract the sequence
*     and the atom names and coordinates. Checking is also done for
*     residue compatibility between sequence and atom. C-alpha only
*     files are flagged. The sequence residues are expected together,
*     then the secondary structure records (sheets together), and then
*     the atom records which are expected in residue order.
*
      cmplen = 0             ! Andrej
      soulen = 0             ! Andrej
      clslen = 0             ! Andrej
      seqlen = 0
      hedlen = 0
      inseqr = space
      sqrsct = 0
      brcode = '    '
      astruc = .false.
  100 continue
      read(brkfi,'(A)',end=3000) brkrec
      keywd = brkrec(1:keylen)
      if (keywd .eq. seqkey) go to 200
      if (keywd .eq. hedkey) go to 500
      if (keywd .eq. cmpkey) go to 600
      if (keywd .eq. soukey) go to 700
      if (keywd .eq. atmkey) go to 900
      if (keywd .eq. htakey) go to 900
      if (keywd .eq. modkey) then
        readok = .false.
        seqlen = 0
        go to 3000
      end if
      if (keywd .eq. hlxkey .or. keywd .eq. shtkey .or.
     ~    keywd .eq. trnkey .or. keywd .eq. dsfkey) then
        if (.not. astruc) then
          open(unit = authfi, status = 'SCRATCH')
          astruc = .true.
        end if
        write(authfi,'(a)') brkrec
      end if
      go to 100

  200 continue
      read (brkrec,220) (aain(i),i = 1, aapln)
  220 format(18x,13(1x,a3))
      do 250, i = 1, aapln
        if (aain(i) .ne. '   ') then
          if (resfnd(inseqr,aain(i)) .eq. 0) then
            sqrsct = sqrsct + 1
            inseqr((sqrsct-1)*3+1:sqrsct*3) = aain(i)
          end if
        end if
  250 continue
      go to 100
*
*     set up header records
*
  500 continue
      read(brkrec,520) contch, wkstr, incode
  520 format(9x,a1,a40,12x,a4)
      if (contch .eq. space) then
        brcode = incode
        class = wkstr
        clslen = strlen(class)
      end if
      go to 100
  600 continue
      read(brkrec,620) contch, wkstr
  620 format(9x,a1,a50)
      if (contch .eq. space) then
        compnd = wkstr
        cmplen = strlen(compnd)
      end if
      go to 100
  700 continue
      read(brkrec,720) contch, wkstr
  720 format(9x,a1,a50)
      if (contch .eq. space) then
        source = wkstr
        soulen = strlen(source)
      end if
      go to 100
*
*     set up header if any exists
*
  900 continue
      headln(1:6) = brcode // '  '
      hedlen = 6
      if (clslen .gt. 0) then
        headln(7:8+clslen) = class(1:clslen) // '  '
        hedlen = hedlen + clslen + 2
      end if
      if (cmplen .gt. 0) then
        hedend = hedlen + cmplen + 2
        if (hedend .gt. 132) hedend = 132
        headln(hedlen+1:hedend) = compnd(1:cmplen) // '  '
        hedlen = hedend
      end if
      if (soulen .gt. 0) then
        hedend = hedlen + soulen+ 2
        if (hedend .gt. 132) hedend = 132
        headln(hedlen+1:hedend)  = source(1:soulen)
        hedlen = hedend
      end if
      if (hedlen .gt. 132) hedlen = 132
*
*     look for next atom records to process
*
 1000 continue
      if (brkrec(1:keylen) .ne. atmkey .and.
     ~      brkrec(1:keylen) .ne. htakey) then
        read(brkfi,'(A)',end=3000) brkrec
        go to 1000
      end if
*
*     have first atom record for an aa
*     for each aa get the seqnum and resnam and check against the
*     read in sequence. If ok initialise to no atoms present and
*     get the coords
*
      aacnt = 0
      aastd(1) = space
      read(brkrec,1080)oldchn
 1080 format(21x,a1)
 
 1100 continue
      keywd = brkrec(1:keylen)
      aacode = 0
      altset = .false.
      setatm = '    '
      read(brkrec,1120) oldatm, setloc, resnam, seqnum, oldocc, oldthm
 1120 format(12x,a4,a1,a3,1x,a6,27x,2f6.2)
      if (sqrsct .ne. 0) then
        if (resfnd(inseqr(1:sqrsct*3),resnam) .eq. 0) go to 1400
      end if
      if (resfnd(dudcod,resnam) .ne. 0) go to 1400
      respl = resfnd(modcod,resnam)
      if (respl .ne. 0) then
        respl = (respl - 1) / 3 + 1
        if (Modpl(respl:respl) .eq. '+') then
          aastd(aacnt+1) = stdmod(respl:respl)
        else
          if (aastd(aacnt) .eq. 'S') then
            aastd(aacnt) = stdmod(respl:respl)
          else
            if (aastd(aacnt) .eq. 'O') then
              aastd(aacnt) = nstdmd(respl:respl)
            else
              if (llt(aastd(aacnt),'a')) then
                aastd(aacnt) = stdmod(nmod+1:nmod+1)
              else
                aastd(aacnt) = nstdmd(nmod+1:nmod+1)
              end if
            end if
          end if
        end if
        go to 1400
      end if

      aacode = AAconv(resnam, aacod3, hetcod)

      if (aacode .le. 0) go to 1400
      aacnt = aacnt + 1
      if (aastd(aacnt) .eq. space) then
        aastd(aacnt) = 'S'
        if (aacode .gt. stdaa) aastd(aacnt) = 'O'
      else
        if (aacode .gt. stdaa) then
          wkpl = Indexa(stdmod,aastd(aacnt))
          aastd(aacnt) = nstdmd(wkpl:wkpl)
        end if
      end if
      seqcod(aacnt) = aacode
      seqbcd(aacnt) = seqnum
      atcnt = 0
      scatct = 0
      thmcnt = 0
      thmprs(aacnt) = space
      do 1200 i = 1, mnchna
        atomin(i,aacnt) = .false.
 1200 continue
      do 1220 i = 1, sdchna
        scatin(i,aacnt) = .false.
 1220 continue
      do 1240 i = 1, 3
        atpres(i,aacnt) = space
 1240 continue
*
*     process cordinates find atom type and if main chain set in coords
*
 1300 continue
      read(brkrec,1320)atmnam,altloc,(acoord(i),i=1,3), occup, therm
 1320 format(12x,a4,a1,13x,3f8.3,2f6.2)
      if (atmnam .eq. ' OXT' .or. atmnam .eq. ' NXT' .or.
     ~    atmnam(2:2) .eq. 'H' .or. atmnam(2:2) .eq. 'D')
     ~  go to 1400
      attype = Indexa(atname,atmnam)
      if (altloc .ne. space) then
        if (setatm .eq. '    ') then
          setatm = atmnam
          setloc = altloc
          oldocc = occup
          oldthm = therm
        end if
        if (.not. altset) then
          if (setatm .eq. atmnam) then
            if (occup .gt. oldocc) then
              setloc = altloc
              oldocc = occup
              if (oldthm .ne. 0) thmcnt = thmcnt - 1
              oldthm = therm
              if (attype .ne. 0) then
                atcnt = atcnt - 1
              else
                scatct = scatct - 1
              end if
            end if
          else
            altset = .true.
          end if
        end if
      end if
      if (therm .ne. 0.0 .and. (altloc .eq. setloc .or.
     ~          altloc .eq. space)) thmcnt = thmcnt + 1
      if (attype .ne. 0 .and. (altloc .eq. setloc .or.
     ~    altloc .eq. space)) then
        attype = attype / 4 + 1
        do 1350 i = 1, ncoord
          mcaaat(i,attype,aacnt) = acoord(i)
 1350   continue
        atomin(attype,aacnt) = .true.
        if (attype .ne. calph) caonly = .false.
        atcnt = atcnt + 1
      end if
      if (attype .eq. 0 .and. (altloc .eq. setloc .or.
     ~    altloc .eq. space)) then
        scatct = scatct + 1
        if (aacode .le. sideaa) then
          if (atmnam .eq. cbet) then
            sctype = 1
          else
            sctype = Indexa(scname(aacode),atmnam)
            if (sctype .eq. 0 .and. altpos(aacode) .ne. 0)
     ~          sctype = Indexa(altnam(altpos(aacode)),atmnam)
            if (sctype .ne. 0) sctype = sctype / 4 + 2
          end if
          if (sctype .ne. 0) then     
            scatin(sctype,aacnt) = .true.
            do 1375 i = 1, ncoord
              scaaat(i,sctype,aacnt) = acoord(i)
 1375       continue
          end if
        end if
      end if
*
*     search for the next atom record and treat as appropriate
*     process coords only if same aa 
*     if new aa start aa process again after setting the count fields
*
 1400 continue
      read(brkfi,'(a)',end=2000) brkrec
      If (brkrec(1:keylen) .ne. endkey) then
        If (brkrec(1:keylen) .eq. atmkey .or.
     ~        brkrec(1:keylen) .eq. htakey) then
          read(brkrec,'(21x,a6)') innum
          if (innum .eq. seqnum .and. keywd .eq. brkrec(1:keylen)) then
            if (aacode .le. 0) go to 1400
            go to 1300
          else
            if (aacode .gt. 0)
     ~        call cntset(atpres, thmprs, altcod, nscats, aacode, 
     ~                    aacnt, atcnt, scatct, thmcnt, setloc)
            If (aacnt .ge. maxres) then
              Write(errfi,*)'Too many amino acids - input terminated'
              goto 2100
            else
              if (aacode .gt. 0) aastd(aacnt + 1) = space
              go to 1100
            end if
          end if
        end if
        go to 1400
      end if
 2000 continue
      if (aacode .gt. 0)
     ~  call cntset(atpres, thmprs, altcod, nscats, aacode, 
     ~              aacnt, atcnt, scatct, thmcnt, setloc)
 2100 continue
      Seqlen = aacnt
 3000 continue
      if (seqlen .eq. 0) readok = .false.
      close(unit=brkfi)
      END

      Subroutine cntset(atpres, thmprs, altcod, nscats, aacode, 
     ~                  aacnt, atcnt, scatct, thmcnt, setloc)
*
*     perform end of amino acid flag setting
*
      INCLUDE 'sstruc.par'
      character atpres(3,maxres)*1, thmprs(maxres)*1, altcod(maxres)*1,
     ~          setloc*1
      integer nscats(sideaa), aacode, aacnt, scatct, thmcnt, atcnt
      integer totats
      character atmchk*1

      altcod(aacnt) = setloc
      atpres(1,aacnt) = space
      atpres(2,aacnt) = space
      atpres(3,aacnt) = space
      thmprs(aacnt) = space
      totats = atcnt + scatct
      if (aacode .le. sideaa) then
        if (nscats(aacode) .ge. 0) then
          atpres(1,aacnt) = atmchk(mnchna+nscats(aacode)-1,totats)
          atpres(2,aacnt) = atmchk(mnchna-1,atcnt)
          atpres(3,aacnt) = atmchk(nscats(aacode),scatct)
          thmprs(aacnt) = atmchk(mnchna+nscats(aacode)-1,thmcnt)
        end if
      end if
      end

      character*1 function atmchk(Total,count)
*
*     set 'present' fields to all none or some as appropriate
*
      INCLUDE 'sstruc.par'
      integer total, count
      if (total .eq. 0) then
        atmchk = space
      else
        if (count .eq. 0) then
          atmchk = 'N'
        else
          if (count .ge. total) then
            atmchk = 'A'
          else
            atmchk = 'S'
          end if
        end if
      end if
      END

      integer function Strlen(string)
*
*     find out the real length of the string
*
      INCLUDE 'sstruc.par'
      character*(*) string

      integer nxch
      do 200, nxch = len(string), 1, -1
        if (string(nxch:nxch) .ne. space) goto 300
  200 continue
      nxch = 0
  300 continue
      strlen = nxch
      end
      
      integer function AAconv(aacode, aacod3, hetcod)
*
*     find the numerical position of an amino acid code
*
      INCLUDE 'sstruc.par'
      character aacode*3, aacod3*(3*abetsz), hetcod*(nhet*3)
      integer resfnd
      integer aaval
      aaval = resfnd(aacod3,aacode)
      if (aaval .eq. 0) then
        aaval = resfnd(hetcod,aacode)
        if (aaval .eq. 0) then
          aaval = unkcod
cjpo          write(errfi,530) aacode, ' unknown amino acid code'
  530     format(a3,a)
        else
          aaval = abetsz + aaval / 3 + 1
        end if
      else
        aaval = aaval / 3 + 1
      end if
      AAconv = aaval
      END

      integer function resfnd(soustr,questr)
*
*     locate query in source on 3 character boundaries
*
      character soustr*(*), questr*3
      integer respl, nxpl

      respl = Indexa(soustr,questr)
  100 continue
      If (mod(respl,3) .ne. 1 .and. respl .ne. 0) then
        nxpl = Indexa(soustr(respl+1:),questr)
        if (nxpl .ne. 0) then
          respl = respl + nxpl
        else
          respl = nxpl
        end if
        go to 100
      end if
      resfnd = respl
      End

      Subroutine Chnbrk(mcaaat, atomin, seqbcd, brksym, chnsz, chnct,
     ~                  chnend, caonly, seqlen)
*
*     search through looking for distant peptide bonds. if any over
*     the limit are found or if a c or n atom doesn't exist then a
*     chain break is deemed to occur
*
      INCLUDE 'sstruc.par'
      real mcaaat(ncoord,mnchna,maxres)
      logical atomin(mnchna,maxres), caonly
      integer chnsz(maxchn), chnct, seqlen, chnend(maxchn)
      character seqbcd(maxres)*6, brksym(maxres)*1
      Real atdist

      integer nxres, ninchn

      do 200 nxres = 1, seqlen
        brksym(nxres) = space
  200 continue
      ninchn = 1
      chnct = 1
      do 1000, nxres = 2, seqlen
        if (caonly) then
          if (atdist(mcaaat(1,calph,nxres-1),mcaaat(1,calph,nxres))
     ~        .gt. cadbnd) then
            call chnset(brksym, chnsz, chnct, chnend, ninchn, seqbcd, 
     ~                  nxres, seqlen)
          else
            ninchn = ninchn + 1
            if (seqbcd(nxres-1)(1:1) .ne. seqbcd(nxres)(1:1)) 
     ~        Write(errfi,500) nxres-1, seqbcd(nxres-1),
     ~                         nxres, seqbcd(nxres)
  500         format('chain letter change but no chain break between ',
     ~               i4,2x,a6,' and',i4,2x,a6)
          end if
        else
          If (atomin(carb,nxres-1) .and. atomin(nmain,nxres)) then
            if (atdist(mcaaat(1,carb,nxres-1),mcaaat(1,nmain,nxres))
     ~          .gt. pepbnd) then
              call chnset(brksym, chnsz, chnct, chnend, ninchn, seqbcd, 
     ~                    nxres, seqlen)
            else
              ninchn = ninchn + 1
              if (seqbcd(nxres-1)(1:1) .ne. seqbcd(nxres)(1:1)) 
     ~          Write(errfi,500) nxres-1, seqbcd(nxres-1),
     ~                         nxres, seqbcd(nxres)
            end if
          else
            call chnset(brksym, chnsz, chnct, chnend, ninchn, seqbcd, 
     ~                  nxres, seqlen)
          end if
        end if
 1000 continue
      call chnset(brksym, chnsz, chnct, chnend, ninchn, seqbcd,
     ~            seqlen+1, seqlen)
      END

      SUBROUTINE Chnset(brksym, chnsz, chnct, chnend, ninchn, seqbcd, 
     ~                  nxres, seqlen)
      INCLUDE 'sstruc.par'
      integer chnsz(maxchn), chnct, ninchn, seqlen, nxres,
     ~        chnend(maxchn)
      character seqbcd(maxres)*6, brksym(maxres)*1

      chnsz(chnct) = ninchn
      if (chnct .eq. 1) then
        chnend(chnct) = ninchn
      else
        chnend(chnct) = chnend(chnct-1) + ninchn
      end if
      ninchn = 1
      if (nxres .le. seqlen) then
        brksym(nxres-1) = brkch
        brksym(nxres) = brkch
        write(errfi,100) nxres-1, seqbcd(nxres-1), nxres, seqbcd(nxres)
  100   format('chain break between ',i4,'(',a6,') and ',i4,'(',a6,')')
        chnct = chnct + 1
      end if
      end

      Subroutine mkcadt(mcaaat, atomin, cadsts, seqbcd, chnsz, chnct,
     ~                  seqlen)
*
*     generate the calpha distances for I+1 to I+11. take note of nulls
*     accross brookhaven chain breaks but not accross disconections at 
*     this point
*
      INCLUDE 'sstruc.par'
      integer seqlen, chnct, chnsz(maxchn)
      real mcaaat(ncoord,mnchna,maxres), cadsts(11,maxres)
      logical atomin(mnchna,maxres)
      character seqbcd(maxres)*6

      integer nxres, nxdist
      real mxdist, adist
      parameter (mxdist = 99.9)
      real atdist

      do 1000, nxres = 1, seqlen
        do 500, nxdist = 1, 11
          if (nxres+nxdist .gt. seqlen .or.
     ~                          .not. atomin(calph,nxres)) then
            cadsts(nxdist,nxres) = -1
          else
            if (seqbcd(nxres)(1:1) .ne. seqbcd(nxres+nxdist)(1:1)
     ~             .or. .not. atomin(calph,nxres+nxdist)) then
              cadsts(nxdist,nxres) = -1
            else
              adist = atdist(mcaaat(1,calph,nxres),
     ~                       mcaaat(1,calph,nxres+nxdist))
              if (adist .gt. mxdist) adist = mxdist
              cadsts(nxdist,nxres) = adist
            end if
          end if
  500   continue
 1000 continue
      END

      Subroutine CreatH(mcaaat, atomin, chnsz, chnct, seqlen)
*
*     Procedure to create hydrogen atoms. These are deemed to be
*     one angstrom from the nitrogen atom and angled so that N-H
*     is parallel to C=O before. By taking the vector from O to C
*     the N-H bond is generated on the other side of C-N to the O.
*
      INCLUDE 'sstruc.par'

      integer chnsz(maxchn), chnct, seqlen
      real mcaaat(ncoord,mnchna,maxres)
      logical atomin(mnchna,maxres)
*
*     local variables
*
      integer nxres, i, chnst, nxchn
      Real atvec(ncoord), colen
*
      chnst = 0
      do 2000, nxchn = 1, chnct
        do 1900, nxres = chnst + 2, chnst + chnsz(nxchn)
          If (atomin(nmain,nxres) .and. atomin(carb,nxres-1) .and.
     ~        atomin(oxyg,nxres-1)) then
            Colen = 0.0
            do 300 i = 1,ncoord
              atvec(i) = mcaaat(i,carb,nxres-1) - mcaaat(i,oxyg,nxres-1)
              colen = colen + atvec(i) ** 2
  300       continue
            colen = sqrt(colen)
            do 500 i = 1, ncoord
              atvec(i) = atvec(i) / colen
  500       continue
            do 700 i = 1, ncoord
              mcaaat(i,nhyd,nxres) = mcaaat(i,nmain,nxres) + atvec(i)
  700       continue
            atomin(nhyd,nxres) = .true.
          else
            write(errfi,1100) nxres
 1100       format('hydrogen atom not generated for residue ',i4)
            atomin(nhyd,nxres) = .false.
          end if
 1900   continue
        chnst = chnst + chnsz(nxchn)
 2000 continue
      END

      Subroutine MkHBnd(Mcaaat, atomin, Hbond, Hbonde, seqcod, chnend,
     ~                  seqlen)
*
*     Procedure to discover the possible hydrogen bonds between two
*     amino acids. Each amino acid is allowed 4 bonds - 2 from the 
*     C=O and 2 from the N-H. Should it be that 3 or more bonds are
*     possible the most favourable are kept. A hydrogen
*     bond is deemed to exist if the Kabsch and Sander energy formula
*     gives a value less than the cutoff. The angle and distance
*     approach is implicitly built in to this method. The formula
*     corresponds to a maximum distance of 5.2 A for in line atoms
*     and a maximum angular deviation of 63 degrees. A check on the
*     maximum distance is used to abandon unnecessary calculations.
*     the formula is
*     E = q1*q2*(1/d(ON)+1/d(CH)-1/d(OH)-1/d(CN))*f
*     Residues must be separated by at least one residue to bond
*     Proline cannot be a donor of a hydrogen bond
*
      INCLUDE 'sstruc.par'

      integer Seqlen
      Real Mcaaat(ncoord,mnchna,maxres), Hbonde(maxbnd,maxres)
      integer Hbond(maxbnd,maxres), seqcod(maxres), chnend(maxchn)
      logical Atomin(mnchna,maxres)
      Real Atdist
      integer Aplace
*
*     local variables
*
      integer ksf, mndist
      real mxdist, maxeng, ksq1, ksq2, maxcad, englim
      parameter (mxdist = 5.2, maxeng = -0.5, mndist = 0.5,
     ~  englim = -9.9, ksq1 = 0.42, ksq2 = 0.2, ksf = 332, maxcad = 8.0)
      integer Nxres, Othres, cpl, npl, i, nbonds, fstchn, othchn
      logical copres, nhpres
      real don, doh, dch, dcn, energy, cadist

      do 200 nxres = 1, seqlen
        do 200 i = 1,maxbnd
          hbond(i,nxres) = 0
          hbonde(i,nxres) = 0.0
  200 continue
      nbonds = 0
      fstchn = 1
      do 2000 nxres = 1, seqlen
        if (nxres .gt. chnend(fstchn)) fstchn = fstchn + 1
        nhpres = .false.
        if (atomin(nmain,nxres) .and. atomin(nhyd,nxres) .and.
     ~      seqcod(nxres) .ne. procod) nhpres = .true.
        if (nhpres) then
          othchn = 1
          do 1900 othres = 1, seqlen
            if (othres .gt. chnend(othchn)) othchn = othchn + 1
            If ((abs(nxres - othres) .eq. 1 .and. fstchn .ne. othchn)
     ~           .or. abs(nxres - othres) .ge. 2) then
              If (atomin(calph,nxres) .and. atomin(calph,othres)) then
                cadist = atdist(mcaaat(1,calph,othres),
     ~                       mcaaat(1,calph,nxres))
                If (cadist .lt. maxcad) then
                  If (atomin(carb,othres) .and. atomin(oxyg,othres))then
                    don = atdist(mcaaat(1,oxyg,othres),
     ~                           mcaaat(1,nmain,nxres))
                    doh = atdist(mcaaat(1,oxyg,othres),
     ~                           mcaaat(1,nhyd,nxres))
                    dch = atdist(mcaaat(1,carb,othres),
     ~                           mcaaat(1,nhyd,nxres))
                    dcn = atdist(mcaaat(1,carb,othres),
     ~                           mcaaat(1,nmain,nxres))
                    If (don .eq. 0 .or. doh .eq. 0 .or.
     ~                  dch .eq. 0 .or. dcn .eq. 0) then
                      write(errfi,250)nxres, othres
  250                 format('coincident atoms in hydrogen bonding',
     ~                       ' donor ',I4,' acceptor ',i4)
                    else
                      energy = ksq1 * ksq2 * ksf * 
     ~                         (1/don + 1/dch - 1/doh - 1/dcn)
                      If (energy .lt. englim) then
                        write(errfi,300) nxres, othres, don, dch,
     ~                                   doh, dcn
  300                   format('atoms too close ',I4,2x,i4,2x,4f8.3)
                        energy = englim
                      end if
                      If (energy .lt. maxeng) call engset(hbonde,
     ~                      hbond, energy, nxres, othres, nbonds)
                    end if
                  end if
                end if
              end if
            end if
 1900     continue
        end if
 2000 continue
c      write(errfi,2100) 'number of hydrogen bonds is ',nbonds
c 2100 format(a,i5)
      END

      Subroutine Engset(hbonde, hbond, energy, donres, accres, bndct)
*
*     find a place to put this h-bond. discard third bonds if necessary
*
      Include 'sstruc.par'
      real hbonde(maxbnd,maxres), energy
      integer hbond(maxbnd,maxres), donres, accres, bndct

      integer acpl, donpl, rejptr
      integer engpl

      donpl = engpl(energy,hbonde(nst,donres))
      acpl = engpl(energy,hbonde(cst,accres))
      If (acpl .eq. 0 .or. donpl .eq. 0) then
cjpo       write(errfi,400) donres, accres, energy
cjpo 400 format('third (+) Hbond (N-C) ',I4,2x,i4,
cjpo    ~                          ' energy ',f6.2,' abandoned')
      else
        if (hbond(nst+1,donres) .ne. 0) then
cjpo          write(errfi,400) donres, hbond(nst+1,donres),
cjpo     ~                     hbonde(nst+1,donres)
          rejptr = hbond(nst+1,donres) 
          call rejbnd(donres,hbond(cst,rejptr),hbonde(cst,rejptr))
          bndct = bndct - 1
        end if
        if (hbond(cst+1,accres) .ne. 0) then
cjpo          write(errfi,400) hbond(cst+1,accres), accres, 
cjpo     ~                     hbonde(cst+1,accres)
          rejptr = hbond(cst+1,accres) 
          call rejbnd(accres,hbond(nst,rejptr),hbonde(nst,rejptr))
          bndct = bndct - 1
        end if
        If (acpl .eq. 1) then
          hbond(cst+1,accres) = hbond(cst,accres)
          hbonde(cst+1,accres) = hbonde(cst,accres)
        end if
        If (donpl .eq. 1) then
          hbond(nst+1,donres) = hbond(nst,donres)
          hbonde(nst+1,donres) = hbonde(nst,donres)
        end if
        hbond(cst+acpl-1,accres) = donres
        hbonde(cst+acpl-1,accres) = energy
        hbond(nst+donpl-1,donres) = accres
        hbonde(nst+donpl-1,donres) = energy
        bndct = bndct + 1
      end if
      END

      SUBROUTINE rejbnd(rejptr,bond,bonde)
      INCLUDE 'sstruc.par'
      integer rejptr, bond(2)
      real bonde(2)
      integer rejpl

      rejpl = 1
      if (bond(1).ne. rejptr) rejpl = rejpl + 1
      if (rejpl .eq. 1) then
        bond(1) = bond(2)
        bonde(1) = bonde(2)
      end if
      bond(2) = 0
      bonde(2) = 0
      end

      INTEGER FUNCTION Engpl(energy, bonde)
      INCLUDE 'sstruc.par'
      real energy, bonde(2)
      engpl = 0
      If (energy .lt. bonde(1)) then
        engpl = 1
      else
        If (energy .lt. bonde(2)) engpl = 2
      end if
      end
    
      REAL Function Atdist(coorda,coordb)
*
*     routine to determine the distance in space between the two
*     atoms represented by the coordinate arrays.
*
      INCLUDE 'sstruc.par'

      Real Coorda(ncoord),Coordb(ncoord)
*
      integer i
      Real Dist
      dist = 0.0
      do 200 i = 1,ncoord
        dist = dist + (coorda(i) - coordb(i)) ** 2
  200 continue
      atdist = sqrt(dist)
      END

      INTEGER Function Aplace(Bondpl)
*
*     find free place in bond table or return 0
*
      INCLUDE 'sstruc.par'
      integer bondpl(2), i, wkpl
      do 200 i = 1,2
        If (Bondpl(i) .eq. 0) then
          wkpl = i
          go to 250
        end if
  200 continue
      wkpl = 0
  250 continue
      aplace = wkpl
      END

      Subroutine Ooi(atomin, mcaaat, seqbcd, oois, seqlen)
*
*     routine to calculate Ooi numbers. these are done for n8 and n14.
*     all atoms are included as there is noway to handle i+/-2 over
*     missing densities. Numbers are for within brookhaven chain only.
*
      INCLUDE 'sstruc.par'
      logical atomin(mnchna,maxres)
      real mcaaat(ncoord,mnchna,maxres)
      character seqbcd(maxres)*6
      integer oois(2,maxres), seqlen

      integer nxaa, stchn, aapart, wkchn(maxchn), stpl, nxchn, nxpart,
     ~        numchn
      character chnch*1
      real wkdist
      real atdist

      stchn = 1
      chnch = seqbcd(1)(1:1)
      numchn = 0
      do 200, nxaa = 1, seqlen
        if (atomin(calph,nxaa)) then
          oois(1,nxaa) = 0
          oois(2,nxaa) = 0
        else
          oois(1,nxaa) = -1
          oois(2,nxaa) = -1
        end if
        if (seqbcd(nxaa)(1:1) .ne. chnch) then
          numchn = numchn + 1
          wkchn(numchn) = nxaa - stchn
          chnch = seqbcd(nxaa)(1:1)
          stchn = nxaa
        end if
  200 continue
      numchn = numchn + 1
      wkchn(numchn) = seqlen + 1 - stchn

      stpl = 0
      do 3000, nxchn = 1, numchn
        do 2000, nxaa = stpl + 1, stpl + wkchn(nxchn) - 1
          do 1000, nxpart = nxaa + 1, stpl + wkchn(nxchn)
            if (atomin(calph,nxaa) .and. atomin(calph,nxpart)) then
              wkdist = atdist(mcaaat(1,calph,nxaa),
     ~                        mcaaat(1,calph,nxpart))
              if (wkdist .lt. ooi2rd) then
                oois(ooi2,nxaa) = oois(ooi2,nxaa) + 1
                oois(ooi2,nxpart) = oois(ooi2,nxpart) + 1
                if (wkdist .lt. ooi1rd) then
                  oois(ooi1,nxaa) = oois(ooi1,nxaa) + 1
                  oois(ooi1,nxpart) = oois(ooi1,nxpart) + 1
                end if
              end if
            end if
 1000     continue
 2000   continue
        stpl = stpl + wkchn(nxchn)
 3000 continue
      end      

      Subroutine MkAngl(McAAat, McAngs, Atomin, chnsz, chnct, caonly,
     ~                  Seqlen)
*
*     Procedure to calculate the main chain dihedral angles
*     if all the atoms are present.
*
      INCLUDE 'sstruc.par'

      Real McAAat(ncoord,mnchna,maxres), McAngs(nmnang,maxres)
      Logical Atomin(mnchna,maxres), caonly
      integer Seqlen, chnsz(maxchn), chnct
      Real Dihed
*
*     local variables
*
      integer angrng(nmnang,3), angatm(nmnang,4), angoff(nmnang,4),
     ~        angchn(nmnang), I, j, Nxres, Nxset, chnst, nxchn, stang,
     ~        finang

      data ((angrng(I,j),j=1,3), i=1,nmnang)
     ~      /2, 0, phi, 1, -1, psi, 1, -1, omega, 2, -2, chiral,
     ~       3, -2, kappa, 2, 0, tco/
      data ((angatm(I,j),j=1,4), i=1,nmnang)
     ~     /carb, nmain, calph, carb, nmain, calph, carb, nmain,
     ~      calph, carb, nmain, calph, calph, calph, calph, calph,
     ~      calph, calph, calph, calph, carb, oxyg, carb, oxyg/
      data ((angoff(I,j),j=1,4), i=1,nmnang)
     ~     /-1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, 1, -1, 0, 1, 2,
     ~      -2, 0, 0, 2, 0, 0, -1, -1/
      data angchn / 2, 2, 2, 4, 5, 2/
c      SAVE angrng, angatm, angoff, angchn
*
      stang = 1
      finang = nmnang
      if (caonly) then
        stang = chiral
        finang = kappa
      end if
      do 4000, Nxset = stang, finang
        chnst = 0
        do 3000, nxchn = 1, chnct
          if (chnsz(nxchn) .lt. angchn(nxset)) then
            do 1000, nxres = chnst + 1, chnst + chnsz(nxchn)
              McAngs(angrng(nxset,3),Nxres) = Null
 1000       continue
          else
            do 2000, Nxres = chnst + angrng(nxset,1),
     ~                       chnst + chnsz(nxchn) + angrng(nxset,2)
              If (atomin(angatm(nxset,1), nxres+angoff(nxset,1)) .and.
     ~            atomin(angatm(nxset,2), nxres+angoff(nxset,2)) .and.
     ~            atomin(angatm(nxset,3), nxres+angoff(nxset,3)) .and.
     ~            atomin(angatm(nxset,4), nxres+angoff(nxset,4))) then
                McAngs(angrng(nxset,3),Nxres) = Dihed(nxset,
     ~            Mcaaat(1,angatm(nxset,1), nxres+angoff(nxset,1)),
     ~            Mcaaat(1,angatm(nxset,2), nxres+angoff(nxset,2)),
     ~            Mcaaat(1,angatm(nxset,3), nxres+angoff(nxset,3)),
     ~            Mcaaat(1,angatm(nxset,4), nxres+angoff(nxset,4)))
              else
                McAngs(angrng(nxset,3),Nxres) = Null
              end if
 2000       continue
            do 2400, nxres = chnst + 1, chnst + angrng(nxset,1) - 1
              McAngs(angrng(nxset,3),nxres) = Null
 2400       continue
            do 2600, nxres = chnst + chnsz(nxchn) + angrng(nxset,2) + 1,
     ~                       chnst + chnsz(nxchn)
              McAngs(angrng(nxset,3),nxres) = Null
 2600       continue
          end if
          chnst = chnst + chnsz(nxchn)
 3000   continue
 4000 continue

      END

      subroutine Mksang(mcaaat, atomin, scaaat, scangs, scatin, 
     ~                  seqcod, aacod3, hetcod, seqlen)
*
*     calculate chi angles from atom data. check for that alternates
*     are closest to c alpha
*
      INCLUDE 'sstruc.par'
      real mcaaat(ncoord,mnchna,maxres), scaaat(ncoord,sdchna,maxres),
     ~     scangs(nsdang,maxres)
      character aacod3*(abetsz*3), hetcod*(nhet*3)
      logical atomin(mnchna,maxres), scatin(sdchna,maxres)
      integer seqcod(maxres), seqlen

      integer natsdc(sideaa), nxang, i, j, nxchi, nxres, nchi, aacod,
     ~        extprc(sideaa), swatm, extatm, chipl, nswap, wkswap
      logical athere(maxchi+3)
      real relats(ncoord,maxchi+3), distmn, distal, ang1, ang2
      character outerr*80, wkres*3
      real atdist, dihed
      data natsdc / 0, 0, 1, 2, 3, 2, 0, 2, 2, 0, 4, 2, 3,
     ~              2, 0, 1, 3, 4, 1, 1, 0, 1, 2, 0, 2, 0, 0, 3, 4,
     ~              0, 2, 1, 1, 1, 2, 0, 4, 0, 2, 0, 2, 2, 1/
      data extprc / 0, 0, 0, 4, 4, 4, 0, 0, 0, 0, 0, 2, 0,
     ~              0, 0, 0, 0, 0, 0, 3, 0, 2, 0, 0, 4, 0, 0, 0, 0,
     ~              0, 4, 0, 0, 0, 4, 0, 0, 0, 4, 0, 2, 4, 0/
c      save natsdc, extprc

      nswap = 0
      outerr = space
      do 5000 nxres = 1, seqlen
        aacod = seqcod(nxres)
        nchi = 0
        nchi = natsdc(aacod)
        do 100, nxang = nchi + 1, nsdang
          scangs(nxang,nxres) = null
  100   continue
        if (nchi .gt. 0) then
          athere(1) = atomin(nmain,nxres)
          athere(2) = atomin(calph,nxres)
          do 200, i = 1, ncoord
            relats(i,1) = mcaaat(i,nmain,nxres)
            relats(i,2) = mcaaat(i,calph,nxres)
  200     continue
          extatm = 0
          if (extprc(aacod) .gt. 1) extatm = 1
          do 400 i = 1, nchi + 1 + extatm
            athere(2+i) = scatin(i,nxres)
            do 300, j = 1, ncoord
              relats(j,2+i) = scaaat(j,i,nxres)
  300       continue
  400     continue
          swatm = 0
          if (extprc(aacod) .eq. 1) then
            if (atomin(calph,nxres) .and. scatin(nchi+1,nxres)
     ~          .and. scatin(nchi+2,nxres)) then
              distmn = atdist(mcaaat(1,calph,nxres),
     ~                        scaaat(1,nchi+1,nxres))
              distal = atdist(mcaaat(1,calph,nxres),
     ~                        scaaat(1,nchi+2,nxres))
              if (distal .lt. distmn) then 
                swatm = 1
                athere(nchi+3) = scatin(nchi+2,nxres)
                do 500, i = 1, ncoord
                  relats(i,nchi+3) = scaaat(i,nchi+2,nxres)
  500           continue
              end if
            end if
          end if
          do 1000, nxchi = 1, nchi
            If (athere(nxchi) .and. athere(nxchi+1) .and.
     ~          athere(nxchi+2) .and. athere(nxchi+3)) then
              scangs(nxchi,nxres) = dihed(nmnang+nxchi,
     ~          relats(1,nxchi), relats(1,nxchi+1),
     ~          relats(1,nxchi+2), relats(1,nxchi+3))
            else
              scangs(nxchi,nxres) = null
            end if
 1000     continue
          if (extprc(aacod) .gt. 1 .and. 
     ~                        scangs(nchi,nxres) .ne. null) then
            If (athere(nchi) .and. athere(nchi+1) .and.
     ~          athere(nchi+2) .and. athere(nchi+3+extatm)) then
              scangs(nchi+1,nxres) = dihed(nmnang+nchi+extatm,
     ~          relats(1,nchi), relats(1,nchi+1),
     ~          relats(1,nchi+2), relats(1,nchi+3+extatm))
            else
              scangs(nchi+1,nxres) = null
            end if
            if (scangs(nchi+1,nxres) .ne. null) then
              If (extprc(aacod) .eq. 2 .or. extprc(aacod) .eq. 3) then
                ang1 = scangs(nchi,nxres)
                ang2 = scangs(nchi+1,nxres)
                if (ang1 .lt. 0) ang1 = 360 + ang1
                if (ang2 .lt. 0) ang2 = 360 + ang2
                chipl = nchi
                if (abs(ang1 - ang2) .gt. 180) then
                  if (ang2 .gt. ang1 .and. extprc(aacod) .eq. 2)
     ~                                                chipl = nchi + 1
                  if (ang1 .gt. ang2 .and. extprc(aacod) .eq. 3)
     ~                                                chipl = nchi + 1
                else
                  if (ang2 .lt. ang1 .and. extprc(aacod) .eq. 2)
     ~                                                chipl = nchi + 1
                  if (ang1 .lt. ang2 .and. extprc(aacod) .eq. 3)
     ~                                                chipl = nchi + 1
                end if
                if (chipl .ne. nchi) then
                  swatm = 1
                  scangs(nchi,nxres) = scangs(nchi+1,nxres)
                end if
              end if
              if (extprc(aacod) .eq. 4) then
                if (abs(scangs(nchi+1,nxres)) .lt. 
     ~                         abs(scangs(nchi,nxres))) then
                  swatm = 1
                  scangs(nchi,nxres) = scangs(nchi+1,nxres)
                end if
              end if
              scangs(nchi+1,nxres) = null
            end if
          end if
          if (swatm .eq. 1) then
            if (nswap .eq. 0) 
     ~        write(errfi,'(A)') 'side chain atoms swapped for '
            wkswap = mod(nswap,8) + 1
            if (seqcod(nxres) .le. abetsz) then
              wkres = aacod3(seqcod(nxres)*3-2:seqcod(nxres)*3)
            else
              wkres = hetcod((seqcod(nxres)-abetsz)*3-2:
     ~                       (seqcod(nxres)-abetsz)*3)
            end if
            write(outerr((wkswap-1)*10+1:wkswap*10),'(a3,1x,i4,2x)')
     ~        wkres, nxres
            if (wkswap .eq. 8) then
              write(errfi,'(A)') outerr
              outerr = space
            end if
            nswap = nswap + 1
          end if
        end if
 5000 continue
      if (mod(nswap,8) .ne. 0) write(errfi,'(A)') outerr
      END

      Real Function Dihed(angnum, AtomA, AtomB, AtomC, AtomD)
*
*     routine to calculate the dihedral angle between the 4 atoms
*     given
*
      INCLUDE 'sstruc.par'

      Real Atoma(ncoord), Atomb(ncoord), Atomc(ncoord), atomd(ncoord)
      integer angnum
      Real Atdist

      Real dihatm(ncoord,dihdat), codist(ncoord,dihdat-1),
     ~     atmdst(dihdat-1), dotprd(dihdat-1,dihdat-1),
     ~     ang, cosang, detant
      integer i, j, k

      do 100, I = 1, ncoord
        Dihatm(i,1) = atoma(i)
        Dihatm(i,2) = atomb(i)
        Dihatm(i,3) = atomc(i)
        Dihatm(i,4) = atomd(i)
  100 continue

      do 200, i = 1, dihdat-1
        atmdst(i) = atdist(Dihatm(1,i),Dihatm(1,I+1))
        do 150, j = 1,ncoord
          codist(j,i) = dihatm(j,I+1) - dihatm(j,i)
  150   continue
  200 continue

      do 300, i = 1, dihdat-1
      do 300, j = 1, dihdat-1
        dotprd(i,j) = 0
  300 continue
      do 400, i = 1, dihdat - 2
        do 400, j = i+1, dihdat - 1
          do 400, k = 1, ncoord
            dotprd(i,j) = dotprd(i,j) + codist(k,i) * codist(k,j)
  400 continue
      do 500, i = 1, dihdat-2
        do 500, j = i+1, dihdat - 1
          if (atmdst(i) .eq. 0 .or. atmdst(j) .eq. 0) then
            dotprd(i,j) = 1
          else
            dotprd(i,j) = dotprd(i,j) / atmdst(i) / atmdst(j)
          end if
  500 continue

      if (angnum .gt. ndihed .and. angnum .le. nmnang) then
        cosang =  dotprd(1,3)
      else
        if (dotprd(1,2) .eq. 1 .or. dotprd(2,3) .eq. 1) then
          cosang = 1
        else
          cosang = (dotprd(1,2) * dotprd(2,3) - dotprd(1,3)) /
     ~             (sqrt(1-dotprd(1,2)**2) * sqrt(1-dotprd(2,3)**2))
        end if
      end if
      if (abs(cosang) .gt. 1) cosang = anint(cosang/abs(cosang))
      ang = acos(cosang) * radian

      if (angnum .le. ndihed .or. angnum .gt. nmnang) then
        detant = codist(1,1) * codist(2,2) * codist(3,3) -
     ~           codist(1,1) * codist(2,3) * codist(3,2) +
     ~           codist(1,2) * codist(2,3) * codist(3,1) -
     ~           codist(1,2) * codist(2,1) * codist(3,3) +
     ~           codist(1,3) * codist(2,1) * codist(3,2) -
     ~           codist(1,3) * codist(2,2) * codist(3,1)
        If (detant .lt. 0) ang = -ang
      end if

      dihed = ang
      END

      Subroutine MkTB(Hbond, SSbond, mcangs, bridpt, chnend, seqlen)
*
*     routine to establish the turns and bridges
*
      INCLUDE 'sstruc.par'

      integer Hbond(maxbnd,maxres), seqlen, bridpt(2,maxres),
     ~        chnend(maxchn)
      Real Mcangs(nmnang,maxres)
      Character*1 SSbond(nstruc,maxres)
      integer strdpl

      integer nrules, parlel
      Parameter (nrules = 3, parlel = 1)
      integer nxres, nxbond, nxturn, turnsz(nturns), turnpl(nturns),
     ~        bsofst(nrules,4), i, j, nxrule, bndi, bndj, bndptr, 
     ~        brpl, brdgpt, bsrng(nrules,2), pbrdg(nrules), brpla,
     ~        brdg(4,maxres), brptr, brdir, newptr, nxptr, nxpl,
     ~        exrule(2,nrules), brptpl, nwptpl, nxstrn, 
     ~        lststd, stsht, finsht, nxstr, chcode, stchpl, thstpl,
     ~        nxstpl, nxblg, nxbrdg, strpl, brdpt, nxbrpl,
     ~        strcd(2,maxres), bstval, shtcod(maxres), fstchn

      Character turnch(nturns)*1, wkch, strdch, brdch,
     ~          stchap*(nstchs), stchpa*(nstchs)
*
*     for turn calculations
*
      data turnch / '3', '4', '5'/
      data turnsz / 3, 4, 5/
      data turnpl / thrtnh, alphah, pih/
*
*     for bridge calculations
*
      data ((bsofst(i,j),j = 1,4),i = 1,nrules)
     ~     / -1, 0, 0, 1, 0, 0, 0, 0, -1, -1, -2, 1/
      data ((bsrng(i,j),j = 1,2), i = 1,nrules)
     ~     / 2, -1, 1, 0, 2, -1/
      data pbrdg / 1, -1, -1/
      data exrule /-1, 1, 1, 1, -1, -1/
*
*     fro strand and bridge labelling
*
      data stchap /'ABCDEFGHIJKLMNOPQRSTUVWXYZ'/
      data stchpa /'abcdefghijklmnopqrstuvwxyz'/
c      SAVE turnch, turnsz, turnpl, bsofst, bsrng, pbrdg, stchap, stchpa

      do 100, nxres = 1, seqlen
        do 100, i = 1, nstruc
          ssbond(i,nxres) = space
  100 continue

      fstchn = 1
      do 1000, nxres = 1, seqlen
        if (nxres .gt. chnend(fstchn)) fstchn = fstchn + 1
        do 800, nxbond = 1, 2
          do 600, nxturn = 1, nturns
            if (Hbond(nxbond,nxres) .ne. 0) then
              If (Hbond(nxbond,nxres) - nxres .eq. turnsz(nxturn) .and. 
     ~            Hbond(nxbond,nxres) .le. chnend(fstchn)) then
                If (SSbond(turnpl(nxturn),nxres) .eq. bndend) then
                  SSbond(turnpl(nxturn),nxres) = bndbth
                else
                  SSbond(turnpl(nxturn),nxres) = bndbeg
                end if
                SSbond(turnpl(nxturn),Hbond(nxbond,nxres)) = bndend
                do 400, i = nxres + 1, nxres + turnsz(nxturn) - 1
                  If (SSbond(turnpl(nxturn),i) .eq. space)
     ~              SSbond(turnpl(nxturn),i) = turnch(nxturn)
  400           continue
                do 500 i = nxres, nxres + turnsz(nxturn)
                  SSbond(turn,i) = trnch
  500           continue
              end if
            end if
  600     continue
  800   continue
 1000 continue

      do 2000, nxres = 1,seqlen
        do 2000, nxbond = 1, 4
          brdg(nxbond,nxres) = 0
 2000 continue

      do 4000, nxrule = 1, nrules
        do 3000, Nxres = bsrng(nxrule,1), seqlen + bsrng(nxrule,2)
          do 2400, bndi = 1, 2
            bndptr = hbond(bndi,nxres+bsofst(nxrule,1))
            if (nxrule .le. parlel .or. bndptr .gt. nxres) then
              brdgpt = bndptr + bsofst(nxrule,2)
              If (bndptr .gt. - bsofst(nxrule,3) .and.
     ~            abs(nxres - brdgpt) .gt. bdgsep)  then
                do 2200, bndj = 1, 2
                  If (hbond(bndj,bndptr+bsofst(nxrule,3)) .eq. 
     ~                                 nxres+bsofst(nxrule,4)) then
                    brpl = 1
                    If (brdg(brpl,nxres) .ne. 0) brpl = 2
                    Brdg(brpl,nxres) = pbrdg(nxrule) * brdgpt
                    brdg(brpl+2,nxres) = exrule(1,nxrule)
                    brpla = 1
                    If (brdg(brpla,brdgpt) .ne. 0) brpla = 2
                    Brdg(brpla,brdgpt) = pbrdg(nxrule) * nxres
                    brdg(brpla+2,brdgpt) = exrule(2,nxrule)
                  end if
 2200           continue
              end if
            end if
 2400     continue
 3000   continue
 4000 continue

      do 4500, nxres = 1, seqlen
        if (mcangs(chiral,nxres) .ne. null) then
          ssbond(chisgn,nxres) = '+'
          if (mcangs(chiral,nxres) .lt. 0.0) ssbond(chisgn,nxres) = '-'
        end if
 4500 continue

      continue
      do 5100, nxres = 1, seqlen
        shtcod(nxres) = 0
        strcd(1,nxres) = 0
        strcd(2,nxres) = 0
        bridpt(1,nxres) = 0
        bridpt(2,nxres) = 0
 5100 continue
      nxstrn = 0

      do 7000, nxres = 1, seqlen
        do 6900, brpl = 1, 2
          If (brdg(brpl,nxres) .ne. 0 .and. 
     ~        abs(brdg(brpl,nxres)) .gt. nxres) then
            brptr = abs(brdg(brpl,nxres))
            brdir = brdg(brpl,nxres) / brptr
            brptpl = 2
            if (abs(brdg(1,brptr)) .eq. nxres) brptpl = 1
            do 5600, nxptr = nxres + 1, min(seqlen,nxres+blgszl)
              do 5500, nxpl = 1,2
                If (brdg(nxpl,nxptr) * brdir .gt. 0) then
                  newptr = abs(brdg(nxpl,nxptr))
                  if ((newptr-brptr)/abs(newptr-brptr) .eq. brdir) then
                    if ((nxptr - nxres .gt. blgszs .and.
     ~                   abs(newptr - brptr) .le. blgszs) .OR.
     ~                  (nxptr - nxres .le. blgszs .and.
     ~                   abs(newptr - brptr) .le. blgszl)) then
                      nwptpl = 2
                      if (abs(brdg(1,newptr)) .eq. nxptr) nwptpl = 1
                      do 5300, i = nxres, nxptr
                        if (ssbond(sheet,i) .eq. space)
     ~                             ssbond(sheet,i) = shtsym
 5300                 continue
                      if (brdg(brpl+2,nxres) .lt. 0)
     ~                        ssbond(sheet,nxres) = shtsma
                      if (brdg(nxpl+2,nxptr) .lt. 0)
     ~                        ssbond(sheet,nxptr) = shtsma
                      do 5400, i = brptr, newptr, brdir
                        if (ssbond(sheet,i) .eq. space)
     ~                              ssbond(sheet,i) = shtsym
 5400                 continue
                      if (brdg(brptpl+2,brptr) .lt. 0)
     ~                        ssbond(sheet,brptr) = shtsma
                      if (brdg(nwptpl+2,newptr) .lt. 0)
     ~                        ssbond(sheet,newptr) = shtsma
                      if (strcd(brpl,nxres) .eq. 0) then
                        nxstrn = nxstrn + 1
                        strcd(brpl,nxres) = nxstrn * brdir
                        strcd(brptpl,brptr) = nxstrn * brdir
                      end if
                      strcd(nxpl,nxptr) = strcd(brpl,nxres)
                      strcd(nwptpl,newptr) = strcd(brptpl,brptr)
                      go to 5700
                    end if
                  end if
                end if
 5500         continue
 5600       continue
 5700       continue
            wkch = pbch
            if (brdir .lt. 0) wkch = apbch
            ssbond(bridge,nxres) = wkch
            ssbond(bridge,brptr) = wkch
          end if
 6900   continue
 7000 continue

      nxres  = 1
      lststd = 0                                  ! Andrej
 7500 continue
      if (ssbond(sheet,nxres) .eq. space) then
        nxres = nxres + 1
        if (nxres .gt. seqlen) goto 10000
        goto 7500
      end if

      stsht = nxres
      nxres = stsht + 1
 7700 continue
      if (ssbond(sheet,nxres) .ne. space) then
        nxres = nxres + 1
        if (nxres .gt. seqlen) goto 7800
        goto 7700
      end if
 7800 continue
      finsht = nxres - 1
      lststd = 0

 8000 continue
      call whstrd(strcd, stsht, finsht, nxstr, nxpl, lststd, bstval)
      if (bstval .ne. 0) then
        lststd = abs(strcd(nxpl,nxstr))
        chcode = mod(lststd,nstchs)
        if (chcode .eq. 0) chcode = nstchs
        if (strcd(nxpl,nxstr) .lt. 0) then
          strdch = stchap(chcode:chcode)
        else
          strdch = stchpa(chcode:chcode)
        end if
        stchpl = bridg1
        if (ssbond(bridg1,nxstr) .ne. space) stchpl = bridg2
        if (stchpl .ne. bridg2) then
          thstpl = nxstr
 8200     continue
          nxstpl = strdpl(thstpl,finsht,strcd(nxpl,nxstr),strcd)
          if (nxstpl .le. 0) goto 8300
          if (ssbond(bridg1,nxstpl) .ne. space) stchpl = bridg2
          thstpl = nxstpl
          if (stchpl .ne. bridg2) goto 8200
        end if
 8300   continue
        ssbond(stchpl,nxstr) = strdch
        bridpt(stchpl-bridg1+1,nxstr) = abs(brdg(nxpl,nxstr))
        thstpl = nxstr
 8500   continue
        nxstpl = strdpl(thstpl,finsht,strcd(nxpl,nxstr),strcd)
        if (nxstpl .le. 0) goto 8700
        do 8600, nxblg = thstpl + 1, nxstpl - 1
          ssbond(stchpl,nxblg) = bulgch
 8600   continue
        ssbond(stchpl,nxstpl) = strdch
        strpl = 1
        if (strcd(1,nxstpl) .ne. strcd(nxpl,nxstr)) strpl = 2
        bridpt(stchpl-bridg1+1,nxstpl) = abs(brdg(strpl,nxstpl))
        thstpl = nxstpl
        go to 8500
 8700   continue
        go to 8000
      end if
      nxres = finsht + 1
      if (nxres .le. seqlen) goto 7500
10000 continue
      If (lststd .gt. nstchs)
     ~  Write(errfi,10100) lststd
10100   format('there are ',I3,' strands. Labels have restarted')
      lststd = 0

      Call shtlab(ssbond, bridpt, shtcod, seqlen)
      do 10500 nxres = 1, seqlen
        if (shtcod(nxres) .ne. 0) then
          chcode = mod(shtcod(nxres),nstchs)
          if (chcode .eq. 0) chcode = nstchs
          ssbond(sheet1,nxres) = stchap(chcode:chcode)
        end if
10500 continue

      nxbrdg = 0
      do 11000, nxres = 1, seqlen
        do 10900, nxpl = 1, 2
          If (brdg(nxpl,nxres) .ne. 0 .and. strcd(nxpl,nxres) .eq. 0
     ~        .and. abs(brdg(nxpl,nxres)) .gt. nxres) then
            nxbrdg = nxbrdg + 1
            brdpt = abs(brdg(nxpl,nxres))
            chcode = mod(nxbrdg,nstchs)
            if (chcode .eq. 0) chcode = nstchs
            if (brdg(nxpl,nxres) .lt. 0) then
              brdch = stchap(chcode:chcode)
            else
              brdch = stchpa(chcode:chcode)
            end if
            nxbrpl = bridg1
            if (ssbond(bridg1,nxres) .ne. space) nxbrpl = bridg2
            ssbond(nxbrpl,nxres) = brdch
            bridpt(nxbrpl-bridg1+1,nxres) = brdpt
            nxbrpl = bridg1
            if (ssbond(bridg1,brdpt) .ne. space) nxbrpl = bridg2
            ssbond(nxbrpl,brdpt) = brdch
            bridpt(nxbrpl-bridg1+1,brdpt) = nxres
          end if
10900   continue
11000 continue
      If (nxbrdg .gt. nstchs)
     ~  Write(errfi,11100) nxbrdg
11100   format('there are ',I3,' bridges. Labels have restarted')

      END

      integer Function strdpl(stpl, finpl, thcode, strand)
*
*     find the next occurrence of the code if any in the array
*
      INCLUDE 'sstruc.par'
      integer stpl, finpl, thcode, strand(2,maxres)
      integer nxpl

      do 1000, nxpl = stpl + 1, finpl
        If (strand(1,nxpl) .eq. thcode) go to 1100
        If (strand(2,nxpl) .eq. thcode) go to 1100
 1000 continue
      nxpl = 0
 1100 continue
      strdpl = nxpl
      END

      Subroutine Whstrd(strcd, stsht, finsht, stres, stpl, lststd,
     ~                  bstval)
*
*     find the starting position of the lowest numbered strand that
*     is greater than the last
*
      Include 'sstruc.par'
      integer strcd(2,maxres), stsht, finsht, stres, stpl, lststd,
     ~        bstval

      integer  nxstr, nxpl

      bstval = 0
      do 1000, nxstr = stsht, finsht
        do 1000, nxpl = 1, 2
          If (abs(strcd(nxpl,nxstr)) .gt. lststd) then
            if (bstval .eq. 0) then
              bstval = abs(strcd(nxpl,nxstr))
              stpl = nxpl
              stres = nxstr
            else
              if (abs(strcd(nxpl,nxstr)) .lt. bstval) then
                bstval = abs(strcd(nxpl,nxstr))
                stpl = nxpl
                stres = nxstr
              end if
            end if
          end if
 1000 continue
      END

      Subroutine Shtlab(ssbond, bridpt, shtcod, seqlen)
*
*     label the sheets using numbers which are turned into
*     alphabetic symbols later
*
      INCLUDE 'sstruc.par'
      character*1 ssbond(nstruc,maxres)
      integer bridpt(2,maxres), shtcod(maxres), seqlen

      integer nxres, stsht, finsht, shtnum, nxisht, nxpl
      logical found

      do 100, nxres = 1, seqlen
        shtcod(maxres) = 0
  100 continue

      shtnum = 0
      nxres = 1
 1000 continue
      call fdshus(nxres, ssbond, shtcod, stsht, finsht, seqlen)
      if (stsht .ne. 0) then
        shtnum = shtnum + 1
        do 1500, nxisht = stsht, finsht
          shtcod(nxisht) = shtnum
          do 1400, nxpl = 1,2
            If (bridpt(nxpl,nxisht) .ne. 0) then
              if (shtcod(bridpt(nxpl,nxisht)) .eq. 0)
     ~          call setsht(bridpt(nxpl,nxisht), shtnum, shtcod, 
     ~                      ssbond, seqlen)
            end if
 1400     continue
 1500   continue

 2000   continue
        call fdshps(finsht+1, shtnum, ssbond, shtcod, bridpt, found,
     ~              seqlen)
        if (found) go to 2000

        nxres = finsht + 1
        if (nxres .le. seqlen) go to 1000
      end if
      If (shtnum .gt. nstchs)
     ~  Write(errfi,2100) shtnum
 2100   format('there are ',I3,' sheets. Labels have restarted')
      END

      Subroutine fdshus(stpt, ssbond, shtcod, stsht, finsht, seqlen)
*
*     find the first unset sheet starting at stpt
*
      INCLUDE 'sstruc.par'
      integer stpt, stsht, finsht, shtcod(maxres), seqlen
      character ssbond(nstruc,maxres)*1

      integer nxres

      stsht = 0
      nxres = stpt
 1000 continue
      if (ssbond(sheet,nxres) .eq. space .or. shtcod(nxres) .ne. 0) then
        nxres = nxres + 1
        if (nxres .gt. seqlen) go to 2000
        go to 1000
      end if
      stsht = nxres
 1500 continue
      nxres = nxres + 1
      if (nxres .le. seqlen) then
        if (ssbond(sheet,nxres) .ne. space) goto 1500
      end if
      finsht = nxres - 1
 2000 continue
      END

      subroutine fdshps(stpt, shtnum, ssbond, shtcod, bridpt, found,
     ~                  seqlen)
*
*     find the next partially set sheet, if any (neg shtnum), set it
*     to fully set and then set its partners to partly set if not
*     already set
*
      INCLUDE 'sstruc.par'
      integer stpt, shtnum, shtcod(maxres), bridpt(2,maxres), seqlen
      character ssbond(nstruc,maxres)
      logical found

      integer nxres, stsht, finsht, nxpl

      found = .false.
      nxres = stpt
 1000 continue
      if (shtcod(nxres) .ne. - shtnum) then
        nxres = nxres + 1
        if (nxres .gt. seqlen) goto 5000
        goto 1000
      end if
      stsht = nxres
      found = .true.
 1500 continue
      if (shtcod(nxres) .eq. -shtnum) then
        nxres = nxres + 1
        if (nxres .le. seqlen) goto 1500
      end if
      finsht = nxres - 1

      do 2000, nxres = stsht, finsht
        shtcod(nxres) = shtnum
        do 1900, nxpl = 1,2
          if (bridpt(nxpl,nxres) .ne. 0) then
            if (shtcod(bridpt(nxpl,nxres)) .eq. 0)
     ~        call setsht(bridpt(nxpl,nxres), shtnum, shtcod, ssbond, 
     ~                    seqlen)
          end if
 1900   continue
 2000 continue
 5000 continue
      END

      Subroutine Setsht(refpl, shtnum, shtcod, ssbond, seqlen)
*
*     finds the limits of the super sheet containing refpl and
*     sets this to partially set
*
      INCLUDE 'sstruc.par'
      integer refpl, shtnum, shtcod(maxres), seqlen
      character ssbond(nstruc,maxres)*1

      integer nxres, stsht, finsht

      nxres = refpl
  200 continue
      if (ssbond(sheet,nxres) .ne. space) then
        nxres = nxres + 1
        if (nxres .le. seqlen) go to 200
      end if
      finsht = nxres - 1
      nxres = refpl
  400 continue
      if (ssbond(sheet,nxres) .ne. space) then
        nxres = nxres - 1
        if (nxres .ge. 1) go to 400
      end if
      stsht = nxres + 1

      do 800, nxres = stsht, finsht
        shtcod(nxres) = -shtnum
  800 continue
      END

      Subroutine MkBend(mcangs, ssbond, seqlen)
*
*     routine to determine which main chain residues are bent at over
*     the requisite angle
*
      INCLUDE 'sstruc.par'

      Real mcangs(nmnang,maxres)
      Character*1 Ssbond(nstruc,maxres)
      integer seqlen
      Real Atdist
*
      real bendsz
      parameter (bendsz = 70.0)
      integer nxres

      do 1000, nxres = 1, seqlen
        if (mcangs(kappa,nxres) .gt. bendsz .and.
     ~      mcangs(kappa,nxres) .ne. null) ssbond(bend,nxres) = bendch
 1000   continue
      END

      Subroutine MkSumm(ssbond, sssumm, summpl, seqlen)
*
*     generate a structure summary using the priority rules
*
      INCLUDE 'sstruc.par'
      character*1 ssbond(nstruc,maxres), sssumm(maxres), summpl(maxres)
      integer seqlen
      integer nxres
      character stsym(nstruc)*1, altsym(nstruc)*1
      data stsym /'H', ' ', 'E', 'B', ' ', ' ', 'G', 'I', 'T', 'S', '+'/
      data altsym/'h', ' ', 'e', 'b', ' ', ' ', 'g', 'i', 't', 's', '+'/
c      save stsym, altsym


      do 200, nxres = 1, seqlen
        SSsumm(nxres) = space
        summpl(nxres) = space
  200 continue
      call hlxset(ssbond, sssumm, summpl, alphah, ahsz, stsym(alphah),
     ~            altsym(alphah), seqlen)
      do 400, nxres = 1, seqlen
        If (ssbond(sheet,nxres) .ne. space) then
          if (sssumm(nxres) .eq. space) sssumm(nxres) = stsym(sheet)
          if (summpl(nxres) .eq. space .or. summpl(nxres) .eq.
     ~        altsym(alphah) .or. summpl(nxres) .eq. altsym(sheet))
     ~          summpl(nxres) = stsym(sheet)
          if (ssbond(sheet,nxres) .eq. shtsma) then
            if (summpl(nxres-1) .eq. space .or. summpl(nxres-1) .eq.
     ~          stsym(bridge)) summpl(nxres-1) = altsym(sheet)
            if (summpl(nxres+1) .eq. space .or. summpl(nxres-1) .eq.
     ~          stsym(bridge)) summpl(nxres+1) = altsym(sheet)
          end if
        end if
        If (ssbond(bridge,nxres) .ne. space) then
          if (sssumm(nxres) .eq. space) sssumm(nxres) = stsym(bridge)
          if (summpl(nxres) .eq. space) summpl(nxres) = stsym(bridge)
        end if
  400 continue
      call hlxset(ssbond, sssumm, summpl, thrtnh, thrtsz, stsym(thrtnh),
     ~            altsym(thrtnh), seqlen)
      call hlxset(ssbond, sssumm, summpl, pih, pihsz, stsym(pih),
     ~            altsym(pih), seqlen)
      call hlxres(sssumm, stsym(thrtnh), altsym(thrtnh), stsym(turn),
     ~            altsym(turn), thrtsz, seqlen)
      call hlxres(summpl, stsym(thrtnh), altsym(thrtnh), stsym(turn),
     ~            altsym(turn), thrtsz, seqlen)
      call hlxres(sssumm, stsym(pih), altsym(pih), stsym(turn),
     ~            altsym(turn), pihsz, seqlen)
      call hlxres(summpl, stsym(pih), altsym(pih), stsym(turn),
     ~            altsym(turn), pihsz, seqlen)
      call turnst(ssbond, sssumm, summpl, stsym(turn), altsym(turn),
     ~            seqlen)
      call bendst(ssbond, sssumm, summpl, seqlen)

      END

      Subroutine Hlxset(ssbond, sssumm, summpl, hlxpos, hlxsz, hlxch,
     ~                  altch, seqlen)
*
*     go through the bond tables and note any helices into the
*     summary line
*
      INCLUDE 'sstruc.par'
      character*1 ssbond(nstruc,maxres), sssumm(maxres), hlxch, altch,
     ~            summpl(maxres)
      integer hlxpos, hlxsz, seqlen

      integer nxres, nxsym

      do 1000, nxres = 2, seqlen - hlxsz
        if (ssbond(hlxpos,nxres) .eq. bndbeg .or.
     ~      ssbond(hlxpos,nxres) .eq. bndbth) then
          if (ssbond(hlxpos,nxres-1) .eq. bndbeg .or.
     ~        ssbond(hlxpos,nxres-1) .eq. bndbth) then
            do 500, nxsym = 0, hlxsz - 1
              if (sssumm(nxres+nxsym) .eq. space)
     ~          sssumm(nxres+nxsym) = hlxch
              if (summpl(nxres+nxsym) .eq. space .OR. 
     ~            summpl(nxres+nxsym) .eq. altch)
     ~          summpl(nxres+nxsym) = hlxch
  500       continue
            if (summpl(nxres-1) .eq. space) summpl(nxres-1) = altch
            if (summpl(nxres+hlxsz) .eq. space)
     ~             summpl(nxres+hlxsz) = altch
          end if
        end if
 1000 continue
      END

      Subroutine hlxres(summ, hlxch, altch, turnch, alturn, hlxsz,
     ~                  seqlen)
*
*     to remove helix character runs of less than minimal helix size
*
      INCLUDE 'sstruc.par'
      character*1 summ(maxres), hlxch, turnch, altch, alturn
      integer hlxsz, seqlen

      integer nxres, stpos, nxsym

      stpos = 0
      do 1000, nxres = 1, seqlen
        if (summ(nxres) .eq. hlxch .or. summ(nxres) .eq. altch) then
          If (stpos .eq. 0) stpos = nxres
        else
          if (stpos .ne. 0) then
            if (nxres - stpos .lt. hlxsz) then
              do 500, nxsym = stpos, nxres - 1
                if (summ(nxsym) .eq. hlxch) then
                  summ(nxsym) = turnch
                else
                  summ(nxsym) = alturn
                end if
  500         continue
            end if
            stpos = 0
          end if
        end if
 1000 continue
      if (stpos .ne. 0) then
        if (seqlen - stpos + 1 .lt. hlxsz) then
              do 1500, nxsym = stpos, seqlen
                if (summ(nxres) .eq. hlxch) then
                  summ(nxsym) = turnch
                else
                  summ(nxsym) = alturn
                end if
 1500         continue
        end if
      end if
      END

      Subroutine turnst(ssbond, sssumm, summpl, turnch, alturn, seqlen)
*
*     search for solitary turn type bonds and set in the symbol
*
      INCLUDE 'sstruc.par'
      character*1 ssbond(nstruc,maxres), sssumm(maxres), turnch,
     ~            summpl(maxres), alturn
      integer seqlen

      integer nxres, nxsym, nxhlx, hlxpri(nhelix), hlxsz(nhelix)

      hlxpri(1) = alphah
      hlxpri(2) = thrtnh
      hlxpri(3) = pih
      hlxsz(1) = ahsz
      hlxsz(2) = thrtsz
      hlxsz(3) = pihsz

      do 2000, nxhlx = 1, nhelix
        If (ssbond(hlxpri(nxhlx),1) .eq. bndbeg .and.
     ~      ssbond(hlxpri(nxhlx),2) .ne. bndbeg) then
          do 200, nxsym = 2, hlxsz(nxhlx)
            If (sssumm(nxsym) .eq. space) sssumm(nxsym) = turnch
            If (summpl(nxsym) .eq. space .or. summpl(nxsym) .eq.
     ~          alturn) summpl(nxsym) = turnch
  200     continue
          If (summpl(1) .eq. space) summpl(1) = alturn
          If (summpl(hlxsz(nxhlx)+1) .eq. space)
     ~                     summpl(hlxsz(nxhlx)+1) = alturn
        end if
        do 1000, nxres = 2, seqlen - hlxsz(nxhlx)
          If ((ssbond(hlxpri(nxhlx),nxres) .eq. bndbeg .or.
     ~         ssbond(hlxpri(nxhlx),nxres) .eq. bndbth) .AND.
     ~        (ssbond(hlxpri(nxhlx),nxres-1) .ne. bndbeg .and.
     ~         ssbond(hlxpri(nxhlx),nxres-1) .ne. bndbth) .AND.
     ~        (ssbond(hlxpri(nxhlx),nxres+1) .ne. bndbeg .and.
     ~         ssbond(hlxpri(nxhlx),nxres+1) .ne. bndbth)) then
            do 500, nxsym = nxres + 1, Nxres + hlxsz(nxhlx) - 1
              If (sssumm(nxsym) .eq. space) sssumm(nxsym) = turnch
              If (summpl(nxsym) .eq. space .or. summpl(nxsym) .eq.
     ~            alturn) summpl(nxsym) = turnch
  500       continue
            If (summpl(nxres) .eq. space) summpl(nxres) = alturn
            If (summpl(nxres+hlxsz(nxhlx)) .eq. space)
     ~                       summpl(nxres+hlxsz(nxhlx)) = alturn
          end if
 1000   continue
 2000 continue
      END

      Subroutine Bendst(ssbond, sssumm, summpl, seqlen)
*
*     enter bend symbols into summary line
*
      Include 'sstruc.par'
      character*1 ssbond(nstruc,maxres), sssumm(maxres), summpl(maxres)
      integer seqlen

      Integer nxres
      do 1000, nxres = 1, seqlen
        If (ssbond(bend,nxres) .ne. space) then
          if (sssumm(nxres) .eq. space) sssumm(nxres) =
     ~                                      ssbond(bend,nxres)
          If (summpl(nxres) .eq. space) summpl(nxres) =
     ~                                      ssbond(bend,nxres)
        end if
 1000 continue
      END

      Subroutine mkauth(seqbcd, autsum, ssord, astruc, seqlen)
*
*     reread the author structure records and create the structure
*
      INCLUDE 'sstruc.par'
      character seqbcd(maxres)*6, autsum(maxres)*1, ssord*21
      logical astruc
      integer seqlen
      integer respl

      integer nxsym, stpos, finpos, lstpos, hlxno, hlxcls, nxres,
     ~        strno, sense, strpri, hlxcds
      parameter (hlxcds = 10)
      character stres*6, finres*6, autrec*80, strkch*1, shtid*3,
     ~          hlxch(hlxcds)*1
      data hlxch/'H','J','I','K','G','L','M','N','O','P'/
c      save hlxch

      do 100, nxres = 1, seqlen
        autsum(nxres) = space
  100 continue

      if (astruc) then
        Rewind(unit=authfi)
        lstpos = 0

  200   continue
        read(authfi,'(a)',end=2000) autrec
        if (autrec(1:keylen) .eq. hlxkey) then
          read(autrec,400) hlxno, stres(1:1), stres(2:6), finres(1:1),
     ~                     finres(2:6), hlxcls
  400     format(7x,i3,9x,a1,1x,a5,5x,a1,1x,a5,i2)
          strkch = hlxch(1)
          if (hlxcls .le. hlxcds .and. hlxcls .gt. 0) 
     ~                                 strkch = hlxch(hlxcls)
        else
          if (autrec(1:keylen) .eq. shtkey) then
            read(autrec,600) strno, shtid, stres, finres, sense
  600       format(7x,i3,1x,a3,7x,a6,5x,a6,i2)
            strkch = shtsym
          else
            if (autrec(1:keylen) .eq. trnkey) then
              read(autrec,800) stres, finres
  800         format(19x,a6,5x,a6)
              strkch = trnch
            else
              go to 200
            end if
          end if
        end if
        stpos = respl(seqbcd, stres, lstpos, seqlen)
        finpos = respl(seqbcd, finres, lstpos, seqlen)
        If (finpos .lt. 0 .or. stpos .lt. 0 .or.
     ~      finpos .lt. stpos .or. (strkch .eq. hlxch(1) .and. 
     ~       (hlxcls .le. 0 .or. hlxcls .gt. hlxcds))) then
          Write(errfi,*)'structure record in error'
          Write(errfi,*)autrec
        else
          strpri = Indexa(ssord,strkch)
          do 1000, nxsym = stpos, finpos
            if (Indexa(ssord,autsum(nxsym)) .gt. strpri) 
     ~        autsum(nxsym) = strkch
 1000     continue
        end if
        go to 200
 2000   continue
      end if
      end

      Subroutine disulf(seqbcd, dsdsts, scangs, scaaat, scatin, atomin,
     ~                  mcaaat, seqcod, dsdef, dspart, astruc, seqlen)
*
*     read the author file and extract any author definitions of
*     disulphide bridges. check for possible bridges on a distance
*     criterion as well.
*
      INCLUDE 'sstruc.par'
      character seqbcd(maxres)*6, dsdef(maxres)*1
      real scangs(nsdang,maxres), dsdsts(3,maxres),
     ~     scaaat(ncoord,sdchna,maxres), mcaaat(ncoord,mnchna,maxres)
      integer dspart(maxres), seqcod(maxres), seqlen
      logical astruc, scatin(sdchna,maxres), atomin(mnchna,maxres)
      real atdist
      integer respl

      character autdef*1, disdef*1
      parameter (autdef = 'A', disdef = 'D')
      integer nxdist, nxres, pos1, pos2, errset, partpl, nxcys
      real partds, ssdist
      logical cystyp
      character autrec*80, code1*6, code2*6, typefl*1

      do 100, nxres = 1, seqlen
        dsdef(nxres) = space
        dspart(nxres) = 0
        do 100, nxdist = 1, 3
          dsdsts(nxdist,nxres) = 0.0
  100 continue

      If (astruc) then
        rewind(unit=authfi)

        pos1 = 0
  200   continue
        if (pos1 .lt. 0) pos1 = 0
        read (authfi,'(A)', end = 2000) autrec
        if (autrec(1:keylen) .ne. dsfkey) go to 200

        read(autrec,400) code1(1:1), code1(2:6), code2(1:1), code2(2:6)
  400   format(15x,a1,1x,a5,7x,a1,1x,a5)
        pos1 = respl(seqbcd, code1, pos1, seqlen)
        errset = 0
        if (pos1 .gt. 0) pos2 = respl(seqbcd, code2, pos1, seqlen)
        If (pos1 .gt. 0 .and. pos2 .gt. 0) then
          if (Cystyp(seqcod(pos1)) .and. cystyp(seqcod(pos2))) then
            typefl = autdef
            call dsset(pos1, pos2, dsdsts, scangs, scaaat, scatin,
     ~                 atomin, mcaaat, dsdef, dspart, typefl, seqlen)
          else
            errset = 1
          end if
        else
          errset = 1
        end if
        If (errset .eq. 1) then
          write(errfi,'(A)') 'error in SSBOND record'
          write(errfi,'(A)') autrec
        end if
        go to 200

 2000   continue
        close(unit = authfi)
      end if

      nxres = 1
 2500 continue
      if (cystyp(seqcod(nxres))) then
        if (scatin(sgamma,nxres)) then
          partpl = 0
          partds = 0.0
          do 3000, nxcys = nxres + 1, seqlen
            if (cystyp(seqcod(nxcys))) then
              if (scatin(sgamma,nxcys)) then
                ssdist = atdist(scaaat(1,sgamma,nxres),
     ~                          scaaat(1,sgamma,nxcys))
                if (ssdist .lt. sssep) then
                  if (partds .ne. 0.0) then
                    if (partds .lt. ssdist) then
                      Write(errfi,2700) nxres, nxcys, ssdist
                    else
                      Write(errfi,2700) nxres, partpl, partds
 2700                 format('multifurcation of disulphides ',
     ~                         I4,1x,i4,1x,f3.1)
                      partpl = nxcys
                      partds = ssdist
                    end if
                  else
                    partpl = nxcys
                    partds = ssdist
                  end if
                end if
              end if
            end if
 3000     continue
          if (partpl .ne. 0) then
            typefl = disdef
            call dsset(nxres, partpl, dsdsts, scangs, scaaat, scatin,
     ~                 atomin, mcaaat, dsdef, dspart, typefl, seqlen)
          end if
        end if
      end if
      nxres = nxres + 1
      if (nxres .le. seqlen) go to 2500
      END

      logical function cystyp(seqcod)
*
*     check that we do have a potential disulfide former
*
      INCLUDE 'sstruc.par'
      integer seqcod

      If (seqcod .eq. cyscod .or. seqcod .eq. mprcod) then
        cystyp = .true.
      else
        cystyp = .false.
      end if
      end

      Subroutine dsset(part1, part2, dsdsts, scangs, scaaat, scatin,
     ~                 atomin, mcaaat, dsdef, dspart, typefl, seqlen)
*
*     calculate all the features for a disulphide bridge. that is 3 chi
*     angles, calpha, cbeta, and ss distances. set in the partner and
*     definition fields. Check for conflict with author defined records
*     or previously defined bridges
*
      INCLUDE 'sstruc.par'
      integer part1, part2, dspart(maxres), seqlen
      real dsdsts(3,maxres), scangs(nsdang,maxres), 
     ~     scaaat(ncoord,sdchna,maxres), mcaaat(ncoord,mnchna,maxres)
      logical scatin(sdchna,maxres), atomin(mnchna,maxres)
      character dsdef(maxres)*1, typefl*1
      real atdist, dihed

      character*1 botdef, autdef
      real mxdis
      parameter (botdef = 'B', autdef = 'A', mxdis = 9.9)
      integer nxdis

      If ((dspart(part1) .ne. 0 .and. dspart(part1) .ne. part2) .or.
     ~    (dspart(part1) .eq. 0 .and. dspart(part2) .ne. 0)) then
        if (dsdef(part1) .ne. space .or. dsdef(part2) .ne. space) then
          write(errfi,100) part1, part2
  100     format('conflict with author defined disulphide -',
     ~           ' abandonning ',I4,1x,I4)
        else
          write(errfi,200) part1, part2
  200     format('conflict with previously defined disulphide -',
     ~           ' abandoning ',I4,1x,I4)
        end if
      else
        if (dsdef(part1) .eq. autdef) then
          dsdef(part1) = botdef
          dsdef(part2) = botdef
        else
          dspart(part1) = part2
          dspart(part2) = part1
          dsdef(part1) = typefl
          dsdef(part2) = typefl
          if (atomin(calph,part1) .and. scatin(cbeta,part1) .and. 
     ~        scatin(sgamma,part1) .and. scatin(sgamma,part2))
     ~      scangs(2,part1) = dihed(nmnang+2,
     ~        mcaaat(1,calph,part1), scaaat(1,cbeta,part1),
     ~        scaaat(1,sgamma,part1), scaaat(1,sgamma,part2))
          if (scatin(cbeta,part1) .and. scatin(sgamma,part1) .and. 
     ~        scatin(sgamma,part2) .and. scatin(cbeta,part2)) then
            scangs(3,part1) = dihed(nmnang+3,
     ~        scaaat(1,cbeta,part1), scaaat(1,sgamma,part1), 
     ~        scaaat(1,sgamma,part2), scaaat(1,cbeta,part2))
            scangs(3,part2) = scangs(3,part1)
          end if
          if (atomin(calph,part2) .and. scatin(cbeta,part2) .and. 
     ~        scatin(sgamma,part2) .and. scatin(sgamma,part1))
     ~      scangs(2,part2) = dihed(nmnang+2,
     ~        mcaaat(1,calph,part2), scaaat(1,cbeta,part2),
     ~        scaaat(1,sgamma,part2), scaaat(1,sgamma,part1))
          do 1000, nxdis = 1, 3
            dsdsts(nxdis,part1) = -1
 1000     continue
          If (atomin(calph,part1) .and. atomin(calph,part2))
     ~      dsdsts(1,part1) = atdist(mcaaat(1,calph,part1),
     ~                               mcaaat(1,calph,part2))
          If (scatin(cbeta,part1) .and. atomin(cbeta,part2))
     ~      dsdsts(2,part1) = atdist(scaaat(1,cbeta,part1),
     ~                               scaaat(1,cbeta,part2))
          If (scatin(sgamma,part1) .and. scatin(sgamma,part2))
     ~      dsdsts(3,part1) = atdist(scaaat(1,sgamma,part1),
     ~                               scaaat(1,sgamma,part2))
          do 1200, nxdis = 1, 3
            if (dsdsts(nxdis,part1) .gt. mxdis) then
              write(errfi,1100) part1, part2, dsdsts(nxdis,part1)
 1100         format('disulphide distance too large for',I4,1x,I4,1x,
     -               f5.1)
              dsdsts(nxdis,part1) = mxdis
            end if
 1200     continue
          do 1500, nxdis = 1,3
            dsdsts(nxdis,part2) = dsdsts(nxdis,part1)
 1500     continue
        end if
      end if
      END

      Integer function respl(seqbcd, theres, lstpos, seqlen)
*
*     search through the residue identifiers to find the position
*     of the relevant code. Codes will normally be in order so the
*     search starts from the last position.
*
      INCLUDE 'sstruc.par'
      character seqbcd(maxres)*6, theres*6
      integer lstpos, seqlen

      integer nxres

      do 100, nxres = lstpos + 1, seqlen
        if (seqbcd(nxres) .eq. theres) go to 300
  100 continue
      do 200, nxres = 1, lstpos
        if (seqbcd(nxres) .eq. theres) go to 300
  200 continue
      nxres = -1
  300 continue
      respl = nxres
      end

      Subroutine result(hbonde, hbond, aacod3, aacod1, seqcod, seqbcd, 
     ~                  brksym, ssbond, bridpt, sssumm, summpl, autsum, 
     ~                  mcangs, altcod, aastd, atpres, thmprs, scangs,
     ~                  cadsts, brknam, headln, hedlen, outsty, dsdsts,
     ~                  dsdef, dspart, hetcod, oois, seqlen)
*
*     Results are in 4 parts. The standard output set of details
*     one line to an amino acid. The second set makes the full set
*     again one line per amino acid. The third set is the structure
*     patterns and summaries at 100 amino acids to the line
*     with finally error and information messages
*
      INCLUDE 'sstruc.par'
      integer hbond(maxbnd,maxres), seqcod(maxres), seqlen,
     ~        bridpt(2,maxres), hedlen, dspart(maxres), oois(2,maxres)
      real hbonde(maxbnd,maxres), mcangs(nmnang,maxres),
     ~     scangs(nsdang,maxres), cadsts(11,maxres), dsdsts(3,maxres)
      character aacod3*(abetsz*3), ssbond(nstruc,maxres)*1,
     ~          sssumm(maxres)*1, brknam*(fnamln), summpl(maxres),
     ~          seqbcd(maxres)*6, aacod1*(abetsz), autsum(maxres)*1,
     ~          brksym(maxres)*1, headln*132, altcod(maxres)*1,
     ~          aastd(maxres)*1, atpres(3,maxres), thmprs(maxres),
     ~          outsty*1, dsdef(maxres)*1, hetcod*(nhet*3)

      integer nxres, i, bpl(maxbnd), staa, nxstr, nxang, nxst, nxrow,
     ~        aathln, strpts(9), errct, j, nxpl, hednum, nxbit
      character aa3*3, aa1*1, wkrec*80, lefthd(13)*9, rthd(13)*9,
     ~          wkln1*(aaonln), wkln2*122, sumln(3)*(aaonln),
     ~          seqser*11, sumser*11
      data bpl /3,1,4,2/
      data strpts/chisgn, bend, turn, pih, thrtnh, bridg2, bridg1,
     ~            sheet1, alphah/
      data lefthd /'chirality', '    bends', '    turns', '  5-turns',
     ~             '  3-turns', ' bridge-2', ' bridge-1', '   sheets',
     ~             '  4-turns', '   author', 'Kabs/Sand', '  summary',
     ~             ' sequence'/
      data rthd   /'chirality', 'bends    ', 'turns    ', '5-turns  ',
     ~             '3-turns  ', 'bridge-2 ', 'bridge-1 ', 'sheets   ',
     ~             '4-turns  ', 'author   ', 'Kabs/Sand', 'summary  ',
     ~             'sequence '/
c      save bpl, strpts, lefthd, rthd

      hednum = 1
      call hedset(brknam, headln, hedlen, hednum, seqlen)
      do 500, nxres = 1, seqlen
        if (seqcod(nxres) .le. abetsz) then
          staa = (seqcod(nxres) - 1) * 3 + 1
          aa3 = aacod3(staa:staa+2)
          aa1 = aacod1(seqcod(nxres):seqcod(nxres))
        else
          staa = (seqcod(nxres) - abetsz - 1) * 3 + 1
          aa3 = hetcod(staa:staa+2)
          aa1 = '-'
        end if
        write(outfi,300) nxres, brksym(nxres), seqbcd(nxres),
     ~    altcod(nxres), aa3, aa1, autsum(nxres), sssumm(nxres),
     ~    summpl(nxres), (ssbond(nxstr,nxres), nxstr = alphah, sheet1),
     ~    (ssbond(nxstr,nxres), nxstr = bridg1, chisgn),
     ~    (bridpt(nxstr,nxres), nxstr = 1, 2),
     ~    (mcangs(nxang,nxres), nxang = 1, nmnang),
     ~    (hbond(bpl(i),nxres), hbonde(bpl(i),nxres), i=1,4),
     ~    oois(ooi1,nxres), oois(ooi2,nxres)
  300   format(i4,a1,a6,1x,a1,1x,a3,1x,a1,3(1x,a1),1x,9a1,2(1x,i4),
     ~         4(1x,f6.1),2(1x,f5.1),4(1x,i4,1x,f4.1),2(1x,i2))
  500 continue
      write(outfi,'(a)')char(12)

      if (outsty .eq. 'F') then
        hednum = 2
        call hedset(brknam, headln, hedlen, hednum, seqlen)
        do 1000, nxres = 1, seqlen
          if (seqcod(nxres) .le. abetsz) then
            staa = (seqcod(nxres) - 1) * 3 + 1
            aa3 = aacod3(staa:staa+2)
            aa1 = aacod1(seqcod(nxres):seqcod(nxres))
          else
            staa = (seqcod(nxres) - abetsz - 1) * 3 + 1
            aa3 = hetcod(staa:staa+2)
            aa1 = '-'
          end if
          continue
          write(outfi,900)nxres, brksym(nxres), seqbcd(nxres), aa3, aa1,
     ~      aastd(nxres), (atpres(i,nxres),i=1,3),
     ~      thmprs(nxres), (scangs(i,nxres),i=1,4), dspart(nxres), 
     ~      dsdef(nxres), (dsdsts(i,nxres),i=1,3), 
     ~      (cadsts(i,nxres),i=1,11)
  900     format(i4,a1,a6,1x,a3,1x,a1,5(1x,a1),4(1x,f6.1),1x,i4,1x,
     ~           a1,3(1x,f4.1),11(1x,f4.1))
 1000   continue
        write(outfi,'(a)') char(12)
      end if

      write(outfi,'(a)') brknam
      write(outfi,'(a)') headln(1:hedlen)
      do 2000, nxst = 1, seqlen, aaonln
        write(outfi,*)
        aathln = aaonln
        if (nxst + aathln - 1 .gt. seqlen) aathln = seqlen - nxst + 1
        do 1100, nxstr = 1,aathln
          sumln(1)(nxstr:nxstr) = autsum(nxst+nxstr-1)
          sumln(2)(nxstr:nxstr) = sssumm(nxst+nxstr-1)
 1100   continue
        do 1300, nxrow = 1, 2
          wkln2 = lefthd(9+nxrow) // '  ' // sumln(nxrow)(1:aathln) //
     ~             '  ' // rthd(9+nxrow)
          write(outfi,1200)wkln2(1:aathln+22)
 1200     format(a)
 1300   continue
        do 1500, nxrow = 1, 9
          do 1400, nxstr = 1, aathln
            wkln1(nxstr:nxstr) = ssbond(strpts(nxrow),nxst+nxstr-1)
 1400     continue
          wkln2 = lefthd(nxrow) // '  ' // wkln1(1:aathln) // '  '
     ~            // rthd(nxrow)
          write(outfi,1200)wkln2(1:aathln+22)
 1500   continue
        do 1600, nxstr = 1,aathln
          sumln(3)(nxstr:nxstr) = summpl(nxst+nxstr-1)
          if (seqcod(nxst+nxstr-1) .le. abetsz) then
            wkln1(nxstr:nxstr) = 
     ~          aacod1(seqcod(nxst+nxstr-1):seqcod(nxst+nxstr-1))
          else
            wkln1(nxstr:nxstr) = '-'
          end if
 1600   continue
        wkln2 = lefthd(12) // '  ' // sumln(3)(1:aathln) // '  '
     ~          // rthd(12)
        write(outfi,1200) wkln2(1:aathln+22)
        wkln2 = lefthd(13) // '  ' // wkln1(1:aathln) // '  ' //rthd(13)
        write(outfi,1200) wkln2(1:aathln+22)
        write(outfi,1700) (nxst-1+j*10,j=1,aathln/10)
 1700   format(11x,10(i10))
 2000 continue

      call msgprt
      write(outfi,'(a)') char(12)
      end

      subroutine carslt(aacod3, aacod1, seqcod, seqbcd, mcangs, cadsts,
     ~                  brksym, altcod, aastd, brknam, headln, hedlen,
     ~                  autsum, hetcod, oois, seqlen)
*
*     results for c-alpha only files. these are placed in a differently
*     named version of the output file
*
      INCLUDE 'sstruc.par'
      character aacod3*(abetsz*3), aacod1*(abetsz), seqbcd(maxres)*6,
     ~          altcod(maxres)*1, aastd(maxres)*1, headln*132,
     ~          brknam*(fnamln), brksym(maxres)*1, autsum(maxres)*1,
     ~          hetcod*(nhet*3)
      integer seqcod(maxres), hedlen, seqlen, oois(2,maxres)
      real mcangs(nmnang,maxres), cadsts(11,maxres)

      integer hednum, nxres, staa, nxang, nxdis
      character aa3*3, aa1*1

      hednum = 3
      call hedset(brknam, headln, hedlen, hednum, seqlen)
      do 1000, nxres = 1, seqlen
        if (seqcod(nxres) .le. abetsz) then
          staa = (seqcod(nxres) - 1) * 3 + 1
          aa3 = aacod3(staa:staa+2)
          aa1 = aacod1(seqcod(nxres):seqcod(nxres))
        else
          staa = (seqcod(nxres) - abetsz - 1) * 3 + 1
          aa3 = hetcod(staa:staa+2)
          aa1 = '-'
        end if
        write(outfi,300) nxres, brksym(nxres), seqbcd(nxres),
     ~        altcod(nxres), aa3, aa1, aastd(nxres), autsum(nxres),
     ~        (mcangs(nxang,nxres), nxang = chiral,kappa),
     ~        (cadsts(nxdis,nxres), nxdis = 1,11),
     ~        oois(ooi1,nxres), oois(ooi2,nxres)
  300   format(i4,a1,a6,1x,a1,1x,a3,3(1x,a1),1x,f6.1,1x,f5.1,
     ~         11(1x,f4.1),2(1x,i2))
 1000 continue
      call msgprt
      write(outfi,'(a)') char(12)
      end

      SUBROUTINE msgprt
*
*     write the error messages to the output file
*
      INCLUDE 'sstruc.par'
      character wkrec*80
      integer errflg

      rewind(unit=errfi)
      errflg = 0
  200 continue
      read(errfi,300,end=1000) wkrec
  300 format(a)
      if (errflg .eq. 0) then
        write(outfi,500)'Messages'
  500   format(////a)
      end if
      errflg = 1
      write(outfi,'(a)') wkrec
      write(*,*) wkrec
      go to 200
 1000 continue
      close(unit=errfi)
      END
        
      Subroutine hedset(brknam, headln, hedlen, hednum, seqlen)
*
*     produce headings from results parts 1 and 2 and carslt
*
      INCLUDE 'sstruc.par'
      character brknam*(fnamln), headln*132
      Integer hedlen, hednum, seqlen

      write(outfi,'(2a)')
     ~            'Secondary structure calculation program - ',
     ~            'copyright by David Keith Smith, 1989'
      write(outfi,100) brknam
  100 format(a)
      write(outfi,'(a)') headln(1:hedlen)
      write(outfi,120) seqlen
  120 format('Sequence length -',I5)
 
      if (hednum .eq. 1) then
        write(outfi,'(3a)')
     ~  '            A       A K K                                  ',
     ~  '                                      hydrogen bonding',
     ~  '             Ooi''s'
        write(outfi,'(99a)')
     ~  'strk chain/ l amino u & S structure   bridge        dihedral',
     ~  ' angles     ',
     ~  '                donor    acceptor   donor    acceptor  N  N'
        write(outfi,'(99a)')
     ~  'num  seq.no t acids t S + patterns   partners    phi    psi',
     ~  '  omega  alpha ',
     ~  'kappa   tco to/energy fr/energy to/energy fr/energy  8 14'
      else if (hednum .eq. 2) then
        Write(outfi,'(a)') '                  S Atoms T'
        Write(outfi,'(99a)')
     ~  'strk chain/ amino t prsnt h  side chain dihedral angles ',
     ~  '  disulphide bridges                  C alpha distances for'
        Write(outfi,'(99a)')
     ~  'num  seq.no acids d W M S m  Chi1   Chi2   Chi3   Chi4  ',
     ~  ' ptr T CACA CBCB  S/S  i+1  i+2  i+3  i+4  i+5  i+6  i+7',
     ~  '  i+8  i+9 i+10 i+11'
      else if (hednum .eq. 3) then
        write (outfi,'(99a)')
     ~  '            A       S A    dihedral    ',
     ~  '                                                     Ooi''s'
        write(outfi,'(99a)')
     ~  'strk chain/ l amino t u     angles                   C alpha ',
     ~  'distances for                   N  N'
        write(outfi,'(99a)')
     ~  'num  seq.no t acids d t  alpha kappa  i+1  i+2  i+3  i+4  ',
     ~  'i+5  i+6  i+7  i+8  i+9 i+10 i+11  8 14'
      end if
      end

c --- LPI-F77 sometimes returns -65536 or -16383 when it can't find the target
      integer function indexa(string, target)
        character string*(*), target*(*)
        i = index(string,target)
        if (i.lt.0) then
          indexa = 0
        else
          indexa = i
        end if
        return
      end

      subroutine exit(iex)
      integer iex
      stop
      end
