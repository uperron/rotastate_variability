      PROGRAM SUMMER
C
C +----------------------------------------------------------------------------+
C |                                                                            |
C |               Published by the Movement of the 23rd of November            |
C |                                                                            |
C |                Copyright April-December 1989, John Overington              |
C |                                                                            |
C +----------------------------------------------------------------------------+
C
C To sum the mutation matrices produced by beauty, results give the 
C probability that a mutation will occur, and also the standard deviation of
C this probability. NB this error is only valid for 0.0 < P > 1.0 and NSAMPLE
C greater than about 30. Therefore do not trust the low statistics. In future
C version will allow for a more flexible estimation of this error.
C
C This program forms part of the structure prediction suite "22 Glorious Years"
C
C ------------------------------------------------------------------------------
C 
C VERSION HISTORY
C
C  1 - Initial version, simple counting and differences of probability 
C      distributions.
C  2 - Version where the differences are between the matrix with the property
C      compared to the matrix without the property.
C  3 - Included totalling of environment matrices
C  4 - Increased the number of matrices examined, made optional output of 
C      standard errors in the data, made number of summed matrices determined
C      from data file
C  5 - Changed input data structure from JOY, rewrote all of program affected
C      Added option to output difference mutation matrices. Removed restriction 
C      on number of matrices from JOY output. N.B. This version is compatible
C      with JOY V 15, in earlier versions the classification used was different.
C      The matrices will still be read and analyzed satisfactorily but the
C      number of residues in each environment will be wrong. BEWARE !!
C      Removed some of the redundant summing of residue environment classes.
C  6 - Added 21st residue type to alignment, (the insertion character).
C  7 - Added single feature mutation matrices. Hopefully a useful summary.
C      Changed command line structure. Added counting of various sets.
C      Included version string and version reporting. Reduced MAXMAT to 65
C      Added .dat file suitable for input directly into BOEING GRAPH. Added
C      option g to control this, default is no. Added option to ignore H-bonding
C      of MET and CYS. Option to do this is s. Added output of differences for
C      various features. Fixed bug in number of substitutions per dataset.
C      Added output of number of substitution bins. Made parameters in include
C      file. Added report of % contributions to data.
C  8 - Changed reading of data from .sum files, now read no of sequences and
C      no of residues in each dataset. N.B. this makes the old and new versions
C      incompatible, reordered sections of code for efficiency. Added summary of
C      composition statistics for table. Changed format of substitution data
C      from joy now 20I4, was 20I3, N.B. This makes earlier versions unreadable.
C      Included HIS as being able to form a hydrogen bond to a mainchain NH.
C      Included output of summed feature probability tables. (at the moment to
C      unit 44). Added output of number of accessible and inaccessible secondary
C      structure residues. Added output from unit 44 to standard output file.
C      Added summation of cis-peptide substitution, table 66. Output in non
C      standard order though. Added output of new table for environmental
C      conservation probabilities. Have to output probabilities for this table.
C      Fixed bug in output of summed feature table titles. Output probability
C      for all buried and surface classes of secondary structure. Added ':'
C      character to delimit fields in probability tables, this makes things
C      easier to deal with with tbl. Added output of entropy at bottom of 
C      probability columns. Split cis-peptide table into inaccessible and 
C      accessible cis-peptide, increased MAXMAT, etc.
C 0.9  Added calculation of C and F parameters to allow easy inclusion
C      in tables.
C 1.0  Added thiol cyh as separate residue type
C
C Joined with Joy development
C
C ------------------------------------------------------------------------------
C
      INCLUDE 'summer.h'
C
C ----------------------------------------------------------------------------
C
      character*121 HEADER2
      character*101 HEADER, SPACER
      character*61 HEADER3
      character*80 TITLE(MAXMAT)
      character*60 TEMP2,TEMP3
      character*30 RESPON
      character*70 UNDER
      character*14 FILNAM(MAXFIL), INFIL, OUTFIL, datfil, options
      character*11 date
      character*10 DATASET(MAXFIL)
      character*5 buffer
      character*4 version
      character*1 RES(22), aatyp
      integer NMUT(NUMAA,22,MAXMAT,MAXFIL)
      integer NMUTALP(NUMAA,22),NMUTBET(NUMAA,22),NMUTPHI(NUMAA,22)
      integer NMUTCOI(NUMAA,22),NMUTACC(NUMAA,22),NMUTINACC(NUMAA,22)
      integer nmutaccalp(NUMAA,22),nmutinnalp(NUMAA,22)
      integer nmutaccbet(NUMAA,22),nmutinnbet(NUMAA,22)
      integer nmutaccphi(NUMAA,22),nmutinnphi(NUMAA,22)
      integer nmutacccoi(NUMAA,22),nmutinncoi(NUMAA,22)
      integer NMUTHELNCAP(NUMAA,22,9), NMUTHELCCAP(NUMAA,22,9)
      integer NMUTSHTNCAP(NUMAA,22,9), NMUTSHTCCAP(NUMAA,22,9)
      integer NMUTINNSSHB(NUMAA,22),NMUTINNSSSNHB(NUMAA,22)
      integer NMUTS(NUMAA,22),NMUTO(NUMAA,22),NMUTN(NUMAA,22)
      integer NMUTNOHB(NUMAA,22), NTOT(NUMAA,22,MAXMAT)
      integer NSUM(NUMAA,MAXMAT), NN(NUMAA,4,2,2,2,2), NNTOT(NUMAA,4,2,2,2,2)
      integer NTEMP(NUMAA), NCOMPTOT(NUMAA), NALPHA(NUMAA), NBETA(NUMAA)
      integer NPHI(NUMAA), NCOIL(NUMAA), ninnalpha(NUMAA), ninnbeta(NUMAA)
      integer ninNPHI(NUMAA),ninNCOIL(NUMAA), NACCESS(NUMAA), NSSHB(NUMAA)
      integer NMOHB(NUMAA), NMNHB(NUMAA), NINNSSHB(NUMAA), NINNMOHB(NUMAA)
      integer NINNMNHB(NUMAA), NMAT(MAXFIL), NSAMPtot, NSAMPalpha, NSAMPbeta
      integer NSAMPposphi, NSAMPother, NSAMPcoil, NSAMPaccess, NSAMPinaccess
      integer NCLASS1(NUMAA), NCLASS5(NUMAA), NCLASS10(NUMAA), NCLASS20(NUMAA)
      integer NCLASS50(NUMAA), NCLASS100(NUMAA), NCLASS200(NUMAA)
      integer NCLASS500(NUMAA), NCLASS1subst(NUMAA), NCLASS5subst(NUMAA)
      integer NCLASS10subst(NUMAA), NCLASS20subst(NUMAA), NCLASS50subst(NUMAA)
      integer NCLASS100subst(NUMAA), NCLASS200subst(NUMAA), NCLASS500subst(NUMAA)
      integer NZALP, NZACALP, NZINALP, NZBET, NZACBET, NZINBET
      integer NZPHI, NZACPHI, NZINPHI, NZCOI, NZACCOI, NZINCOI
      integer NZTOT(NUMAA,6)
      integer nperset(maxfil),
     -              nsumalp(NUMAA),
     -              nsumbet(NUMAA),
     -              nsumcoi(NUMAA),
     -              nsumphi(NUMAA),
     -              nsuminnalp(NUMAA),
     -              nsuminnbet(NUMAA),
     -              nsuminncoi(NUMAA),
     -              nsuminnphi(NUMAA),
     -              nsumaccalp(NUMAA),
     -              nsumaccbet(NUMAA),
     -              nsumacccoi(NUMAA),
     -              nsumaccphi(NUMAA),
     -              nsumacc(NUMAA),
     -              nsuminacc(NUMAA),
     -              nsums(NUMAA),
     -              nsumo(NUMAA),
     -              nsumn(NUMAA)
      integer       NSUMINNSSHB(NUMAA),NSUMINNSSSNHB(NUMAA)
      integer       NSUMNOHB(NUMAA),
     -              nseq(maxfil),nres(maxfil),
     -              npossible(NUMAA)
      real PTOT(NUMAA,22,MAXMAT),
     -     PNOTTOT(NUMAA,22,MAXMAT),
     -     PDIFF(NUMAA,22,MAXMAT),
     -     PDIFFERR(NUMAA,22,MAXMAT),
     -     PERR(NUMAA,22,MAXMAT),
     -     PNOTERR(NUMAA,22,MAXMAT),
     -     PSUM(NUMAA,MAXMAT)
      real PMUTALP(NUMAA,22)
      real PMUTBET(NUMAA,22)
      real PMUTPHI(NUMAA,22)
      real PMUTCOI(NUMAA,22)
      real PMUTACCALP(NUMAA,22)
      real PMUTACCBET(NUMAA,22)
      real PMUTACCPHI(NUMAA,22)
      real PMUTACCCOI(NUMAA,22)
      real PMUTINNALP(NUMAA,22)
      real PMUTINNBET(NUMAA,22)
      real PMUTINNPHI(NUMAA,22)
      real PMUTINNCOI(NUMAA,22)
      real PMUTACC(NUMAA,22)
      real PMUTINACC(NUMAA,22)
      real PMUTS(NUMAA,22)
      real PMUTO(NUMAA,22)
      real PMUTNOHB(NUMAA,22)
      real pcomptot(NUMAA)
      real PMUTNOTS(NUMAA,22)
      real PMUTNOTO(NUMAA,22)
      real PMUTNOTN(NUMAA,22)
      real PMUTN(NUMAA,22),
     -     PMUTNOTALP(NUMAA,22),
     -     PMUTNOTBET(NUMAA,22),
     -     PMUTNOTPHI(NUMAA,22),
     -     PMUTNOTCOI(NUMAA,22)
      real PERRALP(NUMAA,22)
      real PERRBET(NUMAA,22)
      real PERRPHI(NUMAA,22)
      real PERRCOI(NUMAA,22)
      real PERRACC(NUMAA,22)
      real PERRINACC(NUMAA,22)
      real PERRNOTS(NUMAA,22)
      real PERRNOTO(NUMAA,22)
      real PERRNOTN(NUMAA,22)
      real PERRS(NUMAA,22)
      real PERRO(NUMAA,22)
      real PERRN(NUMAA,22)
      real PERRNOTALP(NUMAA,22)
      real PERRNOTBET(NUMAA,22)
      real PERRNOTPHI(NUMAA,22)
      real PERRNOTCOI(NUMAA,22)
      real pperset(maxfil)
      real ERRPALPHA(22)
      real ERRPBETA(22)
      real ERRPPHI(22)
      real ERRPCOIL(22)
      real ERRPACC(22)
      real ERRPINACC(22)
      real ERRPSSHB(22)
      real ERRPMOHB(22)
      real ERRPMNHB(22)
      INTEGER       I,II,INDJBIG,INDJSMALL,INDKBIG,INDKSMALL,J,K,L,M,N,NACC,
     -              NCLASSTOT1,NCLASSTOT10,NCLASSTOT100,NCLASSTOT100SUBST,
     -              NCLASSTOT10SUBST,NCLASSTOT1SUBST,NCLASSTOT20,NCLASSTOT200,
     -              NCLASSTOT200SUBST,NCLASSTOT20SUBST,NCLASSTOT5,NCLASSTOT50,
     -              NCLASSTOT500,NCLASSTOT500SUBST,NCLASSTOT50SUBST,
     -              NCLASSTOT5SUBST,NCONSIDERED,NFIL,NIND,NM,NOPT,NPOS,
     -              NSAMPSNHB,NSAMPSOHB,NSAMPSSHB,
     -              NSEC,NSN,NSO,NSS,NTOTRES,NTOTSEQ,NTOTSUBST,NUMJOYMAT
      REAL          SMALL,BIG,ENTROPY,P
      LOGICAL       HBPOSSMO(NUMAA),
     -              HBPOSSMN(NUMAA),
     -              HBPOSSSS(NUMAA),
     -              OUTERR,
     -              OUTPROB,
     -              DODIFF,
     -              exist,
     -              graph
C
      integer       NTOTALPHA,NTOTBETA,NTOTPHI,NTOTCOIL,NTOTACC,NTOTINACC
      integer       NTOTSS,NTOTCO,NTOTNH
      integer       NPOSSSS,NPOSSCO,NPOSSNH
      integer       NSAMPINNALPHA,NSAMPINNBETA,NSAMPINNPHI,NSAMPINNCOIL
      real          PTOTALPHA,PTOTBETA,PTOTPHI,PTOTCOIL,PTOTACC,PTOTINACC
      real          PTOTSS,PTOTCO,PTOTNH
      real          PALPHA(NUMAA),
     -              PBETA(NUMAA),
     -              PPHI(NUMAA),
     -              PCOIL(NUMAA),
     -              PACC(NUMAA),
     -              PINACC(NUMAA)
      real          PSS(NUMAA),
     -              PNH(NUMAA),
     -              PCO(NUMAA)
      real          CFALPHA(NUMAA),
     -              CFBETA(NUMAA),
     -              CFPHI(NUMAA),
     -              CFCOIL(NUMAA),
     -              CFACC(NUMAA),
     -              CFINACC(NUMAA)
      real          CFSS(NUMAA),
     -              CFCO(NUMAA),
     -              CFNH(NUMAA)
      real          RSAMPTOT
C
C ----------------------------------------------------------------------------
C
      DATA UNDER    /'______________________________________________________________________'/
      DATA HEADER   /'A    C    D    E    F    G    H    I    K    L    M    N    P    Q    R    S    T    V    W    Y    J'/
      DATA HEADER3  /'A  C  D  E  F  G  H  I  K  L  M  N  P  Q  R  S  T  V  W  Y  J'/
      DATA SPACER   /'-----------------------------------------------------------------------------------------------------'/
      DATA HEADER2  /'A    C    D    E    F    G    H    I    K    L    M    N    P    Q    R    S    T    V    W    Y    J'/
      DATA RES      /'A','C','D','E','F','G','H','I','K','L',
     -               'M','N','P','Q','R','S','T','V','W','Y','J','-'/
      DATA NPOSSIBLE / 8, 8,64,64, 8, 8,64, 8,32, 8,
     -                32,64, 8,64,32,64,64, 8,32,64,64/
      DATA HBPOSSMO /.FALSE.,.FALSE., .TRUE., .TRUE.,.FALSE.,
     -               .FALSE., .TRUE.,.FALSE., .TRUE.,.FALSE.,
     -               .FALSE., .TRUE.,.FALSE., .TRUE., .TRUE.,
     -                .TRUE., .TRUE.,.FALSE., .TRUE., .TRUE.,.TRUE./
      DATA HBPOSSMN /.FALSE.,.FALSE., .TRUE., .TRUE.,.FALSE.,
     -               .FALSE., .TRUE.,.FALSE.,.FALSE.,.FALSE.,
     -                .TRUE., .TRUE.,.FALSE., .TRUE.,.FALSE.,
     -                .TRUE., .TRUE.,.FALSE.,.FALSE., .TRUE.,.TRUE./
      DATA HBPOSSSS /.FALSE.,.FALSE., .TRUE., .TRUE.,.FALSE.,
     -               .FALSE., .TRUE.,.FALSE., .TRUE.,.FALSE.,
     -                .TRUE., .TRUE.,.FALSE., .TRUE., .TRUE.,
     -                .TRUE., .TRUE.,.FALSE., .TRUE., .TRUE.,.TRUE./
      DATA OUTERR  /.FALSE./
      DATA OUTPROB /.FALSE./
      DATA DODIFF  /.FALSE./
      DATA GRAPH   /.FALSE./
      DATA SMALL   /1.E-15/
C
C 103 is for capping tables, and 67 is for non-capping data files. This is
C the default (67)
C
c     DATA NUMJOYMAT /103/
      DATA NUMJOYMAT /67/
C
      DATA DATE    /'07-JAN-1992'/
      DATA VERSION /'1.0c'/
C
      do J=1,6
        do I=1,NUMAA
          NZTOT(I,J)=0
        end do
      end do
C
      nconsidered=0
      do i=1,NUMAA
        nconsidered=nconsidered+npossible(i)
      end do
C
C -----------------------------------------------------------------------------
C
C Get options etc
C
      nopt=0
      call getarg(1,options)
      if (index(options,'.').gt.0) then
        infil=options 
        goto 12
      end if
      if (index(options,'v').gt.0) then
        nopt=nopt+1
        write (6,'(''summer - version '',A)') version
      end if
      if (index(options,'h').gt.0) then
        nopt=nopt+1
        write (6,'(''summer -- an analysis program for joy'')')
        write (6,'(''options : d - output differences'')')
        write (6,'(''          e - output error terms'')')
        write (6,'(''          h - help'')')
        write (6,'(''          p - output probabilities'')')
        write (6,'(''          s - ignore sulphur H-bonds'')')
        write (6,'(''          v - version'')')
        write (6,'(''usage: summer -deghpsv <list.file>'')')
        call exit(2)
      end if
      if (index(options,'g').gt.0) then    
        nopt=nopt+1
        graph=.true.
      end if
      if (index(options,'d').gt.0) then    
        nopt=nopt+1
        dodiff=.true.
      end if
      if (index(options,'e').gt.0) then
        nopt=nopt+1
        outerr=.true.
      end if
      if (index(options,'p').gt.0) then
        nopt=nopt+1
        outprob=.true.
      end if
      if (index(options,'-').gt.0) then
        goto 11
      else
        write (6,'(''summer: no file name in command line'')')
        write (6,'(''usage: summer -deghpv <list.file>'')')
        call exit(1)
      end if
C
11    continue
      call getarg(2,infil)
      if (index(infil,'.').eq.0) then
        write (6,'(''summer: no file name in command line'')')
        write (6,'(''usage: summer -deghpv <list.file>'')')
        call exit(1)
      end if
12    inquire (file=infil,exist=exist)
      if (.not.exist) then
	write (6,'(''summer: list file does not exist '')') 
	call exit(1)
      end if
C
      NPOS=INDEX(INFIL,'.')
      OUTFIL(1:NPOS)=INFIL(1:NPOS)
      DATFIL(1:NPOS)=INFIL(1:NPOS)
      OUTFIL(NPOS+1:NPOS+3)='tot'
      DATFIL(NPOS+1:NPOS+3)='dat'
C
C -----------------------------------------------------------------------------
C
C Inititalizations
C
      NPOSSSS=0
      NPOSSCO=0
      NPOSSNH=0
      NTOTALPHA=0
      NTOTBETA=0
      NTOTPHI=0
      NTOTCOIL=0
      NTOTACC=0
      NTOTSS=0
      NTOTCO=0
      NTOTNH=0
      NSAMPinnalpha=0
      NSAMPinnbeta=0
      NSAMPinnphi=0
      NSAMPinncoil=0
      NCLASSTOT1SUBST=0
      NCLASSTOT5SUBST=0
      NCLASSTOT10SUBST=0
      NCLASSTOT20SUBST=0
      NCLASSTOT50SUBST=0
      NCLASSTOT100SUBST=0
      NCLASSTOT200SUBST=0
      NCLASSTOT500SUBST=0
      NCLASStot1=0
      NCLASStot5=0
      NCLASStot10=0
      NCLASStot20=0
      NCLASStot50=0
      NCLASStot100=0
      NCLASStot200=0 
      NCLASStot500=0
      ntotres=0
      ntotseq=0
      do i=1,maxfil
        nseq(i)=0
        nres(i)=0
      end do
      do i=1,NUMAA
        nalpha(i)=0
        nbeta(i)=0
        NPHI(i)=0
        NCOIL(i)=0
        ninnalpha(i)=0
        ninnbeta(i)=0
        ninNPHI(i)=0
        ninNCOIL(i)=0
        NSUMACCALP(i)=0
        NSUMINNALP(i)=0
        NSUMACCBET(i)=0
        NSUMINNBET(i)=0
        NSUMACCPHI(i)=0
        NSUMINNPHI(i)=0
        NSUMACCCOI(i)=0
        NSUMINNCOI(i)=0
        NSUMALP(i)=0
        NSUMBET(i)=0
        NSUMCOI(i)=0
        NSUMPHI(i)=0
        nsumacc(i)=0
        nsuminacc(i)=0
        NSUMINNSSHB(I)=0
        NSUMINNSSSNHB(I)=0
        nsums(i)=0
        nsumo(i)=0
        nsumn(i)=0
        NSUMNOHB(I)=0
        NCOMPTOT(i)=0
        NCLASS1(i)=0
        NCLASS5(i)=0
        NCLASS10(i)=0
        NCLASS20(i)=0
        NCLASS50(i)=0
        NCLASS100(i)=0
        NCLASS200(i)=0
        NCLASS500(i)=0
        NCLASS1subst(i)=0
        NCLASS5subst(i)=0
        NCLASS10subst(i)=0
        NCLASS20subst(i)=0
        NCLASS50subst(i)=0
        NCLASS100subst(i)=0
        NCLASS200subst(i)=0
        NCLASS500subst(i)=0
      end do
      NSAMPtot=0
      NSAMPalpha=0
      NSAMPbeta=0
      NSAMPposphi=0
      NSAMPcoil=0
      NSAMPother=0
      NSAMPaccess=0
      NSAMPinaccess=0
      NSAMPsshb=0
      NSAMPsohb=0
      NSAMPsnhb=0
      do M=1,MAXMAT
        do I=1,NUMAA
          NSUM(I,M)=0
          PSUM(I,M)=0.0
          do J=1,22
            NTOT(I,J,M)=0
            PDIFF(I,J,M)=0.0
            PDIFFERR(I,J,M)=0.0
          end do
        end do
      end do
      do n=1,2
        do m=1,2
          do l=1,2
            do k=1,2
              do j=1,4
                do i=1,NUMAA
                  NNTOT(I,J,K,L,M,N)=0
                end do
              end do
            end do
          end do
        end do
      end do
      do I=1,22
        do J=1,NUMAA
          nmutaccalp(j,i)=0
          nmutinnalp(j,i)=0
          nmutaccbet(j,i)=0
          nmutinnbet(j,i)=0
          nmutaccphi(j,i)=0
          nmutinnphi(j,i)=0
          nmutacccoi(j,i)=0
          nmutinncoi(j,i)=0
          NMUTINNSSHB(J,I)=0
          NMUTINNSSSNHB(J,I)=0
          NMUTALP(J,I)=0
          NMUTBET(J,I)=0
          NMUTPHI(J,I)=0
          NMUTCOI(J,I)=0
          NMUTACC(J,I)=0
          NMUTINACC(J,I)=0
          NMUTS(J,I)=0
          NMUTO(J,I)=0
          NMUTN(J,I)=0
          NMUTNOHB(J,I)=0
        end do
      end do
C
C -----------------------------------------------------------------------------
C
C Get list of .sum file names
C
      call READLIS(INFIL,FILNAM,NFIL)
C
c     write (6,'(/,''processing files'')')
c     write (6,'(A)') UNDER(1:NFIL)
C
C ------------------------------------------------------------------------------
C
C Process each file
C
      do I=1,NFIL
        write (6,'(''*'',$)')
        open (UNIT=1,FILE=FILNAM(I),FORM='FORMATTED',STATUS='OLD',
     -        CARRIAGECONTROL='LIST',err=8)
        read (1,'(2x)')
        read (1,'(20X,A)',err=34) DATASET(I)
c       write (6,*) ' dataset = ',dataset(i)
        read (1,'(20x,i4)',err=34) nseq(i)
c       write (6,*) ' number of sequences = ',nseq(i)
        read (1,'(20x,i4)',err=34) nres(i)
c       write (6,*) ' number of residues = ',nres(i)
	READ (1,'(2x)',err=34)
c       write (6,*) ' number of amino acids = ',numaa
        do II=1,NUMAA
c         write (6,'(I3,$)') II
	  read (1,'(17X,A1)',err=34) AATYP
c         write (6,'(A1,$)') AATYP
          read (1,'(8(8I4,/))',err=34) (((((NN(II,J,K,L,M,N),J=1,4),K=1,2),
     -                               L=1,2),M=1,2),N=1,2)
	  read (1,'(2x)')
        end do
c       write (6,'('' '')')
C
C read in the mutation matrices
C Format is:
C Blank line, Title, Blank line, NUMAA * Data, etc.
C
C changed from read till end to read until limit
C
        K=1
c       write (6,*) ' reading in matrices '
        do K=1,NUMJOYMAT
c5         read (1,'(A,/,A,/,A)',END=6,err=34) TEMP2,TITLE(K),TEMP3
          read (1,'(A,/,A,/,A)',END=6,err=34) TEMP2,TITLE(K),TEMP3
          do J=1,22
            read (1,'(6X,21I4)',END=6,ERR=7) (NMUT(M,J,K,I),M=1,NUMAA)
          end do
c         K=K+1
c         GOTO 5
          end do
6       CONTINUE
        NMAT(I)=K-1
        if (nmat(i).gt.maxmat) then
          write (6,'(''summer: too many tables -- increase MAXMAT'')')
          call exit(2)
        end if
        if (NMAT(I).NE.NUMJOYMAT) then
          write (6,'(''summer: non-standard joy .sub file read '',A)') FILNAM(I)
        end if
        close (UNIT=1)
        ntotseq=ntotseq+nseq(i)
        ntotres=ntotres+nres(i)
C
C Sum the environments to produce totals for some important classes of 
C environment
C
        do J=1,2
          do K=1,2
            do L=1,2
              do M=1,2
                do N=1,4
                  do II=1,NUMAA
                    NNTOT(II,N,M,L,K,J)=NNTOT(II,N,M,L,K,J)+NN(II,N,M,L,K,J)
                    NCOMPTOT(II)=NCOMPTOT(II)+NN(II,N,M,L,K,J)
                  end do
                end do
              end do
            end do
          end do
        end do
        do II=1,NUMAA
          do J=1,2
            do K=1,2
              do L=1,2
                NINNALPHA(II)=NINNALPHA(II)+ NN(II,1,2,L,K,J)
                NINNBETA(II) =NINNBETA(II) + NN(II,2,2,L,K,J)
                NINNPHI(II)  =NINNPHI(II)  + NN(II,3,2,L,K,J)
                NINNCOIL(II) =NINNCOIL(II) + NN(II,4,2,L,K,J)
                do M=1,2
                  NALPHA(II) =NALPHA(II) + NN(II,1,M,L,K,J)
                  NBETA(II)  =NBETA(II)  + NN(II,2,M,L,K,J)
                  NPHI(II)   =NPHI(II)   + NN(II,3,M,L,K,J)
                  NCOIL(II)  =NCOIL(II)  + NN(II,4,M,L,K,J)
                end do
              end do
            end do
          end do
          do J=1,4
            do K=1,2
              do L=1,2
                do M=1,2
                  NACCESS(II)=NACCESS(II)+NN(II,J,1,M,L,K)
                end do
              end do
            end do
            if (HBPOSSSS(II)) then
              do K=1,2
                do L=1,2
                  do M=1,2
                    NSSHB(II)=NSSHB(II)+NN(II,J,M,1,L,K)
                  end do
                  NINNSSHB(II)=NINNSSHB(II)+NN(II,J,2,1,L,K)
                end do
              end do
            end if
            if (HBPOSSMO(II)) then
              do K=1,2
                do L=1,2
                  do M=1,2
                    NMOHB(II)=NMOHB(II)+NN(II,J,M,L,1,K)
                  end do
                  NINNMOHB(II)=NINNMOHB(II)+NN(II,J,2,L,1,K)
                end do
              end do
            end if
            if (HBPOSSMN(II)) then
              do K=1,2
                do L=1,2
                  do M=1,2
                    NMNHB(II)=NMNHB(II)+NN(II,J,M,L,K,1)
                  end do
                  NINNMNHB(II)=NINNMNHB(II)+NN(II,J,2,L,K,1)
                end do
              end do
            end if 
          end do
        end do
        if (I.GT.1) then
          if (NMAT(I).NE.NMAT(1)) then
            write (6,'(''summer: inconsistent number of tables in '',A)') 
     -             FILNAM(I)
            call exit(1)
          end if
        end if
      end do
c     write (6,'('' '')')
C
C ----------------------------------------------------------------------------
C
C Find the total number of occupied classes, for each amino acid type, and
C also for all classes
C
      do j=1,NUMAA
        do k=1,4
          do l=1,2
            do m=1,2
              do n=1,2
                do nm=1,2
                  if (nntot(j,k,l,m,n,nm).ge.1) then
                    NCLASS1(j)=NCLASS1(j)+1
                    if (nntot(j,k,l,m,n,nm).ge.5) then
                      NCLASS5(j)=NCLASS5(j)+1
                      if (nntot(j,k,l,m,n,nm).ge.10) then
                        NCLASS10(j)=NCLASS10(j)+1
                        if (nntot(j,k,l,m,n,nm).ge.20) then
                          NCLASS20(j)=NCLASS20(j)+1
                          if (nntot(j,k,l,m,n,nm).ge.50) then
                            NCLASS50(j)=NCLASS50(j)+1
                            if (nntot(j,k,l,m,n,nm).ge.100) then
                              NCLASS100(j)=NCLASS100(j)+1
                              if (nntot(j,k,l,m,n,nm).ge.200) then
                                NCLASS200(j)=NCLASS200(j)+1
                                if (nntot(j,k,l,m,n,nm).ge.500) then
                                  NCLASS500(j)=NCLASS500(j)+1
                                end if
                              end if
                            end if
                          end if
                        end if
                      end if
                    end if
                  end if
                end do
              end do
            end do
          end do
        end do
        NCLASStot1=NCLASStot1+NCLASS1(j)
        NCLASStot5=NCLASStot5+NCLASS5(j)
        NCLASStot10=NCLASStot10+NCLASS10(j)
        NCLASStot20=NCLASStot20+NCLASS20(j)
        NCLASStot50=NCLASStot50+NCLASS50(j)
        NCLASStot100=NCLASStot100+NCLASS100(j)
        NCLASStot200=NCLASStot200+NCLASS200(j)
        NCLASStot500=NCLASStot500+NCLASS500(j)
      end do
C
C -----------------------------------------------------------------------------
C
C Sum all the matrices into one big one
C
      ntotsubst=0
      do I=1,NFIL
        NPERSET(I)=0
        do J=1,22
          do K=1,NUMAA
            NPERSET(I)=NPERSET(I)+NMUT(K,J,1,i)
          end do
        end do
        ntotsubst=ntotsubst+nperset(i)
        do M=1,NMAT(1)
          do J=1,22
            do K=1,NUMAA
              NTOT(K,J,M)=NTOT(K,J,M)+NMUT(K,J,M,I)
            end do
          end do
        end do
      end do
C
C Find sum of columns for each matrix
C
      do M=1,NMAT(1)
        do I=1,NUMAA
          do J=1,22
            NSUM(I,M)=NSUM(I,M)+NTOT(I,J,M)
          end do
        end do
      end do
C
C find the number of occupied columns
C
      do j=1,NUMAA
        do i=2,NMAT(1)-2
          if (NSUM(j,i).ge.1) then
            NCLASS1subst(j)=NCLASS1subst(j)+1
            if (NSUM(j,i).ge.5) then
              NCLASS5subst(j)=NCLASS5subst(j)+1
              if (NSUM(j,i).ge.10) then
                NCLASS10subst(j)=NCLASS10subst(j)+1
                if (NSUM(j,i).ge.20) then
                  NCLASS20subst(j)=NCLASS20subst(j)+1
                  if (NSUM(j,i).ge.50) then
                    NCLASS50subst(j)=NCLASS50subst(j)+1
                    if (NSUM(j,i).ge.100) then
                      NCLASS100subst(j)=NCLASS100subst(j)+1
                      if (NSUM(j,i).ge.200) then
                        NCLASS200subst(j)=NCLASS200subst(j)+1
                        if (NSUM(j,i).ge.500) then
                          NCLASS500subst(j)=NCLASS500subst(j)+1
                        end if
                      end if
                    end if
                  end if
                end if
              end if
            end if
          end if
        end do
        NCLASStot1SUBST=NCLASStot1SUBST+NCLASS1SUBST(j)
        NCLASStot5SUBST=NCLASStot5SUBST+NCLASS5SUBST(j)
        NCLASStot10SUBST=NCLASStot10SUBST+NCLASS10SUBST(j)
        NCLASStot20SUBST=NCLASStot20SUBST+NCLASS20SUBST(j)
        NCLASStot50SUBST=NCLASStot50SUBST+NCLASS50SUBST(j)
        NCLASStot100SUBST=NCLASStot100SUBST+NCLASS100SUBST(j)
        NCLASStot200SUBST=NCLASStot200SUBST+NCLASS200SUBST(j)
        NCLASStot500SUBST=NCLASStot500SUBST+NCLASS500SUBST(j)
      end do
C
C Find percentage contribution of each dataset
C
      do I=1,NFIL
        PPERSET(I)=(real(NPERSET(I))/real(ntotsubst))*100.0
      end do
C
C ------------------------------------------------------------------------------
C
      open (UNIT=3,FILE=OUTFIL,FORM='FORMATTED',STATUS='unknown')
      write (3,'(''produced by summer, version '',A,4X,A,/)') version,date
C
      if (graph) then
        open (UNIT=4,FILE=datFIL,FORM='FORMATTED',STATUS='unknown')
      end if
C
C ------------------------------------------------------------------------------
C
      write (3,'(26X,''SUMMED SUBSTITUTION TABLES'',//)')
      write (3,'(29X,''Data from datasets'',//)')
      write (3,'(11x,''set  sequences residues substitutions   (% of total)'')')
      do I=1,NFIL
        write (3,'(7X,I3,1X,A10,I4,5X,I4,8X,I6,9X,F6.2)') 
     -         I,dataset(i),nseq(i),nres(i),NPERSET(I),PPERSET(I)
      end do
      write (3,'(11X,''----------------------------------------------------'')')
      write (3,'(7x,''total'',8x,i5,4x,i5,6x,i8)') ntotseq,ntotres,ntotsubst
C
C ------------------------------------------------------------------------------
C
      write (3,'(//,31X,''ENVIRONMENT TABLES'',/)')
      do I=1,NUMAA
        write (3,'(17X,A,/,
     -         ''  -- Accessible-- --Inaccessible-'',/,
     -         ''   a   b   +   o   a   b   +   o  H-bonds'')') RES(I)
        if (hbpossmn(i)) then
          if (hbpossmo(i)) then
            write (3,'(8I4,3X,''S O N'')')  ((NNTOT(I,J,L,1,1,1),J=1,4),L=1,2)
            write (3,'(8I4,3X,''  O N'')')  ((NNTOT(I,J,L,2,1,1),J=1,4),L=1,2)
          end if
          write (3,'(8I4,3X,''S   N'')')  ((NNTOT(I,J,L,1,2,1),J=1,4),L=1,2)
          write (3,'(8I4,3X,''    N'')')  ((NNTOT(I,J,L,2,2,1),J=1,4),L=1,2)
        end if
        if (hbpossss(i)) then
          if (hbpossmo(i)) then
            write (3,'(8I4,3X,''S O  '')')  ((NNTOT(I,J,L,1,1,2),J=1,4),L=1,2)
            write (3,'(8I4,3X,''  O  '')')  ((NNTOT(I,J,L,2,1,2),J=1,4),L=1,2)
          end if
          write (3,'(8I4,3X,''S    '')')  ((NNTOT(I,J,L,1,2,2),J=1,4),L=1,2)
        end if
        write (3,'(8I4,3X,''    '',/)') ((NNTOT(I,J,L,2,2,2),J=1,4),L=1,2)
C
        write (3,'(A,'':Total number  '',21X,I4,/)') RES(I),NCOMPTOT(I)
        write (3,'(A,'':Alpha         '',21X,I4,4X,
     -             ''(inaccessible) '',I4)') RES(I),NALPHA(I),NINNALPHA(I)
        write (3,'(A,'':Beta          '',21X,I4,4X,
     -             ''(inaccessible) '',I4)') RES(I),NBETA(I),NINNBETA(I)
        write (3,'(A,'':Positive phi  '',21X,I4,4X,
     -             ''(inaccessible) '',I4)') RES(I),NPHI(I),NINNPHI(I)
        write (3,'(A,'':Other         '',21X,I4,4X,
     -             ''(inaccessible) '',I4)') RES(I),NCOIL(I),NINNCOIL(I)
        write (3,'(''  '')')
        write (3,'(A,'':Accessible    '',21X,I4)') RES(I),NACCESS(I)
        write (3,'(A,'':Inaccessible  '',21X,I4,/)') RES(I),NCOMPTOT(I)-NACCESS(I)
C
        if (HBPOSSSS(I)) then
          write (3,'(A,'':Sidechain-sidechain                '',I4,4X,
     -               ''(inaccessible) '',I4)') RES(I),NSSHB(I),NINNSSHB(I)
        end if
        if (HBPOSSMO(I)) then
          write (3,'(A,'':Sidechain-mainchain O              '',I4,4X,
     -               ''(inaccessible) '',I4)') RES(I),NMOHB(I),NINNMOHB(I)
        end if
        if (HBPOSSMN(I)) then
          write (3,'(A,'':Sidechain-mainchain N              '',I4,4X,
     -               ''(inaccessible) '',I4)') RES(I),NMNHB(I),NINNMNHB(I)
        end if
        write (3,'(/)')
      end do
C
C ------------------------------------------------------------------------------
C
C Calculate and output total sample statistics
C
      do i=1,NUMAA
        NSAMPtot=NSAMPtot+ncomptot(i)
        NSAMPalpha=NSAMPalpha+nalpha(i)
        NSAMPbeta=NSAMPbeta+nbeta(i)
        NSAMPposphi=NSAMPposphi+NPHI(i)
        NSAMPaccess=NSAMPaccess+naccess(i)
        NSAMPsshb=NSAMPsshb+nsshb(i)
        NSAMPsohb=NSAMPsohb+nmohb(i)
        NSAMPsnhb=NSAMPsnhb+nmnhb(i)
      end do
      NSAMPcoil=NSAMPtot-NSAMPalpha-NSAMPbeta-NSAMPposphi
      NSAMPinaccess=NSAMPtot-NSAMPaccess
C
      write (3,'(''  '')')
      write (3,'(/,''Number of occupied bins (residue counts)'',/)')
      write (3,'(''   number  >0     >5    >10    >20    >50   >100   '',
     -       ''>200   >500   poss.'')')
      do i=1,NUMAA
        write (3,'(5X,A,8(2X,I5),2x,i5)') res(i),NCLASS1(i),NCLASS5(i),
     -         NCLASS10(i),NCLASS20(i),NCLASS50(i),NCLASS100(i),
     -         NCLASS200(i),NCLASS500(i),npossible(i)
      end do
      write (3,'(8X,''--------------------------------------------'',
     -       ''-----------------'')')
      write (3,'('' Total'',8(2X,I5),2x,i5)') NCLASStot1,NCLASStot5,NCLASStot10,
     -       NCLASStot20,NCLASStot50,NCLASStot100,NCLASStot200,NCLASStot500,
     -       nconsidered
      write (3,'(''   '')')
C
      write (3,'(/,''Number of occupied bins (substitution counts)'',/)')
      write (3,'(''   number  >0     >5    >10    >20    >50   >100   '',
     -       ''>200   >500'')')
      do i=1,NUMAA
        write (3,'(5X,A,8(2X,I5))') res(i),NCLASS1subst(i),NCLASS5subst(i),
     -         NCLASS10subst(i),NCLASS20subst(i),NCLASS50subst(i),
     -         NCLASS100subst(i),NCLASS200subst(i),NCLASS500subst(i)
      end do
      write (3,'(8X,''------------------------------------------------------'')')
      write (3,'('' Total'',8(2X,I5))') NCLASStot1subst,NCLASStot5subst,
     -       NCLASStot10subst,NCLASStot20subst,NCLASStot50subst,
     -       NCLASStot100subst,NCLASStot200subst,NCLASStot500subst
      write (3,'(''   '')')
C
C ----------------------------------------------------------------------------
C
C New section for table of residue counts
C
      write (3,'(32x,''RESIDUE SAMPLE SUMMARY'',/)')
      write (3,'(6x,''total   %   alpha   beta  + phi   coil'',
     -       '' access  inacc   sshb   sohb   snhb'')')
      do i=1,NUMAA
        pcomptot(i)=(real(ncomptot(i))/real(NSAMPtot))*100.
        write (3,'(3x,a1,2x,i5,f5.1,9(2x,i5))') 
     -         res(i),ncomptot(i),pcomptot(i),nalpha(i),nbeta(i),
     -         NPHI(i),NCOIL(i),naccess(i),(ncomptot(i)-naccess(i)),nsshb(i),
     -         nmohb(i),nmnhb(i)
      end do
      write (3,'(6X,''-------------------------------------------------'',
     -       ''-------------------'')')
      write (3,'(''total '',I5,5x,9(2x,I5))') NSAMPtot,NSAMPalpha,NSAMPbeta,
     -       NSAMPposphi,NSAMPcoil,NSAMPaccess,NSAMPinaccess,NSAMPsshb,
     -       NSAMPsohb,NSAMPsnhb
      write (3,'(//)')
C
C --------------------------------------------------------------------------
C
      write (3,'(//,18x,''ACCESSIBILITY AS A FUNCTION OF 2 STRUCTURE'')')
C
      write (3,'(//)')
      write (3,'(9x,''Ntot  --- alpha ---  ---- beta ---  ---- +phi ---  ---- coil ---'')')
      write (3,'(15x,4(''Tot  Acc  Inn  ''))')
      do i=1,NUMAA
        NSAMPinnalpha=NSAMPinnalpha+ninnalpha(i)
        NSAMPinnbeta=NSAMPinnbeta+ninnbeta(i)
        NSAMPinnphi=NSAMPinnphi+ninnphi(i)
        NSAMPinncoil=NSAMPinncoil+ninncoil(i)
        write (3,'(5x,1x,a,1x,I5,1x,4(i4,1x,i4,1x,i4,1x))') 
     -         res(i),ncomptot(i),
     -         nalpha(i),nalpha(i)-ninnalpha(i),ninnalpha(i),
     -         nbeta(i),nbeta(i)-ninnbeta(i),ninnbeta(i),
     -         nphi(i),nphi(i)-ninnphi(i),ninnphi(i),
     -         ncoil(i),ncoil(i)-ninncoil(i),ninncoil(i)
      end do
      write (3,'('' ------------------------------------------------------------------------'')')
      write (3,'(''  Total'',1x,i5,4(i5,i5,i5))') 
     -       NSAMPtot,
     -       NSAMPalpha,NSAMPalpha-NSAMPinnalpha,NSAMPinnalpha,
     -       NSAMPbeta,NSAMPbeta-NSAMPinnbeta,NSAMPinnbeta,
     -       NSAMPposphi,NSAMPposphi-NSAMPinnphi,NSAMPinnphi,
     -       NSAMPcoil,NSAMPcoil-NSAMPinncoil,NSAMPinncoil
      write (3,'(//)')
c
      write (3,'('' class   Ntot  Inn    f'')')
      write (3,'('' alpha :'',I5,I5,2x,F5.3)') NSAMPalpha,NSAMPinnalpha,real(NSAMPinnalpha)/real(NSAMPalpha)
      write (3,'('' beta  :'',I5,I5,2x,F5.3)') NSAMPbeta,NSAMPinnbeta,real(NSAMPinnbeta)/real(NSAMPbeta)
      write (3,'('' phi   :'',I5,I5,2x,F5.3)') NSAMPposphi,NSAMPinnphi,real(NSAMPinnphi)/real(NSAMPposphi)
      write (3,'('' coil  :'',I5,I5,2x,F5.3)') NSAMPcoil,NSAMPinncoil,real(NSAMPinncoil)/real(NSAMPcoil)
      write (3,'(''-------------------------'')')
      write (3,'('' total :'',I5,I5,2x,f5.3)') NSAMPtot,NSAMPinaccess,real(NSAMPinaccess)/real(NSAMPtot)
      write (3,'(//)')
C
C --------------------------------------------------------------------------
C
C Calculate the C+F parameters
C
C Calculate the number of alpha, beta residues
C
      do I=1,NUMAA
        if (hbpossss(i)) then
          npossss=npossss+ncomptot(i)
        end if
        if (hbpossmo(i)) then
          npossco=npossco+ncomptot(i)
        end if
        if (hbpossmn(i)) then
          npossnh=npossnh+ncomptot(i)
        end if
        NTOTALPHA=NTOTALPHA+NALPHA(I)
        NTOTBETA=NTOTBETA+NBETA(I)
        NTOTPHI=NTOTPHI+NPHI(I)
        NTOTCOIL=NTOTCOIL+NCOIL(I)
        NTOTACC=NTOTACC+NACCESS(I)
        NTOTSS=NTOTSS+NSSHB(I)
        NTOTCO=NTOTCO+NMOHB(I)
        NTOTNH=NTOTNH+NMNHB(I)
      end do
      NTOTINACC=NSAMPTOT-NTOTACC
C
      write (6,*) 'sample contains ',NSAMPtot,' residues'
      RSAMPTOT=REAL(NSAMPTOT)
c
      PTOTBETA=REAL(NTOTBETA)/REAL(NSAMPTOT)
      PTOTPHI=REAL(NTOTPHI)/REAL(NSAMPTOT)
      PTOTCOIL=REAL(NTOTCOIL)/REAL(NSAMPTOT)
      PTOTALPHA=REAL(NTOTALPHA)/REAL(NSAMPTOT)
      PTOTACC=REAL(NTOTACC)/REAL(NSAMPTOT)
      PTOTINACC=REAL(NTOTINACC)/REAL(NSAMPTOT)
      PTOTSS=REAL(NTOTSS)/real(npossss)
      PTOTCO=REAL(NTOTCO)/real(npossco)
      PTOTNH=REAL(NTOTNH)/real(npossnh)
C
c      write (6,*) ' calculating C+F param for '
C
C Calculate parameters for each AA
C
      do i=1,NUMAA
C
        PALPHA(I)=REAL(NALPHA(I))/(REAL(NCOMPTOT(I))+SMALL)
        ERRPALPHA(I)=SQRT((REAL(NALPHA(I))*(REAL(NCOMPTOT(I)-NALPHA(I))))/((REAL(NCOMPTOT(I))**3)+SMALL))
        PBETA(I)=REAL(NBETA(I))/(REAL(NCOMPTOT(I))+SMALL)
        ERRPBETA(I)=SQRT((REAL(NBETA(I))*(REAL(NCOMPTOT(I)-NBETA(I))))/((REAL(NCOMPTOT(I))**3)+SMALL))
        PPHI(I)=REAL(NPHI(I))/(REAL(NCOMPTOT(I))+SMALL)
        ERRPPHI(I)= SQRT((REAL(NPHI(I))* (REAL(NCOMPTOT(I)-NPHI(I))))/((REAL(NCOMPTOT(I))**3)+SMALL))
        PCOIL(I)=REAL(NCOIL(I))/(REAL(NCOMPTOT(I))+SMALL)
        ERRPCOIL(I)=SQRT((REAL(NCOIL(I))*(REAL(NCOMPTOT(I)-NCOIL(I))))/((REAL(NCOMPTOT(I))**3)+SMALL))

        PACC(I)=REAL(NACCESS(I))/(REAL(NCOMPTOT(I))+SMALL)
        ERRPACC(I)=SQRT((REAL(NACCESS(I))*(REAL(NCOMPTOT(I)-NACCESS(I))))/((REAL(NCOMPTOT(I))**3)+SMALL))
        PINACC(I)=REAL(NCOMPTOT(I)-NACCESS(I))/(REAL(NCOMPTOT(I))+SMALL)
        ERRPINACC(I)=SQRT((REAL((NCOMPTOT(I)-NACCESS(I)))*(REAL(NACCESS(I))))/((REAL(NCOMPTOT(I))**3)+SMALL))

        if (HBPOSSSS(I)) then
          PSS(I)=REAL(NSSHB(I))/(REAL(NCOMPTOT(I))+SMALL)
          ERRPSSHB(I)=SQRT((REAL(NSSHB(I))*(REAL(NCOMPTOT(I)-NSSHB(I))))/((REAL(NCOMPTOT(I))**3)+SMALL))
          PCO(I)=REAL(NMOHB(I))/(REAL(NCOMPTOT(I))+SMALL)
          ERRPMOHB(I)=SQRT((REAL(NMOHB(I))*(REAL(NCOMPTOT(I)-NMOHB(I))))/((REAL(NCOMPTOT(I))**3)+SMALL))
          PNH(I)=REAL(NMNHB(I))/(REAL(NCOMPTOT(I))+SMALL)
          ERRPMNHB(I)=SQRT((REAL(NMNHB(I))*(REAL(NCOMPTOT(I)-NMNHB(I))))/((REAL(NCOMPTOT(I))**3)+SMALL))
        end if
C
C Now the parameters themselves
C
c       write (6,*) ' doing param'
        CFALPHA(I)=PALPHA(I)/(PTOTALPHA+SMALL)
        ERRPALPHA(I)=ERRPALPHA(I)/(PTOTALPHA+SMALL)

        CFBETA(I)=PBETA(I)/(PTOTBETA+SMALL)
        ERRPBETA(I)=ERRPBETA(I)/(PTOTBETA+SMALL)

        CFPHI(I)=PPHI(I)/(PTOTPHI+SMALL)
        ERRPPHI(I)=ERRPPHI(I)/(PTOTPHI+SMALL)

        CFCOIL(I)=PCOIL(I)/(PTOTCOIL+SMALL)
        ERRPCOIL(I)=ERRPCOIL(I)/(PTOTCOIL+SMALL)

        CFACC(I)=PACC(I)/(PTOTACC+SMALL)
        ERRPACC(I)=ERRPACC(I)/(PTOTACC+SMALL)

        CFINACC(I)=PINACC(I)/(PTOTINACC+SMALL)
        ERRPINACC(I)=ERRPINACC(I)/(PTOTINACC+SMALL)

        CFSS(I)=PSS(I)/(PTOTSS+SMALL)
        ERRPSSHB(I)=ERRPSSHB(I)/(PTOTSS+SMALL)

        CFCO(I)=PCO(I)/(PTOTCO+SMALL)
        ERRPMOHB(I)=ERRPMOHB(I)/(PTOTCO+SMALL)

        CFNH(I)=PNH(I)/(PTOTNH+SMALL)
        ERRPMNHB(I)=ERRPMNHB(I)/(PTOTNH+SMALL)
      end do
C
      write (3,'(26X,''CHOU AND FASMAN PARAMETERS'',/)')
C
      write (3,'(''    Secondary structure'',/)')
      write (3,'('' class   Total  Fraction'')')
      write (3,'('' alpha : '',I5,4X,F6.3)') NTOTALPHA,PTOTALPHA
      write (3,'('' beta  : '',I5,4X,F6.3)') NTOTBETA,PTOTBETA
      write (3,'('' phi   : '',I5,4X,F6.3)') NTOTPHI,PTOTPHI
      write (3,'('' coil  : '',I5,4X,F6.3)') NTOTCOIL,PTOTCOIL
      write (3,'(//)')
      write (3,'(5x,''Ntot    ---- alpha ----  ----- beta ----  ----- +phi ----  ----- coil ----'')')
C
      do i=1,NUMAA
        write (3,'(2x,a,1x,i5,2x,4(I5,1x,F5.3,1X,F5.3))') res(i),ncomptot(i),
     -         nalpha(i),palpha(i),cfalpha(i),nbeta(i),pbeta(i),cfbeta(i),
     -         nphi(i),pphi(i),cfphi(i),ncoil(i),pcoil(i),cfcoil(i)
        write (3,'(''errors'',17x,4(f5.3,12x))') errpalpha(i),errpbeta(i),errpphi(i),errpcoil(i)
      end do
C
      write (3,'(//)')
      write (3,'(''    Solvent accessiblity'',/)')
      write (3,'('' class   Total  Fraction'')')
      write (3,'('' acc   : '',I5,4X,F6.3)') NTOTACC,PTOTACC
      write (3,'('' inacc : '',I5,4X,f6.3)') NTOTINACC,PTOTINACC
      write (3,'(//)')
      write (3,'(5x,''Ntot    ----- acc -----  ----- inac ----'')')
      do i=1,NUMAA
        write (3,'(2x,a,1x,i5,2x,2(I5,1x,f5.3,1x,f5.3))') res(i),ncomptot(i),
     -         naccess(i),pacc(i),cfacc(i),(ncomptot(i)-naccess(i)),pinacc(i),cfinacc(i)
        write (3,'(''errors'',17x,2(f5.3,12x))') errpacc(i),errpinacc(i)
      end do
C
      write (3,'(//)')
      write (3,'(''    Hydrogen bonding'',/)')
      write (3,'('' class       Total  Fraction'')')
      write (3,'('' side-side : '',I5,4X,F6.3)') NTOTSS,PTOTSS
      write (3,'('' side-CO   : '',I5,4X,f6.3)') NTOTCO,PTOTCO
      write (3,'('' side-NH   : '',I5,4X,F6.3)') NTOTNH,PTOTNH
      write (3,'(//)')
      write (3,'(5x,''Ntot    ----- SS ------  ----- CO ------  ----- NH ------'')')
      do i=1,NUMAA
        if (hbpossss(i)) then
          write (3,'(2x,a,1x,i5,2x,3(I5,1x,f5.3,1x,f5.3))') res(i),ncomptot(i),
     -           nsshb(i),pss(i),cfss(i),nmohb(i),pco(i),cfco(i),nmnhb(i),pnh(i),cfnh(i)
          write (3,'(''errors'',17x,3(f5.3,12x))') errpsshb(i),errpmohb(i),errpmnhb(i)
        end if
      end do
C
C -----------------------------------------------------------------------------
C
      write (3,'(//,33X,''RAW SCORE TABLES'',/)')
C
C -2 for both the cis-peptide tables
C -38 for the capping and cis-peptide
C
      do M=1,NMAT(1)-38
c      do M=1,NMAT(1)-2
        write (3,'(//,A,/)') TITLE(M)
        write (3,'(8X,A)') HEADER 
        do I=1,22
          write (3,'(1X,A,2X,21I5)') RES(I),(NTOT(J,I,M),J=1,21)
        end do
        write (3,'(8X,A)') spacer 
        write (3,'('' tot'',21I5)') (NSUM(I,M),I=1,21)
        NIND=M-1
        if (NIND.GT.0) then
          call vecaddr(nind,nsec,nacc,nss,nso,nsn)
          write (3,'(''from'',21I5)') (NNTOT(I,NSEC,NACC,NSS,NSO,NSN),I=1,NUMAA)
        end if
        write (3,'(/)')
      end do
C
C ----------------------------------------------------------------------------
C
C section to allow summing of individual features to give more 
C significant matrices. Assumes normal order and structure of matrices.
C
      if (NMAT(1).EQ.NUMJOYMAT) then
C
C The funny example one for thesis
C
        do I=10,65,16
          do J=1,22
            do K=1,NUMAA
              NMUTINNSSHB(K,J)=NMUTINNSSHB(K,J)+NTOT(K,J,I)
              NMUTINNSSHB(K,J)=NMUTINNSSHB(K,J)+NTOT(K,J,I+1)
              NMUTINNSSHB(K,J)=NMUTINNSSHB(K,J)+NTOT(K,J,I+2)
              NMUTINNSSHB(K,J)=NMUTINNSSHB(K,J)+NTOT(K,J,I+3)
            end do
          end do
        end do
        do I=10,65,16
          do J=1,22
            do K=1,NUMAA
              NMUTINNSSSNHB(K,J)=NMUTINNSSSNHB(K,J)+NTOT(K,J,I)
              NMUTINNSSSNHB(K,J)=NMUTINNSSSNHB(K,J)+NTOT(K,J,I+2)
            end do
          end do
        end do
C
        do I=2,17
          do J=1,22
            do K=1,NUMAA
              NMUTALP(K,J)=NMUTALP(K,J)+NTOT(K,J,I)
              NMUTBET(K,J)=NMUTBET(K,J)+NTOT(K,J,I+16)
              NMUTPHI(K,J)=NMUTPHI(K,J)+NTOT(K,J,I+32)
              NMUTCOI(K,J)=NMUTCOI(K,J)+NTOT(K,J,I+48)
            end do
          end do
        end do
        do I=2,9
          do J=1,22
            do K=1,NUMAA
              NMUTACCALP(K,J)=NMUTACCALP(K,J)+NTOT(K,J,I)
              NMUTACCBET(K,J)=NMUTACCBET(K,J)+NTOT(K,J,I+16)
              NMUTACCPHI(K,J)=NMUTACCPHI(K,J)+NTOT(K,J,I+32)
              NMUTACCCOI(K,J)=NMUTACCCOI(K,J)+NTOT(K,J,I+48)
            end do
          end do
        end do
        do I=10,17
          do J=1,22
            do K=1,NUMAA
              NMUTINNALP(K,J)=NMUTINNALP(K,J)+NTOT(K,J,I)
              NMUTINNBET(K,J)=NMUTINNBET(K,J)+NTOT(K,J,I+16)
              NMUTINNPHI(K,J)=NMUTINNPHI(K,J)+NTOT(K,J,I+32)
              NMUTINNCOI(K,J)=NMUTINNCOI(K,J)+NTOT(K,J,I+48)
            end do
          end do
        end do
        do I=2,65
          if ((I.GE.2.AND.I.LE.9).OR.
     -        (I.GE.18.AND.I.LE.25).OR.
     -        (I.GE.34.AND.I.LE.41).OR.
     -        (I.GE.50.AND.I.LE.57)) then
            do J=1,22
              do K=1,NUMAA
                NMUTACC(K,J)=NMUTACC(K,J)+NTOT(K,J,I)
              end do
            end do
          ELSE
            do J=1,22
              do K=1,NUMAA
                NMUTINACC(K,J)=NMUTINACC(K,J)+NTOT(K,J,I)
              end do
            end do
          end if
        end do
C
C Which classes will have no H-bonds 9, 17, 25, 33, 41, 49, 57, 65
C 
        do I=9,65,8
          do J=1,22
            do K=1,NUMAA
              NMUTNOHB(K,J)=NMUTNOHB(K,J)+NTOT(K,J,I)
            end do
          end do
        end do
C
        do I=2,65,8
          do J=1,22
            do K=1,NUMAA
              NMUTS(K,J)=NMUTS(K,J)+NTOT(K,J,I)
              NMUTS(K,J)=NMUTS(K,J)+NTOT(K,J,I+1)
              NMUTS(K,J)=NMUTS(K,J)+NTOT(K,J,I+2)
              NMUTS(K,J)=NMUTS(K,J)+NTOT(K,J,I+3)
            end do
          end do
        end do
        do I=2,65,4
          do J=1,22
            do K=1,NUMAA
              NMUTO(K,J)=NMUTO(K,J)+NTOT(K,J,I)
              NMUTO(K,J)=NMUTO(K,J)+NTOT(K,J,I+1)
            end do
          end do
        end do
        do I=2,65,2
          do J=1,22
            do K=1,NUMAA
              NMUTN(K,J)=NMUTN(K,J)+NTOT(K,J,I)
            end do
          end do
        end do
C
C ------------------------------------------------------------------------------
C
C Output the raw summed feature scores
C
        write (3,'(//,33X,''SUMMED PROPERTY TABLES'',/)')
C
        call WRRAW('Alpha',NMUTALP,NSUMALP)
        call WRRAW('Accessible Alpha',NMUTACCALP,NSUMACCALP)
        call WRRAW('Inaccessible Alpha',NMUTINNALP,NSUMINNALP)
        call WRRAW('Beta',NMUTBET,NSUMBET)
        call WRRAW('Accessible Beta',NMUTACCBET,NSUMACCBET)
        call WRRAW('Inaccessible Beta',NMUTINNBET,NSUMINNBET)
        call WRRAW('Phi',NMUTPHI,NSUMPHI)
        call WRRAW('Accessible Phi',NMUTACCPHI,NSUMACCPHI)
        call WRRAW('Inaccessible Phi',NMUTINNPHI,NSUMINNPHI)
        call WRRAW('Coil',NMUTCOI,NSUMCOI)
        call WRRAW('Accessible Coil',NMUTACCCOI,NSUMACCCOI)
        call WRRAW('Inaccessible Coil',NMUTINNCOI,NSUMINNCOI)
        call WRRAW('Inaccessible SSHB',NMUTINNSSHB,NSUMINNSSHB)
        call WRRAW('Inaccessible SSHB + SNHB',NMUTINNSSSNHB,NSUMINNSSSNHB)
C
C Output the data for simply the classification at the 8 class level ??
C
        write (3,'(//,''SUMMARY OF HIGHEST MARGINALS'',/)')
C
        write (3,'(5X,'' --- Alpha ----  --- Beta -----  --- + Phi ----  --- Coil -----'')')
        write (3,'(7X,''acc     inn     acc     inn     acc    inn      acc     inn'')')
        NZALP=0
        NZBET=0
        NZPHI=0
        NZCOI=0
        NZACALP=0
        NZACBET=0
        NZACPHI=0
        NZACCOI=0
        NZINALP=0
        NZINBET=0
        NZINPHI=0
        NZINCOI=0
        do I=1,NUMAA
          NZACALP=NZACALP+NSUMACCALP(I)
          NZINALP=NZINALP+NSUMINNALP(I)
          NZALP=NZALP+NSUMACCALP(I)+NSUMINNALP(I)
          NZACBET=NZACBET+NSUMACCBET(I)
          NZINBET=NZINBET+NSUMINNBET(I)
          NZBET=NZBET+NSUMACCBET(I)+NSUMINNBET(I)
          NZACPHI=NZACPHI+NSUMACCPHI(I)
          NZINPHI=NZINPHI+NSUMINNPHI(I)
          NZPHI=NZPHI+NSUMACCPHI(I)+NSUMINNPHI(I)
          NZACCOI=NZACCOI+NSUMACCCOI(I)
          NZINCOI=NZINCOI+NSUMINNCOI(I)
          NZCOI=NZCOI+NSUMACCCOI(I)+NSUMINNCOI(I)
C
          write (3,'(1X,A1,2X,8(2X,I6))') RES(I),NSUMACCALP(I),NSUMINNALP(I),
     -                   NSUMACCBET(I),NSUMINNBET(I),
     -                   NSUMACCPHI(I),NSUMINNPHI(I),
     -                   NSUMACCCOI(I),NSUMINNCOI(I)
        end do
        write (3,'(4X,8(2X,I6))') NZACALP,NZINALP,NZACBET,NZINBET,
     -              NZACPHI,NZINPHI,NZACCOI,NZINCOI
        write (3,'(4X,4(4X,I8,4X))') NZALP,NZBET,NZPHI,NZCOI
        write (3,'('' '')')
C
C ----------------------------------------------------------------------------
C
C New cis-peptide tables
C
        write (3,'(//,''Accessible cis-peptide'',/)') 
        write (3,'(8X,A)') HEADER 
        do I=1,22
          write (3,'(1X,A,2X,21I5)') RES(I),(NTOT(J,I,66),J=1,NUMAA)
        end do
        write (3,'(8X,A)') spacer 
        write (3,'('' tot'',21I5)') (NSUM(I,66),I=1,NUMAA)
        write (3,'(//,''Inaccessible cis-peptide'',/)') 
        write (3,'(8X,A)') HEADER 
        do I=1,22
          write (3,'(1X,A,2X,21I5)') RES(I),(NTOT(J,I,67),J=1,NUMAA)
        end do
        write (3,'(8X,A)') spacer 
        write (3,'('' tot'',21I5)') (NSUM(I,67),I=1,NUMAA)
C
C Capping tables
C
        write (3,'(//,''Capping data tables'',/)')
        write (3,'(//,''Helix N-cap -4'',/)') 
        write (3,'(6X,A)') HEADER3
        do I=1,22
          write (3,'(1X,A,2X,21I3)') RES(I),(NTOT(J,I,68),J=1,NUMAA)
        end do
        write (3,'(6X,A)') spacer 
        write (3,'('' tot'',21I3)') (NSUM(I,68),I=1,NUMAA)
        write (3,'(//,''Helix N-cap -3'',/)') 
        write (3,'(6X,A)') header3
        do I=1,22
          write (3,'(1X,A,2X,21I3)') RES(I),(NTOT(J,I,69),J=1,NUMAA)
        end do
        write (3,'(6X,A)') spacer 
        write (3,'('' tot'',21I3)') (NSUM(I,69),I=1,NUMAA)
        write (3,'(//,''Helix N-cap -2'',/)') 
        write (3,'(6X,A)') header3
        do I=1,22
          write (3,'(1X,A,2X,21I3)') RES(I),(NTOT(J,I,70),J=1,NUMAA)
        end do
        write (3,'(6X,A)') spacer 
        write (3,'('' tot'',21I3)') (NSUM(I,70),I=1,NUMAA)
        write (3,'(//,''Helix N-cap -1'',/)') 
        write (3,'(6X,A)') header3
        do I=1,22
          write (3,'(1X,A,2X,21I3)') RES(I),(NTOT(J,I,71),J=1,NUMAA)
        end do
        write (3,'(6X,A)') spacer 
        write (3,'('' tot'',21I3)') (NSUM(I,71),I=1,NUMAA)
        write (3,'(//,''Helix N-cap 0'',/)') 
        write (3,'(6X,A)') header3
        do I=1,22
          write (3,'(1X,A,2X,21I3)') RES(I),(NTOT(J,I,72),J=1,NUMAA)
        end do
        write (3,'(6X,A)') spacer 
        write (3,'('' tot'',21I3)') (NSUM(I,72),I=1,NUMAA)
        write (3,'(//,''Helix N-cap +1'',/)') 
        write (3,'(6X,A)') header3
        do I=1,22
          write (3,'(1X,A,2X,21I3)') RES(I),(NTOT(J,I,73),J=1,NUMAA)
        end do
        write (3,'(6X,A)') spacer 
        write (3,'('' tot'',21I3)') (NSUM(I,73),I=1,NUMAA)
        write (3,'(//,''Helix N-cap +2'',/)') 
        write (3,'(6X,A)') header3
        do I=1,22
          write (3,'(1X,A,2X,21I3)') RES(I),(NTOT(J,I,74),J=1,NUMAA)
        end do
        write (3,'(6X,A)') spacer 
        write (3,'('' tot'',21I3)') (NSUM(I,74),I=1,NUMAA)
        write (3,'(//,''Helix N-cap +3'',/)') 
        write (3,'(6X,A)') header3
        do I=1,22
          write (3,'(1X,A,2X,21I3)') RES(I),(NTOT(J,I,75),J=1,NUMAA)
        end do
        write (3,'(6X,A)') spacer 
        write (3,'('' tot'',21I3)') (NSUM(I,75),I=1,NUMAA)
        write (3,'(//,''Helix N-cap +4'',/)') 
        write (3,'(6X,A)') header3
        do I=1,22
          write (3,'(1X,A,2X,21I3)') RES(I),(NTOT(J,I,76),J=1,NUMAA)
        end do
        write (3,'(6X,A)') spacer 
        write (3,'('' tot'',21I3)') (NSUM(I,76),I=1,NUMAA)
        write (3,'(//,''Helix C-cap -4'',/)') 
        write (3,'(6X,A)') header3
        do I=1,22
          write (3,'(1X,A,2X,21I3)') RES(I),(NTOT(J,I,77),J=1,NUMAA)
        end do
        write (3,'(6X,A)') spacer 
        write (3,'('' tot'',21I3)') (NSUM(I,77),I=1,NUMAA)
        write (3,'(//,''Helix C-cap -3'',/)') 
        write (3,'(6X,A)') header3
        do I=1,22
          write (3,'(1X,A,2X,21I3)') RES(I),(NTOT(J,I,78),J=1,NUMAA)
        end do
        write (3,'(6X,A)') spacer 
        write (3,'('' tot'',21I3)') (NSUM(I,78),I=1,NUMAA)
        write (3,'(//,''Helix C-cap -2'',/)') 
        write (3,'(6X,A)') header3
        do I=1,22
          write (3,'(1X,A,2X,21I3)') RES(I),(NTOT(J,I,79),J=1,NUMAA)
        end do
        write (3,'(6X,A)') spacer 
        write (3,'('' tot'',21I3)') (NSUM(I,79),I=1,NUMAA)
        write (3,'(//,''Helix C-cap -1'',/)') 
        write (3,'(6X,A)') header3
        do I=1,22
          write (3,'(1X,A,2X,21I3)') RES(I),(NTOT(J,I,80),J=1,NUMAA)
        end do
        write (3,'(6X,A)') spacer 
        write (3,'('' tot'',21I3)') (NSUM(I,80),I=1,NUMAA)
        write (3,'(//,''Helix C-cap 0'',/)') 
        write (3,'(6X,A)') header3
        do I=1,22
          write (3,'(1X,A,2X,21I3)') RES(I),(NTOT(J,I,81),J=1,NUMAA)
        end do
        write (3,'(6X,A)') spacer 
        write (3,'('' tot'',21I3)') (NSUM(I,81),I=1,NUMAA)
        write (3,'(//,''Helix C-cap +1'',/)') 
        write (3,'(6X,A)') header3
        do I=1,22
          write (3,'(1X,A,2X,21I3)') RES(I),(NTOT(J,I,82),J=1,NUMAA)
        end do
        write (3,'(6X,A)') spacer 
        write (3,'('' tot'',21I3)') (NSUM(I,82),I=1,NUMAA)
        write (3,'(//,''Helix C-cap +2'',/)') 
        write (3,'(6X,A)') header3
        do I=1,22
          write (3,'(1X,A,2X,21I3)') RES(I),(NTOT(J,I,83),J=1,NUMAA)
        end do
        write (3,'(6X,A)') spacer 
        write (3,'('' tot'',21I3)') (NSUM(I,83),I=1,NUMAA)
        write (3,'(//,''Helix C-cap +3'',/)') 
        write (3,'(6X,A)') header3
        do I=1,22
          write (3,'(1X,A,2X,21I3)') RES(I),(NTOT(J,I,84),J=1,NUMAA)
        end do
        write (3,'(6X,A)') spacer 
        write (3,'('' tot'',21I3)') (NSUM(I,84),I=1,NUMAA)
        write (3,'(//,''Helix C-cap +4'',/)') 
        write (3,'(6X,A)') header3
        do I=1,22
          write (3,'(1X,A,2X,21I3)') RES(I),(NTOT(J,I,85),J=1,NUMAA)
        end do
        write (3,'(6X,A)') spacer 
        write (3,'('' tot'',21I3)') (NSUM(I,85),I=1,NUMAA)
        write (3,'(//,''Sheet N-cap -4'',/)') 
        write (3,'(6X,A)') header3
        do I=1,22
          write (3,'(1X,A,2X,21I3)') RES(I),(NTOT(J,I,86),J=1,NUMAA)
        end do
        write (3,'(6X,A)') spacer 
        write (3,'('' tot'',21I3)') (NSUM(I,86),I=1,NUMAA)
        write (3,'(//,''Sheet N-cap -3'',/)') 
        write (3,'(6X,A)') header3
        do I=1,22
          write (3,'(1X,A,2X,21I3)') RES(I),(NTOT(J,I,87),J=1,NUMAA)
        end do
        write (3,'(6X,A)') spacer 
        write (3,'('' tot'',21I3)') (NSUM(I,87),I=1,NUMAA)
        write (3,'(//,''Sheet N-cap -2'',/)') 
        write (3,'(6X,A)') header3
        do I=1,22
          write (3,'(1X,A,2X,21I3)') RES(I),(NTOT(J,I,88),J=1,NUMAA)
        end do
        write (3,'(6X,A)') spacer 
        write (3,'('' tot'',21I3)') (NSUM(I,88),I=1,NUMAA)
        write (3,'(//,''Sheet N-cap -1'',/)') 
        write (3,'(6X,A)') header3
        do I=1,22
          write (3,'(1X,A,2X,21I3)') RES(I),(NTOT(J,I,89),J=1,NUMAA)
        end do
        write (3,'(6X,A)') spacer 
        write (3,'('' tot'',21I3)') (NSUM(I,89),I=1,NUMAA)
        write (3,'(//,''Sheet N-cap 0'',/)') 
        write (3,'(6X,A)') header3
        do I=1,22
          write (3,'(1X,A,2X,21I3)') RES(I),(NTOT(J,I,90),J=1,NUMAA)
        end do
        write (3,'(6X,A)') spacer 
        write (3,'('' tot'',21I3)') (NSUM(I,90),I=1,NUMAA)
        write (3,'(//,''Sheet N-cap +1'',/)') 
        write (3,'(6X,A)') header3
        do I=1,22
          write (3,'(1X,A,2X,21I3)') RES(I),(NTOT(J,I,91),J=1,NUMAA)
        end do
        write (3,'(6X,A)') spacer 
        write (3,'('' tot'',21I3)') (NSUM(I,91),I=1,NUMAA)
        write (3,'(//,''Sheet N-cap +2'',/)') 
        write (3,'(6X,A)') header3
        do I=1,22
          write (3,'(1X,A,2X,21I3)') RES(I),(NTOT(J,I,92),J=1,NUMAA)
        end do
        write (3,'(6X,A)') spacer 
        write (3,'('' tot'',21I3)') (NSUM(I,92),I=1,NUMAA)
        write (3,'(//,''Sheet N-cap +3'',/)') 
        write (3,'(6X,A)') header3
        do I=1,22
          write (3,'(1X,A,2X,21I3)') RES(I),(NTOT(J,I,93),J=1,NUMAA)
        end do
        write (3,'(6X,A)') spacer 
        write (3,'('' tot'',21I3)') (NSUM(I,93),I=1,NUMAA)
        write (3,'(//,''Sheet N-cap +4'',/)') 
        write (3,'(6X,A)') header3
        do I=1,22
          write (3,'(1X,A,2X,21I3)') RES(I),(NTOT(J,I,94),J=1,NUMAA)
        end do
        write (3,'(6X,A)') spacer 
        write (3,'('' tot'',21I3)') (NSUM(I,94),I=1,NUMAA)
        write (3,'(//,''Sheet C-cap -4'',/)') 
        write (3,'(6X,A)') header3
        do I=1,22
          write (3,'(1X,A,2X,21I3)') RES(I),(NTOT(J,I,95),J=1,NUMAA)
        end do
        write (3,'(6X,A)') spacer 
        write (3,'('' tot'',21I3)') (NSUM(I,95),I=1,NUMAA)
        write (3,'(//,''Sheet C-cap -3'',/)') 
        write (3,'(6X,A)') header3
        do I=1,22
          write (3,'(1X,A,2X,21I3)') RES(I),(NTOT(J,I,96),J=1,NUMAA)
        end do
        write (3,'(6X,A)') spacer 
        write (3,'('' tot'',21I3)') (NSUM(I,96),I=1,NUMAA)
        write (3,'(//,''Sheet C-cap -2'',/)') 
        write (3,'(6X,A)') header3
        do I=1,22
          write (3,'(1X,A,2X,21I3)') RES(I),(NTOT(J,I,97),J=1,NUMAA)
        end do
        write (3,'(6X,A)') spacer 
        write (3,'('' tot'',21I3)') (NSUM(I,97),I=1,NUMAA)
        write (3,'(//,''Sheet C-cap -1'',/)') 
        write (3,'(6X,A)') header3
        do I=1,22
          write (3,'(1X,A,2X,21I3)') RES(I),(NTOT(J,I,98),J=1,NUMAA)
        end do
        write (3,'(6X,A)') spacer 
        write (3,'('' tot'',21I3)') (NSUM(I,98),I=1,NUMAA)
        write (3,'(//,''Sheet C-cap 0'',/)') 
        write (3,'(6X,A)') header3
        do I=1,22
          write (3,'(1X,A,2X,21I3)') RES(I),(NTOT(J,I,99),J=1,NUMAA)
        end do
        write (3,'(6X,A)') spacer 
        write (3,'('' tot'',21I3)') (NSUM(I,99),I=1,NUMAA)
        write (3,'(//,''Sheet C-cap +1'',/)') 
        write (3,'(6X,A)') header3
        do I=1,22
          write (3,'(1X,A,2X,21I3)') RES(I),(NTOT(J,I,100),J=1,NUMAA)
        end do
        write (3,'(6X,A)') spacer 
        write (3,'('' tot'',21I3)') (NSUM(I,100),I=1,NUMAA)
        write (3,'(//,''Sheet C-cap +2'',/)') 
        write (3,'(6X,A)') header3
        do I=1,22
          write (3,'(1X,A,2X,21I3)') RES(I),(NTOT(J,I,101),J=1,NUMAA)
        end do
        write (3,'(6X,A)') spacer 
        write (3,'('' tot'',21I3)') (NSUM(I,101),I=1,NUMAA)
        write (3,'(//,''Sheet C-cap +3'',/)') 
        write (3,'(6X,A)') header3
        do I=1,22
          write (3,'(1X,A,2X,21I3)') RES(I),(NTOT(J,I,102),J=1,NUMAA)
        end do
        write (3,'(6X,A)') spacer 
        write (3,'('' tot'',21I3)') (NSUM(I,102),I=1,NUMAA)
        write (3,'(//,''Sheet C-cap +4'',/)') 
        write (3,'(6X,A)') header3
        do I=1,22
          write (3,'(1X,A,2X,21I3)') RES(I),(NTOT(J,I,103),J=1,NUMAA)
        end do
        write (3,'(6X,A)') spacer 
        write (3,'('' tot'',21I3)') (NSUM(I,103),I=1,NUMAA)
C
C -----------------------------------------------------------------------------
C
        write (3,'(//,''Accessible'',/)') 
        write (3,'(8X,A)') HEADER 
        do J=1,NUMAA
          NTEMP(J)=0
        end do
        do I=1,22
          write (3,'(1X,A,2X,21I5)') RES(I),(NMUTACC(J,I),J=1,NUMAA)
          do J=1,NUMAA
            NSUMACC(J)=NSUMACC(J)+NMUTACC(J,I)
          end do
        end do
        write (3,'(8X,A)') spacer 
        write (3,'('' tot'',21I5)') (NSUMACC(I),I=1,NUMAA)
C
        write (3,'(//,''Inaccessible'',/)') 
        write (3,'(8X,A)') HEADER 
        do J=1,NUMAA
          NTEMP(J)=0
        end do
        do I=1,22
          write (3,'(1X,A,2X,21I5)') RES(I),(NMUTINACC(J,I),J=1,NUMAA)
          do J=1,NUMAA
            NSUMINACC(J)=NSUMINACC(J)+NMUTINACC(J,I)
          end do
        end do
        write (3,'(8X,A)') spacer 
        write (3,'('' tot'',21I5)') (NSUMINACC(I),I=1,NUMAA)
C
        write (3,'(//,''Sidechain to sidechain H-bond'',/)') 
        write (3,'(8X,A)') HEADER 
        do J=1,NUMAA
          NTEMP(J)=0
        end do
        do I=1,22
          write (3,'(1X,A,2X,21I5)') RES(I),(NMUTS(J,I),J=1,NUMAA)
          do J=1,NUMAA
            NSUMS(J)=NSUMS(J)+NMUTS(J,I)
          end do
        end do
        write (3,'(8X,A)') spacer 
        write (3,'('' tot'',21I5)') (NSUMS(I),I=1,NUMAA)
C
        write (3,'(//,''Sidechain to mainchain carbonyl H-bond'',/)') 
        write (3,'(8X,A)') HEADER 
        do J=1,NUMAA
          NTEMP(J)=0
        end do
        do I=1,22
          write (3,'(1X,A,2X,21I5)') RES(I),(NMUTO(J,I),J=1,NUMAA)
          do J=1,NUMAA
            NSUMO(J)=NSUMO(J)+NMUTO(J,I)
          end do
        end do
        write (3,'(8X,A)') spacer 
        write (3,'('' tot'',21I5)') (NSUMO(I),I=1,NUMAA)
C
        write (3,'(//,''Sidechain to mainchain amide H-bond'',/)') 
        write (3,'(8X,A)') HEADER 
        do J=1,NUMAA
          NTEMP(J)=0
        end do
        do I=1,22
          write (3,'(1X,A,2X,21I5)') RES(I),(NMUTN(J,I),J=1,NUMAA)
          do J=1,NUMAA
            NSUMN(J)=NSUMN(J)+NMUTN(J,I)
          end do
        end do
        write (3,'(8X,A)') spacer 
        write (3,'('' tot'',21I5)') (NSUMN(I),I=1,NUMAA)
C
        write (3,'(//,''No hydrogen bonding'',/)') 
        write (3,'(8X,A)') HEADER 
        do J=1,NUMAA
          NTEMP(J)=0
        end do
        do I=1,22
          write (3,'(1X,A,2X,21I5)') RES(I),(NMUTNOHB(J,I),J=1,NUMAA)
          do J=1,NUMAA
            NSUMNOHB(J)=NSUMNOHB(J)+NMUTNOHB(J,I)
          end do
        end do
        write (3,'(8X,A)') spacer 
        write (3,'('' tot'',21I5)') (NSUMNOHB(I),I=1,NUMAA)
C
        write (3,'(/)')
      end if
C
C ----------------------------------------------------------------------------
C
      if (outprob) then
        write (3,'(//,33X,''PROBABILITY TABLES'',/)')
        do M=1,NMAT(1)
          call WRPROB(TITLE(M),NTOT(1,1,M),NSUM(1,M),PTOT(1,1,M))
          write (3,'(/)')
        end do
        write (6,*) ' '
C 
C ----------------------------------------------------------------------------
C
C new section for summed probabilities
C
        write (3,'(//,''SUMMED FEATURE PROBABILITIES'')')
        call WRPROB('Alpha',NMUTALP,NSUMALP,PMUTALP)
        call WRPROB('Beta',NMUTBET,NSUMBET,PMUTBET)
        call WRPROB('Positive Phi',NMUTPHI,NSUMPHI,PMUTPHI)
        call WRPROB('Coil',NMUTCOI,NSUMCOI,PMUTCOI)
        call WRPROB('Accessible',NMUTACC,NSUMACC,PMUTACC)
        call WRPROB('Inaccessible',NMUTINACC,NSUMINACC,PMUTINACC)
        call WRPROB('Side to side h-bond',NMUTS,NSUMS,PMUTS)
        call WRPROB('Side to carbonyl h-bond',NMUTO,NSUMO,PMUTO)
        call WRPROB('Side to amide h-bond',NMUTN,NSUMN,PMUTN)
        call WRPROB('No hydrogen bonding',NMUTNOHB,NSUMNOHB,PMUTNOHB)
C
C Now the marginal probabilities
C
        write (3,'(//,''SUMMED MARGINAL PROBABILITIES'')')
        call WRPROB('Accessible Alpha',NMUTACCALP,NSUMACCALP,PMUTACCALP) 
        call WRPROB('Inaccessible Alpha',NMUTINNALP,NSUMINNALP,PMUTINNALP) 
        call WRPROB('Accessible Beta',NMUTACCBET,NSUMACCBET,PMUTACCBET) 
        call WRPROB('Inaccessible Beta',NMUTINNBET,NSUMINNBET,PMUTINNBET) 
        call WRPROB('Accessible phi',NMUTACCPHI,NSUMACCPHI,PMUTACCPHI) 
        call WRPROB('Inaccessible phi',NMUTINNPHI,NSUMINNPHI,PMUTINNPHI) 
        call WRPROB('Accessible Coil',NMUTACCCOI,NSUMACCCOI,PMUTACCCOI) 
        call WRPROB('Inaccessible Coil',NMUTINNCOI,NSUMINNCOI,PMUTINNCOI) 
      end if
C
C ----------------------------------------------------------------------------
C
C Now calculate the difference matrices, 
C Differences are between the the distribution with a property and the 
C distribution without the property
C
      if (DODIFF) then
        write (3,'(//,33X,''PROBABILITY DIFFERENCE TABLES'',/)')
        do I=2,NMAT(1)-38
          do J=1,22
            do K=1,NUMAA
              if (HBPOSSSS(K)) then
                PDIFF(K,J,I)=PTOT(K,J,I)-PNOTTOT(K,J,I)
                if (OUTERR) then
                  PDIFFERR(K,J,I)=PERR(K,J,I)+PNOTERR(K,J,I)
                end if
              end if
            end do
          end do
        end do
        do I=NMAT(1)-37,NMAT(1)
          do J=1,22
            do K=1,NUMAA
              PDIFF(K,J,I)=PTOT(K,J,I)-PTOT(K,J,1)
              if (OUTERR) then
                PDIFFERR(K,J,I)=PERR(K,J,I)+PNOTERR(K,J,1)
              end if
            end do
          end do
        end do
C
        do I=2,NMAT(1)-38
          write (3,'(//,A,/)') TITLE(I)
          write (3,'(A)') header2
          do J=1,22
            write (3,'(1X,A,2X,21F6.3)') RES(J),(PDIFF(K,J,I),K=1,NUMAA) 
            if (OUTERR) then
              write (3,'(3X,''('',21F6.3,'')'')') (PDIFFERR(K,J,I),K=1,NUMAA)
            end if
          end do
C
C Search the matrix for the biggest and positive and negative changes
C
          BIG=0.00001
          SMALL=0.00001
          do J=1,22
            do K=1,NUMAA
              if (PDIFF(K,J,I).LT.SMALL) then
                SMALL=PDIFF(K,J,I)
                INDJSMALL=J
                INDKSMALL=K
              end if
              if (PDIFF(K,J,I).GT.BIG) then
                BIG=PDIFF(K,J,I)
                INDJBIG=J
                INDKBIG=K
              end if
            end do
          end do
          write (3,'(//,5X,''Largest depletion   '',A,''  -  '',A,2X,F6.3,/,
     -           5X,''Largest enrichment  '',A,''  -  '',A,2X,F6.3,/)') 
     -           RES(INDKSMALL),RES(INDJSMALL),SMALL,RES(INDKBIG),
     -           RES(INDJBIG),BIG
        end do
C
C capping
C
        do I=NMAT(1)-37,NMAT(1)
          write (3,'(//,A,/)') TITLE(I)
          write (3,'(A)') header2
          do J=1,22
            write (3,'(1X,A,2X,21F6.3)') RES(J),(PDIFF(K,J,I),K=1,NUMAA) 
          end do
        end do
      end if
C
C do probability difference tables for selected features
C
      if (dodiff) then
        do i=1,22
         do j=1,NUMAA
          call stderr(nmutalp(j,i),nsumalp(j),pmutalp(j,i),perralp(j,i))
          call stderr(nmutbet(j,i),nsumbet(j),pmutbet(j,i),perrbet(j,i))
          call stderr(nmutcoi(j,i),nsumcoi(j),pmutcoi(j,i),perrcoi(j,i))
          call stderr(nmutphi(j,i),nsumphi(j),pmutphi(j,i),perrphi(j,i))
          call stderr(nmutacc(j,i),nsumacc(j),pmutacc(j,i),perracc(j,i))
          call stderr(nmuts(j,i),nsums(j),pmuts(j,i),perrs(j,i))
          call stderr(nmuto(j,i),nsumo(j),pmuto(j,i),perro(j,i))
          call stderr(nmutn(j,i),nsumn(j),pmutn(j,i),perrn(j,i))
          call stderr(nmutinacc(j,i),nsuminacc(j),pmutinacc(j,i),perrinacc(j,i))
C
          call stderr((ntot(j,i,1)-nmutalp(j,i)),(NSUM(j,1)-nsumalp(j)),
     -                pmutnotalp(j,i),perrnotalp(j,i))
          call stderr((ntot(j,i,1)-nmutbet(j,i)),(NSUM(j,1)-nsumbet(j)),
     -                pmutnotbet(j,i),perrnotbet(j,i))
          call stderr((ntot(j,i,1)-nmutcoi(j,i)),(NSUM(j,1)-nsumcoi(j)),
     -                pmutnotcoi(j,i),perrnotcoi(j,i))
          call stderr((ntot(j,i,1)-nmutphi(j,i)),(NSUM(j,1)-nsumphi(j)),
     -                pmutnotphi(j,i),perrnotphi(j,i))
          call stderr((ntot(j,i,1)-nmuts(j,i)),(NSUM(j,1)-nsums(j)),
     -                pmutnots(j,i),perrnots(j,i))
          call stderr((ntot(j,i,1)-nmuto(j,i)),(NSUM(j,1)-nsumo(j)),
     -                pmutnoto(j,i),perrnoto(j,i))
          call stderr((ntot(j,i,1)-nmutn(j,i)),(NSUM(j,1)-nsumn(j)),
     -                pmutnotn(j,i),perrnotn(j,i))
         end do
        end do
        call WRDIFF('Alpha',PMUTALP,PMUTNOTALP,PERRALP,PERRNOTALP)
        call WRDIFF('Beta',PMUTBET,PMUTNOTBET,PERRBET,PERRNOTBET)
        call WRDIFF('Phi',PMUTPHI,PMUTNOTPHI,PERRPHI,PERRNOTPHI)
        call WRDIFF('Coil',PMUTCOI,PMUTNOTCOI,PERRCOI,PERRNOTCOI)
        call WRDIFF('Inaccessible',PMUTACC,PMUTINACC,PERRACC,PERRINACC)
        call WRDIFF('Side to side',PMUTS,PMUTNOTS,PERRS,PERRNOTS)
        call WRDIFF('Side to carbonyl',PMUTO,PMUTNOTO,PERRO,PERRNOTO)
        call WRDIFF('Side to amide',PMUTN,PMUTNOTN,PERRN,PERRNOTN)
      end if
C
C Output conservation table
C
      if (outprob) then
        write (3,'(//,20X,''SUMMARY OF CONSERVATION DATA'',/)')
        write (3,'(''     all  alpha   beta  + phi   coil'',
     -              ''    acc  inacc   sshb   sohb   snhb   nohb'')')
        do k=1,NUMAA
          write (3,'(A,11F7.3)') res(k),ptot(k,k,1),pmutalp(k,k),
     -                   pmutbet(k,k),pmutphi(k,k),
     -                   pmutcoi(k,k),pmutacc(k,k),pmutinacc(k,k),
     -                   pmuts(k,k),pmuto(k,k),pmutn(k,k),pmutnohb(k,k)
        end do
      end if
C
C -----------------------------------------------------------------------------
C
      close (UNIT=3)
      if (graph) then
        close (unit=4)
      end if
C
      call exit(0)
C
C Trap funny exits etc.
C
291   call exit(2)
9     write (6,'(''summer: cannot find data file'')')
      call exit(1)
34    write (6,'(''summer: error reading .sum file '',A)') filnam(i)
      call exit(1)
7     write (6,'(''summer: corrupted substitution data '',A)') filnam(i)
      call exit(1)
8     write (6,'(''summer: cannot find file '',A)') filnam(i)
      call exit(1)
15    write (6,'(''summer: error reading list file'')')
      END
C
C ============================================================================
C
      subroutine READLIS(INFIL,FILNAM,NFIL)
C
      include 'summer.h'
C
      character*14 INFIL,FILNAM(MAXFIL)
      integer       N,NFIL
C
      OPEN (UNIT=1,FILE=INFIL,FORM='FORMATTED',STATUS='OLD',err=9,
     -      CARRIAGECONTROL='LIST')
C
      N=1
1     READ (1,'(A)',END=14,err=15) FILNAM(N)
      N=N+1
      GOTO 1
14    CLOSE (UNIT=1)
      NFIL=N-1
      if (nfil.eq.0) then
        write (6,'(''summer: no filenames in list file'')')
        call exit(1)
      end if
      if (nfil.gt.maxfil) then
	write (6,'(''summer: too many files -- increase MAXFILE'')')
	call exit(1)
      end if
C
      return
C
9     write (6,'(''summer: readlis: cannot find data file'')')
      call exit(1)
15    write (6,'(''summer: readlis: error reading list file'')')
      call exit(1)
C
      end
C
C ============================================================================
C
      SUBROUTINE STDERR(N,NTOT,EST,ERR)
C
C Returns the sample proportion EST, and the standard error ERR
C
C ------------------------------------------------------------------------------
C
      implicit NONE
      integer N,
     -        NTOT
      real    EST,
     -        ERR,
     -        RN,
     -        RNTOT,
     -        SMALL
C
C -----------------------------------------------------------------------------
C
      data SMALL/1.0E-10/
C
C -----------------------------------------------------------------------------
C
C Convert integer values to real values
C
      RN=REAL(N)
      RNTOT=REAL(NTOT)+SMALL
C
      if (NTOT.gt.0) then
        EST=RN/RNTOT
        ERR=SQRT((RN*(RNTOT-RN))/(RNTOT**3))
      else 
        EST=0.0
        ERR=1.0
      end if
C
      return
      end
C
C ============================================================================
C
      subroutine VECADDR(nind,nsec,nacc,nss,nso,nsn)
C
      IMPLICIT NONE
      INTEGER  NIND,NSEC,NSS,NACC,NSO,NSN
C
      if (nind.le.16) then
        nsec=1
      else if (nind.le.32) then
        nsec=2
      else if (nind.le.48) then
        nsec=3
      else 
        nsec=4
      end if
C
      if ((nind.ge.1.and.nind.le.8).or.
     -    (nind.ge.17.and.nind.le.24).or.
     -    (nind.ge.33.and.nind.le.40).or.
     -    (nind.ge.49.and.nind.le.56)) then
        nacc=1
      else 
        nacc=2
      end if
C
      if ((nind.ge.1.and.nind.le.4).or.
     -    (nind.ge.9.and.nind.le.12).or.
     -    (nind.ge.17.and.nind.le.20).or.
     -    (nind.ge.25.and.nind.le.28).or.
     -    (nind.ge.33.and.nind.le.36).or.
     -    (nind.ge.41.and.nind.le.44).or.
     -    (nind.ge.49.and.nind.le.52).or.
     -    (nind.ge.57.and.nind.le.60)) then
        nss=1
      else 
        nss=2
      end if
      if ((nind.ge.1.and.nind.le.2).or.
     -    (nind.ge.5.and.nind.le.6).or.
     -    (nind.ge.9.and.nind.le.10).or.
     -    (nind.ge.13.and.nind.le.14).or.
     -    (nind.ge.17.and.nind.le.18).or.
     -    (nind.ge.21.and.nind.le.22).or.
     -    (nind.ge.25.and.nind.le.26).or.
     -    (nind.ge.29.and.nind.le.30).or.
     -    (nind.ge.33.and.nind.le.34).or.
     -    (nind.ge.37.and.nind.le.38).or.
     -    (nind.ge.41.and.nind.le.42).or.
     -    (nind.ge.45.and.nind.le.46).or.
     -    (nind.ge.49.and.nind.le.50).or.
     -    (nind.ge.53.and.nind.le.54).or.
     -    (nind.ge.57.and.nind.le.58).or.
     -    (nind.ge.61.and.nind.le.62)) then
        nso=1
      else
        nso=2
      end if
      nind=mod(nind,2)
      if (nind.eq.1) then
        nsn=1
      else
        nsn=2
      end if
C
      RETURN
      END
C
C ============================================================================
C
      subroutine WRDIFF(TITLE,P,PNOT,PERR,PNOTERR)
C
      include 'summer.h'
C
      character*121 HEADER2
      character*(*) TITLE
      character*1 RES(22)
      real P(NUMAA,22),PNOT(NUMAA,22),PERR(NUMAA,22),PNOTERR(NUMAA,22)
      logical  GRAPH
C
C     DATA HEADER2  /'A     C     D     E     F     G     H     I     K     L     M     N     P     Q     R     S     T     V     W     Y     J'/
      DATA HEADER2  /'A     H     I     K     L     M     N     P     Q     R     S     T     V     W     Y     J'/
      DATA RES      /'A','C','D','E','F','G','H','I','K','L',
     -               'M','N','P','Q','R','S','T','V','W','Y','J','-'/
C
      if (GRAPH) then
        write (4,'(//,A,/)') TITLE
        write (4,'('' "  " "A" "C" "D" "E" "F" "G" "H" "I" "K" "L" '',
     -           ''"M" "N" "P" "Q" "R" "S" "T" "V" "W" "Y"'')')
      end if
      write (3,'(//,A,/)') TITLE
      write (3,'(9X,A)') HEADER2
      do J=1,22
        write (3,'(1X,A,2X,21F6.3)') RES(J),((P(K,J)-PNOT(K,J)),
     -         K=1,NUMAA) 
        write (3,'(1X,1X,2X,21F6.3)') ((SQRT((PERR(K,J)**2)+(PNOTERR(K,J)**2))),K=1,NUMAA)
      end do
      return
      end
C
C ============================================================================
C
      subroutine WRPROB(TITLE,NMUT,NSUM,PMUT)
C
      include 'summer.h'
C
      character*121 HEADER2
      character*(*) TITLE
      character*1   RES(22)
      integer NMUT(NUMAA,22),NSUM(NUMAA)
      real PMUT(NUMAA,22),PERR(NUMAA)
C
      DATA RES      /'A','C','D','E','F','G','H','I','K','L',
     -               'M','N','P','Q','R','S','T','V','W','Y','J','-'/
      DATA HEADER2  /'   I     K     L     M     N     P     Q     R     S     T     V     W     Y     J'/
C      DATA HEADER2  /'A     C     D     E     F     G     H     I     K     L     M     N     P     Q     R     S     T     V     W     Y     J'/

C
C Calculates and writes out probabilities
C
      write (3,'(//,A,/)') TITLE
      write (3,'(7X,A)') HEADER2
      do I=1,22
        do J=1,NUMAA
          CALL STDERR(NMUT(J,I),NSUM(J),PMUT(J,I),PERR(J))
        end do
        write (3,'(1X,A1,2X,21(F6.3))') RES(I),(PMUT(J,I),J=1,NUMAA)
        write (3,'(1X,1X,2X,21(F6.3))') (PERR(J),J=1,NUMAA)
      end do
      write (3,'(''ent '',21f6.3)') ((ENTROPY(PMUT(I,1))),I=1,NUMAA)
C     write (16,'(''ent '',21f5.2)') ((ENTROPY(PMUT(I,1))),I=1,NUMAA)
      return
      end
C
C ============================================================================
C
      subroutine WRRAW(TITLE,NMUT,NSUM)
c
      include 'summer.h'
      character*(*) TITLE
      character*101 HEADER,SPACER
      character*1   RES(22)
      integer       NMUT(NUMAA,22),NSUM(NUMAA)

      data RES /'A','C','D','E','F','G','H','I','K','L','M','N','P','Q','R','S','T','V','W','Y','J','-'/
      DATA HEADER   /'A    C    D    E    F    G    H    I    K    L    M    N    P    Q    R    S    T    V    W    Y    J'/
      DATA SPACER   /'-----------------------------------------------------------------------------------------------------'/

      write (3,'(//,A,/)') TITLE
      write (3,'(7X,A)') HEADER 
      do I=1,22
        write (3,'(A,2X,21I5)') RES(I),(NMUT(J,I),J=1,NUMAA)
        do J=1,NUMAA
          NSUM(J)=NSUM(J)+NMUT(J,I)
        end do
      end do
      write (3,'(7X,A)') spacer 
      write (3,'(''tot'',21I5)') (NSUM(I),I=1,NUMAA)
      return
      end
C
C =============================================================================
C
      REAL FUNCTION ENTROPY(P)
C
      real P(21,22)
      entropy=0.0
C
      do I=1,22
        if (P(1,I).GT.1.e-15) then
          ENTROPY=ENTROPY+(P(1,I)*(LOG(P(1,I))))
        end if
      end do
      ENTROPY=-ENTROPY
C
      RETURN
      END
