      PROGRAM GETTRAROT
C
C JPO Nov - 88
C
C Simple RMS fitting program, Fits two datasets to each other, note that
C these must have an equal number of CA records in them, otherwise the
C program will crash.
C
C jpo - July 97
C
C Changed format to allow simple use in filters, etc 
C -s option for silence
C
      integer STDIN, STDOUT, STDERR
      parameter   (MAXATS=500)
      parameter (STDIN=5,STDOUT=6,STDERR=0)
C
      character*80 TEXT
      character*30 TEXT1(MAXATS),TEXT2(MAXATS)
      character*14 FILE1,FILE2,OPTIONS
      character*5  RESNM1(MAXATS),RESNM2(MAXATS)
      character*4  VERSION
      character*3  RESTP1(MAXATS),RESTP2(MAXATS)
      real         X1(3,MAXATS),X2(3,MAXATS)
      real         ROT(3,3),C1(3),C2(3),
     -             TRANS(3),DIST(MAXATS)
      integer      NATS1,NATS2
      logical      OUTCOR,OUTTRA,OUTDIS,FEXIST,USEDOPT,SILENT
C
C --------------------------------------------------------------------------
C
      data OUTCOR  /.FALSE./
      data OUTTRA  /.true./
      data OUTDIS  /.false./
      data USEDOPT /.FALSE./
      data VERSION /' 0.1'/
C
C --------------------------------------------------------------------------
C
      call getarg(1,options)
      if (index(options,'.').ge.1) then
        file1=options
        goto 1
      end if
      if (index(options,'d').ge.1) then
        OUTDIS=.true.
      end if
      if (index(options,'v').ge.1) then
        write (6,'(''rms - version '',A)') VERSION
        usedopt=.true.
      end if
      if (index(options,'t').ge.1) then
        outtra=.true.
        usedopt=.true.
      end if
      if (index(options,'f').ge.1) then
        outcor=.true.
        usedopt=.true.
      end if
C
      call getarg(2,file1)
1     inquire (FILE=FILE1,EXIST=FEXIST)
      if (.NOT.FEXIST) then
	WRITE (STDERR,'(''rms: file1 does not exist '')')
	call exit(1) 
      end if
      if (usedopt) then
        call getarg(3,file2)
      else
        call getarg(2,file2)
      end if
      INQUIRE (FILE=FILE2,EXIST=FEXIST)
      if (.NOT.FEXIST) then
	WRITE (STDERR,'(''rms: file2 does not exist '')')
        call exit(1)
      end if
C
C --------------------------------------------------------------------------
C
C Data for X1
C
      OPEN (UNIT=1,FILE=FILE1,STATUS='old',FORM='FORMATTED',ERR=234)
      NATS1=1
10    READ (1,17,END=100) TEXT
17    FORMAT (A)
      if (TEXT(1:6).EQ.'ATOM  '.AND.TEXT(13:16).EQ.' CA ') then
	READ (TEXT,111) TEXT1(NATS1),(X1(I,NATS1),I=1,3)
	READ (TEXT,263) RESTP1(NATS1),RESNM1(NATS1)
263     FORMAT (17X,A3,2X,A5)
111     FORMAT (A30,3F8.3)
        NATS1=NATS1+1
      END IF
      GOTO 10
100   NATS1=NATS1-1
      CLOSE (UNIT=1)
C
C Data for X2
C
      OPEN (UNIT=2,FILE=FILE2,STATUS='OLD',FORM='FORMATTED',ERR=234)
      NATS2=1
20    READ (2,17,END=200) TEXT
      if (TEXT(1:6).EQ.'ATOM  '.AND.TEXT(13:16).EQ.' CA ') then
	READ (TEXT,111) TEXT2(NATS2),(X2(I,NATS2),I=1,3)
	READ (TEXT,263) RESTP2(NATS2),RESNM2(NATS2)
        NATS2=NATS2+1
      END IF
      GOTO 20
200   NATS2=NATS2-1
      CLOSE (UNIT=2)
C
C Check array bounds
C
      if (NATS1 .lt. 3) then
        write (STDERR,'(''rms: less than three points in molecule 1 -- cannot fit'')')
        call exit(1)
      else if (NATS1 .gt. MAXATS) then
        write (STDERR,'(''rms: too many atoms in molecule 1'')')
        call exit(1)
      end if
      if (NATS2 .lt. 3) then
        write (STDERR,'(''rms: less than three points in molecule 2 -- cannot fit'')')
        call exit(1)
      else if (NATS2 .gt. MAXATS) then
        write (STDERR,'(''rms: too many atoms in molecule 2'')')
        call exit(1)
      end if
C
C --------------------------------------------------------------------------
C
C Check for an unequal number of residues in the two files
C
      IF (NATS1 .NE. NATS2) then
        write (STDERR,'(''rms: inconsistent no. of residues in files '')') NATS1,NATS2
        call exit(1)
      END IF
C
C ----------------------------------------------------------------------------
C
C Calculate COG of first set of coordinates
C Calculate COG of second set of coordinates
C Find the rotation matrix that maps second set of coords onto first
C Apply this transformation to the second set of coordinates
C Find RMS between two coordinate sets
C
      call ORIGIN(X1(1,1),NATS2,C1)
      call ORIGIN(X2(1,1),NATS2,C2)
      call MATFIT(X1(1,1),X2(1,1),ROT,NATS2,0,0.)
      call ROTFIT(X2,ROT,NATS2)
      call RMSFIT(X1,X2,RMSD,NATS2)
C
      IF (OUTTRA) then
        DO I=1,3
          TRANS(I)=C1(I)-C2(I)
        end do
        write (6,*) ' c1 ',c1(1),c1(2),c1(3)
        write (6,*) ' c2 ',c2(1),c2(2),c2(3)
        write (STDOUT,'(15f12.6,1x,i5,1x,f12.6)') (C1(K),K=1,3), (C2(K),K=1,3), ((ROT(J,K),J=1,3),K=1,3), NATS2, RMSD
      END IF
C
      if (OUTDIS) then
        write (STDOUT,'(/,'' MOLECULE 1   MOLECULE 2   DISTANCE '',
     -        /,''------------------------------------'')')
        do I=1,NATS2
          call DISTAN(X1(1,I),X2(1,I),DIST(I))
	  write (STDOUT,'(2X,A3,1X,A5,'' -- '',A3,1X,A5,3X,F6.3)') RESTP1(I),
     -         RESNM1(I),RESTP2(I),RESNM2(I),DIST(I)
	end do
	close (UNIT=8)
        write (STDOUT,'(''------------------------------------'',/)')
        write (STDOUT,'('' No. of atoms : '',I4,//,
     -             '' RMSD         : '',F6.3,//)') NATS2,RMSD
      end if
C
232   call exit(0)
234   call exit(1)
999   call exit(1)
      END
