      program TRAROT
C
      integer STDIN, STDOUT, STDERR
      parameter   (MAXATS=40, MAXRES=750, MAXHET=300)
      parameter (STDIN=5,STDOUT=6,STDERR=0)

      character*256 FILENAME
      character*10  BUFFER
      character*5   RESNUM(MAXRES), HETNUM(MAXHET)
      character*3   RESNAM(MAXRES), HETNAM(MAXHET), ATM(MAXATS,MAXRES)
      character*4   HETATM(MAXHET)
      character*1   CHNNAM(MAXRES)
      integer       LENRES(MAXRES), NUMRES, NUMCHN, NUMATS, NUMHET
      real          COORDS(3,MAXATS,MAXRES), OCCUP(MAXATS,MAXRES),
     -              BVAL(MAXATS,MAXRES), HETCOORDS(3,MAXHET),
     -              HETBVAL(MAXHET), HETOCCUP(MAXHET), X1, X2, X3, C1(3)
C
      real TRANS(3), ROT(3,3), RMSD
      integer NCA
C
C -------------------------------------------------------------------------------------------------
C
C get filename
C
      if (IARGC() .ne. 16) then
        write (STDERR,'(''usage - trarot file t1 t2 t3 c1 c2 c3 r11 r12 r13 r21 r22 r23 r31 r32 r33'')')
        call EXIT(1)
      else
        call GETARG(1,FILENAME)
        call GETARG(2,BUFFER)
        read (BUFFER,'(F12.6)',err=923) TRANS(1)
        call GETARG(3,BUFFER)
        read (BUFFER,'(F12.6)',err=923) TRANS(2)
        call GETARG(4,BUFFER)
        read (BUFFER,'(F12.6)',err=923) TRANS(3)
        call GETARG(5,BUFFER)
        read (BUFFER,'(F12.6)',err=923) C1(1)
        call GETARG(6,BUFFER)
        read (BUFFER,'(F12.6)',err=923) C1(2)
        call GETARG(7,BUFFER)
        read (BUFFER,'(F12.6)',err=923) C1(3)
        call GETARG(8,BUFFER)
        read (BUFFER,'(F12.6)',err=923) ROT(1,1)
        call GETARG(9,BUFFER)
        read (BUFFER,'(F12.6)',err=923) ROT(2,1)
        call GETARG(10,BUFFER)
        read (BUFFER,'(F12.6)',err=923) ROT(3,1)
        call GETARG(11,BUFFER)
        read (BUFFER,'(F12.6)',err=923) ROT(1,2)
        call GETARG(12,BUFFER)
        read (BUFFER,'(F12.6)',err=923) ROT(2,2)
        call GETARG(13,BUFFER)
        read (BUFFER,'(F12.6)',err=923) ROT(3,2)
        call GETARG(14,BUFFER)
        read (BUFFER,'(F12.6)',err=923) ROT(1,3)
        call GETARG(15,BUFFER)
        read (BUFFER,'(F12.6)',err=923) ROT(2,3)
        call GETARG(16,BUFFER)
        read (BUFFER,'(F12.6)',err=923) ROT(3,3)
      end if
C
C read coord set
C
      call RDPDB(FILENAME,ATM,RESNUM,RESNAM,CHNNAM,COORDS,OCCUP,BVAL,
     -                 LENRES,NUMATS,NUMRES,NUMCHN,NUMHET,HETCOORDS,
     -                 HETNUM,HETNAM,HETATM)
C
C apply transform
C
C move to C1
C
      do I=1,NUMRES
        do J=1,LENRES(I)
          do K=1,3
            COORDS(K,J,I)=COORDS(K,J,I)+C1(K)
          end do
        end do
      end do
      do I=1,NUMHET
        do J=1,3
          HETCOORDS(J,I)=HETCOORDS(J,I)+C1(J)
        end do
      end do
C
C
C first to rotation
C
      do I=1,NUMRES
        do J=1,LENRES(I)
          X1 = ROT(1,1)*COORDS(1,J,I) + ROT(1,2)*COORDS(2,J,I) + ROT(1,3)*COORDS(3,J,I)
          X2 = ROT(2,1)*COORDS(1,J,I) + ROT(2,2)*COORDS(2,J,I) + ROT(2,3)*COORDS(3,J,I)
          X3 = ROT(3,1)*COORDS(1,J,I) + ROT(3,2)*COORDS(2,J,I) + ROT(3,3)*COORDS(3,J,I)
          COORDS(1,J,I) = X1
          COORDS(2,J,I) = X2
          COORDS(3,J,I) = X3
        end do
      end do
      do J=1,NUMHET
        X1 = ROT(1,1)*HETCOORDS(1,J) + ROT(1,2)*HETCOORDS(2,J) + ROT(1,3)*HETCOORDS(3,J)
        X2 = ROT(2,1)*HETCOORDS(1,J) + ROT(2,2)*HETCOORDS(2,J) + ROT(2,3)*HETCOORDS(3,J)
        X3 = ROT(3,1)*HETCOORDS(1,J) + ROT(3,2)*HETCOORDS(2,J) + ROT(3,3)*HETCOORDS(3,J)
        HETCOORDS(1,J) = X1
        HETCOORDS(2,J) = X2
        HETCOORDS(3,J) = X3
      end do
C
C now the tranlsation
C
      do I=1,NUMRES
        do J=1,LENRES(I)
          do K=1,3
            COORDS(K,J,I)=COORDS(K,J,I)+TRANS(K)
          end do
        end do
      end do
      do I=1,NUMHET
        do J=1,3
          HETCOORDS(J,I)=HETCOORDS(J,I)+TRANS(J)
        end do
      end do
C
C output coordinates
C
      call WRPDB(STDOUT,ATM,RESNUM,RESNAM,CHNNAM,COORDS,OCCUP,BVAL,
     +                 LENRES,NUMATS,NUMRES,NUMCHN,NUMHET,HETCOORDS,
     +                 HETNUM,HETNAM,HETATM)
C
C normal exit
C
      call EXIT(0)
C
923   write (STDERR,'(''trarot: error parsing transform'')')
      call EXIT(1)
C
      end
C
C ---------------------------------------------------------------------
C
      subroutine ROTATE(X,R,N)
C
C  Rotates N contents of X using rotation matrix R.
C
      integer N
      real X(3,N),R(3,3)
C
      do I=1,N
        X1 = R(1,1)*X(1,I) + R(1,2)*X(2,I) + R(1,3)*X(3,I)
        X2 = R(2,1)*X(1,I) + R(2,2)*X(2,I) + R(2,3)*X(3,I)
        X3 = R(3,1)*X(1,I) + R(3,2)*X(2,I) + R(3,3)*X(3,I)
        X(1,I) = X1
        X(2,I) = X2
        X(3,I) = X3
      end do
C
      return
      end
C
C =============================================================================
C
      SUBROUTINE RDPDB(FILENAME,ATM,RESNUM,RESNAM,CHNNAM,COORDS,OCCUP,BVAL,
     -                 LENRES,NUMATS,NUMRES,NUMCHN,NUMHET,HETCOORDS,
     -                 HETNUM,HETNAM,HETATM)
C
C copyright (c) 1988-1991 John Overington
C part of joy
C
C Subroutine that reads a PDB format file. ATOMS and HETATM are read into
C separate arrays. If two (or more) conformers of the same sidechain exist
C then the A form is taken in preference.
C
      parameter   (MAXATS=40, MAXRES=750, MAXHET=300)
C
      character*256 FILENAME
      CHARACTER*66  CARD((MAXATS*MAXRES)+MAXHET)
      CHARACTER*5   RESNUM(MAXRES),
     -              HETNUM(MAXHET)
      CHARACTER*3   RESNAM(MAXRES), HETNAM(MAXHET), ATM(MAXATS,MAXRES)
      character*4   HETATM(MAXHET)
      CHARACTER*1   CHNNAM(MAXRES)
      INTEGER       LENRES(MAXRES), NUMRES, NUMCHN, NUMATS, NUMHET
      integer       MAXREC,NUMREC,I,J,LM,M,K
      real          COORDS(3,MAXATS,MAXRES), OCCUP(MAXATS,MAXRES),
     -              BVAL(MAXATS,MAXRES), HETCOORDS(3,MAXHET),
     -              HETBVAL(MAXHET), HETOCCUP(MAXHET)

C -------------------------------------------------------------------------
C
      MAXREC=(MAXATS*MAXRES)+MAXHET
      DO I=1,MAXRES
        LENRES(I)=0
      END DO
      NUMRES=0
      NUMCHN=0
      NUMATS=0
      NUMHET=0
C
C -------------------------------------------------------------------------
C
      open (unit=1,file=FILENAME,status='old',form='FORMATTED',err=999)
C
C -------------------------------------------------------------------------
C
C Read all data into CARD buffer, NUMREC items, modified to read both hetatoms
C and normal atom records into buffer. Fixed treatment of NMR data files
C (If find ENDMDL, then skip rest of file)
C
      NUMREC=1
4     READ (1,'(A)',end=3,err=902) CARD(NUMREC)
        IF (CARD(NUMREC)(1:6).EQ.'ATOM  '.OR.
     +      CARD(NUMREC)(1:6).EQ.'HETATM') THEN
          NUMREC=NUMREC+1
          if (numrec.gt.maxrec) then
            write (STDERR,'(''rdpdb: too many records'')')
            call exit(1)
          end if
        else if (CARD(NUMREC)(1:6) .eq. 'ENDMDL') then
          goto 3
        end if
      GOTO 4
3     CONTINUE
      NUMREC=NUMREC-1
      CLOSE (UNIT=1)
      NUMATS=NUMREC
C
C -------------------------------------------------------------------------
C
      J=1
      I=1
      LM=1
C
C Process all these NUMREC records
C
      DO M=1,NUMREC
        IF (CARD(M)(1:6).EQ.'ATOM  ') THEN
          IF (M .gt. 1) THEN
            IF (CARD(M)(23:27).NE.CARD(M-1)(23:27)) THEN
              LENRES(I)=J-1
              if (lenres(i).gt.maxats) then
                write (STDERR,'(''rdpdb: too many atoms - '',
     -                     ''increase MAXATS'')')
                call exit(1)
              end if
              I=I+1
              J=1
              NUMRES=NUMRES+1
            END IF
          END IF
          READ (CARD(M),'(13X,A3,1X,A3,1X,A1,A5,3X,3F8.3,2F6.2)',
     +          err=903)
     -          ATM(J,I),RESNAM(I),CHNNAM(I),RESNUM(I),
     -          (COORDS(K,J,I),K=1,3),OCCUP(J,I),BVAL(J,I)
          J=J+1
          if (J.gt.MAXRES) then
            write (STDERR,'(''rdpdb: too many residues '',
     +                 ''-- increase MAXRES'')')
            call EXIT(1)
          end if
          NUMATS=NUMATS+1
C
          if (M .gt. 1) then
            if (CARD(M)(22:22).ne.CARD(M-1)(22:22)) then
              NUMCHN=NUMCHN+1
            end if
          end if
C
C -------------------------- HETATMS -----------------------------------
C
        ELSE 
          READ (CARD(M),'(12X,A4,1X,A3,2X,A5,3X,3F8.3,2F6.2)',
     +          err=903)
     -          HETATM(LM),HETNAM(LM),HETNUM(LM),
     +          (HETCOORDS(K,LM),K=1,3),
     -          HETOCCUP(LM),HETBVAL(LM)
          IF (HETATM(LM).EQ.'E  ') THEN
            HETATM(LM)='FE '
          END IF
          LM=LM+1
          if (LM.gt.MAXHET) then
            write (STDERR,'(''rdpdb: too many het atoms '',
     +                 ''-- increase MAXHET'')')
            call EXIT(1)
          end if
          NUMHET=NUMHET+1
        END IF
      END DO
C
C Don't forget the last residue length
C
      LENRES(I)=J-1
      NUMRES=NUMRES+1
      NUMCHN=NUMCHN+1
C
C Catch array bounds exceeded
C
      if (lenres(i).gt.maxats) then
        write (STDERR,'(''rdpdb: too many atoms'')')
        call exit(1)
      end if
      if (numres.gt.maxres) then
        write (STDERR,'(''rdpdb: too many residues'')')
        call exit(1)
      end if
      if (numhet.gt.maxhet) then
        write (STDERR,'(''rdpdb: too many hetatoms'')')
        call exit(1)
      end if
C
C -------------------------------------------------------------------------
C
      return
C
C -------------------------------------------------------------------------
C
999   write (STDERR,'(''rdpdb: cannot process file '',A)') file
      call exit(1)
902   write (STDERR,'(''rdpdb: bad format in file'')')
      call EXIT(1)
903   write (STDERR,'(''rdpdb: bad read of internal file'')')
      call EXIT(1)
C
      END
C
C ==============================================================================
C
      subroutine WRPDB(IIO,ATM,RESNUM,RESNAM,CHNNAM,COORDS,OCCUP,BVAL,
     +                 LENRES,NUMATS,NUMRES,NUMCHN,NUMHET,HETCOORDS,
     +                 HETNUM,HETNAM,HETATM)
C
C copyright (c) 1988-1991 John Overington
C part of joy
C
      INTEGER IIO, STDERR
      parameter   (MAXATS=40, MAXRES=750, MAXHET=300, STDERR=0)
C
      character*5 RESNUM(MAXRES), HETNUM(MAXHET)
      character*3 RESNAM(MAXRES), HETNAM(MAXHET), ATM(MAXATS,MAXRES)
      character*4 HETATM(MAXHET)
      character*1 CHNNAM(MAXRES)
      integer LENRES(MAXRES), NUMRES, NUMCHN, NUMATS, NUMHET
      integer I, J, K, L
      real COORDS(3,MAXATS,MAXRES), OCCUP(MAXATS,MAXRES)
      real HETOCCUP(MAXHET)
      real BVAL(MAXATS,MAXRES), HETCOORDS(3,MAXHET), HETBVAL(MAXHET)
C
      N=0
C
      do I=1,NUMRES
        do J=1,LENRES(I)
          N=N+1
          write (IIO,'(''ATOM  '',I5,2X,A3,1X,A3,1X,A1,A5,3X,3F8.3,2F6.2)',err=903)
     +          N,ATM(J,I),RESNAM(I),CHNNAM(I),RESNUM(I),
     +          (COORDS(K,J,I),K=1,3),OCCUP(J,I),BVAL(J,I)
        end do
      end do
      do L=1,NUMHET
        N=N+1
        write (IIO,'(''HETATM'',I5,1X,A4,1X,A3,2X,A5,3X,3F8.3,2F6.2)',err=903)
     +        N,HETATM(L),HETNAM(L),HETNUM(L),(HETCOORDS(K,L),K=1,3),
     +        HETOCCUP(L),HETBVAL(L)
      end do
C
      close (unit=1)
C
      return
C
999   write (STDERR,'(''wrpdb: error opening file '',A)') FILE
      call EXIT(1)
903   write (STDERR,'(''wrpdb: error reading file '',A)') FILE
      call EXIT(1)
C
      end

