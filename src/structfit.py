import numpy as np
import re
import argparse
import chimera
from MatchMaker import (match,
                        CP_BEST,
                        GAP_OPEN,
                        GAP_EXTEND,
                        defaults,
                        MATRIX,
                        ITER_CUTOFF)
parser = argparse.ArgumentParser(description='Superimpose two PDB structures based on a sequence alignment, return RMSD')
parser.add_argument('id1', type=str, help='a PDB id')
parser.add_argument('id2', type=str, help='a second PDB id')
parser.add_argument('-v', '--verbose', action='store_true', default=False, help='Output all residues included in RMSD calculation')
args = parser.parse_args()


model1, model2 = [args.id1, args.id2]
m0 = chimera.openModels.open(model1,type="PDB")[0]
m1 = chimera.openModels.open(model2,type="PDB")
atoms1, atoms2, rmsd, fullrmsd = match(CP_BEST, (m0, m1), defaults[MATRIX],"nw",defaults[GAP_OPEN],defaults[GAP_EXTEND])[0]

print "%s\t%s\t%f\t%f" % (model1, model2, rmsd, fullrmsd)
if args.verbose:
  print
  print "\t".join(map(str, ["res_1", "ch_1", "res_2", "ch_2"]))
  arr = []
  for a1, a2 in zip(atoms1, atoms2):
    r1, c1 = re.split('[:.]', str(a1.residue))[1:]
    r2, c2 = re.split('[:.]', str(a2.residue))[1:]
    arr.append([r1, c1, r2, c2])
  # return the unique rows
  arr = np.vstack({tuple(row) for row in arr})
  arr = np.sort(arr)
  for row in arr:
    print "\t".join(map(str, row))
